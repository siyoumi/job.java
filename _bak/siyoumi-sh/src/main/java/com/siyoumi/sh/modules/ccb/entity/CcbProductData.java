package com.siyoumi.sh.modules.ccb.entity;

import lombok.Data;

//查询商品参数
@Data
public class CcbProductData {
    String token; //token
    String channelId;
    String channelKey;
}
