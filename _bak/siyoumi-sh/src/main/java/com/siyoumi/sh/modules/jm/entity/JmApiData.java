package com.siyoumi.sh.modules.jm.entity;

import lombok.Data;

import java.util.List;

@Data
public class JmApiData {
    public String siteId;
    public String apiRoot;
    public List<JmApiToken> jmApiTokens;
}
