package com.siyoumi.sh.modules.jm;

import com.siyoumi.sh.modules.common.HandleBase;
import com.siyoumi.sh.modules.jm.entity.*;
import com.siyoumi.util.XApp;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JmHandleBase
        extends HandleBase {
    @Setter
    @Getter
    private JmApiData jmApiData;

    public String getSiteId() {
        return getJmApiData().getSiteId();
    }

    public String getApiRoot() {
        return getJmApiData().getApiRoot();
    }


    public JmApiData getApiDataByServer(String server) {
        JmHandleBase handle = null;
        switch (server) {
            case "wan":
                handle = new WanHandle();
                break;
            case "jm":
                handle = new JmHandle();
                break;
        }

        if (handle == null) {
            return null;
        }

        return handle.getJmApiData();
    }

    public List<StarMasterList> startMasterListOfObj(String actId, String masterOpenid, JmApiToken apiToken) {
        XReturn r = startMasterList(actId, masterOpenid, apiToken);
        return startMasterListOfObj(r);
    }

    /**
     * 发起列表，转对象
     */
    public List<StarMasterList> startMasterListOfObj(XReturn r) {
        List<StarMasterList> masterList = new ArrayList<>();
        if (r.err()) {
            return masterList;
        }

        Map<String, Object> mapData = r.getData("data");
        List<Map<String, Object>> mapMasterList = (List<Map<String, Object>>) mapData.get("master_list");
        for (Map<String, Object> mapMaster : mapMasterList) {
            String masterId = (String) mapMaster.get("samaster_id");
            Integer helpCount = (Integer) mapMaster.get("__help_count");
            String str01 = (String) mapMaster.get("samaster_str_01");
            String str00 = (String) mapMaster.get("samaster_str_00");

            StarMasterList data = new StarMasterList();
            data.setMasterId(masterId);
            data.setStr_00(str00);
            data.setStr_01(str01);
            data.setHelpCount(helpCount);
            masterList.add(data);
        }

        return masterList;
    }


    /**
     * 查看发起列表
     *
     * @param actId
     * @param apiToken
     */
    public XReturn startMasterList(String actId, String masterOpenid, JmApiToken apiToken) {
        String apiUrl = XStr.concat(getApiRoot(), "z_app/star_act/web/zz_api__api_x__user_master_list?"
                , "site_id=", getSiteId()
                , "&act_id=", actId
                , "&openid=", masterOpenid
                , "&", apiToken.getWxFromQuery()
        );

        return forApi(apiUrl);
    }

    /**
     * 发起
     *
     * @param actId
     * @param apiToken
     */
    public XReturn startLaunch(String actId, JmApiToken apiToken) {
        String apiUrl = XStr.concat(getApiRoot(), "z_app/star_act/web/zz_api__api_x__user_launch?"
                , "site_id=", getSiteId()
                , "&act_id=", actId
                , "&", apiToken.getWxFromQuery()
        );

        return forApi(apiUrl);
    }

    /**
     * 更新分数
     *
     * @param actId
     * @param masterId 发起ID
     * @param score    分数
     * @param apiToken
     */
    public XReturn startLaunchUpdate(String actId, String masterId, Integer score, JmApiToken apiToken) {
        String scoreEnc = XStr.md5(XStr.md5(score.toString()).toUpperCase()).toUpperCase();
        String apiUrl = XStr.concat(getApiRoot(), "z_app/star_act/web/zz_api__api_x__user_launch_update?"
                , "site_id=", getSiteId()
                , "&act_id=", actId
                , "&master_id=", masterId
                , "&samaster_score=" + score
                , "&samaster_score_enc=", scoreEnc
                , "&", apiToken.getWxFromQuery()
        );

        return forApi(apiUrl);
    }

    /**
     * 抽奖
     *
     * @param actId
     * @param apiToken
     */
    public XReturn startChou(String actId, JmApiToken apiToken) {
        return startChou(actId, apiToken, null);
    }

    public XReturn startChou(String actId, JmApiToken apiToken, String masterId) {
        String apiUrl = XStr.concat(getApiRoot(), "z_app/star_act/web/zz_api__api_x__user_chou?"
                , "site_id=", getSiteId()
                , "&act_id=", actId
                , "&", apiToken.getWxFromQuery()
        );

        if (XStr.hasAnyText(masterId)) {
            apiUrl += "&master_id=" + masterId;
        }

        return forApi(apiUrl);
    }

    /**
     * 帮忙
     *
     * @param actId
     * @param apiToken
     */
    public XReturn startHelp(String actId, String masterId, JmApiToken apiToken, String data) {
        String apiUrl = XStr.concat(getApiRoot(), "z_app/star_act/web/zz_api__api_x__user_help?"
                , "site_id=", getSiteId()
                , "&act_id=", actId
                , "&master_id=", masterId
                , "&data=", data
                , "&", apiToken.getWxFromQuery()
        );

        return forApi(apiUrl);
    }

    /**
     * 获取活动信息
     */
    public StarInfo startInfo(String actId, JmApiToken apiToken) {
        String apiUrl = XStr.concat(getApiRoot(), "z_app/star_act/web/zz_api__api_x__get_star_act?"
                , "site_id=", getSiteId()
                , "&act_id=", actId
                , "&", apiToken.getWxFromQuery()
        );

        StarInfo info = new StarInfo();
        XReturn r = forApi(apiUrl);
        if (r.err()) {
            return info;
        }

        Map data = r.getData("data");
        info.setSact_start_time(XDate.parse((String) data.get("sact_start_time")));
        info.setSact_end_time(XDate.parse((String) data.get("sact_end_time")));
        info.setSact_name((String) data.get("sact_name"));

        return info;
    }

    /**
     * 获取积分
     *
     * @param apiToken
     */
    public Integer getFun(JmApiToken apiToken) {
        String apiUrl = XStr.concat(getApiRoot(), "z_app/fun/web/zz_api__api__list?"
                , "site_id=", getSiteId()
                , "&", apiToken.getWxFromQuery()
        );

        XReturn r = forApi(apiUrl);
        if (r.err()) {
            return 0;
        }

        return r.getData("fun");
    }


    /**
     * 接口循环调用
     *
     * @param apiUrl
     */
    protected XReturn forApi(String apiUrl) {
        XReturn r;
        while (true) {
            r = requestApi(apiUrl);
            if (r.getErrMsg().contains("connect timed out")) {
                r.setErrCode(-30301234);
            }
            if (r.getErrCode() != -30301234) {
                break;
            }
            XApp.sleepMs(100);
        }

        return r;
    }
}


