package com.siyoumi.sh.modules.jm.entity;

import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import lombok.Data;

import java.time.Duration;
import java.time.LocalDateTime;

//积分商城
@Data
public class FunMallGoods {
    String abc_id;  //活动名称
    String abc_name;  //活动名称
    LocalDateTime abc_begin_time;  //开始时间
    LocalDateTime abc_end_time; //结束时间
    Integer abc_fun; //需要积分
    Integer stcnt_count_left; //剩余库存

    /**
     * 离开始还剩多少秒
     */
    public Long leftSecond() {
        Duration between = XDate.between(XDate.now(), getAbc_begin_time());
        long seconds = between.getSeconds();
        if (seconds < 0) {
            seconds = 0;
        }

        return seconds;
    }


    public XReturn validDate() {
        if (XDate.now().isBefore(getAbc_begin_time())) {
            return XReturn.getR(20019, "未开始");
        }

        if (XDate.now().isAfter(getAbc_end_time())) {
            return XReturn.getR(20022, "已结束");
        }

        return XReturn.getR(0, "进行中");
    }
}
