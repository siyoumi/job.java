package com.siyoumi.sh.modules.ccb.entity;

import lombok.Data;

//兑换中心-奖品
@Data
public class CcbFuTask {
    String title; //名称
    String taskId;
    Integer status; //状态
}
