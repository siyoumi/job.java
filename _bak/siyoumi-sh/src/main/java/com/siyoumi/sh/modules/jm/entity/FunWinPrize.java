package com.siyoumi.sh.modules.jm.entity;

import lombok.Data;

import java.time.LocalDateTime;

//积分夺宝
@Data
public class FunWinPrize {
    String funwin_id;  //夺宝ID
    Integer funwin_total; //奖品数量
    String psend_name;  //名称
    LocalDateTime funwin_start_date;  //开始时间
    LocalDateTime funwin_end_date; //结束时间

    Integer state; //状态
}
