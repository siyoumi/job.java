package com.siyoumi.sh.modules.yuexiu.entity;

import lombok.Data;

//签到参数
@Data
public class YueXiuSignData {
    String cid;
    String marketId;

    public static YueXiuSignData getIns(String marketId, String cid) {
        YueXiuSignData data = new YueXiuSignData();
        data.setCid(cid);
        data.setMarketId(marketId);
        return data;
    }
}
