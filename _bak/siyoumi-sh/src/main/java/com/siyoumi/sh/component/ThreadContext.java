package com.siyoumi.sh.component;

import com.siyoumi.util.XStr;
import com.siyoumi.util.entity.ThreadLocalData;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ThreadContext {
    private static String keyArgs = "__args__";

    public static void set(String k, Object v) {
        ThreadLocalData.set(k, v);
    }

    public static <T> T get(String k) {
        return get(k, null);
    }

    public static <T> T get(String k, T def) {
        if (XStr.isNullOrEmpty(k)) {
            return def;
        }

        return ThreadLocalData.get(k, def);
    }


    public static void setArgs(String... args) {
        List<String> v = new ArrayList<>();
        if (args.length > 0) {
            v = List.of(args);
        }
        set(keyArgs, v);
    }

    public static List<String> getArgs() {
        return get(keyArgs);
    }
}
