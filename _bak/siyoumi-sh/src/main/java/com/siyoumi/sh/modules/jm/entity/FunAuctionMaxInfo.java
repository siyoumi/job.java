package com.siyoumi.sh.modules.jm.entity;

import lombok.Data;

//拍卖最高价-信息
@Data
public class FunAuctionMaxInfo {
    String wxuser_id;  //用户
    Integer arecord_fun = 0; //积分价
}
