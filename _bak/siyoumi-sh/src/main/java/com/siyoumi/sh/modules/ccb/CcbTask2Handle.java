package com.siyoumi.sh.modules.ccb;

import com.siyoumi.sh.modules.ccb.entity.CcbApiToken;
import com.siyoumi.sh.modules.ccb.entity.CcbUser;
import com.siyoumi.sh.modules.common.IHandle;
import com.siyoumi.sh.modules.common.send_msg.TlSendMsg;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XHttpClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

//建行ccb,任务
@Slf4j
public class CcbTask2Handle
        extends CcbHandleBase
        implements IHandle {


    @SneakyThrows
    @Override
    public void run() {
        TlSendMsg.remove();

        List<CcbApiToken> tokenArr = getArrToken();
        if (tokenArr.size() <= 0) {
            log.error("未配置CCB_TOKEN");
            return;
        }
        log.info("BEGIN: {}", XDate.toDateTimeString());

        logAndSet(TlSendMsg.get(), "**跨境环游之旅** \n\n");
        for (CcbApiToken apiToken : getArrToken()) {
            getToken(apiToken);
            sendMsg(TlSendMsg.get().toString());
        }
        TlSendMsg.remove();
        log.info("END: {}", XDate.toDateTimeString());
    }

    public String getToken(CcbApiToken apiToken) {
        CcbUser ccbUser = getZhcToken(apiToken, "KJZQZFJMK001");
        if (ccbUser == null) {
            logAndSet(TlSendMsg.get(), "获取zhc_token失败\n\n");
            return null;
        }

        XHttpClient client = XHttpClient.getInstance();
        client.get(ccbUser.getRedirectUrl(), getHeader());
        return null;
    }
}
