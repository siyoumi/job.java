package com.siyoumi.sh.modules.common.send_msg;

//统一发信息接口
public class TlSendMsg {
    static ThreadLocal<StringBuilder> tl = new ThreadLocal<>();

    static public void remove() {
        tl.remove();
    }

    static public StringBuilder get() {
        StringBuilder sb = tl.get();
        if (sb == null) {
            sb = new StringBuilder();
            tl.set(sb);
        }
        return sb;
    }
}
