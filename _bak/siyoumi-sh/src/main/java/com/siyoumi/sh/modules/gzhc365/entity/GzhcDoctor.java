package com.siyoumi.sh.modules.gzhc365.entity;

import lombok.Data;

//医生
@Data
public class GzhcDoctor
        extends GzhcHis {
    String id;
    Boolean test = false; //测试模式

    GzhcDoctorWorkToday doctorWork; //抢指定的日志
}
