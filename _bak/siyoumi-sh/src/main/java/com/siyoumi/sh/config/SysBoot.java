package com.siyoumi.sh.config;

import com.alibaba.fastjson.JSON;
import com.siyoumi.sh.component.ThreadContext;
import com.siyoumi.sh.modules.ccb.CcbPrizeStockHandle;
import com.siyoumi.sh.modules.ccb.CcbTaskHandle;
import com.siyoumi.sh.modules.common.IHandle;
import com.siyoumi.sh.modules.gzhc365.GzhcHandle;
import com.siyoumi.sh.modules.gzhc365.GzhcHandle34;
import com.siyoumi.sh.modules.jm.*;
import com.siyoumi.sh.modules.qmai.QmaiHandle;
import com.siyoumi.sh.modules.yuexiu.YueXiuHandle;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

//测试入口
@Slf4j
@Component
public class SysBoot
        implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        log.debug("sys boot");
        log.debug("args:{}", JSON.toJSONString(args));

        ThreadContext.setArgs(args);

        String handleIndex = "";
        if (args.length > 0) {
            handleIndex = args[0];
        }

        //ISendMsg sendMsg = ISendMsg.getIns();
        //sendMsg.send("test");
        //CronUtil.setMatchSecond(true);
        //CronUtil.start(true);

        List<IHandle> handleArr = new ArrayList<>();
        if (XStr.hasAnyText(handleIndex)) {
            IHandle handle = selectHandle(handleIndex);
            if (handle == null) {
                log.error("未找到handle {}", handleIndex);
                return;
            }

            handleArr.add(handle);
        } else {
            if (SysConfig.getIns().isDev()) {
                log.debug("测试");
                //handleArr.add(new YueXiuHandle());
                //handleArr.add(new QmaiHandle());
                handleArr.add(new CcbPrizeStockHandle());
            } else {
                log.debug("默认");
                //handleArr.add(new JmHandle());
                //handleArr.add(new QmaiHandle());
                //handleArr.add(new WatsonsHandle());
                handleArr.add(new YueXiuHandle());
                //handleArr.add(new WanHandle());
            }
        }

        for (IHandle handle : handleArr) {
            try {
                handle.run();
            } catch (Exception ex) {
                ex.printStackTrace();
                log.error(ex.getMessage());
            }
        }
    }


    //指定运行
    protected IHandle selectHandle(String handleIndex) {
        switch (handleIndex) {
            case "wan":
                return new WanHandle();
            case "jm":
                return new JmHandle();
            case "watsons":
                return new WatsonsHandle();
            case "yuexiu":
                return new YueXiuHandle();
            case "fun_auction":
                return new FunAuctionHandle();
            case "fun_mall":
                return new FunMallHandle();
            case "fun_win":
                return new FunWinHandle();
            case "chou":
                return new StarActChouHandle();
            case "game":
                return new StarActGameHandle();
            case "ccb":
                return new CcbTaskHandle();
            case "gzhc":
                return new GzhcHandle();
            case "gzhc_34":
                return new GzhcHandle34();
            case "qmai":
                return new QmaiHandle();
            default:
                return null;
        }
    }
}
