package com.siyoumi.sh.modules.ccb;

import com.alibaba.fastjson.JSONObject;
import com.siyoumi.sh.component.XRedis;
import com.siyoumi.sh.modules.ccb.entity.CcbApiToken;
import com.siyoumi.sh.modules.ccb.entity.CcbUser;
import com.siyoumi.sh.modules.common.HandleBase;
import com.siyoumi.sh.modules.common.send_msg.TlSendMsg;
import com.siyoumi.util.*;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//建行ccb
@Slf4j
public class CcbHandleBase
        extends HandleBase {
    @Getter
    String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 NetType/WIFI MicroMessenger/7.0.20.1781(0x6700143B) " +
            "WindowsWechat(0x63090621) XWEB/8391 Flue";

    protected Map<String, String> getHeader() {
        Map<String, String> mapHeader = new HashMap<>();
        mapHeader.put("User-Agent", getUserAgent());
        return mapHeader;
    }

    /**
     * token格式
     * wParam|name@wParam2|name2
     */
    protected List<CcbApiToken> getArrToken() {
        String token = System.getenv("CCB_TOKEN");
        if (XStr.isNullOrEmpty(token)) {
            return new ArrayList<>();
        }

        List<CcbApiToken> tokenList = new ArrayList<>();
        String[] tokenArr = token.split("@");
        for (String s : tokenArr) {
            String[] dataArr = s.split("\\|");

            tokenList.add(CcbApiToken.getIns(dataArr[0], dataArr[1]));
        }

        return tokenList;
    }


    protected XReturn paresR(String returnStr) {
        XReturn r = XReturn.parse(returnStr);
        String message = r.getData("message");
        Object code = r.getData("code");
        Integer codeNum = 123456;
        if (code instanceof String) {
            codeNum = XStr.toInt((String) code);
        }
        if (code instanceof Integer) {
            codeNum = (Integer) code;
        }

        r.setErrMsg(message);
        r.setErrCode(codeNum);
        if (codeNum == 200) { //200表示成功
            r.setErrCode(0);
        }

        return r;
    }

    public CcbUser getZhcToken(CcbApiToken apiToken) {
        return getZhcToken(apiToken, null);
    }

    public CcbUser getZhcToken(CcbApiToken apiToken, String shortId) {
        if (XStr.isNullOrEmpty(shortId)) {
            shortId = "polFsWD2jPnjhOx9ruVBcA";
        }

        String url = "https://event.ccbft.com/api/flow/nf/shortLink/redirect/ccb_gjb";
        Map<String, Object> postData = new HashMap<>();
        postData.put("appId", "wxd513efdbf26b5744");
        postData.put("archId", "ccb_gjb");
        postData.put("channelId", "wx");
        postData.put("shortId", shortId);
        postData.put("ifWxFirst", false);
        postData.put("wxUUID", apiToken.getUuid());

        XReturn r = postJson(url, getHeader(), postData);
        if (r.err()) {
            logAndSet(TlSendMsg.get(), "获取zhc_toekn失败：" + r.getErrMsg() + "\n\n");
            return null;
        }

        JSONObject data = (JSONObject) r.get("data");
        String redirectUrl = data.getString("redirectUrl");
        Map<String, String> urlQuery = XStr.urlQuery(redirectUrl);
        String zhcToken = urlQuery.get("__dmsp_token");

        CcbUser ccbUser = new CcbUser();
        ccbUser.setZhcToken(zhcToken);
        ccbUser.setWxUUID(apiToken.getUuid());
        ccbUser.setRedirectUrl(redirectUrl);

        return ccbUser;
    }

    //zhc_token登陆
    public XReturn zhcTokenAuthLogin(String zhcToken) {
        String url = "https://m3.dmsp.ccb.com/api/businessCenter/auth/login";

        Map<String, Object> mapPost = new HashMap<>();
        mapPost.put("channelId", "wx");
        mapPost.put("channelType", "01");
        mapPost.put("recordCode", "MzM0MWNiNjUtNzUwNi00MWFhLTg3OTgtNjZiNDM3ZDUyOWJm");
        mapPost.put("taskId", "RW20221028500076");
        mapPost.put("token", zhcToken);

        XReturn r = postJson(url, getHeader(), mapPost);
        if (r.err()) {
            return r;
        }

        return r;
    }


    public XReturn postJson(String url, Map<String, String> header, Map<String, Object> postData) {
        log.debug("url: {}", url);
        log.debug("post_data: {}", XJson.toJSONString(postData));

        XHttpClient client = XHttpClient.getInstance();
        String returnStr = client.postJson(url, header, postData);
        log.debug("return_str: {}", returnStr);
        return paresR(returnStr);
    }
}
