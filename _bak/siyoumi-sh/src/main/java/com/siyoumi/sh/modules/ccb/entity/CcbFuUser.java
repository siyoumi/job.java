package com.siyoumi.sh.modules.ccb.entity;

import lombok.Data;

//
@Data
public class CcbFuUser {
    String userId; //名称
    String token;
}
