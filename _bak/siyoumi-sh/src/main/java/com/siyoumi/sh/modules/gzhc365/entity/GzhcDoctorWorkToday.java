package com.siyoumi.sh.modules.gzhc365.entity;

import lombok.Data;

import java.time.LocalDateTime;

//获取医生可约的挂号记录(当天)
@Data
public class GzhcDoctorWorkToday {
    LocalDateTime date;
    String time = "";
    String deptNo; //科室Id
    String extFields; //科室名称
    Integer regFee; //挂号金额
    Integer leftNum; //剩余库存
    Integer totalNum; //总库存
    Integer timeType = 2; //0:早上,1:下午,2：全天
}
