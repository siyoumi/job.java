package com.siyoumi.sh.modules.jm.entity;

import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import lombok.Data;

import java.time.Duration;
import java.time.LocalDateTime;

//拍卖
@Data
public class FunAuctionGoods {
    String agoods_id;  //活动名称
    String agoods_name;  //活动名称
    LocalDateTime agoods_date_begin;  //开始时间
    LocalDateTime agoods_date_end; //结束时间
    Integer fun_max; //当前最高分

    /**
     * 离结束还剩多少秒
     */
    public Long leftSecond() {
        Duration between = XDate.between(XDate.now(), getAgoods_date_end());
        long seconds = between.getSeconds();
        //seconds -= 51587;
        if (seconds < 0) {
            seconds = 0;
        }

        return seconds;
    }

    //更新结束时间
    public void updateDateEnd(Integer leftSecond) {
        setAgoods_date_end(XDate.now().plusSeconds(leftSecond));
    }

    public XReturn validDate() {
        if (XDate.now().isBefore(getAgoods_date_begin())) {
            return XReturn.getR(20019, "活动未开始");
        }

        if (XDate.now().isAfter(getAgoods_date_end())) {
            return XReturn.getR(20022, "活动已结束");
        }

        return XReturn.getR(0, "进行中");
    }
}
