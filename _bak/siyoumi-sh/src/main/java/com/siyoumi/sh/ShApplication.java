package com.siyoumi.sh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication()
@ConfigurationPropertiesScan("com.siyoumi.sh.config")
public class ShApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ShApplication.class, args);

        context.close();
    }
}
