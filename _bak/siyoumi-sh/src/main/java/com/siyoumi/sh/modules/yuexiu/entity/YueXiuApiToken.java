package com.siyoumi.sh.modules.yuexiu.entity;

import com.siyoumi.util.XStr;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class YueXiuApiToken {
    String openid;
    String phone;
    String token;
    String tokenCheckFun;
    String cid;

    public static YueXiuApiToken getIns(String token, String phone, String openid, String cid) {
        return getIns(token, phone, openid, cid, null);
    }

    public static YueXiuApiToken getIns(String token, String phone, String openid, String cid, String tokenCheckFun) {
        YueXiuApiToken data = new YueXiuApiToken();
        data.setToken(token);
        data.setPhone(phone);
        data.setOpenid(openid);
        data.setCid(cid);
        data.setTokenCheckFun(tokenCheckFun);
        return data;
    }


    public List<YueXiuSignData> getSignData() {
        List<YueXiuSignData> list = new ArrayList<>();
        if (XStr.isNullOrEmpty(getCid())) {
            return list;
        }

        String[] cidArr = getCid().split(",");
        for (String c : cidArr) {
            String[] citemArr = c.split("#");

            list.add(YueXiuSignData.getIns(citemArr[0], citemArr[1]));
        }
        return list;
    }
}
