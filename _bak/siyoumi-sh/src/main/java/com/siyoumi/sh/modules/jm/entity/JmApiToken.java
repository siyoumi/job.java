package com.siyoumi.sh.modules.jm.entity;

import com.siyoumi.util.XStr;
import lombok.Data;

@Data
public class JmApiToken {
    String name;
    String wxFrom;
    String wxFromEnc;

    /**
     * wx_from=xxx&wx_from_enc=xxx
     */
    public String getWxFromQuery() {
        return XStr.concat("wx_from=", wxFrom, "&wx_from_enc=", wxFromEnc);
    }

    public static JmApiToken getIns(String name, String wxfrom, String wxEromEnc) {
        JmApiToken token = new JmApiToken();
        token.setName(name);
        token.setWxFrom(wxfrom);
        token.setWxFromEnc(wxEromEnc);
        return token;
    }
}
