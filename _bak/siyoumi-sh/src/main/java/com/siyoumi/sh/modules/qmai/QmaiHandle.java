package com.siyoumi.sh.modules.qmai;

import com.siyoumi.sh.modules.common.HandleBase;
import com.siyoumi.sh.modules.common.IHandle;
import com.siyoumi.sh.modules.qmai.entity.QmaiApiToken;
import com.siyoumi.util.*;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//蒙自源
@Slf4j
public class QmaiHandle
        extends HandleBase
        implements IHandle {
    @Override
    public void run() {
        List<QmaiApiToken> tokenArr = getArrToken();
        if (tokenArr.size() <= 0) {
            log.error("未配置QMAI_TOKEN");
            return;
        }
        XLog.info(this.getClass(), "BEGIN:" + XDate.toDateTimeString());


        StringBuilder sendMsg = new StringBuilder();
        logAndSet(sendMsg, "**蒙自源签到** \n\n");

        for (QmaiApiToken item : tokenArr) {
            XReturn r = signIn(item);
            logAndSet(sendMsg, XStr.concat(item.getName(), "-签到：", r.getErrMsg(), "\n\n"));

            r = getFun(item);
            if (r.ok()) {
                BigDecimal fun = r.getData("data");
                logAndSet(sendMsg, XStr.concat(item.getName(), "-积分：" + fun, "\n\n"));
            } else {
                logAndSet(sendMsg, XStr.concat(item.getName(), "-积分：", r.getErrMsg(), "\n\n"));
            }
        }
        //发信息
        sendMsg(sendMsg.toString());

        XLog.info(this.getClass(), "END:" + XDate.toDateTimeString());
    }

    /**
     * token格式
     * token|phone|name&token2|phone2|name2
     */
    protected List<QmaiApiToken> getArrToken() {
        String token = System.getenv("QMAI_TOKEN");
        if (XStr.isNullOrEmpty(token)) {
            return new ArrayList<>();
        }

        List<QmaiApiToken> tokenArr = new ArrayList<>();
        String[] jmTokenArr = token.split("&");
        for (String s : jmTokenArr) {
            String[] dataArr = s.split("\\|");
            tokenArr.add(QmaiApiToken.getIns(dataArr[0], dataArr[1], dataArr[2]));
        }

        return tokenArr;
    }

    //签到
    private XReturn signIn(QmaiApiToken data) {
        String url = "https://webapi.qmai.cn/web/catering/integral/sign/signIn";
        url = "https://webapi.qmai.cn/web/cmk-center/sign/takePartInSign";

        Map<String, Object> postData = new HashMap<>();
        postData.put("activityId", "1015954890407419904");
        postData.put("appid", "wx6cff4ec7eaabc6db");
        postData.put("mobilePhone", data.getPhone());
        postData.put("userName", data.getName());

        XHttpClient client = XHttpClient.getInstance();
        String returnStr = client.postJson(url, apiHeader(data.getToken()), postData);
        return paresR(returnStr);
    }

    //获取积分
    private XReturn getFun(QmaiApiToken data) {
        String url = "https://webapi.qmai.cn/web/catering/crm/total-points";

        Map<String, Object> postData = new HashMap<>();
        postData.put("appid", "wx6cff4ec7eaabc6db");

        XHttpClient client = XHttpClient.getInstance();
        String returnStr = client.postJson(url, apiHeader(data.getToken()), postData);
        return paresR(returnStr);
    }


    private XReturn paresR(String returnStr) {
        log.info("return_str: {}", returnStr);
        XReturn r = XReturn.parse(returnStr);
        String message = r.getData("message");
        Integer code = XStr.toInt(r.getData("code") + "");

        r.setErrMsg(message);
        r.setErrCode(code);

        return r;
    }


    private Map<String, String> apiHeader(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Qm-From-Type", "catering");
        headers.put("Qm-User-Token", token);
        headers.put("Qm-From", "wechat");
        headers.put("store-id", "211679");

        return headers;
    }
}
