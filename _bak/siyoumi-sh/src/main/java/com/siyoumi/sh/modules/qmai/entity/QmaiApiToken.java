package com.siyoumi.sh.modules.qmai.entity;

import lombok.Data;

@Data
public class QmaiApiToken {
    String name;
    String token;
    String phone;

    public static QmaiApiToken getIns(String token, String phone, String username) {
        QmaiApiToken data = new QmaiApiToken();
        data.setToken(token);
        data.setPhone(phone);
        data.setName(username);
        return data;
    }
}
