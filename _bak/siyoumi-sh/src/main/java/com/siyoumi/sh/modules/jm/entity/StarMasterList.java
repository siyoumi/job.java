package com.siyoumi.sh.modules.jm.entity;

import com.siyoumi.util.XStr;
import lombok.Data;

@Data
public class StarMasterList {
    String masterId;  //发起ID
    Integer helpCount; //可帮忙次数
    String str_00;  //红包数额
    String str_01;  //是否已中奖，不为空表示中奖

    public Boolean win() {
        return XStr.hasAnyText(getStr_01());
    }
}
