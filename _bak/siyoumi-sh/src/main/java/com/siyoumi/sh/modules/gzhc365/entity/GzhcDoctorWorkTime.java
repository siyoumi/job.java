package com.siyoumi.sh.modules.gzhc365.entity;

import lombok.Data;

//当天挂号时间段情况
@Data
public class GzhcDoctorWorkTime {
    String extFields; //下单接口用
    Integer leftSource; //剩余金额 好像只会1
    Integer registerFee; //支付金额(分)
    String scheduleId; //
    String visitBeginTime; //开始时间
    String visitEndTime; //结束时间
    Integer visitPeriod; //1
    String visitQueue; //猜是剩余库存
}

