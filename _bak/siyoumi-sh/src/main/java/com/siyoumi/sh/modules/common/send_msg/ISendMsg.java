package com.siyoumi.sh.modules.common.send_msg;

import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;

//统一发信息接口
public interface ISendMsg {
    static ISendMsg getIns() {
        ISendMsg app = null;

        String dingTalk = System.getenv("DINGTALK_TOKEN");
        if (XStr.hasAnyText(dingTalk)) {
            //使用钉钉
            return new DingTalkSendMsg();
        }

        return app;
    }

    /**
     * 对外发送方法
     *
     * @param msg 内容
     * @return 返回发送结果
     */
    default XReturn send(String msg) {
        return send(msg, XStr.maxLen(msg, 20));
    }

    XReturn send(String msg, String title);
}
