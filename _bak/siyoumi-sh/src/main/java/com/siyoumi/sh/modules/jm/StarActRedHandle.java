package com.siyoumi.sh.modules.jm;

import com.siyoumi.sh.component.ThreadContext;
import com.siyoumi.sh.modules.common.IHandle;
import com.siyoumi.sh.modules.jm.entity.JmApiData;
import com.siyoumi.sh.modules.jm.entity.JmApiToken;
import com.siyoumi.sh.modules.jm.entity.StarInfo;
import com.siyoumi.sh.modules.jm.entity.StarMasterList;
import com.siyoumi.util.XApp;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

//拆红包
@Slf4j
public class StarActRedHandle
        extends JmHandleBase
        implements IHandle {
    //red wan 活动ID 用户序号

    @Override
    @SneakyThrows
    public void run() {
        List<String> args = ThreadContext.getArgs();
        if (args.size() < 3) {
            log.error("args参数异常，参考：red wan 活动ID 用户序号，{}", args);
            return;
        }
        String server = args.get(1);

        JmApiData apiData = getApiDataByServer(server);
        if (apiData == null) {
            log.error("args server参数异常，{}", args);
            return;
        }
        setJmApiData(apiData);

        String actId = args.get(2);
        Integer userIndex = -1;
        if (args.size() > 3) {
            userIndex = XStr.toInt(args.get(3));
        }

        List<JmApiToken> apiTokenArr = apiData.getJmApiTokens();
        if (userIndex >= 0) {
            if (apiTokenArr.size() - 1 < userIndex) {
                log.error("args 用户序号参数异常，{}", args);
                return;
            }
            apiTokenArr = List.of(apiData.getJmApiTokens().get(userIndex));
        }

        StringBuilder sendMsg = new StringBuilder();
        //活动时间验证
        StarInfo starInfo = startInfo(actId, apiTokenArr.get(0));
        logAndSet(sendMsg, "**" + starInfo.getSact_name() + "** \n\n");
        logAndSet(sendMsg, "活动时间：" + XDate.toDateTimeString(starInfo.getSact_start_time())
                + "-" + XDate.toDateTimeString(starInfo.getSact_end_time())
                + " \n\n");
        XReturn rDate = starInfo.validDate();
        if (rDate.err()) {
            logAndSet(sendMsg, rDate.getErrMsg() + " \n\n");
            return;
        }

        //拆红包
        play(sendMsg, actId, apiTokenArr);

        sendMsg(sendMsg.toString());
    }

    /**
     * 开始玩游戏
     */
    public void play(StringBuilder sendMsg, String actId, List<JmApiToken> tokenArr) {

        for (JmApiToken apiToken : tokenArr) {
            logAndSet(sendMsg, "**" + apiToken.getName() + "** 开始领红包\n\n");
            int redCount = 0;
            while (true) {
                XReturn r = startLaunch(actId, apiToken);
                if (r.err()) {
                    break;
                }
                redCount++;
            }
            logAndSet(sendMsg, "当前领红包数量：" + redCount + "\n\n");

            sleepAndLog(1);
            logAndSet(sendMsg, "获取红包列表 \n\n", false);
            XReturn r = startMasterList(actId, apiToken.getWxFrom(), apiToken);
            if (r.err()) {
                logAndSet(sendMsg, "获取红包列表：" + r.getErrMsg() + "\n\n");
            } else {
                List<StarMasterList> masterList = startMasterListOfObj(r);
                logAndSet(sendMsg, "主人开始乱猜 \n\n", false);
                for (StarMasterList item : masterList) {
                    if (item.win()) continue; //猜中了
                    for (Integer i = 0; i < item.getHelpCount(); i++) {
                        int redNum = XApp.random(100, 999);
                        XReturn rHelp = startHelp(actId, item.getMasterId(), apiToken, redNum + "");
                        if (rHelp.err()) {
                        }
                        sleepAndLog(1);
                    }
                }
            }

            sleepAndLog(1);
        }

        log.debug("开始帮忙猜");
        for (JmApiToken apiTokenMaster : tokenArr) {
            for (JmApiToken apiTokenFriend : tokenArr) {
                if (apiTokenFriend.getWxFrom().equals(apiTokenMaster.getWxFrom())) {
                    logAndSet(sendMsg, "自己不帮自己猜", true);
                    continue;
                }

                sleepAndLog(2);
                List<StarMasterList> masterListByFriend = startMasterListOfObj(actId, apiTokenMaster.getWxFrom(), apiTokenFriend);
                for (StarMasterList item : masterListByFriend) {
                    if (item.win()) {
                        logAndSet(sendMsg, "已猜中", true);
                        continue;
                    }
                    if (item.getHelpCount() <= 0) {
                        logAndSet(sendMsg, "机会已用光", true);
                        continue;
                    }
                    if (XStr.toInt(item.getStr_00()) < 200) {
                        logAndSet(sendMsg, "红包小于200", true);
                        continue;
                    }

                    logAndSet(sendMsg, apiTokenFriend.getName() + " 帮 " + apiTokenMaster.getName() + " 猜[" + item.getStr_00() + "]\n\n");
                    XReturn r = startHelp(actId, item.getMasterId(), apiTokenFriend, item.getStr_00());
                    logAndSet(sendMsg, r.getErrMsg() + "\n\n");
                    if (r.err()) {
                        logAndSet(sendMsg, r.getErrMsg(), true);
                        break;
                        //if (r.getErrCode() == -70 || r.getErrCode() == -80) {
                        //}
                    }

                    item.setStr_01(XApp.getUUID()); //标记猜
                    sleepAndLog(1);
                }
            }
        }

        for (JmApiToken apiToken : tokenArr) {
            Integer fun = getFun(apiToken);

            logAndSet(sendMsg, "**" + apiToken.getName() + "** 积分：" + fun + "\n\n");
            List<StarMasterList> masterList = startMasterListOfObj(actId, apiToken.getWxFrom(), apiToken);
            for (StarMasterList item : masterList) {
                String msg = "-- 红包：" + item.getStr_00();
                if (item.win()) {
                    msg += "[猜中]";
                }
                logAndSet(sendMsg, msg + "\n\n");
            }
        }
    }

}
