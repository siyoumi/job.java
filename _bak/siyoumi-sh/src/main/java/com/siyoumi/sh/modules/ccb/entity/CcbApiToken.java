package com.siyoumi.sh.modules.ccb.entity;

import lombok.Data;

@Data
public class CcbApiToken {
    String name;
    String uuid;

    public static CcbApiToken getIns(String uuid, String name) {
        CcbApiToken data = new CcbApiToken();
        data.setName(name);
        data.setUuid(uuid);
        return data;
    }
}
