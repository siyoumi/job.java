package com.siyoumi.sh.modules.jm;

import com.alibaba.fastjson.JSONObject;
import com.siyoumi.sh.component.ThreadContext;
import com.siyoumi.sh.modules.common.IHandle;
import com.siyoumi.sh.modules.jm.entity.*;
import com.siyoumi.util.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

//游戏
@Slf4j
public class StarActGameHandle
        extends JmHandleBase
        implements IHandle {
    //game wan 活动ID 用户序号

    /**
     * 游戏停多少秒提交分数
     */
    protected Integer gameStopSeconds() {
        return 31;
    }

    @Override
    @SneakyThrows
    public void run() {
        List<String> args = ThreadContext.getArgs();
        if (args.size() < 3) {
            log.error("args参数异常，参考：game wan 活动ID 用户序号，{}", args);
            return;
        }
        String server = args.get(1);

        JmApiData apiData = getApiDataByServer(server);
        if (apiData == null) {
            log.error("args server参数异常，{}", args);
            return;
        }
        setJmApiData(apiData);

        String actId = args.get(2);
        Integer userIndex = -1;
        if (args.size() > 3) {
            userIndex = XStr.toInt(args.get(3));
        }

        List<JmApiToken> apiTokenArr = apiData.getJmApiTokens();
        if (userIndex >= 0) {
            if (apiTokenArr.size() - 1 < userIndex) {
                log.error("args 用户序号参数异常，{}", args);
                return;
            }
            apiTokenArr = List.of(apiData.getJmApiTokens().get(userIndex));
        }

        StringBuilder sendMsg = new StringBuilder();
        //活动时间验证
        StarInfo starInfo = startInfo(actId, apiTokenArr.get(0));
        logAndSet(sendMsg, "**" + starInfo.getSact_name() + "** \n\n");
        logAndSet(sendMsg, "活动时间：" + XDate.toDateTimeString(starInfo.getSact_start_time())
                + "-" + XDate.toDateTimeString(starInfo.getSact_end_time())
                + " \n\n");
        XReturn rDate = starInfo.validDate();
        if (rDate.err()) {
            logAndSet(sendMsg, rDate.getErrMsg() + " \n\n");
            return;
        }


        //玩游戏
        for (JmApiToken apiToken : apiTokenArr) {
            play(sendMsg, actId, apiToken);
        }

        //抽奖
        logAndSet(sendMsg, "**抽奖** \n\n");
        for (JmApiToken apiToken : apiTokenArr) {
            XReturn rChou = startChou(actId, apiToken);
            String prizeName = "";
            if (rChou.ok()) {
                JSONObject mapData = rChou.getData("data");
                JSONObject mapPrize = (JSONObject) mapData.get("prize");
                prizeName = (String) mapPrize.get("name");
                if (XStr.hasAnyText(prizeName)) {
                    prizeName = "," + prizeName;
                }
            }
            logAndSet(sendMsg, apiToken.getName() + "：" + rChou.getErrMsg() + prizeName + "\n\n");
        }

        sendMsg(sendMsg.toString());
    }

    /**
     * 开始玩游戏
     */
    public void play(StringBuilder sendMsg, String actId, JmApiToken apiToken) {
        logAndSet(sendMsg, "**" + apiToken.getName() + "** 开始玩游戏\n\n");

        XReturn r = startLaunch(actId, apiToken);
        logAndSet(sendMsg, "发起：" + r.getErrMsg() + "\n\n");
        if (r.err()) {
            return;
        }

        sleepAndLog(gameStopSeconds());
        Map data = (Map) r.get("data");
        String samasterId = (String) data.get("samaster_id");
        int score = XApp.random(25, 30) * 10;
        r = startLaunchUpdate(actId, samasterId, score, apiToken);
        logAndSet(sendMsg, "得分" + score + ", " + r.getErrMsg() + "\n\n");
    }

}
