package com.siyoumi.sh.modules.jm;

import com.alibaba.fastjson.JSONObject;
import com.siyoumi.sh.component.ThreadContext;
import com.siyoumi.sh.modules.common.IHandle;
import com.siyoumi.sh.modules.jm.entity.FunWinPrize;
import com.siyoumi.sh.modules.jm.entity.JmApiData;
import com.siyoumi.sh.modules.jm.entity.JmApiToken;
import com.siyoumi.sh.modules.jm.entity.StarInfo;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//星球活动-抽奖
@Slf4j
public class StarActChouHandle
        extends JmHandleBase
        implements IHandle {
    //chou wan 活动ID

    @Override
    @SneakyThrows
    public void run() {
        List<String> args = ThreadContext.getArgs();
        if (args.size() < 3) {
            log.error("args参数异常，参考：chou wan 活动ID，{}", args);
            return;
        }
        String server = args.get(1);
        JmApiData apiData = getApiDataByServer(server);
        if (apiData == null) {
            log.error("args server参数异常，{}", args);
            return;
        }
        setJmApiData(apiData);

        List<JmApiToken> apiTokenArr = getJmApiData().getJmApiTokens();
        if (apiTokenArr.size() <= 0) {
            log.error("未配置TOKEN");
            return;
        }
        String actId = args.get(2);

        StringBuilder sendMsg = new StringBuilder();
        //活动时间验证
        StarInfo starInfo = startInfo(actId, apiTokenArr.get(0));
        logAndSet(sendMsg, "**" + starInfo.getSact_name() + "** \n\n");
        logAndSet(sendMsg, "活动时间：" + XDate.toDateTimeString(starInfo.getSact_start_time())
                + "-" + XDate.toDateTimeString(starInfo.getSact_end_time())
                + " \n\n");
        XReturn rDate = starInfo.validDate();
        if (rDate.err()) {
            logAndSet(sendMsg, rDate.getErrMsg() + " \n\n");
            return;
        }

        //开始抽奖
        for (JmApiToken apiToken : apiTokenArr) {
            logAndSet(sendMsg, "**" + apiToken.getName() + "** 开始抽奖励\n\n");
            XReturn r = startLaunch(actId, apiToken);
            logAndSet(sendMsg, "发起：" + r.getErrMsg() + "\n\n");
            sleepAndLog(1);
            if (r.err()) {
                continue;
            }
            JSONObject data = r.getData("data");
            String masterId = data.getString("samaster_id");

            XReturn rChou = startChou(actId, apiToken, masterId);
            String prizeName = "";
            if (rChou.ok()) {
                JSONObject mapData = rChou.getData("data");
                JSONObject mapPrize = (JSONObject) mapData.get("prize");
                prizeName = (String) mapPrize.get("name");
                if (XStr.hasAnyText(prizeName)) {
                    prizeName = "," + prizeName;
                }
            }
            logAndSet(sendMsg, apiToken.getName() + "：" + rChou.getErrMsg() + prizeName + "\n\n");
//            break;
        }
    }

}
