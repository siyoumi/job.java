package com.siyoumi.sh.modules.ccb.entity;

import lombok.Data;

//用户信息
@Data
public class CcbUser {
    String zhcToken;
    String wxUUID;
    String redirectUrl;
}
