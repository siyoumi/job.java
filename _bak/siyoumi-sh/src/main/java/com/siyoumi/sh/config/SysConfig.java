package com.siyoumi.sh.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

@Data
@ConfigurationProperties(prefix = "siyoumi")
public class SysConfig {
    static private SysConfig ins = new SysConfig();
    private String env = "dev";


    public Boolean isDev() {
        return isServer("dev");
    }

    public Boolean isServer(String server) {
        return server.equals(getEnv().toLowerCase());
    }

    static public SysConfig getIns() {
        return ins;
    }

    @PostConstruct
    private void setStatic() {
        ins = this;
    }
}
