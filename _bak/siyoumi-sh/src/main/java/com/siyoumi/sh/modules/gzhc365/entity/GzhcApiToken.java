package com.siyoumi.sh.modules.gzhc365.entity;

import lombok.Data;

@Data
public class GzhcApiToken {
    String name;
    String token;
    String patientId = "";

    public static GzhcApiToken getIns(String token, String name) {
        GzhcApiToken data = new GzhcApiToken();
        data.setName(name);
        data.setToken(token);
        return data;
    }
}
