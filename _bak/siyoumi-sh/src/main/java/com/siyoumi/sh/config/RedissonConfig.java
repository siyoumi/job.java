package com.siyoumi.sh.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

//配置说明
//https://github.com/redisson/redisson/wiki/2.-%E9%85%8D%E7%BD%AE%E6%96%B9%E6%B3%95
@Data
@ConfigurationProperties(prefix = "redisson")
public class RedissonConfig {
    private String address; //redis://127.0.0.1:6379
    private String password;
    private Integer database = 0;
    private Integer timeout = 3000;
    private Integer masterConnectionPoolSize = 2;
}
