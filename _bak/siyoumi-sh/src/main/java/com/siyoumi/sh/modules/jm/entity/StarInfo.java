package com.siyoumi.sh.modules.jm.entity;

import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import lombok.Data;

import java.time.LocalDateTime;

//活动信息
@Data
public class StarInfo {
    String sact_name;  //活动名称
    LocalDateTime sact_start_time;  //开始时间
    LocalDateTime sact_end_time; //结束时间


    public XReturn validDate() {
        if (XDate.now().isBefore(getSact_start_time())) {
            return XReturn.getR(20019, "活动未开始");
        }

        if (XDate.now().isAfter(getSact_end_time())) {
            return XReturn.getR(20022, "活动已结束");
        }

        return XReturn.getR(0);
    }
}
