package com.siyoumi.sh.modules.gzhc365.entity;

import lombok.Data;

//医院
@Data
public class GzhcHis {
    String hisId = "2511"; //医院（广中医）
    String subHisId = "588"; //医院分部（广州机场路16号大院）
    String platformSource = "3";
    String subSource = "1";

    public String getPlatformId() {
        return getHisId();
    }
}
