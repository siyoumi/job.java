package com.siyoumi.sh.modules.ccb.entity;

import lombok.Data;

//兑换中心-奖品
@Data
public class CcbProduct {
    String activityName; //名称
    Integer remainingInventory; //剩余库存
    Integer price; //名称
}
