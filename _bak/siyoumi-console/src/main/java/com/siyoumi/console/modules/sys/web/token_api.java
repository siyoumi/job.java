package com.siyoumi.console.modules.sys.web;

import com.siyoumi.component.XApp;
import com.siyoumi.component.XRedis;
import com.siyoumi.component.api.jj.JJ;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.config.SysConfig;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//发送token
@RestController
@RequestMapping("/app/sys/token")
public class token_api
        extends ConsoleBase {
    @GetMapping("")
    public XReturn index() {
        XReturn r = XReturn.getR(0);

        XRedis.getBean().deleteByPattern("console:*");
        //XRedis.getBean().setEx();
        String token = XApp.getUUID();
        XRedis.getBean().setEx("console:" + token, SysConfig.getIns().getEnv(), 3600);

        String url = SysConfig.getIns().getAppRoot() + "app/console/api?token=" + token;
        StringBuilder sb = new StringBuilder();
        sb.append("**控制台授权**\n\n");
        sb.append(XStr.concat("[授权链接](", url, ")\n\n"));
        sb.append(token + "\n\n");

        JJ jj = JJ.getIns();
        jj.send(sb.toString(), "", false);

        return r;
    }
}
