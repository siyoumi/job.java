package com.siyoumi.console.modules.test;

import com.siyoumi.component.api.jj.JJ;
import com.siyoumi.component.api.jj.RobotType;
import com.siyoumi.controller.ApiController;
import com.siyoumi.util.XReturn;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test/task")
public class TestTask
        extends ApiController {
    @GetMapping("run")
    public XReturn run() {
        //JmTaskWatsons app = new JmTaskWatsons();
        //app.exec();
        //JmTaskRoco app = new JmTaskRoco();
        //app.exec();
        //JmTask app = new JmTask();
        //app.exec();

        String msg = "[task] 2022-10-19 16:16:33 \n" +
                " querying database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column 'gcg_level_id' in 'field list'\n" +
                "http://php.job:56010/zz_app_job/groupchat_fun/fun_add\n" +
                "-- 查看详情 --\n";
        JJ jj = JJ.getIns(RobotType.MSG);
        jj.send(msg, "", false);

        //if (XStr.hasAnyText(input("ex"))) {
        //    throw new XException("test");
        //}
        //
        //getR().setData("config", SysConfig.getIns());
        //getR().setData("x", XHttpContext.getX(false));

        return getR();
    }
}
