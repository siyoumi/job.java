package com.siyoumi.console.config;

import com.siyoumi.console.modules.sys.interceptor.ConsoleInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorAppConfig
        implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //过滤器
        registry.addInterceptor(new ConsoleInterceptor())
                .addPathPatterns("/app/console/**")
                .order(10);
        ;
    }
}
