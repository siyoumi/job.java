package com.siyoumi.console.config;

import com.siyoumi.config.SysConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotBlank;

@Data
@Validated
@ConfigurationProperties(prefix = "cmd")
public class CmdConfig {
    static private CmdConfig ins = new CmdConfig();

    @NotBlank(message = "请配置脚本，例：dir,ipconfig")
    private String script;
    private String longToken;

    static public CmdConfig getIns() {
        return ins;
    }

    @PostConstruct
    private void setStatic() {
        ins = this;
    }
}
