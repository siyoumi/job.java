package com.siyoumi.console.modules.sys.web;

import com.siyoumi.controller.ApiController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

@Slf4j
public class ConsoleBase
        extends ApiController {
    @ModelAttribute
    public void initController(ModelMap modelMap) {
        log.debug("initController");
    }
}
