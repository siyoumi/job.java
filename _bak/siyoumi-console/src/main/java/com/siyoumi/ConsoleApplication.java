package com.siyoumi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.ConfigurableApplicationContext;


@MapperScan({"com.siyoumi.console.mapper", "com.siyoumi.mapper"})
@SpringBootApplication
@ConfigurationPropertiesScan({"com.siyoumi.config","com.siyoumi.console.config"})
public class ConsoleApplication
{
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ConsoleApplication.class, args);
    }
}

