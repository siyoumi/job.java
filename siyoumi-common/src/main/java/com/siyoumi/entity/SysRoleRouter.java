package com.siyoumi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.entity.base.SysRoleRouterBase;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@TableName(value = "t_sys_role_router", schema = "wx_app")
public class SysRoleRouter
        extends SysRoleRouterBase {


}
