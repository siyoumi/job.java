package com.siyoumi.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.entity.base.SysLogBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_log", schema = "wx_app")
public class SysLog
        extends SysLogBase
{

}
