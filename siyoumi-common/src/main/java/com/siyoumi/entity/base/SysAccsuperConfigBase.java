package com.siyoumi.entity.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.siyoumi.entity.EntityBase;
import com.siyoumi.entity.SysAccsuperConfig;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysAccsuperConfigBase
        extends EntityBase<SysAccsuperConfig>
        implements Serializable {
    @Override
    public String prefix() {
        return "aconfig_";
    }

    static public String table() {
        return "wx_app.t_s_accsuper_config";
    }

    static public String tableKey() {
        return "aconfig_id";
    }


    @TableId(value = "aconfig_id", type = IdType.INPUT)
    private String aconfig_id;
    private String aconfig_x_id;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime aconfig_create_date;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime aconfig_update_date;
    private String aconfig_type;
    private String aconfig_type_app;
    private String aconfig_token;
    private String aconfig_gh_id;
    private String aconfig_app_id;
    private String aconfig_app_secret;
    private Integer aconfig_pay_enable;
    private String aconfig_pay_app_id;
    private String aconfig_pay_app_secret;
    private String aconfig_partnerId;
    private String aconfig_partnerKey;
    private String aconfig_partnerId_sub;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime aconfig_access_token_date;
    private String aconfig_access_token;
    private String aconfig_refresh_token;
    private String aconfig_oauth_root;
    private String aconfig_js_ticket;
    private String aconfig_js_ticket_wxcard;
    private String aconfig_fun_name;
    private String aconfig_x_id__wx;
    private String aconfig_test;
    private Integer aconfig_wxopen;
    private String aconfig_wxopen_parent;
    private String aconfig_key;

}
