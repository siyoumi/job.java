package com.siyoumi.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.entity.SysAccsuperZzzApp;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysAccsuperZzzAppBase
        extends EntityBase<SysAccsuperZzzApp>
        implements Serializable
{
    @Override
    public String prefix(){
        return "acczapp_";
    }

    static public String table() {
        return "wx_app.t_s_accsuper_zzz_app";
    }

    static public String tableKey() {
        return "acczapp_id";
    }


	@TableId(value = "acczapp_id", type = IdType.INPUT)
	private Long acczapp_id;
	private String acczapp_x_id;
	private LocalDateTime acczapp_create_date;
	private LocalDateTime acczapp_update_date;
	private String acczapp_app_id;

}
