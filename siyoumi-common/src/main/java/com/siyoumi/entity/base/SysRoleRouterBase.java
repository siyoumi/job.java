package com.siyoumi.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.entity.SysRoleRouter;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysRoleRouterBase
        extends EntityBase<SysRoleRouter>
        implements Serializable
{
    @Override
    public String prefix(){
        return "roro_";
    }

    static public String table() {
        return "wx_app.t_sys_role_router";
    }

    static public String tableKey() {
        return "roro_id";
    }


	@TableId(value = "roro_id", type = IdType.INPUT)
	private String roro_id;
	private String roro_x_id;
	private LocalDateTime roro_create_date;
	private LocalDateTime roro_update_date;
	private String roro_role_id;
	private String roro_appr_path;

}
