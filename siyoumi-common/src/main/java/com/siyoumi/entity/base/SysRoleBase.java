package com.siyoumi.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.entity.SysRole;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysRoleBase
        extends EntityBase<SysRole>
        implements Serializable
{
    @Override
    public String prefix(){
        return "role_";
    }

    static public String table() {
        return "wx_app.t_sys_role";
    }

    static public String tableKey() {
        return "role_id";
    }


	@TableId(value = "role_id", type = IdType.INPUT)
	private String role_id;
	private String role_x_id;
	private LocalDateTime role_create_date;
	private LocalDateTime role_update_date;
	private String role_name;
	private Integer role_order;
	private Integer role_level;

}
