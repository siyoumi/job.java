package com.siyoumi.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.entity.SysAccount;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysAccountBase
        extends EntityBase<SysAccount>
        implements Serializable {
    @Override
    public String prefix() {
        return "acc_";
    }

    static public String table() {
        return "wx_app.t_s_account";
    }

    static public String tableKey() {
        return "acc_id";
    }


    @TableId(value = "acc_id", type = IdType.INPUT)
    private String acc_id;
    private String acc_x_id;
    private LocalDateTime acc_create_date;
    private LocalDateTime acc_update_date;
    private String acc_role;
    private String acc_uid;
    private String acc_pwd;
    private String acc_name;
    private Integer acc_lock;
    private String acc_openid;
    private String acc_headimg;
    private LocalDateTime acc_lock_end_date;
    private String acc_department_id;
    private String acc_phone;
    private String acc_store_app_id;
    private String acc_store_id;
    private Long acc_del;

}
