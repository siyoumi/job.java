package com.siyoumi.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.entity.SysLog;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysLogBase
        extends EntityBase<SysLog>
        implements Serializable
{
    @Override
    public String prefix(){
        return "log_";
    }

    static public String table() {
        return "wx_app.t_s_log";
    }

    static public String tableKey() {
        return "log_id";
    }


	@TableId(value = "log_id", type = IdType.INPUT)
	private String log_id;
	private String log_x_id;
	private LocalDateTime log_create_date;
	private String log_type;
	private String log_type_sub;
	private String log_str_00;
	private String log_str_01;
	private String log_txt_00;
	private Integer log_int_00;
	private Integer log_int_01;

}
