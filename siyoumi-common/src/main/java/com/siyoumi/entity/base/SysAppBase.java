package com.siyoumi.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.entity.SysApp;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysAppBase
        extends EntityBase<SysApp>
        implements Serializable
{
    @Override
    public String prefix(){
        return "app_";
    }

    static public String table() {
        return "wx_app.t_s_app";
    }

    static public String tableKey() {
        return "app_id";
    }


	@TableId(value = "app_id", type = IdType.INPUT)
	private String app_id;
	private LocalDateTime app_create_date;
	private LocalDateTime app_update_date;
	private Integer app_enable;
	private String app_type;
	private String app_icon;
	private String app_name;
	private String app_desc;
	private Integer app_is_wx;
	private Integer app_is_wxapp;
	private Integer app_order;

}
