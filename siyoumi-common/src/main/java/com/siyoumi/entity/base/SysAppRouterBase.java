package com.siyoumi.entity.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.siyoumi.entity.EntityBase;
import com.siyoumi.entity.SysAppRouter;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysAppRouterBase
        extends EntityBase<SysAppRouter>
        implements Serializable {
    @Override
    public String prefix() {
        return "appr_";
    }

    static public String table() {
        return "wx_app.t_s_app_router";
    }

    static public String tableKey() {
        return "appr_id";
    }


    @TableId(value = "appr_id", type = IdType.INPUT)
    private String appr_id;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime appr_create_date;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime appr_update_date;
    private String appr_app_id;
    private String appr_pid;
    private String appr_meta_title;
    private String appr_meta_title_client;
    private String appr_meta_icon;
    private String appr_path;
    private String appr_file_path;
    private Integer appr_hidden;
    private Integer appr_order;
    private String appr_path_query;

}
