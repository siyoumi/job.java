package com.siyoumi.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.entity.base.SysAccsuperConfigBase;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_accsuper_config", schema = "wx_app")
public class SysAccsuperConfig
        extends SysAccsuperConfigBase {
    /**
     * 主站
     *
     * @param x
     */
    static public Boolean master(String x) {
        return "x".equals(x);
    }

    /**
     * 小程序帐号
     */
    public Boolean wxApp() {
        return "wxapp".equals(getAconfig_type());
    }

    public Boolean wx() {
        return "wx".equals(getAconfig_type());
    }

    /**
     * 开放平台授权
     */
    public Boolean wxOpen() {
        return getAconfig_wxopen() == 1;
    }

    /**
     * 授权子站点
     */
    public Boolean wxOpenClient() {
        return getAconfig_wxopen() == 1 && XStr.hasAnyText(getAconfig_wxopen_parent());
    }

    public Boolean corp() {
        return "corp".equals(getAconfig_type());
    }

    /**
     * token截前24位
     */
    public String aesKey() {
        String token = getAconfig_token();
        if (token.length() < 24) {
            XValidator.err(EnumSys.ERR_VAL.getR("token小于24长度"));
        }
        return token.substring(0, 24);
    }

    /**
     * token截后16位
     */
    public String aesIv() {
        String token = getAconfig_token();
        return token.substring(token.length() - 16);
    }
}
