package com.siyoumi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.entity.base.SysAccsuperZzzAppBase;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_accsuper_zzz_app", schema = "wx_app")
public class SysAccsuperZzzApp
        extends SysAccsuperZzzAppBase
{
}
