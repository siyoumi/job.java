package com.siyoumi.interceptor;

import com.siyoumi.config.SysConfig;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.component.XJwt;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.component.http.XHttpContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//前端
@Slf4j
public class WebInterceptor
        extends InterceptorBase
        implements HandlerInterceptor {
    /**
     * 执行前
     *
     * @return boolean
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.debug("BEGIN");

        XReturn r = authToken();
        if (r.err()) {
            return returnErr(response, r);
        }
        log.debug(XJson.toJSONString(r));

        r.setData("type", "web");
        String x = r.getData("x");
        XHttpContext.setX(x); //设置x
        XHttpContext.set(XHttpContext.keyTokenData, r);

        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.debug("END");
    }


    /**
     * 验证token
     *
     * @return 验证结果
     */
    protected XReturn authToken() {
        log.debug("检查token");
        String token = XHttpContext.getToken();
        if (XStr.isNullOrEmpty(token)) {
            return EnumSys.TOKEN_MISS.getR();
        }

        XJwt jwt = XJwt.getBean();
        XReturn r = jwt.parseToken(token);
        if (SysConfig.getIns().isDev()) {
            //if (r.getErrCode().equals(SysErr.TOKEN_EXPIRED.getErrcode())) {
            //    XLog.debug(this.getClass(), "DEV环境，不检查时间");
            //    r = XReturn.getR(0);
            //}
        }

        return r;
    }
}
