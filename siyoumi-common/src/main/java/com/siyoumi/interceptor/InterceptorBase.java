package com.siyoumi.interceptor;

import com.siyoumi.util.XJson;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

@Slf4j
public class InterceptorBase {
    public boolean returnErr(HttpServletResponse response, XReturn r) {
        log.error(XJson.toJSONString(r));
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.addHeader("Content-Type", "application/json");
        //
        //response.addHeader("Access-Control-Allow-Origin", "*");
        //response.addHeader("Access-Control-Allow-Credentials", "true");
        //response.addHeader("Access-Control-Allow-Headers", "DNT,Keep-Alive,User-Agent,Cache-Control,Content-Type,token");
        //
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        writer.write(XJson.toJSONString(r));

        return false;
    }
}
