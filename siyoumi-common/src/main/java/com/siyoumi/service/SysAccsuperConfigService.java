package com.siyoumi.service;

import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mapper.SysAccsuperConfigMapper;
import com.siyoumi.component.XRedis;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XAes;
import com.siyoumi.util.XReturn;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

//t_s_accsuper_config
@Service
public class SysAccsuperConfigService
        extends ServiceImplBase<SysAccsuperConfigMapper, SysAccsuperConfig> {
    static public SysAccsuperConfigService getBean() {
        return XSpringContext.getBean(SysAccsuperConfigService.class);
    }

    public String getRedisKeyAccessToken() {
        return getEntityCacheKey("access_token");
    }

    public String getRedisKeyJsTicket() {
        return getEntityCacheKey("js_ticket");
    }

    /**
     * 中心站点
     *
     * @return
     */
    static public Boolean isCenter() {
        return "x".equals(XHttpContext.getX());
    }

    @Override
    public String getEntityCacheKey(String keyFix) {
        String className = getEntityName();
        return XStr.concat("cache_model:", className, "|", keyFix);
    }

    public List<Map<String, Object>> getList(String type) {
        JoinWrapperPlus<SysAccsuperConfig> query = join();
        query.eq("aconfig_type", type)
                .select("aconfig_id", "aconfig_type");

        return mapper().selectMaps(query);
    }

    /**
     * 重置超管密码
     */
    public XReturn resetPwd(String id, Boolean dev) {
        SysAccsuperConfig entityConfig = loadEntity(id);
        if (entityConfig == null) {
            return EnumSys.ERR_VAL.getR("id参数异常");
        }

        SysAccountService appAcc = SysAccountService.getBean();
        SysAccount entityAcc;
        if (!dev) {
            //超管
            entityAcc = appAcc.getAccByUserName(entityConfig.getKey());
        } else {
            //开发者
            entityAcc = appAcc.getAccByUserName(XStr.concat(entityConfig.getKey(), "@dev"));
            if (entityAcc == null) {
                return EnumSys.ERR_VAL.getR("未设置开发者");
            }
        }

        //重置密码格式，A当天日期，例：A20201119
        String newPwd = SysAccountService.getDefPwd();
        return SysAccountService.getBean().resetPwd(entityAcc.getKey(), newPwd);
    }

    /**
     * 积分名称
     */
    public String getFunName(SysAccsuperConfig entityConfig) {
        String funName = "积分";
        if (XStr.hasAnyText(entityConfig.getAconfig_fun_name())) {
            funName = entityConfig.getAconfig_fun_name();
        }

        return funName;
    }


    public SysAccsuperConfig getXConfig(String x, Boolean getCache) {
        String key = getEntityCacheKey(x);
        return XRedis.getBean().getAndSetData(key, k -> {
            return getEntity(x);
        }, SysAccsuperConfig.class, getCache);
    }

    /**
     * 获取关联公众号，配置
     *
     * @param x
     */
    public SysAccsuperConfig getXConfigWx(String x) {
        SysAccsuperConfig entityConfig = getXConfig(x, true);
        if (!entityConfig.wxApp()) {
            return entityConfig;
        }

        SysAccsuperConfig entityConfigWx = null;
        if (XStr.hasAnyText(entityConfig.getAconfig_x_id__wx())) {
            entityConfigWx = getXConfig(entityConfig.getAconfig_x_id__wx(), true);
        }

        return entityConfigWx;
    }

    /**
     * 开放平台主站点
     *
     * @param x
     */
    public SysAccsuperConfig getXConfigWxOpenParent(String x) {
        SysAccsuperConfig entityConfig = getXConfig(x, true);

        SysAccsuperConfig entityConfigParent = null;
        if (entityConfig.getAconfig_wxopen() == 1) {
            if (XStr.hasAnyText(entityConfig.getAconfig_wxopen_parent())) {
                entityConfigParent = getXConfig(entityConfig.getAconfig_wxopen_parent(), true);
            } else {
                entityConfigParent = entityConfig;
            }
        }

        return entityConfigParent;
    }


    /**
     * 获取关联公众号，配置
     */
    public SysAccsuperConfig getXConfigByAppId(String appid) {
        String key = getEntityCacheKey(appid);
        return XRedis.getBean().getAndSetData(key, k -> {
            JoinWrapperPlus<SysAccsuperConfig> query = join();
            query.eq("aconfig_app_id", appid);
            return first(query);
        }, SysAccsuperConfig.class);
    }
}
