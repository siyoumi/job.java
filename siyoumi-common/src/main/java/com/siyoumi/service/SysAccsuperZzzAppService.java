package com.siyoumi.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.entity.SysAccsuperZzzApp;
import com.siyoumi.entity.SysApp;
import com.siyoumi.mapper.SysAccsuperZzzAppMapper;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SysAccsuperZzzAppService
        extends ServiceImplBase<SysAccsuperZzzAppMapper, SysAccsuperZzzApp> {
    static public SysAccsuperZzzAppService getBean() {
        return XSpringContext.getBean(SysAccsuperZzzAppService.class);
    }

    /**
     * x可设置的应用列表
     */
    public List<SysApp> listApps(SysAccsuperConfig entityXConig) {
        SysAppService app = SysAppService.getBean();

        QueryWrapper<SysApp> query = app.q().eq("app_type", "")
                .select("app_id", "app_name")
                .eq("app_enable", 1);
        switch (entityXConig.getAconfig_type()) {
            case "wx": //公众号
                query.eq("app_is_wx", 1);
                break;
            case "wxapp": //小程序
                query.eq("app_is_wxapp", 1);
                break;
        }
        //过滤已添加的应用
        query.notExists(XStr.concat("SELECT 1 FROM wx_app.t_s_accsuper_zzz_app "
                , "WHERE acczapp_app_id = app_id AND acczapp_x_id = {0}"), entityXConig.getKey());

        return app.get(query);
    }
}
