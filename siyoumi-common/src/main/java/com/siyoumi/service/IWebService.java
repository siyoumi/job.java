package com.siyoumi.service;

import com.siyoumi.exception.EnumSys;
import com.siyoumi.exception.XException;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.validator.XValidator;

public interface IWebService {
    default XReturn getTokenData() {
        return XHttpContext.getTokenData();
    }

    /**
     * 用户ID
     */
    default String getOpenid() {
        XReturn tokenData = getTokenData();
        String openid = tokenData.getData("openid");
        if (XStr.isNullOrEmpty(openid)) {
            XValidator.err(EnumSys.ERR_OPENID.getR());
        }

        return openid;
    }

    default String getUid() {
        return getUid(true);
    }

    default String getUid(Boolean throwEx) {
        XReturn tokenData = getTokenData();
        String uid = tokenData.getData("uid");
        if (XStr.isNullOrEmpty(uid) && throwEx) {
            XValidator.err(EnumSys.ERR_UID.getR());
        }
        return uid;
    }

    default Boolean isLogin() {
        return XStr.hasAnyText(getUid(false));
    }
}
