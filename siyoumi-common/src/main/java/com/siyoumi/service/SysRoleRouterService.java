package com.siyoumi.service;

import com.siyoumi.entity.SysRoleRouter;
import com.siyoumi.mapper.SysRoleRouterMapper;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;

@Service
public class SysRoleRouterService
        extends ServiceImplBase<SysRoleRouterMapper, SysRoleRouter>
{
    static public SysRoleRouterService getBean()
    {
        return XSpringContext.getBean(SysRoleRouterService.class);
    }
}
