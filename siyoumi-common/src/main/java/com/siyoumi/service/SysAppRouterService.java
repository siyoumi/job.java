package com.siyoumi.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.entity.SysAppRouter;
import com.siyoumi.mapper.SysAppRouterMapper;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XStr;
import com.siyoumi.component.http.InputData;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysAppRouterService
        extends ServiceImplBase<SysAppRouterMapper, SysAppRouter> {
    static public SysAppRouterService getBean() {
        return XSpringContext.getBean(SysAppRouterService.class);
    }


    public QueryWrapper<SysAppRouter> getListQuery(InputData inputData) {
        String compKw = inputData.input("compKw");
        String appIdx = inputData.input("app_idx");
        String pid = inputData.input("pid");

        QueryWrapper<SysAppRouter> query = q();
        query.orderByAsc("appr_order");
        if (XStr.hasAnyText(compKw)) { //名称
            query.like("appr_meta_title", compKw);
        }
        if (XStr.hasAnyText(pid)) {  //1级
            if (pid.equals("all_1")) {
                //全部
                query.eq("appr_pid", "");
            } else {
                query.eq("appr_pid", pid);
            }
        }
        if (XStr.hasAnyText(appIdx)) { //应用ID
            query.eq("appr_app_id", appIdx);
        }

        return query;
    }

    public List<SysAppRouter> getList(List<String> appIds, List<String> pids) {
        QueryWrapper<SysAppRouter> query = q();
        query.orderByAsc("appr_order")
                .orderByAsc("appr_app_id")
                .orderByAsc("appr_id");

        if (appIds != null) {
            //应用ID
            query.in("appr_app_id", appIds);
        }
        if (pids != null) {
            //pid or id
            query.and(q ->
            {
                q.in("appr_pid", pids)
                        .or()
                        .in("appr_id", pids);
            });
        }
        return mapper().selectList(query);
    }


    /**
     * 获取页面标题
     */
    public String getMetaTitle(SysAppRouter entity) {
        if (XStr.hasAnyText(entity.getAppr_meta_title_client())) {
            return entity.getAppr_meta_title_client();
        }

        return entity.getAppr_meta_title();
    }
}
