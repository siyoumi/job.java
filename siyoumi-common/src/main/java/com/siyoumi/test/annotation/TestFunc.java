package com.siyoumi.test.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//标记测试方法
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TestFunc {
    String value() default "";

    //是否需要开启事务
    boolean addTransaction() default false;
}
