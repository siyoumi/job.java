package com.siyoumi.test.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//标记测试类
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface TestClass {
    //执行方法
    String[] runMethods() default {};
}
