package com.siyoumi.test;

import com.alibaba.fastjson.JSON;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.entity.EntityBase;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.util.XFile;
import com.siyoumi.util.XSqlStr;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//应用测试
@Slf4j
public class TestAppController
        extends TestController {
    /**
     * 测试站点
     */
    public String getTestX() {
        XValidator.err(EnumSys.TEST_ERR.getR("未配置测试站点"));
        return null;
    }

    /**
     * 需要删除的表
     */
    public List<ServiceImplBase> delServices() {
        return new ArrayList<>();
    }

    /**
     * 只删除test前缀测试数据
     */
    public Boolean delOnlyTestFix() {
        return false;
    }

    public String testFix() {
        return "test_";
    }

    protected void init10HandleDelData() {
        List<ServiceImplBase> listSvc = delServices();
        for (ServiceImplBase svc : listSvc) {
            JoinWrapperPlus query = svc.join();
            if (delOnlyTestFix()) {
                query.likeRight(svc.getEntityKeyName("id"), testFix()); //只删除test_开关
            }
            query.eq(svc.getEntityKeyName("x_id"), getTestX());
            svc.remove(query);
        }
    }

    /**
     * 测试目录
     */
    public String getTestDataDir() {
        String root = System.getProperty("user.dir");
        String name = this.getClass().getName();
        //com/siyoumi/app/test/app_book/test_json
        String name_x = name.replaceAll("\\.", "/");
        String dir = XStr.concat(root, "\\siyoumi-app\\src\\main\\java\\", name_x, "\\..\\test_json");
        return dir;
    }

    private Map<String, Object> getTestEntityMap() {
        return XHttpContext.get("test_entity");
    }


    @SneakyThrows
    protected void init20HandleCreateData() {
        Map<String, Object> testEntity = new HashMap<>();

        String dirData = getTestDataDir();
        XValidator.isNullOrEmpty(dirData, "未配置测试数据目录");

        Stream<Path> PathStream = Files.list(Paths.get(dirData));
        List<Path> listPath = PathStream.collect(Collectors.toList());
        for (Path path : listPath) {
            String filePath = path.toString();
            if (!StringUtils.endsWith(filePath, ".json")) {
                log.info("非json文件");
                continue;
            }
            String txt = XFile.getContent(filePath);
            if (XStr.isNullOrEmpty(txt)) {
                log.info("内容为空");
                continue;
            }
            //log.info(txt);

            File file = new File(filePath);
            String fileName = file.getName();
            String tdName = fileName.replace(".json", "");
            String className = XSqlStr.tableNameToClassName(tdName);

            String entityName = "com.siyoumi.app.entity." + className;
            log.info("{}数据导入开始", entityName);
            Class<?> entityClass = Class.forName(entityName);
            //service
            Class<?> svcClass = Class.forName("com.siyoumi.app.service." + className + "Service");
            ServiceImplBase svc = (ServiceImplBase) XSpringContext.getBean(svcClass);
            List<?> listVal = JSON.parseArray(txt, entityClass);
            for (Object data : listVal) {
                EntityBase item = (EntityBase) data;
                assertTrue(StringUtils.startsWith(item.getKey(), testFix()), XStr.concat(entityName, "主键异常缺少前缀:", testFix()));
                initHandleCreateDataSaveEntityBegin(item);
                svc.save(item);
                log.info("{}:导入成功", item.getKey());

                testEntity.put(item.getKey(), item);
            }
        }

        XHttpContext.set("test_entity", testEntity);
    }

    /**
     * 文件转实体进行加工，比例：产品表，预约入口开放日期，需要特殊处理，不能写死
     *
     * @param entity
     */
    protected void initHandleCreateDataSaveEntityBegin(EntityBase entity) {

    }

    protected void init30HandleCustom() {
    }

    @Override
    protected void handleOneBefore(String method) {
        log.debug("删除测试数据");
        init10HandleDelData();
        log.debug("删除测试数据-完成");

        log.debug("测试数据初始化");
        init20HandleCreateData();
        log.debug("测试数据初始化-完成");

        log.debug("测试数据初始化-自定义");
        init30HandleCustom();
        log.debug("测试数据初始化-自定义-完成");
    }
}
