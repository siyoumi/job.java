package com.siyoumi.test;

import com.siyoumi.component.XApp;
import com.siyoumi.controller.ApiController;
import com.siyoumi.exception.BaseExceptionHandler;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.exception.XException;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.GetMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

//测试
@Slf4j
public class TestController
        extends ApiController {
    @GetMapping()
    final public Object index() {
        LinkedMap<Method, Annotation> mapMethods = new LinkedMap<>();

        TestClass anTestClass = null;
        for (Annotation annotation : this.getClass().getAnnotations()) {
            if (annotation instanceof TestClass) {
                anTestClass = (TestClass) annotation;
            }
        }
        if (anTestClass == null) {
            XValidator.err(EnumSys.TEST_ERR.getR("测试类缺少@TestClass注解"));
        }

        String[] testMethods = anTestClass.runMethods();
        if (testMethods.length > 0) {
            log.debug("执行测试类，指定测试方法");
            for (String methodName : testMethods) {
                Method method = null;
                try {
                    method = this.getClass().getMethod(methodName, null);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();

                    XValidator.err(EnumSys.TEST_ERR.getR(methodName + "方法没找到"));
                }

                TestFunc annotation = method.getAnnotation(TestFunc.class);
                if (annotation != null) {
                    mapMethods.put(method, annotation);
                }
            }
        } else {
            log.debug("执行测试类，所有测试方法");
            for (Method method : this.getClass().getDeclaredMethods()) {
                TestFunc annotation = method.getAnnotation(TestFunc.class);
                if (annotation != null) {
                    mapMethods.put(method, annotation);
                }
            }
        }

        if (mapMethods.size() <= 0) {
            XValidator.err(EnumSys.SYS.getR("未检查到测试方法"));
        }

        LinkedMap<String, XReturn> mapR = new LinkedMap<>(); //统一输出
        for (Map.Entry<Method, Annotation> entry : mapMethods.entrySet()) {
            Method method = entry.getKey();
            TestFunc anTestFunc = (TestFunc) entry.getValue();
            String methodName = method.getName();
            if (XStr.hasAnyText(anTestFunc.value())) {
                methodName = anTestFunc.value();
            }

            log.debug("{}---BEGIN", methodName);
            LocalDateTime beginDate = XDate.now();
            //执行测试
            XReturn r = handleOne(method, anTestFunc);
            //
            long execTime = XDate.between(beginDate, XDate.now()).toMillis();
            r.setData("exec_time", execTime + "ms");
            mapR.put(methodName, r);

            log.debug("{}---exec_time:{}ms", methodName, execTime);
            log.debug("{}---END", methodName);
        }


        XReturn r = XReturn.getR(0);
        r.setData("test_log", mapR);
        return r;

        //StringBuilder sb = new StringBuilder();
        //mapR.forEach((methodName, r) -> {
        //    sb.append("<p style='" + (r.err() ? "color:red" : "") + "'>\n");
        //    sb.append(methodName);
        //    sb.append("<br/>\n");
        //    sb.append("<textarea style='width:90%'>");
        //    sb.append(JSON.toJSONString(r));
        //    sb.append("</textarea>");
        //    sb.append("</p>");
        //    sb.append("<br/>\n");
        //});
        //
        //
        //return sb.toString();
    }

    /**
     * 方法执行前
     */
    protected void handleOneBefore(String method) {

    }

    /**
     * 方法执行后
     */
    protected void handleOneAfter(String method, XReturn r) {

    }

    /**
     * 单个方法执行逻辑
     *
     * @param method
     * @param anTestFunc
     */
    private XReturn handleOne(Method method, TestFunc anTestFunc) {
        String methodName = method.getName();
        if (XStr.hasAnyText(anTestFunc.value())) {
            methodName = anTestFunc.value();
        }

        TransactionStatus transactionStatus = null;
        XReturn r;
        try {
            if (anTestFunc.addTransaction()) {
                log.debug("{}---开启事务", methodName);
                transactionStatus = XApp.getTransaction().getTransactionManager().getTransaction(new DefaultTransactionDefinition());
            }

            handleOneBefore(methodName); //执行前

            r = (XReturn) method.invoke(this);

            handleOneAfter(methodName, r); //执行后
            log.debug("{}", XStr.toJsonStr(r));

            if (transactionStatus != null) {
                log.debug("{}---事务提交", methodName);
                XApp.getTransaction().getTransactionManager().commit(transactionStatus);
            }
        } catch (Exception ex) {
            r = EnumSys.TEST_ERR.getR();
            r.setErrMsg(ex.getMessage());
            if (ex instanceof InvocationTargetException) {
                Throwable targetException = ((InvocationTargetException) ex).getTargetException();
                r.setErrMsg(targetException.getMessage());

                targetException.printStackTrace();
                if (targetException instanceof XException) {
                    r = ((XException) targetException).getR();
                }
                r.setData("err_info", BaseExceptionHandler.listErrInfo(targetException));
            } else {
                ex.printStackTrace();
                r.setData("err_info", BaseExceptionHandler.listErrInfo(ex));
            }


            log.error("{}", XStr.toJsonStr(r));

            if (transactionStatus != null) {
                log.debug("{}---事务回滚", methodName);
                XApp.getTransaction().getTransactionManager().rollback(transactionStatus);
            }
        } finally {
            log.debug("完成");
        }
        r.remove("api_date");

        return r;
    }


    public void assertTrue(Boolean expected, String msg) {
        if (!expected) {
            XValidator.err(EnumSys.TEST_ERR.getR(msg));
        }
    }

    public <T> void assertEqualsVal(T expected, T actual, String msg) {
        log.error("expected:{},actual:{}", expected, actual);
        XReturn r = EnumSys.TEST_ERR.getR(msg);
        r.setData("expected", expected);
        r.setData("actual", actual);

        String expectedType = expected.getClass().getSimpleName();
        String actualType = actual.getClass().getSimpleName();
        log.info(expectedType);
        log.info(actualType);

        if (!expectedType.equals(actualType)) {
            r.setErrMsg("参数类型异常：", expectedType, ",", actualType);
            XValidator.err(r);
        }

        boolean pass;
        if (expected instanceof BigDecimal) {
            pass = ((BigDecimal) expected).compareTo((BigDecimal) actual) == 0;
        } else {
            pass = expected.equals(actual);
        }

        if (!pass) {
            XValidator.err(r);
        }
    }
}
