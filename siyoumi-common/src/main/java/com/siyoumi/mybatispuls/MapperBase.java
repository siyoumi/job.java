package com.siyoumi.mybatispuls;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface MapperBase<T>
        extends BaseMapper<T> {
    List<T> get(@Param("ew") Wrapper<T> queryWrapper);

    IPage<T> get(IPage<T> page, @Param("ew") Wrapper<T> queryWrapper);

    Map<String, Object> firstMap(@Param("ew") Wrapper<T> queryWrapper);

    IPage<Map<String, Object>> getMaps(IPage<T> page, @Param("ew") Wrapper<T> queryWrapper);

    List<Map<String, Object>> getMaps(@Param("ew") Wrapper<T> queryWrapper);
}
