package com.siyoumi.mybatispuls;

import com.siyoumi.util.entity.ThreadLocalData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

@Slf4j
public class XDataSource
        extends AbstractRoutingDataSource {
    enum type {
        DS_DEFAULT, DS_TEST
    }

    static void setDsKey(XDataSource.type type) {
        ThreadLocalData.set("ds", type.name());
    }

    static String getDsKey() {
        return ThreadLocalData.get("ds", type.DS_DEFAULT.name());
    }

    @Override
    protected Object determineCurrentLookupKey() {
        String key = getDsKey();
        log.debug("XDataSource-key: {}", key);
        return key;
    }
}
