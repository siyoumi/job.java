package com.siyoumi.mybatispuls;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class XDataSourceConfig {
    //@Bean(name = "dataSourceDefault")
    //@ConfigurationProperties("spring.datasource")
    //public DataSource dataSourceDefault() {
    //    log.info("dataSourceDefault");
    //    return DataSourceBuilder.create().build();
    //}
    //
    //@Bean(name = "dataSourceTest")
    //@ConfigurationProperties("spring.datasource.test")
    //public DataSource dataSourceTest() {
    //    log.info("dataSourceTest");
    //    return DataSourceBuilder.create().build();
    //}


    //@Bean(name = "dataSource")
    //public DataSource dataSource(@Qualifier("dataSourceDefault") DataSource dsDef,
    //                             @Qualifier("dataSourceTest") DataSource dsTest) {
    //    XDataSource ds = new XDataSource();
    //    // 设置默认数据源
    //    ds.setDefaultTargetDataSource(dsDef);
    //    // 添加数据源映射
    //    Map<Object, Object> targetDataSources = new HashMap<>();
    //    targetDataSources.put(XDataSource.type.DS_DEFAULT.name(), dsDef);
    //    targetDataSources.put(XDataSource.type.DS_TEST.name(), dsTest);
    //    ds.setTargetDataSources(targetDataSources);
    //    return ds;
    //}
}
