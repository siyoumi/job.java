package com.siyoumi.mybatispuls;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.logging.Log;

//打印去掉，拦截器打印完整sql
@Slf4j
public class SqlLogImpl
        implements Log {
    public SqlLogImpl(String clazz) {
    }

    @Override
    public boolean isDebugEnabled() {
        return true;
    }

    @Override
    public boolean isTraceEnabled() {
        return true;
    }

    @Override
    public void error(String s, Throwable throwable) {
        System.err.println(s);
        throwable.printStackTrace(System.err);
    }

    @Override
    public void error(String s) {
        log.error(s);
    }

    @Override
    public void debug(String s) {
        //log.debug(s);
    }

    @Override
    public void trace(String s) {
        //log.trace(s);
    }

    @Override
    public void warn(String s) {
        //log.warn(s);
    }
}
