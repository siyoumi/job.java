package com.siyoumi.mybatispuls;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.siyoumi.mybatispuls.func.FirstMap;
import com.siyoumi.mybatispuls.func.Get;
import com.siyoumi.mybatispuls.func.GetMaps;

import java.util.List;

public class FuncInjector
        extends DefaultSqlInjector {
    @Override
    public List<AbstractMethod> getMethodList(Class<?> mapperClass, TableInfo tableInfo) {
        List<AbstractMethod> list = super.getMethodList(mapperClass, tableInfo);
        list.add(new Get());
        list.add(new GetMaps());
        list.add(new FirstMap());

        return list;
    }
}
