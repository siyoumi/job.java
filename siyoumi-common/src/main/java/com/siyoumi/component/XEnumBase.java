package com.siyoumi.component;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.SneakyThrows;

/**
 * 枚举
 */
abstract public class XEnumBase<K>
        extends LinkedHashMap<K, String> {
    @SneakyThrows
    static public <T> T of(Class<T> clazz) {
        T app = XBean.newIns(clazz);
        return app;
    }

    protected XEnumBase() {
        initKV();
    }

    /**
     * 初始化key value
     */
    protected abstract void initKV();


    public String get(K key, String def) {
        String s = super.get(key);
        if (s == null) {
            return def;
        }

        return s;
    }

    public List<K> keys() {
        return super.keySet().stream().collect(Collectors.toList());
    }
}
