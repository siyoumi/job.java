package com.siyoumi.component.http;

import com.siyoumi.config.SysConfig;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XStr;
import lombok.NonNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface InputDataInterface {
    default String input(@NonNull String k) {
        return input(k, null);
    }

    default Map getBodyJson() {
        String body = XHttpContext.getBody();
        if (XStr.isNullOrEmpty(body)) {
            return null;
        }

        Map data = new HashMap();
        try {
            data = XJson.parseObject(body, Map.class);
        } catch (Exception ex) {
            XLog.error(this.getClass(), ex.getMessage());
        }
        return data;
    }

    String input(@NonNull String k, String def);

    default String getID() {
        return input("id");
    }

    default String getPid() {
        return input("pid");
    }

    default List<String> getIds() {
        String ids = input("ids");
        if (XStr.isNullOrEmpty(ids)) {
            return null;
        }

        return Arrays.asList(ids.split(","));
    }


    default Boolean isAdminEdit() {
        if (XStr.hasAnyText(getID())) {
            return true;
        }

        return false;
    }

    default Boolean isAdminAdd() {
        return !isAdminEdit();
    }


    /**
     * 当前页
     */
    default Integer getPageIndex() {
        return XStr.toInt(input("page"), 1);
    }

    default Integer getPageSize(Integer max) {
        Integer pageSize = XStr.toInt(input("page_size"), 20);
        if (pageSize <= 0) {
            pageSize = 10;
            if (SysConfig.getIns().isDev()) {
                pageSize = 2;
            }
        }
        return Math.min(max, pageSize);
    }

    /**
     * 页码
     */
    default Integer getPageSize() {
        if (isAdminExport()) {
            //导出
            if (SysConfig.getIns().isDev()) {
                return 10;
            }
            return 5000;
        }
        return getPageSize(1000);
    }


    /**
     * 导出
     */
    default Boolean isAdminExport() {
        return XHttpContext.get("__export__", 0) == 1;
    }
}
