package com.siyoumi.component.api.jj;

public enum RobotType
{
    ERROR("RobotErr"),
    MSG("RobotMsg");

    private String type;

    RobotType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
}
