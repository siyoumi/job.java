package com.siyoumi.component.api.jj.robot;

abstract public class RobotAbstract
{
    public abstract String getAccessToken();

    public abstract String getSecret();
}
