package com.siyoumi.component.api.jj;

import com.siyoumi.component.api.jj.robot.RobotAbstract;
import lombok.Data;

//钉钉机器人config参数
@Data
public class DingtalkRobot
        extends RobotAbstract {
    private String accessToken;
    private String secret;
}
