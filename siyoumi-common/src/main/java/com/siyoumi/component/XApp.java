package com.siyoumi.component;

import com.siyoumi.config.SysConfig;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XSnowflake;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class XApp
        extends com.siyoumi.util.XApp {
    /**
     * 文件访问链接
     */
    static public String fileUrl(String filePath) {
        if (XStr.isNullOrEmpty(filePath)) {
            return "";
        }

        if (XStr.startsWith(filePath, "http")) {
            return filePath;
        }

        String imgRoot = SysConfig.getIns().getImgRoot();
        return XStr.concat(imgRoot, filePath);
    }

    static public Long getID() {
        XSnowflake sf = XSnowflake.create(SysConfig.getIns().getIndex());
        return sf.nextId();
    }

    static public String getStrID() {
        return getStrID("");
    }

    static public String getStrID(String prefix) {
        return prefix + getID();
    }

    static public String shortId() {
        return shortId(getID());
    }

    public static String charsAll = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * 数量 转 短
     * 包含，数字大小写英文
     */
    static public String shortId(long num) {
        int scale = 62;

        StringBuilder sb = new StringBuilder();
        int remainder;
        while (num > scale - 1) {//大于62-1才计算 小于就直接是自身即可
            remainder = Long.valueOf(num % scale).intValue();
            sb.append(charsAll.charAt(remainder));
            num = num / scale;
        }
        sb.append(charsAll.charAt(Long.valueOf(num).intValue()));

        //log.debug(sb.toString());
        //log.debug(sb.reverse().toString());
        return sb.reverse().toString(); //颠倒字符串目的，为了让开头不变
        //return StringUtils.leftPad(id, 6, '0'); //不足6位向左补0
    }

    /**
     * Resource路径下文件内容
     * view/index.html
     * 最终路径
     * resources/view/index.html
     *
     * @param pathResource
     */
    @SneakyThrows
    static public String getFileContent(String pathResource) {
        ClassPathResource classPathResource = new ClassPathResource(pathResource);
        InputStream inputStream = classPathResource.getInputStream();
        Scanner scanner = new Scanner(inputStream, "UTF-8");
        String txt = scanner.useDelimiter("\\A").next();
        //
        inputStream.close();

        return txt;
    }


    static public String getUploadFileDir(String x) {
        return getUploadFileDir(x, true);
    }

    /**
     * 获取上传文件目录
     *
     * @param x
     */
    static public String getUploadFileDir(String x, Boolean addDate) {
        String dirPath = XStr.concat(SysConfig.getIns().getUploadPath(), x, "/");
        if (addDate) {
            String ym = XDate.format(XDate.now(), "yyyyMM");
            dirPath = XStr.concat(dirPath, ym, "/");
        }
        //
        File file = new File(dirPath);
        if (!file.exists()) {
            //目录不存在，创建
            file.mkdirs();
        }

        return dirPath;
    }


    static public Boolean isJoinWrpperPlus(Object o) {
        return o.getClass().getSimpleName().equals("JoinWrapperPlus");
    }


    static public String setCsvCol(String v) {
        return setCsvCol(v, null);
    }

    /**
     * 处理特殊字符，一般情况用于导出
     *
     * @param v
     * @param prefix
     */
    static public String setCsvCol(String v, String prefix) {
        String vv = v.replace(",", "，")
                .replace("\\n", "")
                .replace("\"", "");
        if (XStr.hasAnyText(prefix)) {
            vv = prefix + vv;
        }

        return vv;
    }


    /**
     * 事务处理器
     */
    static public TransactionTemplate getTransaction() {
        return getTransaction(30);
    }

    static public TransactionTemplate getTransaction(int timeout) {
        TransactionTemplate transactionTemplate = XSpringContext.getBean(TransactionTemplate.class);
        transactionTemplate.setTimeout(timeout);
        return transactionTemplate;
    }

    /**
     * 是否事务中
     */
    static public boolean isTransaction() {
        return TransactionSynchronizationManager.isActualTransactionActive();
    }
}
