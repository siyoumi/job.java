package com.siyoumi.component.auth_img.entity;

import lombok.Data;

@Data
public class AuthImgData {
    String code;

    public static AuthImgData getInstance(String code) {
        AuthImgData data = new AuthImgData();
        data.setCode(code);

        return data;
    }
}
