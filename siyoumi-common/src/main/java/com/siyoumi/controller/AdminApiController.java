package com.siyoumi.controller;


import com.siyoumi.component.XApp;
import com.siyoumi.config.SysConfig;
import com.siyoumi.component.http.InputDataInterface;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//后台接口
public class AdminApiController
        extends ApiController
        implements InputDataInterface {
    @Override
    public XReturn getR() {
        String key = "common_return";

        XReturn r = XHttpContext.get(key);
        if (r == null) {
            r = XReturn.getR(0);
            //初始化，页面参数
            HashMap<String, Object> pageInfo = new HashMap<>();
            pageInfo.put("is_admin_edit", isAdminEdit());
            pageInfo.put("app_id", XHttpContext.getAppId());
            pageInfo.put("static_file_root", SysConfig.getIns().getImgRoot());

            //权限
            HashMap<String, Object> role = new HashMap<>();
            SysAccount entityAcc = getEntityAcc();
            role.put("is_dev", SysAccountService.getBean().isDev(entityAcc));
            role.put("is_super_admin", SysAccountService.getBean().isSuperAdmin(entityAcc));
            pageInfo.put("role", role);

            r.setData("page_info", pageInfo);

            HashMap<String, Object> pageFrom = new HashMap<>();
            r.setData("page_form", pageFrom);

            XHttpContext.set(key, r);
        }
        r.setErrCode(0);

        return r;
    }


    /**
     * 设置页面标题
     */
    public void setPageTitle(String... title) {
        setPageInfo("title", XStr.concat(title));
    }

    /**
     * 设置页面参数
     */
    public void setPageInfo(String k, Object v) {
        setPageData("page_info", k, v);
    }

    /**
     * 设置提交参数
     */
    public void setPageFrom(String k, Object v) {
        setPageData("page_form", k, v);
    }


    private void setPageData(String rKey, String k, Object v) {
        HashMap<String, Object> data = (HashMap<String, Object>) getR().get(rKey);
        if (data == null) {
            data = new HashMap<>();
        }
        data.put(k, v);

        getR().setData(rKey, data);
    }

    public void setAdminExport() {
        XHttpContext.set("__export__", 1);
    }

    public String getAccId() {
        return XHttpContext.getTokenData().getData("acc_id");
    }

    public SysAccount getEntityAcc() {
        return SysAccountService.getBean().getSysAccountByToken();
    }

    /**
     * 商家ID
     */
    public String getStoreId() {
        return getEntityAcc().getAcc_store_id();
    }

    /**
     * 商家帐号
     */
    public Boolean isStore() {
        return "store".equals(getEntityAcc().getAcc_role());
    }

    /**
     * 导出数据处理
     *
     * @param tableHead
     * @param tableData
     * @param first     true：表头
     */
    public List<List<String>> adminExprotData(Map<String, String> tableHead, List<Map<String, Object>> tableData, Boolean first) {
        List<List<String>> listData = new ArrayList<>();
        if (first) {
            //设置表头，第一页才设置
            List<String> head = new ArrayList<>();
            tableHead.forEach((k, v) -> {
                head.add(v);
            });
            listData.add(head);
        }

        //表内容
        for (Map<String, Object> item : tableData) {

            List<String> data = new ArrayList<>();
            for (Map.Entry<String, String> entryHead : tableHead.entrySet()) {
                Object v = item.get(entryHead.getKey());
                if (v == null) {
                    v = "";
                }
                String val = XApp.setCsvCol(v.toString());
                data.add(val);
            }

            listData.add(data);
        }

        return listData;
    }
}
