package com.siyoumi.controller;

import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
public class CustomerErrorController
        implements ErrorController {
    @RequestMapping("/error")
    public XReturn handleError(HttpServletRequest request, @RequestParam Map<String, Object> params) {
        //获取statusCode:401,404,500
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == 404) {
            XReturn r = XReturn.getR(statusCode, "API NOT FOUND");
            //XValidator.err(r);
//            r.putAll(param);
            return r;
        } else {
            //XValidator.err(EnumSys.SYS.getR());
            return EnumSys.SYS.getR();
        }
    }
}
