package com.siyoumi.controller;

import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.XReturn;

//通用接口
public class ApiController
        extends BaseController {
    /**
     * 统一接口
     */
    public XReturn getR() {
        String key = "common_return";

        XReturn r = XHttpContext.get(key);
        if (r == null) {
            r = XReturn.getR(0);
            XHttpContext.set(key, r);
        }
        r.setErrCode(0);

        return r;
    }
}
