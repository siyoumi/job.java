package com.siyoumi.mapper;

import com.siyoumi.entity.SysAppRouter;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_app_router
@Component
public interface SysAppRouterMapper
        extends MapperBase<SysAppRouter>
{
}
