package com.siyoumi.mapper;

import com.siyoumi.entity.SysAccsuperZzzApp;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_accsuper_zzz_app
@Component
public interface SysAccsuperZzzAppMapper
        extends MapperBase<SysAccsuperZzzApp>
{
}
