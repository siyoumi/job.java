package com.siyoumi.mapper;

import com.siyoumi.entity.SysRoleRouter;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_role_router
@Component
public interface SysRoleRouterMapper
        extends MapperBase<SysRoleRouter>
{
}
