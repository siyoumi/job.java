package com.siyoumi.mapper;

import com.siyoumi.entity.SysApp;
import com.siyoumi.mybatispuls.MapperBase;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;


//t_s_app
@Component
public interface SysAppMapper
        extends MapperBase<SysApp> {
    @Select({
            "SET innodb_lock_wait_timeout=#{sec}",
    })
    void setInnodbLockWaitTimeout(int sec);
}
