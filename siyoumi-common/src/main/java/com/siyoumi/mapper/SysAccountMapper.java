package com.siyoumi.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.mybatispuls.MapperBase;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.Map;


//t_s_account
@Component
public interface SysAccountMapper
        extends MapperBase<SysAccount>
{
    @Select("SELECT ${ew.getSqlSelect} FROM wx_app.t_s_account JOIN wx_app.t_sys_role ON acc_role = role_id ${ew.getCustomSqlSegment}")
    IPage<Map<String, Object>> listToMap(IPage<SysAccount> page, @Param("ew") Wrapper<SysAccount> queryWrapper);
}
