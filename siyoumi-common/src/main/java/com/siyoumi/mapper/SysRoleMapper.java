package com.siyoumi.mapper;

import com.siyoumi.entity.SysRole;
import com.siyoumi.mybatispuls.MapperBase;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;


//t_sys_role
@Component
public interface SysRoleMapper
        extends MapperBase<SysRole>
{
    /**
     * 根据角色获取1级菜单列表
     *
     * @param role 角色ID
     * @return 1级菜单列表
     */
    @Select("SELECT DISTINCT appr_pid\n" +
            "FROM wx_app.t_s_app_router JOIN wx_app.t_sys_role_router ON roro_appr_path = appr_path\n" +
            "WHERE roro_role_id = #{role} AND appr_pid <> ''")
    List<String> getPidByRole(String role);
}
