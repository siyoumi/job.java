package com.siyoumi.mapper;

import com.siyoumi.entity.SysLog;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_log
@Component
public interface SysLogMapper
        extends MapperBase<SysLog>
{
}
