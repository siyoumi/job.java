package com.siyoumi.mapper;

import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_accsuper_config
@Component
public interface SysAccsuperConfigMapper
        extends MapperBase<SysAccsuperConfig>
{
}
