package com.siyoumi.aspect;

import com.siyoumi.annotation.PreAuth;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

//权限
@Slf4j
@Aspect
@Component
public class AspectPreAuth {
    @Around("@annotation(preAuth)")
    public Object around(ProceedingJoinPoint proceedingJoinPoint, PreAuth preAuth) throws Throwable {
        log.debug("AspectPreAuth:{}", preAuth.value());

        return proceedingJoinPoint.proceed();
    }
}
