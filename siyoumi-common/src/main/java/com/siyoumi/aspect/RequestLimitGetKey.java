package com.siyoumi.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

public interface RequestLimitGetKey {
    String getKey(ProceedingJoinPoint point);
}
