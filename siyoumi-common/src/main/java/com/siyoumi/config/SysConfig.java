package com.siyoumi.config;

import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.api.jj.DingtalkRobot;
import com.siyoumi.component.http.XHttpContext;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ConfigurationProperties(prefix = "siyoumi")
@Validated
public class SysConfig {
    static public SysConfig getBean() {
        return XSpringContext.getBean(SysConfig.class);
    }

    static private SysConfig ins = new SysConfig();

    //登陆失败数量
    private Integer loginErrMax = 3;
    private Integer index = 0;
    private String env = "dev";
    private String jwtPk; //jwt密钥
    private String jwtPkSiyoumi; //jwt密钥-siyoumi

    @NotBlank(message = "请配置应用域名")
    private String appRoot = "http://app.x.siyoumi.com/";
    @NotBlank(message = "请配置图片域名")
    private String imgRoot;
    @NotBlank(message = "请配置图片保存路径")
    private String uploadPath;
    private String resPath;
    private Integer uploadFileSizeMax = 2;//上传文件大小
    private String certPath; //支付证书路径

    private DingtalkRobot dingtalkRobot; //钉钉参数
    private SysConfigConsole console; //控制台参数

    @NestedConfigurationProperty
    private List<SysConfigTask> task;

    public Boolean isDev() {
        return isServer("dev");
    }

    public Boolean isServer(String server) {
        return server.equals(getEnv().toLowerCase());
    }

    static public SysConfig getIns() {
        return ins;
    }

    @PostConstruct
    private void setStatic() {
        ins = this;
    }
}
