package com.siyoumi.config;

import com.siyoumi.interceptor.AdminInterceptor;
import com.siyoumi.interceptor.CommonInterceptor;
import com.siyoumi.interceptor.OpenApiInterceptor;
import com.siyoumi.interceptor.WebInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig
        implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //通用过滤器
        registry.addInterceptor(new CommonInterceptor())
                .addPathPatterns("/**")
                .order(0)
//                .excludePathPatterns("/test**")
        ;

        //后台过滤器
        registry.addInterceptor(new AdminInterceptor())
                .addPathPatterns("/xadmin/**")
                .order(1);

        //前端过滤器
        registry.addInterceptor(new WebInterceptor())
                .addPathPatterns("/wxapp/**")
                .order(1);

        //open api过滤器
        registry.addInterceptor(new OpenApiInterceptor())
                .addPathPatterns("/open/**")
                .order(1);
    }
}
