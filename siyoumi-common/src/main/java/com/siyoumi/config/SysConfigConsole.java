package com.siyoumi.config;

import lombok.Data;

@Data
public class SysConfigConsole {
    String address; //域名，端口，例：app.sh.siyoumi.com:8110
    String longToken; //长期token
    String cmdIndex; //命令下标
}
