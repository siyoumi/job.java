package com.siyoumi.config;

import feign.Request;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class FeignConfig {
    @Bean
    public System.Logger.Level feignLoggerLevel() {
        if (SysConfig.getIns().isDev()) {
            return System.Logger.Level.DEBUG;
        }
        return System.Logger.Level.INFO;
    }

    /**
     * 超时时间配置
     */
    @Bean
    public Request.Options options() {
        return new Request.Options(3, TimeUnit.SECONDS, 5, TimeUnit.SECONDS, true);
    }
}
