package com.siyoumi.config;

import com.siyoumi.util.XDate;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Component
public class LocalDateTimeFormatter
        implements Formatter<LocalDateTime> {
    @Override
    public LocalDateTime parse(String text, Locale locale) throws ParseException {
        return XDate.parse(text);
    }


    @Override
    public String print(LocalDateTime localDateTime, Locale locale) {
        return XDate.toDateTimeString(localDateTime);
    }


}
