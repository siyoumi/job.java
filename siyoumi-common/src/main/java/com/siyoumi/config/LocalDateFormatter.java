package com.siyoumi.config;

import com.siyoumi.util.XDate;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Component
public class LocalDateFormatter
        implements Formatter<LocalDate> {

    private final DateTimeFormatter formatter = XDate.DATE_PATTERN;


    @Override
    public LocalDate parse(String text, Locale locale) throws ParseException {
        return LocalDate.parse(text, formatter);
    }


    @Override
    public String print(LocalDate localDate, Locale locale) {
        return formatter.format(localDate);
    }


}
