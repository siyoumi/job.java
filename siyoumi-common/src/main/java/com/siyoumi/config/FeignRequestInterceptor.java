package com.siyoumi.config;

import com.siyoumi.component.XJwt;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class FeignRequestInterceptor
        implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        String pk = SysConfig.getIns().getJwtPkSiyoumi();
        log.debug("pk: {}", pk);
        XReturn r = XReturn.getR(0);

        String token = XJwt.getBean().getToken(r, pk, 300);
        log.debug("token: {}", token);
        requestTemplate.header("token", token);
    }
}
