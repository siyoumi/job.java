package com.siyoumi.config;

import lombok.Data;

@Data
public class SysConfigTask {
    String id;
    String name;
}