package com.siyoumi.annotation;

import com.siyoumi.aspect.RequestLimitGetKeyDef;

import java.lang.annotation.*;


@Documented
@Inherited
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestLimit {
    // 在 second 秒内，尝试获取锁，获取锁失败会提示
    int second() default 10;

    //redis key默认RequestLimitGetKeyDef
    Class key() default RequestLimitGetKeyDef.class;
}
