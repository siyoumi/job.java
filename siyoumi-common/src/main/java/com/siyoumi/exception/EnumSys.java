package com.siyoumi.exception;

/**
 * 10：系统错误
 * 20：app错误
 */
public enum EnumSys
        implements IEnumSys {
    OK(0, "成功"),
    TEST_ERR(11000, "测试方法错误"),
    MISS_VAL(10001, "缺少参数"),
    ERR_VAL(10002, "参数异常"),
    ARR_SIZE_0(10003, "列表数量为0"),
    ERR_OPENID(10004, "用户异常"),
    ERR_UID(10005, "用户未登陆"),
    WEB_AUTH_ERR(10008, "权限不足"),
    JJ_ROBOT(10010, "钉钉机械人类型异常"),
    API_ERROR(10020, "接口异常"),
    REDIS_GETLOCK_ERR(10100, "获取锁失败"),
    REDIS_ERR(10110, "REDIS事件异常"),
    WXAPI_ERROR(10220, "微信接口异常"),
    LIMIT_MAX_ERROR(10250, "访问过于频繁"),
    FILE_EXTENSTION(10300, "文件格式异常"),
    FILE_SIZE(10310, "文件不能大小异常"),
    FILE_EXISTS(10320, "文件不存在"),
    SYS_VAILD(10422, "提交的数据不正确，请重新输入"),
    ERR_DATA(10450, "数据异常"),
    TOKEN_MISS(10550, "缺少令牌"),
    TOKEN_EXISTS(10560, "令牌不存在"),
    TOKEN_ERR(10570, "令牌已失效"),
    TOKEN_EXPIRED(10580, "令牌已失效[expired]"),
    PWD_ERR(10590, "密码错误"),
    ADMIN_AUTH_APP_ERR(10507, "权限不足"),
    ADMIN_AUTH_SUPER_ERR(10508, "权限不足，非超管"),
    ADMIN_AUTH_IMG_ERR(10115, "验证失败"),
    ENV_ERR(10551, "环境错误"),
    SYS(10500, "系统未知错误"),
    SYS_STATUS(10560, "系统状态异常"),

    ORDER_PAY_OK(10600, "订单已支付"),
    ORDER_PAY_EXPIRE_HANDLE(10610, "订单过期已处理"),
    ;

    private Integer errcode;
    private String errmsg;

    EnumSys(Integer errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }


    @Override
    public Integer getErrcode() {
        return errcode;
    }

    @Override
    public String getErrmsg() {
        return errmsg;
    }
}
