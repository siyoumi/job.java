package com.siyoumi.exception;

import com.siyoumi.util.XReturn;

//web错误显示
public class XWebException
        extends XException {
    private XReturn r;

    public XWebException(XReturn r) {
        super(r.getErrMsg());
        this.r = r;
    }
}
