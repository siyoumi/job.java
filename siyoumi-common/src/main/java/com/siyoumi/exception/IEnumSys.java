package com.siyoumi.exception;

import com.siyoumi.util.XReturn;

public interface IEnumSys {
    default XReturn getR() {
        return XReturn.getR(this.getErrcode(), this.getErrmsg());
    }

    default XReturn getR(String errmsg) {
        return XReturn.getR(this.getErrcode(), errmsg);
    }
    
    Integer getErrcode();

    String getErrmsg();
}
