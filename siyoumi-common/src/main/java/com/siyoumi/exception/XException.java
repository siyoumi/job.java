package com.siyoumi.exception;

import com.siyoumi.util.XReturn;
import lombok.Getter;
import lombok.Setter;

public class XException
        extends RuntimeException {
    private XReturn r = EnumSys.SYS.getR();
    @Getter
    private Boolean showErrInfo = true;

    public XException() {
        super();
    }

    public XException(String msg) {
        super(msg);
        this.r.setErrMsg(msg);
    }

    public XException(String errmsg, int errcode) {
        super(errmsg);
        this.r.setErrCode(errcode);
        this.r.setErrMsg(errmsg);
    }

    public XException(XReturn r) {
        super(r.getErrMsg());
        this.r = r;
    }

    public XException(XReturn r, Boolean showErrInfo) {
        super(r.getErrMsg());
        this.r = r;
        this.showErrInfo = showErrInfo;
    }

    public XReturn getR() {
        return this.r;
    }
}
