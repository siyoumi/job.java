package com.siyoumi.task;

import com.siyoumi.exception.IEnumSys;
import com.siyoumi.util.XReturn;

/**
 * 10：系统错误
 * 20：app错误
 */
public enum EnumTask
        implements IEnumSys {
    STOP(11000, "终止执行"),
    RUN(0, "继续执行"),
    ;


    private Integer errcode;
    private String errmsg;

    EnumTask(Integer errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    @Override
    public Integer getErrcode() {
        return errcode;
    }

    @Override
    public String getErrmsg() {
        return errmsg;
    }
}
