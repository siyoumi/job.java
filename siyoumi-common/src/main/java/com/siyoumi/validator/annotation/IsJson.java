package com.siyoumi.validator.annotation;

import com.siyoumi.validator.IsJsonValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//json
//仅仅用于请求字段的非空校验
@Target(ElementType.FIELD)
//需要jvm运行时使用
@Retention(RetentionPolicy.RUNTIME)
//使用validator时必须添加，指定与NotNullValidator自定义校验器配合使用
@Constraint(validatedBy = IsJsonValidator.class)
public @interface IsJson {
    //使用validator时必须添加，主要是将validator进行分类，不同的group中会执行不同的validator操作
    Class<?>[] groups() default {};

    //使用validator时必须添加，主要针对bean的
    Class<? extends Payload>[] payload() default {};

    //使用validator时必须添加，定制化的提示信息
    String message() default "非json格式";

    //自定义注解类型，如果不适用validator，只需要写这一个注解类型即可。
    int ret() default 7;
}
