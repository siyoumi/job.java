package com.siyoumi.validator;

import com.siyoumi.util.XStr;
import com.siyoumi.validator.annotation.IsJson;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsJsonValidator
        implements ConstraintValidator<IsJson, Object> {
    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        if (o instanceof String) {
            if (XStr.isNullOrEmpty((String) o)) {
                return false;
            }

            try {
                XStr.toJson((String) o);
                return true;
            } catch (Exception ex) {
            }
        }

        return false;
    }
}
