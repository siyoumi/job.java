package com.siyoumi.validator;

import com.siyoumi.util.XStr;
import com.siyoumi.validator.annotation.HasAnyText;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class HasAnyTextValidator
        implements ConstraintValidator<HasAnyText, Object> {
    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        if (o instanceof String) {
            return XStr.hasAnyText((String) o);
        } else {
            if (o != null) {
                return true;
            }
        }

        return false;
    }
}
