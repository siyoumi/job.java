package com.siyoumi.util;


import com.alibaba.fastjson.JSON;
import com.siyoumi.component.XBean;
import com.siyoumi.entity.SysApp;
import com.siyoumi.component.XJwt;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class testStr {
    @Test
    @DisplayName("字符串通用方法")
    public void test() {
        Assert.isTrue(!XStr.hasAnyText(""), "空字符串");
        Assert.isTrue(XStr.hasAnyText("123"), "数字字符串");
        Assert.isTrue(XStr.isNullOrEmpty("undefined"), "undefined字符串");
        Assert.isTrue(XStr.isNullOrEmpty(null), "null");
    }

    @Test
    @DisplayName("实体转json")
    void test_020() {
        XReturn r = XReturn.getR(0);
        r.setData("a", 123);

        String str = JSON.toJSONString(r); //实体转json
        XReturn r2 = JSON.parseObject(str, XReturn.class); //json转实体
        Integer a = r2.getData("a", 0);


        XLog.debug(this.getClass(), str);
        XLog.debug(this.getClass(), r2);
        XLog.debug(this.getClass(), a);
        XLog.debug(this.getClass(), r.getData("abc"));

        XReturn json_r = XReturn.parse("123");
        XLog.debug(this.getClass(), json_r);
    }


    @Test
    @DisplayName("雪花算法")
    void test_040() {
        XSnowflake sf = XSnowflake.create(8L);
        for (int i = 0; i < 50; i++) {
            XLog.debug(this.getClass(), sf.nextId());
        }
    }

    @Test
    @DisplayName("测试JWT")
    void test_050() {
        XJwt app = XJwt.getBean();
        HashMap<String, Object> arr = new HashMap<>();
        arr.put("abc", 123);
        arr.put("test", "aaaaa");

        String token = app.getToken(arr);
        XLog.debug(this.getClass(), token);
        XReturn r = app.parseToken(token);
        XLog.debug(this.getClass(), r);
        Assert.isTrue(r.ok(), r.getErrMsg());

        //过期测试
        String token1 = app.getToken(arr, 1);
//        HelperUtil.funcSleep(1);
        XLog.debug(this.getClass(), token1);

        r = app.parseToken(token1);
        XLog.debug(this.getClass(), r);
        Assert.isTrue(r.ok(), r.getErrMsg());
    }


    @Test
    @DisplayName("测试Map")
    void test_060() {
        Map<String, Object> arr = new HashMap<String, Object>();
        arr.put("123", "123123");
        Object v = arr.get("123");
        System.out.println(v);
    }


    @Test
    @DisplayName("下划线转驼峰")
    public void test_070() {
        String td = "sys_set_data";
        System.out.println(td);
        String td_new = XStr.lineToHump(td);
        System.out.println(td_new);
    }


    @Test
    @DisplayName("base64使用")
    public void test_080() {
        String str1 = "eyJleHAiOjE2NTIwODExNjcsIm1hcCI6eyJhYmMiOjEyMywidGVzdCI6ImFhYWFhIn19";
        XLog.debug(this.getClass(), str1);
        //
        String dec_pwd = XStr.base64Dec(str1);
        XLog.debug(this.getClass(), dec_pwd);
        //
        XLog.debug(this.getClass(), XStr.md5(dec_pwd));
    }


    @Test
    @DisplayName("字符串拼接")
    public void test_090() {
        String str = XStr.concat("1234", "abc", "aaaa");
        XLog.debug(this.getClass(), str);
    }


    @Test
    @DisplayName("转json")
    public void test_tojson() {
        String str1 = "{\"exp\":1652081167,\"map\":{\"abc\":123,\"test\":\"aaaaa\"}}";
        Map<String, Object> json = XStr.toJson(str1);

        XLog.debug(this.getClass(), json);
        XLog.debug(this.getClass(), json.get("map"));

        Map<String, Object> map = (Map<String, Object>) json.get("map");
        XLog.debug(this.getClass(), map.get("abc"));
    }


    @Test
    @DisplayName("转json数组")
    void test_110() {
        String str = "[{\"app_create_date\":\"2019-07-09T22:46:09\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"account\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"帐号管理\",\"app_order\":0,\"app_type\":\"super_admin\",\"app_update_date\":\"2021-04-06T21:55:45\",\"key\":\"account\",\"prefix\":\"app_\"},{\"app_create_date\":\"2019-11-05T21:07:02\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"accsuper_admin\",\"app_is_wx\":0,\"app_is_wxapp\":0,\"app_name\":\"超管管理\",\"app_order\":0,\"app_type\":\"x\",\"app_update_date\":\"2021-04-06T21:44:30\",\"key\":\"accsuper_admin\",\"prefix\":\"app_\"},{\"app_create_date\":\"2020-10-31T15:51:52\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"act\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"活动\",\"app_order\":0,\"app_type\":\"\",\"app_update_date\":\"2021-04-06T21:55:45\",\"key\":\"act\",\"prefix\":\"app_\"},{\"app_create_date\":\"2019-11-05T21:07:02\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"app_admin\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"应用管理\",\"app_order\":0,\"app_type\":\"super_admin\",\"app_update_date\":\"2021-04-06T21:55:45\",\"key\":\"app_admin\",\"prefix\":\"app_\"},{\"app_create_date\":\"2021-04-18T18:48:04\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"code_exchange\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"码兑换中心\",\"app_order\":0,\"app_type\":\"\",\"app_update_date\":\"2021-05-14T22:49:00\",\"key\":\"code_exchange\",\"prefix\":\"app_\"},{\"app_create_date\":\"2020-09-03T21:59:39\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"file\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"文件管理\",\"app_order\":0,\"app_type\":\"\",\"app_update_date\":\"2021-04-06T21:55:45\",\"key\":\"file\",\"prefix\":\"app_\"},{\"app_create_date\":\"2019-11-17T21:51:35\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"place\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"景点\",\"app_order\":0,\"app_type\":\"\",\"app_update_date\":\"2021-09-25T10:53:27\",\"key\":\"place\",\"prefix\":\"app_\"},{\"app_create_date\":\"2019-07-09T22:45:15\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"pwd\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"修改密码\",\"app_order\":0,\"app_type\":\"common\",\"app_update_date\":\"2021-04-06T21:47:34\",\"key\":\"pwd\",\"prefix\":\"app_\"},{\"app_create_date\":\"2019-11-04T21:26:13\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"setting\",\"app_is_wx\":0,\"app_is_wxapp\":0,\"app_name\":\"设置\",\"app_order\":0,\"app_type\":\"super_admin\",\"app_update_date\":\"2021-03-06T11:30:02\",\"key\":\"setting\",\"prefix\":\"app_\"},{\"app_create_date\":\"2021-10-23T16:44:58\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"sh\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"脚本\",\"app_order\":0,\"app_type\":\"x\",\"app_update_date\":\"2021-10-23T16:44:58\",\"key\":\"sh\",\"prefix\":\"app_\"},{\"app_create_date\":\"2020-07-14T22:08:19\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"sheet\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"表单\",\"app_order\":0,\"app_type\":\"common\",\"app_update_date\":\"2022-05-12T22:55:44\",\"key\":\"sheet\",\"prefix\":\"app_\"},{\"app_create_date\":\"2020-05-31T17:56:00\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"sys_log\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"系统日志\",\"app_order\":0,\"app_type\":\"common\",\"app_update_date\":\"2021-04-06T21:51:03\",\"key\":\"sys_log\",\"prefix\":\"app_\"},{\"app_create_date\":\"2020-12-06T22:25:16\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"tmplmsg\",\"app_is_wx\":1,\"app_is_wxapp\":0,\"app_name\":\"模板消息\",\"app_order\":0,\"app_type\":\"common\",\"app_update_date\":\"2022-05-14T10:25:28\",\"key\":\"tmplmsg\",\"prefix\":\"app_\"},{\"app_create_date\":\"2020-06-26T13:10:07\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"user\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"用户\",\"app_order\":0,\"app_type\":\"common\",\"app_update_date\":\"2021-04-06T21:51:03\",\"key\":\"user\",\"prefix\":\"app_\"},{\"app_create_date\":\"2019-11-05T21:07:02\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"view_admin\",\"app_is_wx\":0,\"app_is_wxapp\":0,\"app_name\":\"页面管理\",\"app_order\":0,\"app_type\":\"x\",\"app_update_date\":\"2021-04-06T21:44:30\",\"key\":\"view_admin\",\"prefix\":\"app_\"},{\"app_create_date\":\"2021-07-18T10:41:17\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"website\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"官网\",\"app_order\":0,\"app_type\":\"\",\"app_update_date\":\"2021-07-18T10:42:42\",\"key\":\"website\",\"prefix\":\"app_\"},{\"app_create_date\":\"2021-07-18T10:40:43\",\"app_desc\":\"\",\"app_enable\":1,\"app_icon\":\"\",\"app_id\":\"website_option\",\"app_is_wx\":1,\"app_is_wxapp\":1,\"app_name\":\"官网配置\",\"app_order\":0,\"app_type\":\"\",\"app_update_date\":\"2021-08-01T21:07:08\",\"key\":\"website_option\",\"prefix\":\"app_\"}]";
        List<Map> maps = XStr.toJsonArr(str);
        for (Map<String, Object> obj : maps) {
            SysApp tt = new SysApp();
            XBean.copyProperties(obj, tt);
            XLog.debug(this.getClass(), tt);
        }
        //List<SysApp> sysApps = JSON.parseArray(str, SysApp.class);

        //XLog.info(this.getClass(), sysApps);
    }

    @Test
    @DisplayName("字符串转其他类型")
    void test_120() {
        XLog.debug(this.getClass(), XStr.parseType("1", Integer.class, 0));
        LocalDateTime dateTime = XStr.parseType("2022-07-16 09:56:50", LocalDateTime.class, XDate.now());
        XLog.debug(this.getClass(), dateTime);
        XLog.debug(this.getClass(), XStr.parseType("1.12", BigDecimal.class, 0));
        XLog.debug(this.getClass(), XStr.parseType("123456", Long.class, 0));
        XLog.debug(this.getClass(), XStr.parseType("123456", String.class, null));
    }

    @Test
    @DisplayName("字符串替换")
    void test_130() {
        String str = "abc12[aa2aa]";
        String str2 = str.replaceAll("[0-9]", "");
        XLog.debug(this.getClass(), str);
        XLog.debug(this.getClass(), str2);
    }
}
