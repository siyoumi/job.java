package com.siyoumi.util;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;


public class testDate {
    @Test
    @DisplayName("日期通用方法")
    public void test() {
        XLog.debug(this.getClass(), XDate.toDateTimeString());

        Duration between = XDate.between(XDate.now().plusMinutes(2), XDate.now());
        XLog.debug(this.getClass(), between.toSeconds());
        XLog.debug(this.getClass(), between.abs().toSeconds());
    }

    @Test
    @DisplayName("时间戳转换")
    public void test_001() {
        LocalDateTime now = XDate.now();
        long ms = XDate.toS(now);
        XLog.debug(this.getClass(), ms);

        LocalDateTime dt = XDate.parseTimestamp(ms);
        XLog.debug(this.getClass(), dt);
//        XDate.parseByTimestamp()
    }

    @Test
    @DisplayName("字符串转datetime")
    public void test_002() {
        String str = "2020-05-22 15:19:00";

        LocalDateTime dt = XStr.toDateTime(str);
        XLog.debug(this.getClass(), dt);

        //str = "2020-05-22";
        LocalDateTime today = XDate.today();
        XLog.debug(this.getClass(), today);
        XLog.debug(this.getClass(), XDate.format(today, "yyyyMMdd"));
    }
}
