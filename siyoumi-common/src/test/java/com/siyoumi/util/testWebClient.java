package com.siyoumi.util;

import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;

import java.util.HashMap;
import java.util.Map;

public class testWebClient {
    @Test
    void test_000() {
        String url = "https://sapi.wx.xiao-bo.com/z/app.zyhn/z_app/lucky_card/0/web/zz_api__api__haibao__friendList" +
                "?page=1&page_size=10&site_id=app.zyhn&wx_from=oWwkf4-no4wpolzuPRq8UAP4KFXo&wx_from_enc=ZDhiNDFmMYjFkM2EyWTBMVzV2Tkhkd2IyeDZkVkJTY1RoVlFWQTBTMFpZYnc9PQ%3D%3D&";

        XHttpClient client = XHttpClient.getInstance();
        client.setProxy("127.0.0.1", 1080);
        String returnStr = client.get(XStr.urlDec(url), null);
        XLog.debug(this.getClass(), returnStr);
    }

    @Test
    void test_001() {
        String url = "https://sapi.wx.xiao-bo.com/z/app.zyhn/z_app/lucky_card/0/web/zz_api__api__haibao__friendList" +
                "?page=1&page_size=10&site_id=app.zyhn&wx_from=oWwkf4-no4wpolzuPRq8UAP4KFXo&wx_from_enc=ZDhiNDFmMYjFkM2EyWTBMVzV2Tkhkd2IyeDZkVkJTY1RoVlFWQTBTMFpZYnc9PQ%3D%3D&";

        XHttpClient client = XHttpClient.getInstance();
        String returnStr = client.get(XStr.urlDec(url), null);

        XLog.debug(this.getClass(), returnStr);
    }

    @Test
    @DisplayName("表单提交")
    public void test_102() {
        Map<String, String> post_data = new HashMap<String, String>();
        post_data.put("abc", "1");

        Map<String, String> header_data = new HashMap<String, String>();
        header_data.put("apixAuth", "1");
        header_data.put(HttpHeaders.COOKIE, "login_url=%2Fz_ui%2Fa_admin%2Findex%2Fad; center_url=%2Fz_ui%2Fa_admin%2Fcenter_ad; token_login=53176171-627476b66154e-356a-a344-8eefbe977ed8; site_id=app.jmeii; token_qr=92548972-627476c3b4212-3299-b5bd-54e1cc337e4e; center_id=ad");

        String url = "http://localhost:8000/oa/test";
        //url = "https://app.siyoumi.com/test";
        url = "https://sapi.uat.xiao-bo.com/z_app/sign_in/web/zz_api__api__new__sign?site_id=app" +
                ".english&wx_from=onTJv5a-qVvK_S4ckE6PyPV51hKE&wx_from_enc" +
                "=NDgyZjAzNYjI1VVNuWTFZUzF4Vm5aTFgxTTBZMnRGTmxCNVVGWTFNV2hMUlE9PQ==&wx_word=sign_in10001000";
        XHttpClient client = XHttpClient.getInstance();
        String s = client.postForm(url, header_data, post_data);
        XReturn r = JSON.parseObject(s, XReturn.class);
        XLog.debug(this.getClass(), r);
    }

    @Test
    @DisplayName("json提交")
    public void test_103() {
        Map<String, Object> post_data = new HashMap<String, Object>();
        post_data.put("abc", "1");

        String url = "http://localhost:8000/oa/test";
        url = "https://app.siyoumi.com/test";
        XHttpClient client = XHttpClient.getInstance();
        String s = client.postJson(url, null, post_data);
        XLog.debug(this.getClass(), s);
    }
}
