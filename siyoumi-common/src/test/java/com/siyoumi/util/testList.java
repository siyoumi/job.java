package com.siyoumi.util;


import com.siyoumi.component.http.InputData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.*;


public class testList {
    @Test
    @DisplayName("列表操作")
    public void test_100() {
        String str = "21345678910";

        List<String> list = Arrays.asList(str.split(""));
        //list;
        XLog.debug(this.getClass(), list);

        Boolean o = list.stream().findAny().map(v ->
        {
            return v.equals("abc");
        }).get();
        XLog.debug(this.getClass(), o);

        Boolean firstFind = list.stream().findFirst().map(v ->
        {
            return v.equals("1");
        }).get();
        XLog.debug(this.getClass(), firstFind);
        Assert.isTrue(!firstFind, "查询第1项");

        Boolean anyFind = list.contains("2");
        XLog.debug(this.getClass(), anyFind);
        Assert.isTrue(anyFind, "查询所有项");

        //过滤非1的数据
        Object[] newList = list.stream().filter(v ->
        {
            return v.equals("1"); //true，就是要
        }).toArray();
        XLog.debug(this.getClass(), Arrays.asList(newList));
    }


    @Test
    @DisplayName("contains是全等匹配的")
    public void test_110() {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("13");
        list.add("user");
        list.add("account");


        XLog.debug(this.getClass(), list.contains("3"));
        XLog.debug(this.getClass(), list.contains("user"));
        XLog.debug(this.getClass(), list);
    }


    @Test
    @DisplayName("pullAll")
    public void test_200() {
        InputData in = InputData.getIns();
        in.put("a", "123");

        HashMap<String, Object> map = new HashMap<>();
        map.put("a", 123);

        in.putAll(map);
        XLog.debug(this.getClass(), in);
    }
}
