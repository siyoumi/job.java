package com.siyoumi.util;

import com.siyoumi.component.XJwt;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class testAbs
{
    @Test
    @DisplayName("单例")
    void test_001()
    {
        XJwt ins = (XJwt) XJwt.getBean();
        Map<String, Object> map = new HashMap<>();
        map.put("abc", 123);
        String token = ins.getToken(map);

        XLog.debug(this.getClass(), token);
    }
}
