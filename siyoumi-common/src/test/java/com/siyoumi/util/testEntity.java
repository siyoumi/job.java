package com.siyoumi.util;

import com.siyoumi.entity.SysAccount;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.HashMap;

public class testEntity {
    @Test
    @DisplayName("静态方法")
    void test_100() throws IllegalAccessException {
        SysAccount acc = new SysAccount();
        acc.setAutoID();
        acc.setAcc_x_id("x");

        HashMap<String, Object> map = new HashMap<>();

        for (Field f : acc.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            Object v = f.get(acc);
            XLog.debug(this.getClass(), f.getName(), "=", v);
        }
    }



}
