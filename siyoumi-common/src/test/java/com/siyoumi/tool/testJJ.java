package com.siyoumi.tool;

import com.siyoumi.component.api.jj.JJ;
import com.siyoumi.component.api.jj.RobotType;
import com.siyoumi.component.api.jj.robot.RobotErr;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class testJJ {
    @Test
    @DisplayName("发送钉钉")
    void test_001() {
        RobotErr robot = new RobotErr();
        JJ jj = JJ.getIns(robot);
        XReturn r = jj.send("test_msg", "http://admin.siyoumi.com", false);
        XLog.debug(this.getClass(), r);
        Assert.isTrue(r.ok(), r.getErrMsg());
    }

    @Test
    @DisplayName("发送钉钉，反射调用")
    void test_002() {
        JJ jj = JJ.getIns(RobotErr.class);
        XReturn r = jj.send("test_msg", "", false);
        XLog.debug(this.getClass(), r);
        Assert.isTrue(r.ok(), r.getErrMsg());
    }

    @Test
    @DisplayName("发送钉钉，简单")
    void test_003() {
        JJ jj = JJ.getIns(RobotErr.class);
        XReturn r = jj.send("test_msg", "", false);
        XLog.debug(this.getClass(), r);
        Assert.isTrue(r.ok(), r.getErrMsg());
    }


    @Test
    @DisplayName("发送钉钉，类型调用")
    void test_004() {
        JJ jj = JJ.getIns(RobotType.MSG);
        XReturn r = jj.send("test_msg", "", false);
        XLog.debug(this.getClass(), r);
        Assert.isTrue(r.ok(), r.getErrMsg());
    }
}
