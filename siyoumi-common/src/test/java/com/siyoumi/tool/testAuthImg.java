package com.siyoumi.tool;

import com.siyoumi.component.XApp;
import com.siyoumi.util.XAuthImg;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class testAuthImg
{
    @Test
    void test() throws IOException
    {
        Integer rnd = XApp.random(1000, 9999);
        String securityCode = rnd + "";


        Integer codeLength = securityCode.length();//验证码长度
        Integer fontSize = 18;//字体大小
        int fontWidth = fontSize + 1;
        //图片宽高
        int width = codeLength * fontWidth + 6;
        int height = fontSize * 2 + 1;
        //图片
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();
        g.setColor(Color.WHITE);//设置背景色
        g.fillRect(0, 0, width, height);//填充背景
        g.setColor(Color.LIGHT_GRAY);//设置边框颜色
        g.setFont(new Font("Arial", Font.BOLD, height - 2));//边框字体样式
        g.drawRect(0, 0, width - 1, height - 1);//绘制边框
        //绘制噪点
        Random rand = new Random();
        g.setColor(Color.LIGHT_GRAY);
        for (int i = 0; i < codeLength * 6; i++)
        {
            int x = rand.nextInt(width);
            int y = rand.nextInt(height);
            g.drawRect(x, y, 1, 1);//绘制1*1大小的矩形
        }
        //绘制验证码
        int codeY = height - 10;
        g.setColor(new Color(19, 148, 246));
        //Georgia  是个字体，如果想用中文要指定中文字体；eg:"宋体";
        //g.setFont(new Font("Georgia", Font.BOLD, fontSize));
        g.setFont(new Font("宋体", Font.BOLD, fontSize));
        for (int i = 0; i < codeLength; i++)
        {
            double deg = new Random().nextDouble() * 20;
//            g.rotate(Math.toRadians(deg), i * 16 + 13, codeY - 7.5);
            g.drawString(String.valueOf(securityCode.charAt(i)), i * 16 + 5, codeY);
//            g.rotate(Math.toRadians(-deg), i * 16 + 13, codeY - 7.5);
        }
        g.dispose();//关闭资源

        //调用工具类
        File file = new File("H:\\_download\\1.png");
        ImageIO.write(image, "png", file);
    }

    @Test
    void test_100() throws IOException
    {
        BufferedImage img = XAuthImg.createAuthCode("78945");
        //调用工具类
        File file = new File("H:\\_download\\2.png");
        ImageIO.write(img, "png", file);
    }

}
