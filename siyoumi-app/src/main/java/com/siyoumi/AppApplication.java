package com.siyoumi;

import com.siyoumi.config.SysConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Properties;

/***
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │                 │
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │                 │+
 *   │       ─┴─       │
 *   │                 │
 *   └───┐         ┌───┘
 *       │         │
 *       │         │   + +
 *       │         │
 *       │         └──────────────┐
 *       │                        │
 *       │                        ├─┐
 *       │                        ┌─┘
 *       │                        │
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
@EnableFeignClients(basePackages = "com.siyoumi.app.feign")
@SpringBootApplication
@ConfigurationPropertiesScan("com.siyoumi.config")
public class AppApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(AppApplication.class);

        Properties props = new Properties();
        //取消mybatis取消1级缓存
        props.setProperty("mybatis-plus.configuration.cache-enabled", "false");
        props.setProperty("mybatis-plus.configuration.local-cache-scope", "STATEMENT");
        app.setDefaultProperties(props);

        if (SysConfig.getIns().isDev()) {
            //app.setHeadless(false);
            //app.setWebApplicationType(WebApplicationType.NONE);
        }

        ConfigurableApplicationContext context = app.run(args);

        //if (SysConfig.getIns().isDev()) {
        //    new PrintLogin();
        //}
    }
}

