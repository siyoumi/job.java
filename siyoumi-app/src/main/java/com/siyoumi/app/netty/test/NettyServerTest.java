package com.siyoumi.app.netty.test;

import com.siyoumi.app.netty.NettyWebSocketServer;
import com.siyoumi.app.netty.to.NettyConnectionData;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

//TCP服务端
@Slf4j
public class NettyServerTest
        extends NettyWebSocketServer {
    @SneakyThrows
    public static void main(String[] args) {
        NettyConnectionData data = NettyConnectionData.getIns();
        data.setPort(8800);
        data.setHandlerClazz(NettyServerHandlerTest.class);

        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup(8);
        try {
            //创建服务器端的引导对象
            ServerBootstrap bootstrap = new ServerBootstrap();
            //将两个线程组放进引导对象
            bootstrap.group(bossGroup, workerGroup)
                    //NioServerSocketChannel服务端channel
                    .channel(NioServerSocketChannel.class)
                    // 初始化服务器连接队列大小，服务端处理客户端连接请求是顺序处理的,所以同一时间只能处理一个客户端连接。
                    // 多个客户端同时来的时候,服务端将不能处理的客户端连接请求放在队列中等待处理
                    .option(ChannelOption.SO_BACKLOG, 1024)
                    .childHandler(new ChannelInitializer<SocketChannel>() {//创建通道初始化对象，设置初始化参数
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            //每一个链接过来(每一次创建channel)都会执行这个方法
                            log.debug("初始化pipeline");

                            //粘包处理
                            //1.
                            //ch.pipeline().addLast(new LengthFieldBasedFrameDecoder(1024, 0, 4, 0, 4));
                            //// LengthFieldPrepender是一个编码器，主要是在响应字节数据前面添加字节长度字段
                            //ch.pipeline().addLast(new LengthFieldPrepender(4));

                            //2.分割符
                            ByteBuf delimiter = Unpooled.copiedBuffer("$$".getBytes());
                            ch.pipeline().addLast(new DelimiterBasedFrameDecoder(1024, delimiter));

                            //StringDecoder要放到LengthFieldBasedFrameDecoder后面,对得到消息体的ByteBuf转码为String类型
                            //ch.pipeline().addLast("encode", new StringDecoder());
                            ch.pipeline().addLast(new StringEncoder());
                            ch.pipeline().addLast(new StringDecoder());

                            ch.pipeline().addLast(data.getHandler());
                            ch.pipeline().addLast(new NettyServerHandlerTest01());
                        }
                    });

            //log.info("TCP服务启动，ip={},port={},path={}", data.getIp(), data.getPort(), data.getPath());
            ChannelFuture cf = bootstrap.bind(data.getPort());
            //给cf注册监听器，监听我们关心的事件
            cf.addListener((ChannelFutureListener) future -> {
                if (cf.isSuccess()) {
                    log.info("TCP服务启动成功，ip={},port={},path={}", data.getIp(), data.getPort(), data.getPath());
                } else {
                    log.info("TCP服务启动失败，ip={},port={},path={}", data.getIp(), data.getPort(), data.getPath());
                }
            });
            cf.channel().closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}
