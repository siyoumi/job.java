package com.siyoumi.app.netty.to;

import com.siyoumi.component.XBean;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.Data;

//链接参数
@Data
public class NettyConnectionData {
    String ip = "0.0.0.0";
    Integer port; //8200
    String path; // /ws/login_qr
    Integer idleTimeSeconds = 30; //心跳检测，超时断开连接
    //业务处理
    Class<?> handlerClazz;


    public static NettyConnectionData getIns() {
        return new NettyConnectionData();
    }

    public static NettyConnectionData getIns(Integer port, String path, Class<?> handlerClazz) {
        NettyConnectionData data = new NettyConnectionData();
        data.setPort(port);
        data.setPath(path);
        data.setHandlerClazz(handlerClazz);
        return data;
    }

    public ChannelInboundHandlerAdapter getHandler() {
        return (ChannelInboundHandlerAdapter) XBean.newIns(getHandlerClazz());
    }
}
