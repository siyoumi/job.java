package com.siyoumi.app.netty.test;

import com.siyoumi.app.netty.NettyUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NettyServerHandlerTest
        extends SimpleChannelInboundHandler<WebSocketFrame> {
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.debug("{}----管道开启", ctx.channel().id());
        super.channelActive(ctx);

        //NettyUtil.sendText(ctx.channel(), "123");
        //NettyUtil.sendTextAndClose(ctx.channel(), "456");
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        log.debug("{}----管道关闭", ctx.channel().id());

        super.channelInactive(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        String msgX = (String) msg;
        log.debug("{}----{}", ctx.channel().id(), msgX);

        String x = "服务器收到了：" + msgX;
        //1.
        //ByteBuf buf = Unpooled.copiedBuffer(x, CharsetUtil.UTF_8);
        //ctx.writeAndFlush(buf);

        //2.分割符
        x += "$$";
        //NettyUtil.sendText(ctx.channel(), x);
        ByteBuf buf = Unpooled.copiedBuffer(x, CharsetUtil.UTF_8);
        ctx.writeAndFlush(buf);

        super.channelRead(ctx, msg);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame webSocketFrame) throws Exception {
        TextWebSocketFrame textWebSocketFrame = (TextWebSocketFrame) webSocketFrame;
        String msg = textWebSocketFrame.text();
        log.debug("{}----{}", ctx.channel().id(), msg);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state().equals(IdleState.WRITER_IDLE)) {
                log.debug("{}----发送心跳", ctx.channel().id().asShortText());
                NettyUtil.sendText(ctx.channel(), "ping");
            }
        }
        super.userEventTriggered(ctx, evt);
    }
}
