package com.siyoumi.app.netty;

import com.siyoumi.app.netty.entity.NettyMsg;
import com.siyoumi.app.netty.entity.NettyRoomUser;
import com.siyoumi.component.XRedis;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.AttributeKey;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
//netty 工具类
public class NettyUtil {
    static Map<String, Channel> mapChannel = new HashMap<>(); //记录所有连接

    public static AttributeKey<String> getAttrKey(String name) {
        AttributeKey<String> key;
        if (AttributeKey.exists(name)) {
            key = AttributeKey.valueOf(name);
        } else {
            key = AttributeKey.newInstance(name);
        }
        return key;
    }

    public static void setAttr(Channel channel, String key, String val) {
        channel.attr(NettyUtil.getAttrKey(key)).set(val);
    }

    public static String getAttr(Channel channel, String key) {
        return channel.attr(NettyUtil.getAttrKey(key)).get();
    }

    public static void setTokenData(Channel channel, XReturn r) {
        String tokenData = XStr.toJsonStr(r);
        log.debug("{}----token_data：{}", channel.id().asShortText(), tokenData);
        setAttr(channel, "token_data", tokenData);
    }

    public static XReturn getTokenData(Channel channel) {
        String tokenData = getAttr(channel, "token_data");
        return XJson.parseObject(tokenData, XReturn.class);
    }

    public static String getTokenData(Channel channel, String key) {
        XReturn r = getTokenData(channel);
        if (r == null) {
            return null;
        }
        return r.getData(key);
    }

    public static String getX(Channel channel) {
        return getTokenData(channel, "x");
    }

    public static String getOpenid(Channel channel) {
        return getTokenData(channel, "openid");
    }


    public static NettyRoomUser getRoomUser(Channel channel) {
        AttributeKey<String> key = NettyUtil.getAttrKey("room_user");
        String s = channel.attr(key).get();
        if (XStr.isNullOrEmpty(s)) {
            return new NettyRoomUser();
        }
        return XJson.parseObject(s, NettyRoomUser.class);
    }

    public static void sendText(Channel channel, NettyMsg msg) {
        sendText(channel, XJson.toJSONString(msg));
    }

    public static void sendText(Channel channel, String msg) {
        log.debug("{}----发送内容：{}", channel.id().asShortText(), msg);
        channel.writeAndFlush(new TextWebSocketFrame(msg));
    }

    public static void sendTextAndClose(Channel channel, NettyMsg msg) {
        sendTextAndClose(channel, XJson.toJSONString(msg));
    }

    public static void sendTextAndClose(Channel channel, String msg) {
        log.debug("{}----发送内容并关闭：{}", channel.id().asShortText(), msg);
        ChannelFuture future = channel.writeAndFlush(new TextWebSocketFrame(msg));
        future.addListener(ChannelFutureListener.CLOSE);
    }
}
