package com.siyoumi.app.netty.entity;

import com.siyoumi.util.IEnum;

//信息行为
public enum EnumNettyMsgAction
        implements IEnum {
    NULL(0, "无处理"),
    AUTH(400, "验证通知"),
    MSG(1, "发送通知"),
    CREATE_ROOM(10, "创建房间"),
    JOIN_ROOM(11, "加入房间"),
    ;


    private final Integer key;
    private final String val;

    EnumNettyMsgAction(Integer key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key.toString();
    }
}
