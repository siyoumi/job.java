package com.siyoumi.app.netty.entity;

public enum EnumNettyErr {
    OK(0, "成功"),
    MISS_ROOM_ID(30001, "miss room_id"),
    MISS_USER_ID(30002, "miss id"),
    MISS_EVENT(30003, "miss event"),
    ROOM_ID_ERR(30005, "房间号不存在"),
    CREATE_ROOM(30010, "已创建房间"),
    CREATE_JOIN_MAX(30011, "房间已满"),
    NOT_JOIN_ROOM(30012, "用户未加入房间"),
    USRE_JOINED_ROOM(30020, "用户已在房间中"),
    ROOM_ERR(30500, "用户已在房间中"),
    TIMEOUT(30501, "超时关闭通道"),
    AUTH_MISS_TOKEN(30550, "auth error"),
    AUTH_MISS_OPENID(30551, "miss openid"),
    ;
    private Integer errcode;
    private String errmsg;

    EnumNettyErr(Integer errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    public NettyMsg getR(String roomId) {
        return NettyMsg.getR(roomId, errcode, errmsg);
    }

    public NettyMsg getR(String roomId, String errmsg) {
        return NettyMsg.getR(roomId, errcode, errmsg);
    }

    public Integer getErrcode() {
        return errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }
}
