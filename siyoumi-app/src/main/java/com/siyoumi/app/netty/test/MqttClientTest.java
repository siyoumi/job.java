package com.siyoumi.app.netty.test;

import com.siyoumi.app.netty.NettyMqttUtil;
import com.siyoumi.component.XApp;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

//mqtt客户端
@Slf4j
public class MqttClientTest {
    @SneakyThrows
    public static void main(String[] args) {
        log.info("size: {}", NettyMqttUtil.getMapChannel().size());
        //MqttUtil.sendMessage();

        String topic = "test";

        String host = "tcp://192.168.0.186:8300";
        String clientID = XApp.getStrID();
        String username = "siyoumi";
        String password = "siyoumi-pwd";

        MqttClient client = new MqttClient(host, clientID, new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(true);
        options.setUserName(username);
        options.setPassword(password.toCharArray());
        options.setConnectionTimeout(10);
        options.setKeepAliveInterval(3);

        MqttCallback callback = new MqttCallback();
        callback.setClient(client);
        client.setCallback(callback);
        client.connect(options);

        //订阅主题
        client.subscribe(topic, 0);

        //发消息
        MqttMessage message = new MqttMessage();
        message.setQos(0);
        message.setRetained(false);
        message.setPayload("ping".getBytes());
        MqttTopic mqttTopic = client.getTopic(topic);
        if (null == mqttTopic) {
            log.error("topic not exist");
        }
        MqttDeliveryToken token = mqttTopic.publish(message);
        token.waitForCompletion();
    }
}


