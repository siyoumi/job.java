package com.siyoumi.app.netty.test;

import com.siyoumi.app.netty.NettyUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NettyClientHandlerTest
        extends SimpleChannelInboundHandler<WebSocketFrame> {
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.debug("{}----启动", ctx.channel().id());
        super.channelActive(ctx);

        //粘包处理
        //1.头部标记消息体长度所占字节数
        //for (int i = 0; i < 10; i++) {
        //    String msg = "HelloServer" + i;
        //    log.debug("{}----msg:{}", ctx.channel().id(), msg);
        //    ByteBuf buf = Unpooled.copiedBuffer(msg, CharsetUtil.UTF_8);
        //    ctx.writeAndFlush(buf);
        //}

        //2.分割符
        for (int i = 0; i < 10; i++) {
            String msg = "HelloServer" + i + "$$";
            log.debug("{}----msg:{}", ctx.channel().id(), msg);
            ByteBuf buf = Unpooled.copiedBuffer(msg, CharsetUtil.UTF_8);
            ctx.writeAndFlush(buf);
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        log.debug("{}----msg:{}", ctx.channel().id(), msg);

        super.channelRead(ctx, msg);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame webSocketFrame) throws Exception {
        TextWebSocketFrame textWebSocketFrame = (TextWebSocketFrame) webSocketFrame;
        String msg = textWebSocketFrame.text();
        log.debug("{}----{}", ctx.channel().id(), msg);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state().equals(IdleState.WRITER_IDLE)) {
                log.debug("{}----发送心跳", ctx.channel().id().asShortText());
                NettyUtil.sendText(ctx.channel(), "ping");
            }
        }
        super.userEventTriggered(ctx, evt);
    }
}
