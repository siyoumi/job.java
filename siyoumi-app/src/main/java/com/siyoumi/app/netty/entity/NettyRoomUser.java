package com.siyoumi.app.netty.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

//房间用户信息
@Slf4j
@Data
public class NettyRoomUser {
    String id; //openid
    String x;
    String roomId;
    Boolean roomMaster = false;
}
