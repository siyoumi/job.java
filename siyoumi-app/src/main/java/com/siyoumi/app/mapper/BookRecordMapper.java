package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysBookRecord;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_record
@Component
public interface BookRecordMapper
        extends MapperBase<SysBookRecord>
{
}
