package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.ActDataLaunch;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_act_data_launch
@Component
public interface ActDataLaunchMapper
        extends MapperBase<ActDataLaunch>
{
}
