package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2Group;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_group
@Component
public interface M2GroupMapper
        extends MapperBase<M2Group>
{
}
