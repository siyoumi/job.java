package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FksFile;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fks_file
@Component
public interface FksFileMapper
        extends MapperBase<FksFile>
{
}
