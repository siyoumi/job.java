package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookStoreGroup;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_store_group
@Component
public interface BookStoreGroupMapper
        extends MapperBase<BookStoreGroup>
{
}
