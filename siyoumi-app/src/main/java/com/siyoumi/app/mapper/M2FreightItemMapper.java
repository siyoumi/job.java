package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2FreightItem;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_freight_item
@Component
public interface M2FreightItemMapper
        extends MapperBase<M2FreightItem>
{
}
