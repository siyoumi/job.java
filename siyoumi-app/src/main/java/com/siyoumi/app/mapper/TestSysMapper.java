package com.siyoumi.app.mapper;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;


//test
@Component
public interface TestSysMapper {
    @Select("select sleep(2) s")
    Long sleep();
}
