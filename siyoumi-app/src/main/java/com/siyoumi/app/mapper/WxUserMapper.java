package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.WxUser;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_wx_user
@Component
public interface WxUserMapper
        extends MapperBase<WxUser>
{
}
