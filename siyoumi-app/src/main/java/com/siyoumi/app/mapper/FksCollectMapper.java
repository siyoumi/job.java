package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FksCollect;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fks_collect
@Component
public interface FksCollectMapper
        extends MapperBase<FksCollect>
{
}
