package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.ActDataChou;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_act_data_chou
@Component
public interface ActDataChouMapper
        extends MapperBase<ActDataChou>
{
}
