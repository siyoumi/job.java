package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysMoneyOption;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_money_option
@Component
public interface SysMoneyOptionMapper
        extends MapperBase<SysMoneyOption>
{
}
