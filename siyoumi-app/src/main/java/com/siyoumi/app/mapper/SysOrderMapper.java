package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysOrder;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_order
@Component
public interface SysOrderMapper
        extends MapperBase<SysOrder>
{
}
