package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssQuestion;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_question
@Component
public interface EssQuestionMapper
        extends MapperBase<EssQuestion>
{
}
