package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssClass;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_class
@Component
public interface EssClassMapper
        extends MapperBase<EssClass>
{
}
