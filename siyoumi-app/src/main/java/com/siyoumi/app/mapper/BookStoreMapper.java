package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookStore;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_store
@Component
public interface BookStoreMapper
        extends MapperBase<BookStore>
{
}
