package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_spu
@Component
public interface BookSpuMapper
        extends MapperBase<BookSpu>
{
}
