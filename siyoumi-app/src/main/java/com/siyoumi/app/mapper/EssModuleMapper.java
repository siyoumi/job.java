package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssModule;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_module
@Component
public interface EssModuleMapper
        extends MapperBase<EssModule>
{
}
