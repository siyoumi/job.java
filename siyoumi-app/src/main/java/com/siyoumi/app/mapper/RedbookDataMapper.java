package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.RedbookData;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_redbook_data
@Component
public interface RedbookDataMapper
        extends MapperBase<RedbookData>
{
}
