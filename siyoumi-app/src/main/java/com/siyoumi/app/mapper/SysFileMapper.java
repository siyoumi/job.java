package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysFile;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_file
@Component
public interface SysFileMapper
        extends MapperBase<SysFile>
{
}
