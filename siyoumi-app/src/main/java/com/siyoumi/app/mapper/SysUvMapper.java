package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysUv;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_uv
@Component
public interface SysUvMapper
        extends MapperBase<SysUv>
{
}
