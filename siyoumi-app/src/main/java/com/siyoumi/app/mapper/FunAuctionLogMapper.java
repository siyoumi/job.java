package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FunAuctionLog;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fun_auction_log
@Component
public interface FunAuctionLogMapper
        extends MapperBase<FunAuctionLog>
{
}
