package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssStudyRecord;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_study_record
@Component
public interface EssStudyRecordMapper
        extends MapperBase<EssStudyRecord>
{
}
