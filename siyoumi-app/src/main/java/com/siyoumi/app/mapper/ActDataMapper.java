package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.ActData;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_act_data
@Component
public interface ActDataMapper
        extends MapperBase<ActData>
{
}
