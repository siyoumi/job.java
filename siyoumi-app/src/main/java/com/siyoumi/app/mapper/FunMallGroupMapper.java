package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FunMallGroup;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fun_mall_group
@Component
public interface FunMallGroupMapper
        extends MapperBase<FunMallGroup>
{
}
