package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookSetDay;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_set_day
@Component
public interface BookSetDayMapper
        extends MapperBase<BookSetDay>
{
}
