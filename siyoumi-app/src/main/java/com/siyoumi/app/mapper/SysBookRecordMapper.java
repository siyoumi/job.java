package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysBookRecord;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_book_record
@Component
public interface SysBookRecordMapper
        extends MapperBase<SysBookRecord>
{
}
