package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysSheetRecord;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_a_sheet_record
@Component
public interface SysSheetRecordMapper
        extends MapperBase<SysSheetRecord>
{
}
