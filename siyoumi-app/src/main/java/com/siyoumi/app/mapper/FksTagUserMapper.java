package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FksTagUser;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fks_tag_user
@Component
public interface FksTagUserMapper
        extends MapperBase<FksTagUser>
{
}
