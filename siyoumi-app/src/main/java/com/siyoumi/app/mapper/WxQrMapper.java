package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.WxQr;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_wx_qr
@Component
public interface WxQrMapper
        extends MapperBase<WxQr>
{
}
