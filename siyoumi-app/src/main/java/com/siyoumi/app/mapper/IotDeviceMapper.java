package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.IotDevice;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_iot_device
@Component
public interface IotDeviceMapper
        extends MapperBase<IotDevice>
{
}
