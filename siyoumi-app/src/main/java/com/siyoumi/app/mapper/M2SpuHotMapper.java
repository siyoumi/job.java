package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2SpuHot;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_spu_hot
@Component
public interface M2SpuHotMapper
        extends MapperBase<M2SpuHot>
{
}
