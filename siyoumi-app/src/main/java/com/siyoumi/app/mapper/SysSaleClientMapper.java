package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysSaleClient;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_sale_client
@Component
public interface SysSaleClientMapper
        extends MapperBase<SysSaleClient>
{
}
