package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysMaster;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_master
@Component
public interface SysMasterMapper
        extends MapperBase<SysMaster>
{
}
