package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookRefundOrder;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_refund_order
@Component
public interface BookRefundOrderMapper
        extends MapperBase<BookRefundOrder>
{
}
