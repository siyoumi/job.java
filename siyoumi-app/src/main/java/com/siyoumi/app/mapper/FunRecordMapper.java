package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FunRecord;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fun_record
@Component
public interface FunRecordMapper
        extends MapperBase<FunRecord>
{
}
