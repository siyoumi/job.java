package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.WxUserInfo;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_wx_user_a_info
@Component
public interface WxUserInfoMapper
        extends MapperBase<WxUserInfo>
{
}
