package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookSpuSet;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_spu_set
@Component
public interface BookSpuSetMapper
        extends MapperBase<BookSpuSet>
{
}
