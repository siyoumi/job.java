package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FunWin;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fun_win
@Component
public interface FunWinMapper
        extends MapperBase<FunWin>
{
}
