package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysSale;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_sale
@Component
public interface SysSaleMapper
        extends MapperBase<SysSale>
{
}
