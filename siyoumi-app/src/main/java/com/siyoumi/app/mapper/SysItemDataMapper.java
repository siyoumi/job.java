package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysItemData;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_item_data
@Component
public interface SysItemDataMapper
        extends MapperBase<SysItemData>
{
}
