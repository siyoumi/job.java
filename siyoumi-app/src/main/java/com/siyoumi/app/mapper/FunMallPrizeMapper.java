package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FunMallPrize;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fun_mall_prize
@Component
public interface FunMallPrizeMapper
        extends MapperBase<FunMallPrize>
{
}
