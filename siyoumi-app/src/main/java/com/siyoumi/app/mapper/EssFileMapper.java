package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssFile;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_file
@Component
public interface EssFileMapper
        extends MapperBase<EssFile>
{
}
