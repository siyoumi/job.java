package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FunWinPut;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fun_win_put
@Component
public interface FunWinPutMapper
        extends MapperBase<FunWinPut>
{
}
