package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssStudyStudent;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_study_student
@Component
public interface EssStudyStudentMapper
        extends MapperBase<EssStudyStudent>
{
}
