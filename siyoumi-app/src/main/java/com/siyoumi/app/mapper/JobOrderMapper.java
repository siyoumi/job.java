package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.JobOrder;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_job_order
@Component
public interface JobOrderMapper
        extends MapperBase<JobOrder>
{
}
