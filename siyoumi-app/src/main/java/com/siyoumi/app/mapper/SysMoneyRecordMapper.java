package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysMoneyRecord;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_money_record
@Component
public interface SysMoneyRecordMapper
        extends MapperBase<SysMoneyRecord>
{
}
