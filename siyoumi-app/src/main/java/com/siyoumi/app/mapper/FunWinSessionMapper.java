package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FunWinSession;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fun_win_session
@Component
public interface FunWinSessionMapper
        extends MapperBase<FunWinSession>
{
}
