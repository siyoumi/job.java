package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.WxTmplmsg;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_wx_tmplmsg
@Component
public interface WxTmplmsgMapper
        extends MapperBase<WxTmplmsg>
{
}
