package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_prize_set
@Component
public interface SysPrizeSetMapper
        extends MapperBase<SysPrizeSet>
{
}
