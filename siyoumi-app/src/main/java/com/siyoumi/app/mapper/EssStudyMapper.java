package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssStudy;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_study
@Component
public interface EssStudyMapper
        extends MapperBase<EssStudy>
{
}
