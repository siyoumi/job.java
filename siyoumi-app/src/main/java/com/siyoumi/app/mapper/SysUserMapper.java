package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysUser;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_user
@Component
public interface SysUserMapper
        extends MapperBase<SysUser>
{
}
