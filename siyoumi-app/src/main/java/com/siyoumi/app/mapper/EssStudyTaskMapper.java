package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssStudyTask;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_study_task
@Component
public interface EssStudyTaskMapper
        extends MapperBase<EssStudyTask>
{
}
