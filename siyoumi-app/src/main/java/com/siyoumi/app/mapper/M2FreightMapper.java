package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2Freight;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_freight
@Component
public interface M2FreightMapper
        extends MapperBase<M2Freight>
{
}
