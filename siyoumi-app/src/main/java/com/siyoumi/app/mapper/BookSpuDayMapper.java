package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookSpuDay;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_spu_day
@Component
public interface BookSpuDayMapper
        extends MapperBase<BookSpuDay>
{
}
