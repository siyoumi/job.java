package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.LnumWin;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_lnum_win
@Component
public interface LnumWinMapper
        extends MapperBase<LnumWin>
{
}
