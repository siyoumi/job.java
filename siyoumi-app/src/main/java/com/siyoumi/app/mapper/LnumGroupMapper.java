package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.LnumGroup;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_lnum_group
@Component
public interface LnumGroupMapper
        extends MapperBase<LnumGroup>
{
}
