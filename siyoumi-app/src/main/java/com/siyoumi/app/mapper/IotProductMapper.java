package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.IotProduct;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_iot_product
@Component
public interface IotProductMapper
        extends MapperBase<IotProduct>
{
}
