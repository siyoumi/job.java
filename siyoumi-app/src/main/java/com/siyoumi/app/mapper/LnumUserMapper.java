package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.LnumUser;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_lnum_user
@Component
public interface LnumUserMapper
        extends MapperBase<LnumUser>
{
}
