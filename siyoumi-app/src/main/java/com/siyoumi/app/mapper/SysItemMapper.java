package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysItem;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_item
@Component
public interface SysItemMapper
        extends MapperBase<SysItem>
{
}
