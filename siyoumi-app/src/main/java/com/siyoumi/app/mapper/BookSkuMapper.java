package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookSku;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_sku
@Component
public interface BookSkuMapper
        extends MapperBase<BookSku>
{
}
