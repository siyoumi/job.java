package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.LnumSession;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_lnum_session
@Component
public interface LnumSessionMapper
        extends MapperBase<LnumSession>
{
}
