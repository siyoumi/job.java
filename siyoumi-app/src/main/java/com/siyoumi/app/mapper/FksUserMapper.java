package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FksUser;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fks_user
@Component
public interface FksUserMapper
        extends MapperBase<FksUser>
{
}
