package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.ActSet;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_act_set
@Component
public interface ActSetMapper
        extends MapperBase<ActSet>
{
}
