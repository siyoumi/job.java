package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysSheet;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_a_sheet
@Component
public interface SysSheetMapper
        extends MapperBase<SysSheet>
{
}
