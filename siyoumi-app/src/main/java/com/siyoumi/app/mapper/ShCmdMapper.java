package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.ShCmd;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sh_cmd
@Component
public interface ShCmdMapper
        extends MapperBase<ShCmd>
{
}
