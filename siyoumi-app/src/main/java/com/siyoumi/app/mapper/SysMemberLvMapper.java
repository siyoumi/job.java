package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysMemberLv;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_member_lv
@Component
public interface SysMemberLvMapper
        extends MapperBase<SysMemberLv>
{
}
