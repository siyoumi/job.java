package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.WxSubscribemsg;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_wx_subscribemsg
@Component
public interface WxSubscribemsgMapper
        extends MapperBase<WxSubscribemsg>
{
}
