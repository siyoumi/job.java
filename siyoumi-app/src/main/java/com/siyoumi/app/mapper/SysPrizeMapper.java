package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysPrize;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_prize
@Component
public interface SysPrizeMapper
        extends MapperBase<SysPrize>
{
}
