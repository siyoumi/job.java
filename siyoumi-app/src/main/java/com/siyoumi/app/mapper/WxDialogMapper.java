package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.WxDialog;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_wx_dialog
@Component
public interface WxDialogMapper
        extends MapperBase<WxDialog>
{
}
