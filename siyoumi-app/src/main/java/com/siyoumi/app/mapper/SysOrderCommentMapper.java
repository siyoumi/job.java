package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysOrderComment;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_order_comment
@Component
public interface SysOrderCommentMapper
        extends MapperBase<SysOrderComment>
{
}
