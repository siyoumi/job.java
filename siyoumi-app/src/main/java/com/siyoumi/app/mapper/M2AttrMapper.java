package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2Attr;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_attr
@Component
public interface M2AttrMapper
        extends MapperBase<M2Attr>
{
}
