package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2Store;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_store
@Component
public interface M2StoreMapper
        extends MapperBase<M2Store>
{
}
