package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookStoreTxt;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_store_txt
@Component
public interface BookStoreTxtMapper
        extends MapperBase<BookStoreTxt>
{
}
