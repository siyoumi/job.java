package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookItemZzz;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_item_zzz
@Component
public interface BookItemZzzMapper
        extends MapperBase<BookItemZzz>
{
}
