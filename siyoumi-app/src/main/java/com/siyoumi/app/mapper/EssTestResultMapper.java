package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssTestResult;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_test_result
@Component
public interface EssTestResultMapper
        extends MapperBase<EssTestResult>
{
}
