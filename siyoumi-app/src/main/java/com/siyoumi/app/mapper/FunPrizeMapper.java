package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FunPrize;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fun_prize
@Component
public interface FunPrizeMapper
        extends MapperBase<FunPrize>
{
}
