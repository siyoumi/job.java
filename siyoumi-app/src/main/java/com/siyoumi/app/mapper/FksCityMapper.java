package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FksCity;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fks_city
@Component
public interface FksCityMapper
        extends MapperBase<FksCity>
{
}
