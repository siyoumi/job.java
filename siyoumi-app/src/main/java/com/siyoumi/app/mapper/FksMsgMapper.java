package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FksMsg;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fks_msg
@Component
public interface FksMsgMapper
        extends MapperBase<FksMsg>
{
}
