package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssFileType;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_file_type
@Component
public interface EssFileTypeMapper
        extends MapperBase<EssFileType>
{
}
