package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysPrizeSetChou;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_prize_set_chou
@Component
public interface SysPrizeSetChouMapper
        extends MapperBase<SysPrizeSetChou>
{
}
