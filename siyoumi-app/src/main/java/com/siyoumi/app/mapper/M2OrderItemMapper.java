package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2OrderItem;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_order_item
@Component
public interface M2OrderItemMapper
        extends MapperBase<M2OrderItem>
{
}
