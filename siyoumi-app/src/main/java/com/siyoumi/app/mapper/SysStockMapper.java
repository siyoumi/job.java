package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysStock;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_stock
@Component
public interface SysStockMapper
        extends MapperBase<SysStock>
{
}
