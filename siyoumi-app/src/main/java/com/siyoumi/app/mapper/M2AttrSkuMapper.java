package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2AttrSku;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_attr_sku
@Component
public interface M2AttrSkuMapper
        extends MapperBase<M2AttrSku>
{
}
