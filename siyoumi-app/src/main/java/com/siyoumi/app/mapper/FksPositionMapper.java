package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FksPosition;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fks_position
@Component
public interface FksPositionMapper
        extends MapperBase<FksPosition>
{
}
