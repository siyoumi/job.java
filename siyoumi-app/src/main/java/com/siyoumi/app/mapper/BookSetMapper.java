package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookSet;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_set
@Component
public interface BookSetMapper
        extends MapperBase<BookSet>
{
}
