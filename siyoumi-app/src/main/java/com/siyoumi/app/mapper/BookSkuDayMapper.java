package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysBookSkuDay;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_sku_day
@Component
public interface BookSkuDayMapper
        extends MapperBase<SysBookSkuDay>
{
}
