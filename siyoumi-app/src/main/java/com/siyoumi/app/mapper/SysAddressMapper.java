package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysAddress;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_address
@Component
public interface SysAddressMapper
        extends MapperBase<SysAddress>
{
}
