package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookSpuTxt;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_spu_txt
@Component
public interface BookSpuTxtMapper
        extends MapperBase<BookSpuTxt>
{
}
