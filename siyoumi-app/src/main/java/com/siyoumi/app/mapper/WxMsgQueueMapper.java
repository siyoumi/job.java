package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.WxMsgQueue;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_wx_msg_queue
@Component
public interface WxMsgQueueMapper
        extends MapperBase<WxMsgQueue>
{
}
