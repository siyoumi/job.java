package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookItem;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_item
@Component
public interface BookItemMapper
        extends MapperBase<BookItem>
{
}
