package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.mybatispuls.MapperBase;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;


//t_s_abc
public interface SysAbcMapper
        extends MapperBase<SysAbc>
{
    @Select("SELECT * FROM wx_app.t_s_abc WHERE abc_id = {#id} AND abc_table = {#table}")
    SysAbc getByTable(@Param("id") String id, @Param("table") String table);
}
