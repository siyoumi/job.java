package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysSaleRecord;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_sale_record
@Component
public interface SysSaleRecordMapper
        extends MapperBase<SysSaleRecord>
{
}
