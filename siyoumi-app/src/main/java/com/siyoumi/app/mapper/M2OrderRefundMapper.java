package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2OrderRefund;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_order_refund
@Component
public interface M2OrderRefundMapper
        extends MapperBase<M2OrderRefund>
{
}
