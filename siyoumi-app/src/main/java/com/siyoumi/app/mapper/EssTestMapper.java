package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssTest;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_test
@Component
public interface EssTestMapper
        extends MapperBase<EssTest>
{
}
