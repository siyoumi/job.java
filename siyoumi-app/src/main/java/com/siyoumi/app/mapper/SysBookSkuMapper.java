package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysBookSku;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_book_sku
@Component
public interface SysBookSkuMapper
        extends MapperBase<SysBookSku>
{
}
