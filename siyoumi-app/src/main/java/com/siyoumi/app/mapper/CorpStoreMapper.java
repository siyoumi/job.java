package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.CorpStore;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_corp_store
@Component
public interface CorpStoreMapper
        extends MapperBase<CorpStore>
{
}
