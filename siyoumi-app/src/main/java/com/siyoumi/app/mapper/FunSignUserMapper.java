package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FunSignUser;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fun_sign_user
@Component
public interface FunSignUserMapper
        extends MapperBase<FunSignUser>
{
}
