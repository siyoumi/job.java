package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssUser;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_user
@Component
public interface EssUserMapper
        extends MapperBase<EssUser>
{
}
