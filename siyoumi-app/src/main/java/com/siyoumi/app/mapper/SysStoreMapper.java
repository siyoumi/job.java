package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysStore;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_store
@Component
public interface SysStoreMapper
        extends MapperBase<SysStore>
{
}
