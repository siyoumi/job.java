package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.RedbookQuan;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_redbook_quan
@Component
public interface RedbookQuanMapper
        extends MapperBase<RedbookQuan>
{
}
