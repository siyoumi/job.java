package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysRefundOrder;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_refund_order
@Component
public interface SysRefundOrderMapper
        extends MapperBase<SysRefundOrder>
{
}
