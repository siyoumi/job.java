package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FunAuctionSet;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fun_auction_set
@Component
public interface FunAuctionSetMapper
        extends MapperBase<FunAuctionSet>
{
}
