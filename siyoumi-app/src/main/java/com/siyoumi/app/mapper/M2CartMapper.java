package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2Cart;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_cart
@Component
public interface M2CartMapper
        extends MapperBase<M2Cart>
{
}
