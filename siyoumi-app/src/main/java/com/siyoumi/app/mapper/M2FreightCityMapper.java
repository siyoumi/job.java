package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2FreightCity;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_freight_city
@Component
public interface M2FreightCityMapper
        extends MapperBase<M2FreightCity>
{
}
