package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.RedbookComment;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_redbook_comment
@Component
public interface RedbookCommentMapper
        extends MapperBase<RedbookComment>
{
}
