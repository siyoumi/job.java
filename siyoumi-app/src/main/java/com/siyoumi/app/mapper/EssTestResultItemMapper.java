package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssTestResultItem;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_test_result_item
@Component
public interface EssTestResultItemMapper
        extends MapperBase<EssTestResultItem>
{
}
