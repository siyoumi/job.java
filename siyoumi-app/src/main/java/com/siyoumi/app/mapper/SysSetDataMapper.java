package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysSetData;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_set_data
@Component
public interface SysSetDataMapper
        extends MapperBase<SysSetData>
{
}
