package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2Spu;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_spu
@Component
public interface M2SpuMapper
        extends MapperBase<M2Spu>
{
}
