package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysSet;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_set
@Component
public interface SysSetMapper
        extends MapperBase<SysSet>
{
}
