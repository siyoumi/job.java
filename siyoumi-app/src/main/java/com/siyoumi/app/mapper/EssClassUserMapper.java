package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssClassUser;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_class_user
@Component
public interface EssClassUserMapper
        extends MapperBase<EssClassUser>
{
}
