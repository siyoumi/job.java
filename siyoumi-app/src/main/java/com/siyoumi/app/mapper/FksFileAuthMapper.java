package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FksFileAuth;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fks_file_auth
@Component
public interface FksFileAuthMapper
        extends MapperBase<FksFileAuth>
{
}
