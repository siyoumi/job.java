package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.ShUser;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sh_user
@Component
public interface ShUserMapper
        extends MapperBase<ShUser>
{
}
