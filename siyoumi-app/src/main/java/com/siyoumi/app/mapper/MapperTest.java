package com.siyoumi.app.mapper;

import com.siyoumi.component.XSpringContext;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.Map;


//test
@Component
public interface MapperTest {
    static public MapperTest getBean() {
        return XSpringContext.getBean(MapperTest.class);
    }

    @Select("call wx_app.test(#{num})")
    Integer test(@Param("num") Integer num);
}
