package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssTestStudent;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_test_student
@Component
public interface EssTestStudentMapper
        extends MapperBase<EssTestStudent>
{
}
