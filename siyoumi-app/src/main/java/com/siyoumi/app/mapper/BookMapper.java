package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysBook;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book
@Component
public interface BookMapper
        extends MapperBase<SysBook>
{
}
