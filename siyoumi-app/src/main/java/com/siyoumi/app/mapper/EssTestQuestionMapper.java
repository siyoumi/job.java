package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.EssTestQuestion;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_ess_test_question
@Component
public interface EssTestQuestionMapper
        extends MapperBase<EssTestQuestion>
{
}
