package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysDepartment;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_sys_department
@Component
public interface SysDepartmentMapper
        extends MapperBase<SysDepartment>
{
}
