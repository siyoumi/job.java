package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.LnumSessionPrize;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_lnum_session_prize
@Component
public interface LnumSessionPrizeMapper
        extends MapperBase<LnumSessionPrize>
{
}
