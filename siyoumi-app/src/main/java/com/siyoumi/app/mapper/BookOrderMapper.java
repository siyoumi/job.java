package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_book_order
@Component
public interface BookOrderMapper
        extends MapperBase<BookOrder>
{
}
