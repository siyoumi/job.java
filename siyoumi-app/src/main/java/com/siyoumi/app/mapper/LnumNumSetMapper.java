package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.LnumNumSet;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_lnum_num_set
@Component
public interface LnumNumSetMapper
        extends MapperBase<LnumNumSet>
{
}
