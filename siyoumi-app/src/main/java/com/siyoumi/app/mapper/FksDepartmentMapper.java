package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FksDepartment;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fks_department
@Component
public interface FksDepartmentMapper
        extends MapperBase<FksDepartment>
{
}
