package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.SysCode;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_s_code
@Component
public interface SysCodeMapper
        extends MapperBase<SysCode>
{
}
