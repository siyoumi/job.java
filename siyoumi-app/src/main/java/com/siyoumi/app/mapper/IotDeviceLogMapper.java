package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.IotDeviceLog;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_iot_device_log
@Component
public interface IotDeviceLogMapper
        extends MapperBase<IotDeviceLog>
{
}
