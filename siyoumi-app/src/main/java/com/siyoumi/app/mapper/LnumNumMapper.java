package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.LnumNum;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_lnum_num
@Component
public interface LnumNumMapper
        extends MapperBase<LnumNum>
{
}
