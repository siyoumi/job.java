package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2Sku;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_sku
@Component
public interface M2SkuMapper
        extends MapperBase<M2Sku>
{
}
