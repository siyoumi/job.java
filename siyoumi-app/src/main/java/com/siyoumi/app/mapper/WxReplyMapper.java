package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.WxReply;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_wx_reply
@Component
public interface WxReplyMapper
        extends MapperBase<WxReply>
{
}
