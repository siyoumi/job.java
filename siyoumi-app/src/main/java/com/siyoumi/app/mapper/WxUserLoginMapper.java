package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.WxUserLogin;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_wx_user_login
@Component
public interface WxUserLoginMapper
        extends MapperBase<WxUserLogin>
{
}
