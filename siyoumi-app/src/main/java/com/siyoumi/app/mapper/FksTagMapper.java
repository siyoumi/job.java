package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.FksTag;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_fks_tag
@Component
public interface FksTagMapper
        extends MapperBase<FksTag>
{
}
