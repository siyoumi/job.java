package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.M2Order;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_m2_order
@Component
public interface M2OrderMapper
        extends MapperBase<M2Order>
{
}
