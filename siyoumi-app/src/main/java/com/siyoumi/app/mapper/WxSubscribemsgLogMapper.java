package com.siyoumi.app.mapper;

import com.siyoumi.app.entity.WxSubscribemsgLog;
import com.siyoumi.mybatispuls.MapperBase;
import org.springframework.stereotype.Component;


//t_wx_subscribemsg_log
@Component
public interface WxSubscribemsgLogMapper
        extends MapperBase<WxSubscribemsgLog>
{
}
