package com.siyoumi.app.modules.lucky_num.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//随机开奖参数-子项
@Data
public class LNumChouWinItem {
    @HasAnyText
    String prize_id;
    @HasAnyText
    Long num;

    String prize_name;
}
