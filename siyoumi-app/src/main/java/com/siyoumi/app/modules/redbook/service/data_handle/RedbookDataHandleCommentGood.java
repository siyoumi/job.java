package com.siyoumi.app.modules.redbook.service.data_handle;

import com.siyoumi.app.entity.RedbookComment;
import com.siyoumi.app.modules.redbook.service.RedbookDataHandle;
import com.siyoumi.app.modules.redbook.service.SvcRedbookComment;
import com.siyoumi.app.modules.redbook.service.SvcRedbookData;
import com.siyoumi.app.modules.redbook.vo.RedbookSetData;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;

/**
 * 评论点赞
 */
public class RedbookDataHandleCommentGood
        extends RedbookDataHandle {
    @Override
    protected XReturn handleCan(RedbookSetData vo) {
        RedbookComment entityComment = SvcRedbookComment.getApp().getEntity(vo.getRdata_id_src());
        if (entityComment == null) {
            return EnumSys.ERR_VAL.getR("评论ID异常");
        }

        vo.setRdata_quan_id(entityComment.getRbc_rquan_id());
        return EnumSys.OK.getR();
    }

    @Override
    public XReturn handleAfter(RedbookSetData vo) {
        //更新点赞 收藏数量
        Long count = SvcRedbookData.getBean().getCount(vo.getRdata_type(), vo.getRdata_id_src(), vo.getRdata_uid(), null, null);

        RedbookComment entityComment = SvcRedbookComment.getApp().getEntity(vo.getRdata_id_src());
        RedbookComment entityCommentUpdate = new RedbookComment();
        entityCommentUpdate.setRbc_id(entityComment.getKey());
        entityCommentUpdate.setRbc_good_count(count);
        SvcRedbookComment.getApp().saveOrUpdatePassEqualField(entityComment, entityCommentUpdate);

        return EnumSys.OK.getR();
    }
}
