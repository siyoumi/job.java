package com.siyoumi.app.modules.app_ess.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.EssModule;
import com.siyoumi.app.modules.app_ess.entity.EnumEssModuleEnable;
import com.siyoumi.app.modules.app_ess.service.SvcEssModule;
import com.siyoumi.app.modules.app_ess.vo.VoEssModuleAudit;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__module__list")
public class app_ess__module__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("景场列表");

        InputData inputData = InputData.fromRequest();

        JoinWrapperPlus<EssModule> query = SvcEssModule.getBean().listQuery(inputData);

        IPage<EssModule> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcEssModule.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumEssModuleEnable enumModuleEnable = XEnumBase.of(EnumEssModuleEnable.class);
        for (Map<String, Object> item : list) {
            EssModule entity = XBean.fromMap(item, EssModule.class);
            item.put("id", entity.getKey());
            //状态
            item.put("enable", enumModuleEnable.get(entity.getEmod_enable()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcEssModule.getBean().delete(getIds());
    }

    //审核
    @PostMapping("/audit")
    public XReturn audit(@Validated VoEssModuleAudit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        return SvcEssModule.getBean().audit(vo);
    }
}
