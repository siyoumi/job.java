package com.siyoumi.app.modules.mall2.entity;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaM2Group {
    @HasAnyText
    @Size(max = 50)
    String mgroup_name;
    String mgroup_acc_id;
    Integer mgroup_order;
}
