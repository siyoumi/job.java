package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;


//评测添加题目
@Data
public class VoEssTestBatchAddQuestion {
    @HasAnyText(message = "缺少评测ID")
    String test_id;
    @HasAnyText
    @EqualsValues(vals = {"0", "1"}, message = "参数有误，0、1")
    Integer type = 0; //【0】自动选题；【1】手动选题
    List<VoEssTestBatchAddQuestionAuto> list_auto; //自动题目

    List<VoEssTestBatchAddQuestionHand> list_hand; //手动题目
}
