package com.siyoumi.app.modules.app_book.admin;

import com.siyoumi.app.entity.BookSku;
import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.modules.app_book.service.SvcBookSku;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.modules.app_book.vo.VaBookSku;
import com.siyoumi.app.modules.app_book.vo.VaBookSpu;
import com.siyoumi.component.XApp;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__sku__edit")
public class app_book__sku__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("价格-编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("bsku_spu_id", input("spu_id"));
        data.put("bsku_day", 0);
        data.put("bsku_day_price", BigDecimal.ZERO);
        if (isAdminEdit()) {
            BookSku entity = SvcBookSku.getApp().loadEntity(getID());
            //
            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaBookSku vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        BookSpu entitySpu = SvcBookSpu.getApp().getEntity(vo.getBsku_spu_id());
        XValidator.err(SvcBookSpu.getBean().canEdit(entitySpu));
        //
        if (vo.getBsku_day_price().compareTo(BigDecimal.ZERO) <= 0) {
            result.addError(XValidator.getErr("bsku_day_price", "价格不能小于0"));
        }
        {
            JoinWrapperPlus<BookSku> query = SvcBookSku.getBean().listQuery(vo.getBsku_spu_id());
            query.eq("bsku_day", vo.getBsku_day());
            if (isAdminEdit()) {
                query.ne("bsku_id", getID());
            }
            BookSku entitySku = SvcBookSku.getApp().first(query);
            if (entitySku != null) {
                result.addError(XValidator.getErr("bsku_day", "天数存在"));
            }
        }
        XValidator.getResult(result);
        //
        InputData inputData = InputData.fromRequest();
        if (isAdminAdd()) {
            vo.setBsku_store_id(getStoreId());
        }
        //
        return SvcBookSku.getBean().save(inputData, vo);
    }
}