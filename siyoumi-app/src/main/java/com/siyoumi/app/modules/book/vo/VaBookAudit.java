package com.siyoumi.app.modules.book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//审核
@Data
public class VaBookAudit {
    @HasAnyText(message = "请选择预约记录")
    String record_id;
    @HasAnyText(message = "缺少审核状态")
    Integer status;
    Boolean sendMsg = true;
}
