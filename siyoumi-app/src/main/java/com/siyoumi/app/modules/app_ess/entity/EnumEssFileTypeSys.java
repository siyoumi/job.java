package com.siyoumi.app.modules.app_ess.entity;

import com.siyoumi.component.XEnumBase;


public class EnumEssFileTypeSys
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "系统资源");
        put(1, "补充资源");
    }
}
