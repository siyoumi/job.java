package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

@Data
public class VaTaskFun {
    String uid;
    @HasAnyText
    @EqualsValues(vals = {"file", "module", "test1"})
    String type;
}
