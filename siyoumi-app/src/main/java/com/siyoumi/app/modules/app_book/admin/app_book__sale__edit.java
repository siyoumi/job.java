package com.siyoumi.app.modules.app_book.admin;

import com.siyoumi.app.entity.SysSale;
import com.siyoumi.app.modules.app_book.entity.EnumBookSaleLevel;
import com.siyoumi.app.modules.app_book.service.SvcBookSet;
import com.siyoumi.app.modules.app_book.vo.VaBookSet;
import com.siyoumi.app.modules.user.service.SvcSysSale;
import com.siyoumi.app.modules.user.vo.VoSale;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__sale__edit")
public class app_book__sale__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("主播-编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("sale_level", 0);
        if (isAdminEdit()) {
            SysSale entity = SvcSysSale.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
            //等级
            data.put("sale_level", entity.getSale_level().toString());
        }
        getR().setData("data", data);

        //等级
        EnumBookSaleLevel enumSaleLevel = XEnumBase.of(EnumBookSaleLevel.class);
        setPageInfo("enum_sale_level", enumSaleLevel);

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VoSale vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        if (isAdminAdd()) {
            vo.setSale_acc_id(getAccId());
        }
        //
        return SvcSysSale.getBean().save(vo);
    }
}