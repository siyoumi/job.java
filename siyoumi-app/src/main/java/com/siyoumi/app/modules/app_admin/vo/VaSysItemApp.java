package com.siyoumi.app.modules.app_admin.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

// 活动属性模板
@Data
public class VaSysItemApp {
    @NotBlank
    @Size(max = 50)
    private String item_id;
    @NotBlank
    private String item_app_id;
    private String item_id_src;
    @NotBlank
    @Size(max = 50)
    private String item_name;
    private String item_data;

    private Long item_order;
}
