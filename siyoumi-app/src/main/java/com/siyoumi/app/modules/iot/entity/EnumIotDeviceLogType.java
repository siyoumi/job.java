package com.siyoumi.app.modules.iot.entity;

import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.XEnumBase;
import org.springframework.stereotype.Service;

/**
 * 日志类型
 */
@Service
public class EnumIotDeviceLogType
        extends XEnumBase<String> {
    static public EnumIotDeviceLogType getBean() {
        return XSpringContext.getBean(EnumIotDeviceLogType.class);
    }

    @Override
    protected void initKV() {
        put("push", "上行");
        put("pull", "下拉");
        put("device_state", "设备状态");
    }
}
