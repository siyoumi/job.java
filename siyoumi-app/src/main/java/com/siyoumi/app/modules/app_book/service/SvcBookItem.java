package com.siyoumi.app.modules.app_book.service;

import com.siyoumi.app.entity.BookItem;
import com.siyoumi.app.entity.BookItemZzz;
import com.siyoumi.app.modules.app_book.vo.VaBookItem;
import com.siyoumi.app.service.BookItemService;
import com.siyoumi.app.service.BookItemZzzService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//
@Slf4j
@Service
public class SvcBookItem
        implements IWebService {
    static public SvcBookItem getBean() {
        return XSpringContext.getBean(SvcBookItem.class);
    }

    static public BookItemService getApp() {
        return BookItemService.getBean();
    }

    /**
     * 更新子项关联表
     *
     * @param type
     * @param typeId
     * @param itemIds
     */
    public void updateItemZZ(String type, String typeId, String storeId, List<String> itemIds) {
        //删除旧套餐
        JoinWrapperPlus<BookItemZzz> query = listZzQuery(type);
        query.eq("bizz_type_id", typeId);
        BookItemZzzService.getBean().remove(query);

        //添加新套餐
        List<BookItemZzz> list = new ArrayList<>();
        for (String id : itemIds) {
            BookItemZzz entity = new BookItemZzz();
            entity.setAutoID();
            entity.setBizz_x_id(XHttpContext.getX());
            entity.setBizz_type(type);
            entity.setBizz_type_id(typeId);
            entity.setBizz_store_id(storeId);
            entity.setBizz_item_id(id);
            list.add(entity);
        }
        BookItemZzzService.getBean().saveBatch(list);
    }

    public List<String> getItemIds(String type, String typeId) {
        JoinWrapperPlus<BookItemZzz> query = listZzQuery(type);
        query.eq("bizz_type_id", typeId);
        query.select("bizz_item_id");

        List<Map<String, Object>> list = BookItemZzzService.getBean().getMaps(query);
        return list.stream().map(item -> (String) item.get("bizz_item_id")).collect(Collectors.toList());
    }

    public List<Map<String, Object>> getList(String type, String pid) {
        JoinWrapperPlus<BookItem> query = listQuery();
        query.eq("bitem_type", type);
        if (XStr.hasAnyText(pid)) {
            query.eq("bitem_pid", pid);
        }

        query.select("bitem_id", "bitem_name", "bitem_pic");
        return getApp().getMaps(query);
    }

    public BookItem getEntityByName(String type, String name) {
        JoinWrapperPlus<BookItem> query = listQuery();
        query.eq("bitem_type", type)
                .eq("bitem_name", name);

        return getApp().first(query);
    }

    public JoinWrapperPlus<BookItem> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<BookItem> listQuery(InputData inputData) {
        String name = inputData.input("name");
        String pid = inputData.input("pid");
        String type = inputData.input("type");

        JoinWrapperPlus<BookItem> query = getApp().join();
        query.eq("bitem_x_id", XHttpContext.getX());
        //排序
        query.orderByAsc("bitem_order")
                .orderByDesc("bitem_id");
        if (XStr.hasAnyText(name)) { //名称
            query.like("bitem_name", name);
        }
        if (XStr.hasAnyText(pid)) { //pid
            query.eq("bitem_pid", pid);
        }
        if (XStr.hasAnyText(type)) { //类型
            query.in("bitem_type", List.of(type.split(",")));
        }

        return query;
    }

    public JoinWrapperPlus<BookItemZzz> listZzQuery(String type) {
        JoinWrapperPlus<BookItemZzz> query = BookItemZzzService.getBean().join();
        query.eq("bizz_x_id", XHttpContext.getX())
                .eq("bizz_type", type);

        return query;
    }

    public BookItem save(String type, String pid, String name) {
        VaBookItem data = VaBookItem.of(type, pid, name, 0);
        BookItem entity = getEntityByName(type, name);
        if (entity != null) {
            data.setBitem_id(entity.getBitem_id());
        }
        return save(data);
    }

    public BookItem save(VaBookItem vo) {
        List<String> ignoreField = new ArrayList<>();
        InputData inputData = InputData.getIns();
        inputData.put("id", vo.getBitem_id());

        if (inputData.isAdminEdit()) {
            ignoreField.add("bitem_pid");
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, true, ignoreField);
            if (r.err()) {
                return null;
            }
            return r.getData("entity");
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }
}
