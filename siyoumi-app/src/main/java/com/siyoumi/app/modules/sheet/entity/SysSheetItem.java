package com.siyoumi.app.modules.sheet.entity;

import lombok.Data;

@Data
public class SysSheetItem {
    String field_key;
    Integer field_type;
    String label;
    Integer required = 1;
    String type;
    String type_item;
    Integer type_checkbox_max = 0;
    Integer use_reg = 0;
    String reg;
}
