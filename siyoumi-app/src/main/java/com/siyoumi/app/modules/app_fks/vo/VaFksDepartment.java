package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//部门
@Data
public class VaFksDepartment {
    private String fdep_acc_id;
    @Size(max = 50)
    @HasAnyText
    private String fdep_name;
    @HasAnyText
    private Integer fdep_order;
}
