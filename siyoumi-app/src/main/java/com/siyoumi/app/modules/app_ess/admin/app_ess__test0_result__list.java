package com.siyoumi.app.modules.app_ess.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.app_ess.service.SvcEssTest;
import com.siyoumi.app.service.EssTestResultService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__test0_result__list")
public class app_ess__test0_result__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("评价提交列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "etres_id",
                "etres_submit",
                "etres_submit_date",
                "etres_submit_minute",
                "etres_test_fun",
                "user_id",
                "user_name",
                "eclass_id",
                "eclass_name",
                "etest_id",
                "etest_title",
        };
        JoinWrapperPlus<EssTestResult> query = SvcEssTest.getBean().listResultQuery(inputData);
        query.join(EssTest.table(), EssTest.tableKey(), "etres_test_id");
        query.join(SysUser.table(), SysUser.tableKey(), "etres_uid");
        query.join(EssClassUser.table(), SysUser.tableKey(), "ecu_uid");
        query.join(EssClass.table(), EssClass.tableKey(), "ecu_class_id");
        query.select(select);
        query.eq("ecu_user_type", 0);
        query.orderByDesc("etres_id");
        //
        IPage<EssTestResult> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = EssTestResultService.getBean().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        for (Map<String, Object> item : list) {
            EssTestResult entity = XBean.fromMap(item, EssTestResult.class);
            item.put("id", entity.getKey());
            //提交状态
            XReturn rSubmitState = SvcEssTest.getBean().getResultSubmitState(entity);
            item.put("submit_state", rSubmitState.getErrMsg());
        }

        if (!isAdminExport()) {
            getR().setData("list", list);
            getR().setData("count", pageData.getTotal());
        } else {
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("id", "ID");
            tableHead.put("etest_title", "评价");
            tableHead.put("user_name", "学生");
            tableHead.put("user_id", "学生uid");
            tableHead.put("eclass_name", "班级");
            tableHead.put("submit_state", "提交状态");
            tableHead.put("etres_submit_date", "提交时间");
            tableHead.put("etres_submit_minute", "考试时长");
            tableHead.put("etres_test_fun", "得分");

            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }


        return getR();
    }

    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }
}
