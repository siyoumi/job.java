package com.siyoumi.app.modules.app_fks.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FksDepartment;
import com.siyoumi.app.entity.FksPosition;
import com.siyoumi.app.entity.FksUser;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_fks.service.SvcFksUser;
import com.siyoumi.app.modules.user.entity.EnumUserEnable;
import com.siyoumi.app.modules.user.vo.SysUserAudit;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.app.service.SysUserService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__user__list")
public class app_fks__user__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("员工列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "user_id",
                "user_name",
                "user_headimg",
                "user_phone",
                "user_enable",
                "user_enable_date",
                "fdep_name",
                "fposi_name",
        };
        JoinWrapperPlus<FksUser> query = SvcFksUser.getBean().listQuery(inputData);
        query.join(FksDepartment.table(), FksDepartment.tableKey(), "fuser_department_id");
        query.join(FksPosition.table(), FksPosition.tableKey(), "fuser_position_id");
        query.select(select);

        IPage<FksUser> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcFksUser.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumUserEnable enumUserEnable = XEnumBase.of(EnumUserEnable.class);
        for (Map<String, Object> item : list) {
            SysUser entitySysUser = SysUserService.getBean().loadEntity(item);
            item.put("id", entitySysUser.getKey());
            //状态
            item.put("enable", enumUserEnable.get(entitySysUser.getUser_enable()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcFksUser.getBean().delete(getIds());
    }

    //审核
    @PostMapping("/audit")
    public XReturn audit(@Validated SysUserAudit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        return SvcSysUser.getBean().audit(vo);
    }
}
