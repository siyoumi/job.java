package com.siyoumi.app.modules.app_ess.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.EssModule;
import com.siyoumi.app.entity.EssStudy;
import com.siyoumi.app.entity.EssTest;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_ess.service.SvcEssStudy;
import com.siyoumi.app.modules.app_ess.service.SvcEssTest;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__study__list")
public class app_ess__study__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("学习任务列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "estudy_id",
                "estudy_name",
                "estudy_date_begin",
                "estudy_date_end",
                "estudy_student_total",
                "user_id",
                "user_name",
                "emod_name",
        };
        JoinWrapperPlus<EssStudy> query = SvcEssStudy.getBean().listQuery(inputData);
        query.join(SysUser.table(), SysUser.tableKey(), "estudy_uid");
        query.join(EssModule.table(), EssModule.tableKey(), "estudy_module_id");
        query.select(select);
        query.orderByDesc("estudy_id");
        IPage<EssStudy> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = SvcEssStudy.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        for (Map<String, Object> item : list) {
            EssStudy entity = XBean.fromMap(item, EssStudy.class);
            item.put("id", entity.getKey());
            //状态
            item.put("state", SvcEssStudy.getBean().getState(entity));
        }

        getR().setData("list", list);
        getR().setData("count", pageData.getTotal());

        return getR();
    }
}
