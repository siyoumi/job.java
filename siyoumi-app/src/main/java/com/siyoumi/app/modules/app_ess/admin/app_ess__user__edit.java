package com.siyoumi.app.modules.app_ess.admin;

import com.siyoumi.app.entity.EssClass;
import com.siyoumi.app.entity.EssUser;
import com.siyoumi.app.entity.FksUser;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_ess.entity.EnumEssUserType;
import com.siyoumi.app.modules.app_ess.service.SvcEssClass;
import com.siyoumi.app.modules.app_ess.service.SvcEssQuestion;
import com.siyoumi.app.modules.app_ess.service.SvcEssUser;
import com.siyoumi.app.modules.app_ess.vo.VaEssClass;
import com.siyoumi.app.modules.app_ess.vo.VaEssUser;
import com.siyoumi.app.modules.app_ess.vo.VoEssQuestionAddBatch;
import com.siyoumi.app.modules.app_ess.vo.VoEssUserAddBatch;
import com.siyoumi.app.modules.app_fks.service.SvcFksUser;
import com.siyoumi.app.modules.user.entity.EnumSex;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.component.XApp;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__user__edit")
public class app_ess__user__edit
        extends AdminApiController {

    @GetMapping()
    public XReturn index() {
        setPageTitle("用户-编辑");

        Map<String, Object> data = new HashMap<>();
        //data.put("fcity_order", 0);
        if (isAdminEdit()) {
            EssUser entity = SvcEssUser.getApp().loadEntity(getID());
            SysUser entitySysUser = SvcSysUser.getApp().loadEntity(entity.getKey());
            //
            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            dataAppend.putAll(entitySysUser.toMap());
            //
            //合并
            data = entity.toMap();
            data.put("euser_type", entity.getEuser_type().toString());
            data.put("euser_sex", entity.getEuser_sex().toString());
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        EnumEssUserType enumType = EnumEssUserType.of(EnumEssUserType.class);
        setPageInfo("enum_type", enumType); //类型 老师学生
        EnumSex enumSex = EnumEssUserType.of(EnumSex.class);
        setPageInfo("enum_sex", enumSex); //性别
        setPageInfo("def_pwd", SvcSysUser.defPwd()); //重置密码
        //导入模板
        setPageInfo("import_url", XApp.fileUrl("res/import/导入帐号.xls"));

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated VaEssUser vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        //{
        //    JoinWrapperPlus<EssUser> query = SvcEssUser.getBean().listQuery();
        //    query.eq("user_phone", vo.getUser_phone());
        //    if (isAdminEdit()) {
        //        query.ne(EssUser.tableKey(), getID());
        //    }
        //    EssUser entity = SvcEssUser.getApp().first(query);
        //    if (entity != null) {
        //        result.addError(XValidator.getErr("user_phone", "手机号已存在"));
        //    }
        //}
        if (XStr.hasAnyText(vo.getUser_username())) {
            JoinWrapperPlus<EssUser> query = SvcEssUser.getBean().listQuery();
            query.eq("user_username", vo.getUser_username());
            if (isAdminEdit()) {
                query.ne(EssUser.tableKey(), getID());
            }
            EssUser entity = SvcEssUser.getApp().first(query);
            if (entity != null) {
                result.addError(XValidator.getErr("user_username", "登陆帐号已存在"));
            }
        }
        XValidator.getResult(result);
        //
        if (isAdminEdit()) {
            SysUser entityUser = SvcSysUser.getApp().loadEntity(getID());
            if (entityUser.getUser_enable() == 1) {
                vo.setUser_phone(null); //审核通过手机号不能修改
            }
            vo.setEuser_type(null);
        } else {
            vo.setEuser_id(XApp.getStrID());
            vo.setUser_phone(SvcSysUser.tempPhone(vo.getEuser_id()));
        }
        //
        InputData inputData = InputData.fromRequest();
        return SvcEssUser.getBean().edit(inputData, vo);
    }


    /**
     * 重置密码
     */
    @PostMapping("/reset_pwd")
    @Transactional(rollbackFor = Exception.class)
    public XReturn resetPwd() {
        return SvcSysUser.getBean().resetPwd(getIds());
    }

    /**
     * 批量添加
     *
     * @param vo
     * @param result
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/add")
    public XReturn add(@Validated @RequestBody VoEssUserAddBatch vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        return SvcEssUser.getBean().addBatch(vo);
    }
}
