package com.siyoumi.app.modules.user.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class SysUserEdit {
    String uid;

    @Size(max = 50, message = "名字最多50位字符")
    String user_name;
    @Size(max = 50, message = "登录贴最多50位字符")
    String user_username;
    @Size(max = 200)
    String user_headimg;

    @Size(max = 50)
    String user_email;
}
