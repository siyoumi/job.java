package com.siyoumi.app.modules.sys_status.entity;

import com.siyoumi.util.XStr;
import lombok.Data;

@Data
public class SysStatusInfo {
    //redis
    Boolean redis_status = false;
    String redis_errmsg = "";
    //msyql
    Boolean mysql_status = false;
    String mysql_errmsg = "";

    public Boolean sysOk() {
        return getRedis_status() && getMysql_status();
    }

    public String print() {
        StringBuilder sb = new StringBuilder();
        if (getRedis_status()) {
            sb.append("redis: ok \n\n");
        } else {
            sb.append("redis: **error** \n\n");
            sb.append("redis: " + XStr.maxLen(getRedis_errmsg(), 100) + " \n\n");
        }

        if (getMysql_status()) {
            sb.append("mysql: ok \n\n");
        } else {
            sb.append("mysql: **error** \n\n");
            sb.append("mysql: " + XStr.maxLen(getRedis_errmsg(), 100) + " \n\n");
        }
        return sb.toString();
    }
}
