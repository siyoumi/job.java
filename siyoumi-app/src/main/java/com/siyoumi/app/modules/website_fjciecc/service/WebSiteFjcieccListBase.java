package com.siyoumi.app.modules.website_fjciecc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.website_fjciecc.service.SvcFjciecc;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class WebSiteFjcieccListBase
        extends AdminApiController {
    public String title() {
        return "";
    }

    public String abcTable() {
        return "";
    }

    //for处理
    public void forHandle(List<Map<String, Object>> list) {
    }

    //导出处理
    public List<List<String>> exportHandle(IPage<Map<String, Object>> pageData, List<Map<String, Object>> list) {
        return null;
    }

    public XReturn index() {
        setPageTitle(title());

        InputData inputData = InputData.fromRequest();

        JoinWrapperPlus<SysAbc> query = SvcFjciecc.getBean().listQuery(inputData, List.of(abcTable()));

        IPage<SysAbc> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), !isAdminExport());
        //list
        IPage<Map<String, Object>> pageData = SysAbcService.getBean().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        forHandle(list);

        if (!isAdminExport()) {
            getR().setData("list", list);
            getR().setData("count", count);
        } else {
            //导出
            List<List<String>> listDataEx = exportHandle(pageData, list);
            getR().setData("list", listDataEx);
        }


        return getR();
    }


    //删除
    public XReturn del(String[] ids) {
        SysAbcService svcAbc = SysAbcService.getBean();
        return svcAbc.delete(Arrays.asList(ids), abcTable());
    }
}
