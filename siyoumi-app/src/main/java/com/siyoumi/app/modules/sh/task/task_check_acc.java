package com.siyoumi.app.modules.sh.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.ShUser;
import com.siyoumi.app.service.ShUserService;
import com.siyoumi.component.api.jj.JJ;
import com.siyoumi.component.api.jj.robot.RobotJD;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.config.SysConfig;
import com.siyoumi.task.EnumTask;
import com.siyoumi.task.TaskController;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

//检查帐号是否过期
@RestController
@RequestMapping("/task/sh/task_check_acc")
public class task_check_acc
        extends TaskController {
    @Override
    protected List handleData() {
        List<String> list = new ArrayList<>();
        list.add("1");
        return list;
    }

    @Override
    protected void handleBefore(Object data) {

    }

    @Override
    protected XReturn handle(Object data) {
        ShUserService app = ShUserService.getBean();
        //
        QueryWrapper<ShUser> query = app.join();
        query.eq("suser_x_id", XHttpContext.getX())
                .eq("suser_type", 0);
        query.lt("suser_key_expire_date", XDate.now().minusHours(ShUser.expireHourMax()))
                .eq("suser_enable", 1)
                .orderByAsc("suser_key_expire_date")
                .orderByDesc("suser_id");
        List<ShUser> listUser = app.get(query);
        if (listUser.size() <= 0) {
            return XReturn.getR(0, "无数据处理");
        }

        StringBuilder sb = new StringBuilder();
        for (ShUser entityUser : listUser) {
            Integer leftExpireDay = entityUser.leftExpireDay();
            String accMsg = "帐号已过期";
            if (leftExpireDay < 0) {
                accMsg = XStr.concat("帐号已过期", Math.abs(leftExpireDay) + "天");
            }

            String msg = XStr.concat(entityUser.ptPin(), "[**", entityUser.getSuser_name(), "**]：", accMsg);
            sb.append(msg + " \n\n");
        }
        //登陆链接
        //msg.append(XStr.concat("[-- 查看详情 --](", errorUrl, ") \n\n"));
        sb.append("[https://bean.m.jd.com/bean/signIndex.action](https://bean.m.jd.com/bean/signIndex.action)");

        if (!SysConfig.getIns().isDev()) {
            JJ jj = JJ.getIns(RobotJD.class);
            jj.send(sb.toString(), null, true);
        }


        return EnumTask.STOP.getR();
    }
}
