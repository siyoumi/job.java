package com.siyoumi.app.modules.sh.service;

import com.siyoumi.app.entity.ShUser;
import com.siyoumi.app.service.ShUserService;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SvcShCcbUser {
    static public SvcShCcbUser getBean() {
        return XSpringContext.getBean(SvcShCcbUser.class);
    }

    static public ShUserService getApp() {
        return ShUserService.getBean();
    }

    public JoinWrapperPlus<ShUser> listQuery() {
        JoinWrapperPlus<ShUser> query = getApp().getQuery(1);
        query.eq("suser_enable", 1)
                .gt("suser_key_expire_date", XDate.now().minusHours(2));
        return query;
    }

    public String exportConfigSh() {
        JoinWrapperPlus<ShUser> query = listQuery();
        List<ShUser> list = getApp().get(query);

        String ck = list.stream()
                .map(item -> {
                    return XStr.concat(item.getSuser_key(), "|", item.getSuser_name());
                })
                .collect(Collectors.joining("@"));

        return "CCB_TOKEN=" + ck;
    }

    //导出config
    public String exportConfig() {
        String config = "" +
                "CCB_COOKIE={CK}\n" +
                "CCB_AUTO_CHOU=1\n" +
                "CCB_TOULAN=5\n" +
                "CCB_OPENCV_URL=http://192.168.0.91:8080\n" +
                "DD_BOT_ACCESS_TOKEN=701783614f2d2c2cd818d7c0eb8c4e3c8ec93c22ca57364121f2601a3eabaaa7\n" +
                "DD_BOT_SECRET=SEC821b5bf9f85e7307988f48988a9df76a698a4628f0ee53f9e990ecc2a9945d60\n" +
                "\n" +
                "CCB_ACTIVITY_URL=http://165.1.76.1:5000/ccb/api/activity";

        ShUserService app = ShUserService.getBean();
        //
        JoinWrapperPlus<ShUser> query = listQuery();
        List<ShUser> list = app.get(query);

        String ck = "";
        if (list.size() > 0) {
            String wp = "2YpYj1aD7zJ5/Kqnh2mvjX7CI+rSNAWlCO9+jlhf06MTiBfhkZOH/RD+qh8lY8rtMroQEfaG+RTpFMkXm6bdA7edaYnWV5waLgFc10AnmGpLlBF1issWvZGE7yyCXPcq2gVglURhz5+/J0x5yPuLA6jCTRV7PX5tvxMrcqymCbAne3VBb/ptr0FCHIDHyskH1KP99jtJV+I2svC46usXPqCjBsib+Bz7a32AKyNwpNpVUZ8AK738TTPesq+sxEVj72RM2xf6r0AigXd9VztcEcg7pGPNwCaGls1BHzlt0HtnvgD2Hq0hK4EWHXKQiTRr+xr6G0TLjYJ9Bb4OC/yz28DZJs5vypwapDLpczoie3w3SXRyQ4CJ2RHVAQGsT4nu36ZINdXhvLAAxcsusAaM1AudARBO8q4RFylT90q64LsrVc3X2JWEXbTz6+/EjTZFfuttVUMtedD61m8ZgH2ZZepV67jvASeaEA4KTg3VuD3bBj+F+l7nHUHrOvo3M7RIh7x6clv3NpesVs0vgJEROLnvs2sbhUkvsUfu9Zdbtwoo+kRP5fHfgL58/d4xeJBZL4K3txR7ZQt+VgVuEc6ytpF076y2oTLNrvEzaCjPovBSa2o/Si2LHw==";

            list.add(list.get(0));
            ck = list.stream()
                    .map(item -> {
                        return XStr.concat(item.getSuser_name(), ";", wp, ";", item.getSuser_key());
                    })
                    .collect(Collectors.joining("&"));
        }
        config = config.replace("{CK}", ck);

        return config;
    }
}
