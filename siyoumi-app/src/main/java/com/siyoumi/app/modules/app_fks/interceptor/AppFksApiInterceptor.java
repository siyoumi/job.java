package com.siyoumi.app.modules.app_fks.interceptor;

import com.siyoumi.app.entity.ActSet;
import com.siyoumi.app.entity.FksUser;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.act.service.SvcActSet;
import com.siyoumi.app.modules.app_fks.service.SvcFksUser;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.interceptor.InterceptorBase;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//fks api
@Slf4j
public class AppFksApiInterceptor
        extends InterceptorBase
        implements HandlerInterceptor {
    /**
     * 执行前
     *
     * @return boolean
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("BEGIN");

        XReturn tokenData = XHttpContext.getTokenData();
        String uid = tokenData.getData("uid");
        if (XStr.isNullOrEmpty(uid)) {
            return returnErr(response, EnumSys.ERR_UID.getR());
        }
        SysUser entityUser = SvcSysUser.getBean().getEntity(uid);
        XReturn r = SvcSysUser.getBean().valid(entityUser);
        if (r.err()) {
            return returnErr(response, r);
        }


        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("END");
    }
}
