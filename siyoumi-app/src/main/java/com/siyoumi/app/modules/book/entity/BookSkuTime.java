package com.siyoumi.app.modules.book.entity;

import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.Data;

//时间段时间
@Data
public class BookSkuTime {
    String beginTime;
    String endTime;

    public static BookSkuTime parse(String t) {
        if (XStr.isNullOrEmpty(t)) {
            XValidator.err(20020, "时间段为空, " + t);
        }

        String[] tArr = t.split("-");
        if (tArr.length != 2) {
            XValidator.err(20030, "时间段内容异常, " + t);
        }

        BookSkuTime data = new BookSkuTime();
        data.setBeginTime(tArr[0]);
        data.setEndTime(tArr[1]);
        return data;
    }
}
