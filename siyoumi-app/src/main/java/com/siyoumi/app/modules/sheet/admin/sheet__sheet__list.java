package com.siyoumi.app.modules.sheet.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysSheet;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSet;
import com.siyoumi.app.modules.sheet.service.SvcSheet;
import com.siyoumi.app.service.SysSheetService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/sheet/sheet__sheet__list")
public class sheet__sheet__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("表单列表");


        InputData inputData = InputData.fromRequest();

        JoinWrapperPlus<SysSheet> query = SvcSheet.getBean().listQuery(inputData);

        IPage<SysSheet> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcSheet.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            SysSheet entitySheet = XBean.fromMap(data, SysSheet.class);
            data.put("id", entitySheet.getKey());

            // 过期
            data.put("type", entitySheet.expire());

            return data;
        }).collect(Collectors.toList());

        getR().setData("list", listData);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        SvcSheet app = SvcSheet.getBean();
        return app.delete(Arrays.asList(ids));
    }
}
