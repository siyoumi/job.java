package com.siyoumi.app.modules.sys_code.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.entity.SysCode;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.modules.redbook.service.SvcRedbookQuan;
import com.siyoumi.app.modules.redbook.vo.VaRedBookQuan;
import com.siyoumi.app.modules.sys_code.service.SvcCode;
import com.siyoumi.app.modules.sys_code.vo.CodeAddVo;
import com.siyoumi.app.service.RedbookQuanService;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/xadmin/sys_code/sys_code__item__edit")
public class sys_code__item__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        InputData inputData = InputData.fromRequest();
        String key = inputData.input("key");
        if (XStr.isNullOrEmpty(key)) {
            return EnumSys.MISS_VAL.getR("缺少码库ID");
        }
        SysAbc entityGroup = SysAbcService.getBean().getEntityByTable(key, "sys_code_group");
        setPageTitle(entityGroup.getAbc_name(), "-批量添加");

        Map<String, Object> data = new HashMap<>();
        data.put("code_key", entityGroup.getKey());
        data.put("code_vaild_date_begin", XDate.toDateTimeString(XDate.today()));
        data.put("code_vaild_date_end", XDate.toDateTimeString(XDate.today().plusMonths(1L).minusSeconds(1L)));
        data.put("num", 1);
        if (isAdminEdit()) {
        }
        getR().setData("data", data);

        return getR();
    }


    @PostMapping("/save")
    public XReturn save(@Validated() CodeAddVo vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //自定义验证
        SysAbc entityGroup = SysAbcService.getBean().getEntityByTable(vo.getCode_key(), "sys_code_group");
        if (entityGroup == null) {
            result.addError(XValidator.getErr("code_key", "码库ID异常"));
        }
        XValidator.getResult(result);

        String appId = XHttpContext.getAppId();
        String x = XHttpContext.getX();

        log.debug("批量添加");
        List<SysCode> list = new ArrayList<>();
        for (Integer i = 0; i < vo.getNum(); i++) {
            String code = SvcCode.genCode("");
            SysCode entityCode = new SysCode();
            XBean.copyProperties(vo, entityCode);
            entityCode.setCode_x_id(x);
            entityCode.setCode_app_id(appId);
            entityCode.setCode_code(code);

            list.add(entityCode);
        }
        SvcCode.getApp().saveBatch(list);


        return EnumSys.OK.getR();
    }
}


