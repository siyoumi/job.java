package com.siyoumi.app.modules.mall2.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

//退款申请接口
@Data
public class OrderItemRefundVo {
    @NotBlank(message = "miss item_id")
    String item_id;

    @NotBlank(message = "请输入原因")
    @Size(max = 100)
    String desc = "";
    //后台申请人参数
    String apply_acc; //后台申请人acc_id
    @Size(max = 50)
    String admin_pwd = "";
    BigDecimal refund_price; //退款金额
}
