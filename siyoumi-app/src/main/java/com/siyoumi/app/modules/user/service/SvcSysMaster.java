package com.siyoumi.app.modules.user.service;

import com.siyoumi.app.entity.SysMaster;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.user.vo.VoBindMaster;
import com.siyoumi.app.service.SysMasterService;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//邀请关系表
@Slf4j
@Service
public class SvcSysMaster
        implements IWebService {
    static public SvcSysMaster getBean() {
        return XSpringContext.getBean(SvcSysMaster.class);
    }

    static public SysMasterService getApp() {
        return SysMasterService.getBean();
    }

    public JoinWrapperPlus<SysMaster> listQuery() {
        InputData inputData = InputData.getIns();
        return listQuery(inputData);
    }

    public JoinWrapperPlus<SysMaster> listQuery(String uid) {
        InputData inputData = InputData.getIns();
        inputData.put("uid", uid);

        return listQuery(inputData);
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<SysMaster> listQuery(InputData inputData) {
        String uid = inputData.input("uid");
        String openid = inputData.input("openid");

        JoinWrapperPlus<SysMaster> query = getApp().join();
        query.eq("mast_x_id", XHttpContext.getX());

        if (XStr.hasAnyText(openid)) { //openid
            query.eq("mast_openid", openid);
        }
        if (XStr.hasAnyText(uid)) { //uid
            query.eq("mast_uid", uid);
        }

        return query;
    }

    /**
     * 是否已存在记录
     *
     * @param uid
     * @param openid
     */
    public Boolean exists(String uid, String openid) {
        JoinWrapperPlus<SysMaster> query = listQuery(uid);
        query.eq("mast_openid", openid);
        query.select("mast_openid");

        return getApp().first(query) != null;
    }

    public SysMaster getEntity(String openid) {
        JoinWrapperPlus<SysMaster> query = listQuery();
        query.eq("mast_openid", openid);

        return getApp().first(query);
    }

    /**
     * 绑定主人关系
     *
     * @param vo
     */
    public XReturn bindMaster(VoBindMaster vo) {
        SysUser entityUser = SvcSysUser.getBean().getEntity(vo.getMaster_uid());
        XValidator.isNull(entityUser, "uid error");

        String masterOpenid = SvcSysUser.getBean().getOpenid(vo.getMaster_uid());
        if (vo.getOpenid().equals(masterOpenid)) {
            return XReturn.getR(20107, "自己不能邀请自己");
        }

        SysMaster entity = getEntity(vo.getOpenid());
        if (entity == null) {
            SysMaster entityNew = new SysMaster();
            entityNew.setMast_x_id(XHttpContext.getX());
            entityNew.setMast_openid(vo.getOpenid());
            entityNew.setMast_uid(vo.getMaster_uid());
            entityNew.setAutoID();
            getApp().save(entityNew);
        } else {
            //已存在关系，更新主人
            SysMaster entityUpdate = new SysMaster();
            entityUpdate.setMast_id(entity.getMast_id());
            entityUpdate.setMast_uid(vo.getMaster_uid());
            getApp().saveOrUpdatePassEqualField(entity, entityUpdate);
        }

        return EnumSys.OK.getR();
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }
}
