package com.siyoumi.app.modules.app_ess.admin;

import com.siyoumi.app.entity.EssClass;
import com.siyoumi.app.entity.EssUser;
import com.siyoumi.app.modules.app_ess.service.SvcEssClass;
import com.siyoumi.app.modules.app_ess.service.SvcEssUser;
import com.siyoumi.app.modules.app_ess.vo.VoEssAddStudent;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__class__student__add")
public class app_ess__class__student__add
        extends AdminApiController {

    @GetMapping()
    public XReturn index() {
        String classId = input("class_id");
        XValidator.isNullOrEmpty(classId, "miss class_id");

        EssClass entityClass = SvcEssClass.getApp().getEntity(classId);
        XValidator.isNull(entityClass, "班级ID异常");
        setPageTitle(entityClass.getEclass_name(), "-学生分班");

        Map<String, Object> data = new HashMap<>();
        data.put("class_id", entityClass.getKey());
        if (isAdminEdit()) {
            EssClass entity = SvcEssClass.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //学生列表-排队已添加学生
        {
            JoinWrapperPlus<EssUser> query = SvcEssUser.getBean().listQuery(0);
            query.eq("euser_set_class", 0);
            query.select("user_id", "user_name");
            List<Map<String, Object>> listStudent = SvcEssUser.getApp().getMaps(query);
            setPageInfo("list_student", listStudent);
        }


        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated VoEssAddStudent vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        InputData inputData = InputData.fromRequest();
        return SvcEssClass.getBean().studentAddClass(vo);
    }
}