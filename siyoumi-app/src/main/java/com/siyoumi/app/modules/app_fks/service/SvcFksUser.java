package com.siyoumi.app.modules.app_fks.service;

import com.siyoumi.app.entity.FksFile;
import com.siyoumi.app.entity.FksPosition;
import com.siyoumi.app.entity.FksUser;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_fks.vo.VaFksUser;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.app.service.FksUserService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XRedis;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//用户
@Slf4j
@Service
public class SvcFksUser
        implements IWebService {
    static public SvcFksUser getBean() {
        return XSpringContext.getBean(SvcFksUser.class);
    }

    static public FksUserService getApp() {
        return FksUserService.getBean();
    }

    public FksUser getEntity(String id) {
        return XRedis.getBean().getAndSetData(getApp().getEntityCacheKey(id), k -> {
            return getApp().loadEntity(id);
        }, FksUser.class);
    }

    public JoinWrapperPlus<FksUser> listUserQuery(String uid) {
        JoinWrapperPlus<FksUser> query = listQuery();
        query.eq(FksUser.tableKey(), uid);

        return query;
    }

    /**
     * 获取指定权限所有用户
     *
     * @param positionLeve
     */
    public List<Map<String, Object>> listByLevel(Integer positionLeve, String[] select) {
        JoinWrapperPlus<FksUser> query = listQuery();
        query.join(FksPosition.table(), FksPosition.tableKey(), "fuser_position_id");
        query.select(select);
        query.eq("fposi_level", positionLeve);

        return getApp().getMaps(query);
    }

    public JoinWrapperPlus<FksUser> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<FksUser> listQuery(InputData inputData) {
        String name = inputData.input("name");

        JoinWrapperPlus<FksUser> query = getApp().join();
        query.eq("fuser_x_id", XHttpContext.getX());
        query.join(SysUser.table(), SysUser.tableKey(), FksUser.tableKey());

        if (XStr.hasAnyText(name)) { //名称
            query.like("fposi_name", name);
        }

        return query;
    }


    public XReturn edit(InputData inputData, VaFksUser vo) {
        List<String> ignoreField = new ArrayList<>();

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, false, ignoreField);

            SysUser entityUser = SvcSysUser.getApp().getEntity(vo.getFuser_id());
            SysUser entityUserUpdate = new SysUser();
            XBean.copyProperties(vo, entityUserUpdate);
            entityUserUpdate.setUser_id(vo.getFuser_id());
            entityUserUpdate.setUser_x_id(XHttpContext.getX());
            if (entityUser == null) {
                //新建，设置初始密码
                entityUserUpdate.setUser_pwd(XApp.encPwd(XHttpContext.getX(), vo.getUser_pwd()));
            }

            SvcSysUser.getApp().saveOrUpdatePassEqualField(entityUser, entityUserUpdate);

            //清缓存
            getApp().delEntityCache(vo.getFuser_id());
            SvcSysUser.getApp().delEntityCache(vo.getFuser_id());

            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);
        SvcSysUser.getBean().delete(ids);

        return r;
    }


    /**
     * 更新笔记数量
     */
    public Long updateNoteCount(String uid) {
        JoinWrapperPlus<FksFile> query = SvcFksFile.getApp().join();
        query.eq("ffile_type", 1)
                .eq("ffile_uid", uid)
                .eq("ffile_file_ext", "note");
        Long noteCount = SvcFksFile.getApp().count(query);

        FksUser entityUpdate = new FksUser();
        entityUpdate.setFuser_id(uid);
        entityUpdate.setFuser_note_count(noteCount);
        getApp().updateById(entityUpdate);

        return noteCount;
    }
}
