package com.siyoumi.app.modules.redbook.service.data_handle;

import com.siyoumi.app.entity.RedbookQuan;
import com.siyoumi.app.modules.redbook.service.RedbookDataHandle;
import com.siyoumi.app.modules.redbook.service.SvcRedbookData;
import com.siyoumi.app.modules.redbook.service.SvcRedbookQuan;
import com.siyoumi.app.modules.redbook.vo.RedbookSetData;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;

/**
 * 朋友圈点赞
 */
public class RedbookDataHandleQuanGood
        extends RedbookDataHandle {
    @Override
    protected XReturn handleCan(RedbookSetData vo) {
        RedbookQuan entityQuan = SvcRedbookQuan.getApp().getEntity(vo.getRdata_id_src());
        if (entityQuan == null) {
            return EnumSys.ERR_VAL.getR("来源ID异常");
        }

        vo.setRdata_quan_id(entityQuan.getRquan_id());
        return EnumSys.OK.getR();
    }

    @Override
    public XReturn handleAfter(RedbookSetData vo) {
        RedbookQuan entityQuan = SvcRedbookQuan.getApp().loadEntity(vo.getRdata_id_src());
        //更新点赞 收藏数量
        Long count = SvcRedbookData.getBean().getCount(vo.getRdata_type(), vo.getRdata_id_src(), vo.getRdata_uid(), null, null);
        RedbookQuan entityQuanUpdate = new RedbookQuan();
        entityQuanUpdate.setRquan_id(entityQuan.getRquan_id());
        if (vo.getRdata_type() == 0) {
            //点赞
            entityQuanUpdate.setRquan_good_count(count);
        } else {
            //收藏
            entityQuanUpdate.setRquan_collect_count(count);
        }
        SvcRedbookQuan.getApp().saveOrUpdatePassEqualField(entityQuan, entityQuanUpdate);

        return EnumSys.OK.getR();
    }
}
