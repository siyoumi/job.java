package com.siyoumi.app.modules.account.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaAccount {
    //@HasAnyText(message = "请选择角色")
    @Size(max = 50)
    private String acc_role;
    @HasAnyText
    @Size(max = 50)
    private String acc_uid;
    @HasAnyText
    @Size(max = 50)
    private String acc_name;
    private Integer acc_lock;
    private String acc_openid;
    private String acc_headimg;
    private String acc_phone;
    private String acc_department_id;

    private String acc_pwd;
    private String acc_store_app_id;
}
