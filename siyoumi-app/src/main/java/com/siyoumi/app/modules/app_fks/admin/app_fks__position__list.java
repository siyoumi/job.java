package com.siyoumi.app.modules.app_fks.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FksPosition;
import com.siyoumi.app.modules.app_fks.entity.EnumFksPositionLevel;
import com.siyoumi.app.modules.app_fks.service.SvcFksPosition;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__position__list")
public class app_fks__position__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("职位列表");

        InputData inputData = InputData.fromRequest();

        JoinWrapperPlus<FksPosition> query = SvcFksPosition.getBean().listQuery(inputData);

        IPage<FksPosition> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcFksPosition.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumFksPositionLevel enumLevel = XEnumBase.of(EnumFksPositionLevel.class);

        for (Map<String, Object> item : list) {
            FksPosition entity = XBean.fromMap(item, FksPosition.class);
            item.put("id", entity.getKey());

            item.put("level", enumLevel.get(entity.getFposi_level()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        //职位权限
        setPageInfo("position_level", enumLevel);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcFksPosition.getBean().delete(getIds());
    }
}
