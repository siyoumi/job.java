package com.siyoumi.app.modules.money.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/money/money__item__list")
public class money__item__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("钱包充值项-列表");

        String appId = XHttpContext.getAppId();

        InputData inputData = InputData.fromRequest();
        String compKw = inputData.input("compKw");

        SysAbcService svcAbc = SysAbcService.getBean();
        JoinWrapperPlus<SysAbc> query = svcAbc.listQuery(appId, false);

        //List<String> select = new ArrayList<>();
        //select.add("t_s_abc.*");
        //query.select(select.toArray(new String[select.size()]));
        query.eq("abc_table", "money_option")
                .eq("abc_int_01", 0)
                .orderByAsc("abc_order")
                .orderByDesc("abc_id");
        if (XStr.hasAnyText(compKw)) { // 名称
            query.like("abc_name", compKw);
        }

        IPage<SysAbc> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = svcAbc.getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            SysAbc entityAbc = svcAbc.loadEntity(data);
            data.put("id", entityAbc.getKey());

            return data;
        }).collect(Collectors.toList());

        getR().setData("list", listData);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SysAbcService.getBean().deleteLogic(Arrays.asList(ids), "money_option");
    }
}
