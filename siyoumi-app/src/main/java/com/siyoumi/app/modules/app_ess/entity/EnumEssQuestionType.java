package com.siyoumi.app.modules.app_ess.entity;

import com.siyoumi.component.XEnumBase;


public class EnumEssQuestionType
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "单选题");
        put(1, "多选题");
    }
}
