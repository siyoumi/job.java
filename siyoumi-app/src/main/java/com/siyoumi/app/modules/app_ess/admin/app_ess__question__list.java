package com.siyoumi.app.modules.app_ess.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.EssModule;
import com.siyoumi.app.entity.EssQuestion;
import com.siyoumi.app.modules.app_ess.entity.EnumEssQuestionLevel;
import com.siyoumi.app.modules.app_ess.entity.EnumEssQuestionType;
import com.siyoumi.app.modules.app_ess.entity.EnumEssType;
import com.siyoumi.app.modules.app_ess.service.SvcEssModule;
import com.siyoumi.app.modules.app_ess.service.SvcEssQuestion;
import com.siyoumi.app.modules.app_ess.vo.VoEssQuestionAddBatch;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__question__list")
public class app_ess__question__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("题目列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "etqu_id",
                "etqu_type",
                "etqu_question_type",
                "etqu_question",
                "etqu_question_item",
                "etqu_level",
                "etqu_answer",
                "etqu_level",
                "etqu_fun",
                "etqu_answer_total",
                "etqu_answer_bingo_total",
                "etqu_order",
                "emod_name",
        };
        JoinWrapperPlus<EssQuestion> query = SvcEssQuestion.getBean().listQuery(inputData);
        query.join(EssModule.table(), EssModule.tableKey(), "etqu_module_id");
        query.select(select);

        IPage<EssQuestion> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcEssQuestion.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumEssQuestionLevel enumQuestionLevel = XEnumBase.of(EnumEssQuestionLevel.class);
        EnumEssQuestionType enumQuestionType = XEnumBase.of(EnumEssQuestionType.class);
        EnumEssType enumType = XEnumBase.of(EnumEssType.class);
        for (Map<String, Object> item : list) {
            EssQuestion entity = XBean.fromMap(item, EssQuestion.class);
            item.put("id", entity.getKey());

            item.put("question_type", enumQuestionType.get(entity.getEtqu_question_type()));
            item.put("question_level", enumQuestionLevel.get(entity.getEtqu_level()));
            item.put("type", enumType.get(entity.getEtqu_type()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        setPageInfo("list_module", SvcEssModule.getBean().getList()); //模块列表
        setPageInfo("enum_type", enumType); //题库类型
        setPageInfo("enum_question_level", enumQuestionLevel); //题目难度等级
        setPageInfo("enum_question_type", enumQuestionType); //类型

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcEssQuestion.getBean().delete(getIds());
    }
}
