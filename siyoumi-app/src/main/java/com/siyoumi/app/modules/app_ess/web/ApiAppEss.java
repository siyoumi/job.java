package com.siyoumi.app.modules.app_ess.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.app_ess.entity.EnumEssFileType;
import com.siyoumi.app.modules.app_ess.service.*;
import com.siyoumi.app.modules.app_ess.vo.*;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.app.service.EssClassUserService;
import com.siyoumi.app.service.EssTestResultService;
import com.siyoumi.app.sys.service.SvcSysUv;
import com.siyoumi.app.sys.vo.VoUvSave;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/wxapp/app_ess/api")
public class ApiAppEss
        extends WxAppApiController {

    /**
     * 初始化
     *
     * @return
     */
    @GetMapping("init")
    public XReturn init() {
        EssUser entityUser = SvcEssUser.getBean().getEntity(getUid());
        Map<String, Object> mapUser = XBean.toMap(entityUser, new String[]{
                "euser_type",
                "euser_sex",
                "euser_speciality",
                "euser_school_date",
                "euser_contact",
                "euser_address",
                "euser_model_count",
                "euser_model_fun",
                "euser_file_count",
                "euser_file_fun",
                "euser_test0_count",
                "euser_test0_fun",
                "euser_test1_count",
                "euser_test1_fun",
        });
        SysUser entitySysUser = SvcSysUser.getBean().getEntity(getUid());
        mapUser.put("user_id", entitySysUser.getUser_id());
        mapUser.put("user_name", entitySysUser.getUser_name());
        mapUser.put("user_username", entitySysUser.getUser_username());
        mapUser.put("user_headimg", entitySysUser.getUser_headimg());
        mapUser.put("user_fun", entitySysUser.getUser_fun());

        getR().put("user", mapUser);

        //所属班级
        {
            JoinWrapperPlus<EssClassUser> query = SvcEssClass.getBean().listUserQuery();
            query.join(EssClass.table(), EssClass.tableKey(), "ecu_class_id");
            query.eq("ecu_uid", getUid());
            query.select("eclass_id", "eclass_name");
            List<Map<String, Object>> listClass = EssClassUserService.getBean().getMaps(query);
            getR().put("list_class", listClass);
        }


        return getR();
    }

    /**
     * 更新用户资料
     *
     * @param vo
     * @param result
     */
    @PostMapping("/user_edit")
    public XReturn userEdit(@Validated VoEssUserEdit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        EssUser entityUpdate = new EssUser();
        XBean.copyProperties(vo, entityUpdate);
        entityUpdate.setEuser_id(getUid());
        SvcEssUser.getApp().updateById(entityUpdate);

        return EnumSys.OK.getR();
    }

    //模块列表
    @GetMapping("module_list")
    public XReturn moduleList() {
        InputData inputData = InputData.fromRequest();

        String[] select = {
                "emod_id",
                "emod_name",
                "emod_pic",
                "emod_set_task_total",
                "emod_exe_tag",
                "emod_enable",
                "emod_question0",
                "emod_question1",
        };
        JoinWrapperPlus<EssModule> query = SvcEssModule.getBean().listQuery(inputData);
        query.select(select);

        IPage<EssModule> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = SvcEssModule.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();

        getR().put("list", list);

        return getR();
    }

    //资源列表
    @GetMapping("file_list")
    public XReturn fileList() {
        InputData inputData = InputData.fromRequest();
        String moduleId = inputData.input("module_id");
        XValidator.isNullOrEmpty(moduleId, "miss module_id");

        String[] select = {
                "efile_id",
                "efile_name",
                "efile_type",
                "efile_type_sys",
                "efile_path",
                "efile_file_size",
                "efile_file_ext",
                "efile_collect_count",
                "efile_order",
        };
        JoinWrapperPlus<EssFile> query = SvcEssFile.getBean().listQuery(inputData);
        query.select(select);

        IPage<EssFile> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), true);
        //list
        IPage<Map<String, Object>> pageData = SvcEssFile.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();

        List<String> fileIds = list.stream().map(item -> (String) item.get("efile_id")).collect(Collectors.toList());
        List<String> collectIds = SvcSysUv.getBean().getTypeIds(fileIds, "file", getUid()); //收藏

        for (Map<String, Object> item : list) {
            EssFile entity = XBean.fromMap(item, EssFile.class);
            //访问路径
            item.put("file_url", XApp.fileUrl(entity.getEfile_path()));
            //收藏
            item.put("is_collect", collectIds.contains(entity.getKey()));
        }

        getR().setData("list", list);
        getR().setData("total", pageData.getTotal());

        EnumEssFileType enumFileType = XEnumBase.of(EnumEssFileType.class);
        getR().put("file_type", enumFileType);
        return getR();
    }

    /**
     * 学习任务列表
     */
    @GetMapping("study_list")
    public XReturn studyList() {
        InputData inputData = InputData.fromRequest();
        String moduleId = inputData.input("module_id");
        XValidator.isNullOrEmpty(moduleId, "miss module_id");

        String[] select = {
                "estudy_id",
                "estudy_name",
                "estudy_date_begin",
                "estudy_date_end",
        };
        JoinWrapperPlus<EssStudy> query = SvcEssStudy.getBean().listQuery(inputData);
        String sqlExists = "SELECT 1 FROM wx_app_x.t_ess_study_student WHERE esstu_study_id = estudy_id AND esstu_uid = {0}";
        query.exists(sqlExists, getUid());
        query.select(select);
        IPage<EssStudy> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), true);
        //list
        IPage<Map<String, Object>> pageData = SvcEssStudy.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        //收集所有ID
        List<String> estudyIds = list.stream().map(item -> (String) item.get("estudy_id"))
                .collect(Collectors.toList());
        List<Map<String, Object>> listRecord = SvcEssStudy.getBean().getRecordList(estudyIds, getUid()); //子任务列表
        //
        for (Map<String, Object> item : list) {
            EssStudy entity = XBean.fromMap(item, EssStudy.class);
            item.put("id", entity.getKey());
            item.put("state", SvcEssStudy.getBean().getState(entity));
            //学生完成记录
            List<Map<String, Object>> record = listRecord.stream().filter(i -> i.get("esre_study_id").equals(entity.getKey()))
                    .collect(Collectors.toList());
            item.put("record", record);
        }

        getR().setData("total", pageData.getTotal());
        getR().setData("list", list);

        return getR();
    }

    /**
     * 学习任务完成
     */
    @GetMapping("study_task_done")
    public XReturn studyTaskDone(@Validated VoEssStudyTaskDone vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);
        //
        return SvcEssStudy.getBean().studyTaskDone(vo);
    }

    /**
     * 评测列表
     */
    @GetMapping("test_list")
    public XReturn testList() {
        InputData inputData = InputData.fromRequest();

        String[] select = {
                "etest_id",
                "etest_title",
                "etest_date_begin",
                "etest_date_end",
                "etest_fun_total",
                "etest_state",
                "etest_state_date",
                "etres_id",
                "etres_submit",
                "etres_submit_minute",
                "etres_test_fun",
                "etres_submit_date_end",
        };
        JoinWrapperPlus<EssTest> query = SvcEssTest.getBean().listQuery(inputData);
        query.join(EssTestResult.table(), EssTest.tableKey(), "etres_test_id");
        query.eq("etres_uid", getUid()); //指定老师
        query.select(select);
        IPage<EssTest> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), true);
        //list
        IPage<Map<String, Object>> pageData = SvcEssTest.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        for (Map<String, Object> item : list) {
            EssTest entity = XBean.fromMap(item, EssTest.class);
            EssTestResult entityResult = XBean.fromMap(item, EssTestResult.class);
            item.put("id", entity.getKey());
            //状态
            XReturn rState = SvcEssTest.getBean().getState(entity);
            item.put("state", rState);
            //离结束还剩余X分钟
            item.put("date_end_left_minute", entity.dateEndLeftMinute());
            //提交剩余X秒
            item.put("submit_left_second", entityResult.submitLeftSecond());
        }

        getR().setData("list", list);
        getR().setData("total", pageData.getTotal());

        return getR();
    }

    /**
     * 评价开始
     */
    @PostMapping("test_start")
    public XReturn testStart(@Validated VoEssTestStartTest vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);
        //
        return SvcEssTest.getBean().startTest(vo);
    }

    /**
     * 评价提交答案
     */
    @PostMapping("test_answer")
    public XReturn testAnswer(@Validated @RequestBody VoEssTestAnswer vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);
        //
        return SvcEssTest.getBean().answer(vo);
    }

    /**
     * 评测详情
     */
    @GetMapping("test_result_info")
    public XReturn testResultInfo() {
        InputData inputData = InputData.fromRequest();
        String resultId = inputData.input("result_id");
        XValidator.isNullOrEmpty(resultId, "miss result_id");

        EssTestResult entityResult = EssTestResultService.getBean().getEntity(resultId);
        XValidator.isNull(entityResult, "result_id异常");
        //老师也能访问
        //if (!entityResult.getEtres_uid().equals(getUid())) {
        //    return EnumSys.ERR_VAL.getR("用户异常");
        //}

        String[] select = {
                "etqu_id",
                "etqu_type",
                "etqu_module_id",
                "etqu_question_type",
                "etqu_question",
                "etqu_question_item",
                "etqu_level",
                "etqu_answer",
                "etri_test_fun",
                "etri_answer_state",
                "etri_answer",
                "emod_id",
                "emod_name",
        };
        JoinWrapperPlus<EssQuestion> query = SvcEssQuestion.getBean().listQuery(inputData);
        query.join(EssTestResultItem.table(), "etri_question_id", EssQuestion.tableKey());
        query.join(EssModule.table(), EssModule.tableKey(), "etqu_module_id");
        query.select(select);
        query.eq("etri_result_id", resultId);
        query.orderByAsc("etri_id");
        //list
        List<Map<String, Object>> list = SvcEssQuestion.getApp().getMaps(query);
        //
        //
        Map<String, Object> mapModule = new HashMap<>();
        for (Map<String, Object> item : list) {
            String moduleId = (String) item.get("emod_id");
            String moduleName = (String) item.get("emod_name");
            mapModule.put(moduleId, moduleName);
        }
        //
        for (Map<String, Object> item : list) {
            //EssQuestion entity = XBean.fromMap(item, EssQuestion.class);
            //item.put("id", entity.getKey());

            if (entityResult.getEtres_send_fun() != 1) { //未公布，不显示正确答案
                item.put("etqu_answer", null);
            }
        }

        Map<String, Object> mapResult = XBean.toMap(entityResult, new String[]{
                "etres_id",
                "etres_submit",
                "etres_submit_date",
                "etres_submit_minute",
                "etres_submit_date_end",
                "etres_end",
                "etres_test_fun",
                "etres_send_fun",
        });
        mapResult.put("left_second", entityResult.submitLeftSecond()); //考试剩余时间

        getR().setData("list", list);
        getR().setData("result", mapResult); //试卷
        //评测设置
        EssTest entityTest = SvcEssTest.getApp().getEntity(entityResult.getEtres_test_id());
        Map<String, Object> mapTest = XBean.toMap(entityTest, new String[]{
                "etest_id",
                "etest_title",
                "etest_test_not_submit",
                "etest_test_minute",
                "etest_random",
                "etest_score_show",
                "etest_date_begin",
                "etest_date_end",
                "etest_fun_total",
                "etest_question_total",
        });
        mapTest.put("date_end_left_minute", entityTest.dateEndLeftMinute());
        getR().setData("test", mapTest);
        //场景
        getR().setData("module", mapModule);

        return getR();
    }


    /**
     * 题库
     */
    @GetMapping("question_list")
    public XReturn questionList() {
        InputData inputData = InputData.fromRequest();

        EssUser entityUser = SvcEssUser.getBean().getEntity(getUid());

        String[] select = {
                "etqu_id",
                "etqu_type",
                "etqu_module_id",
                "etqu_question_type",
                "etqu_question",
                "etqu_question_item",
                "etqu_level",
                "etqu_answer",
                "etqu_level",
                "etqu_answer_total",
                "etqu_answer_bingo_total",
                "etqu_order",
                "emod_id",
                "emod_name",
        };
        JoinWrapperPlus<EssQuestion> query = SvcEssQuestion.getBean().listQuery(inputData);
        query.join(EssModule.table(), EssModule.tableKey(), "etqu_module_id");
        query.select(select);
        if (!SvcEssUser.getBean().isTeacher(entityUser)) {
            query.eq("etqu_type", 1);
        }

        IPage<EssQuestion> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), true);
        //list
        IPage<Map<String, Object>> pageData = SvcEssQuestion.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        //
        Map<String, Object> mapModule = new HashMap<>();
        for (Map<String, Object> item : list) {
            String moduleId = (String) item.get("emod_id");
            String moduleName = (String) item.get("emod_name");
            mapModule.put(moduleId, moduleName);
        }
        //
        for (Map<String, Object> item : list) {
            EssQuestion entity = XBean.fromMap(item, EssQuestion.class);
            item.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", pageData.getTotal());
        getR().setData("module", mapModule);
        return getR();
    }


    /**
     * 积分排行榜
     */
    @GetMapping("user_list")
    public XReturn userList() {
        InputData inputData = InputData.fromRequest();

        SysUser entityUser = SvcSysUser.getBean().getEntity(getUid());

        String[] select = {
                "user_id",
                "user_name",
                "user_update_date",
                "user_fun",
        };
        JoinWrapperPlus<EssUser> query = SvcEssUser.getBean().listQuery(inputData);
        query.gt("user_fun", 0)
                .eq("euser_type", 0);
        query.select(select);
        query.orderByDesc("user_fun")
                .orderByAsc("user_update_date");
        Long rankIndex = -1L;
        //用户排名
        if (entityUser.getUser_fun() > 0) {
            JoinWrapperPlus<EssUser> queryRank = query.cloneJoinPlus();
            queryRank.and(
                    q -> q.gt("user_fun", entityUser.getUser_fun())
                            .or(qq -> qq.eq("user_fun", entityUser.getUser_fun())
                                    .lt("user_update_date", entityUser.getUser_update_date())
                            )
            );
            rankIndex = SvcEssUser.getApp().count(queryRank);
        }

        IPage<EssUser> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = SvcEssUser.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        //
        for (Map<String, Object> item : list) {
        }

        getR().setData("list", list);
        getR().setData("rank_index", rankIndex); //积分排序
        return getR();
    }

    /**
     * 积分任务
     */
    @PostMapping("task_fun")
    public XReturn taskFun(@Validated VaTaskFun vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);
        //
        vo.setUid(getUid());
        return SvcEssSetting.getBean().taskFun(vo);
    }


    /**
     * 用户收藏
     */
    @PostMapping("collect")
    @Transactional(rollbackFor = Exception.class)
    public XReturn collect(@Validated VoUvSave vo, BindingResult result) {
        XValidator.isNullOrEmpty(vo.getType_id(), "miss type_id");
        List<String> types = List.of("file");
        if (!types.contains(vo.getType())) {
            result.addError(XValidator.getErr("type", "请输入指定值：" + types));
        }
        //
        XValidator.getResult(result, true, true);

        vo.setApp_id("app_ess");
        vo.setUid(getUid());
        XReturn r = SvcSysUv.getBean().save(vo);
        if (r.ok()) {
            if ("file".equals(vo.getType())) {
                //更新总数
                SvcEssFile.getBean().updateCollect(vo.getType_id());
            }
        }

        return r;
    }
}
