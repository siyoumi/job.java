package com.siyoumi.app.modules.app_menu.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_menu.vo.MenuListType;
import com.siyoumi.app.modules.app_menu.vo.MenuListVo;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/wxapp/app_menu/z_api")
public class app_menu__api
        extends WxAppApiController {
    //初始化
    @GetMapping()
    public XReturn index() {
        SysAbcService app = SysAbcService.getBean();
        //类型
        List<Map<String, Object>> listType = app.getListKV(XHttpContext.getAppId(), "app_menu_type");
        //食用方式及菜单数量
        JoinWrapperPlus<SysAbc> query = app.listQuery(XHttpContext.getAppId(), true);
        query.select("abc_pid", "COUNT(*) menu_count")
                .eq("abc_table", "app_menu_item")
                .groupBy("abc_pid");
        List<Map<String, Object>> listCount = app.getMaps(query);
        //
        List<Map<String, Object>> listTypeData = listType.stream().map(item -> {
            String id = (String) item.get("abc_id");
            item.put("count", 0);
            //菜单数量
            Map<String, Object> count = listCount.stream().filter(v ->
            {
                return v.get("abc_pid").equals(id);
            }).findFirst().orElse(null);
            if (count != null) {
                item.put("count", count.get("menu_count"));
            }

            return item;
        }).collect(Collectors.toList());
        getR().put("list_type", listTypeData);

        //烹饪方式
        List<Map<String, Object>> listType2 = app.getListKV(XHttpContext.getAppId(), "app_menu_type2"
                , "abc_id", "abc_name", "abc_pid");
        getR().put("list_type2", listType2);

        //过敏源
        List<Map<String, Object>> listType0 = app.getListKV(XHttpContext.getAppId(), "app_menu_type0");
        getR().put("list_type0", listType0);

        return getR();
    }

    //菜单列表
    @RequestMapping("/funcListMenu")
    public XReturn menuList(@RequestBody MenuListVo vo) {
        InputData inputData = InputData.fromRequest();


        SysAbcService app = SysAbcService.getBean();
        JoinWrapperPlus<SysAbc> query = app.listQuery(XHttpContext.getAppId(), false);
        //join
        query.leftJoin(SysAbc.table() + " AS type", "type.abc_id", "t_s_abc.abc_pid");
        //
        query.select("t_s_abc.abc_id",
                "t_s_abc.abc_name",
                "t_s_abc.abc_str_03",
                "t_s_abc.abc_str_02",
                "t_s_abc.abc_str_01",
                "type.abc_name type1_name"
        );
        query.eq("t_s_abc.abc_table", "app_menu_item");
        if (XStr.hasAnyText(vo.getPid())) { //食用方式
            query.eq("t_s_abc.abc_pid", vo.getPid());
        }
        if (XStr.hasAnyText(vo.getName())) { //名称
            query.like("t_s_abc.abc_name", vo.getName());
        }

        if (vo.getList2() != null) { //烹饪方式
            List<MenuListType> type2 = vo.getList2().stream()
                    .filter(MenuListType::getChecked)
                    .collect(Collectors.toList());
            if (type2.size() > 0) {
                query.and(q -> {
                    type2.forEach(item -> {
                        q.or().like("t_s_abc.abc_str_02", item.getName());
                    });
                });
            }
        }

        if (vo.getList0() != null) { //过敏源
            List<MenuListType> type0 = vo.getList0().stream()
                    .filter(MenuListType::getChecked).collect(Collectors.toList());
            if (type0.size() > 0) {
                query.and(q -> {
                    type0.forEach(item -> {
                        q.or().like("t_s_abc.abc_str_03", item.getName());
                    });
                });
            }
        }


        IPage<SysAbc> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = app.getMaps(page, query);
        List<Map<String, Object>> listData = pageData.getRecords();
        List<Map<String, Object>> list = listData.stream().map(item -> {
            //处理
            String str02 = (String) item.get("abc_str_02");
            String[] str02Arr = str02.split(",");
            item.put("abc_str_02", str02Arr);

            return item;
        }).collect(Collectors.toList());

        getR().setData("list", list);

        return getR();
    }

    //菜单详情
    @GetMapping("/funcMenuInfo")
    public XReturn menuInfo() {
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }

        SysAbc entityAbc = SysAbcService.getBean().getEntityByTable(getID(), "app_menu_item");
        if (entityAbc == null) {
            return EnumSys.ERR_VAL.getR("id异常");
        }

        getR().setData("entity", entityAbc);

        return getR();
    }
}
