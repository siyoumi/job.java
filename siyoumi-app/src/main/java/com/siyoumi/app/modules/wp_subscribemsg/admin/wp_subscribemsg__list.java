package com.siyoumi.app.modules.wp_subscribemsg.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.WxSubscribemsg;
import com.siyoumi.app.service.WxSubscribemsgService;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.component.http.XHttpContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/wp_subscribemsg/wp_subscribemsg__list")
public class wp_subscribemsg__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("订阅消息列表");


        String compKw = input("compKw");


        WxSubscribemsgService app = WxSubscribemsgService.getBean();

        //query
        QueryWrapper<WxSubscribemsg> query = app.q();
        query.eq("wxsg_x_id", XHttpContext.getX())
                .orderByDesc("id")
        ;
        if (XStr.hasAnyText(compKw)) //模板名称
        {
            query.like("wxsg_name", compKw);
        }

        IPage<WxSubscribemsg> page = new Page<>(getPageIndex(), getPageSize());

        IPage<WxSubscribemsg> pageData = app.get(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<WxSubscribemsg> listData = pageData.getRecords();
        List<Map<String, Object>> list = listData.stream().map(item ->
        {
            Map<String, Object> map = item.toMap();
            map.put("id", item.getKey());

            return map;
        }).collect(Collectors.toList());


        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(List<String> ids) {
        WxSubscribemsgService app = WxSubscribemsgService.getBean();

        if (ids.size() <= 0) {
            return XReturn.getR(EnumSys.MISS_VAL.getErrcode(), "miss ids");
        }

        app.delete(ids);


        return getR();
    }
}
