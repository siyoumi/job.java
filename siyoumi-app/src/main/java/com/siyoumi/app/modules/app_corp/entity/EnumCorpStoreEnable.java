package com.siyoumi.app.modules.app_corp.entity;

import com.siyoumi.util.IEnum;


public enum EnumCorpStoreEnable
        implements IEnum {
    //0|待审核,1|审核已通过,10|审核不通过
    WAIT(0, "待审核"),
    PASS(1, "审核已通过"),
    NOTPASS(10, "审核不通过"),
    ;


    private Integer key;
    private String val;

    EnumCorpStoreEnable(Integer key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key.toString();
    }
}
