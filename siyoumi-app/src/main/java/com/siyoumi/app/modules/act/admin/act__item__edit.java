package com.siyoumi.app.modules.act.admin;

import com.siyoumi.app.entity.ActSet;
import com.siyoumi.app.entity.SysItem;
import com.siyoumi.app.modules.act.service.SvcActSet;
import com.siyoumi.app.modules.act.vo.VaActSet;
import com.siyoumi.app.modules.app_admin.vo.SysItemVo;
import com.siyoumi.app.service.ActSetService;
import com.siyoumi.app.service.SysItemService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/act/act__item__edit")
public class act__item__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("活动-编辑");

        SvcActSet app = SvcActSet.getBean();
        SysItemService appItem = SysItemService.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("aset_item_id", "def");
        data.put("aset_status", 0);
        data.put("aset_order", 0);
        if (isAdminEdit()) {
            ActSet entity = SvcActSet.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            if (XStr.hasAnyText(entity.getAset_item_id())) {
                SysItem entityItem = appItem.getEntity(entity.getAset_item_id());
                // 模板属性-值
                Map<String, String> itemData = appItem.getItemData(entityItem.getKey(), entity.getKey());
                itemData.forEach((k, v) -> {
                    dataAppend.put("data_" + k, v);
                });
                // 模板属性-项
                setPageInfo("sys_items", entityItem.commonData(SysItemVo.class));
            }
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        data.put("ad", input("ad", ""));
        getR().setData("data", data);

        // 属性模板
        List<SysItem> listItem = appItem.listByAppId("act");
        setPageInfo("list_item", listItem);

        return getR();
    }


    @PostMapping("/save")
    public XReturn save(@Validated() VaActSet vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        ActSetService app = ActSetService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("aset_x_id");
        }

        InputData inputData = InputData.fromRequest();
        inputData.putAll(XBean.toMap(vo));
        
        return XApp.getTransaction().execute(status -> {
            return app.saveEntity(inputData, vo, true, ignoreField);
        });
    }
}
