package com.siyoumi.app.modules.app_show.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.JobOrder;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.modules.app_show.entity.EnumJobOrderSrc;
import com.siyoumi.app.modules.app_show.entity.EnumJobOrderState;
import com.siyoumi.app.modules.app_show.entity.EnumJobOrderType;
import com.siyoumi.app.modules.app_show.service.SvcJobOrder;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import org.hibernate.validator.internal.util.StringHelper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/app_show/app_show__job_order__list")
public class app_show__job_order__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("订单-列表");

        String appId = XHttpContext.getAppId();

        InputData inputData = InputData.fromRequest();

        SvcJobOrder svcJobOrder = SvcJobOrder.getBean();

        String[] select = {
                "jorder_id",
                "jorder_create_date",
                "jorder_type",
                "jorder_name",
                "jorder_banner",
                "jorder_price_total",
                "jorder_price_sales",
                "jorder_state",
                "jorder_src",
                "wxuser_openid",
                "wxuser_nickname",
        };
        JoinWrapperPlus<JobOrder> query = svcJobOrder.listQuery();
        query.select(select);
        query.join(WxUser.table(), "wxuser_openid", "jorder_openid");


        IPage<JobOrder> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcJobOrder.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            JobOrder entity = XBean.fromMap(data, JobOrder.class);
            data.put("id", entity.getKey());
            //状态
            String state = IEnum.getEnmuVal(EnumJobOrderState.class, entity.getJorder_state());
            data.put("state", state);
            //类型
            String type = IEnum.getEnmuVal(EnumJobOrderType.class, entity.getJorder_type());
            data.put("type", type);
            //来源
            String src = IEnum.getEnmuVal(EnumJobOrderSrc.class, entity.getJorder_src());
            data.put("src", src);
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcJobOrder.getBean().delete(Arrays.asList(ids));
    }
}
