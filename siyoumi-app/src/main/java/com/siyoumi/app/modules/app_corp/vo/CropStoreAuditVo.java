package com.siyoumi.app.modules.app_corp.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CropStoreAuditVo {
    List<String> ids;
    @NotNull
    Integer enable;
}
