package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;


public class EnumBookItemType
        extends XEnumBase<String> {
    @Override
    protected void initKV() {
        put("device", "房源设施");
        put("play", "娱乐设施");
        put("province", "省");
        put("city", "市");
        put("customer", "客服");
    }
}
