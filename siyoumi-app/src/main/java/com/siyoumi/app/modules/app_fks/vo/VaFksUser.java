package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//用户
@Data
public class VaFksUser {
    private String fuser_id;

    @HasAnyText(message = "请选择部门")
    private String fuser_department_id;
    @HasAnyText(message = "请选择职位")
    private String fuser_position_id;

    //@HasAnyText
    @Size(max = 50)
    private String user_pwd;
    @HasAnyText(message = "请输入用户名")
    @Size(max = 50)
    private String user_name;
    //@HasAnyText
    @Size(max = 200)
    private String user_headimg;
    @HasAnyText(message = "缺少手机号")
    @Size(max = 50)
    private String user_phone;
    @HasAnyText(message = "缺少手机号ENC")
    private String user_phone_enc;
    private String user_openid;
}
