package com.siyoumi.app.modules.mall2.vo;

import lombok.Data;

//下单接口子项
@Data
public class OrderItemVo {
    String sku_id;
    String cart_id;
    Integer count;

    //后续赋值
    SkuData skuData;
}
