package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//项
@Data
public class VaBookItem {
    private String bitem_id;
    private String bitem_acc_id;
    private String bitem_pid;

    @HasAnyText
    @EqualsValues(vals = {"device", "play", "province", "city", "customer"})
    private String bitem_type;
    @Size(max = 50)
    @HasAnyText
    private String bitem_name;
    @Size(max = 200)
    private String bitem_pic;
    @Size(max = 50)
    private String bitem_str_00;
    @HasAnyText
    private Integer bitem_order;

    static public VaBookItem of(String type, String pid, String name, Integer order) {
        VaBookItem vo = new VaBookItem();
        vo.setBitem_type(type);
        vo.setBitem_pid(pid);
        vo.setBitem_name(name);
        vo.setBitem_order(order);
        return vo;
    }
}
