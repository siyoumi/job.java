package com.siyoumi.app.modules.redbook.admin;

import com.siyoumi.app.entity.RedbookQuan;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.modules.redbook.entity.EnumRedbookQuanType;
import com.siyoumi.app.modules.redbook.service.SvcRedbookQuan;
import com.siyoumi.app.modules.redbook.vo.VaRedBookQuan;
import com.siyoumi.app.service.RedbookQuanService;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/redbook/redbook__item__edit")
public class redbook__item__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("小红书-编辑");

        RedbookQuanService app = SvcRedbookQuan.getApp();

        Map<String, Object> data = new HashMap<>();
        data.put("rquan_type", 0);
        data.put("rquan_enable", 0);
        if (isAdminEdit()) {
            RedbookQuan entity = app.loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            if (XStr.hasAnyText(entity.getRquan_uid())) {
                WxUser entityWxUser = WxUserService.getBean().getByOpenid(entity.getRquan_uid());
                dataAppend.put("wxuser_nickname", entityWxUser.getWxuser_nickname());
                dataAppend.put("wxuser_headimgurl", entityWxUser.getWxuser_headimgurl());
            }
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
            data.put("rquan_type", entity.getRquan_type().toString());
        }
        getR().setData("data", data);


        EnumRedbookQuanType enumRedbookQuanType = XEnumBase.of(EnumRedbookQuanType.class);
        //类型
        setPageInfo("types", enumRedbookQuanType);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaRedBookQuan vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //自定义验证
        WxUser entityWxUser = WxUserService.getBean().getByOpenid(vo.getRquan_uid());
        if (entityWxUser == null) {
            result.addError(XValidator.getErr("rquan_openid", "openid非法"));
        }
        XValidator.getResult(result);


        RedbookQuanService app = SvcRedbookQuan.getApp();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("rquan_group_id");
            ignoreField.add("rquan_openid");
        }

        InputData inputData = InputData.fromRequest();

        return app.saveEntity(inputData, vo, true, ignoreField);
    }
}

