package com.siyoumi.app.modules.website_fjciecc.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

//社会责任
@Data
public class VaFjcieccSocial {
    @HasAnyText
    @Size(max = 100)
    String abc_name;

    String abc_app_id;
    String abc_table;

    @Size(max = 200)
    String abc_str_00;
    LocalDateTime abc_date_00;

    String abc_txt_00;
    Integer abc_order;
}

