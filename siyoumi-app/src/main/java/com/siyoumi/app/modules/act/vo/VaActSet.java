package com.siyoumi.app.modules.act.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class VaActSet {
    @NotBlank
    @Size(max = 50)
    private String aset_name;

    private String aset_item_id;

    @NotNull(message = "不能为空")
    private LocalDateTime aset_begin_date;
    @NotNull(message = "不能为空")
    private LocalDateTime aset_end_date;
    private Integer aset_win_multi;
    @Size(max = 2000)
    private String aset_desc;

    private Integer aset_status;
    private Integer aset_order;
}
