package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//取消分班
@Data
public class VoEssCancelClass {
    @HasAnyText
    private List<String> ids;
}
