package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.util.XStr;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//资源文件
@Data
public class VaEssFile {
    private String efile_acc_id;
    @HasAnyText
    private String efile_module_id;
    @HasAnyText
    private String efile_type;

    @HasAnyText
    @Size(max = 50)
    private String efile_name;
    @HasAnyText
    @Size(max = 200)
    private String efile_path;
    @Size(max = 50)
    private String efile_file_ext;

    private Long efile_file_size;
    private Integer efile_order;

    public String fileName() {
        return XStr.concat(getEfile_name(), ".", getEfile_file_ext());
    }
}
