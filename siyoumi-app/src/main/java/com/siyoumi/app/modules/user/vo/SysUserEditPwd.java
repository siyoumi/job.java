package com.siyoumi.app.modules.user.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class SysUserEditPwd {
    String uid;

    @HasAnyText(message = "请输入新密码")
    @Size(max = 200, message = "密码最多200位字符")
    String user_pwd;
    @HasAnyText(message = "请输入旧密码")
    @Size(max = 200, message = "密码最多20位字符")
    String user_pwd_old;
}
