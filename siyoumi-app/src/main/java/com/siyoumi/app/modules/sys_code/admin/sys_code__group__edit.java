package com.siyoumi.app.modules.sys_code.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.sys_code.vo.VaCodeGroup;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/sys_code/sys_code__group__edit")
public class sys_code__group__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("码库-编辑");

        SysAbcService svcAbc = SysAbcService.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("abc_order", 0);
        if (isAdminEdit()) {
            SysAbc entity = svcAbc.getEntityByTable(getID(), "sys_code_group");

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }


    @PostMapping("/save")
    public XReturn save(@Validated() VaCodeGroup vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAbcService svcAbc = SysAbcService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_table");
            ignoreField.add("abc_app_id");
        }

        InputData inputData = InputData.fromRequest();
        if (isAdminAdd()) {
            vo.setAbc_table("sys_code_group");
            vo.setAbc_app_id(XHttpContext.getAppId());
        }

        inputData.putAll(XBean.toMap(vo));
        return svcAbc.saveEntity(inputData, vo, true, ignoreField);
    }
}
