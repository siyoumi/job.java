package com.siyoumi.app.modules.sys_log.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.modules.sys_log.entity.EnumSysLogTypeSub;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysLog;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysLogService;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/sys_log/sys_log__sys_error__list")
public class sys_log__sys_error__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("系统日志");

        String day = input("day", "90");

        SysLogService appLog = SysLogService.getBean();

        LocalDateTime dateBegin = XDate.today().minusDays(Long.valueOf(day));
        LocalDateTime dateEnd = XDate.today().plusDays(1L).minusSeconds(1L);

        InputData inputData = InputData.fromRequest();
        JoinWrapperPlus<SysLog> query = appLog.join();
        query.eq("log_x_id", XHttpContext.getX())
                .eq("log_type", "sys")
                .between("log_create_date", dateBegin, dateEnd)
                .orderByDesc("log_id");

        IPage<SysLog> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<SysLog> pageData = appLog.get(page, query);
        List<SysLog> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(entity ->
        {
            HashMap<String, Object> data = new HashMap<>(entity.toMap());
            data.put("id", entity.getKey());

            //子类型
            String typeSub = IEnum.getEnmuVal(EnumSysLogTypeSub.class, entity.getLog_type_sub());
            data.put("type_sub", typeSub);

            return data;
        }).collect(Collectors.toList());

        getR().setData("list", listData);
        getR().setData("count", count);
        return getR();
    }
}
