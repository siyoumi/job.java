package com.siyoumi.app.modules.website_fjciecc.service;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XApp;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class WebSiteFjcieccEditBase
        extends AdminApiController {
    public String title() {
        return "";
    }

    public String abcTable() {
        return "";
    }

    public void indexAfter() {
    }

    public void indexBefore(Map<String, Object> data) {
    }

    public XReturn index() {
        setPageTitle(title());

        SysAbcService svcAbc = SysAbcService.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("abc_order", 0);
        if (isAdminEdit()) {
            SysAbc entity = svcAbc.getEntityByTable(getID(), abcTable());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        indexBefore(data);
        getR().setData("data", data);

        indexAfter();

        return getR();
    }
}
