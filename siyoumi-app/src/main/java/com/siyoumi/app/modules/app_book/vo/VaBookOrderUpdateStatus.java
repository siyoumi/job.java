package com.siyoumi.app.modules.app_book.vo;

import lombok.Data;

//更新订单状态
@Data
public class VaBookOrderUpdateStatus {
    Integer status;
    String desc;

    static public VaBookOrderUpdateStatus of(Integer status, String desc) {
        VaBookOrderUpdateStatus vo = new VaBookOrderUpdateStatus();
        vo.setStatus(status);
        vo.setDesc(desc);
        return vo;
    }
}
