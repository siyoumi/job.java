package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//批量添加文件
@Data
public class FksFileAddBatch {
    private String uid; //作者

    private Integer type = 1; //【1】共享空间；【2】核心数据；
    //@EqualsValues(vals = {"common", "department", "person", "note"})
    @HasAnyText
    private String type_id; //共享空间类型
    //@EqualsValues(vals = {"room", "proj", "ecology", "city", "people"})
    private String data_type; //核心数据分类

    @EqualsValues(vals = {"0", "1"})
    private Integer auth = 0;  //【0】全部可见；【1】部分可见；
    private List<String> auth_uids; //权限uid

    private List<FksFileAdd> file_list;
}
