package com.siyoumi.app.modules.book.service;

import com.siyoumi.app.entity.SysBookSku;
import com.siyoumi.app.entity.SysBookSkuDay;
import com.siyoumi.app.entity.SysStock;
import com.siyoumi.app.modules.book.vo.VaBookSkuDayBatch;
import com.siyoumi.app.service.BookSkuDayService;
import com.siyoumi.app.service.SysStockService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

//sku日期
@Slf4j
@Service
public class SvcSysBookSkuDate
        implements IWebService {
    static public SvcSysBookSkuDate getBean() {
        return XSpringContext.getBean(SvcSysBookSkuDate.class);
    }

    static public BookSkuDayService getApp() {
        return BookSkuDayService.getBean();
    }


    public SysBookSkuDay getEntity(String skuId, String date) {
        InputData inputData = InputData.getIns();
        inputData.put("sku_id", skuId);
        LocalDateTime b = XDate.parse(date);
        LocalDateTime e = XDate.parse(date).plusDays(1).minusSeconds(1);
        inputData.put("date_begin", XDate.toDateString(b));
        inputData.put("date_end", XDate.toDateString(e));
        inputData.put("all", "1");

        return getApp().first(listQuery(inputData));
    }

    public JoinWrapperPlus<SysBookSkuDay> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<SysBookSkuDay> listQuery(InputData inputData) {
        String skuId = inputData.input("sku_id");
        String dateBegin = inputData.input("date_begin");
        String dateEnd = inputData.input("date_end");
        String all = inputData.input("all");

        JoinWrapperPlus<SysBookSkuDay> query = getApp().join();
        query.join(SysStock.table(), SysBookSkuDay.tableKey(), "stock_id_src");
        query.eq("bday_x_id", XHttpContext.getX());

        if ("1".equals(all)) { //显示全部

        } else {
            query.eq("bday_del", 0);
        }
        if (XStr.hasAnyText(skuId)) { //sku_id
            query.eq("bday_sku_id", skuId);
        }
        if (XStr.hasAnyText(dateBegin) && XStr.hasAnyText(dateEnd)) { //天数
            LocalDateTime b = XDate.parse(dateBegin);
            LocalDateTime e = XDate.parse(dateEnd);
            query.between("bday_date", b, e);
        }

        return query;
    }

    /**
     * 批量编辑
     *
     * @param vo
     */
    @SneakyThrows
    public XReturn batch(VaBookSkuDayBatch vo) {
        XValidator.checkTransaction();

        SysBookSku entitySku = SvcSysBookSku.getApp().first(vo.getSku_id());

        LocalDateTime dateBegin = XDate.parse(vo.getDate_begin());
        LocalDateTime dateEnd = XDate.parse(vo.getDate_end());
        List<String> dates = XDate.getStringList(dateBegin, dateEnd, 0);
        for (String date : dates) {
            SysBookSkuDay entityDay = getEntity(vo.getSku_id(), date);
            if (entityDay == null) {
                entityDay = new SysBookSkuDay();
                entityDay.setBday_acc_id(vo.getAcc_id());
                entityDay.setBday_x_id(XHttpContext.getX());
                entityDay.setBday_sku_id(entitySku.getKey());
                entityDay.setBday_book_id(entitySku.getBsku_book_id());
                entityDay.setBday_date(XDate.parse(date));
                entityDay.setAutoID();
                getApp().save(entityDay);
            }

            SysBookSkuDay entityDayUpdate = new SysBookSkuDay();
            entityDayUpdate.setBday_id(entityDay.getKey());
            entityDayUpdate.setBday_del(0L);
            if (vo.getSet_before() == 1) {
                //截止时间
                entityDayUpdate.setBday_before_enable(vo.getBefore_enable());
                entityDayUpdate.setBday_before_time(vo.getBefore_m());
            }
            if (vo.getSet_append_max() == 1) {
                //同行人
                entityDayUpdate.setBday_append_max(vo.getAppend_max());
            }
            getApp().saveOrUpdatePassEqualField(entityDay, entityDayUpdate);

            //库存
            SysStock entityStock = SysStockService.getBean().getEntityBySrc(entityDay.getKey(), false);
            if (entityStock == null) {
                entityStock = new SysStock();
                entityStock.setStock_x_id(XHttpContext.getX());
                entityStock.setStock_id_src(entityDay.getKey());
                entityStock.setStock_count_left(0L);

                SysStockService.getBean().save(entityStock);
                entityStock = SysStockService.getBean().getEntityBySrc(entityDay.getKey(), false);
            }
            if (vo.getSet_left() == 1) {
                //更新库存
                if (vo.getLeft_count_old() == null) {
                    //批量修改，没有old库存
                    vo.setLeft_count_old(entityStock.getStock_count_left());
                }
                SysStockService.getBean().updateLeft(entityDay.getKey(), vo.getLeft_count(), vo.getLeft_count_old());
            }
        }

        return XReturn.getR(0);
    }

    /**
     * 删除
     */
    public XReturn delete(List<String> ids) {
        JoinWrapperPlus<SysBookSkuDay> query = listQuery();
        query.in(SysBookSkuDay.tableKey(), ids)
                .eq("bday_del", 0);

        List<SysBookSkuDay> list = getApp().get(query);
        XApp.getTransaction().execute(status -> {
            for (SysBookSkuDay entity : list) {
                getApp().delete(entity.getKey());
            }

            return null;
        });


        return EnumSys.OK.getR();
    }
}
