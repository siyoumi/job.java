package com.siyoumi.app.modules.book.vo;

import lombok.Data;

//预约提交-同行人
@Data
public class VaBookSumbitAppend {
    String brecord_name;
    String brecord_phone;
    String brecord_idcard;
}
