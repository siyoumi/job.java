package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;


//评测复制
@Data
public class VoEssTestCopy {
    @HasAnyText(message = "缺少评测ID")
    String test_id;

    @HasAnyText(message = "缺少名称")
    @Size(max = 50, message = "名称长度不能超50")
    String test_title;
    @HasAnyText
    List<String> teacher_uids;
}
