package com.siyoumi.app.modules.fun_mall.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FunMallGroup;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.fun_mall.service.SvcFunMallGroup;
import com.siyoumi.app.modules.mall2.service.SvcM2Group;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/fun_mall/fun_mall__group__list")
public class fun_mall__group__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("分组列表");

        InputData inputData = InputData.fromRequest();

        JoinWrapperPlus<FunMallGroup> query = SvcFunMallGroup.getBean().listQuery(inputData);

        IPage<FunMallGroup> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcFunMallGroup.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            FunMallGroup entity = SvcFunMallGroup.getApp().loadEntity(data);
            data.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);
        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcFunMallGroup.getBean().delete(List.of(ids));
    }
}
