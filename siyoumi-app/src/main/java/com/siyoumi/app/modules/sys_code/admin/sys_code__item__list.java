package com.siyoumi.app.modules.sys_code.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.RedbookQuan;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.entity.SysCode;
import com.siyoumi.app.modules.redbook.entity.EnumRedbookQuanEnable;
import com.siyoumi.app.modules.redbook.entity.EnumRedbookQuanType;
import com.siyoumi.app.modules.redbook.service.SvcRedbookQuan;
import com.siyoumi.app.modules.sys_code.entity.EnumUse;
import com.siyoumi.app.modules.sys_code.service.SvcCode;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.app.service.SysCodeService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/sys_code/sys_code__item__list")
public class sys_code__item__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        InputData inputData = InputData.fromRequest();
        String key = inputData.input("key");
        if (XStr.isNullOrEmpty(key)) {
            return EnumSys.MISS_VAL.getR("缺少码库ID");
        }
        SysAbc entityGroup = SysAbcService.getBean().getEntityByTable(key, "sys_code_group");
        setPageTitle(entityGroup.getAbc_name(), "-兑换码列表");

        String appId = XHttpContext.getAppId();

        JoinWrapperPlus<SysCode> query = SvcCode.getBean().listQuery(appId, inputData);
        IPage<SysCode> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), !isAdminExport());
        //list
        IPage<Map<String, Object>> pageData = SvcCode.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            SysCode entity = SvcCode.getApp().loadEntity(data);
            data.put("id", entity.getKey());

            data.put("exprire", entity.expire());
        }

        setPageInfo("arr_use", IEnum.toMap(EnumUse.class));

        if (!isAdminExport()) {
            getR().setData("list", list);
            getR().setData("count", count);
        } else {
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("code_code", "兑换码");
            tableHead.put("code_vaild_date_begin", "有效期");
            tableHead.put("code_vaild_date_end", "有效期");
            tableHead.put("code_use", "使用状态");
            tableHead.put("code_use_date", "使用时间");
            tableHead.put("code_create_date", "创建日期");

            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }

        return getR();
    }


    //删除
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcCode.getBean().delete(Arrays.asList(ids));
    }


    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }

}
