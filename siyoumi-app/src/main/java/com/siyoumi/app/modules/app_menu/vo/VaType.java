package com.siyoumi.app.modules.app_menu.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class VaType {
    @NotBlank
    @Size(max = 200)
    private String abc_name;
}
