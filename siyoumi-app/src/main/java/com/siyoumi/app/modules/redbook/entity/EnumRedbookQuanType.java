package com.siyoumi.app.modules.redbook.entity;

import com.siyoumi.component.XEnumBase;

//类型
public class EnumRedbookQuanType
        extends XEnumBase<Integer> {
    static public EnumRedbookQuanType of() {
        return of(EnumRedbookQuanType.class);
    }

    @Override
    protected void initKV() {
        put(0, "图片");
        put(1, "视频");
    }
}
