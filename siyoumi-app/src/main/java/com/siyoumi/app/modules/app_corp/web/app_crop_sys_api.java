package com.siyoumi.app.modules.app_corp.web;

import com.siyoumi.app.modules.app_corp.service.SvcCropStore;
import com.siyoumi.app.modules.app_corp.vo.VaCropStore;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.ApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/sys/app_corp/api")
public class app_crop_sys_api
        extends ApiController {
    //商家信息
    @GetMapping("store_info")
    public XReturn storeInfo() {
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }

        return SvcCropStore.getBean().info(getID());
    }
}
