package com.siyoumi.app.modules.app_admin.entity;

import com.siyoumi.util.IEnum;

//跳转方式
public enum EnumItemType
        implements IEnum {
    INPUT("input", "文本框"),
    IMG("img", "图片"),
    RADIO("radio", "单选项"),
    TEXTAREA("textarea", "文本域");


    private String key;
    private String val;

    EnumItemType(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
