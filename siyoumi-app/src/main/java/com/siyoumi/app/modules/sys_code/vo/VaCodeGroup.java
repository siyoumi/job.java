package com.siyoumi.app.modules.sys_code.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaCodeGroup {
    @HasAnyText
    @Size(max = 50)
    String abc_name;

    Integer abc_order;

    String abc_table;
    String abc_app_id;
}
