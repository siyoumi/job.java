package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FksCity;
import com.siyoumi.app.modules.account.service.SvcAccount;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__account__list")
public class app_book__account__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商家帐号列表");

        InputData inputData = InputData.fromRequest();

        JoinWrapperPlus<SysAccount> query = SvcAccount.getBean().listQuery(inputData);
        query.eq("acc_role", "store");

        IPage<SysAccount> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SysAccountService.getBean().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> item : list) {
            SysAccount entityAcc = SysAccountService.getBean().loadEntity(item);

            item.put("id", entityAcc.getKey());
            item.put("uid", SysAccountService.getBean().getUid(entityAcc));
            item.put("vaild", SysAccountService.getBean().isValid(entityAcc));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcAccount.getBean().delete(getIds());
    }
}
