package com.siyoumi.app.modules.iot.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.IotDevice;
import com.siyoumi.app.entity.IotProduct;
import com.siyoumi.app.modules.iot.service.SvcIotDevice;
import com.siyoumi.app.modules.iot.service.SvcIotProduct;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/iot/iot__device__list")
public class iot__device__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("产品-设备列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "idev_id",
                "idev_pro_id",
                "idev_num",
                "idev_key",
                "idev_create_date",
                "idev_order",
                "ipro_id",
                "ipro_name",
        };
        JoinWrapperPlus<IotDevice> query = SvcIotDevice.getBean().listQuery(inputData);
        query.join(IotProduct.table(), IotProduct.tableKey(), "idev_pro_id");
        query.select(select);
        query.orderByAsc("idev_order")
                .orderByDesc("idev_create_date");

        IPage<IotDevice> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcIotDevice.getApp().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            IotDevice entity = SvcIotDevice.getApp().loadEntity(data);
            data.put("id", entity.getKey());
            data.put("state", SvcIotDevice.getBean().getState(entity));
        }

        getR().setData("list", list);
        getR().setData("count", count);
        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcIotDevice.getBean().delete(List.of(ids));
    }
}
