package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

//批量加价
@Data
public class VaBookSetDay {
    private String bsday_acc_id;
    @HasAnyText
    private String bspud_set_id;

    @HasAnyText
    private BigDecimal bsday_add_price_breakfast;
    @HasAnyText
    private BigDecimal bsday_add_price_lunch;
    @HasAnyText
    private BigDecimal bsday_add_price_dinner;

    @HasAnyText
    private LocalDateTime begin_date;
    @HasAnyText
    private LocalDateTime end_date;

    private List<Integer> week;
}
