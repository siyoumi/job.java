package com.siyoumi.app.modules.book.entity;

import lombok.Data;

//预约配置信息
@Data
public class BookInfo {
    String contact = "";
    String location = "";
    String info = "";
}
