package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

@Data
public class VoEssQuestionAddBatch {
    @HasAnyText
    private String etqu_module_id;
    @HasAnyText
    private Integer etqu_type; //类型
    private String etqu_acc_id;

    List<VoEssQuestion> upload_data;
}
