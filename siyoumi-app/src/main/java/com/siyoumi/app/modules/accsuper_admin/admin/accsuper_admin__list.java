package com.siyoumi.app.modules.accsuper_admin.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/accsuper_admin/accsuper_admin__list")
public class accsuper_admin__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("超管列表");

        String compKw = input("compKw");

        SysAccsuperConfigService app = SysAccsuperConfigService.getBean();
        //query
        JoinWrapperPlus<SysAccsuperConfig> query = app.join();
        //join
        query.leftJoin(SysAccount.table(), "acc_uid", "aconfig_id");
        query.select("acc_name", "t_s_accsuper_config.*")
                .orderByDesc("aconfig_create_date");
        if (XStr.hasAnyText(compKw)) //名称
        {
            query.eq("aconfig_id", compKw);
        }

        IPage<SysAccsuperConfig> page = new Page<>(getPageIndex(), getPageSize());

        IPage<Map<String, Object>> pageData = app.mapper().getMaps(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<Map<String, Object>> listData = pageData.getRecords();
        List<Map<String, Object>> list = listData.stream().map(item ->
        {
            item.put("id", item.get("aconfig_id"));

            return item;
        }).collect(Collectors.toList());


        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //重置密码-超管密码或者开发者
    @Transactional
    @RequestMapping({"/funcResetPwd", "/reset_pwd"})
    public XReturn resetPwd() {
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }
        String dev = input("dev", "1");

        SysAccsuperConfigService app = SysAccsuperConfigService.getBean();
        return app.resetPwd(getID(), "1".equals(dev));
    }
}
