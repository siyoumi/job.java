package com.siyoumi.app.modules.app_show.admin;

import com.siyoumi.app.entity.JobOrder;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_show.entity.EnumJobOrderState;
import com.siyoumi.app.modules.app_show.service.SvcJobOrder;
import com.siyoumi.app.modules.app_show.vo.VaJobOrder;
import com.siyoumi.app.modules.app_show.vo.VaShowItem;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_show/app_show__job_order__edit")
public class app_show__job_order__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("订单-编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("jorder_type", 0);
        data.put("jorder_rate", 0.1);
        data.put("jorder_state", "0");
        if (isAdminEdit()) {
            JobOrder entity = SvcJobOrder.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);

            data.put("jorder_state", entity.getJorder_state().toString());
        }
        getR().setData("data", data);

        //订单状态
        LinkedHashMap<String, String> mapJobOrderstate = IEnum.toMap(EnumJobOrderState.class);
        setPageInfo("map_state", mapJobOrderstate);

        return getR();
    }


    @PostMapping("/save")
    public XReturn save(@Validated() VaJobOrder vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        if (isAdminAdd()) {
            vo.setJorder_src("admin");
        }

        InputData inputData = InputData.fromRequest();
        return SvcJobOrder.getBean().edit(inputData, vo);
    }
}
