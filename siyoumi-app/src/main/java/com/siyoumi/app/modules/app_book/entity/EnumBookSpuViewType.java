package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;

//景色
public class EnumBookSpuViewType
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "无");
        put(1, "湖景");
        put(2, "海景");
        put(3, "园景");
        put(4, "街景");
        put(5, "楼景");
        put(6, "山景");
        put(7, "其他");
    }
}
