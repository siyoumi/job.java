package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;

//订单状态
public class EnumBookOrderStatus
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        //【0】待支付；【1】已支付；【5】待确认；【10】待入住；【15】入住中；【20】已退房；
        put(0, "待支付");
        put(1, "已支付");
        put(3, "关闭订单");
        put(5, "待确认");
        put(7, "确认拒绝");
        put(10, "待入住");
        put(15, "入住中");
        put(20, "已退房");
    }
}
