package com.siyoumi.app.modules.lucky_num.service;

import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.lucky_num.vo.VaLnumSessionPrize;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSet;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSetAdmin;
import com.siyoumi.app.sys.service.prize.PrizeSend;
import com.siyoumi.app.service.LnumSessionPrizeService;
import com.siyoumi.app.service.LnumWinService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SvcLNumSessionPrize
        implements IWebService {
    static public SvcLNumSessionPrize getBean() {
        return XSpringContext.getBean(SvcLNumSessionPrize.class);
    }

    static public LnumSessionPrizeService getApp() {
        return LnumSessionPrizeService.getBean();
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<LnumSessionPrize> listQuery(InputData inputData) {
        String prizeName = inputData.input("prize_name");
        String sessionId = inputData.input("session_id");

        JoinWrapperPlus<LnumSessionPrize> query = getApp().join();
        query.eq("lnsp_x_id", XHttpContext.getX());
        //排序
        query.orderByAsc("lnsp_order")
                .orderByDesc("lnsp_create_date");
        if (XStr.hasAnyText(prizeName)) { //名称
            query.like("lnsp_name", prizeName);
        }
        if (XStr.hasAnyText(sessionId)) { //奖品期
            query.eq("lnsp_session_id", sessionId);
        }

        return query;
    }

    /**
     * 奖品列表
     *
     * @param sessionId
     */
    public List<LnumSessionPrize> list(String sessionId) {
        InputData inputData = InputData.getIns();
        inputData.put("session_id", sessionId);
        JoinWrapperPlus<LnumSessionPrize> query = listQuery(inputData);
        return getApp().get(query);
    }

    public List<LnumSessionPrize> list(List<String> sessionIds) {
        if (sessionIds.size() <= 0) {
            return new ArrayList<>();
        }

        InputData inputData = InputData.getIns();
        JoinWrapperPlus<LnumSessionPrize> query = listQuery(inputData);
        query.in("lnsp_session_id", sessionIds);
        return getApp().get(query);
    }


    /**
     * 编辑
     *
     * @param inputData
     * @param vo
     * @return
     */
    @SneakyThrows
    public XReturn edit(InputData inputData, VaLnumSessionPrize vo) {
        LnumSession entitySession = SvcLNumSession.getApp().first(vo.getLnsp_session_id());
        if (entitySession.win()) {
            return XReturn.getR(20101, "已开奖");
        }

        LnumSessionPrize entity = null;
        LnumSessionPrize entityNew = new LnumSessionPrize();
        if (inputData.isAdminAdd()) {
            entityNew.setAutoID();
            entityNew.setLnsp_x_id(XHttpContext.getX());
        } else {
            entity = getApp().first(inputData.getID());
            if (entity == null) {
                return XReturn.getR(20020, "id异常");
            }
        }
        XBean.copyProperties(vo, entityNew);

        entity = getApp().saveOrUpdatePassEqualField(entity, entityNew);

        //发奖配置
        SvcSysPrizeSetAdmin.getIns(false, false).editSave(entity.getKey(), "lucky_num", vo);

        XReturn r = XReturn.getR(0);
        r.setData("entity", entity);
        return r;
    }

    /**
     * 删除
     */
    @SneakyThrows
    public XReturn delete(String sessionId, List<String> ids) {
        if (XStr.isNullOrEmpty(sessionId)) {
            return XReturn.getR(20253, "miss session_id");
        }

        LnumSession entitySession = SvcLNumSession.getApp().first(sessionId);
        if (entitySession == null) {
            return XReturn.getR(20263, "session_id error");
        }
        if (entitySession.win()) {
            return XReturn.getR(20101, "已开奖");
        }

        InputData inputData = InputData.getIns();
        inputData.put("session_id", entitySession.getKey());
        JoinWrapperPlus<LnumSessionPrize> query = listQuery(inputData);
        query.in(LnumSessionPrize.tableKey(), ids);

        List<LnumSessionPrize> list = getApp().list(query);
        List<String> prizeIds = list.stream().map(item -> item.getLnsp_id()).collect(Collectors.toList());

        getApp().delete(prizeIds);

        XReturn r = XReturn.getR(0);
        return r;
    }


    /**
     * 发奖
     *
     * @param winNum
     * @param entityPrize
     * @param prizeItemNum
     * @param numCount
     */
    public XReturn sendPrize(long winNum, LnumSessionPrize entityPrize, Integer prizeItemNum, Long numCount) {
        LnumNum entityNum = SvcLNumGroup.getBean().getEntityNum(entityPrize.getLnsp_group_id(), winNum);
        if (entityNum == null) {
            return XReturn.getR(20500, "中奖号码异常，" + winNum);
        }

        LnumWin entityWin = new LnumWin();
        entityWin.setAutoID();
        entityWin.setLnwin_x_id(entityNum.getLnum_x_id());
        entityWin.setLnwin_group_id(entityPrize.getLnsp_group_id());
        entityWin.setLnwin_session_id(entityPrize.getLnsp_session_id());
        entityWin.setLnwin_wxuser_id(entityNum.getLnum_wxuser_id());
        entityWin.setLnwin_num(entityNum.getLnum_num());
        entityWin.setLnwin_prize_id(entityPrize.getLnsp_id());
        entityWin.setLwin_wxuser_id__master(null);
        LnumWinService.getBean().save(entityWin);

        SysPrizeSet entityPrizeSet = SvcSysPrizeSet.getBean().getEntityByIdSrc(entityPrize.getLnsp_id());

        //奖品ID + 奖品系数
        String prizeKey = entityPrize.getLnsp_id() + "|" + prizeItemNum;
        if (true) {
            log.debug("好友发奖");
            //发奖
            PrizeSend prizeSend = PrizeSend.getBean(entityPrizeSet);
            XReturn r = prizeSend.send(prizeKey, entityWin.getLnwin_wxuser_id());
            XValidator.err(r);

            //更新中奖数量
//            SvcLNumUser.getBean().updateWinCount(entityWin.getLwin_group_id(), entityWin.getLwin_wxuser_id());
        }

        return EnumSys.OK.getR();
    }
}
