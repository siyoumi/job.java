package com.siyoumi.app.modules.sheet.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
public class SysSheetSaveData {
    @NotBlank
    @Size(max = 50)
    private String sheet_id;
    // 提交的内容
    @NotNull(message = "提交内容为空")
    private Map<String, String> json;
}
