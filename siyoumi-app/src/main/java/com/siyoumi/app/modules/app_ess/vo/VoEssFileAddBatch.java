package com.siyoumi.app.modules.app_ess.vo;

import lombok.Data;

import java.util.List;

@Data
public class VoEssFileAddBatch {
    Integer type; //类型
    String acc_id;
    List<VoEssQuestion> list;
}
