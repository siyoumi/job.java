package com.siyoumi.app.modules.app_show.entity;

import com.siyoumi.util.IEnum;

//订单类型
public enum EnumJobOrderType
        implements IEnum {
    WXAPP(0, "微信小程序"),
    H5(1, "H5");


    private Integer key;
    private String val;

    EnumJobOrderType(Integer key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key.toString();
    }
}
