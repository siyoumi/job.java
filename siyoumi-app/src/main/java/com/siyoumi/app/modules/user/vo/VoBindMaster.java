package com.siyoumi.app.modules.user.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;

//绑定主人
@Data
@Accessors(chain = true)
public class VoBindMaster {
    @HasAnyText(message = "miss master_uid")
    String master_uid;
    String openid;
}
