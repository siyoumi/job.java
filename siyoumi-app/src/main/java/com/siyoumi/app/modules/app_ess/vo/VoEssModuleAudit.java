package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//用户审核
@Data
public class VoEssModuleAudit {
    @HasAnyText
    private List<String> ids;
    @HasAnyText
    @EqualsValues(vals = {"0", "1"})
    private Integer enable;
}
