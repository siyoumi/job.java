package com.siyoumi.app.modules.sh.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.ShUser;
import com.siyoumi.app.entity.base.ShUserBase;
import com.siyoumi.app.modules.sh.service.SvcShCcbUser;
import com.siyoumi.app.service.ShUserService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/sh/sh__ccb_user__list")
public class sh__ccb_user__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("CCB用户-列表");

        String compKw = input("compKw");

        ShUserService app = ShUserService.getBean();
        JoinWrapperPlus<ShUser> query = app.getQuery(1);
        if (XStr.hasAnyText(compKw)) { //名称
            query.like(ShUser.F.suser_name, compKw);
        }

        InputData inputData = InputData.fromRequest();
        IPage<ShUser> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = app.getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> item : list) {
            ShUser entity = XBean.fromMap(item, ShUser.class);

            item.put("id", entity.getKey());
            item.put("left_hours", entity.leftExpireHours());
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        ShUserService app = ShUserService.getBean();
        //
        JoinWrapperPlus<ShUser> query = app.getQuery(1);
        query.in(ShUser.tableKey(), getIds())
                .eq(ShUserBase.F.suser_x_id, XHttpContext.getX());
        app.mapper().delete(query);

        return getR();
    }


    @GetMapping("/export_config")
    public void exportConfig() {
        String config = SvcShCcbUser.getBean().exportConfig();

        exportTxt("_docker_config", config);
    }
}
