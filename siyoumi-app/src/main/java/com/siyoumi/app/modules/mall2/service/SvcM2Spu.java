package com.siyoumi.app.modules.mall2.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.M2Spu;
import com.siyoumi.app.modules.mall2.vo.SpuAttrData;
import com.siyoumi.app.modules.mall2.vo.SpuAttrVo;
import com.siyoumi.app.modules.mall2.vo.VaM2Spu;
import com.siyoumi.app.service.M2SpuService;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;

import java.util.List;

//商品
public interface SvcM2Spu {
    static SvcM2Spu getBean() {
        return XSpringContext.getBean(SvcM2Spu.class);
    }

    static M2SpuService getApp() {
        return M2SpuService.getBean();
    }


    /**
     * 商品属性最大配置数量
     */
    static Integer getAttrCountMax() {
        return 3;
    }


    default JoinWrapperPlus<M2Spu> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    JoinWrapperPlus<M2Spu> listQuery(InputData inputData);

    /**
     * select
     *
     * @return query
     */
    XReturn list(InputData inputData, Boolean searchCount);

    /**
     * 商品详情
     *
     * @param spuId
     */
    XReturn info(String spuId);

    /**
     * 数量
     *
     * @param groupId
     */
    Long getCount(String groupId);

    /**
     * 删除
     */
    XReturn delete(List<String> ids);

    XReturn edit(InputData inputData, VaM2Spu vo);

    /**
     * 设置审核状态
     * 并更新上下架
     *
     * @param entitySpu
     * @param status    审核状态
     * @param wxFrom    管理员openid
     * @param desc      描述
     */
    XReturn setAuditStatus(M2Spu entitySpu, Long status, String wxFrom, String desc);

    /**
     * 获取商品规格属性列表
     *
     * @param entitySpu
     */
    List<SpuAttrVo> getListAttrSku(M2Spu entitySpu);

    /**
     * 保存商品规格属性
     */
    XReturn saveAttrSku(M2Spu entitySpu, List<SpuAttrVo> listAttr);


    /**
     * 删除商品属性
     *
     * @param attrIds 属性ID
     */
    XReturn deleteAttrSku(M2Spu entitySpu, List<String> attrIds);

    /**
     * 属性规格同步
     *
     * @param entitySpu
     */
    XReturn syncAttrSku(M2Spu entitySpu);

    /**
     * 属性规格变列表
     * 黄，红
     * 10，20
     * 变
     * 黄 10
     * 黄 20
     * 红 10
     * 红 20
     *
     * @param getSku true：获取sku数据，例：库存，价格
     */
    List<SpuAttrData> getListAttrData(List<SpuAttrVo> listAttrSku, Boolean getSku);
}
