package com.siyoumi.app.modules.fun_mall.admin;

import com.siyoumi.app.entity.FunMallGroup;
import com.siyoumi.app.modules.fun_mall.service.SvcFunMallGroup;
import com.siyoumi.app.modules.fun_mall.vo.VaFunMallGroup;
import com.siyoumi.app.service.M2GroupService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/fun_mall/fun_mall__group__edit")
public class fun_mall__group__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("分组-编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("fgroup_order", 0);
        if (isAdminEdit()) {
            FunMallGroup entity = SvcFunMallGroup.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }


    @PostMapping("/save")
    @Transactional(rollbackFor = Exception.class)
    public XReturn save(@Validated() VaFunMallGroup vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("fgroup_acc_id");
        } else {
            vo.setFgroup_acc_id(getAccId());
        }

        InputData inputData = InputData.fromRequest();
        return SvcFunMallGroup.getApp().saveEntity(inputData, vo, true, ignoreField);
    }
}
