package com.siyoumi.app.modules.app_show.service;

import com.siyoumi.app.entity.ActSet;
import com.siyoumi.app.entity.JobOrder;
import com.siyoumi.app.modules.app_show.vo.VaJobOrder;
import com.siyoumi.app.service.JobOrderService;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.component.XApp;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//项目
@Slf4j
@Service
public class SvcJobOrder {
    static public SvcJobOrder getBean() {
        return XSpringContext.getBean(SvcJobOrder.class);
    }

    static public JobOrderService getApp() {
        return JobOrderService.getBean();
    }


    public JoinWrapperPlus<JobOrder> listQuery() {
        return listQuery(InputData.getIns());
    }

    public JoinWrapperPlus<JobOrder> listQuery(InputData inputData) {
        String orderId = inputData.input("order_id");
        String openid = inputData.input("openid");
        String state = inputData.input("state");

        JoinWrapperPlus<JobOrder> query = SvcJobOrder.getApp().join();
        query.eq("jorder_x_id", XHttpContext.getX())
                .eq("jorder_del", 0);
        query.orderByDesc("jorder_id");

        if (XStr.hasAnyText(state)) { //订单状态
            query.eq("jorder_state", state);
        }
        if (XStr.hasAnyText(orderId)) { //订单号
            query.eq("jorder_id", orderId);
        }
        if (XStr.hasAnyText(openid)) { //用户
            query.eq("jorder_openid", openid);
        }

        return query;
    }


    /**
     * 编辑
     *
     * @param inputData
     * @param vo
     */
    public XReturn edit(InputData inputData, VaJobOrder vo) {
        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminAdd()) {
            vo.setJorder_id(XApp.getStrID());
        } else {
            ignoreField.add("jorder_src");
        }
        return SvcJobOrder.getApp().saveEntity(inputData, vo, false, ignoreField);
    }


    /**
     * 删除
     *
     * @param ids
     * @return
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        SvcJobOrder.getApp().update()
                .set("jorder_del", XDate.toS())
                .eq("jorder_x_id", XHttpContext.getX())
                .in(JobOrder.tableKey(), ids)
                .update();

        return r;
    }
}
