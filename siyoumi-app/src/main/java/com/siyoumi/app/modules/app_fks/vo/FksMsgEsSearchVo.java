package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class FksMsgEsSearchVo {
    @HasAnyText(message = "miss keyword")
    @Size(max = 100, message = "搜索关键词不能超100个字")
    String keyword;
    Integer type; //类型 【0】词条；【1】规划；【2】成果；
    Integer page;
    Integer pageSize;
}
