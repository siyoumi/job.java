package com.siyoumi.app.modules.prize.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAddress;
import com.siyoumi.app.entity.SysPrize;
import com.siyoumi.app.modules.prize.service.SvcSysPrize;
import com.siyoumi.app.sys.service.prize.PrizeUse;
import com.siyoumi.app.sys.service.prize.entity.PrizeUseData;
import com.siyoumi.app.service.SysAddressService;
import com.siyoumi.app.service.SysPrizeService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wxapp/prize/z_api")
public class ApiPrize
        extends WxAppApiController {
    /**
     * 我的奖品列表
     */
    @GetMapping("list")
    public XReturn list() {
        InputData inputData = InputData.fromRequest();
        String type = inputData.input("type");

        String[] select = {
                "prize_id",
                "prize_app_id",
                "prize_type",
                "prize_name",
                "prize_pic",
                "prize_id_src",
                "prize_begin_date",
                "prize_end_date",
                "prize_use_type",
                "prize_use",
                "prize_use_date",
                "prize_create_date",
                "prize_desc",
                "prize_user_name",
                "prize_user_phone",
                "prize_user_address",
                "prize_del",
                "prize_deliver_no",
                "prize_deliver_date",
        };
        JoinWrapperPlus<SysPrize> query = SvcSysPrize.getBean().listQuery(inputData);
        query.eq("prize_openid", getOpenid());
        query.select(select);

        if (XStr.hasAnyText(type)) {
            switch (type) {
                case "0": // 未使用
                    query.eq("prize_use", 0)
                            .gt("prize_end_date", XDate.now());
                    break;
                case "1": // 已领取
                    query.eq("prize_use", 1);
                    break;
                case "2": // 已过期
                    query.eq("prize_use", 0)
                            .lt("prize_end_date", XDate.now());
                    break;
            }
        }

        IPage<SysPrize> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = SvcSysPrize.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        for (Map<String, Object> item : list) {
            SysPrize entitySysPrize = XBean.fromMap(item, SysPrize.class);
            item.put("id", entitySysPrize.getKey());
            //过期
            item.put("is_expire", entitySysPrize.expire());
        }

        getR().setData("type", type);
        getR().setData("list", list);

        return getR();
    }

    /**
     * 奖品详情
     */
    @GetMapping("prize_info")
    public XReturn prizeInfo() {
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }

        SysPrizeService app = SysPrizeService.getBean();
        SysPrize entityPrize = app.getEntity(getID());
        if (entityPrize == null) {
            return EnumSys.ERR_VAL.getR("id异常");
        }

        if (!entityPrize.use() && entityPrize.getPrize_use_type().equals("2")) {
            // 需要发货
            // 默认地址
            SysAddress entityAddressDef = SysAddressService.getBean().getDef(getOpenid());
            getR().setData("address_def", entityAddressDef);
        }

        getR().setData("prize", entityPrize);
        getR().setData("is_expire", entityPrize.expire());

        return getR();
    }

    /**
     * 奖品使用
     */
    @PostMapping("use")
    @Transactional
    public XReturn use(@Validated() PrizeUseData vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true, true);

        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }
        SysPrizeService appPrize = SysPrizeService.getBean();
        SysPrize entityPrize = appPrize.getEntity(getID());
        if (entityPrize == null) {
            return EnumSys.ERR_VAL.getR("id 异常");
        }
        if (!entityPrize.getPrize_openid().equals(getOpenid())) {
            return EnumSys.ERR_OPENID.getR("不是本人");
        }

        PrizeUse prizeUse = PrizeUse.getBean(entityPrize);
        XReturn r = prizeUse.use(vo);

        return r;
    }
}

