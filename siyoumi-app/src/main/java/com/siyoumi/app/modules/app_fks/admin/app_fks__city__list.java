package com.siyoumi.app.modules.app_fks.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FksCity;
import com.siyoumi.app.modules.app_fks.service.SvcFksCtiy;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__city__list")
public class app_fks__city__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("城市列表");

        InputData inputData = InputData.fromRequest();

        JoinWrapperPlus<FksCity> query = SvcFksCtiy.getBean().listQuery(inputData);

        IPage<FksCity> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcFksCtiy.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> item : list) {
            FksCity entity = XBean.fromMap(item, FksCity.class);
            item.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcFksCtiy.getBean().delete(getIds());
    }
}
