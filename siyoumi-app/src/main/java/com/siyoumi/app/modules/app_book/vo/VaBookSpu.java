package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

//房源
@Data
public class VaBookSpu {
    private String bspu_id;
    private String bspu_acc_id;
    private String bspu_store_id;

    @HasAnyText
    @Size(max = 100)
    private String bspu_name;
    @Size(max = 200)
    private String bspu_logo;
    @Size(max = 500)
    private String bspu_banner;
    @HasAnyText
    private Long bspu_stock_max;
    @HasAnyText
    private Integer bspu_user_count;

    private Integer bspu_area;
    private String bspu_area_balcony;
    private Integer bspu_food_type;
    private Integer bspu_bed_12;
    private Integer bspu_bed_13;
    private Integer bspu_bed_18;
    private Integer bspu_toilet;
    private Integer bspu_toilet_type;
    private Integer bspu_view_type;

    private Integer bspu_order;

    private String bsput_txt_00;

    List<String> set_ids;
    Integer set_do = 0;

    List<String> item_device_ids;
    Integer set_item_device = 0;
}
