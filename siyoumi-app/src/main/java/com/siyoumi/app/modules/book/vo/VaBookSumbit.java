package com.siyoumi.app.modules.book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;

//预约提交
@Data
public class VaBookSumbit {
    @HasAnyText(message = "请选择日期时间段")
    String day_id;
    @Size(max = 50, message = "姓名长度50")
    String brecord_name;
    @Size(max = 50, message = "手机长度50")
    String brecord_phone;
    @Size(max = 50, message = "身份证长度50")
    String brecord_idcard;
    //同行人
    List<VaBookSumbitAppend> appends;
}
