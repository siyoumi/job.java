package com.siyoumi.app.modules.lucky_num.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.LnumGroup;
import com.siyoumi.app.modules.lucky_num.entity.LuckyNumType;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumGroup;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/lucky_num/lucky_num__group__list")
public class lucky_num__group__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("号码组-列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "lngroup_id",
                "lngroup_acc_id",
                "lngroup_type",
                "lngroup_name",
                "lngroup_order",
        };
        JoinWrapperPlus<LnumGroup> query = SvcLNumGroup.getBean().listQuery(inputData);
        query.select(select);

        IPage<LnumGroup> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcLNumGroup.getApp().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            LnumGroup entity = SvcLNumGroup.getApp().loadEntity(data);
            data.put("id", entity.getKey());

            //类型
            String type = IEnum.getEnmuVal(LuckyNumType.class, entity.getLngroup_type());
            data.put("type", type);
        }

        getR().setData("list", list);
        getR().setData("count", count);


        return getR();
    }


    //删除
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcLNumGroup.getBean().delete(List.of(ids));
    }
}
