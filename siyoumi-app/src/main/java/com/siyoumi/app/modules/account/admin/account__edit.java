package com.siyoumi.app.modules.account.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.modules.account.service.SvcAccount;
import com.siyoumi.app.modules.account.service.SvcDepartment;
import com.siyoumi.app.modules.account.vo.VaAccount;
import com.siyoumi.component.XApp;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.config.SysConfig;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.entity.SysRole;
import com.siyoumi.entity.base.SysAccountBase;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.service.SysRoleService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/account/account__edit")
public class account__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("帐号编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("role_order", 0);
        if (isAdminEdit()) {
            SysAccountService app = SysAccountService.getBean();
            SysAccount entity = app.loadEntity(getID(), XHttpContext.getX());

            HashMap<String, Object> dataAppend = new HashMap<>();
            dataAppend.put("id", entity.getKey());

            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        SysRoleService appRole = SysRoleService.getBean();
        List<SysRole> listRole = appRole.mapper().selectList(appRole.getListQuery(InputData.getIns())); //角色列表

        setPageInfo("list_role", listRole);
        setPageInfo("super_uid", XHttpContext.getX()); //uid
        setPageInfo("login_err_max", SysConfig.getIns().getLoginErrMax()); //登陆错误次数
        setPageInfo("list_depertment", SvcDepartment.getBean().getList()); //部门

        return getR();
    }


    @PostMapping("/save")
    public XReturn save(@Validated() VaAccount vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAccountService app = SysAccountService.getBean();
        //自定义验证
        //uid
        {
            QueryWrapper<SysAccount> query = app.q();
            query.eq(app.fdX(), XHttpContext.getX())
                    .eq("acc_uid", vo.getAcc_uid());
            if (isAdminEdit()) {
                query.ne(app.fdKey(), getID());
            }
            if (app.first(query) != null) {
                result.addError(XValidator.getErr("acc_uid", "帐号已存在"));
            }
        }
        //手机号
        if (XStr.hasAnyText(vo.getAcc_phone())) {
            JoinWrapperPlus<SysAccount> query = app.join();
            query.eq(app.fdX(), XHttpContext.getX())
                    .eq(SysAccountBase.F.acc_phone, vo.getAcc_phone());
            if (isAdminEdit()) {
                query.ne(app.fdKey(), getID());
            }
            if (app.first(query) != null) {
                result.addError(XValidator.getErr("acc_phone", "手机号已存在"));
            }
        } else {
            vo.setAcc_phone(null);
        }
        XValidator.getResult(result);

        InputData inputData = InputData.fromRequest();
        return SvcAccount.getBean().edit(inputData, vo);
    }
}
