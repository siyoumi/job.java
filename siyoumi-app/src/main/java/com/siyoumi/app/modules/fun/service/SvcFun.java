package com.siyoumi.app.modules.fun.service;

import com.siyoumi.app.entity.FunRecord;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.fun.vo.VaFunAdd;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.app.service.FunRecordService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XRedisLock;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.exception.XException;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.*;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SvcFun {
    static public SvcFun getBean() {
        return XSpringContext.getBean(SvcFun.class);
    }

    static public FunRecordService getApp() {
        return FunRecordService.getBean();
    }


    public FunRecord getEntityByKey(String key) {
        JoinWrapperPlus<FunRecord> query = getQuery(InputData.getIns());
        query.eq("funrec_uix", key);
        return SvcFun.getApp().first(query);
    }

    public JoinWrapperPlus<FunRecord> getQuery(InputData inputData) {
        String uid = inputData.input("openid");
        String idSrc = inputData.input("id_src");
        String dateBegin = inputData.input("date_begin");
        String dateEnd = inputData.input("date_end");
        String abType = inputData.input("ab_type");

        JoinWrapperPlus<FunRecord> query = getApp().join();
        query.eq("funrec_x_id", XHttpContext.getX());

        if (XStr.hasAnyText(uid)) { //用户
            query.eq("funrec_uid", uid);
        }

        if (XStr.hasAnyText(idSrc)) { //来源
            query.eq("funrec_id_src", idSrc);
        }

        if (XStr.hasAnyText(dateBegin) && XStr.hasAnyText(dateEnd)) {
            LocalDateTime b = XDate.parse(dateBegin);
            LocalDateTime e = XDate.parse(dateEnd);
            query.between("funrec_create_date", b, e);
        }

        if (XStr.hasAnyText(abType)) { //+：加、-：减
            if ("-".equals(abType)) {
                //减
                query.lt("funrec_num", 0);
            } else {
                //加
                query.ge("funrec_num", 0);
            }
        }

        return query;
    }

    public JoinWrapperPlus<FunRecord> getQuery(String uid, String idSrc) {
        InputData inputData = InputData.getIns();
        inputData.put("openid", uid);
        inputData.put("id_src", idSrc);

        return getQuery(inputData);
    }

    /**
     * 未过期的积分记录
     *
     * @param uid
     * @param offset
     * @param limit
     */
    public List<FunRecord> listTrue(String uid, int offset, int limit) {
        JoinWrapperPlus<FunRecord> query = getQuery(uid, null);
        query.select("funrec_id", "funrec_num_left");
        query.gt("funrec_expire_date", XDate.now())
                .gt("funrec_num_left", 0)
                .orderByAsc("funrec_expire_date");
        query.last(XSqlStr.limitX(limit, offset));

        return getApp().get(query);
    }


    public FunRecord add(String key, String uid, String idSrc, String appId, Integer num, String desc) {
        return add(key, uid, idSrc, appId, num, desc, null, null);
    }


    /**
     * 加减积分
     *
     * @param key
     * @param uid
     * @param idSrc
     * @param appId
     * @param num
     * @param desc
     * @param str00
     * @param str01
     */
    public FunRecord add(String key, String uid, String idSrc, String appId, Integer num, String desc, String str00, String str01) {
        return add(VaFunAdd.of(key, uid, idSrc, appId, num, desc, str00, str01));
    }

    public FunRecord add(VaFunAdd data) {
        if (data.getNum() == 0) {
            throw new XException("num为0");
        }

        FunRecord entity = new FunRecord();
        entity.setFunrec_x_id(XHttpContext.getX());
        entity.setFunrec_uix(data.getKey());
        entity.setFunrec_uid(data.getUid());
        entity.setFunrec_id_src(data.getIdSrc());
        entity.setFunrec_app_id(data.getAppId());
        entity.setFunrec_num(data.getNum());
        entity.setFunrec_desc(data.getDesc());
        if (data.getStr00() != null) {
            entity.setFunrec_str_00(data.getStr00());
        }
        if (data.getStr01() != null) {
            entity.setFunrec_str_01(data.getStr01());
        }

        if (data.getNum() > 0) {
            log.info("加操作");
            entity.setFunrec_expire_date(XDate.now().plusYears(1L));
            entity.setFunrec_num_left(entity.getFunrec_num());
        } else {
            log.info("减操作，特殊");
            String lockKey = "fun|" + data.getUid();
            XReturn r = XRedisLock.lockFunc(lockKey, k -> {
                return handleSub(data.getUid(), data.getNum());
            });
            XValidator.err(r);
        }
        entity.setAutoID(true);
        getApp().save(entity);

        return entity;
    }

    /**
     * 减操作
     *
     * @param openid
     * @param num
     */
    public XReturn handleSub(String openid, Integer num) {
        num = Math.abs(num);
        log.debug("开始直接减操作：{}", num);
        log.debug("一直获取+记录，直接够扣积分为止");

        Integer preFunSum = 0;
        List<FunRecord> preFunList = new ArrayList<>(); //需要扣减的记录
        while (preFunSum <= num) {
            List<FunRecord> listFun = listTrue(openid, preFunList.size(), 10);
            if (listFun.size() <= 0) {
                return XReturn.getR(20153, "积分不足");
            }

            for (FunRecord entityFun : listFun) {
                preFunList.add(entityFun);
                preFunSum += entityFun.getFunrec_num_left();
            }
        }

        Integer leftFun = num;
        XApp.getTransaction().execute(status -> {
            List<String> updateIds = new ArrayList<>();

            //扣减积分逻辑
            for (FunRecord entityFun : preFunList) {
                if (entityFun.getFunrec_num_left() > leftFun) {
                    log.debug("减一部分：" + leftFun);
                    int updateSubFun = entityFun.getFunrec_num() - leftFun;
                    getApp().update()
                            .eq("funrec_id", entityFun.getKey())
                            .set("funrec_num_left", updateSubFun);
                } else {
                    log.debug("全减：" + entityFun.getFunrec_num_left());
                    updateIds.add(entityFun.getKey());
                }
            }

            if (!updateIds.isEmpty()) {
                log.debug("批量更新减全部");
                getApp().update()
                        .in("funrec_id", updateIds)
                        .set("funrec_num_left", 0);
            }
            return true;
        });


        return XReturn.getR(0);
    }

    /**
     * 我的积分
     *
     * @param uid
     */
    public Long getFun(String uid) {
        SysUser entity = SvcSysUser.getApp().first(uid);
        if (entity == null) {
            return 0L;
        }
        return entity.getUser_fun();
    }

    /**
     * 更新积分
     *
     * @param uid
     */
    public Long updateFun(String uid) {
        JoinWrapperPlus<FunRecord> query = getQuery(uid, null);
        query.gt("funrec_expire_date", XDate.now())
                .gt("funrec_num_left", 0);
        BigDecimal sum = getApp().sum(query, "funrec_num_left");
        long fun = sum.longValue();

        SysUser entityUser = SvcSysUser.getApp().getEntity(uid);
        if (entityUser == null) {
            XValidator.err(EnumSys.ERR_VAL.getR("not find user"));
        }
        SysUser update = new SysUser();
        update.setUser_id(entityUser.getKey());
        update.setUser_fun(fun);
        SvcSysUser.getApp().updateById(update);

        SvcSysUser.getApp().delEntityCache(uid);

        return fun;
    }


    /**
     * 删除积分
     *
     * @param ids
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        JoinWrapperPlus<FunRecord> query = getApp().join();
        query.in(FunRecord.tableKey(), ids)
                .select("DISTINCT funrec_uid uid");
        List<Map<String, Object>> listUser = getApp().getMaps(query);

        log.debug("先删除记录，再更新所有用户积分");
        getApp().delete(ids);

        for (Map<String, Object> mapItem : listUser) {
            String uid = (String) mapItem.get("uid");
            updateFun(uid);
        }

        return XReturn.getR(0);
    }
}
