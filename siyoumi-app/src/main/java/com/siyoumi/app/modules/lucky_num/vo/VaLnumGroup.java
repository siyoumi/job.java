package com.siyoumi.app.modules.lucky_num.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//号码组
@Data
public class VaLnumGroup
        extends VaLnumNumSet {
    private String lngroup_acc_id;
    @HasAnyText
    private String lngroup_type;
    @HasAnyText
    @Size(max = 50)
    private String lngroup_name;
    private Integer lngroup_order;
}
