package com.siyoumi.app.modules.app_corp.admin;

import com.siyoumi.app.entity.CorpStore;
import com.siyoumi.app.modules.app_corp.service.SvcCropStore;
import com.siyoumi.app.modules.app_corp.vo.VaCropStore;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_corp/app_corp__store__edit")
public class app_corp__store__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商家-编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("cstore_order", 0);
        if (isAdminEdit()) {
            CorpStore entity = SvcCropStore.getApp().first(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);

            data.put("url", SvcCropStore.getBean().getUrl(entity.getKey()));
            data.put("wxapp_path", SvcCropStore.getBean().getWxAppPath(entity.getKey()));
        }
        getR().setData("data", data);

        return getR();
    }


    @PostMapping("/save")
    public XReturn save(@Validated() VaCropStore vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        InputData inputData = InputData.fromRequest();
        return SvcCropStore.getBean().edit(inputData, vo);
    }
}
