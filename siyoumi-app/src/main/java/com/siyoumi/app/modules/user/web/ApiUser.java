package com.siyoumi.app.modules.user.web;

import com.siyoumi.app.entity.SysSale;
import com.siyoumi.app.modules.user.service.SvcSysMaster;
import com.siyoumi.app.modules.user.service.SvcSysSale;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.app.modules.user.vo.*;
import com.siyoumi.component.XBean;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/wxapp/user/api")
public class ApiUser
        extends WxAppApiController {

    @PostMapping("/user_edit")
    public XReturn userEdit(@Validated SysUserEdit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        vo.setUid(getUid());
        return SvcSysUser.getBean().userEdit(vo);
    }

    @PostMapping("/user_edit_pwd")
    public XReturn userEditPwd(@Validated SysUserEditPwd vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        vo.setUid(getUid());
        return SvcSysUser.getBean().userEditPwd(vo);
    }

    /**
     * 绑定主人，好友关系
     *
     * @param vo
     * @param result
     */
    @PostMapping("/bind_master")
    public XReturn bindMaster(@Validated VoBindMaster vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        vo.setOpenid(getOpenid());
        return SvcSysMaster.getBean().bindMaster(vo);
    }

    /**
     * 分销员信息
     */
    @GetMapping("/sale_info")
    public XReturn saleInfo() {
        SysSale entitySale = SvcSysSale.getBean().getEntitySaleByUid(getUid());
        Map<String, Object> mapSale = XBean.toMap(entitySale, new String[]{
                "sale_id",
                "sale_level",
                "sale_type",
                "sale_uid",
                "sale_state",
                "sale_state_date",
                "sale_money_left",
                "sale_money_total",
        });

        getR().put("sale", mapSale);

        return getR();
    }

    /**
     * 申请分销员
     *
     * @param vo
     * @param result
     */
    @PostMapping("/sale_apply")
    public XReturn saleApply(@Validated VoSale vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        SysSale entitySale = SvcSysSale.getBean().getEntitySaleByUid(getUid());
        if (entitySale != null) {
            return XReturn.getR(20070, "已申请");
        }

        vo.setSale_uid(getUid());
        vo.setSale_id(getUid());
        vo.setSale_level(0);

        return SvcSysSale.getBean().save(vo);
    }

    /**
     * 绑定分销员
     *
     * @param vo
     * @param result
     */
    @PostMapping("/sale_bind")
    public XReturn saleBind(@Validated VoSaleBindClient vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        vo.setClient_uid(getUid());
        return SvcSysSale.getBean().bindClient(vo);
    }
}
