package com.siyoumi.app.modules.redbook.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//评论
@Data
public class RedbookCommentVo {
    @HasAnyText(message = "缺少小红书ID")
    String quan_id;
    String pid; //1级评论
    String reply_to; //回复谁
    @Size(max = 1000)
    String pic;
    @HasAnyText
    @Size(max = 500, message = "不能超500个字")
    String content; //评论内容
    String uid;
}
