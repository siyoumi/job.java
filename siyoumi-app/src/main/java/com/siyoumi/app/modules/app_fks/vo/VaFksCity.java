package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//城市
@Data
public class VaFksCity {
    private String fcity_acc_id;
    @Size(max = 50)
    @HasAnyText
    private String fcity_name;
    @Size(max = 200)
    private String fcity_pic;
    @HasAnyText
    private Integer fcity_order;
}
