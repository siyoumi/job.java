package com.siyoumi.app.modules.sh.task.jm;

import com.siyoumi.component.api.jj.JJ;
import com.siyoumi.component.api.jj.robot.RobotErr;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


//星期任务
@RestController
public class JmTask
        extends TaskBase {

    @Override
    public void exec() {
        XLog.info(this.getClass(), "JmTask_BEGIN:" + XDate.toDateTimeString());

        Map<String, String> arrTask = getArrTask();

        StringBuilder sendMsg = new StringBuilder();
        sendMsg.append("**jm**\n\n");

        for (Map.Entry<String, String> task : arrTask.entrySet()) {
            String taskName = task.getKey();
            String returnStr = "";
            switch (taskName) {
                case "开宝箱":
                case "扭蛋":
                case "摇钱树":
                case "摇钱树2号":
                    returnStr = raoHandle(task);
                    break;
                default:
                    returnStr = commonHandle(task);
                    break;
            }

            sendMsg.append(returnStr);
        }

        //发jj
        JJ jj = JJ.getIns(RobotErr.class);
        jj.send(sendMsg.toString(), "", true);

        XLog.info(this.getClass(), "JmTask_END:" + XDate.toDateTimeString());
    }

    //摇钱树
    public String raoHandle(Map.Entry<String, String> task) {
        String apiUrlLunch = "https://sapi.uat.xiao-bo.com/z_app/star_act/web/zz_api__api_x__user_launch?" +
                "site_id=app.english&act_id={sact_id}&{wx_from}";
        apiUrlLunch = apiUrlLunch.replace("{sact_id}", task.getValue());

        String apiUrlChou = "https://sapi.uat.xiao-bo.com/z_app/star_act/web/zz_api__api_x__user_chou?" +
                "site_id=app.english&master_id={master_id}&{wx_from}";

        StringBuilder sendMsg = new StringBuilder();
        String taskName = task.getKey();
        sendMsg.append(JJ.txtBold(taskName));

        Map<String, String> arrWxFrom = getArrWxFrom();
        for (Map.Entry<String, String> entry : arrWxFrom.entrySet()) {
            String userName = entry.getKey();
            //发起
            String apiUrl = apiUrlLunch
                    .replace("{wx_from}", entry.getValue());
            XReturn r = requestApi(apiUrl);
            sendMsg.append(XStr.concat(userName, "-发起:", r.getErrMsg(), "\n\n"));
            if (r.ok()) {
                Map data = (Map) r.getData("data");
                String samasterId = (String) data.get("samaster_id");

                apiUrl = apiUrlChou
                        .replace("{wx_from}", entry.getValue())
                        .replace("{master_id}", samasterId);
                r = requestApi(apiUrl);
                sendMsg.append(XStr.concat(userName, "-抽奖:", r.getErrMsg(), "\n\n"));
            }
        }

        return sendMsg.toString();
    }

    @Override
    public Map<String, String> getArrTask() {
        Map<String, String> arr = new HashMap<>();
        arr.put("星球积分签到"
                , "https://sapi.uat.xiao-bo.com/z_app/sign_in/web/zz_api__api__new__sign?site_id=app.english&wx_word=sign_in10001000&{wx_from}");
        arr.put("摇钱树", "sact-62fb312cd55cd-3584-9b8e-4f661cbd3ae5");
        arr.put("摇钱树2号", "sact-62d66594d0417-30d0-aa41-62e05124acbc");
        arr.put("扭蛋", "sact-627b24c483d45-3182-bb5f-10caf9b9dfae");
        arr.put("开宝箱", "sact-627b24c483d45-3182-bb5f-10caf9b9dfae");
        return arr;
    }

    @Override
    public Map<String, String> getArrWxFrom() {
        Map<String, String> arr = new HashMap<>();
        arr.put("佬大", "wx_from=onTJv5a-qVvK_S4ckE6PyPV51hKE&wx_from_enc=ee3ae04d6YjI1VVNuWTFZUzF4Vm5aTFgxTTBZMnRGTmxCNVVGWTFNV2hMUlE9PQ==");

        return arr;
    }
}
