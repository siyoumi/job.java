package com.siyoumi.app.modules.app_show.entity;

import com.siyoumi.util.IEnum;

//订单状态
public enum EnumJobOrderState
        implements IEnum {
    //0：初始化
    //5：交谈中
    //10：已报价
    //15：开发中
    //20：已完成
    INIT(0, "初始化"),
    TALK(5, "交谈中"),
    QUOTED(10, "已报价"),
    DEV(15, "开发中"),
    DONE(20, "已完成");


    private Integer key;
    private String val;

    EnumJobOrderState(Integer key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key.toString();
    }
}
