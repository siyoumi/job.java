package com.siyoumi.app.modules.app_fks.service;

import com.siyoumi.app.entity.FksPosition;
import com.siyoumi.app.modules.app_fks.vo.VaFksPosition;
import com.siyoumi.app.service.FksPositionService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XRedis;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//职位
@Slf4j
@Service
public class SvcFksPosition
        implements IWebService {
    static public SvcFksPosition getBean() {
        return XSpringContext.getBean(SvcFksPosition.class);
    }

    static public FksPositionService getApp() {
        return FksPositionService.getBean();
    }

    /**
     * 获取职位等级
     * 等级越小，权限越大
     *
     * @param id
     */
    public Integer getPositionLevel(String id) {
        FksPosition entity = getEntity(id);
        if (entity == null) {
            return 999;
        }

        return entity.getFposi_level();
    }

    public FksPosition getEntity(String id) {
        return XRedis.getBean().getAndSetData(getApp().getEntityCacheKey(id), k -> {
            return getApp().getEntity(id);
        }, FksPosition.class);
    }

    public List<Map<String, Object>> getList() {
        JoinWrapperPlus<FksPosition> query = listQuery();
        query.select("fposi_id", "fposi_name", "fposi_level");
        return getApp().getMaps(query);
    }

    public JoinWrapperPlus<FksPosition> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<FksPosition> listQuery(InputData inputData) {
        String name = inputData.input("name");

        JoinWrapperPlus<FksPosition> query = getApp().join();
        query.eq("fposi_x_id", XHttpContext.getX())
                .eq("fposi_del", 0);
        //排序
        query.orderByAsc("fposi_level");
        if (XStr.hasAnyText(name)) { //名称
            query.like("fposi_name", name);
        }

        return query;
    }

    public XReturn edit(InputData inputData, VaFksPosition vo) {
        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminEdit()) {
            ignoreField.add("fdep_acc_id");
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, true, ignoreField);
            if (r.ok()) {
                FksPosition entity = r.getData("entity");
                FksPositionService.getBean().delEntityCache(entity.getKey());

            }
            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }
}
