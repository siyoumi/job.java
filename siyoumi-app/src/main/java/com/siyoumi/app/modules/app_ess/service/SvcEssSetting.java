package com.siyoumi.app.modules.app_ess.service;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_ess.vo.VaTaskFun;
import com.siyoumi.app.modules.app_ess.vo.VaEssSettingTxt00;
import com.siyoumi.app.modules.fun.service.SvcFun;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XRedis;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

//资源文件
@Slf4j
@Service
public class SvcEssSetting
        implements IWebService {
    static public SvcEssSetting getBean() {
        return XSpringContext.getBean(SvcEssSetting.class);
    }

    static public SysAbcService getApp() {
        return SysAbcService.getBean();
    }

    public VaEssSettingTxt00 getSetting() {
        SysAbc entityAbc = XRedis.getBean().getAndSetData(getApp().getEntityCacheKey("app_ess_setting"), k -> {
            return getApp().getEntityByUix("app_ess_setting");
        }, SysAbc.class);

        if (entityAbc == null || XStr.isNullOrEmpty(entityAbc.getAbc_txt_00())) {
            return new VaEssSettingTxt00();
        }
        return XJson.parseObject(entityAbc.getAbc_txt_00(), VaEssSettingTxt00.class);
    }

    /**
     * 积分任务，加积分
     *
     * @param vo
     */
    public XReturn taskFun(VaTaskFun vo) {
        VaEssSettingTxt00 setting = getSetting();

        Integer fun = 0;
        String desc = "积分任务-";
        switch (vo.getType()) {
            case "file":
                fun = setting.getAdd_fun_file();
                desc += "观看资源文件";
                break;
            case "test1":
                fun = setting.getAdd_fun_test1();
                desc += "理论练习";
                break;
            case "module":
                fun = setting.getAdd_fun_module();
                desc += "仿真练习";
                break;
        }
        if (fun <= 0) {
            return XReturn.getR(0, "无需要加分");
        }
        String redisKey = XStr.concat("app_ess_fun:", vo.getUid(), vo.getType());
        Boolean ok = XRedis.getBean().setIfExists(redisKey, "1", 300);
        if (!ok) {
            return XReturn.getR(0, "已加过分");
        }

        Integer funX = fun;
        String descX = desc;
        return XApp.getTransaction().execute(status -> {
            SvcFun.getBean().add(XApp.getStrID()
                    , vo.getUid()
                    , "app_ess"
                    , "app_ess"
                    , funX
                    , descX);
            //更新用户
            SvcEssUser.getApp().update()
                    .setSql(XStr.format("euser_{0}_count = euser_{0}_count + 1,euser_{0}_fun = euser_{0}_fun + {1}", vo.getType(), funX.toString()))
                    .eq("euser_id", vo.getUid())
                    .update()
            ;

            SvcFun.getBean().updateFun(vo.getUid());
            SvcEssUser.getApp().delEntityCache(vo.getUid());
            return EnumSys.OK.getR();
        });
    }
}
