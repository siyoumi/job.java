package com.siyoumi.app.modules.lucky_num.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumGroup;
import com.siyoumi.app.service.LnumWinService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/lucky_num/lucky_num__num_win__list")
public class lucky_num__num_win__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("中奖列表");

        InputData inputData = InputData.fromRequest();
        String groupId = inputData.input("group_id");

        String[] select = {
                "lnwin_id",
                "lnwin_x_id",
                "lnwin_group_id",
                "lnwin_session_id",
                "lnwin_prize_id",
                "lnwin_wxuser_id",
                "lnwin_num",
                "lnwin_create_date",
                "wxuser_openid",
                "wxuser_nickname",
                "lnsp_name",
                "lns_name",
                "lngroup_name",
        };
        JoinWrapperPlus<LnumWin> query = SvcLNumGroup.getBean().numWinQuery(groupId, inputData);
        query.join(LnumSession.table(), LnumSession.tableKey(), "lnwin_session_id");
        query.join(WxUser.table(), WxUser.tableOpenid(), "lnwin_wxuser_id");
        query.join(LnumSessionPrize.table(), LnumSessionPrize.tableKey(), "lnwin_prize_id");
        query.join(LnumGroup.table(), LnumGroup.tableKey(), "lnwin_group_id");
        query.select(select);

        IPage<LnumWin> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = LnumWinService.getBean().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            LnumWin entity = LnumWinService.getBean().loadEntity(data);
            data.put("id", entity.getKey());
        }

        if (!isAdminExport()) {
            getR().setData("list", list);
            getR().setData("count", count);
        } else {
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("lnwin_id", "ID");
            tableHead.put("lnwin_group_id", "号码ID");
            tableHead.put("lngroup_name", "号码组");
            tableHead.put("wxuser_openid", "openid");
            tableHead.put("wxuser_nickname", "微信名");
            tableHead.put("lnwin_session_id", "奖品期ID");
            tableHead.put("lns_name", "奖品期");
            tableHead.put("lnsp_name", "奖品");
            tableHead.put("lnwin_num", "中奖号码");

            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }

        return getR();
    }

    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/del")
    public XReturn del() {
        return null;
    }
}
