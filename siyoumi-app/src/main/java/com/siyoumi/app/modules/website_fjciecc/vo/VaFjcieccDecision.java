package com.siyoumi.app.modules.website_fjciecc.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//决策咨询
@Data
public class VaFjcieccDecision {
    @HasAnyText
    @Size(max = 100)
    String abc_name;

    String abc_app_id;
    String abc_table;

    @Size(max = 200)
    String abc_str_00;
    String abc_str_01;
    @Size(max = 200)
    String abc_str_02;
    @Size(max = 200)
    String abc_str_03;

    String abc_txt_00;
    Integer abc_order;
}

