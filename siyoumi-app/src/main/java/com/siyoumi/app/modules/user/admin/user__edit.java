package com.siyoumi.app.modules.user.admin;

import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.siyoumi.app.entity.SysAddress;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.entity.WxUserInfo;
import com.siyoumi.app.service.SysAddressService;
import com.siyoumi.app.service.WxUserInfoService;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.*;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.validator.XValidator;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/user/user__edit")
public class user__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("微信用户-详情");

        Map<String, Object> data = new HashMap<>();
        if (isAdminEdit()) {
            WxUserService service = WxUserService.getBean();
            WxUser entity = service.getEntity(getID(), XHttpContext.getX());
            if (entity == null) {
                return XReturn.getR(20036, "id异常");
            }
            //用户扩展表
            WxUserInfo entityWxInfo = WxUserInfoService.getBean().getByOpenid(entity.getWxuser_openid());

            HashMap<String, Object> dataAppend = new HashMap<>();

            //默认地址
            String address = "";
            SysAddress entityAddrDef = SysAddressService.getBean().getDef(entity.getWxuser_openid());
            if (entityAddrDef != null) {
                address = XStr.concat(entityAddrDef.getAddr_province()
                        , entityAddrDef.getAddr_city()
                        , entityAddrDef.getAddr_district()
                        , entityAddrDef.getAddr_address()
                );
            }
            dataAppend.put("address", address);

            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
            if (entityWxInfo != null) {
                data.putAll(entityWxInfo.toMap());
            }
        }


        getR().setData("data", data);

        return getR();
    }

    @PostMapping("/save")
    public XReturn save(@Validated() WxUser entity, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        //自定义验证
        WxUserService app = WxUserService.getBean();

        InputData inputData = XHttpContext.InputAll();
        Map<String, Object> entityMap = entity.toMap();

        //排队字段，只能修改wxuser_desc
        List<String> ignoreField = new ArrayList<>();
        for (String key : entityMap.keySet()) {
            if (key.equals("wxuser_desc")) continue;
            if (key.equals("wxuser_id")) continue;
            if (key.equals("id")) continue;
            ignoreField.add(key);
        }

        inputData.putAll(entity.toMap());

        return app.saveEntity(inputData, true, ignoreField);
    }

    //还原更新时间
    @GetMapping("/resetUpdateDate")
    public XReturn resetUpdateDate() {
        if (XStr.isNullOrEmpty(getID())) {
            return XReturn.getR(EnumSys.MISS_VAL.getErrcode(), "miss id");
        }

        WxUserService app = WxUserService.getBean();
        WxUser entity = app.getByOpenid(getID());
        if (entity == null) {
            return XReturn.getR(20069, "id异常");
        }

        UpdateChainWrapper<WxUser> updateQuery = app.update();
        updateQuery
                .eq(app.fdKey(), entity.getKey())
                .set("wxuser_user_data_update_date", XDate.date2000())
                .update();

        return getR();
    }
}

