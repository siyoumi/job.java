package com.siyoumi.app.modules.user.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

//分销员审核
@Data
@Accessors(chain = true)
public class VoSaleAudit {
    @HasAnyText
    private List<String> ids;
    @HasAnyText
    @EqualsValues(vals = {"1", "10"})
    private Integer enable;
}
