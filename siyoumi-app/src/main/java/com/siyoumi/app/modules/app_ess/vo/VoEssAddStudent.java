package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//添加学生
@Data
public class VoEssAddStudent {
    @HasAnyText
    private List<String> uids;
    @HasAnyText
    private String class_id;
}
