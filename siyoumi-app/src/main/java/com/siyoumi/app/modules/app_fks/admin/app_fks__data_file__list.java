package com.siyoumi.app.modules.app_fks.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FksCity;
import com.siyoumi.app.entity.FksFile;
import com.siyoumi.app.modules.app_fks.entity.EnumFksFileDataType;
import com.siyoumi.app.modules.app_fks.service.SvcFksCtiy;
import com.siyoumi.app.modules.app_fks.service.SvcFksFile;
import com.siyoumi.app.sys.service.FileHandle;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XFile;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__data_file__list")
public class app_fks__data_file__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        InputData inputData = InputData.fromRequest();
        String folderId = inputData.input("folder_id", "");
        String typeId = inputData.input("type_id", "");

        String folderName = "";
        String cityName = "";
        if (XStr.hasAnyText(folderId)) { //目录名称
            FksFile entityFolder = SvcFksFile.getApp().first(folderId);
            folderName = entityFolder.getFfile_name();

            typeId = entityFolder.getFfile_type_id();
            setPageFrom("type_id", typeId);
            inputData.put("type_id", typeId);
        }
        if (XStr.hasAnyText(typeId)) { //城市
            FksCity entityCity = SvcFksCtiy.getApp().first(typeId);
            cityName = entityCity.getFcity_name();
        }
        String titlePrefix = XStr.format("{0}{1}", cityName
                , XStr.hasAnyText(folderName) ? "-" + folderName : ""
        );

        setPageTitle("文件列表");
        if (XStr.hasAnyText(titlePrefix)) {
            setPageTitle(titlePrefix, "-文件列表");
        }

        String[] select = {
                "ffile_id",
                "ffile_folder",
                "ffile_folder_id",
                "ffile_data_type",
                "ffile_name",
                "ffile_create_date",
                "ffile_path",
                "ffile_file_url",
                "ffile_file_ext",
                "ffile_file_size",
                "ffile_order",
                "fcity_name",
        };
        JoinWrapperPlus<FksFile> query = SvcFksFile.getBean().listQuery(2, inputData);
        query.select(select);
        query.join(FksCity.table(), FksCity.tableKey(), "ffile_type_id");
        query.orderByDesc("ffile_folder")
                .orderByAsc("ffile_order")
                .orderByAsc("ffile_id");

        IPage<FksFile> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcFksFile.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumFksFileDataType enumDataType = XEnumBase.of(EnumFksFileDataType.class);
        for (Map<String, Object> item : list) {
            FksFile entity = XBean.fromMap(item, FksFile.class);
            item.put("id", entity.getKey());
            //数据分类
            item.put("date_type", enumDataType.get(entity.getFfile_data_type()));
            //大小
            item.put("file_size", SvcFksFile.fileSize(entity.getFfile_file_size()));
        }

        getR().setData("list", list);
        getR().setData("count", count);
        //数据分类
        setPageInfo("enum_data_type", enumDataType);
        //城市列表
        setPageInfo("list_city", SvcFksCtiy.getBean().getList());

        setPageFrom("folder_id", folderId);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcFksFile.getBean().delete(getIds());
    }
}
