package com.siyoumi.app.modules.user.vo;

import com.siyoumi.exception.EnumSys;
import com.siyoumi.validator.XValidator;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

//入帐出帐
@Data
@Accessors(chain = true)
public class VoSaleAddRecord {
    String srec_key;
    String srec_id_src;
    String srec_app_id;
    String srec_uid;
    String srec_ab_type;
    BigDecimal srec_num;
    String srec_desc;
    String srec_id_00;
    String srec_id_01;
    Integer srec_int_00;
    String srec_str_00;

    Boolean updateLeft = true;

    static public VoSaleAddRecord of(String key, String uid, BigDecimal num, String desc, String appId, String idSrc) {
        if (num.compareTo(BigDecimal.ZERO) == 0) {
            XValidator.err(EnumSys.ERR_VAL.getR("num == 0"));
        }

        return new VoSaleAddRecord()
                .setSrec_key(key)
                .setSrec_uid(uid)
                .setSrec_num(num)
                .setSrec_desc(desc)
                .setSrec_app_id(appId)
                .setSrec_id_src(idSrc)
                ;
    }
}
