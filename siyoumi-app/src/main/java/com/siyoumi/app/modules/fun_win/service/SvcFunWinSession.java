package com.siyoumi.app.modules.fun_win.service;

import com.siyoumi.app.entity.FunMallPrize;
import com.siyoumi.app.entity.FunWinSession;
import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.app.modules.fun_win.vo.VaFunWinSession;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSetAdmin;
import com.siyoumi.app.service.FunWinSessionService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

//积分商城-商品
@Service
@Slf4j
public class SvcFunWinSession
        implements IWebService {
    static public SvcFunWinSession getBean() {
        return XSpringContext.getBean(SvcFunWinSession.class);
    }

    static public FunWinSessionService getApp() {
        return FunWinSessionService.getBean();
    }


    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<FunWinSession> listQuery(InputData inputData) {
        String name = inputData.input("name");

        JoinWrapperPlus<FunWinSession> query = getApp().join();
        query.join(SysPrizeSet.table(), "pset_id_src", "fwins_id");
        query.eq("fwins_x_id", XHttpContext.getX());
        getApp().addQueryAcc(query);
        //排序
        query.orderByAsc("fwins_order")
                .orderByDesc("fwins_id");
        if (XStr.hasAnyText(name)) //名称
        {
            query.like("fwins_name", name);
        }

        return query;
    }

    public XReturn edit(InputData inputData, VaFunWinSession vo) {
        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminEdit()) {
            ignoreField.add("fwins_acc_id");
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, true, ignoreField);
            if (r.err()) {
                return r;
            }

            FunWinSession entity = r.getData("entity");
            SvcSysPrizeSetAdmin.getIns().editSave(entity.getKey(), "fun_win", vo);

            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }
}
