package com.siyoumi.app.modules.app_ess.entity;

import com.siyoumi.component.XEnumBase;


public class EnumEssModuleEnable
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "下架");
        put(1, "上架");
    }
}
