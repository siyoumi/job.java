package com.siyoumi.app.modules.mall2.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.M2Sku;
import com.siyoumi.app.entity.SysStock;
import com.siyoumi.app.modules.mall2.vo.SkuData;
import com.siyoumi.app.modules.mall2.vo.SkuSetStockVo;
import com.siyoumi.app.service.M2SkuService;
import com.siyoumi.app.service.SysStockService;
import com.siyoumi.util.XReturn;
import com.siyoumi.component.XSpringContext;

import java.util.List;

public interface SvcM2Sku {
    static SvcM2Sku getBean() {
        return XSpringContext.getBean(SvcM2Sku.class);
    }

    static M2SkuService getApp() {
        return M2SkuService.getBean();
    }

    /**
     * 库存
     */
    default SysStock getEntityLeft(String skuId) {
        return SysStockService.getBean().getEntityBySrc(skuId, false);
    }

    QueryWrapper<M2Sku> listQuery(String spuId, List<String> skuids);

    M2Sku getEntitySku(String attrSkuId0, String attrSkuId1, String attrSkuId2);

    XReturn canEdit(String skuId, Boolean throwEx);

    XReturn validLimit(SkuData skuData, String wxFrom, long count);

    XReturn valid(SkuData skuData);

    List<SkuData> getList(String spuId, List<String> skuId);

    XReturn setEnable(List<String> ids, Integer enable);

    XReturn setStock(List<SkuSetStockVo> listVo);
}
