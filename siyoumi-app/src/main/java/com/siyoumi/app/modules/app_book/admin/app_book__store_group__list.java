package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.BookStore;
import com.siyoumi.app.entity.BookStoreGroup;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.app.modules.app_book.service.SvcBookStoreGroup;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__store_group__list")
public class app_book__store_group__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商家分组");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "bsgroup_id",
                "bsgroup_name",
                "bsgroup_pic",
                "bsgroup_order",
        };
        JoinWrapperPlus<BookStoreGroup> query = SvcBookStoreGroup.getBean().listQuery(inputData);
        query.select(select);
        //
        IPage<BookStoreGroup> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcBookStoreGroup.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> item : list) {
            BookStoreGroup entityAcc = SvcBookStoreGroup.getApp().loadEntity(item);
            item.put("id", entityAcc.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcBookStoreGroup.getBean().delete(getIds());
    }
}
