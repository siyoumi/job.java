package com.siyoumi.app.modules.app_ess.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.app_ess.entity.EnumEssFileType;
import com.siyoumi.app.modules.app_ess.service.SvcEssStudy;
import com.siyoumi.app.service.EssStudyRecordService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__study_record__list")
public class app_ess__study_record__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("子任务列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "esre_id",
                "esre_uid",
                "esre_fun",
                "esre_study_id",
                "esre_state",
                "esre_state_date",
                "estask_key",
                "user_id",
                "user_name",
                "estudy_name",
        };
        JoinWrapperPlus<EssStudyRecord> query = SvcEssStudy.getBean().listRecordQuery(inputData);
        query.join(EssStudyTask.table(), EssStudyTask.tableKey(), "esre_task_id");
        query.join(SysUser.table(), SysUser.tableKey(), "esre_uid");
        query.join(EssStudy.table(), EssStudy.tableKey(), "esre_study_id");
        query.select(select);
        query.orderByDesc("esre_id");
        //
        IPage<EssStudyRecord> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = EssStudyRecordService.getBean().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        //
        //数据分类
        EnumEssFileType enumTaskType = XEnumBase.of(EnumEssFileType.class);
        enumTaskType.put("test1", "理论实训");
        setPageInfo("enum_task_type", enumTaskType);
        for (Map<String, Object> item : list) {
            EssStudyRecord entity = XBean.fromMap(item, EssStudyRecord.class);
            item.put("id", entity.getKey());
            //状态
            item.put("state", entity.getEsre_state() == 0 ? "未完成" : "已完成");
            //分类
            item.put("task_type", enumTaskType.get((String) item.get("estask_key")));
        }

        if (!isAdminExport()) {
            getR().setData("list", list);
            getR().setData("count", pageData.getTotal());
        } else {
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("id", "ID");
            tableHead.put("estudy_name", "学习任务");
            tableHead.put("user_name", "学生");
            tableHead.put("user_id", "学生uid");
            tableHead.put("task_type", "任务类型");
            tableHead.put("state", "完成状态");
            tableHead.put("esre_state_date", "完成时间");

            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }


        return getR();
    }

    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }
}
