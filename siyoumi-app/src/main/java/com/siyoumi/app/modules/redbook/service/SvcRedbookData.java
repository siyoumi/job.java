package com.siyoumi.app.modules.redbook.service;

import com.siyoumi.app.entity.RedbookData;
import com.siyoumi.app.entity.RedbookQuan;
import com.siyoumi.app.modules.redbook.vo.RedbookSetData;
import com.siyoumi.app.service.RedbookDataService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

//评论
@Slf4j
@Service
public class SvcRedbookData
        implements IWebService {
    static public SvcRedbookData getBean() {
        return XSpringContext.getBean(SvcRedbookData.class);
    }

    static public RedbookDataService getApp() {
        return RedbookDataService.getBean();
    }


    /**
     * 点赞，收藏
     *
     * @param vo
     */
    public XReturn setData(RedbookSetData vo) {
        return RedbookDataHandle.of(vo.getRdata_type()).handle(vo);
    }

    /**
     * 查询用户一条类型数据记录（点赞等）
     *
     * @param type
     * @param idSrc
     * @param uid
     * @return
     */
    public RedbookData getEntity(Integer type, String idSrc, String uid) {
        JoinWrapperPlus<RedbookData> query = listQuery(type, idSrc);
        query.eq("rdata_uid", uid);

        return getApp().first(query);
    }

    /**
     * 查询次数query
     *
     * @param type
     * @param idSrc
     * @return
     */
    public JoinWrapperPlus<RedbookData> listQuery(Integer type, String idSrc) {
        JoinWrapperPlus<RedbookData> query = getApp().join();
        query.eq("rdata_x_id", XHttpContext.getX())
                .eq("rdata_type", type);

        if (XStr.hasAnyText(idSrc)) {
            query.eq("rdata_id_src", idSrc);
        }

        return query;
    }

    /**
     * 查询类型次数
     *
     * @param type
     * @param idSrc
     * @param uid
     * @param dateBegin
     * @param dateEnd
     * @return
     */
    public Long getCount(Integer type, String idSrc, String uid, LocalDateTime dateBegin, LocalDateTime dateEnd) {
        JoinWrapperPlus<RedbookData> query = listQuery(type, idSrc);
        if (XStr.hasAnyText(idSrc)) {
            query.eq("rdata_uid", uid);
        }
        if (dateBegin != null && dateEnd != null) {
            query.between("rdata_create_date", dateBegin, dateEnd);
        }

        return getApp().count(query);
    }
}

