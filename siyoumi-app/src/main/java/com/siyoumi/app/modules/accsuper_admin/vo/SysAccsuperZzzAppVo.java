package com.siyoumi.app.modules.accsuper_admin.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SysAccsuperZzzAppVo {
    @NotBlank
    private String acczapp_x_id;
    @NotBlank
    private String app_ids;
}
