package com.siyoumi.app.modules.app_admin.admin;

import com.siyoumi.app.entity.SysItem;
import com.siyoumi.app.modules.app_admin.entity.EnumItemType;
import com.siyoumi.app.modules.app_admin.vo.SysItemVo;
import com.siyoumi.app.modules.app_admin.vo.VaSysItemApp;
import com.siyoumi.app.service.SysItemService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_admin/app_admin__item__edit")
public class app_admin__item__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle(XStr.concat("属性模板-配置"));

        Map<String, Object> data = new HashMap<>();
        data.put("item_order", 0);
        data.put("item_app_id", getPid());
        data.put("item_id_src", XApp.getStrID());
        if (isAdminEdit()) {
            SysItem entity = SysItemService.getBean().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //类型
        setPageInfo("enum_item_type", IEnum.toMap(EnumItemType.class));

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaSysItemApp vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        SysItemService app = SysItemService.getBean();
        //自定义验证
        if (isAdminAdd()) {
            SysItem entityItem = app.getEntity(vo.getItem_id());
            if (entityItem != null) {
                result.addError(XValidator.getErr("item_id", "ID已存在"));
            }
        }
        XValidator.getResult(result);
        //
        SysItem entity = new SysItem();
        XBean.copyProperties(vo, entity);
        entity.setItem_x_id("");
        if (isAdminAdd()) {
            //添加
            app.save(entity);
        } else {
            //编辑
            app.updateById(entity);
        }
        getR().setData("entity", entity);

        return getR();
    }


    @GetMapping({"/item__list"})
    public XReturn itemList() {
        String idSrc = input("id_src");
        if (XStr.isNullOrEmpty(idSrc)) {
            return EnumSys.MISS_VAL.getR("miss id_src");
        }

        setPageTitle("项编辑");
        List<SysItemVo> list = new ArrayList<>();

        SysItem entityItem = SysItemService.getBean().getEntityByIdSrc(idSrc, "");
        if (entityItem != null) {
            list = entityItem.commonData(SysItemVo.class);
        }

        getR().setData("list", list);
        //跳转方式
        setPageInfo("enum_item_type", IEnum.toMap(EnumItemType.class));

        return getR();
    }
}
