package com.siyoumi.app.modules.mall2.service;

import com.siyoumi.app.entity.M2Order;
import com.siyoumi.app.entity.M2OrderItem;
import com.siyoumi.app.sys.service.IPayOrder;
import com.siyoumi.app.sys.service.payorder.PayOrder;
import com.siyoumi.app.service.M2OrderItemService;
import com.siyoumi.app.service.M2OrderService;
import com.siyoumi.app.service.SysOrderService;
import com.siyoumi.app.service.SysStockService;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

public class PayOrderM2
        implements IPayOrder {
    static public SysOrderService getApp() {
        return SysOrderService.getBean();
    }

    public M2Order getEntityM2Order() {
        return M2OrderService.getBean().getEntity(getEntity().getOrder_id());
    }

    protected IPayOrder getPayOrder() {
        return PayOrder.getBean(getEntity());
    }

    @Override
    public XReturn pay(String code) {
        return getPayOrder().pay(code);
    }

    @Override
    public XReturn payChoose(String code) {
        return null;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn payCheck() {
        XReturn r = getPayOrder().payCheck();
        if (r.ok()) {
            //支付成功，同步订单支付状态
            M2Order entityUpdate = new M2Order();
            entityUpdate.setMorder_id(getEntity().getOrder_id());
            entityUpdate.setMorder_pay(1);
            M2OrderService.getBean().updateById(entityUpdate);
        }
        return r;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn rollback() {
        if (getEntity().checkPayOk()) {
            return XReturn.getR(20069, "订单已支付");
        }
        if (getEntity().getOrder_pay_expire_handle() == 1) {
            return XReturn.getR(20070, "订单已处理");
        }

        //再检查一次接口
        XReturn r = payCheck();
        if (r.ok()) {
            return XReturn.getR(20069, "订单已支付");
        }

        SysOrderService.getBean().getEntityLock(getEntity().getKey());
        SysOrderService.getBean().setRollbackDone(getEntity());

        XLog.debug(this.getClass(), "订单标记失效");
        M2Order entityUpdate = new M2Order();
        entityUpdate.setMorder_id(getEntity().getOrder_id());
        M2OrderService.getBean().setEnable(entityUpdate, 0);

        XLog.debug(this.getClass(), "库存释放");
        M2OrderItemService appItem = M2OrderItemService.getBean();
        List<M2OrderItem> listItem = appItem.getList(entityUpdate.getMorder_id());
        for (M2OrderItem entityItem : listItem) {
            SysStockService.getBean().subStock(entityItem.getMoitem_sku_id(), -entityItem.getMoitem_count().longValue());
        }

        return XReturn.getR(0);
    }


    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn cancel() {
        XReturn r = getPayOrder().cancel();
        if (r.ok()) {
            XLog.debug(this.getClass(), "标记取消订单");
            M2Order entityUpdate = new M2Order();
            entityUpdate.setMorder_id(getEntity().getOrder_id());
            entityUpdate.setMorder_cancel(1);
            M2OrderService.getBean().updateById(entityUpdate);

            rollback();
        }
        return r;
    }

    @Override
    public XReturn refundApply(String refundId, BigDecimal refundPrice, String desc) {
        return null;
    }

    @Override
    public XReturn refund(String refundId) {
        return null;
    }
}
