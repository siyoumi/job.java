package com.siyoumi.app.modules.fun_mall.service;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.sys.vo.CommonItemData;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.app.service.SysItemService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.IWebService;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SvcFunMall
        implements IWebService {
    static public SvcFunMall getBean() {
        return XSpringContext.getBean(SvcFunMall.class);
    }

    /**
     * 设置输出
     */
    public Map<String, Object> getSetting() {
        SysAbc setting = SysAbcService.getBean().getEntityByUix("fun_mall_setting");
        if (setting == null) {
            XValidator.err(EnumSys.ERR_VAL.getR("应用未初始化"));
        }

        Map<String, Object> mapSetting = XBean.toMap(setting, new String[]{
                "abc_id",
                "abc_long_00",
        });

        //轮播图
        List<CommonItemData> listItemData = SysItemService.getBean().getItemDataByIdSrc(setting.getAbc_str_00());
        mapSetting.put("abc_str_00", listItemData);


        return mapSetting;
    }
}
