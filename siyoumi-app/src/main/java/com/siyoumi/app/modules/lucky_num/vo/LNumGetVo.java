package com.siyoumi.app.modules.lucky_num.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//领号码
@Data
public class LNumGetVo {
    @HasAnyText(message = "miss set_id")
    String set_id;
    String key;
}
