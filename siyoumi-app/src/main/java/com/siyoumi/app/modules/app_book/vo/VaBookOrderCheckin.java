package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//登记入住
@Data
public class VaBookOrderCheckin {
    @HasAnyText(message = "miss ids")
    private List<String> ids;

}
