package com.siyoumi.app.modules.website_fjciecc.admin;

import com.alibaba.fastjson.JSON;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.website_fjciecc.entity.WebSiteFjcieccRecruit;
import com.siyoumi.app.modules.website_fjciecc.service.WebSiteFjcieccEditBase;
import com.siyoumi.app.modules.website_fjciecc.vo.VaFjcieccRecruit;
import com.siyoumi.app.modules.website_fjciecc.vo.VaFjcieccTrends;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/xadmin/website_fjciecc/website_fjciecc__recruit__edit")
public class website_fjciecc__recruit__edit
        extends WebSiteFjcieccEditBase {
    @Override
    public String title() {
        return "招聘岗位-编辑";
    }

    @Override
    public String abcTable() {
        return "fjciecc_recruit";
    }

    @Override
    public void indexBefore(Map<String, Object> data) {
        if (isAdminEdit()) {
            SysAbc entityAbc = SysAbcService.getBean().loadEntity(data);
            WebSiteFjcieccRecruit txt00 = WebSiteFjcieccRecruit.getInstance(entityAbc.getAbc_txt_00());
            data.putAll(XBean.toMap(txt00));
        }
    }

    @Override
    public void indexAfter() {

    }

    @GetMapping()
    public XReturn index() {
        return super.index();
    }

    @PostMapping("/save")
    public XReturn save(@Validated() VaFjcieccRecruit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAbcService svcAbc = SysAbcService.getBean();

        vo.setAbc_table(abcTable());
        vo.setAbc_app_id(XHttpContext.getAppId());

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
            ignoreField.add("abc_app_id");
        }

        InputData inputData = InputData.fromRequest();

        //转json保存
        WebSiteFjcieccRecruit txt00 = XBean.fromMap(inputData, WebSiteFjcieccRecruit.class);
        vo.setAbc_txt_00(JSON.toJSONString(txt00));

        return svcAbc.saveEntity(inputData, vo, true, ignoreField);
    }
}
