package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.annotation.RequestLimit;
import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.app.entity.BookRefundOrder;
import com.siyoumi.app.modules.app_book.entity.EnumBookOrderRefundAuditStatus;
import com.siyoumi.app.modules.app_book.entity.EnumBookOrderRefundHandle;
import com.siyoumi.app.modules.app_book.service.RequestLimitAppBook;
import com.siyoumi.app.modules.app_book.service.SvcBookOrder;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.modules.app_book.vo.VaBookOrderCheckout;
import com.siyoumi.app.modules.app_book.vo.VoBookOrderRefundAudit;
import com.siyoumi.app.service.BookRefundOrderService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/xadmin/app_book/app_book__order_refund__list")
public class app_book__order_refund__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("售后订单");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "brefo_id",
                "brefo_order_id",
                "brefo_create_date",
                "brefo_price",
                "brefo_audit_status",
                "brefo_audit_status_date",
                "brefo_audit_desc",
                "brefo_refund_id",
                "brefo_price_down",
                "brefo_price_pay",
                "brefo_refund_price",
                "brefo_refund_handle",
                "brefo_refund_handle_date",
                "brefo_refund_errmsg",
                "border_price_pay",
        };
        JoinWrapperPlus<BookRefundOrder> query = SvcBookOrder.getBean().listRefundQuery(inputData);
        query.join(BookOrder.table(), BookOrder.tableKey(), "brefo_order_id");
        query.eq("brefo_store_id", getStoreId());
        query.select(select);

        IPage<BookRefundOrder> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = BookRefundOrderService.getBean().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumBookOrderRefundHandle enumOrderRefundHandle = XEnumBase.of(EnumBookOrderRefundHandle.class); //退款状态
        EnumBookOrderRefundAuditStatus enumOrderRefundStatus = XEnumBase.of(EnumBookOrderRefundAuditStatus.class); //审核状态
        for (Map<String, Object> item : list) {
            BookRefundOrder entity = XBean.fromMap(item, BookRefundOrder.class);
            item.put("id", entity.getKey());
            //审核状态
            item.put("audit_status", enumOrderRefundStatus.get(entity.getBrefo_audit_status()));
            //退款状态
            item.put("refund_handle", enumOrderRefundHandle.get(entity.getBrefo_refund_handle()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        setPageInfo("enum_order_refund_audit_status", enumOrderRefundStatus);

        return getR();
    }


    //订单退房
    @RequestLimit(key = RequestLimitAppBook.class)
    @PostMapping("/refund_audit")
    public XReturn refundAudit(@Validated VoBookOrderRefundAudit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        XReturn r = SvcBookOrder.getBean().refundAudit(vo);
        if (r.ok()) {
            if (vo.getAudit_status() == 1) {
                //审核通过，调用1次退款
                try {
                    SvcBookOrder.getBean().refund(vo.getRefund_id());
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                    ex.printStackTrace();
                }
            }
        }

        return r;
    }

    //接口退款
    @PostMapping("/refund")
    public XReturn refund() {
        String refundId = input("refund_id");
        XValidator.isNullOrEmpty(refundId, "miss refund_id");

        return SvcBookOrder.getBean().refund(refundId);
    }
}
