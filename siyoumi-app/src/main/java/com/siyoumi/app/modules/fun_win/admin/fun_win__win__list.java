package com.siyoumi.app.modules.fun_win.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FunWinSession;
import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.app.modules.fun_win.service.SvcFunWinSession;
import com.siyoumi.app.modules.prize.entity.EnumPrizeType;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSet;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/fun_win/fun_win__win__list")
public class fun_win__win__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("中奖列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "pset_name",
                "pset_type",
                "pset_pic",
                "fwins_id",
                "fwins_name",
                "fwins_begin_date",
                "fwins_end_date",
                "fwins_fun_num",
                "fwins_prize_total",
                "fwins_user_total",
                "fwins_win_date",
                "fwins_order",
        };
        JoinWrapperPlus<FunWinSession> query = SvcFunWinSession.getBean().listQuery(inputData);
        SvcFunWinSession.getApp().addQueryAcc(query);
        query.select(select);

        IPage<FunWinSession> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcFunWinSession.getApp().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            FunWinSession entity = SvcFunWinSession.getApp().loadEntity(data);
            data.put("id", entity.getKey());

            SysPrizeSet entityPrizeSet = SvcSysPrizeSet.getApp().loadEntity(data);
            String prizeType = IEnum.getEnmuVal(EnumPrizeType.class, entityPrizeSet.getPset_type());
            data.put("type", prizeType);
        }

        getR().setData("list", list);
        getR().setData("count", count);
        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcFunWinSession.getBean().delete(List.of(ids));
    }
}
