package com.siyoumi.app.modules.book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//取消预约
@Data
public class VaBookCancel {
    @HasAnyText(message = "请选择预约记录")
    String record_id;
}
