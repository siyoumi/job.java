package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.BookSet;
import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.modules.app_book.service.SvcBookSet;
import com.siyoumi.app.modules.app_book.service.SvcBookSku;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.app.modules.app_book.vo.VoBookSetAudit;
import com.siyoumi.app.modules.app_book.vo.VoBookSkuAudit;
import com.siyoumi.app.sys.entity.EnumEnable;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__set__list")
public class app_book__set__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("套餐列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "bset_id",
                "bset_name",
                "bset_day_price_breakfast",
                "bset_day_price_lunch",
                "bset_day_price_dinner",
                "bset_day_price",
                "bset_desc",
                "bset_enable",
                "bset_enable_date",
                "bset_order",
                "bset_del",
        };
        JoinWrapperPlus<BookSet> query = SvcBookSet.getBean().listQuery(inputData);
        query.eq("bset_store_id", getStoreId());
        query.select(select);

        IPage<BookSet> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcBookSet.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumEnable enumEnable = XEnumBase.of(EnumEnable.class); //上下架
        for (Map<String, Object> item : list) {
            BookSet entity = XBean.fromMap(item, BookSet.class);
            item.put("id", entity.getKey());
            //状态
            item.put("enable", enumEnable.get(entity.getBset_enable()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcBookSet.getBean().delete(getIds());
    }

    @Transactional
    @PostMapping("/audit")
    public XReturn audit(@Validated VoBookSetAudit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        vo.setStore_id(getStoreId());

        return SvcBookSet.getBean().audit(vo);
    }
}
