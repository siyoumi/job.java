package com.siyoumi.app.modules.sheet.admin;

import com.siyoumi.app.entity.SysSheet;
import com.siyoumi.app.modules.sheet.entity.EnumRegList;
import com.siyoumi.app.modules.sheet.entity.EnumSheetItemType;
import com.siyoumi.app.modules.sheet.entity.SysSheetItem;
import com.siyoumi.app.modules.sheet.service.SvcSheet;
import com.siyoumi.app.modules.sheet.vo.VaSysSheet;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/sheet/sheet__sheet__edit")
public class sheet__sheet__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("表单-编辑");

        List<SysSheetItem> items = new ArrayList<>();
        Map<String, Object> data = new HashMap<>();
        data.put("sheet_submit_type", 0);
        if (isAdminEdit()) {
            SysSheet entity = SvcSheet.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);

            items = SvcSheet.getBean().getItems(entity.getKey());
        }
        getR().setData("data", data);


        LinkedHashMap<String, String> regList = IEnum.toMap(EnumRegList.class); // 正则表达式
        LinkedHashMap<String, String> itemType = IEnum.toMap(EnumSheetItemType.class); // 子项类型

        LinkedMap<String, Object> mapItemData = new LinkedMap<>();
        mapItemData.put("reg_list", regList);
        mapItemData.put("types", itemType);

        setPageInfo("list_field", SvcSheet.getBean().mapFieldUnUse(items));
        setPageInfo("list_item", items);
        setPageInfo("item_data", mapItemData);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaSysSheet vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SvcSheet app = SvcSheet.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("sheet_x_id");
        }

        if (XStr.isNullOrEmpty(vo.getSheet_uix())) {
            vo.setSheet_uix(null);
        }

        InputData inputData = InputData.fromRequest();
        inputData.putAll(XBean.toMap(vo));
        return SvcSheet.getApp().saveEntity(inputData, true, ignoreField);
    }
}
