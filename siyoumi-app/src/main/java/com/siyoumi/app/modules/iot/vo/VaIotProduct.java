package com.siyoumi.app.modules.iot.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//产品
@Data
public class VaIotProduct {
    private String ipro_id;
    private String ipro_acc_id;
    private String ipro_key;

    @HasAnyText
    private String ipro_type;
    @HasAnyText
    @Size(max = 50)
    private String ipro_name;
    private String ipro_attr;
    private Integer ipro_order;
}
