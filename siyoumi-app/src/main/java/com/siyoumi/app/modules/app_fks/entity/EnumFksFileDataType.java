package com.siyoumi.app.modules.app_fks.entity;

import com.siyoumi.component.XEnumBase;


public class EnumFksFileDataType
        extends XEnumBase<String> {
    @Override
    protected void initKV() {
        put("room", "空间");
        put("proj", "产业");
        put("ecology", "生态");
        put("city", "城市建设");
        put("people", "社会民生");
    }
}
