package com.siyoumi.app.modules.mall2.web;

import com.siyoumi.app.entity.M2OrderRefund;
import com.siyoumi.app.modules.mall2.service.SvcM2Order;
import com.siyoumi.app.modules.mall2.vo.*;
import com.siyoumi.app.service.M2OrderRefundService;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/wxapp/mall2/api")
public class ApiM2Order
        extends ApiM2Base {
    /**
     * 下单
     */
    @PostMapping("order")
    @Transactional(rollbackFor = Exception.class)
    public XReturn order(@Validated @RequestBody OrderVo vo, BindingResult result) {
        //通用验证
        XValidator.getResult(result, true);

        return SvcM2Order.getBean().order(vo);
    }


    /**
     * 订单获取支付参数
     */
    @GetMapping("order_pay")
    public XReturn orderPay() {
        String orderId = getRequest().getParameter("order_id");
        if (XStr.isNullOrEmpty(orderId)) {
            return EnumSys.MISS_VAL.getR("miss order_id");
        }

        return SvcM2Order.getBean().orderPay(orderId);
    }

    /**
     * 订单支付检查
     */
    @Transactional(rollbackFor = Exception.class)
    @GetMapping("order_paycheck")
    public XReturn orderPayCheck() {
        String orderId = getRequest().getParameter("order_id");
        if (XStr.isNullOrEmpty(orderId)) {
            return EnumSys.MISS_VAL.getR("miss order_id");
        }

        return SvcM2Order.getBean().orderPayCheck(orderId);
    }

    /**
     * 订单列表
     */
    @GetMapping("order_list")
    public XReturn orderList() {
        return null;
    }

    @PostMapping("order_address_edit")
    @Transactional(rollbackFor = Exception.class)
    public XReturn orderAddressEdit(@Validated OrderAddressVo vo, BindingResult result) {
        //通用验证
        XValidator.getResult(result, true);
        //
        return SvcM2Order.getBean().orderAddressEdit(List.of(getID()), vo);
    }

    /**
     * 订单取消
     */
    @Transactional(rollbackFor = Exception.class)
    @GetMapping("order_cancel")
    public XReturn orderCancel() {
        String orderId = getRequest().getParameter("order_id");
        if (XStr.isNullOrEmpty(orderId)) {
            return EnumSys.MISS_VAL.getR("miss order_id");
        }

        return SvcM2Order.getBean().orderCancel(orderId);
    }


    //订单完成
    @GetMapping("order_done")
    @Transactional(rollbackFor = Exception.class)
    public XReturn orderDone(@Validated VoOrderDone vo, BindingResult result) {
        //通用验证
        XValidator.getResult(result, true);

        vo.setSrc(0);
        return SvcM2Order.getBean().orderDone(vo);
    }


    //子订单申请退款
    @PostMapping("order_item__refund")
    @Transactional(rollbackFor = Exception.class)
    public XReturn orderItemRefund(@Validated @RequestBody OrderItemRefundVo vo, BindingResult result) {
        //通用验证
        XValidator.getResult(result, true);

        vo.setApply_acc(null);
        return SvcM2Order.getBean().orderItemRefund(vo);
    }

    //子订单申请退款-撤回
    @GetMapping("order_item__refund_rollback")
    @Transactional(rollbackFor = Exception.class)
    public XReturn orderItemRefundRollback() {
        String refundId = getRequest().getParameter("refund_id");
        if (XStr.isNullOrEmpty(refundId)) {
            return EnumSys.MISS_VAL.getR("miss refund_id");
        }

        OrderItemRefundAuditVo vo = new OrderItemRefundAuditVo();
        vo.setRefund_id(refundId);
        vo.setAudit_status(-20);
        vo.setAudit_desc("");
        return SvcM2Order.getBean().orderItemRefundAudit(vo);
    }
}
