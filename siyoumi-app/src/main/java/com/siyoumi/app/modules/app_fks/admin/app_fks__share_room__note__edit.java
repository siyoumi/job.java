package com.siyoumi.app.modules.app_fks.admin;

import com.siyoumi.app.entity.FksPosition;
import com.siyoumi.app.modules.app_fks.entity.EnumFksPositionLevel;
import com.siyoumi.app.modules.app_fks.service.SvcFksPosition;
import com.siyoumi.app.modules.app_fks.vo.VaFksPosition;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__share_room__note__edit")
public class app_fks__share_room__note__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("笔记详情-编辑");

        Map<String, Object> data = new HashMap<>();
        if (isAdminEdit()) {
            FksPosition entity = SvcFksPosition.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            dataAppend.put("fposi_level", entity.getFposi_level().toString());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //职位权限
        EnumFksPositionLevel enumLevel = XEnumBase.of(EnumFksPositionLevel.class);
        setPageInfo("level", enumLevel);

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaFksPosition vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        //JoinWrapperPlus<FksPosition> query = SvcFksPosition.getBean().listQuery();
        //query.eq("fposi_level", vo.getFposi_level());
        //if (isAdminEdit()) {
        //    query.ne("fposi_id", getID());
        //}
        //FksPosition entityPosition = SvcFksPosition.getApp().first(query);
        //if (entityPosition != null) {
        //    result.addError(XValidator.getErr("fposi_level", "等级已存在"));
        //}
        //XValidator.getResult(result);
        //
        if (!isAdminEdit()) {
            vo.setFposi_acc_id(getAccId());
        } else {
            vo.setFposi_acc_id(null);
        }

        InputData inputData = InputData.fromRequest();
        return SvcFksPosition.getBean().edit(inputData, vo);
    }
}