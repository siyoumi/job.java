package com.siyoumi.app.modules.app_book.service.handle_order;

import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.app.modules.app_book.service.BookOrderHandleAbs;
import com.siyoumi.app.modules.app_book.vo.VoBookOrder;
import com.siyoumi.app.modules.fun.service.SvcFun;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XApp;
import com.siyoumi.util.XReturn;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 分销员分账
 */
public class BookOrderHandle20FunDown
        extends BookOrderHandleAbs
        implements IWebService {
    @Override
    public XReturn orderBeforeCheck() {
        if (getVo().getFun() <= 0) {
            return EnumSys.OK.getR();
        }
        if (getVo().getFun() % 100 != 0) {
            return EnumSys.ERR_VAL.getR("积分要1元为最小单位使用");
        }
        Long fun = SvcFun.getBean().updateFun(getUid());
        if (fun < getVo().getFun()) {
            return EnumSys.ERR_DATA.getR("用户积分不足");
        }

        return EnumSys.OK.getR();
    }

    @Override
    public String getTitle() {
        return "积分抵扣";
    }

    /**
     * 元转积分
     *
     * @param price
     */
    static public Integer money2Fun(BigDecimal price) {
        BigDecimal priceFun = price.multiply(BigDecimal.valueOf(100));
        return priceFun.intValue();
    }

    /**
     * 积分转元
     *
     * @param fun
     */
    static public BigDecimal fun2Money(Long fun) {
        return BigDecimal.valueOf(fun).divide(BigDecimal.valueOf(100), 2, RoundingMode.DOWN);
    }

    @Override
    public XReturn handle(BookOrder entityOrder) {
        entityOrder.setBorder_price_down_fun(BigDecimal.ZERO);
        if (getVo().getFun() > 0) {
            //有积分
            entityOrder.setBorder_price_down_fun(fun2Money(getVo().getFun().longValue()));
            SvcFun.getBean().add(entityOrder.getKey()
                    , entityOrder.getBorder_uid()
                    , entityOrder.getKey()
                    , "app_book"
                    , getVo().getFun()
                    , "房间预订"
            );
        }

        return EnumSys.OK.getR();
    }
}
