package com.siyoumi.app.modules.app_menu.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_menu.vo.VaType;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_menu/app_menu__type0__edit")
public class app_menu__type0__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("过敏源编辑");

        SysAbcService app = SysAbcService.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("abc_order", 0);
        data.put("abc_pid", getPid());
        if (isAdminEdit()) {
            SysAbc entity = app.loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated VaType vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAbcService app = SysAbcService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
            ignoreField.add("abc_pid");
        }

        InputData inputData = InputData.fromRequest();
        inputData.putAll(XBean.toMap(vo));
        if (isAdminAdd()) {
            inputData.put("abc_app_id", XHttpContext.getAppId());
            inputData.put("abc_table", "app_menu_type0");
            inputData.put("abc_uix", XApp.getStrID());
        }

        return app.saveEntity(inputData, true, ignoreField);
    }
}
