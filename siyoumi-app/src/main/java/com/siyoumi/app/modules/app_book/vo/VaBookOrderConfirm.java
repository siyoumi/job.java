package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//订单确认
@Data
public class VaBookOrderConfirm {
    @HasAnyText(message = "miss ids")
    private List<String> ids;
    @EqualsValues(vals = {"10", "1"})
    private Integer confirm;
    private String confirm_desc; //原因
}
