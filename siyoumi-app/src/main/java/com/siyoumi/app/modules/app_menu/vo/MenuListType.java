package com.siyoumi.app.modules.app_menu.vo;

import lombok.Data;

@Data
public class MenuListType {
    private String name;
    private String value;
    private Boolean checked;
}
