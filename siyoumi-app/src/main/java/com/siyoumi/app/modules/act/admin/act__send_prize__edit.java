package com.siyoumi.app.modules.act.admin;

import com.siyoumi.app.entity.ActSet;
import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.modules.act.service.SvcActSet;
import com.siyoumi.app.modules.act.vo.SendPrizeVo;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSet;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/act/act__send_prize__edit")
public class act__send_prize__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        String actId = input("act_id");
        if (XStr.isNullOrEmpty(actId)) {
            return EnumSys.MISS_VAL.getR("miss act_id");
        }

        SvcActSet app = SvcActSet.getBean();
        ActSet entityAct = app.getEntity(actId);
        if (entityAct == null) {
            return EnumSys.ERR_VAL.getR("act_id异常");
        }

        setPageTitle(entityAct.getAset_name(), "-派奖");

        Map<String, Object> data = new HashMap<>();
        if (isAdminEdit()) {
        }
        getR().setData("data", data);

        // 奖品列表
        List<SysPrizeSet> listPrize = app.listPrize(entityAct.getKey());
        setPageInfo("list_prize", listPrize);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() SendPrizeVo vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        // 自定义验证
        WxUser entityWxUser = WxUserService.getBean().getByOpenid(vo.getOpenid());
        if (entityWxUser == null) {
            result.addError(XValidator.getErr("openid", "用户不存在"));
        }
        XValidator.getResult(result);

        SysPrizeSet entityPrizeSet = SvcSysPrizeSet.getApp().getEntity(vo.getPset_id());
        return SvcActSet.getBean().chouBug(entityPrizeSet.getPset_id_src(), vo.getOpenid(), vo.getPset_id());
    }
}
