package com.siyoumi.app.modules.app_ess.entity;

import com.siyoumi.component.XEnumBase;


public class EnumEssFileType
        extends XEnumBase<String> {
    @Override
    protected void initKV() {
        put("video", "操作视频");
        put("file", "规范文档");
        put("build_sh", "施工脚本");
        put("build_video", "施工视频");
        put("build_img", "施工图片");
        put("build_paper", "施工图纸");
    }
}
