package com.siyoumi.app.modules.wechat.service;

import com.alibaba.fastjson.JSONObject;
import com.siyoumi.app.sys.service.wxapi.ApiBase;
import com.siyoumi.app.modules.wechat.entity.WechatGroupChatUser;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.util.XHttpClient;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
//微信小程序接口
public class WechatOpenApi
        extends ApiBase {
    static public WechatOpenApi getIns() {
        return getIns(null);
    }

    static public WechatOpenApi getIns(SysAccsuperConfig entityConfig) {
        WechatOpenApi app = new WechatOpenApi();
        return app;
    }

    String root = "http://127.0.0.1:9999/";

    @Override
    protected String getApiUrl(String urlFix) {
        return XStr.concat(root, urlFix);
    }

    /**
     * 获取群所有成员
     *
     * @param groupchatId 群Id
     */
    public List<WechatGroupChatUser> groupchatUser(String groupchatId) {
        String apiUrl = getApiUrl("chatroom-member");

        HashMap<String, String> mapData = new HashMap<>();
        mapData.put("roomid", groupchatId);
        String query = XStr.queryFromMap(mapData, null, false);
        String url = XStr.concat(apiUrl, "?", query);

        XReturn r = api(url);
        if (r.err()) {
            return null;
        }

        JSONObject data = r.getData("data");
        JSONObject members = data.getJSONObject("members");
        List<WechatGroupChatUser> listUser = new ArrayList<>();
        for (Map.Entry<String, Object> entry : members.entrySet()) {
            listUser.add(WechatGroupChatUser.getIns(entry.getKey(), (String) entry.getValue()));
        }
        return listUser;
    }

    /**
     * @param sendUserId
     * @param msg
     * @param at         多个用逗号分隔，@所有人 只需要 notify@all
     */
    public XReturn sendTxt(String sendUserId, String msg, String at) {
        Map<String, Object> postData = new HashMap<>();
        postData.put("receiver", sendUserId);
        postData.put("msg", msg);
        if (at != null) {
            //
            postData.put("aters", at);
        }

        String apiUrl = getApiUrl("text");
        return api(apiUrl, XHttpClient.METHOD_POST, postData);
    }

    /**
     * @param sendUserId
     * @param imgPath
     */
    public XReturn sendImage(String sendUserId, String imgPath) {
        Map<String, Object> postData = new HashMap<>();
        postData.put("receiver", sendUserId);
        postData.put("path", imgPath);

        String apiUrl = getApiUrl("image");
        return api(apiUrl, XHttpClient.METHOD_POST, postData);
    }

    @Override
    protected XReturn api(String url, String method, Map<String, Object> data) {
        XReturn r = super.api(url, method, data);
        Integer status = r.getData("status");
        String message = r.getData("message");
        r.setErrMsg(message);
        r.setErrCode(status);

        return r;
    }
}
