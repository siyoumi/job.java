package com.siyoumi.app.modules.app_admin.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.entity.SysApp;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.service.SysAppService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/app_admin/app_admin__list")
public class app_admin__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("应用管理");


        String compKw = input("compKw");


        SysAppService app = SysAppService.getBean();
        SysAccountService appAcc = SysAccountService.getBean();
        SysAccount entityAcc = appAcc.getSysAccountByToken();


        //query
        QueryWrapper<SysApp> query = app.getAppsQuery(entityAcc);
        query.orderByAsc("app_order");
        if (XStr.hasAnyText(compKw)) //应用名称
        {
            //
            query.like("app_name", compKw);
        }

        IPage<SysApp> page = new Page<>(getPageIndex(), getPageSize());

        IPage<SysApp> pageData = app.get(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<SysApp> listData = pageData.getRecords();
        List<Map<String, Object>> list = listData.stream().map(item ->
        {
            Map<String, Object> map = item.toMap();
            map.put("id", item.getKey());

            return map;
        }).collect(Collectors.toList());


        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    //@Transactional
    //@PostMapping("/del")
    public XReturn del() {
        //SysAccsuperZzzAppService app = SysAccsuperZzzAppService.getBean();
        //
        //String ids = input("ids");
        //if (XStr.isNullOrEmpty(ids)) {
        //    return XReturn.getR(SysErr.MISS_VAL.getErrcode(), "miss ids");
        //}
        //
        //String[] arr = ids.split(",");
        //List<String> collect = Arrays.stream(arr).collect(Collectors.toList());
        //app.removeBatchByIds(collect);


        return getR();
    }
}
