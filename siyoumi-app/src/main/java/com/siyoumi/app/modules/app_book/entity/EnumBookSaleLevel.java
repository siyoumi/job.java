package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;


public class EnumBookSaleLevel
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "普通班");
        put(5, "黄金班");
    }
}
