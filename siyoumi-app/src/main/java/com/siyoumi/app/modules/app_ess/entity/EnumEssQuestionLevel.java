package com.siyoumi.app.modules.app_ess.entity;

import com.siyoumi.component.XEnumBase;


public class EnumEssQuestionLevel
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "易容");
        put(5, "中等");
        put(10, "困难");
    }
}
