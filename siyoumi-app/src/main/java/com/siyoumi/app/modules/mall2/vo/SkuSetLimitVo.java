package com.siyoumi.app.modules.mall2.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

//设置限购
@Data
public class SkuSetLimitVo {
    @Size(min = 1)
    List<String> sku_ids;
    @NotNull
    Long limit_enable;
    @NotNull
    Long limit_max;
}
