package com.siyoumi.app.modules.fun_mall.service;

import com.siyoumi.app.entity.FunMallGroup;
import com.siyoumi.app.service.FunMallGroupService;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//积分商城-分组
@Service
@Slf4j
public class SvcFunMallGroup
        implements IWebService {
    static public SvcFunMallGroup getBean() {
        return XSpringContext.getBean(SvcFunMallGroup.class);
    }

    static public FunMallGroupService getApp() {
        return FunMallGroupService.getBean();
    }


    public List<FunMallGroup> list() {
        JoinWrapperPlus<FunMallGroup> query = listQuery(InputData.getIns());
        return getApp().get(query);
    }

    public JoinWrapperPlus<FunMallGroup> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<FunMallGroup> listQuery(InputData inputData) {
        String name = inputData.input("name");

        JoinWrapperPlus<FunMallGroup> query = getApp().join();
        query.eq("fgroup_x_id", XHttpContext.getX());
        getApp().addQueryAcc(query);

        //排序
        query.orderByAsc("fgroup_order")
                .orderByDesc("fgroup_id");
        if (XStr.hasAnyText(name)) //名称
        {
            query.like("fgroup_name", name);
        }

        return query;
    }


    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }
}
