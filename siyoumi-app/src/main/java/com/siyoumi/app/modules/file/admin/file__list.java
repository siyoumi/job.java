package com.siyoumi.app.modules.file.admin;

import com.siyoumi.app.modules.file.service.FileService;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.entity.SysApp;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.service.SysAppService;
import com.siyoumi.util.XReturn;
import com.siyoumi.component.http.InputData;
import com.siyoumi.util.XStr;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/xadmin/file/file__list")
public class file__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        String dir = input("dir", "");
        String appId = input("app_id");

        String appName = "";
        if (XStr.hasAnyText(appId)) {
            SysApp entityApp = SysAppService.getBean().getEntity(appId);
            if (entityApp == null) {
                return EnumSys.ERR_VAL.getR("app_id参数异常");
            }

            appName = entityApp.getApp_name();
        }

        setPageTitle(appName, "文件列表");
        if (XStr.hasAnyText(dir)) {
            setPageTitle(appName, "文件列表【", dir, "】");
        }

        FileService app = FileService.getBean();
        Object list = app.fileList(InputData.fromRequest());

        getR().setData("list", list);
        getR().setData("count", 100);

        setPageInfo("app_id", XHttpContext.getAppId());
        setPageInfo("dir", app.getHandlerDir(dir, ""));
        setPageInfo("do_all", SysAccsuperConfigService.isCenter());

//        SysAccount entityAcc = getEntityAcc();
//        setPageInfo("dev", SysAccountService.getBean().isDev(entityAcc));

        return getR();
    }


    //文件上传
    @PostMapping("/upload_file")
    public XReturn uploadFile(@RequestParam("file0") MultipartFile file0) {
        return FileService.getBean().uploadFile(file0, InputData.fromRequest());
    }

    //删除
    @PostMapping("/del")
    public XReturn del() {
        return FileService.getBean().del(InputData.fromRequest());
    }

    //重命名
    @GetMapping("/rename")
    public XReturn rename() {
        return FileService.getBean().rename(InputData.fromRequest());
    }

    //添加目录
    @GetMapping("/add_dir")
    public XReturn addDir() {
        return FileService.getBean().addDir(InputData.fromRequest());
    }

    //同步svn
    @GetMapping("/sync_svn")
    public XReturn syncSvn() {
        if (!SysAccountService.getBean().isDev(getEntityAcc())) {
            return EnumSys.ADMIN_AUTH_APP_ERR.getR();
        }
        return FileService.getBean().syncSvn();
    }

    //同步svn是否完成
    @GetMapping("/sync_svn_done")
    public XReturn syncSvnDone() {
        String key = input("key");
        if (XStr.isNullOrEmpty(key)) {
            return EnumSys.MISS_VAL.getR("miss key");
        }
        return FileService.getBean().syncSvnDone(key);
    }
}
