package com.siyoumi.app.modules.act.web;

import com.siyoumi.app.entity.ActSet;
import com.siyoumi.app.modules.act.service.SvcActSet;
import com.siyoumi.controller.WxAppApiController;

public class ActApiBase
        extends WxAppApiController {
    public String getActId() {
        return input("act_id");
    }

    /**
     * 获取活动配置
     */
    public ActSet getActSet() {
        return SvcActSet.getBean().getEntity(getActId());
    }
}
