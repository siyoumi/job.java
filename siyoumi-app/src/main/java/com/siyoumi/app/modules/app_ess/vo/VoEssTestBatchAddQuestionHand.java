package com.siyoumi.app.modules.app_ess.vo;

import lombok.Data;


//评测手动题目
@Data
public class VoEssTestBatchAddQuestionHand {
    String question_id; //场景ID
    Integer fun; //每题分数
}
