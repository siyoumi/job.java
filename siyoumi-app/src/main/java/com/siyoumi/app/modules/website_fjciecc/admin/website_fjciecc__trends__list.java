package com.siyoumi.app.modules.website_fjciecc.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.website_fjciecc.entity.EnumFjcieccPerformance;
import com.siyoumi.app.modules.website_fjciecc.entity.EnumFjcieccTrends;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/website_fjciecc/website_fjciecc__trends__list")
public class website_fjciecc__trends__list
        extends website_fjciecc__performance__list {
    @Override
    public String title() {
        return "公司动态";
    }

    @Override
    public String abcTable() {
        return "fjciecc_trends";
    }

    //for处理
    @Override
    public void forHandle(List<Map<String, Object>> list) {
        SysAbcService svcAbc = SysAbcService.getBean();

        LinkedHashMap<String, String> types = IEnum.toMap(EnumFjcieccTrends.class);
        for (Map<String, Object> data : list) {
            SysAbc entityAbc = svcAbc.loadEntity(data);
            data.put("id", entityAbc.getKey());
            data.put("type", types.get(entityAbc.getAbc_type()));

            data.put("send_date", XDate.toDateString(entityAbc.getAbc_date_00()));
        }

        setPageInfo("types", types);
    }


    @GetMapping()
    public XReturn index() {
        return super.index();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return super.del(ids);
    }
}
