package com.siyoumi.app.modules.app_fks.admin;

import com.siyoumi.app.entity.FksPosition;
import com.siyoumi.app.entity.FksUser;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_fks.service.SvcFksDepartment;
import com.siyoumi.app.modules.app_fks.service.SvcFksPosition;
import com.siyoumi.app.modules.app_fks.service.SvcFksUser;
import com.siyoumi.app.modules.app_fks.vo.VaFksUser;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.app.modules.user.vo.SysUserAudit;
import com.siyoumi.component.XApp;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__user__edit")
public class app_fks__user__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("职位-编辑");

        InputData inputData = InputData.fromRequest();

        Map<String, Object> data = new HashMap<>();
        if (isAdminEdit()) {
            SysUser entity = SvcSysUser.getApp().loadEntity(getID());
            FksUser entityFks = SvcFksUser.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
            data.putAll(entityFks.toMap());
        }
        getR().setData("data", data);

        //部门
        setPageInfo("department", SvcFksDepartment.getBean().getList());
        //职位
        setPageInfo("position", SvcFksPosition.getBean().getList());
        setPageInfo("def_pwd", SvcSysUser.defPwd()); //重置密码

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaFksUser vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        JoinWrapperPlus<FksUser> query = SvcFksUser.getBean().listQuery();
        query.eq("user_phone", vo.getUser_phone());
        if (isAdminEdit()) {
            query.ne(FksUser.tableKey(), getID());
        }
        FksUser entity = SvcFksUser.getApp().first(query);
        if (entity != null) {
            result.addError(XValidator.getErr("user_phone", "手机号已存在"));
        }
        XValidator.getResult(result);
        //
        if (isAdminEdit()) {
            SysUser entityUser = SvcSysUser.getApp().loadEntity(getID());
            if (entityUser.getUser_enable() == 1) {
                vo.setUser_phone(null); //审核通过手机号不能修改
            }
        } else {
            vo.setFuser_id(XApp.getStrID());
            vo.setUser_pwd("123456");
        }

        InputData inputData = InputData.fromRequest();
        return SvcFksUser.getBean().edit(inputData, vo);
    }

    /**
     * 重置密码
     */
    @PostMapping("/reset_pwd")
    @Transactional(rollbackFor = Exception.class)
    public XReturn resetPwd() {
        return SvcSysUser.getBean().resetPwd(getIds());
    }
}
