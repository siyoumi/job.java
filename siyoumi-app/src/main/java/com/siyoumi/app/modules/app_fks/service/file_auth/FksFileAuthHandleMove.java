package com.siyoumi.app.modules.app_fks.service.file_auth;

import com.siyoumi.app.modules.app_fks.service.FksFileAuthHandle;
import com.siyoumi.app.modules.app_fks.vo.FksFileAuthHandleData;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;

//移动权限
public class FksFileAuthHandleMove
        extends FksFileAuthHandle {
    @Override
    protected XReturn check(FksFileAuthHandleData data) {
        return EnumSys.ADMIN_AUTH_APP_ERR.getR();
    }
}
