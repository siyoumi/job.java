package com.siyoumi.app.modules.book.admin;

import com.siyoumi.app.entity.SysBook;
import com.siyoumi.app.entity.SysBookSku;
import com.siyoumi.app.modules.book.entity.BookInfo;
import com.siyoumi.app.modules.book.service.SvcSysBook;
import com.siyoumi.app.modules.book.service.SvcSysBookSku;
import com.siyoumi.app.modules.book.vo.VaBookSku;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/xadmin/book/book__sku__edit")
public class book__sku__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        InputData inputData = InputData.fromRequest();
        String bookId = inputData.input("book_id");

        Map<String, Object> data = new HashMap<>();
        data.put("bsku_order", 0);
        data.put("bsku_hide", 0);
        data.put("bsku_before_time", 0);
        data.put("bsku_book_id", bookId);
        setPageInfo("book_info_json", new BookInfo());
        if (isAdminEdit()) {
            SysBookSku entity = SvcSysBookSku.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());

            String[] nameArr = entity.getBsku_name().split("-");
            dataAppend.put("time_begin", nameArr[0]);
            dataAppend.put("time_end", nameArr[1]);
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);

            bookId = entity.getBsku_book_id();
        }
        getR().setData("data", data);

        SysBook entityBook = SvcSysBook.getApp().first(bookId);
        if (entityBook == null) {
            return EnumSys.ERR_VAL.getR("预约ID异常");
        }
        setPageTitle(entityBook.getBook_name(), "-预约时间段-编辑");

        //类型
        setPageInfo("book", entityBook);

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaBookSku vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        vo.setBsku_name(vo.getTime_begin() + "-" + vo.getTime_end());
        JoinWrapperPlus<SysBookSku> query = SvcSysBookSku.getBean().listQuery();
        query.eq("bsku_name", vo.getBsku_name())
                .eq("bsku_book_id", vo.getBsku_book_id());
        if (isAdminEdit()) {
            query.ne("bsku_id", getID());
        }
        SysBookSku entitySku = SvcSysBookSku.getApp().first(query);
        if (entitySku != null) {
            result.addError(XValidator.getErr("time_end", "时间段已存在"));
        }
        XValidator.getResult(result);
        //
        if (!isAdminEdit()) {
            vo.setBsku_acc_id(getAccId());
        } else {
            vo.setBsku_book_id(null);
            vo.setBsku_acc_id(null);
        }


        InputData inputData = InputData.fromRequest();
        return SvcSysBookSku.getBean().edit(inputData, vo);
    }
}
