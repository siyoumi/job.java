package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;

//订单退款状态
public class EnumBookOrderRefundAuditStatus
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "待审核");
        put(1, "通过");
        put(10, "不通过");
    }
}
