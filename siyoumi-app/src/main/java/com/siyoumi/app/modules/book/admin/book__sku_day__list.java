package com.siyoumi.app.modules.book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysBookSku;
import com.siyoumi.app.entity.SysBookSkuDay;
import com.siyoumi.app.modules.book.service.SvcSysBookSku;
import com.siyoumi.app.modules.book.service.SvcSysBookSkuDate;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/xadmin/book/book__sku_day__list")
public class book__sku_day__list
        extends AdminApiController {

    @GetMapping()
    public XReturn index() {
        InputData inputData = InputData.fromRequest();

        String skuId = inputData.input("sku_id");
        SysBookSku entitySku = SvcSysBookSku.getApp().first(skuId);
        if (entitySku == null) {
            return EnumSys.ERR_VAL.getR("时间段ID异常");
        }
        setPageTitle(entitySku.getBsku_name(), "-日期列表");

        String[] select = {
                "bday_id",
                "bday_date",
                "bday_del",
                "bday_sku_id",
                "stock_count_left",
                "stock_count_use",
                "bday_before_enable",
                "bday_before_time",
                "bday_append_max",
                "bsku_name",
        };
        JoinWrapperPlus<SysBookSkuDay> query = SvcSysBookSkuDate.getBean().listQuery(inputData);
        query.join(SysBookSku.table(), SysBookSku.tableKey(), "bday_sku_id");
        query.select(select);

        IPage<SysBookSkuDay> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcSysBookSkuDate.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            SysBookSkuDay entity = SvcSysBookSkuDate.getApp().loadEntity(data);
            data.put("id", entity.getKey());
            data.put("bday_date", XDate.toDateString(entity.getBday_date()));

            if (entity.getBday_before_enable() == 1) {
                //计算截止时间
                LocalDateTime dateTimeEnd = SvcSysBookSku.getBean().getDateTimeEnd(entitySku
                        , XDate.toDateString()
                        , entity.getBday_before_time());
                data.put("time_end", XDate.format(dateTimeEnd, "HH:mm"));
            }
        }

        getR().setData("list", list);
        getR().setData("count", count);


        return getR();
    }

    //删除
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcSysBookSkuDate.getBean().delete(List.of(ids));
    }
}
