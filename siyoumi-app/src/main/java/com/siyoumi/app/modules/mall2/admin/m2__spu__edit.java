package com.siyoumi.app.modules.mall2.admin;

import com.siyoumi.app.entity.M2Group;
import com.siyoumi.app.entity.M2Spu;
import com.siyoumi.app.modules.mall2.service.SvcM2Spu;
import com.siyoumi.app.modules.mall2.vo.SpuAttrVo;
import com.siyoumi.app.modules.mall2.vo.VaM2Spu;
import com.siyoumi.app.service.M2GroupService;
import com.siyoumi.app.service.M2SpuService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/mall2/m2__spu__edit")
public class m2__spu__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商品-编辑");

        SvcM2Spu app = SvcM2Spu.getBean();

        List<SpuAttrVo> listAttrSku = new ArrayList<>(); //商品属性
        Map<String, Object> data = new HashMap<>();
        data.put("mspu_price", 1);
        data.put("mspu_order", 0);
        if (isAdminEdit()) {
            M2Spu entity = SvcM2Spu.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());

            listAttrSku = app.getListAttrSku(entity);
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        setPageInfo("list_attr_sku", listAttrSku);

        return getR();
    }


    @PostMapping("/save")
    public XReturn save(@Validated VaM2Spu vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //自定交验证
        M2Group entityGroup = M2GroupService.getBean().getEntity(vo.getMspu_group_id());
        if (entityGroup == null) {
            result.addError(XValidator.getErr("mspu_group_id", "分组ID异常"));
        }
        String attrSku = input("attr_sku");
        if (XStr.isNullOrEmpty(attrSku)) {
            result.addError(XValidator.getErr("attr_sku", "请配置商品属性"));
        }
        List<SpuAttrVo> attr = XJson.parseArray(attrSku, SpuAttrVo.class);
        vo.setAttr(attr);

        M2SpuService app = M2SpuService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("mspu_x_id");
            ignoreField.add("mspu_type");
            ignoreField.add("mspu_app_id");
        } else {
            vo.setMspu_app_id("mall2");
            vo.setMspu_acc_id(getAccId());
        }

        M2Spu entitySpu = new M2Spu();
        XBean.copyProperties(vo, entitySpu);

        InputData inputData = InputData.fromRequest();
        inputData.putAll(entitySpu.toMap());
        inputData.put("attr", vo.getAttr()); //规格属性

        return XApp.getTransaction().execute(status -> {
            return app.saveEntity(inputData, vo, true, ignoreField);
        });
    }
}
