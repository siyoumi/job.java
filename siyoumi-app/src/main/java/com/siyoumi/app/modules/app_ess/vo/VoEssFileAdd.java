package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;


//资源文件-批量添加
@Data
public class VoEssFileAdd {
    @HasAnyText
    private String efile_module_id;
    @HasAnyText
    private String efile_type_sys;
    @HasAnyText
    private String efile_type;

    List<VaEssFile> files;
}
