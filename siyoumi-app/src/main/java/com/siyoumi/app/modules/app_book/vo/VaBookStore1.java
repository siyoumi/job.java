package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

//商家保存
@Data
public class VaBookStore1 {
    private String bstore_id;
    @HasAnyText
    private String bstore_acc_id;

    @HasAnyText
    @Size(max = 50)
    private String bstore_name;
    @Size(max = 1000)
    private String bstore_banner;
    private String bstore_point_x;
    private String bstore_point_y;
    @Size(max = 10)
    private String bstore_province;
    @Size(max = 10)
    private String bstore_city;
    @Size(max = 10)
    @HasAnyText
    private String bstore_district;
    @Size(max = 50)
    private String bstore_address;
    @Size(max = 100)
    private String bstore_tag;
    private Long bstore_altitude;
    private Integer bstore_foreign;
    @Size(max = 200)
    private String bstore_title;
    String bstoret_info;
    String bstoret_refund;
    @Size(max = 200)
    String bstore_refund_rule;

    List<String> item_play_ids;
    Integer set_item_play = 0;
}
