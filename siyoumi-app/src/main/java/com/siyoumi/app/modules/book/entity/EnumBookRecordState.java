package com.siyoumi.app.modules.book.entity;

import com.siyoumi.util.IEnum;


//号码组类型
public enum EnumBookRecordState
        implements IEnum {
    WAIT("0", "待审核"),
    OK("1", "已通过"),
    ERR("-1", "不通过"),
    ;


    private String key;
    private String val;

    EnumBookRecordState(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
