package com.siyoumi.app.modules.app_fks.admin;

import com.siyoumi.app.entity.FksCity;
import com.siyoumi.app.entity.FksDepartment;
import com.siyoumi.app.modules.app_fks.service.SvcFksCtiy;
import com.siyoumi.app.modules.app_fks.service.SvcFksDepartment;
import com.siyoumi.app.modules.app_fks.vo.VaFksCity;
import com.siyoumi.app.modules.app_fks.vo.VaFksDepartment;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__city__edit")
public class app_fks__city__edit
        extends AdminApiController {

    @GetMapping()
    public XReturn index() {
        setPageTitle("城市-编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("fcity_order", 0);
        if (isAdminEdit()) {
            FksCity entity = SvcFksCtiy.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaFksCity vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        vo.setFcity_acc_id(getAccId());
        //
        InputData inputData = InputData.fromRequest();
        return SvcFksCtiy.getBean().edit(inputData, vo);
    }
}