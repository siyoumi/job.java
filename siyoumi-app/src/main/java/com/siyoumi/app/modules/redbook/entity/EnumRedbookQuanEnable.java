package com.siyoumi.app.modules.redbook.entity;

import com.siyoumi.component.XEnumBase;

//审核状态
public class EnumRedbookQuanEnable
        extends XEnumBase<Integer> {
    static public EnumRedbookQuanEnable of() {
        return of(EnumRedbookQuanEnable.class);
    }

    @Override
    protected void initKV() {
        put(0, "待处理");
        put(1, "正常");
        put(10, "异常");
    }
}
