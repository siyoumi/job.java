package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.BookItem;
import com.siyoumi.app.entity.BookStoreGroup;
import com.siyoumi.app.modules.app_book.entity.EnumBookItemType;
import com.siyoumi.app.modules.app_book.service.SvcBookItem;
import com.siyoumi.app.modules.app_book.service.SvcBookStoreGroup;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__item__list")
public class app_book__item__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        InputData inputData = InputData.fromRequest();
        String type = inputData.input("type");
        XValidator.isNullOrEmpty(type, "miss type");

        EnumBookItemType enumType = XEnumBase.of(EnumBookItemType.class);

        setPageTitle(enumType.get(type), "-列表");

        String[] select = {
                "bitem_id",
                "bitem_type",
                "bitem_name",
                "bitem_str_00",
                "bitem_pic",
                "bitem_order",
        };
        JoinWrapperPlus<BookItem> query = SvcBookItem.getBean().listQuery(inputData);
        query.select(select);
        //
        IPage<BookItem> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcBookItem.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> item : list) {
            BookItem entity = XBean.fromMap(item, BookItem.class);

            item.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcBookItem.getBean().delete(getIds());
    }
}
