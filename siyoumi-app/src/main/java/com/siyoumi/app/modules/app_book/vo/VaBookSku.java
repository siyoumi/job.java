package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.math.BigDecimal;

//房源价格
@Data
public class VaBookSku {
    private String bsku_store_id;
    @HasAnyText
    private String bsku_spu_id;
    private String bsku_acc_id;
    @HasAnyText
    private Integer bsku_day;
    @HasAnyText
    private BigDecimal bsku_day_price;
}
