package com.siyoumi.app.modules.wx_qr.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.WxQr;
import com.siyoumi.app.service.WxQrService;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.component.http.XHttpContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/wp_qr/wp_qr__list")
public class wp_qr__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("小程序码列表");


        String compKw = input("compKw");


        WxQrService app = WxQrService.getBean();

        //query
        QueryWrapper<WxQr> query = app.q();
        query.eq("wxqr_x_id", XHttpContext.getX())
                .eq("wxqr_type_qr", "")
                .eq("wxqr_type", "wxapp")
                .orderByDesc("wxqr_create_date")
        ;
        if (XStr.hasAnyText(compKw)) //名称
        {
            query.like("wxqr_name", compKw);
        }

        IPage<WxQr> page = new Page<>(getPageIndex(), getPageSize());

        IPage<WxQr> pageData = app.get(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<WxQr> listData = pageData.getRecords();
        List<Map<String, Object>> list = listData.stream().map(item ->
        {
            Map<String, Object> map = item.toMap();
            map.put("id", item.getKey());

            return map;
        }).collect(Collectors.toList());


        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(List<String> ids) {
        WxQrService app = WxQrService.getBean();

        if (ids.size() <= 0) {
            return XReturn.getR(EnumSys.MISS_VAL.getErrcode(), "miss ids");
        }

        app.delete(ids);


        return getR();
    }
}
