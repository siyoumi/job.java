package com.siyoumi.app.modules.sh.entity;

import com.siyoumi.util.IEnum;

//奖品类型
public enum EnumShCmdRun
        implements IEnum {
    WAIT("0", "待执行"),
    RUN("10", "执行中"),
    DONE("1", "已完成"),
    ;


    private String key;
    private String val;

    EnumShCmdRun(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
