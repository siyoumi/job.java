package com.siyoumi.app.modules.redbook.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.RedbookQuan;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.modules.redbook.entity.EnumRedbookQuanEnable;
import com.siyoumi.app.modules.redbook.entity.EnumRedbookQuanType;
import com.siyoumi.app.modules.redbook.service.SvcRedbookQuan;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/redbook/redbook__item__list")
public class redbook__item__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("小红书列表");

        String appId = XHttpContext.getAppId();

        InputData inputData = InputData.fromRequest();
        String compKw = inputData.input("compKw");

        SvcRedbookQuan svcRedbookQuan = SvcRedbookQuan.getBean();
        JoinWrapperPlus<RedbookQuan> query = svcRedbookQuan.listQuery(inputData);
        query.join(SysUser.table(), SysUser.tableKey(), "rquan_uid");

        String[] select = {
                "rquan_id",
                "rquan_create_date",
                "rquan_uid",
                "rquan_type",
                "rquan_content_src",
                "rquan_url",
                "rquan_thumbnail",
                "rquan_star",
                "rquan_top",
                "rquan_good_count",
                "rquan_collect_count",
                "rquan_comment_count",
                "rquan_enable",
                "rquan_enable_date",
                "user_name",
                "user_id",
        };
        query.select(select);

        if (XStr.hasAnyText(compKw)) { // 名称
            query.like("abc_name", compKw);
        }

        IPage<RedbookQuan> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcRedbookQuan.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();
        //
        EnumRedbookQuanType enumRedbookQuanType = EnumRedbookQuanType.of();
        EnumRedbookQuanEnable enumRedbookQuanEnable = EnumRedbookQuanEnable.of();
        //
        for (Map<String, Object> data : list) {
            RedbookQuan entity = SvcRedbookQuan.getApp().loadEntity(data);
            data.put("id", entity.getKey());

            data.put("type", enumRedbookQuanType.get(entity.getRquan_type()));
            data.put("enable", enumRedbookQuanEnable.get(entity.getRquan_enable()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        //审核状态
        setPageInfo("enable_arr", enumRedbookQuanEnable);

        return getR();
    }


    //删除
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcRedbookQuan.getBean().delete(Arrays.asList(ids), null);
    }

    //置顶
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/top")
    public XReturn top(String[] ids) {
        Integer top = XStr.toInt(input("top"), 0);

        return SvcRedbookQuan.getBean().top(Arrays.asList(ids), top);
    }

    //审核
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/audit")
    public XReturn audit(String[] ids) {
        Integer top = XStr.toInt(input("enable"), 0);

        return SvcRedbookQuan.getBean().enable(Arrays.asList(ids), top);
    }
}
