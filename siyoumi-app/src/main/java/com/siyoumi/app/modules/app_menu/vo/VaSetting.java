package com.siyoumi.app.modules.app_menu.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class VaSetting {
    @NotBlank(message = "miss id")
    private String id;
    @Size(max = 2000)
    private String abc_txt_00;
    @Size(max = 50)
    private String abc_str_00;
}
