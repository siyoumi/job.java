package com.siyoumi.app.modules.money.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.entity.SysMoneyRecord;
import com.siyoumi.app.entity.SysOrder;
import com.siyoumi.app.modules.money.service.PayOrderMoney;
import com.siyoumi.app.modules.money.service.SvcMoneyRecord;
import com.siyoumi.app.modules.money.vo.MoneyOrderVo;
import com.siyoumi.app.sys.service.IPayOrder;
import com.siyoumi.app.sys.service.payorder.PayOrder;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.app.service.SysOrderService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/wxapp/money/api")
public class money_api
        extends WxAppApiController {
    /**
     * 钱包明细
     */
    @GetMapping({"list", "funcList"})
    public XReturn list() {
        InputData inputData = InputData.fromRequest();

        String[] select = {
                "mrec_num",
                "mrec_desc",
                "mrec_create_date",
        };
        JoinWrapperPlus<SysMoneyRecord> query = SvcMoneyRecord.getBean().getQuery(inputData);
        query.select(select);

        IPage<SysMoneyRecord> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = SvcMoneyRecord.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();


        BigDecimal money = SvcMoneyRecord.getBean().getMoney(getOpenid());

        getR().setData("list", list);
        getR().setData("money", money);


        return getR();
    }

    /**
     * 充值项
     */
    @GetMapping({"list_option", "funcListItem"})
    public XReturn listOption() {
        InputData inputData = InputData.fromRequest();

        SysAbcService svcAbc = SysAbcService.getBean();

        String[] select = {
                "abc_id",
                "abc_name",
                "abc_str_00",
                "abc_int_00",
                "abc_num_00",
                "abc_num_01",
        };
        JoinWrapperPlus<SysAbc> query = svcAbc.listQuery("money", false);
        query.select(select);
        query.eq("abc_table", "money_option")
                .eq("abc_int_01", 0);

        IPage<SysAbc> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = svcAbc.getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();

        log.debug("更新用户余额");
        SvcMoneyRecord.getBean().updateMoney(getUid(), false);

        getR().setData("list", list);


        return getR();
    }

    /**
     * 充值下单
     */
    @PostMapping({"order", "funcOrder"})
    public XReturn order(@Validated() MoneyOrderVo vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        String optionId = vo.getId();
        if (vo.getType() == 1) {
            log.debug("自定义充值金额");
            if (vo.getPay_price() == null) {
                return EnumSys.MISS_VAL.getR("请输入支付金额");
            }
        } else {
            log.debug("固定金额");
            if (XStr.isNullOrEmpty(vo.getId())) {
                return EnumSys.ERR_VAL.getR("miss id");
            }
        }

        return PayOrderMoney.order(getUid(), optionId, vo.getPay_price());
    }

    /**
     * 支付检查
     */
    @GetMapping({"check_pay", "funcCheckPay"})
    public XReturn checkPay() {
        String orderId = input("order_id");
        if (XStr.isNullOrEmpty(orderId)) {
            return EnumSys.MISS_VAL.getR("miss order_id");
        }

        SysOrder entityOrder = SysOrderService.getBean().getEntityByOrderId(orderId, false);
        IPayOrder payOrder = PayOrder.getBean(entityOrder);

        return payOrder.payCheck();
    }
}

