package com.siyoumi.app.modules.mall2.vo;

import com.siyoumi.app.entity.M2Sku;
import com.siyoumi.util.XStr;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class SpuAttrData {
    String id0; //1级规格
    String name0;

    String id1 = ""; //2级规格
    String name1;

    String id2 = ""; //3级规格
    String name2;

    Boolean sync = true; //false：未同步,true：已同步

    M2Sku entity_sku;
    //库存
    Long count_left;
    Long count_use;

    public String skuName() {
        List<String> arr = new LinkedList<>();
        if (XStr.hasAnyText(getName0())) {
            arr.add(getName0());
        }
        if (XStr.hasAnyText(getName1())) {
            arr.add(getName1());
        }
        if (XStr.hasAnyText(getName2())) {
            arr.add(getName2());
        }

        return String.join(",", arr);
    }

    /**
     * 收集所有规格ID，标记已同步用
     */
    public List<String> attrSkuIds() {
        List<String> arr = new LinkedList<>();
        if (XStr.hasAnyText(getId0())) {
            arr.add(getId0());
        }
        if (XStr.hasAnyText(getId1())) {
            arr.add(getId1());
        }
        if (XStr.hasAnyText(getId2())) {
            arr.add(getId2());
        }

        return arr;
    }
}
