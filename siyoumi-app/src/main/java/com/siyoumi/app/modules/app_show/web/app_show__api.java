package com.siyoumi.app.modules.app_show.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/wxapp/app_show/z_api")
public class app_show__api
        extends WxAppApiController {
    /**
     * 初始化
     */
    @GetMapping("init")
    public XReturn init() {
        SysAbcService svcAbc = SysAbcService.getBean();
        SysAbc setting = svcAbc.getEntityByUix("app_show_setting");
        if (setting == null) {
            return EnumSys.ERR_VAL.getR("应用未初始化");
        }

        getR().setData("abc_str_00", setting.getAbc_str_00());
        getR().setData("abc_txt_00", setting.getAbc_txt_00());
        getR().setData("abc_txt_01", setting.getAbc_txt_01());

        return getR();
    }

    /**
     * 项目列表
     */
    @GetMapping("list_job")
    public XReturn listJob() {
        SysAbcService svcAbc = SysAbcService.getBean();

        List<String> select = new ArrayList<>();
        select.add("abc_id");
        select.add("abc_name");
        select.add("abc_str_00");
        select.add("abc_txt_00");
        select.add("abc_str_01");
        select.add("abc_str_02");
        select.add("abc_date_00");

        JoinWrapperPlus<SysAbc> query = svcAbc.listQuery(XHttpContext.getAppId(), false);
        query.eq("abc_table", "app_show_item");
        query.select(select.toArray(new String[select.size()]));

        IPage<SysAbc> page = new Page<>(getPageIndex(), getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = svcAbc.getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            SysAbc entityAbc = XBean.fromMap(data, SysAbc.class);
            data.put("id", entityAbc.getKey());

            return data;
        }).collect(Collectors.toList());

        getR().setData("list", listData);

        return getR();
    }
}
