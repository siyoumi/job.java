package com.siyoumi.app.modules.money.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.money.vo.VaMoneyItem;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/money/money__item__edit")
public class money__item__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("钱包充值项-编辑");

        SysAbcService app = SysAbcService.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("abc_order", 0);
        data.put("abc_int_00", 0);
        data.put("abc_num_00", 0);
        data.put("abc_num_01", 0);
        data.put("abc_str_00", XApp.getStrID());
        if (isAdminEdit()) {
            SysAbc entity = app.getEntityByTable(getID(), "money_option");

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaMoneyItem vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAbcService app = SysAbcService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
            ignoreField.add("abc_pid");
            ignoreField.add("abc_str_00");
            ignoreField.add("abc_int_00");
            ignoreField.add("abc_num_00");
            ignoreField.add("abc_num_01");
        }

        InputData inputData = InputData.fromRequest();
        inputData.putAll(XBean.toMap(vo));
        if (isAdminAdd()) {
            inputData.put("abc_app_id", XHttpContext.getAppId());
            inputData.put("abc_table", "money_option");
            inputData.put("abc_uix", XApp.getStrID());
        }

        inputData.putAll(XBean.toMap(vo));
        return app.saveEntity(inputData, true, ignoreField);
    }
}

