package com.siyoumi.app.modules.app_show.entity;

import com.siyoumi.util.IEnum;

//订单来源
public enum EnumJobOrderSrc
        implements IEnum {
    WXAPP("wxapp", "小程序"),
    ADMIN("admin", "后台添加"),
    AUTO("auto", "系统生成");


    private String key;
    private String val;

    EnumJobOrderSrc(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key.toString();
    }
}
