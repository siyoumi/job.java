package com.siyoumi.app.modules.money.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.entity.SysMoneyRecord;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.modules.money.service.SvcMoneyRecord;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.app.service.SysMoneyRecordService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/money/money__record__list")
public class money__record__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("钱包-操作记录");

        String appId = XHttpContext.getAppId();

        InputData inputData = InputData.fromRequest();
        String compKw = inputData.input("compKw");

        SvcMoneyRecord svcMoneyRecord = SvcMoneyRecord.getBean();
        JoinWrapperPlus<SysMoneyRecord> query = svcMoneyRecord.getQuery(inputData);
        query.join(WxUser.table(), "wxuser_openid", "mrec_openid");

        List<String> select = new ArrayList<>();
        select.add("t_s_money_record.*");
        select.add("wxuser_openid");
        select.add("wxuser_nickname");
        select.add("wxuser_headimgurl");
        query.select(select.toArray(new String[select.size()]));
        query.orderByDesc("mrec_id");

        if (XStr.hasAnyText(compKw)) { // 昵称
            query.likeRight("wxuser_nickname", compKw);
        }

        IPage<SysMoneyRecord> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), !isAdminExport());
        //list
        IPage<Map<String, Object>> pageData = SvcMoneyRecord.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            SysMoneyRecord entity = SvcMoneyRecord.getApp().loadEntity(data);
            data.put("id", entity.getKey());
            data.put("can_refund", entity.canRefund());

            return data;
        }).collect(Collectors.toList());

        if (!isAdminExport()) {
            getR().setData("list", listData);
            getR().setData("count", count);
        } else {
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("mrec_id", "ID");
            tableHead.put("mrec_app_id", "应用");
            tableHead.put("wxuser_openid", "openid");
            tableHead.put("wxuser_nickname", "微信名");
            tableHead.put("mrec_num", "数值");
            tableHead.put("mrec_desc", "描述");
            tableHead.put("mrec_create_date", "时间");

            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }

        return getR();
    }

    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }
}
