package com.siyoumi.app.modules.website_fjciecc.admin;

import com.siyoumi.app.modules.website_fjciecc.entity.EnumFjcieccTrends;
import com.siyoumi.app.modules.website_fjciecc.service.WebSiteFjcieccEditBase;
import com.siyoumi.app.modules.website_fjciecc.vo.VaFjcieccTrends;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/xadmin/website_fjciecc/website_fjciecc__trends__edit")
public class website_fjciecc__trends__edit
        extends WebSiteFjcieccEditBase {
    @Override
    public String title() {
        return "公司动态-编辑";
    }

    @Override
    public String abcTable() {
        return "fjciecc_trends";
    }

    @Override
    public void indexAfter() {
        //类型
        LinkedHashMap<String, String> types = IEnum.toMap(EnumFjcieccTrends.class);
        setPageInfo("types", types);
    }

    @GetMapping()
    public XReturn index() {
        return super.index();
    }

    @PostMapping("/save")
    public XReturn save(@Validated() VaFjcieccTrends vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAbcService svcAbc = SysAbcService.getBean();

        vo.setAbc_table(abcTable());
        vo.setAbc_app_id(XHttpContext.getAppId());

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
            ignoreField.add("abc_app_id");
        }

        InputData inputData = InputData.fromRequest();

        return svcAbc.saveEntity(inputData, vo, true, ignoreField);
    }
}
