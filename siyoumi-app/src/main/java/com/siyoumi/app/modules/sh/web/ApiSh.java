package com.siyoumi.app.modules.sh.web;

import com.siyoumi.app.entity.ShCmd;
import com.siyoumi.app.entity.ShUser;
import com.siyoumi.app.feign.view_admin.SiyoumiFeign;
import com.siyoumi.app.modules.sh.service.QinglongApi;
import com.siyoumi.app.modules.sh.service.SvcSh;
import com.siyoumi.app.modules.sh.service.SvcShCcbUser;
import com.siyoumi.app.modules.sh.service.SvcShCmd;
import com.siyoumi.app.service.ShUserService;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.ApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/app/sh/sh_api")
public class ApiSh
        extends ApiController {
    @SneakyThrows
    @GetMapping("/sh_export_config")
    public XReturn shExportConfig() {
        XHttpContext.setX("x");
        //
        String config = SvcSh.getBean().shExportConfig();
        getR().setData("config", config);

        return getR();
    }

    @GetMapping("/sh_set_qinglong_config")
    public XReturn shSetQinglongConfig() {
        String enable = input("enable");
        String config = SiyoumiFeign.getBean().shExportConfig();

        QinglongApi api = QinglongApi.getInstance();
        api.setEvn(1, "JD_COOKIE", config);

        if ("1".equals(enable)) {
            api.evnEnable(1);
        }

        return getR();
    }

    @GetMapping("/ccb_config")
    public String ccbConfig() {
        XHttpContext.setX("x");

        return SvcShCcbUser.getBean().exportConfig();
    }

    @GetMapping("/ccb_config_sh")
    public String ccbConfigSh() {
        XHttpContext.setX("x");

        return SvcShCcbUser.getBean().exportConfigSh();
    }


    @GetMapping("/cmd")
    public XReturn cmd() {
        XHttpContext.setX("x");

        JoinWrapperPlus<ShCmd> query = SvcShCmd.getBean().listQuery();
        query.eq("scmd_run", 0);
        ShCmd entity = SvcShCmd.getApp().first(query);
        if (entity == null) {
            return XReturn.getR(20054, "没脚本");
        }

        getR().setData("id", entity.getKey());
        getR().setData("cmd", entity.getScmd_script());

        return getR();
    }

    @PostMapping("/cmd_done")
    public XReturn cmdDone() {
        Integer run = XStr.toInt(input("run"));
        String runMsg = input("return_msg", "");
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }

        XHttpContext.setX("x");
        return SvcShCmd.getBean().setRun(getID(), 1, runMsg);
    }

    @GetMapping("/cmd_run")
    public XReturn cmdRun() {
        Integer run = XStr.toInt(input("run"));
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }

        XHttpContext.setX("x");
        return SvcShCmd.getBean().setRun(getID(), 10);
    }
}
