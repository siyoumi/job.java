package com.siyoumi.app.modules.mall2.admin;

import com.siyoumi.app.modules.mall2.service.SvcM2Spu;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.component.http.InputData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/xadmin/mall2/m2__spu__list")
public class m2__spu__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商品-列表");

        SvcM2Spu app = SvcM2Spu.getBean();
        XReturn r = app.list(InputData.fromRequest(), true);
        getR().putAll(r);

        return getR();
    }


    //删除
    @PostMapping("/del")
    public XReturn del(List<String> ids) {
        return SvcM2Spu.getBean().delete(ids);
    }
}
