package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//文件重命名
@Data
public class FksFileRename {
    @HasAnyText(message = "缺少文件ID")
    private String file_id;
    @HasAnyText(message = "请输入文件名")
    @Size(max = 50, message = "名称长度不能超50")
    private String file_name;
}
