package com.siyoumi.app.modules.app_ess.interceptor;

import com.siyoumi.app.entity.EssUser;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_ess.service.SvcEssUser;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.interceptor.InterceptorBase;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//fks api
@Slf4j
public class AppEssApiTeacherInterceptor
        extends InterceptorBase
        implements HandlerInterceptor {
    /**
     * 执行前
     *
     * @return boolean
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("BEGIN");

        XReturn tokenData = XHttpContext.getTokenData();
        String uid = tokenData.getData("uid");

        EssUser entityUser = SvcEssUser.getBean().getEntity(uid);
        if (entityUser.getEuser_type() != 1) {
            return returnErr(response, EnumSys.WEB_AUTH_ERR.getR("帐号权限不足"));
        }

        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("END");
    }
}
