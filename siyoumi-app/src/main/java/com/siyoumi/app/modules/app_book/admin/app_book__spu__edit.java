package com.siyoumi.app.modules.app_book.admin;

import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.entity.BookSpuTxt;
import com.siyoumi.app.entity.BookStoreGroup;
import com.siyoumi.app.modules.app_book.entity.*;
import com.siyoumi.app.modules.app_book.service.SvcBookItem;
import com.siyoumi.app.modules.app_book.service.SvcBookSet;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.modules.app_book.service.SvcBookStoreGroup;
import com.siyoumi.app.modules.app_book.vo.VaBookSpu;
import com.siyoumi.app.modules.app_book.vo.VaBookSpuTxt;
import com.siyoumi.app.modules.app_book.vo.VaBookStoreGroup;
import com.siyoumi.app.service.BookSpuTxtService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_book/app_book__spu__edit")
public class app_book__spu__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("房源-编辑");

        VaBookSpuTxt spuTxt = new VaBookSpuTxt(); //扩展表
        List<String> setIds = new ArrayList<>(); //套餐列表
        List<String> itemDeviceIds = new ArrayList<>();//房源设备
        Boolean canEdit = true;

        Map<String, Object> data = new HashMap<>();
        data.put("bspu_order", 0);
        data.put("bspu_view_type", "0");
        data.put("bspu_toilet_type", "0");
        data.put("bspu_toilet", "0");
        if (isAdminEdit()) {
            BookSpu entity = SvcBookSpu.getApp().loadEntity(getID());
            //
            BookSpuTxt entityTxt = BookSpuTxtService.getBean().getEntity(entity.getKey());
            //
            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
            //房源介绍
            if (XStr.hasAnyText(entityTxt.getBsput_txt_00())) {
                spuTxt = XJson.parseObject(entityTxt.getBsput_txt_00(), VaBookSpuTxt.class);
            }
            data.put("bspu_view_type", entity.getBspu_view_type().toString());
            data.put("bspu_toilet_type", entity.getBspu_toilet_type().toString());
            data.put("bspu_toilet", entity.getBspu_toilet().toString());
            data.put("bspu_user_count", entity.getBspu_user_count().toString());
            //data.put("bspu_bed_12", entity.getBspu_bed_12().toString());
            //data.put("bspu_bed_13", entity.getBspu_bed_13().toString());
            //data.put("bspu_bed_18", entity.getBspu_bed_18().toString());

            canEdit = SvcBookSpu.getBean().canEdit(entity).ok();
            //套餐ids
            setIds = SvcBookSpu.getBean().getSpuSetIds(entity.getBspu_id());
            //房源设备
            itemDeviceIds = SvcBookItem.getBean().getItemIds("device", entity.getKey());
        }
        data.put("bsput_txt_00", spuTxt);
        data.put("set_ids", setIds);
        data.put("item_device_ids", itemDeviceIds);
        getR().setData("data", data);

        EnumBookSpuUserCount enumUserCount = XEnumBase.of(EnumBookSpuUserCount.class); //房型
        EnumBookSpuFoodType enumFoodType = XEnumBase.of(EnumBookSpuFoodType.class); //做饭
        EnumBookSpuToilet enumToilet = XEnumBase.of(EnumBookSpuToilet.class); //厕所
        EnumBookSpuToiletType enumToiletType = XEnumBase.of(EnumBookSpuToiletType.class); //厕所类型
        EnumBookSpuViewType enumViewType = XEnumBase.of(EnumBookSpuViewType.class); //景色
        setPageInfo("enum_user_count", enumUserCount);
        setPageInfo("enum_food_type", enumFoodType);
        setPageInfo("enum_toilet", enumToilet);
        setPageInfo("enum_toilet_type", enumToiletType);
        setPageInfo("enum_view_type", enumViewType);
        setPageInfo("can_edit", canEdit);
        //套餐
        setPageInfo("list_set", SvcBookSet.getBean().getList(getStoreId()));
        //房源设备
        setPageInfo("items_device", SvcBookItem.getBean().getList("device", null));

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaBookSpu vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        if (isAdminAdd()) {
            if (vo.getSet_ids().isEmpty()) {
                result.addError(XValidator.getErr("set_ids", "请选择套餐"));
            }
        }
        XValidator.getResult(result);
        //
        InputData inputData = InputData.fromRequest();
        if (isAdminAdd()) {
            vo.setBspu_id(XApp.getStrID("P"));
            vo.setBspu_acc_id(getAccId());
            vo.setBspu_store_id(getStoreId());
        } else {
            BookSpu entitySpu = SvcBookSpu.getApp().getEntity(inputData.getID());
            XValidator.err(SvcBookSpu.getBean().canEdit(entitySpu));

            vo.setBspu_user_count(null);
        }
        //
        return SvcBookSpu.getBean().edit(inputData, vo);
    }
}
