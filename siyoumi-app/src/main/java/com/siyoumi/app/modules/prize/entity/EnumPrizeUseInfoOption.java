package com.siyoumi.app.modules.prize.entity;

import com.siyoumi.util.IEnum;

//奖品类型
public enum EnumPrizeUseInfoOption
        implements IEnum {
    NAME("prize_user_name", "姓名"),
    PHONE("prize_user_phone", "手机"),
    ADDRESS("prize_user_address", "地址");


    private String key;
    private String val;

    EnumPrizeUseInfoOption(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
