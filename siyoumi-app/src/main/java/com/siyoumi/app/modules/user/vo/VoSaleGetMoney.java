package com.siyoumi.app.modules.user.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

//分销员提现
@Data
@Accessors(chain = true)
public class VoSaleGetMoney {
    private String uid;

    private BigDecimal money; //提现金额
}
