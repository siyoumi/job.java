package com.siyoumi.app.modules.view_admin.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.feign.view_admin.SiyoumiFeign;
import com.siyoumi.config.SysConfig;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysApp;
import com.siyoumi.entity.SysAppRouter;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.SysAppRouterService;
import com.siyoumi.service.SysAppService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import com.siyoumi.component.http.InputData;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/view_admin/view_admin__page__list")
public class view_admin__page__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("页面管理-列表");

        SysAppRouterService app = SysAppRouterService.getBean();
        //query
        QueryWrapper<SysAppRouter> query = app.getListQuery(InputData.fromRequest());
        //page
        IPage<SysAppRouter> page = new Page<>(getPageIndex(), getPageSize());

        IPage<SysAppRouter> pageData = app.get(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<SysAppRouter> listData = pageData.getRecords();
        List<Map<String, Object>> list = listData.stream().map(item ->
        {
            Map<String, Object> map = item.toMap();
            map.put("id", item.getKey());

            return map;
        }).collect(Collectors.toList());


        getR().setData("list", list);
        getR().setData("count", count);

        setPageInfo("is_dev", SysConfig.getIns().isDev());

        //1级列表
        InputData inputData = InputData.getIns();
        inputData.put("pid", "all_1");
        QueryWrapper<SysAppRouter> querySysAppRouter = app.getListQuery(inputData);
        querySysAppRouter.select("appr_id", "appr_meta_title");
        List<SysAppRouter> listApp = app.list(querySysAppRouter);
        setPageInfo("list_app", listApp);

        //应用列表
        SysAppService appApp = SysAppService.getBean();
        List<SysApp> apps = appApp.list(appApp.q().select("app_id", "app_name"));
        setPageInfo("apps", apps);

        return getR();
    }


    //删除
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/del")
    public XReturn del(List<String> ids) {
        if (ids.size() <= 0) {
            return XReturn.getR(EnumSys.MISS_VAL.getErrcode(), "miss ids");
        }

        SysAppRouterService app = SysAppRouterService.getBean();
        app.removeBatchByIds(ids);

        return getR();
    }


    //开发环境同步到siyoumi
    @GetMapping("/sync_to_siyoumi")
    public XReturn syncToSiyoumi() {
        XValidator.isDev();

        List<SysAppRouter> list = SysAppRouterService.getBean().list();
        return SiyoumiFeign.getBean().updateAppRouter(list);
    }

    //siyoumi同步到其他服务器
    @Deprecated
    @GetMapping("/sync_from_siyoumi")
    @Transactional(rollbackFor = Exception.class)
    public XReturn syncFromSiyoumi() {
        List<SysAppRouter> list = SiyoumiFeign.getBean().getListAppr();
        return SysAppService.getBean().syncApp(list);
    }

    @GetMapping("/redis_val")
    public XReturn redisVal() {
        String key = input("key");
        if (XStr.isNullOrEmpty(key)) {
            return EnumSys.MISS_VAL.getR("miss key");
        }

        return SiyoumiFeign.getBean().getRedisVal(key);
    }
}
