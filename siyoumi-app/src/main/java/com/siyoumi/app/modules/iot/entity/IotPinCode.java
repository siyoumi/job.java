package com.siyoumi.app.modules.iot.entity;

import com.siyoumi.component.XApp;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.Data;

/**
 * 引脚命令
 */
@Data
public class IotPinCode {
    String actionId;

    /**
     * 场景
     */
    String action;

    /**
     * 引脚序号
     */
    Integer pin;
    /**
     * 引脚动作
     * 0：低电压
     * 1：高电压
     * 2：点击
     */
    Integer pinAction;
    /**
     * out：输出模式
     * in：输入模式
     */
    String pinMode = "out";


    public XReturn toR() {
        XReturn r = XReturn.getR(0);
        r.setData("action_id", getActionId());
        r.setData("action", getAction());
        r.setData("pin", getPin());
        r.setData("pin_action", getPinAction());
        r.setData("pin_mode", getPinMode());

        return r;
    }

    public static IotPinCode parse(String json) {
        XReturn r = XReturn.parse(json);
        IotPinCode data = IotPinCode.of(r.getData("action")
                , r.getData("pin")
                , r.getData("pin_action")
                , r.getData("pin_mode")
        );
        data.setActionId(r.getData("action_id", null));

        return data;
    }

    public static IotPinCode of(Integer pin, Integer pinAction) {
        return of("", pin, pinAction, "out");
    }

    public static IotPinCode of(String action, Integer pin, Integer pinAction, String pinMode) {
        IotPinCode data = new IotPinCode();
        data.setAction(action);
        data.setPin(pin);
        data.setPinAction(pinAction);

        if (XStr.hasAnyText(pinMode)) {
            data.setPinMode(pinMode);
        }
        data.setActionId(XApp.getStrID("A"));

        return data;
    }
}
