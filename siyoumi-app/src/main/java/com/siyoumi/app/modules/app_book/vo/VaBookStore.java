package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.math.BigDecimal;

//管理保存商家
@Data
public class VaBookStore
        extends VaBookStore1 {
    private String bstore_group_id;

    private BigDecimal bstore_app_rate;
    @HasAnyText
    private BigDecimal bstore_sales0_rate;
    @HasAnyText
    private BigDecimal bstore_sales1_rate;

    private Integer bstore_deposit_enable;
    private BigDecimal bstore_deposit_rate;
    @Size(max = 50)
    private String bstore_tag;
    private Long bstore_altitude;
    private Integer bstore_hot;
    private Integer bstore_foreign;
    Integer bstore_enable = 1; //生效状态
}
