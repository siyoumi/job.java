package com.siyoumi.app.modules.app_book.admin;

import com.siyoumi.app.entity.BookSet;
import com.siyoumi.app.modules.app_book.service.SvcBookSet;
import com.siyoumi.app.modules.app_book.vo.VaBookSet;
import com.siyoumi.app.modules.app_book.vo.VaBookSetDay;
import com.siyoumi.component.XApp;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__set_day__edit")
public class app_book__set_day__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("套餐-批量操作");

        Map<String, Object> data = new HashMap<>();
        data.put("bspud_set_id", input("set_id"));
        data.put("bsday_add_price_breakfast", BigDecimal.ZERO);
        data.put("bsday_add_price_lunch", BigDecimal.ZERO);
        data.put("bsday_add_price_dinner", BigDecimal.ZERO);
        data.put("week", List.of(1, 2, 3, 4, 5, 6, 7));
        if (isAdminEdit()) {
            //BookSpu entity = SvcBookSpu.getApp().loadEntity(getID());
            //
            //HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            //dataAppend.put("id", entity.getKey());
            ////
            ////合并
            //data = entity.toMap();
            //data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //星期
        setPageInfo("weeks", XApp.getWeeks());

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaBookSetDay vo, BindingResult result) {
        if (vo.getBegin_date().isBefore(XDate.today())) {
            result.addError(XValidator.getErr("end_date", "开始日期要大于或者等于当天"));
        }
        //统一验证
        XValidator.getResult(result);
        //
        InputData inputData = InputData.fromRequest();
        if (isAdminAdd()) {
            vo.setBsday_acc_id(getAccId());
        } else {
            vo.setBspud_set_id(null);
        }
        //
        return SvcBookSet.getBean().editBatchDay(vo);
    }
}