package com.siyoumi.app.modules.sheet.entity;

import com.siyoumi.util.IEnum;

//表单子项类型
public enum EnumSheetItemType
        implements IEnum {
    TEXT("text", "文本框"),
    TEXTAREA("textarea", "文本域"),
    CHECKBOX("checkbox", "复选框"),
    SELECT("select", "下拉框"),
    RADIO("radio", "单选框"),
    DATE("date", "日期"),
    ADDRESS("address", "地址"),
    UPLOAD_IMG("upload_img", "上传图片"),
    ;


    private String key;
    private String val;

    EnumSheetItemType(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
