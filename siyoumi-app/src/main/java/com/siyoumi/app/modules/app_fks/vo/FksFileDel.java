package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//文件删除
@Data
public class FksFileDel {
    @HasAnyText
    private List<String> file_ids; //文件ids
}
