package com.siyoumi.app.modules.act.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.act.service.SvcActSet;
import com.siyoumi.app.service.ActDataChouService;
import com.siyoumi.app.service.ActSetService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/act/act__data_chou__list")
public class act__data_chou__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("抽奖数据");

        ActDataChouService app = ActDataChouService.getBean();

        InputData inputData = InputData.fromRequest();
        String actId = inputData.input("act_id");
        String openid = inputData.input("openid");

        List<String> select = new ArrayList<>();
        select.add("wxuser_openid");
        select.add("wxuser_nickname");
        select.add("t_act_data_chou.*");
        select.add("pset_name");

        JoinWrapperPlus<ActDataChou> query = app.listQuery(actId);
        query.leftJoin(WxUser.table(), "wxuser_openid", "adc_openid");
        query.leftJoin(SysPrizeSet.table(), "adc_prize_id", "pset_id");
        query.select(select.toArray(new String[select.size()]));

        if (XStr.hasAnyText(openid)) { // 用户
            query.eq("adc_openid", openid);
        }

        IPage<ActDataChou> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), !isAdminExport());
        //list
        IPage<Map<String, Object>> pageData = app.getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            ActDataChou entityChou = app.loadEntity(data);
            data.put("id", entityChou.getKey());

            if (isAdminExport()) {
                // 是否中奖
                Integer isWin = 0;
                if (XStr.hasAnyText(entityChou.getAdc_win_uix())) {
                    isWin = 1;
                }
                data.put("is_win", isWin);
                if (isWin == 0) {
                    // 清空奖品名称
                    data.put("pset_name", "");
                }
            }

            return data;
        }).collect(Collectors.toList());

        if (!isAdminExport()) {
            getR().setData("list", listData);
            getR().setData("count", count);
        } else {
            //导出
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("adc_id", "id");
            tableHead.put("wxuser_openid", "openid");
            tableHead.put("wxuser_nickname", "微信名");
            tableHead.put("is_win", "是否中奖");
            tableHead.put("pset_name", "奖品");
            tableHead.put("adc_create_date", "抽奖时间");


            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }

        return getR();
    }


    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }
}
