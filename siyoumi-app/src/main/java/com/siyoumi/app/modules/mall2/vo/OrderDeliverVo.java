package com.siyoumi.app.modules.mall2.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

//发货接口
@Data
public class OrderDeliverVo {
    @NotBlank(message = "miss order_id")
    String order_id;
    @NotBlank(message = "请输入物流单号")
    @Size(max = 100)
    String deliver_no;
    @NotBlank(message = "请输入物流服务商")
    @Size(max = 100)
    String deliver_store;
}
