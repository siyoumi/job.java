package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.time.LocalDateTime;

//批量减库存
@Data
public class VaBookSpuDayUpdateStock {
    @HasAnyText
    private String spu_id;
    @HasAnyText
    private LocalDateTime begin_date;
    @HasAnyText
    private LocalDateTime end_date;

    @HasAnyText
    private Integer stock_add_use; //加使用量 = 减库存

    static public VaBookSpuDayUpdateStock of(String spu_id, LocalDateTime begin_date, LocalDateTime end_date, Integer stock_add_use) {
        VaBookSpuDayUpdateStock data = new VaBookSpuDayUpdateStock();
        data.setSpu_id(spu_id);
        data.setBegin_date(begin_date);
        data.setEnd_date(end_date);
        data.setStock_add_use(stock_add_use);

        return data;
    }
}
