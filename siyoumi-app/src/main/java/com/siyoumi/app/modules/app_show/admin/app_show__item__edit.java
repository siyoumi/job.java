package com.siyoumi.app.modules.app_show.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_show.vo.VaShowItem;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_show/app_show__item__edit")
public class app_show__item__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("项目-编辑");

        SysAbcService svcAbc = SysAbcService.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("abc_order", 0);
        if (isAdminEdit()) {
            SysAbc entity = svcAbc.getEntityByTable(getID(), "app_show_item");

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaShowItem vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAbcService svcAbc = SysAbcService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
        }

        InputData inputData = InputData.fromRequest();
        if (isAdminAdd()) {
            inputData.put("abc_table", "app_show_item");
            inputData.put("abc_app_id", XHttpContext.getAppId());
            inputData.put("abc_uix", XApp.getStrID());
        }

        inputData.putAll(XBean.toMap(vo));
        return svcAbc.saveEntity(inputData, true, ignoreField);
    }
}
