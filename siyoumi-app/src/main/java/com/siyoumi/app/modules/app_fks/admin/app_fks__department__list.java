package com.siyoumi.app.modules.app_fks.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FksDepartment;
import com.siyoumi.app.modules.app_fks.service.SvcFksDepartment;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__department__list")
public class app_fks__department__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("部门列表");

        InputData inputData = InputData.fromRequest();

        JoinWrapperPlus<FksDepartment> query = SvcFksDepartment.getBean().listQuery(inputData);

        IPage<FksDepartment> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcFksDepartment.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> item : list) {
            FksDepartment entity = XBean.fromMap(item, FksDepartment.class);
            item.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcFksDepartment.getBean().delete(getIds());
    }
}
