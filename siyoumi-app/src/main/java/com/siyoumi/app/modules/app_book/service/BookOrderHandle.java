package com.siyoumi.app.modules.app_book.service;

import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.app.modules.app_book.service.handle_order.BookOrderHandle10GiveFun;
import com.siyoumi.app.modules.app_book.service.handle_order.BookOrderHandle80PriceTotal;
import com.siyoumi.app.modules.app_book.service.handle_order.BookOrderHandle90SharePrice;
import com.siyoumi.app.modules.app_book.service.handle_order.BookOrderHandle20FunDown;
import com.siyoumi.app.modules.app_book.vo.VoBookOrder;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单处理
 */
public class BookOrderHandle
        extends BookOrderHandleAbs {
    static public BookOrderHandle of() {
        return new BookOrderHandle();
    }

    @Override
    public XReturn orderBeforeCheck() {
        return EnumSys.OK.getR();
    }

    protected List<BookOrderHandleAbs> getHandleList() {
        List<BookOrderHandleAbs> list = new ArrayList<>();
        list.add(new BookOrderHandle10GiveFun());
        list.add(new BookOrderHandle20FunDown());
        list.add(new BookOrderHandle80PriceTotal());
        list.add(new BookOrderHandle90SharePrice());
        return list;
    }

    @Override
    public String getTitle() {
        return "";
    }

    @Override
    public XReturn handle(BookOrder entityOrder) {
        LogPipeLine.getTl().setLogMsg("订单处理======BEGIN");
        for (BookOrderHandleAbs handle : getHandleList()) {
            handle.setVo(getVo());
            handle.setListDay(getListDay());

            LogPipeLine.getTl().setLogMsgFormat("{0}---BEGIN", handle.getTitle());
            XReturn r = handle.handle(entityOrder);
            XValidator.err(r);
            LogPipeLine.getTl().setLogMsgFormat("{0}---END", handle.getTitle());
        }
        LogPipeLine.getTl().setLogMsg("更新订单金额---BEGIN");
        LogPipeLine.getTl().setLogMsg("更新订单金额---END");

        LogPipeLine.getTl().setLogMsg("订单处理======END");
        return EnumSys.OK.getR();
    }
}
