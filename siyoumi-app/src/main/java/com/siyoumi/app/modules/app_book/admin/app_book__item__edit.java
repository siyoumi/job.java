package com.siyoumi.app.modules.app_book.admin;

import com.siyoumi.app.entity.BookItem;
import com.siyoumi.app.entity.BookStoreGroup;
import com.siyoumi.app.modules.app_book.service.SvcBookItem;
import com.siyoumi.app.modules.app_book.service.SvcBookStoreGroup;
import com.siyoumi.app.modules.app_book.vo.VaBookItem;
import com.siyoumi.app.modules.app_book.vo.VaBookStoreGroup;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__item__edit")
public class app_book__item__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("bitem_type", input("type", ""));
        data.put("bitem_order", 0);
        if (isAdminEdit()) {
            BookItem entity = SvcBookItem.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaBookItem vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        if (isAdminEdit()) {
            vo.setBitem_id(getID());
        }

        BookItem entity = SvcBookItem.getBean().save(vo);
        XReturn r = EnumSys.OK.getR();
        r.setData("entity", entity);
        return r;
    }
}