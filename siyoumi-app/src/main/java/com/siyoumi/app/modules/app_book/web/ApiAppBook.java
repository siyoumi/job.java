package com.siyoumi.app.modules.app_book.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.annotation.RequestLimit;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.app_book.entity.*;
import com.siyoumi.app.modules.app_book.service.*;
import com.siyoumi.app.modules.app_book.vo.*;
import com.siyoumi.app.service.*;
import com.siyoumi.app.sys.service.SvcSysUv;
import com.siyoumi.app.sys.service.payorder.PayOrder;
import com.siyoumi.app.sys.vo.VoUvSave;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/wxapp/app_book/api")
public class ApiAppBook
        extends WxAppApiController {
    /**
     * 初始化
     */
    @GetMapping("init")
    public XReturn init() {
        SysAbc setting = SvcBookSetting.getBean().getSetting();
        XValidator.isNull(setting, "未初始化");

        getR().put("banner", SysItemService.getBean().getItemDataByIdSrc(setting.getAbc_str_00())); //轮播图
        getR().put("menu", SysItemService.getBean().getItemDataByIdSrc(setting.getAbc_str_01())); //首页菜单
        getR().put("hot_city", SysItemService.getBean().getItemDataByIdSrc(setting.getAbc_str_02())); //热门城市
        getR().put("my", SysItemService.getBean().getItemDataByIdSrc(setting.getAbc_str_03())); //我的

        return getR();
    }

    /**
     * 项列表
     */
    @GetMapping("item_list")
    public XReturn itemList() {
        String type = input("type");
        XValidator.isNullOrEmpty(type, "miss type");
        InputData inputData = InputData.fromRequest();

        String[] select = {
                "bitem_id",
                "bitem_pid",
                "bitem_name",
                "bitem_pic",
                "bitem_str_00",
        };
        JoinWrapperPlus<BookItem> query = SvcBookItem.getBean().listQuery(inputData);
        query.select(select);
        List<Map<String, Object>> list = SvcBookItem.getApp().getMaps(query);
        getR().put("list", list);

        return getR();
    }


    /**
     * 基地列表
     */
    @GetMapping("store_list")
    public XReturn storeList() {
        InputData inputData = InputData.fromRequest();

        String[] select = {
                "bstore_id",
                "bstore_title",
                "bstore_banner",
                "bstore_tag",
                "bstore_province",
                "bstore_city",
                "bstore_district",
                "bstore_day1_price",
                "bset_name",
        };
        JoinWrapperPlus<BookStore> query = SvcBookStore.getBean().listQuery(inputData);
        query.leftJoin(BookSet.table(), BookSet.tableKey(), "bstore_set_id");
        query.eq("bstore_enable", 1);
        query.select(select);
        List<Map<String, Object>> list = SvcBookStore.getApp().getMaps(query);
        //
        List<String> storeIds = list.stream().map(item -> (String) item.get("bstore_id")).collect(Collectors.toList());
        //收藏数据
        List<String> collectIds = new ArrayList<>();
        if (isLogin()) {
            collectIds = SvcSysUv.getBean().getTypeIds(storeIds, "store_collect", getUid());
        }
        for (Map<String, Object> item : list) {
            BookStore entity = XBean.fromMap(item, BookStore.class);

            //收藏
            item.put("is_collect", collectIds.contains(entity.getKey()));
        }

        getR().put("list", list);

        return getR();
    }

    /**
     * 基地详情
     */
    @GetMapping("store_info")
    public XReturn storeInfo() {
        String storeId = input("store_id");
        XValidator.isNullOrEmpty(storeId, "miss store_id");

        BookStore entityStore = SvcBookStore.getApp().getEntity(storeId);
        XValidator.isNull(entityStore, "store_id error");

        BookStoreTxt entityStoreTxt = BookStoreTxtService.getBean().loadEntity(entityStore.getKey());
        VaBookStoreTxtInfo storeTxtInfo = XJson.parseObject(entityStoreTxt.getBstoret_info(), VaBookStoreTxtInfo.class);

        //基地
        Map<String, Object> mapStore = XBean.toMap(entityStore, new String[]{
                "bstore_id",
                "bstore_title",
                "bstore_banner",
                "bstore_tag",
                "bstore_province",
                "bstore_city",
                "bstore_district",
        });
        mapStore.put("txt_info", storeTxtInfo);
        Boolean isCollect = false; //收藏状态
        if (isLogin()) {
            isCollect = SvcSysUv.getBean().exists("store_collect", entityStore.getKey(), getUid());
        }
        mapStore.put("is_collect", isCollect);
        getR().setData("store", mapStore);

        //房源列表
        List<Map<String, Object>> listSpu = SvcBookSpu.getBean().apiSpuList(InputData.fromRequest());
        getR().put("list_spu", listSpu);
        //套餐
        BookSet entitySet = SvcBookSet.getBean().getFirst(storeId);
        Map<String, Object> mapSet = XBean.toMap(entitySet, new String[]{
                "bset_id",
                "bset_name",
                "bset_day_price",
        });
        getR().put("set", mapSet);

        return getR();
    }

    /**
     * 用户收藏
     * 基地，房间
     */
    @PostMapping("collect")
    public XReturn collect(@Validated VoUvSave vo, BindingResult result) {
        XValidator.isNullOrEmpty(vo.getType_id(), "miss type_id");
        List<String> types = List.of("store_collect", "spu_collect");
        if (!types.contains(vo.getType())) {
            result.addError(XValidator.getErr("type", "请输入指定值：" + types));
        }
        //
        XValidator.getResult(result, true, true);

        vo.setApp_id("app_book");
        vo.setUid(getUid());
        return SvcSysUv.getBean().save(vo);
    }

    /**
     * 房源列表
     */
    @GetMapping("spu_list")
    public XReturn spuList() {
        InputData inputData = InputData.fromRequest();

        List<Map<String, Object>> listSpu = SvcBookSpu.getBean().apiSpuList(inputData);

        getR().put("list", listSpu);

        return getR();
    }

    /**
     * 房间详情
     */
    @GetMapping("spu_info")
    public XReturn spuInfo() {
        String spuId = input("spu_id");
        XValidator.isNullOrEmpty(spuId, "miss spu_id");

        BookSpu entitySpu = SvcBookSpu.getApp().getEntity(spuId);
        XValidator.isNull(entitySpu, "spu_id error");
        //房源显示
        Map<String, Object> mapSpu = XBean.toMap(entitySpu);

        BookSpuTxt entitySpuTxt = BookSpuTxtService.getBean().getEntity(entitySpu.getKey());
        VaBookSpuTxt spuTxt = XJson.parseObject(entitySpuTxt.getBsput_txt_00(), VaBookSpuTxt.class);
        mapSpu.put("txt_info", spuTxt);
        //枚举
        EnumBookSpuFoodType enumFoodType = XEnumBase.of(EnumBookSpuFoodType.class); //做饭
        EnumBookSpuToilet enumToilet = XEnumBase.of(EnumBookSpuToilet.class); //厕所
        EnumBookSpuToiletType enumToiletType = XEnumBase.of(EnumBookSpuToiletType.class); //厕所类型
        EnumBookSpuViewType enumViewType = XEnumBase.of(EnumBookSpuViewType.class); //景色
        mapSpu.put("food_type", enumFoodType.get(entitySpu.getBspu_food_type()));
        mapSpu.put("toilet", enumToilet.get(entitySpu.getBspu_toilet()));
        mapSpu.put("toilet_type", enumToiletType.get(entitySpu.getBspu_toilet_type()));
        mapSpu.put("view_type", enumViewType.get(entitySpu.getBspu_view_type()));
        //
        getR().put("spu", mapSpu);
        //房间设施
        List<Map<String, Object>> listDevice = SvcBookItem.getBean().getList("device", null);
        getR().put("device", listDevice);
        //sku
        List<Map<String, Object>> listSku = SvcBookSku.getBean().getList(List.of(entitySpu.getKey()));
        getR().put("list_sku", listSku);

        //set list
        List<Map<String, Object>> listSet = SvcBookSet.getBean().getList(entitySpu.getBspu_store_id());
        getR().put("list_set", listSet);

        //商家
        BookStoreTxt entityStoreTxt = BookStoreTxtService.getBean().loadEntity(entitySpu.getBspu_store_id());
        VaBookStoreTxtInfo storeTxtInfo = XJson.parseObject(entityStoreTxt.getBstoret_info(), VaBookStoreTxtInfo.class);
        //
        BookStore entityStore = SvcBookStore.getApp().getEntity(entitySpu.getBspu_store_id());
        //
        Map<String, Object> mapStore = new HashMap<>();
        mapStore.put("txt_info", storeTxtInfo);
        mapStore.put("bstore_deposit_enable", entityStore.getBstore_deposit_enable());
        mapStore.put("bstore_deposit_rate", entityStore.getBstore_deposit_rate());
        getR().put("store", mapStore);

        return getR();
    }

    /**
     * 房间日期列表（加价&库存）
     */
    @GetMapping("spu_day_list")
    public XReturn spuDayList() {
        String spuId = input("spu_id");
        String setId = input("set_id");
        String dateBegin = input("date_begin");
        String dateEnd = input("date_end");
        XValidator.isNullOrEmpty(spuId, "miss spu_id");
        XValidator.isNullOrEmpty(setId, "miss set_id");
        XValidator.isNullOrEmpty(dateBegin, "miss date_begin");
        XValidator.isNullOrEmpty(dateEnd, "miss date_end");

        BookSpu entitySpu = SvcBookSpu.getApp().getEntity(spuId);
        XValidator.isNull(entitySpu, "spu_id error");

        LocalDateTime b = XDate.parse(dateBegin);
        LocalDateTime e = XDate.parse(dateEnd);
        //
        //List<LocalDateTime> dateList = XDate.getDateList(b, e, null);
        List<SpuDayData> listDay = SvcBookSpu.getBean().listDay(entitySpu, b, e);
        //套餐加价列表
        {
            JoinWrapperPlus<BookSetDay> query = SvcBookSet.getBean().listDayQuery(setId);
            query.between("bsday_date", dateBegin, dateEnd);
            query.select("bsday_id", "bsday_date", "bsday_add_price");
            List<Map<String, Object>> listSet = BookSetDayService.getBean().getMaps(query);
            getR().put("list_set", listSet);
        }
        //
        getR().put("list", listDay);

        return getR();
    }

    /**
     * 下单
     */
    @RequestLimit(key = RequestLimitAppBook.class, second = 2)
    @PostMapping("order")
    public XReturn order(@Validated VoBookOrder vo, BindingResult result) {
        //
        XValidator.getResult(result, true, true);
        //
        if (vo.getDate_begin().isBefore(XDate.today())) {
            result.addError(XValidator.getErr("date_begin", "开始日期要大于或者等于当天"));
        }
        XValidator.getResult(result, true, true);
        //
        getUid();
        //
        XReturn r = SvcBookOrder.getBean().order(vo);
        if (r.err()) {
            //LogPipeLine.getTl().saveDb("order", XHttpContext.getX());
        }
        return r;
    }

    /**
     * 订单详情
     */
    @GetMapping("order_info")
    public XReturn orderInfo() {
        String orderId = input("order_id");
        XValidator.isNullOrEmpty(orderId, "miss order_id");

        String[] select = {
                "border_id",
                "border_user_name",
                "border_user_phone",
                "border_date_begin",
                "border_date_end",
                "border_day",
                "border_sku_user_count",
                "border_set_user_count",
                "border_set_name",
                "border_set_day_price",
                "border_price_final",
                "border_pay_deposit",
                "border_price_pay",
                "border_desc",
                "border_status",
                "bspu_name",
                "bspu_logo",
                "bstore_title",
                "bstore_refund_rule",
                "order_id",
                "order_pay_ok",
                "order_pay_expire_date",
        };
        JoinWrapperPlus<BookOrder> query = SvcBookOrder.getBean().listQuery();
        query.join(BookSpu.table(), BookSpu.tableKey(), "border_spu_id");
        query.join(BookStore.table(), BookStore.tableKey(), "border_store_id");
        query.join(SysOrder.table(), SysOrder.tableKey(), "border_id");
        query.eq("border_id", orderId);
        query.select(select);
        Map<String, Object> mapOrder = SvcBookOrder.getApp().firstMap(query);
        XValidator.isNull(mapOrder, "订单号异常");

        BookOrder entityOrder = XBean.fromMap(mapOrder, BookOrder.class);
        mapOrder.put("border_user_phone", XStr.markStarPhone(entityOrder.getBorder_user_phone())); //手机号加星

        SysOrder entitySysOrder = XBean.fromMap(mapOrder, SysOrder.class);
        mapOrder.put("expire_seconds", entitySysOrder.expireSeconds()); //支付过期时间
        //退款手续费
        List<VoBookOrderRefundRule> refundRule = SvcBookStore.getBean().getRefundRule((String) mapOrder.get("bstore_refund_rule"));
        BigDecimal refundRate = SvcBookStore.getBean().getRefundRate(refundRule, entityOrder.getBorder_date_begin());
        mapOrder.put("refund_rate", refundRate);
        getR().put("order", mapOrder);


        return getR();
    }

    /**
     * 订单列表
     */
    @GetMapping("order_list")
    public XReturn orderList() {
        InputData inputData = InputData.fromRequest();

        String[] select = {
                "border_id",
                "border_date_begin",
                "border_date_end",
                "border_day",
                "border_sku_user_count",
                "border_sku_user_count",
                "border_set_name",
                "border_set_day_price",
                "border_price_final",
                "border_pay_deposit",
                "border_price_pay",
                "border_desc",
                "border_status",
                "bspu_name",
                "bspu_logo",
                "bstore_title",
        };
        JoinWrapperPlus<BookOrder> query = SvcBookOrder.getBean().listQuery();
        query.join(BookSpu.table(), BookSpu.tableKey(), "border_spu_id");
        query.join(BookStore.table(), BookStore.tableKey(), "border_store_id");
        query.eq("border_uid", getUid())
                .eq("border_enable", 1);
        query.select(select);

        IPage<BookOrder> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = SvcBookOrder.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();

        getR().put("list", list);

        return getR();
    }

    /**
     * 申请退款
     */
    @PostMapping("refund_apply")
    public XReturn refundApply(@Validated VoBookOrderRefundApply vo, BindingResult result) {
        //
        XValidator.getResult(result, true, true);

        return SvcBookOrder.getBean().refundApply(vo);
    }

    /**
     * 退款详情
     */
    @PostMapping("refund_info")
    public XReturn refund_info() {
        String refundId = input("refund_id");
        XValidator.isNullOrEmpty(refundId, "miss refund_id");

        BookRefundOrder entityRefund = BookRefundOrderService.getBean().getEntity(refundId);
        XValidator.isNull(entityRefund, "退款订单异常");

        Map<String, Object> mapRefund = XBean.toMap(entityRefund, new String[]{
                "brefo_id",
                "brefo_price",
                "brefo_audit_status",
                "brefo_audit_status_date",
                "brefo_audit_desc",
                "brefo_refund_id",
                "brefo_refund_price",
                "brefo_refund_handle",
                "brefo_refund_handle_date",
                "brefo_refund_errmsg",
        });

        getR().put("refund", mapRefund);

        return getR();
    }
}

