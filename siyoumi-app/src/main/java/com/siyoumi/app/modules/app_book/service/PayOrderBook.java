package com.siyoumi.app.modules.app_book.service;

import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.app.modules.app_book.vo.VaBookSpuDayUpdateStock;
import com.siyoumi.app.service.SysOrderService;
import com.siyoumi.app.sys.service.IPayOrder;
import com.siyoumi.app.sys.service.payorder.PayOrder;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Slf4j
public class PayOrderBook
        implements IPayOrder {
    /**
     * 支付操作
     */
    protected IPayOrder getPayApp() {
        return PayOrder.getBean(getEntity());
    }

    @Override
    public XReturn pay(String code) {
        return getPayApp().pay(code);
    }

    @Override
    public XReturn payChoose(String code) {
        return null;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn payCheck() {
        XReturn r = getPayApp().payCheck();
        if (r.ok()) {
            //支付成功，同步订单支付状态
            BookOrder entityBookOrder = SvcBookOrder.getApp().getEntity(getEntity().getKey());
            SvcBookOrder.getBean().updateStatus(entityBookOrder, 5);
        }
        return r;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn rollback() {
        if (getEntity().checkPayOk()) {
            return EnumSys.ORDER_PAY_OK.getR();
        }
        if (getEntity().getOrder_pay_expire_handle() == 1) {
            return EnumSys.ORDER_PAY_EXPIRE_HANDLE.getR();
        }

        //再检查一次接口
        XReturn r = payCheck();
        if (r.ok()) {
            return EnumSys.ORDER_PAY_OK.getR();
        }

        SysOrderService.getBean().getEntityLock(getEntity().getKey());
        SysOrderService.getBean().setRollbackDone(getEntity());

        log.debug("订单标记失效");
        BookOrder entityBookOrder = SvcBookOrder.getApp().getEntity(getEntity().getKey());
        SvcBookOrder.getBean().updateStatus(entityBookOrder, 3);

        log.debug("库存释放");
        VaBookSpuDayUpdateStock stockUpdate = VaBookSpuDayUpdateStock.of(entityBookOrder.getBorder_spu_id()
                , entityBookOrder.getBorder_date_begin()
                , entityBookOrder.getBorder_date_end_final()
                , -1);
        SvcBookSpu.getBean().dayBatchUpdateStock(stockUpdate);

        return XReturn.getR(0);
    }


    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn cancel() {
        XReturn r = getPayApp().cancel();
        if (r.ok()) {
            log.debug("标记取消订单");

            rollback();
        }
        return r;
    }

    @Override
    public XReturn refundApply(String refundId, BigDecimal refundPrice, String desc) {
        return null;
    }

    @Override
    public XReturn refund(String refundId) {
        return null;
    }
}
