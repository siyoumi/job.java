package com.siyoumi.app.modules.act.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorActConfig
        implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //活动过滤器
        registry.addInterceptor(new ActApiInterceptor())
                .addPathPatterns("/wxapp/act/**")
                .order(10);
        ;
    }
}
