package com.siyoumi.app.modules.app_ess.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorAppEssConfig
        implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //过滤器
        registry.addInterceptor(new AppEssApiInterceptor())
                .addPathPatterns("/wxapp/app_ess/**")
                .order(10);
        ;
        registry.addInterceptor(new AppEssApiTeacherInterceptor())
                .addPathPatterns("/wxapp/app_ess/api_teacher/**")
                .order(11);
        ;
    }
}
