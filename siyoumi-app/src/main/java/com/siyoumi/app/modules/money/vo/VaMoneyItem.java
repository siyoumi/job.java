package com.siyoumi.app.modules.money.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
public class VaMoneyItem {
    @NotBlank
    @Size(max = 50)
    String abc_name;
    @Size(max = 50)
    String abc_str_00;
    Long abc_int_00;
    BigDecimal abc_num_00;
    BigDecimal abc_num_01;
}
