package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

@Data
public class VoEssUserAddBatch {
    @HasAnyText
    private String euser_type;

    List<VaEssUser> upload_data;
}
