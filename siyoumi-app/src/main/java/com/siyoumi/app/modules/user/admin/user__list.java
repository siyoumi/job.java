package com.siyoumi.app.modules.user.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/user/user__list")
public class user__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("微信用户");

        String compKw = input("compKw");
        String compPhone = input("compPhone");

        WxUserService wxUserService = WxUserService.getBean();
        QueryWrapper<WxUser> query = wxUserService.q();
        query.eq(wxUserService.fdX(), XHttpContext.getX())
                .orderByAsc("wxuser_id");
        if (XStr.hasAnyText(compKw)) //昵称，名字，openid，手机号
        {
            query.and(q ->
            {
                q.like("wxuser_nickname", compKw).or()
                        .eq("wxuser_phone", compKw).or()
                        .like("wxuser_true_name", compKw).or()
                        .like("wxuser_desc", compKw).or()
                        .eq("wxuser_openid", compKw);
            });
        }
        if (XStr.hasAnyText(compPhone)) //手机号
        {
            query.eq("wxuser_phone", compPhone);
        }

        IPage<WxUser> page = new Page<>(getPageIndex(), getPageSize());

        IPage<WxUser> pageData = wxUserService.page(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<WxUser> listData = pageData.getRecords();
        List<Map<String, Object>> list = listData.stream().map(item ->
        {
            Map<String, Object> map = item.toMap();
            map.put("id", item.getKey());

            return map;
        }).collect(Collectors.toList());


        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }
}

