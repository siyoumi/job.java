package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//评测题目
@Data
public class VoEssTestQuestion {
    private String etques_module_id;

    @HasAnyText(message = "缺少评测ID")
    private String etques_test_id;
    @HasAnyText(message = "缺少题目ID")
    private String etques_question_id;
    @HasAnyText(message = "缺少得分")
    private Integer etques_fun;

    private Integer etques_order;
}
