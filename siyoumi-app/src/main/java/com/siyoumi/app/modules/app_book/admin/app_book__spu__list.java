package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.modules.app_book.entity.EnumBookSpuUserCount;
import com.siyoumi.app.modules.app_book.service.SvcBookSku;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.app.modules.app_book.vo.VoBookSkuAudit;
import com.siyoumi.app.modules.app_book.vo.VoBookSpuAudit;
import com.siyoumi.app.sys.entity.EnumEnable;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__spu__list")
public class app_book__spu__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("房源列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "bspu_id",
                "bspu_name",
                "bspu_logo",
                "bspu_user_count",
                "bspu_stock_max",
                "bspu_area",
                "bspu_enable",
                "bspu_enable_date",
                "bspu_order",
        };
        JoinWrapperPlus<BookSpu> query = SvcBookSpu.getBean().listQuery(inputData);
        query.eq("bspu_store_id", getStoreId());
        query.select(select);

        IPage<BookSpu> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcBookSpu.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumEnable enumEnable = XEnumBase.of(EnumEnable.class); //上下架
        EnumBookSpuUserCount enumUserCount = XEnumBase.of(EnumBookSpuUserCount.class); //房型
        for (Map<String, Object> item : list) {
            BookSpu entity = XBean.fromMap(item, BookSpu.class);
            item.put("id", entity.getKey());
            //状态
            item.put("enable", enumEnable.get(entity.getBspu_enable()));
            item.put("user_count", enumUserCount.get(entity.getBspu_user_count()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcBookStore.getBean().delete(getIds());
    }


    //上下架
    @Transactional
    @PostMapping("/audit")
    public XReturn audit(@Validated VoBookSpuAudit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        return SvcBookSpu.getBean().audit(vo);
    }
}
