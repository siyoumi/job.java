package com.siyoumi.app.modules.app_book.vo;

import lombok.Data;

//商家保存
@Data
public class VaBookStoreTxtInfo {
    String feature = ""; //特色
    String food = ""; //饮食
    String play = ""; //娱乐
    String traffic = ""; //交通
    String place = ""; //景点
    String health = ""; //医疗
    String shop = ""; //购物
    String other_price = "";  //其他收费政策
    String refund_desc = "";//退订政策
    String children_charge = ""; //儿童收费说明
    String reception = "";//可接待人群说明
}
