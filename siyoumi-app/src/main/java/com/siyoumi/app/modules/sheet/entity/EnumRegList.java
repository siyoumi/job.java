package com.siyoumi.app.modules.sheet.entity;

import com.siyoumi.util.IEnum;

//正则表达工
public enum EnumRegList
        implements IEnum {
    ID_CARD("身份证", "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x))$"),
    PHONE("手机号", "^1[3-9]{1}[0-9]{9}$");


    private String key;
    private String val;

    EnumRegList(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
