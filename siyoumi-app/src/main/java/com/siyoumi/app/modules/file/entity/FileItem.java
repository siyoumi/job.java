package com.siyoumi.app.modules.file.entity;

import lombok.Data;

@Data
public class FileItem {
    String id;
    String type;
    Boolean is_dir;
    String filename;
    Boolean do_edit = false;

    String file_url;
}
