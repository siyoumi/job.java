package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;

//厕所
public class EnumBookSpuToilet
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "独立");
        put(1, "公用");
    }
}
