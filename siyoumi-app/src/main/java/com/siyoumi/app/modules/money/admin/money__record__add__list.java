package com.siyoumi.app.modules.money.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysMoneyRecord;
import com.siyoumi.app.entity.SysOrder;
import com.siyoumi.app.entity.SysRefundOrder;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.modules.money.service.SvcMoneyRecord;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/money/money__record__add__list")
public class money__record__add__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("钱包-充值记录");

        String appId = XHttpContext.getAppId();

        InputData inputData = InputData.fromRequest();
        String compKw = inputData.input("compKw");

        SvcMoneyRecord svcMoneyRecord = SvcMoneyRecord.getBean();
        JoinWrapperPlus<SysMoneyRecord> query = svcMoneyRecord.getQuery(inputData);
        query.join(WxUser.table(), "wxuser_openid", "mrec_openid");
        query.join(SysOrder.table(), "mrec_uix", "order_order_id");
        query.leftJoin(SysRefundOrder.table(), "refo_refund_id", "order_order_id");
        query.eq("mrec_app_id", "money")
                .orderByDesc("mrec_id");

        List<String> select = new ArrayList<>();
        select.add("mrec_id");
        select.add("mrec_uix");
        select.add("wxuser_openid");
        select.add("wxuser_nickname");
        select.add("wxuser_headimgurl");
        select.add("mrec_app_id");
        select.add("mrec_num");
        select.add("mrec_desc");
        select.add("mrec_create_date");
        select.add("order_refund");
        select.add("refo_refund_errcode");
        select.add("refo_refund_errmsg");
        select.add("refo_refund_date");

        query.select(select.toArray(new String[select.size()]));
        query.orderByDesc("mrec_id");

        if (XStr.hasAnyText(compKw)) { // 昵称
            query.likeRight("wxuser_nickname", compKw);
        }

        IPage<SysMoneyRecord> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), !isAdminExport());
        //list
        IPage<Map<String, Object>> pageData = SvcMoneyRecord.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            SysMoneyRecord entity = SvcMoneyRecord.getApp().loadEntity(data);
            data.put("id", entity.getKey());
            data.put("can_refund", entity.canRefund());

            return data;
        }).collect(Collectors.toList());

        if (!isAdminExport()) {
            getR().setData("list", listData);
            getR().setData("count", count);
        } else {
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("mrec_id", "ID");
            tableHead.put("mrec_app_id", "应用");
            tableHead.put("wxuser_openid", "openid");
            tableHead.put("wxuser_nickname", "微信名");
            tableHead.put("mrec_num", "数值");
            tableHead.put("mrec_desc", "描述");
            tableHead.put("mrec_create_date", "时间");
            tableHead.put("__order_refund", "状态");
            tableHead.put("refo_refund_errcode", "errcode");
            tableHead.put("refo_refund_errmsg", "errmsg");
            tableHead.put("refo_refund_date", "退款时间");

            for (Map<String, Object> data : list) {
                Integer orderRefund = (Integer) data.get("order_refund");

                data.put("__order_refund", orderRefund > 0 ? "已申请退款" : "");
            }

            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }

        return getR();
    }

    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }


}
