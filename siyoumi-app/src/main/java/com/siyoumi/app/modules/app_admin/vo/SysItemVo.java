package com.siyoumi.app.modules.app_admin.vo;

import lombok.Data;

//通用项
@Data
public class SysItemVo {
    private String type; //类型
    private String label; //显示标题
    private String field_key; //保存字段名
    private String def = ""; //默认值
    private Integer required = 0;
    private String type_item = ""; //项，多个逗号隔开
}
