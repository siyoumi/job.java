package com.siyoumi.app.modules.wechat.entity;

import lombok.Data;

//用户
@Data
public class WechatRoomUser {
    String id;
    String name;
    Integer count = 0;
}
