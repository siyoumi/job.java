package com.siyoumi.app.modules.act.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.act.service.SvcActSet;
import com.siyoumi.app.service.ActDataChouService;
import com.siyoumi.app.service.ActDataService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/act/act__data__list")
public class act__data__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        ActDataService app = ActDataService.getBean();

        InputData inputData = InputData.fromRequest();
        String actId = inputData.input("act_id");
        String openid = inputData.input("openid");

        ActSet entityAct = SvcActSet.getBean().getEntity(actId);
        setPageTitle(entityAct.getAset_name(), "-参与数据");

        List<String> select = new ArrayList<>();
        select.add("t_act_data.*");
        select.add("wxuser_openid");
        select.add("wxuser_nickname");

        JoinWrapperPlus<ActData> query = app.listQuery(actId, null);
        query.join(WxUser.table(), "wxuser_openid", "adata_openid");
        query.select(select.toArray(new String[select.size()]));
        if (XStr.hasAnyText(openid)) { // 用户
            query.eq("adata_openid", openid);
        }

        IPage<ActData> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), !isAdminExport());
        //list
        IPage<Map<String, Object>> pageData = app.getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            ActData entityChou = app.loadEntity(data);
            data.put("id", entityChou.getKey());

            if (isAdminExport()) {
                data.put("str_00", "`" + entityChou.getAdata_str_00());
            }

            return data;
        }).collect(Collectors.toList());

        if (!isAdminExport()) {
            getR().setData("list", listData);
            getR().setData("count", count);
        } else {
            //导出
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("adata_id", "id");
            tableHead.put("wxuser_openid", "openid");
            tableHead.put("wxuser_nickname", "微信名");
            tableHead.put("str_00", "文字1");
            tableHead.put("adata_str_01", "文字2");
            tableHead.put("adata_create_date", "创建时间");


            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }

        setPageFrom("is_test", entityAct.test());

        return getR();
    }


    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }
}
