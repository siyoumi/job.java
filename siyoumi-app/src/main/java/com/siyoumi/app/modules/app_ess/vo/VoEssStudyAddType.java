package com.siyoumi.app.modules.app_ess.vo;

import lombok.Data;


//添加学习任务-类型
@Data
public class VoEssStudyAddType {
    String type;
    Long fun;
}
