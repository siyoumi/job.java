package com.siyoumi.app.modules.user.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;

//分销员
@Data
@Accessors(chain = true)
public class VoSale {
    private String sale_id;
    private String sale_acc_id;
    private String sale_type = "";
    @HasAnyText(message = "缺少姓名")
    private String sale_name;
    private Integer sale_level;
    private String sale_uid;
}
