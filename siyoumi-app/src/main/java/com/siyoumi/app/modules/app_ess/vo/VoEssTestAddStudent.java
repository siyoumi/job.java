package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;


//评测添加学生
@Data
public class VoEssTestAddStudent {
    @HasAnyText(message = "缺少评测ID")
    String test_id;

    List<String> student_uids;
}
