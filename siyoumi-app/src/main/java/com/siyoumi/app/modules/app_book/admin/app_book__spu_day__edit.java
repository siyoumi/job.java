package com.siyoumi.app.modules.app_book.admin;

import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.app.modules.app_book.vo.VaBookSpuDay;
import com.siyoumi.component.XApp;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__spu_day__edit")
public class app_book__spu_day__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("批量操作");

        Map<String, Object> data = new HashMap<>();
        data.put("bspud_spu_id", input("spu_id"));
        data.put("bspud_stock_add", 0);
        data.put("bspud_spu_add_price", BigDecimal.ZERO);
        data.put("week", List.of(1, 2, 3, 4, 5, 6, 7));
        if (isAdminEdit()) {
            //BookSpu entity = SvcBookSpu.getApp().loadEntity(getID());
            //
            //HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            //dataAppend.put("id", entity.getKey());
            ////
            ////合并
            //data = entity.toMap();
            //data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //星期
        setPageInfo("weeks", XApp.getWeeks());

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaBookSpuDay vo, BindingResult result) {
        if (vo.getBegin_date().isBefore(XDate.today())) {
            result.addError(XValidator.getErr("end_date", "开始日期要大于或者等于当天"));
        }
        //统一验证
        XValidator.getResult(result);
        //
        BookSpu entitySpu = SvcBookSpu.getApp().getEntity(vo.getBspud_spu_id());
        XValidator.err(SvcBookSpu.getBean().canEdit(entitySpu));
        //
        return SvcBookSpu.getBean().dayEditBatch(vo);
    }
}