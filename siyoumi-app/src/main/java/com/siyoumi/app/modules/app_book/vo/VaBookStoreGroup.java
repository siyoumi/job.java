package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//商家分组
@Data
public class VaBookStoreGroup {
    private String bsgroup_acc_id;
    @Size(max = 50)
    @HasAnyText
    private String bsgroup_name;
    @Size(max = 200)
    private String bsgroup_pic;
    @HasAnyText
    private Integer bsgroup_order;
}
