package com.siyoumi.app.modules.app_menu.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/app_menu/app_menu__type__list")
public class app_menu__type__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("食用方式列表");

        SysAbcService app = SysAbcService.getBean();

        InputData inputData = InputData.fromRequest();
        String compKw = inputData.input("compKw");

        QueryWrapper<SysAbc> query = app.listQuery(XHttpContext.getAppId(), false);
        query.eq("abc_table", "app_menu_type");
        if (XStr.hasAnyText(compKw)) { //名称
            query.like("abc_name", compKw);
        }

        IPage<SysAbc> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<SysAbc> pageData = app.get(page, query);
        List<SysAbc> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(entity ->
        {
            HashMap<String, Object> data = new HashMap<>(entity.toMap());
            data.put("id", entity.getKey());

            return data;
        }).collect(Collectors.toList());

        getR().setData("list", listData);
        getR().setData("count", count);
        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        if (ids.length <= 0) {
            return EnumSys.ERR_VAL.getR("至少选择1项");
        }

        SysAbcService app = SysAbcService.getBean();
        QueryWrapper<SysAbc> query = app.listQuery(XHttpContext.getAppId(), false)
                .eq("abc_table", "app_menu_type")
                .in("abc_id", Arrays.asList(ids));
        boolean delCount = app.remove(query);

        XLog.info(this.getClass(), "同时删除烹饪方式");
        QueryWrapper<SysAbc> query2 = app.listQuery(XHttpContext.getAppId(), false)
                .eq("abc_table", "app_menu_type2")
                .in("abc_pid", Arrays.asList(ids));
        app.remove(query2);

        return XReturn.getR(0);
    }
}
