package com.siyoumi.app.modules.website_fjciecc.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.website_fjciecc.entity.EnumFjcieccTrends;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/website_fjciecc/website_fjciecc__social__list")
public class website_fjciecc__social__list
        extends website_fjciecc__performance__list {
    @Override
    public String title() {
        return "社会责任";
    }

    @Override
    public String abcTable() {
        return "fjciecc_social";
    }

    //for处理
    @Override
    public void forHandle(List<Map<String, Object>> list) {
        SysAbcService svcAbc = SysAbcService.getBean();

        for (Map<String, Object> data : list) {
            SysAbc entityAbc = svcAbc.loadEntity(data);
            data.put("id", entityAbc.getKey());
            data.put("send_date", XDate.toDateString(entityAbc.getAbc_date_00()));
        }
    }


    @GetMapping()
    public XReturn index() {
        return super.index();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return super.del(ids);
    }
}
