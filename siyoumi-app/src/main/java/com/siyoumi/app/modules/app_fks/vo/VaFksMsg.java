package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

//号码组
@Data
public class VaFksMsg {
    @HasAnyText
    private String fmsg_type;
    private String fmsg_acc_id;
    @HasAnyText
    @Size(max = 200)
    private String fmsg_title;
    private String fmsg_text;
    @Size(max = 200)
    private String fmsg_pic;
    @Size(max = 2000)
    private String fmsg_keyword;
    private LocalDateTime fmsg_release_date;
    @Size(max = 50)
    private String fmsg_src;
    @Size(max = 200)
    private String fmsg_tiao_title;
    @Size(max = 500)
    private String fmsg_tiao_desc;
    @Size(max = 2000)
    private String fmsg_file_json;
    private Integer fmsg_order;
}
