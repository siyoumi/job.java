package com.siyoumi.app.modules.fun_mall.admin;

import com.siyoumi.app.entity.FunMallGroup;
import com.siyoumi.app.entity.FunMallPrize;
import com.siyoumi.app.entity.M2Group;
import com.siyoumi.app.modules.fun_mall.service.SvcFunMallGroup;
import com.siyoumi.app.modules.fun_mall.service.SvcFunMallPrize;
import com.siyoumi.app.modules.fun_mall.vo.VaFunMallPrize;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSetAdmin;
import com.siyoumi.app.service.M2GroupService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/fun_mall/fun_mall__prize__edit")
public class fun_mall__prize__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商品-编辑");

        InputData inputData = InputData.fromRequest();

        Map<String, Object> data = new LinkedHashMap<>();
        data.put("fprize_fun", 1);
        data.put("fprize_get_total", 0);
        data.put("fprize_order", 0);
        data.put("fprize_enable", 0);
        data.put("set_prize", "");
        if (isAdminEdit()) {
            FunMallPrize entity = SvcFunMallPrize.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //奖品配置
        SvcSysPrizeSetAdmin.getIns(true).editAfter(inputData, getR(), getID());

        //分组
        JoinWrapperPlus<FunMallGroup> queryGroup = SvcFunMallGroup.getBean().listQuery();
        queryGroup.select("fgroup_id", "fgroup_name");
        List<Map<String, Object>> listGroup = SvcFunMallGroup.getApp().getMaps(queryGroup);
        setPageInfo("list_group", listGroup);

        return getR();
    }


    @PostMapping("/save")
    public XReturn save(@Validated() VaFunMallPrize vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SvcSysPrizeSetAdmin.getIns(true).editSaveValidator(result, vo);
        XValidator.getResult(result);

        if (isAdminAdd()) {
            vo.setFprize_acc_id(getAccId());
        }

        InputData inputData = InputData.fromRequest();
        return SvcFunMallPrize.getBean().edit(inputData, vo);
    }
}
