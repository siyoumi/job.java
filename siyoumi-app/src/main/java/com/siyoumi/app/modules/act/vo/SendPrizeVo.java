package com.siyoumi.app.modules.act.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

//发奖
@Data
public class SendPrizeVo {
    @HasAnyText
    @Size(max = 50)
    private String pset_id;

    @HasAnyText
    private String openid;
}
