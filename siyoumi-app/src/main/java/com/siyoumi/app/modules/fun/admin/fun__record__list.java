package com.siyoumi.app.modules.fun.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FunRecord;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.modules.fun.service.SvcFun;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.entity.SysApp;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/fun/fun__record__list")
public class fun__record__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        SysAccsuperConfig entityConfig = XHttpContext.getXConfig();

        setPageTitle(entityConfig.getAconfig_fun_name(), "-记录");

        String appId = XHttpContext.getAppId();

        InputData inputData = InputData.fromRequest();
        String compKw = inputData.input("compKw");

        SvcFun svcFun = SvcFun.getBean();
        JoinWrapperPlus<FunRecord> query = svcFun.getQuery(inputData);
        query.join(SysApp.table(), "app_id", "funrec_app_id");
        query.join(SysUser.table(), SysUser.tableKey(), "funrec_uid");

        String[] select = {
                "t_fun_record.*",
                "app_name",
                "user_id",
                "user_name",
                "user_headimg",
        };
        query.select(select);
        query.orderByDesc("funrec_id");

        if (XStr.hasAnyText(compKw)) { // 昵称
            query.likeRight("user_name", compKw);
        }

        IPage<FunRecord> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), !isAdminExport());
        //list
        IPage<Map<String, Object>> pageData = SvcFun.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> item : list) {
            FunRecord entity = svcFun.getApp().loadEntity(item);
            item.put("id", entity.getKey());
        }
        if (!isAdminExport()) {
            getR().setData("list", list);
            getR().setData("count", count);
        } else {
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("funrec_id", "ID");
            tableHead.put("funrec_app_id", "应用");
            tableHead.put("user_name", "名称");
            tableHead.put("user_id", "uid");
            tableHead.put("funrec_num", "数值");
            tableHead.put("funrec_desc", "描述");
            tableHead.put("funrec_create_date", "时间");

            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }

        return getR();
    }

    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcFun.getBean().delete(Arrays.asList(ids));
    }
}
