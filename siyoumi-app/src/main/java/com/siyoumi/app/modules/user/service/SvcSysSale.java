package com.siyoumi.app.modules.user.service;

import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.user.vo.*;
import com.siyoumi.app.service.SysSaleClientService;
import com.siyoumi.app.service.SysSaleRecordService;
import com.siyoumi.app.service.SysSaleService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

//分销员
@Slf4j
@Service
public class SvcSysSale
        implements IWebService {
    static public SvcSysSale getBean() {
        return XSpringContext.getBean(SvcSysSale.class);
    }

    static public SysSaleService getApp() {
        return SysSaleService.getBean();
    }

    /**
     * 生效
     *
     * @param entity
     * @return
     */
    public XReturn valid(SysSale entity) {
        if (entity == null) {
            return EnumSys.ERR_VAL.getR("分销员ID异常");
        }
        if (entity.getSale_state() != 1) {
            return EnumSys.ERR_VAL.getR("分销员未生效");
        }

        return EnumSys.OK.getR();
    }

    /**
     * 根据uid获取分销员
     */
    public SysSale getEntitySaleByUid(String uid) {
        JoinWrapperPlus<SysSale> query = listQuery();
        query.eq("sale_uid", uid);
        return getApp().first(query);
    }

    /**
     * 获取客户uid获取分销员
     *
     * @param clientUid
     */
    public SysSale getEntitySale(String clientUid) {
        JoinWrapperPlus<SysSale> query = listQuery();
        query.join(SysSaleClient.table(), "sale_uid", "sacl_sale_uid");
        query.eq("sacl_client_uid", clientUid);
        return getApp().first(query);
    }

    public JoinWrapperPlus<SysSale> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<SysSale> listQuery(InputData inputData) {
        String uid = inputData.input("uid");
        String state = inputData.input("state");
        String level = inputData.input("level");

        JoinWrapperPlus<SysSale> query = getApp().join();
        query.eq("sale_x_id", XHttpContext.getX());

        if (XStr.hasAnyText(uid)) { //uid
            query.eq("sale_uid", uid);
        }
        if (XStr.hasAnyText(level)) { //等级
            query.eq("sale_level", level);
        }
        if (XStr.hasAnyText(state)) { //审核状态
            query.eq("sale_state", state);
        }

        return query;
    }

    /**
     * 获取分销关系
     *
     * @param clientUid
     */
    public SysSaleClient getEntityClient(String clientUid) {
        return SysSaleClientService.getBean().first(listClientQuery(clientUid));
    }

    public JoinWrapperPlus<SysSaleClient> listClientQuery(String clientUid) {
        JoinWrapperPlus<SysSaleClient> query = listClientQuery(InputData.getIns());
        query.eq("sacl_client_uid", clientUid);

        return query;
    }

    /**
     * 绑定关系
     *
     * @return query
     */
    public JoinWrapperPlus<SysSaleClient> listClientQuery(InputData inputData) {
        String saleUid = inputData.input("sale_uid");

        JoinWrapperPlus<SysSaleClient> query = SysSaleClientService.getBean().join();
        query.eq("sacl_x_id", XHttpContext.getX());

        if (XStr.hasAnyText(saleUid)) { //分销员
            query.eq("sacl_sale_uid", saleUid);
        }

        return query;
    }

    public JoinWrapperPlus<SysSaleRecord> listRecordQuery() {
        JoinWrapperPlus<SysSaleRecord> query = listRecordQuery(InputData.getIns());
        return query;
    }

    /**
     * 入帐明细
     *
     * @return query
     */
    public JoinWrapperPlus<SysSaleRecord> listRecordQuery(InputData inputData) {
        String uid = inputData.input("uid");
        String dateBegin = inputData.input("date_begin");
        String dateEnd = inputData.input("date_end");
        String abType = inputData.input("ab_type");

        JoinWrapperPlus<SysSaleRecord> query = SysSaleRecordService.getBean().join();
        query.eq("srec_x_id", XHttpContext.getX());

        if (XStr.hasAnyText(uid)) { //分销员
            query.eq("srec_uid", uid);
        }
        if (XStr.hasAnyText(abType)) { //加减
            query.eq("srec_ab_type", abType);
        } else {
            query.in("srec_ab_type", "+", "-");
        }

        if (XStr.hasAnyText(dateBegin) && XStr.hasAnyText(dateEnd)) { //创建时间
            LocalDateTime b = XDate.parse(dateBegin);
            LocalDateTime e = XDate.parse(dateEnd);
            query.between("srec_create_date", b, e);
        }

        return query;
    }

    /**
     * 申请分销员
     *
     * @param vo
     */
    public XReturn save(VoSale vo) {
        InputData inputData = InputData.getIns();

        SysSale entity = getApp().getEntity(vo.getSale_id());
        if (entity != null) {
            inputData.put("id", vo.getSale_id());
        }

        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminEdit()) {
            ignoreField.add("srec_uid");
            ignoreField.add("sale_state");
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, false, ignoreField);
            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn deleteClient(List<String> ids) {
        XReturn r = XReturn.getR(0);

        SysSaleClientService.getBean().delete(ids);

        return r;
    }

    /**
     * 审核
     *
     * @param vo
     */
    @Transactional(rollbackFor = Exception.class)
    public XReturn audit(VoSaleAudit vo) {
        List<SysSale> list = getApp().get(vo.getIds());
        if (list.isEmpty()) {
            return EnumSys.OK.getR();
        }

        for (SysSale entity : list) {
            //if (entity.getSale_state() != 0) {
            //    continue;
            //}

            SysSale entityUpdate = new SysSale();
            entityUpdate.setSale_id(entity.getKey());
            entityUpdate.setSale_state(vo.getEnable());
            entityUpdate.setSale_state_date(XDate.now());
            getApp().updateById(entityUpdate);
        }

        return EnumSys.OK.getR();
    }


    /**
     * 绑定客户
     *
     * @param vo
     */
    public XReturn bindClient(VoSaleBindClient vo) {
        SysSale entitySale = getApp().getEntity(vo.getSale_uid());
        XValidator.isNull(entitySale, "分销员uid异常");
        XValidator.err(valid(entitySale));

        if (!"".equals(entitySale.getSale_type())) {
            return XReturn.getR(20117, "类型异常");
        }

        if (vo.getSale_uid().equals(vo.getClient_uid())) {
            return XReturn.getR(20107, "自己不能绑定自己");
        }

        SysSaleClient entityClient = getEntityClient(vo.getClient_uid());
        if (entityClient != null) {
            if (!entityClient.getSacl_sale_uid().equals(vo.getSale_uid())) {
                return XReturn.getR(20117, "已绑定其他人");
            } else { //已绑定
                return EnumSys.OK.getR();
            }
        }

        SysSaleClient entityNew = new SysSaleClient();
        entityNew.setSacl_x_id(XHttpContext.getX());
        entityNew.setSacl_sale_uid(vo.getSale_uid());
        entityNew.setSacl_client_uid(vo.getClient_uid());
        entityNew.setAutoID();
        SysSaleClientService.getBean().save(entityNew);

        return EnumSys.OK.getR();
    }

    /**
     * 入账出帐
     */
    public SysSaleRecord addRecord(VoSaleAddRecord vo) {
        String abType = vo.getSrec_num().compareTo(BigDecimal.ZERO) > 0 ? "+" : "-";

        SysSaleRecord entity = new SysSaleRecord();
        XBean.copyProperties(vo, entity);
        entity.setSrec_desc(XStr.maxLen(vo.getSrec_desc(), 100));
        entity.setSrec_ab_type(abType);
        entity.setSrec_x_id(XHttpContext.getX());
        entity.setAutoID();

        XApp.getTransaction().execute(status -> {
            boolean save = SysSaleRecordService.getBean().save(entity);
            if (vo.getUpdateLeft()) { //更新余额
                updateMoney(vo.getSrec_uid());
            }

            return save;
        });

        return entity;
    }

    /**
     * 提现
     *
     * @return
     */
    public XReturn getMoney(VoSaleGetMoney vo) {
        return EnumSys.OK.getR();
    }

    /**
     * 获取余额
     *
     * @param uid
     */
    public BigDecimal getMoneyLeft(String uid) {
        SysSale entity = getApp().getEntity(uid);
        if (entity == null) {
            return BigDecimal.ZERO;
        }

        return entity.getSale_money_left();
    }

    /**
     * 更新用户余额，总收入
     *
     * @param uid
     */
    public BigDecimal updateMoney(String uid) {
        JoinWrapperPlus<SysSaleRecord> query = listRecordQuery();
        query.eq("srec_uid", uid);
        //if (updateTotal) {
        //    query.gt("srec_ab_type", "+");
        //}
        BigDecimal money = SysSaleRecordService.getBean().sum(query, "srec_num");

        SysSale entity = getApp().getEntity(uid);
        if (entity == null) {
            XValidator.err(EnumSys.ERR_VAL.getR("not find sale"));
        }
        SysSale update = new SysSale();
        update.setSale_id(entity.getKey());
        update.setSale_money_left(money);
        getApp().updateById(update);

        return money;
    }
}
