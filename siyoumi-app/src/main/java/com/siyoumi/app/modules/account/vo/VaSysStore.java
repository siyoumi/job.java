package com.siyoumi.app.modules.account.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class VaSysStore {
    private String store_id;
    @HasAnyText
    private String store_acc_id;
    @HasAnyText
    @Size(max = 50)
    private String store_name;

    private Integer store_order;

    static public VaSysStore of(String id, String accId, String name) {
        return new VaSysStore()
                .setStore_id(id)
                .setStore_acc_id(accId)
                .setStore_name(name);
    }
}
