package com.siyoumi.app.modules.app_ess.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;


//添加学习任务
@Data
public class VoEssStudyAdd {
    String estudy_uid;
    @HasAnyText(message = "缺少场景ID")
    String estudy_module_id;
    @HasAnyText
    @Size(max = 50, message = "名称长度不能超50")
    String estudy_name;
    @HasAnyText(message = "缺少开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    LocalDateTime estudy_date_begin;
    @HasAnyText(message = "缺少结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    LocalDateTime estudy_date_end;

    @HasAnyText
    List<String> class_ids; //班级ID
    @HasAnyText
    List<VoEssStudyAddType> types; //选的类型
}
