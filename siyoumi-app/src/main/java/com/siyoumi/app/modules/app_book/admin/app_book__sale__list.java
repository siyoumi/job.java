package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysSale;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_book.entity.EnumBookSaleLevel;
import com.siyoumi.app.modules.user.entity.EnumSaleState;
import com.siyoumi.app.modules.user.service.SvcSysSale;
import com.siyoumi.app.modules.user.vo.VoSaleAudit;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__sale__list")
public class app_book__sale__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("主播列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "sale_id",
                "sale_level",
                "sale_money_total",
                "sale_money_left",
                "sale_name",
                "sale_uid",
                "sale_state",
                "sale_state_date",
        };
        JoinWrapperPlus<SysSale> query = SvcSysSale.getBean().listQuery(inputData);
        query.select(select);
        query.eq("sale_type", "");

        IPage<SysSale> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcSysSale.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumBookSaleLevel enumSaleLevel = XEnumBase.of(EnumBookSaleLevel.class);
        EnumSaleState enumSaleState = XEnumBase.of(EnumSaleState.class);
        for (Map<String, Object> item : list) {
            SysSale entity = XBean.fromMap(item, SysSale.class);

            item.put("id", entity.getKey());
            item.put("level", enumSaleLevel.get(entity.getSale_level())); //等级
            item.put("state", enumSaleState.get(entity.getSale_state())); //状态
        }

        getR().setData("list", list);
        getR().setData("count", count);

        setPageInfo("enum_sale_level", enumSaleLevel);
        setPageInfo("enum_sale_state", enumSaleState);


        return getR();
    }

    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcSysSale.getBean().delete(getIds());
    }

    @PostMapping("/audit")
    public XReturn audit(@Validated() VoSaleAudit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        return SvcSysSale.getBean().audit(vo);
    }
}
