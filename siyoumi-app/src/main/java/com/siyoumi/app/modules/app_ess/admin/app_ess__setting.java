package com.siyoumi.app.modules.app_ess.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_ess.vo.VaEssSetting;
import com.siyoumi.app.modules.app_ess.vo.VaEssSettingTxt00;
import com.siyoumi.app.modules.app_show.vo.VaShowSetting;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__setting")
public class app_ess__setting
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("基础配置");

        SysAbcService svcAbc = SysAbcService.getBean();
        String uix = "app_ess_setting";
        SysAbc entity = svcAbc.getEntityByUix(uix);
        if (entity == null) {
            entity = new SysAbc();
            entity.setAbc_app_id(XHttpContext.getAppId());
            entity.setAbc_x_id(XHttpContext.getX());
            entity.setAbc_table("app_ess_setting");
            entity.setAbc_uix(uix);
            entity.setAutoID();

            svcAbc.save(entity);
            entity = svcAbc.getEntityByUix(uix);
        }

        Map<String, Object> data = new HashMap<>();

        HashMap<String, Object> dataAppend = new LinkedHashMap<>();
        dataAppend.put("id", entity.getKey());
        //合并
        data = entity.toMap();
        data.putAll(dataAppend);
        VaEssSettingTxt00 txt00 = new VaEssSettingTxt00();
        if (XStr.hasAnyText(entity.getAbc_txt_00())) {
            txt00 = XJson.parseObject(entity.getAbc_txt_00(), VaEssSettingTxt00.class);
        }
        data.putAll(XBean.toMap(txt00));

        getR().setData("data", data);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaEssSettingTxt00 vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
        }

        VaEssSetting setting = new VaEssSetting();
        setting.setAbc_txt_00(XJson.toJSONString(vo));
        InputData inputData = InputData.fromRequest();
        XReturn r = SysAbcService.getBean().saveEntity(inputData, setting, true, ignoreField);
        if (r.ok()) {
            SysAbc entity = r.getData("entity");
            SysAbcService.getBean().delEntityCache(entity.getKey());
        }

        return r;
    }
}
