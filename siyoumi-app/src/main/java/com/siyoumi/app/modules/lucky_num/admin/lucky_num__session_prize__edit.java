package com.siyoumi.app.modules.lucky_num.admin;

import com.siyoumi.app.entity.LnumSession;
import com.siyoumi.app.entity.LnumSessionPrize;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumSession;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumSessionPrize;
import com.siyoumi.app.modules.lucky_num.vo.VaLnumSessionPrize;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSetAdmin;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/lucky_num/lucky_num__session_prize__edit")
public class lucky_num__session_prize__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("奖品期-奖品编辑");

        InputData inputData = InputData.fromRequest();
        String sessionId = input("session_id");

        Map<String, Object> data = new HashMap<>();
        data.put("lnsp_count", 1);
        data.put("lnsp_order", 0);
        if (isAdminEdit()) {
            LnumSessionPrize entity = SvcLNumSessionPrize.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);

            sessionId = entity.getLnsp_session_id();
        }
        data.put("lnsp_session_id", sessionId);

        getR().setData("data", data);

        //奖品配置
        SvcSysPrizeSetAdmin.getIns().editAfter(inputData, getR(), getID());

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaLnumSessionPrize vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        LnumSession entitySession = SvcLNumSession.getApp().first(vo.getLnsp_session_id());
        if (entitySession == null) {
            result.addError(XValidator.getErr("lnsp_count", "奖品期ID异常"));
        }
        //奖品操作
        SvcSysPrizeSetAdmin.getIns().editSaveValidator(result, vo);
        XValidator.getResult(result);

        if (!isAdminEdit()) {
            vo.setLnsp_acc_id(getAccId());
            vo.setLnsp_group_id(entitySession.getLns_group_id());
        }

        vo.setLnsp_name(vo.getPset_name());
        //vo.setLnsp_desc(vo.getPset_desc());
        vo.setLnsp_pic(vo.getPset_pic());

        InputData inputData = InputData.fromRequest();
        return SvcLNumSessionPrize.getBean().edit(inputData, vo);
    }
}
