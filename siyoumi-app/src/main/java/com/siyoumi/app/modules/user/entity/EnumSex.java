package com.siyoumi.app.modules.user.entity;

import com.siyoumi.component.XEnumBase;


public class EnumSex
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "未知");
        put(1, "男");
        put(2, "女");
    }
}
