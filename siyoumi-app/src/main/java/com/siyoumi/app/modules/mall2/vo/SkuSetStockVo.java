package com.siyoumi.app.modules.mall2.vo;

import lombok.Data;

//修改库存
@Data
public class SkuSetStockVo {
    String sku_id;
    Long left_count;
    Long left_count_old;
}
