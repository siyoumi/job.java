package com.siyoumi.app.modules.account.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;

//切换商家
@Data
@Accessors(chain = true)
public class VaChangeStore {
    @HasAnyText
    private String store_id;
    private String acc_id;

    static public VaChangeStore of(String storeId, String accId) {
        return new VaChangeStore()
                .setStore_id(storeId)
                .setAcc_id(accId);
    }
}
