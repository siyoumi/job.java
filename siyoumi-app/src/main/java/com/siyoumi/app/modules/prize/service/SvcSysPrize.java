package com.siyoumi.app.modules.prize.service;

import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.prize.vo.PrizeDeliverVo;
import com.siyoumi.app.service.SysPrizeService;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class SvcSysPrize {
    static public SvcSysPrize getBean() {
        return XSpringContext.getBean(SvcSysPrize.class);
    }

    static public SysPrizeService getApp() {
        return SysPrizeService.getBean();
    }

    /**
     * 类型
     */
    static public List<String> types() {
        List<String> types = new ArrayList<>();
        types.add("");
        types.add("fun");
        types.add("stock");
        return types;
    }


    public JoinWrapperPlus<SysPrize> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<SysPrize> listQuery(InputData inputData) {
        String name = inputData.input("name");
        String idSrc = inputData.input("id_src");
        String dateBegin = inputData.input("date_begin");
        String dateEnd = inputData.input("date_end");
        String typeUse = inputData.input("type_use");

        JoinWrapperPlus<SysPrize> query = SvcSysPrize.getApp().join();
        query.join(WxUser.table(), "wxuser_openid", "prize_openid");

        query.eq("prize_x_id", XHttpContext.getX())
                .eq("wxuser_x_id", XHttpContext.getX())
                .eq("prize_del", 0)
                .orderByDesc("prize_id");
        if (XStr.hasAnyText(inputData.getID())) {
            query.eq("prize_id", inputData.getID());
        } else {
            if (XStr.hasAnyText(idSrc)) {
                query.eq("prize_id_src", idSrc);
            } else {
                query.in("prize_type", types());
            }
            if (XStr.hasAnyText(name)) { // 名称
                query.like("prize_name", name);
            }
            if (XStr.hasAnyText(dateBegin) && XStr.hasAnyText(dateEnd)) { // 创建日期
                LocalDateTime b = XStr.toDateTime(dateBegin);
                LocalDateTime e = XStr.toDateTime(dateEnd);
                query.between("prize_create_date", b, e);
            }
            if (XStr.hasAnyText(typeUse)) { //使用方式
                query.eq("prize_use_type", typeUse);
            }
        }

        return query;
    }


    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(JoinWrapperPlus<SysPrize> query) {
        List<SysPrize> list = getApp().get(query);
        if (list.size() <= 0) {
            return EnumSys.ARR_SIZE_0.getR("数据为0");
        }

        for (SysPrize entity : list) {
            getApp().delete(entity.getKey());
        }

        return EnumSys.OK.getR();
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        InputData inputData = InputData.getIns();
        JoinWrapperPlus<SysPrize> query = listQuery(inputData);
        query.in("prize_id", ids);

        return delete(query);
    }


    //发货
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn deliver(PrizeDeliverVo vo) {
        SysPrize entityPrize = getApp().getEntity(vo.getPrize_id());
        if (entityPrize == null) {
            return EnumSys.ERR_VAL.getR("奖品ID异常");
        }
        if (!entityPrize.use()) {
            return XReturn.getR(20103, "奖品未核销");
        }

        SysPrize entityUpdate = new SysPrize();
        entityUpdate.setPrize_id(entityPrize.getPrize_id());
        entityUpdate.setPrize_deliver_no(vo.getDeliver_no());
        entityUpdate.setPrize_deliver_date(XDate.now());
        getApp().updateById(entityUpdate);

        return XReturn.getR(0, "发货成功");
    }
}
