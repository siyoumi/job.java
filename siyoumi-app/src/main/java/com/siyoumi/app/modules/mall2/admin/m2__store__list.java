package com.siyoumi.app.modules.mall2.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.M2Group;
import com.siyoumi.app.entity.M2Store;
import com.siyoumi.app.modules.mall2.service.SvcM2Group;
import com.siyoumi.app.modules.mall2.service.SvcM2Store;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/mall2/m2__store__list")
public class m2__store__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商家-列表");

        InputData inputData = InputData.fromRequest();
        JoinWrapperPlus<M2Store> query = SvcM2Store.getBean().listQuery(inputData);

        IPage<M2Store> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcM2Store.getApp().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            M2Store entity = SvcM2Store.getApp().loadEntity(data);
            data.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);
        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcM2Store.getBean().delete(Arrays.asList(ids));
    }
}
