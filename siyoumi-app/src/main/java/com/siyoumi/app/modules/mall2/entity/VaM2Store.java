package com.siyoumi.app.modules.mall2.entity;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaM2Store {
    private String mstore_acc_id;
    @HasAnyText
    @Size(max = 50)
    private String mstore_name;
    @Size(max = 20)
    private String mstore_phone;
    @Size(max = 100)
    private String mstore_address;
    @Size(max = 100)
    private String mstore_desc;
    @Size(max = 200)
    private String mstore_pic_desc;
}
