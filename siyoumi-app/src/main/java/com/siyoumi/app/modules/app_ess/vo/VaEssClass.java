package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;

//班级
@Data
public class VaEssClass {
    private String eclass_acc_id;
    @HasAnyText
    @Size(max = 50)
    private String eclass_name;
    private Integer eclass_order;

    private Integer teacher_set = 0;
    private List<String> teacher_uids; //选择老师
}
