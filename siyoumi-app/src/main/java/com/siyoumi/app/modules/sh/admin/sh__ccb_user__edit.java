package com.siyoumi.app.modules.sh.admin;

import com.siyoumi.app.entity.ShUser;
import com.siyoumi.app.modules.sh.vo.VaShUser;
import com.siyoumi.app.service.ShUserService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/sh/sh__ccb_user__edit")
public class sh__ccb_user__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("CCB用户-编辑");

        ShUserService app = ShUserService.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("suser_enable", 0);
        data.put("suser_order", 0);
        data.put("key", "");
        if (isAdminEdit()) {
            ShUser entity = app.loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            dataAppend.put("key", entity.getSuser_key());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaShUser vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        ShUserService app = ShUserService.getBean();

        InputData inputData = InputData.fromRequest();
        inputData.putAll(XBean.toMap(vo));

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            //ignoreField.add("suser_id");
            ignoreField.add("suser_x_id");
        }
        vo.setSuser_type(1);

        return app.saveEntity(inputData, vo, false, ignoreField);
    }
}
