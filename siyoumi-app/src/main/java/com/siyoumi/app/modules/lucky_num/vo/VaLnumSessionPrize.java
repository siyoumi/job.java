package com.siyoumi.app.modules.lucky_num.vo;

import com.siyoumi.app.modules.prize.vo.VaSysPrizeSet;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//奖品期-奖品
@Data
public class VaLnumSessionPrize
        extends VaSysPrizeSet {
    private String lnsp_acc_id;
    private String lnsp_group_id;
    @HasAnyText
    private String lnsp_session_id;
    @Size(max = 50)
    private String lnsp_name;
    private String lnsp_pic;
    private String lnsp_desc;
    private Integer lnsp_count;
    private Integer lnsp_order;
}
