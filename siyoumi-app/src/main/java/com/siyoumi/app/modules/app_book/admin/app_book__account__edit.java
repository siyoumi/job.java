package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.FksCity;
import com.siyoumi.app.modules.account.service.SvcAccount;
import com.siyoumi.app.modules.account.vo.VaAccount;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.app.modules.app_book.vo.VaBookStoreAccount;
import com.siyoumi.app.modules.app_fks.service.SvcFksCtiy;
import com.siyoumi.app.modules.app_fks.vo.VaFksCity;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__account__edit")
public class app_book__account__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("帐号-编辑");

        Map<String, Object> data = new HashMap<>();
        //data.put("fcity_order", 0);
        if (isAdminEdit()) {
            SysAccount entity = SysAccountService.getBean().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);
        //x
        setPageInfo("super_uid", XHttpContext.getX());

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaAccount vo, BindingResult result) {
        if (XStr.isNullOrEmpty(vo.getAcc_phone())) {
            result.addError(XValidator.getErr("acc_phone", "请输入手机号"));
        }
        //统一验证
        XValidator.getResult(result);
        //
        //uid
        {
            JoinWrapperPlus<SysAccount> query = SvcAccount.getBean().listQuery();
            query.eq("acc_uid", vo.getAcc_uid());
            if (isAdminEdit()) {
                query.ne(SysAccount.tableKey(), getID());
            }
            if (SvcAccount.getApp().first(query) != null) {
                result.addError(XValidator.getErr("acc_uid", "帐号已存在"));
            }
        }
        //phone
        {
            JoinWrapperPlus<SysAccount> query = SvcAccount.getBean().listQuery();
            query.eq("acc_phone", vo.getAcc_phone());
            if (isAdminEdit()) {
                query.ne(SysAccount.tableKey(), getID());
            }
            if (SvcAccount.getApp().first(query) != null) {
                result.addError(XValidator.getErr("acc_phone", "手机号已存在"));
            }
        }
        //
        XValidator.getResult(result);
        //
        vo.setAcc_role("store");
        vo.setAcc_store_app_id("app_book_store");
        //
        InputData inputData = InputData.fromRequest();
        return SvcAccount.getBean().edit(inputData, vo);
    }
}