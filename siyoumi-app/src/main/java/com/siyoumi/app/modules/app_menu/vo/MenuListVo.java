package com.siyoumi.app.modules.app_menu.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class MenuListVo {
    @NotBlank
    @Size(max = 200)
    private String name;
    private String pid;

    private List<MenuListType> list0;
    private List<MenuListType> list2;
}
