package com.siyoumi.app.modules.pwd.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class PwdVo {
    @NotBlank
    @Size(max = 50)
    private String new_pwd;
    @NotBlank
    @Size(max = 50)
    private String new_pwd2;
    @NotBlank
    @Size(max = 50)
    private String old_pwd;
}
