package com.siyoumi.app.modules.lucky_num.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.LnumGroup;
import com.siyoumi.app.entity.LnumNumSet;
import com.siyoumi.app.entity.LnumSession;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumGroup;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumSession;
import com.siyoumi.app.modules.lucky_num.service.SvcLnumNumSet;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/lucky_num/lucky_num__session__list")
public class lucky_num__session__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("奖品期-列表");

        InputData inputData = InputData.fromRequest();

        String groupId = inputData.input("group_id");
        //if (XStr.isNullOrEmpty(groupId)) {
        //    return EnumSys.MISS_VAL.getR("miss group_id");
        //}
        //LnumGroup entityGroup = SvcLNumGroup.getApp().first(groupId);
        //if (entityGroup == null) {
        //    return EnumSys.ERR_VAL.getR("group_id异常");
        //}

        String[] select = {
                "lns_id",
                "lns_acc_id",
                "lns_group_id",
                "lns_name",
                "lns_win_date_end",
                "lns_win",
                "lns_win_date",
                "lns_order",
                "lngroup_name",
                "lngroup_type",
                "lnset_id",
                "lnset_get_date_begin",
                "lnset_get_date_end",
                "lnset_user_get_max",
                "lnset_fun_enable",
                "lnset_fun",
        };
        JoinWrapperPlus<LnumSession> query = SvcLNumSession.getBean().listQuery(inputData);
        query.join(LnumGroup.table(), LnumGroup.tableKey(), "lns_group_id");
        query.leftJoin(LnumNumSet.table(), LnumNumSet.tableKey(), "lns_id");
        query.select(select);

        IPage<LnumSession> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcLNumSession.getApp().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            LnumSession entity = SvcLNumSession.getApp().loadEntity(data);
            data.put("id", entity.getKey());
            //开奖
            data.put("win", entity.win());

            String lnsetId = (String) data.get("lnset_id");
            if (XStr.hasAnyText(lnsetId)) {
                LnumNumSet entitySet = SvcLnumNumSet.getApp().loadEntity(data);
                data.put("get_expire", entitySet.expire());
            }
        }

        getR().setData("list", list);
        getR().setData("count", count);
        return getR();
    }


    //删除
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcLNumSession.getBean().delete(List.of(ids));
    }
}
