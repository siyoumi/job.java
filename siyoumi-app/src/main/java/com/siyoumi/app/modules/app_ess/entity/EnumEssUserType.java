package com.siyoumi.app.modules.app_ess.entity;

import com.siyoumi.component.XEnumBase;


public class EnumEssUserType
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "学生");
        put(1, "老师");
    }
}
