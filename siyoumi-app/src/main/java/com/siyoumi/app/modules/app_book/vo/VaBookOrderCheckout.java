package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//退房
@Data
public class VaBookOrderCheckout {
    @HasAnyText(message = "miss ids")
    private List<String> ids;

}
