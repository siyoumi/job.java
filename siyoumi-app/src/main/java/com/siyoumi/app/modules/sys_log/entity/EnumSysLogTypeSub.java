package com.siyoumi.app.modules.sys_log.entity;

import com.siyoumi.util.IEnum;

//日志子类型
public enum EnumSysLogTypeSub
        implements IEnum {
    ERROR("error", "错误日志"),
    ADMIN("admin", "操作日志");


    private String key;
    private String val;

    EnumSysLogTypeSub(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
