package com.siyoumi.app.modules.redbook.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.RedbookComment;
import com.siyoumi.app.modules.redbook.entity.EnumRedbookQuanEnable;
import com.siyoumi.app.modules.redbook.service.SvcRedbookComment;
import com.siyoumi.app.modules.redbook.vo.RedbookCommentAuditVo;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/redbook/redbook__comment__list")
public class redbook__comment__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("评论列表");

        InputData inputData = InputData.fromRequest();
        String pid = inputData.input("pid");
        String quanId = inputData.input("quan_id");
        //if (XStr.isNullOrEmpty(pid)) {
        //    if (XStr.isNullOrEmpty(quanId)) {
        //        return XReturn.getR(20038, "miss quan_id");
        //    }
        //}

        JoinWrapperPlus<RedbookComment> query = SvcRedbookComment.getBean().selectQuery(inputData);
        if (XStr.hasAnyText(pid)) {
            //2级评论
            query.eq("rbc_pid", pid);
        } else {
            //1级评论
            query.eq("rbc_pid", "");
        }

        IPage<RedbookComment> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcRedbookComment.getApp().getMaps(page, query);
        //
        EnumRedbookQuanEnable enumRedbookQuanEnable = EnumRedbookQuanEnable.of();
        //
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();
        //
        for (Map<String, Object> data : list) {
            RedbookComment entity = SvcRedbookComment.getApp().loadEntity(data);
            data.put("id", entity.getKey());
            //审核
            data.put("enable", enumRedbookQuanEnable.get(entity.getRbc_enable()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }

    //审核
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/audit")
    public XReturn audit(@Validated RedbookCommentAuditVo vo, BindingResult result) {
        //通用验证
        XValidator.getResult(result, true, true);

        return SvcRedbookComment.getBean().audit(vo);
    }
}
