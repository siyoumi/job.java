package com.siyoumi.app.modules.lucky_num.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

//奖品期
@Data
public class VaLnumSession extends VaLnumNumSet {
    private String lns_acc_id;
    @HasAnyText
    private String lns_group_id;
    @HasAnyText
    @Size(max = 50)
    private String lns_name;
    @HasAnyText
    private LocalDateTime lns_win_date_end;
    private Long lns_fun;
    private Integer lns_order;

}
