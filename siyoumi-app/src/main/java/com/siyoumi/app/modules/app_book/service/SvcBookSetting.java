package com.siyoumi.app.modules.app_book.service;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_ess.vo.VaEssSettingTxt00;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XRedis;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

//设置
@Slf4j
@Service
public class SvcBookSetting
        implements IWebService {
    static public SvcBookSetting getBean() {
        return XSpringContext.getBean(SvcBookSetting.class);
    }

    static public SysAbcService getApp() {
        return SysAbcService.getBean();
    }

    public SysAbc getSetting() {
        return XRedis.getBean().getAndSetData(getApp().getEntityCacheKey("app_book_setting"), k -> {
            return getApp().getEntityByUix("app_book_setting");
        }, SysAbc.class);
    }
}
