package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;


//学生提交答案-每题
@Data
public class VoEssTestAnswerItem {
    String item_id;
    private List<Integer> answer; //答案
}
