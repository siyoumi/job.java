package com.siyoumi.app.modules.app_show.vo;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaShowSetting {
    @Size(max = 2000)
    String abc_txt_00;
    @Size(max = 2000)
    String abc_txt_01;
}
