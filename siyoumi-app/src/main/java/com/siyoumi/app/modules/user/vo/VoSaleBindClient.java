package com.siyoumi.app.modules.user.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;

//分销员绑定客户
@Data
@Accessors(chain = true)
public class VoSaleBindClient {
    String client_uid;
    @HasAnyText(message = "miss sale_uid")
    String sale_uid;
}
