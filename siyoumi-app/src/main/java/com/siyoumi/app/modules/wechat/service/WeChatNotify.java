package com.siyoumi.app.modules.wechat.service;

import com.siyoumi.app.modules.wechat.entity.WechatNotifyData;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.ApiController;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

//微信回调
//http://127.0.0.1:8010/sys/wechat/notify
@Slf4j
@RestController
@RequestMapping("/sys/wechat/notify")
public class WeChatNotify
        extends ApiController {
    @RequestMapping()
    public Object index(@RequestBody(required = false) WechatNotifyData data) {
        InputData inputData = InputData.fromRequest();
        //log.info(XJson.toJSONString(inputData));
        List<String> groups = new ArrayList<>();
        //groups.add("10877226186@chatroom");//嗦粉

        groups.add("3539809173@chatroom");//开平-友
        groups.add("24801139274@chatroom");//404
        groups.add("34868800067@chatroom");//1024
        groups.add("18360659908@chatroom");//测试群

        log.info(XJson.toJSONString(data));

        if (data.getIs_group()) {
            if (groups.contains(data.getRoomid())) {
                //摸鱼
                WeChatEventMoyu.getBean().handle(data);

                //抽奖
                WeChatEventChou.getBean().handle(data);

                //看图猜词
                WeChatEventCai.getBean().handle(data);
            }

        } else {
            //if (data.getIs_at()) {
            //    String msgReply = aiReply(data.getContent());
            //
            //    if (XStr.hasAnyText(msgReply)) {
            //        WechatOpenApi.getIns(null).sendTxt(data.getSender(), msgReply, data.getSender());
            //    }
            //}
        }

        return "success";
    }


    public String aiReply(String msg) {
        if (XStr.isNullOrEmpty(msg)) {
            return "";
        }

        return msg.replace("吗", "")
                .replace("?", "!")
                .replace("？", "!");
    }
}
