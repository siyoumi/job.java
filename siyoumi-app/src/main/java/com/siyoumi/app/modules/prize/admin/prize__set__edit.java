package com.siyoumi.app.modules.prize.admin;

import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.prize.entity.EnumPrizeType;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSet;
import com.siyoumi.app.modules.prize.vo.VaSysPrizeSet;
import com.siyoumi.app.service.*;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/prize/prize__set__edit")
public class prize__set__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("奖品-编辑");

        SvcSysPrizeSet app = SvcSysPrizeSet.getBean();

        String idSrc = input("id_src");
        if (XStr.isNullOrEmpty(idSrc)) {
            return EnumSys.MISS_VAL.getR("miss id_src");
        }

        Map<String, Object> data = new HashMap<>();
        data.put("pset_app_id", XHttpContext.getAppId());
        data.put("pset_type", "");
        data.put("pset_use_type", 2);
        data.put("pset_id_src", idSrc);
        data.put("pset_int_00", 0);
        data.put("pset_int_01", 0);
        data.put("pschou_win_rate", 0);
        data.put("pset_vaild_date_type", 1);
        data.put("pset_vaild_date_win_h", 0);
        data.put("pset_key", XApp.getStrID());
        // 为了避免，错误修改，库存会赋到新值
        data.put("__stock_count_use", 0);
        data.put("__stock_count_left", 0);
        data.put("__stock_count_left_old", 0);
        // 通知
        data.put("pset_send_type", 0);
        if (isAdminEdit()) {
            SysPrizeSet entity = SvcSysPrizeSet.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());

            SysStock entityStock = SysStockService.getBean().getEntityBySrc(entity.getKey(), false);
            if (entityStock != null) {
                dataAppend.put("__stock_count_use", entityStock.getStock_count_use());
                dataAppend.put("__stock_count_left", entityStock.getStock_count_left());
                dataAppend.put("__stock_count_left_old", entityStock.getStock_count_left());
            }
            if (SvcSysPrizeSet.isAct()) {
                SysPrizeSetChou entityPrizeChou = SysPrizeSetChouService.getBean().getEntity(entity.getKey());
                if (entityPrizeChou != null) {
                    dataAppend.put("pschou_win_rate", entityPrizeChou.getPschou_win_rate());
                }
            }
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //奖品类型
        LinkedHashMap<String, String> prizeType = IEnum.toMap(EnumPrizeType.class);
        setPageInfo("list_type", prizeType);
        setPageInfo("is_wxapp", XHttpContext.getXConfig().wxApp());

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaSysPrizeSet vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysPrizeSetService app = SysPrizeSetService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("pset_x_id");
            ignoreField.add("pset_type");
        }

        InputData inputData = InputData.fromRequest();
        inputData.putAll(XBean.toMap(vo));
        return app.saveEntity(inputData, true, ignoreField);
    }
}
