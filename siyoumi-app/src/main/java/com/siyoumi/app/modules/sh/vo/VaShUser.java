package com.siyoumi.app.modules.sh.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class VaShUser {
    @HasAnyText
    @Size(max = 50)
    private String suser_name;
    private String suser_account;
    private String suser_phone;
    @HasAnyText
    private String suser_key;
    private LocalDateTime suser_key_expire_date;
    private String suser_share_code;
    private Integer suser_type;
    private Integer suser_enable;
    private Integer suser_order;
}
