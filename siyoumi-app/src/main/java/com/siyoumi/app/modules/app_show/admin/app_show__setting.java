package com.siyoumi.app.modules.app_show.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_show.vo.VaShowSetting;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_show/app_show__setting")
public class app_show__setting
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("基础配置");

        SysAbcService svcAbc = SysAbcService.getBean();
        String uix = "app_show_setting";
        SysAbc entity = svcAbc.getEntityByUix(uix);
        if (entity == null) {
            entity = new SysAbc();
            entity.setAbc_app_id(XHttpContext.getAppId());
            entity.setAbc_x_id(XHttpContext.getX());
            entity.setAbc_table("app_show_setting");
            entity.setAbc_uix(uix);
            entity.setAutoID();

            svcAbc.save(entity);
            entity = svcAbc.getEntityByUix(uix);
        }

        Map<String, Object> data = new HashMap<>();

        HashMap<String, Object> dataAppend = new LinkedHashMap<>();
        dataAppend.put("id", entity.getKey());
        //合并
        data = entity.toMap();
        data.putAll(dataAppend);

        getR().setData("data", data);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaShowSetting vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAbcService svcAbc = SysAbcService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
        }

        InputData inputData = InputData.fromRequest();

        inputData.putAll(XBean.toMap(vo));
        return svcAbc.saveEntity(inputData, true, ignoreField);
    }
}
