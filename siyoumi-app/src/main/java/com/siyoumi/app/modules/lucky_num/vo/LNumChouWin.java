package com.siyoumi.app.modules.lucky_num.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

//开奖参数
@Data
public class LNumChouWin {
    @HasAnyText(message = "缺少奖品期ID")
    String session_id;

    @HasAnyText(message = "请输入号码")
    @Size(min = 1, message = "请输入号码")
    List<LNumChouWinItem> items;
}
