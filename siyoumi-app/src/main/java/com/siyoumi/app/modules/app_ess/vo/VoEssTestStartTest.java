package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;


//学生开始评测
@Data
public class VoEssTestStartTest {
    @HasAnyText(message = "缺少试卷ID")
    String result_id;
}
