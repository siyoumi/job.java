package com.siyoumi.app.modules.mall2.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

//修改商城规格属性
@Data
public class SpuAttrVo {
    String mattr_id; //有值：视为编辑，无值：添加
    @NotBlank(message = "缺少商品ID")
    String mattr_spu_id;
    @NotBlank
    @Size(max = 50)
    String mattr_name;
    Integer del = 0;

    @Size(min = 1, message = "请配置规格属性")
    List<SpuAttrSkuVo> attr_skus;

    @JsonIgnore
    public Boolean isDelete() {
        return getDel() == 1;
    }
}
