package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

//下单
@Data
public class VoBookOrder {
    @HasAnyText(message = "miss spu_id")
    String spu_id;
    @HasAnyText(message = "miss sku_id")
    String sku_id;
    @HasAnyText(message = "miss set_id")
    String set_id;

    @HasAnyText(message = "缺少入住时间")
    LocalDateTime date_begin;
    @HasAnyText(message = "缺少入住天数")
    @Min(value = 1, message = "入住天数不能小于1")
    Integer day_count;

    @HasAnyText(message = "缺少姓名")
    @Size(max = 50)
    String user_name;
    @HasAnyText(message = "缺少手机号")
    @Size(max = 50)
    String user_phone;


    @HasAnyText(message = "请输入预订房间数")
    @Max(value = 20, message = "预订房间数最多20")
    @Min(value = 1, message = "预订房间数不能小于1")
    Integer spu_count;
    @HasAnyText(message = "请输入用餐人数")
    @Min(value = 1, message = "用餐人数不能小于1")
    Integer set_user_count;

    Integer pay_deposit = 0; //【0】全单支付；【1】只付订金；
    Integer fun = 0; //积分抵扣
    @Size(max = 50, message = "备注不能超50长度")
    String desc; //备注
}
