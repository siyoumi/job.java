package com.siyoumi.app.modules.app_ess.vo;

import lombok.Data;

@Data
public class VaEssSettingTxt00 {
    Integer add_fun_file = 0;
    Integer add_fun_module = 0;
    Integer add_fun_test0 = 0;
    Integer add_fun_test1 = 0;
}
