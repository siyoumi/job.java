package com.siyoumi.app.modules.sh.task;

import com.siyoumi.app.modules.sh.task.jm.JmTask;
import com.siyoumi.app.modules.sh.task.jm.JmTaskWatsons;
import com.siyoumi.app.modules.sh.task.jm.TaskBase;
import com.siyoumi.component.XBean;
import com.siyoumi.config.SysConfig;
import com.siyoumi.task.EnumTask;
import com.siyoumi.task.TaskController;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

//jm任务
@RestController
@RequestMapping("/task/sh/task_jm")
public class task_jm
        extends TaskController {
    @Override
    protected List handleData() {
        List<String> list = new ArrayList<>();
        list.add("1");
        return list;
    }

    @Override
    protected void handleBefore(Object data) {

    }

    @Override
    protected XReturn handle(Object data) {
        List<Class<?>> list = new ArrayList<>();
        if (SysConfig.getIns().isDev()) {
            list.add(JmTask.class);
        } else {
            list.add(JmTask.class);
            list.add(JmTaskWatsons.class);
        }
        //list.add(JmTaskRoco.class);
        //list.add(JmTaskWatsons.class);

        XLog.info(this.getClass(), "BEGIN:" + XDate.toDateTimeString());
        for (Class<?> t : list) {
            TaskBase app = (TaskBase) XBean.newIns(t);
            app.exec();
        }
        XLog.info(this.getClass(), "END:" + XDate.toDateTimeString());

        return EnumTask.STOP.getR();
    }
}
