package com.siyoumi.app.modules.fun_mall.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

//分组
@Data
public class VaFunMallGroup {
    private String fgroup_acc_id;
    @HasAnyText
    @Size(max = 50)
    private String fgroup_name;
    private String fgroup_pic;
    private Integer fgroup_order;
}
