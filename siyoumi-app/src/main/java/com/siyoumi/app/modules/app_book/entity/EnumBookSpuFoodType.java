package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;

//做饭
public class EnumBookSpuFoodType
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "无");
        put(1, "包餐");
        put(2, "可做饭");
    }
}
