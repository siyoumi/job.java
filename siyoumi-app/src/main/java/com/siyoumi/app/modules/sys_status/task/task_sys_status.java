package com.siyoumi.app.modules.sys_status.task;

import com.siyoumi.app.modules.sys_status.entity.SysStatusInfo;
import com.siyoumi.component.XRedis;
import com.siyoumi.component.api.jj.JJ;
import com.siyoumi.component.api.jj.robot.RobotMsg;
import com.siyoumi.config.SysConfig;
import com.siyoumi.exception.XException;
import com.siyoumi.task.EnumTask;
import com.siyoumi.task.TaskController;
import com.siyoumi.util.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

//检查所有服务器状态
@RestController
@RequestMapping("/task/sys_status/task_sys_status")
public class task_sys_status
        extends TaskController {

    @Override
    protected List handleData() {
        List<String> list = new ArrayList<>();
        list.add("1");
        return list;
    }

    @Override
    protected void handleBefore(Object data) {
    }

    @Override
    protected XReturn handle(Object data) {
        List<String> servers = new ArrayList<>();
        if (SysConfig.getIns().isDev()) {
            servers.add("dev");
            servers.add("sh");
        } else {
            //servers.add("dev");
            servers.add("siyoumi");
            servers.add("fjciecc");
        }
        long serverTotal = servers.size();

        int existsErrCount = 0;

        String msg = "";
        //服务器
        String currServer = SysConfig.getIns().getEnv();
        msg += XStr.concat("**[", currServer, "]** \n\n");

        for (String server : servers) {
            XLog.debug(this.getClass(), server);

            msg += XStr.concat("**", server, "** \n\n");

            boolean existsErr = false; //存在服务异常
            try {
                String root = getRoot(server);
                String url = XStr.concat(root, "app/sys_status");
                XHttpClient client = XHttpClient.getInstance(30000);
                String returnStr = client.get(url, null);

                XLog.info(this.getClass(), "server: ", server);
                XLog.info(this.getClass(), "url: ", url);
                XLog.info(this.getClass(), "return_str: ", returnStr);

                SysStatusInfo sysStatusInfo = XJson.parseObject(returnStr, SysStatusInfo.class);
                if (!sysStatusInfo.sysOk()) {
                    existsErr = true;
                }
                msg += sysStatusInfo.print();
            } catch (Exception ex) {
                existsErr = true;
                msg += XStr.concat("**", ex.getMessage(), "** \n\n");
            }

            if (existsErr) {
                existsErrCount++;
            }
        }

        boolean sendJJ = false; //是否发提醒

        String title = XStr.concat("server ok:" + (serverTotal - existsErrCount) + "/" + serverTotal);
        if (existsErrCount > 0) {
            //title = "服务器：err";
            msg = "### 检查服务器(*服务器存在异常*) \n\n" + msg;
            sendJJ = true;
        } else {
            msg = "### 检查服务器 \n\n" + msg;

            Boolean ok = XRedis.getBean().setIfExists(this.getClass().getSimpleName(), XDate.toDateTimeString(), 60 * 60 * 5);
            if (ok) {
                sendJJ = true;
            }
        }

        if (sendJJ) {
            JJ jj = JJ.getIns(RobotMsg.class);
            jj.sendApiMarkdown(title, msg, existsErrCount > 0);
        }

        return EnumTask.RUN.getR();
    }


    public String getRoot(String server) {
        String root = "";
        switch (server) {
            case "dev":
                root = "http://dev.x.siyoumi.com/";
                break;
            case "sh":
                root = "http://app.sh.siyoumi.com/";
                break;
            case "siyoumi":
                root = "http://123.207.78.56:8010/";
                break;
            case "fjciecc":
                root = "https://app.ws.fjciecc.com/";
                break;
            default:
                throw new XException("未配置server");
        }
        return root;
    }
}
