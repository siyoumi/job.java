package com.siyoumi.app.modules.app_show.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.JobOrder;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_show.entity.EnumJobOrderState;
import com.siyoumi.app.modules.app_show.entity.EnumJobOrderType;
import com.siyoumi.app.modules.app_show.service.SvcJobOrder;
import com.siyoumi.app.modules.app_show.vo.VaJobOrder;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wxapp/app_show/job_order")
public class app_show__job_order__api
        extends WxAppApiController {
    /**
     * 初始化
     */
    @GetMapping("init")
    public XReturn init() {
        SysAbcService svcAbc = SysAbcService.getBean();
        SysAbc setting = svcAbc.getEntityByUix("app_show_setting");
        if (setting == null) {
            return EnumSys.ERR_VAL.getR("应用未初始化");
        }

        getR().setData("abc_str_00", setting.getAbc_str_00());
        getR().setData("abc_txt_00", setting.getAbc_txt_00());
        getR().setData("abc_txt_01", setting.getAbc_txt_01());


        //订单信息
        JoinWrapperPlus<JobOrder> query = SvcJobOrder.getBean().listQuery();
        query.select("COUNT(*) order_count", "IFNULL(SUM(jorder_price_total),0) price_total");
        query.ge("jorder_state", 15); //开发中订单
        Map<String, Object> mapOrder = SvcJobOrder.getApp().firstMap(query);
        getR().setData("order", mapOrder);


        return getR();
    }

    //添加
    @PostMapping("edit")
    public XReturn edit(@Validated VaJobOrder vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true, true);

        InputData inputData = InputData.fromRequest();
        if (inputData.isAdminAdd()) {
            vo.setJorder_openid(getOpenid());
            vo.setJorder_src("wxapp");
        }

        return SvcJobOrder.getBean().edit(inputData, vo);
        //return getR();
    }

    //项目详情
    @GetMapping("info")
    public XReturn info() {
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }

        JobOrder entity = SvcJobOrder.getApp().getEntity(getID());
        if (entity == null) {
            return EnumSys.ERR_VAL.getR("订单号不存在");
        }

        String state = IEnum.getEnmuVal(EnumJobOrderState.class, entity.getJorder_state());

        getR().setData("order", entity);
        getR().setData("state", state);

        return getR();
    }

    /**
     * 我的订单列表
     */
    @GetMapping("list")
    public XReturn list() {
        SvcJobOrder svcjobOrder = SvcJobOrder.getBean();

        String[] select = {
                "jorder_id",
                "jorder_create_date",
                "jorder_openid",
                "jorder_type",
                "jorder_name",
                "jorder_banner",
                "jorder_desc",
                "jorder_price_total",
                "jorder_price_sales",
                "jorder_state",
                "jorder_src",
        };

        JoinWrapperPlus<JobOrder> query = svcjobOrder.listQuery();
        query.eq("jorder_openid", getOpenid());
        query.select(select);

        IPage<JobOrder> page = new Page<>(getPageIndex(), getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = SvcJobOrder.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();

        for (Map<String, Object> data : list) {
            JobOrder entity = XBean.fromMap(data, JobOrder.class);
            data.put("id", entity.getKey());

            //类型
            String type = IEnum.getEnmuVal(EnumJobOrderType.class, entity.getJorder_type());
            data.put("type", type);
            //状态
            String state = IEnum.getEnmuVal(EnumJobOrderState.class, entity.getJorder_state());
            data.put("state", state);
        }

        getR().setData("list", list);

        return getR();
    }

    /**
     * 删除订单
     */
    @Transactional
    @GetMapping("del")
    public XReturn del() {
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }

        JobOrder entity = SvcJobOrder.getApp().loadEntity(getID());
        if (entity == null) {
            return EnumSys.ERR_VAL.getR("订单号不存在");
        }
        if (!entity.getJorder_openid().equals(getOpenid())) {
            return EnumSys.ERR_OPENID.getR();
        }
        if (entity.getJorder_state() >= 15) {
            String state = IEnum.getEnmuVal(EnumJobOrderState.class, entity.getJorder_state());
            return XReturn.getR(20138, "订单" + state + "，无法删除");
        }

        return SvcJobOrder.getBean().delete(Arrays.asList(entity.getKey()));
    }
}
