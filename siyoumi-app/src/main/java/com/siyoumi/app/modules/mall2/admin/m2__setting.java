package com.siyoumi.app.modules.mall2.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_menu.vo.VaSetting;
import com.siyoumi.app.modules.mall2.vo.VaM2Setting;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.component.XApp;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/mall2/m2__setting")
public class m2__setting
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("基础配置");

        String uix = "mall2_setting";
        SysAbcService app = SysAbcService.getBean();
        SysAbc entity = app.getEntityByUix(uix);
        if (entity == null) {
            entity = new SysAbc();
            entity.setAbc_app_id(XHttpContext.getAppId());
            entity.setAbc_x_id(XHttpContext.getX());
            entity.setAbc_uix(uix);
            entity.setAbc_table(uix);
            entity.setAutoID();
            app.save(entity);

            entity = app.getEntityByUix(uix);
        }

        Map<String, Object> data = new HashMap<>();
        if (entity != null) {
            //合并
            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        if (XStr.isNullOrEmpty(entity.getAbc_str_00())) {
            data.put("abc_str_00", XApp.getStrID());
        }
        getR().setData("data", data);


        return getR();
    }

    @PostMapping("/save")
    @Transactional
    public XReturn save(@Validated VaM2Setting vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_x_id");
            ignoreField.add("abc_table");
        }

        SysAbc entityAbc = new SysAbc();
        XBean.copyProperties(vo, entityAbc);

        InputData inputData = InputData.fromRequest();

        return SysAbcService.getBean().saveEntity(inputData, vo, true, ignoreField);
    }
}
