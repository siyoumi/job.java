package com.siyoumi.app.modules.lucky_num.entity;

import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.XEnumBase;
import org.springframework.stereotype.Service;

@Service
public class EnumLnumType
        extends XEnumBase<String> {
    static public EnumLnumType getBean() {
        return XSpringContext.getBean(EnumLnumType.class);
    }

    @Override
    protected void initKV() {
        put("lucky_num", "集卡");
        put("fun_win", "积分夺宝");
        put("sign_chou", "报名抽奖");
    }
}
