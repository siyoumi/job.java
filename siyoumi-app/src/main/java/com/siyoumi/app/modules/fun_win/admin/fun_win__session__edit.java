package com.siyoumi.app.modules.fun_win.admin;

import com.siyoumi.app.entity.FunWinSession;
import com.siyoumi.app.modules.fun_win.service.SvcFunWinSession;
import com.siyoumi.app.modules.fun_win.vo.VaFunWinSession;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSetAdmin;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/fun_win/fun_win__session__edit")
public class fun_win__session__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("积分夺宝-编辑");

        InputData inputData = InputData.fromRequest();

        Map<String, Object> data = new HashMap<>();
        data.put("fwins_order", 0);
        if (isAdminEdit()) {
            FunWinSession entity = SvcFunWinSession.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //奖品配置
        SvcSysPrizeSetAdmin.getIns().editAfter(inputData, getR(), getID());

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaFunWinSession vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SvcSysPrizeSetAdmin.getIns().editSaveValidator(result, vo);
        XValidator.getResult(result);

        if (!isAdminEdit()) {
            vo.setFwins_acc_id(getAccId());
        }

        vo.setFwins_name(vo.getPset_name());

        InputData inputData = InputData.fromRequest();
        return SvcFunWinSession.getBean().edit(inputData, vo);
    }
}
