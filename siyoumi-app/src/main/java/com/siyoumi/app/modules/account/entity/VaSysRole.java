package com.siyoumi.app.modules.account.entity;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaSysRole {
    @HasAnyText
    @Size(max = 50)
    private String role_name;
    private String role_x_id;
    private Integer role_order;
    private Integer role_level;
    private Integer system_role = 0;
}
