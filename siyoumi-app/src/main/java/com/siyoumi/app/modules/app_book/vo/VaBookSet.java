package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

//套餐
@Data
public class VaBookSet {
    private String bset_acc_id;
    private String bset_store_id;

    @HasAnyText
    @Size(max = 50)
    private String bset_name;
    @HasAnyText
    private BigDecimal bset_day_price_breakfast;
    @HasAnyText
    private BigDecimal bset_day_price_lunch;
    @HasAnyText
    private BigDecimal bset_day_price_dinner;
    private BigDecimal bset_day_price;
    @Size(max = 500)
    private String bset_desc;
    private Integer bset_enable;
    private LocalDateTime bset_enable_date;
    private Integer bset_order;
}
