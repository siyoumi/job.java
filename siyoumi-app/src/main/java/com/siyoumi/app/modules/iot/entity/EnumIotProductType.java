package com.siyoumi.app.modules.iot.entity;

import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.XEnumBase;
import org.springframework.stereotype.Service;

/**
 * 产品类型
 */
@Service
public class EnumIotProductType
        extends XEnumBase<String> {
    static public EnumIotProductType getBean() {
        return XSpringContext.getBean(EnumIotProductType.class);
    }

    @Override
    protected void initKV() {
        put("power_on", "远程开机");
    }
}
