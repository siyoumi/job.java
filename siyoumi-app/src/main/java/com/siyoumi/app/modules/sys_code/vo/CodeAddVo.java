package com.siyoumi.app.modules.sys_code.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

//码批量添加
@Data
public class CodeAddVo {
    @Min(1)
    @Max(1000)
    Integer num;
    @HasAnyText
    String code_key;
    @NotNull
    LocalDateTime code_vaild_date_begin;
    @NotNull
    LocalDateTime code_vaild_date_end;
}
