package com.siyoumi.app.modules.wechat.service;

import com.siyoumi.app.modules.wechat.entity.WechatNotifyData;
import com.siyoumi.util.XReturn;

/**
 * 微信事件处理
 */
public interface WechatEvent {
    XReturn handle(WechatNotifyData data);
}
