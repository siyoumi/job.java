package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//模块
@Data
public class VaEssModule {
    private String emod_acc_id;
    @HasAnyText
    @Size(max = 50)
    private String emod_name;
    @Size(max = 200)
    private String emod_pic;
    @Size(max = 50)
    private String emod_exe_tag;
    private Integer emod_order;
}
