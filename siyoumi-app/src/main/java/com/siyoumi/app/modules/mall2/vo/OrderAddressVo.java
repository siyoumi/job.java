package com.siyoumi.app.modules.mall2.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

//地址参数
@Data
public class OrderAddressVo {
    @NotBlank(message = "请输入姓名")
    @Size(max = 50)
    String user_name;
    @NotBlank(message = "请输入手机号")
    @Size(max = 50)
    String user_phone;
    @NotBlank(message = "请输入地址")
    @Size(max = 500)
    String address;
}
