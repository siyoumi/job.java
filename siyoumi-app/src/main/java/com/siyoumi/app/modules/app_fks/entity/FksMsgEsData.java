package com.siyoumi.app.modules.app_fks.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FksMsgEsData {
    String id;
    Integer fmsg_type;
    String fmsg_title;
    String fmsg_keyword;
}
