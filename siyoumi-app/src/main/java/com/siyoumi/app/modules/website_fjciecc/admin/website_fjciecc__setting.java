package com.siyoumi.app.modules.website_fjciecc.admin;

import com.alibaba.fastjson.JSON;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.website_fjciecc.entity.WebSiteFjcieccRecruit;
import com.siyoumi.app.modules.website_fjciecc.entity.WebSiteFjcieccSettingTxt00;
import com.siyoumi.app.modules.website_fjciecc.service.SvcFjciecc;
import com.siyoumi.app.modules.website_fjciecc.vo.VaFjcieccSetting;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/website_fjciecc/website_fjciecc__setting")
public class website_fjciecc__setting
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("基础配置");

        SysAbcService svcAbc = SysAbcService.getBean();
        String uix = "fjciecc_setting";
        SysAbc entity = svcAbc.getEntityByUix(uix);
        if (entity == null) {
            entity = new SysAbc();
            entity.setAbc_app_id(XHttpContext.getAppId());
            entity.setAbc_x_id(XHttpContext.getX());
            entity.setAbc_table("fjciecc_setting");
            entity.setAbc_uix(uix);
            entity.setAutoID();
            entity.setAbc_acc_id(getAccId());

            svcAbc.save(entity);
            entity = svcAbc.getEntityByUix(uix);
        }

        if (XStr.isNullOrEmpty(entity.getAbc_str_00())) {
            entity.setAbc_str_00(XApp.getStrID());
        }

        Map<String, Object> data = new HashMap<>();

        HashMap<String, Object> dataAppend = new LinkedHashMap<>();
        dataAppend.put("id", entity.getKey());
        //合并
        data = entity.toMap();
        data.putAll(dataAppend);

        WebSiteFjcieccSettingTxt00 txt00 = WebSiteFjcieccSettingTxt00.getInstance(entity.getAbc_txt_00());
        data.putAll(XBean.toMap(txt00));

        getR().setData("data", data);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaFjcieccSetting vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAbcService svcAbc = SysAbcService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
        }
        vo.setAbc_uix("fjciecc_setting");

        InputData inputData = InputData.fromRequest();

        WebSiteFjcieccSettingTxt00 txt00 = XBean.fromMap(inputData, WebSiteFjcieccSettingTxt00.class);
        vo.setAbc_txt_00(JSON.toJSONString(txt00));

        svcAbc.delEntityCache(SvcFjciecc.keySetting);

        return svcAbc.saveEntity(inputData, vo, true, ignoreField);
    }
}
