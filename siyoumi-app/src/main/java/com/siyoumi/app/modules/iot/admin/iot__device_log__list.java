package com.siyoumi.app.modules.iot.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.IotDevice;
import com.siyoumi.app.entity.IotDeviceLog;
import com.siyoumi.app.entity.IotProduct;
import com.siyoumi.app.modules.iot.entity.EnumIotDeviceLogType;
import com.siyoumi.app.modules.iot.service.SvcIotDevice;
import com.siyoumi.app.service.IotDeviceLogService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/iot/iot__device_log__list")
public class iot__device_log__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("产品-日志");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "idlog_id",
                "idlog_create_date",
                "idlog_type",
                "idlog_content",
                "ipro_id",
                "ipro_name",
                "idev_num",
        };
        JoinWrapperPlus<IotDeviceLog> query = SvcIotDevice.getBean().listLogQuery(inputData);
        query.join(IotProduct.table(), IotProduct.tableKey(), "idlog_pro_id");
        query.join(IotDevice.table(), IotDevice.tableKey(), "idlog_device_id");
        query.select(select);
        query.orderByDesc("idlog_create_date");

        IPage<IotDeviceLog> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = IotDeviceLogService.getBean().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            IotDeviceLog entity = IotDeviceLogService.getBean().loadEntity(data);
            data.put("id", entity.getKey());
            //类型
            data.put("type", EnumIotDeviceLogType.getBean().get(entity.getIdlog_type()));
        }

        getR().setData("list", list);
        getR().setData("count", count);
        return getR();
    }
}
