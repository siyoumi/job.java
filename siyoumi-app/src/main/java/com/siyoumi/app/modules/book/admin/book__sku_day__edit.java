package com.siyoumi.app.modules.book.admin;

import com.siyoumi.app.entity.SysBook;
import com.siyoumi.app.entity.SysBookSku;
import com.siyoumi.app.entity.SysBookSkuDay;
import com.siyoumi.app.entity.SysStock;
import com.siyoumi.app.modules.book.service.SvcSysBook;
import com.siyoumi.app.modules.book.service.SvcSysBookSku;
import com.siyoumi.app.modules.book.service.SvcSysBookSkuDate;
import com.siyoumi.app.modules.book.vo.VaBookSkuDayBatch;
import com.siyoumi.app.service.SysStockService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/xadmin/book/book__sku_day__edit")
public class book__sku_day__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        InputData inputData = InputData.fromRequest();
        String skuId = inputData.input("sku_id");

        Map<String, Object> data = new HashMap<>();
        data.put("set_left", 0);

        data.put("set_before", 0);
        data.put("before_enable", 0);
        data.put("before_m", 0);

        data.put("set_append_max", 0);
        data.put("append_max", 0);

        if (isAdminEdit()) {
            setPageTitle("日期-操作");
            SysBookSkuDay entity = SvcSysBookSkuDate.getApp().loadEntity(getID());
            SysStock entityStock = SysStockService.getBean().getEntityBySrc(entity.getKey(), false);

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            dataAppend.put("before_enable", entity.getBday_before_enable());
            dataAppend.put("before_m", entity.getBday_before_time());
            dataAppend.put("append_max", entity.getBday_append_max());
            if (entityStock != null) {
                dataAppend.put("left_count", entityStock.getStock_count_left());
                dataAppend.put("left_count_old", entityStock.getStock_count_left());
            }
            dataAppend.put("date_begin", XDate.toDateString(entity.getBday_date()));
            dataAppend.put("date_end", XDate.toDateString(entity.getBday_date()));
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);

            data.put("set_left", 1);
            data.put("set_before", 1);
            data.put("set_append_max", 1);

            skuId = entity.getBday_sku_id();
        }
        data.put("sku_id", skuId);
        getR().setData("data", data);

        SysBookSku entitySku = SvcSysBookSku.getApp().first(skuId);
        if (entitySku == null) {
            return EnumSys.ERR_VAL.getR("时间段ID异常");
        }
        SysBook entityBook = SvcSysBook.getApp().first(entitySku.getBsku_book_id());

        setPageTitle(entitySku.getBsku_name(), "-日期-批量操作");
        //预约
        setPageInfo("book", entityBook);
        setPageInfo("sku", entitySku);

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaBookSkuDayBatch vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        if (!isAdminEdit()) {
            vo.setAcc_id(getAccId());
        } else {
        }

        return SvcSysBookSkuDate.getBean().batch(vo);
    }
}
