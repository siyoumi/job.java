package com.siyoumi.app.modules.website_fjciecc.vo;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaFjcieccSetting {
    @Size(max = 200)
    String abc_str_00;
    @Size(max = 200)
    String abc_str_01;
    String abc_txt_00;
    String abc_uix;
}
