package com.siyoumi.app.modules.account.service;

import com.siyoumi.app.entity.SysStore;
import com.siyoumi.app.modules.account.vo.VaSysStore;
import com.siyoumi.app.service.SysStoreService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SvcSysStore {
    static public SvcSysStore getBean() {
        return XSpringContext.getBean(SvcSysStore.class);
    }

    static public SysStoreService getApp() {
        return XSpringContext.getBean(SysStoreService.class);
    }

    public List<Map<String, Object>> getList(String accId) {
        String[] select = {
                "store_id",
                "store_name",
        };
        JoinWrapperPlus<SysStore> query = listQuery();
        query.eq("store_acc_id", accId);
        query.select(select);

        return getApp().getMaps(query);
    }

    public JoinWrapperPlus<SysStore> listQuery() {
        return listQuery(InputData.getIns());
    }

    public JoinWrapperPlus<SysStore> listQuery(InputData inputData) {
        String name = inputData.input("name");

        JoinWrapperPlus<SysStore> query = getApp().join();
        query.eq("store_x_id", XHttpContext.getX())
                .eq("store_del", 0);
        //排序
        query.orderByDesc("store_id");
        if (XStr.hasAnyText(name)) { //名称
            query.like("store_name", name);
        }

        return query;
    }

    public XReturn edit(VaSysStore vo) {
        List<String> ignoreField = new ArrayList<>();
        InputData inputData = InputData.getIns();

        SysStore entityStore = getApp().getEntity(vo.getStore_id());
        if (entityStore != null) {
            inputData.put("id", vo.getStore_id());
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, false, ignoreField);

            return r;
        });
    }
}
