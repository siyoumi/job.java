package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;

//标签
@Data
public class VaFksTag {
    private String ftag_uid;
    @Size(max = 50)
    @HasAnyText
    private String ftag_name;
    @HasAnyText
    private List<String> uids; //用户ID
    private Integer uid_set = 0;
}
