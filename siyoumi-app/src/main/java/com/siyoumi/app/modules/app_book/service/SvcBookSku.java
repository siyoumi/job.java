package com.siyoumi.app.modules.app_book.service;

import com.siyoumi.app.entity.BookSku;
import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.modules.app_book.vo.VaBookSku;
import com.siyoumi.app.modules.app_book.vo.VoBookSkuAudit;
import com.siyoumi.app.service.BookSkuService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

//
@Slf4j
@Service
public class SvcBookSku
        implements IWebService {
    static public SvcBookSku getBean() {
        return XSpringContext.getBean(SvcBookSku.class);
    }

    static public BookSkuService getApp() {
        return BookSkuService.getBean();
    }

    /**
     * 根据天数获取sku 金额
     *
     * @param spuId
     * @param day
     */
    public BookSku getEntitySku(String spuId, Integer day) {
        //spu 全部上架sku，日期从小到大排序
        JoinWrapperPlus<BookSku> query = listQuery(spuId);
        query.eq("bsku_enable", 1);
        query.orderByDesc("bsku_day");
        List<BookSku> listSku = getApp().get(query);
        if (listSku.isEmpty()) {
            return null;
        }

        BookSku entitySku = listSku.stream().filter(item -> item.getBsku_day() <= day).findFirst().orElse(null);
        if (entitySku == null) {
            //输入的日期大于所有配置的sku，拿最后一个
            //1,5,15,30 当输入日期大于30 直接拿30价格
            entitySku = listSku.get(listSku.size() - 1);
        }

        return entitySku;
    }

    public List<Map<String, Object>> getList(List<String> spuIds) {
        String[] select = {
                "bsku_id",
                "bsku_day",
                "bsku_spu_id",
                "bsku_day_price",
        };
        JoinWrapperPlus<BookSku> query = listQuery(spuIds);
        query.eq("bsku_enable", 1);
        query.orderByDesc("bsku_day");
        query.select(select);

        return getApp().getMaps(query);
    }

    public JoinWrapperPlus<BookSku> listQuery(String spuId) {
        return listQuery(List.of(spuId));
    }

    public JoinWrapperPlus<BookSku> listQuery(List<String> spuIds) {
        JoinWrapperPlus<BookSku> query = listQuery();
        query.in("bsku_spu_id", spuIds);
        return query;
    }

    public JoinWrapperPlus<BookSku> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<BookSku> listQuery(InputData inputData) {
        String spuId = inputData.input("spu_id");
        String id = inputData.input("id");

        JoinWrapperPlus<BookSku> query = getApp().join();
        query.eq("bsku_x_id", XHttpContext.getX())
                .eq("bsku_del", 0);
        if (XStr.hasAnyText(id)) {
            query.eq("bsku_id", id);
        } else {
            if (XStr.hasAnyText(spuId)) { //spu_id
                query.eq("bsku_spu_id", spuId);
            }
        }

        return query;
    }

    public XReturn save(InputData inputData, VaBookSku vo) {
        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminEdit()) {
            ignoreField.add("bsku_spu_id");
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, true, ignoreField);
            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        JoinWrapperPlus<BookSku> query = listQuery();
        query.in(BookSku.tableKey(), ids);
        List<BookSku> list = getApp().get(query);
        for (BookSku entity : list) {
            if (entity.getBsku_enable() == 1) {
                return XReturn.getR(20108, entity.getBsku_day() + "晚，已上架不能删除");
            }
        }

        getApp().delete(ids);

        return r;
    }


    /**
     * 审核
     *
     * @param vo
     */
    @Transactional(rollbackFor = Exception.class)
    public XReturn audit(VoBookSkuAudit vo) {
        List<BookSku> list = getApp().get(vo.getIds());
        if (list.isEmpty()) {
            return EnumSys.OK.getR();
        }
        //检查上下架
        BookSpu entitySpu = SvcBookSpu.getApp().loadEntity(list.get(0).getBsku_spu_id());
        XValidator.err(SvcBookSpu.getBean().canEdit(entitySpu));

        for (BookSku entity : list) {
            if (Objects.equals(entity.getBsku_enable(), vo.getEnable())) {
                continue;
            }
            if (vo.getEnable() == 1) {
                if (entity.getBsku_day_price().compareTo(BigDecimal.ZERO) <= 0) {
                    XValidator.err(EnumSys.ERR_VAL.getR(entity.skuName() + "：价格为0，不能上架"));
                }
            }

            BookSku entityUpdate = new BookSku();
            entityUpdate.setBsku_id(entity.getKey());
            entityUpdate.setBsku_enable(vo.getEnable());
            getApp().updateById(entityUpdate);
        }
        return EnumSys.OK.getR();
    }
}
