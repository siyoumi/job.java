package com.siyoumi.app.modules.app_fks.entity;

import com.siyoumi.component.XEnumBase;


public class EnumFksMsgType
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "词条");
        put(1, "规划");
        put(2, "成果");
    }
}
