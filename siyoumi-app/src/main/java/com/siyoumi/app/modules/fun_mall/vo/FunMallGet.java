package com.siyoumi.app.modules.fun_mall.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//兑换
@Data
public class FunMallGet {
    @HasAnyText
    String goods_id;
    String openid;
}
