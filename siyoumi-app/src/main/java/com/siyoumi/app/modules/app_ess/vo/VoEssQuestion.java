package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;

//题目
@Data
public class VoEssQuestion {
    private String etqu_acc_id;

    @HasAnyText
    private String etqu_module_id;
    @HasAnyText
    private Integer etqu_type;
    @HasAnyText
    private Integer etqu_question_type;
    @HasAnyText
    @Size(max = 500)
    private String etqu_question;
    @HasAnyText
    private Integer etqu_level;
    @HasAnyText
    private Long etqu_fun;
    private String etqu_question_item;
    private String etqu_answer;

    @HasAnyText
    private List<String> question_item; //题目选项
    @HasAnyText
    private List<Integer> answer; //答案
}
