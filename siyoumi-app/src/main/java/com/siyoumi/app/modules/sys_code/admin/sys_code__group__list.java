package com.siyoumi.app.modules.sys_code.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/sys_code/sys_code__group__list")
public class sys_code__group__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("码库-列表");

        InputData inputData = InputData.fromRequest();
        String compKw = inputData.input("compKw");

        SysAbcService svcAbc = SysAbcService.getBean();

        JoinWrapperPlus<SysAbc> query = svcAbc.listQuery(XHttpContext.getAppId(), false);
        query.eq("abc_table", "sys_code_group");
        if (XStr.hasAnyText(compKw)) {
            query.like("abc_name", compKw);
        }

        IPage<SysAbc> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = svcAbc.getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            SysAbc entityAbc = XBean.fromMap(data, SysAbc.class);
            data.put("id", entityAbc.getKey());
            return data;
        }).collect(Collectors.toList());

        getR().setData("list", listData);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        SysAbcService svcAbc = SysAbcService.getBean();
        return svcAbc.deleteLogic(Arrays.asList(ids), "sys_code_group");
    }
}
