package com.siyoumi.app.modules.app_book.admin;

import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.entity.BookSpuDay;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.modules.app_book.vo.SpuDayData;
import com.siyoumi.app.service.BookSpuDayService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__spu_day__list")
public class app_book__spu_day__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        String spuId = input("spu_id");
        String month = input("month");
        if (XStr.isNullOrEmpty(month)) {
            month = XDate.format(XDate.now(), "yyyy-MM");
        }
        setPageFrom("month", month);


        XValidator.isNullOrEmpty(spuId, "miss spu_id");

        BookSpu entitySpu = SvcBookSpu.getApp().getEntity(spuId);
        XValidator.isNull(entitySpu, "spu_id error");

        setPageTitle(entitySpu.getBspu_name(), "-每日库存&加价");

        InputData inputData = InputData.fromRequest();

        LocalDateTime b = XDate.parse(month + "-01");
        LocalDateTime e = b.plusMonths(1).minusDays(1);
        //1个月日期列表
        //List<LocalDateTime> dates = XDate.getDateList(b, e, null);
        //数据库加价列表
        List<SpuDayData> listSpuDay = SvcBookSpu.getBean().listDay(entitySpu, b, e);
        Map<String, Object> mapDateList = new LinkedHashMap<>();
        for (SpuDayData entity : listSpuDay) {
            mapDateList.put(XDate.toDateString(entity.getDate()), entity);
        }

        getR().setData("list", mapDateList);
        getR().setData("count", listSpuDay.size());

        return getR();
    }
}
