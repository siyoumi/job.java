package com.siyoumi.app.modules.sh.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ShListData {
    String id;
    @HasAnyText
    String code; //命令
    @HasAnyText
    LocalDateTime run_time; //运行时间
    Integer run = 0; //是否已运行
}
