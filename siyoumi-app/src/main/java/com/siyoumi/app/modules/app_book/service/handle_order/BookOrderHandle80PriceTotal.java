package com.siyoumi.app.modules.app_book.service.handle_order;

import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.app.entity.BookSetDay;
import com.siyoumi.app.entity.BookStore;
import com.siyoumi.app.modules.app_book.service.BookOrderHandleAbs;
import com.siyoumi.app.modules.app_book.service.SvcBookSet;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.component.XApp;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * 计算订单金额
 */
public class BookOrderHandle80PriceTotal
        extends BookOrderHandleAbs
        implements IWebService {
    @Override
    public XReturn orderBeforeCheck() {
        return EnumSys.OK.getR();
    }

    @Override
    public String getTitle() {
        return "计算订单金额";
    }

    @Override
    public XReturn handle(BookOrder entityOrder) {
        BigDecimal priceSku = entityOrder.getBorder_sku_day_price()
                .multiply(BigDecimal.valueOf(entityOrder.getBorder_sku_user_count())) //房间人数
                .multiply(BigDecimal.valueOf(entityOrder.getBorder_day_final())) //晚数
                .multiply(BigDecimal.valueOf(entityOrder.getBorder_spu_count()))//房间数
                ;
        priceSku = XApp.toDecimalPay(priceSku);
        LogPipeLine.getTl().setLogMsgFormat("房间基础总金额：{0}", priceSku.toString());
        entityOrder.setBorder_price_sku(priceSku);

        BigDecimal priceSkuAppendDay = getListDay().stream()
                .filter(item -> item.getAdd_price().compareTo(BigDecimal.ZERO) > 0)
                .map(item -> item.getAdd_price())
                .reduce(BigDecimal.ZERO, BigDecimal::add); //加价金额
        BigDecimal priceSkuAppendTotal = priceSkuAppendDay
                //.multiply(BigDecimal.valueOf(entityOrder.getBorder_day_final())) //晚数
                .multiply(BigDecimal.valueOf(entityOrder.getBorder_spu_count()))//房间数
                ;
        priceSkuAppendTotal = XApp.toDecimalPay(priceSkuAppendTotal);
        LogPipeLine.getTl().setLogMsgFormat("房间加价总金额：{0}", priceSkuAppendTotal.toString());
        entityOrder.setBorder_price_sku_append(priceSkuAppendTotal);

        BigDecimal priceSet = entityOrder.getBorder_set_day_price()
                .multiply(BigDecimal.valueOf(entityOrder.getBorder_set_user_count())) //套餐人数
                .multiply(BigDecimal.valueOf(entityOrder.getBorder_day_final())) //晚数
                //.multiply(BigDecimal.valueOf(entityOrder.getBorder_spu_count()))//房间数
                ;
        priceSet = XApp.toDecimalPay(priceSet);
        LogPipeLine.getTl().setLogMsgFormat("套餐基础总金额：{0}", priceSet.toString());
        entityOrder.setBorder_price_set(priceSet);

        BigDecimal priceSetAppendDay = SvcBookSet.getBean().dayPriceTotal(entityOrder.getBorder_set_id(), entityOrder.getBorder_date_begin(), entityOrder.getBorder_date_end_final());
        BigDecimal priceSetAppendTotal = priceSetAppendDay
                .multiply(BigDecimal.valueOf(entityOrder.getBorder_set_user_count())) //套餐人数
                //.multiply(BigDecimal.valueOf(entityOrder.getBorder_spu_count()))//房间数
                ;
        priceSetAppendTotal = XApp.toDecimalPay(priceSetAppendTotal);
        entityOrder.setBorder_price_set_append(priceSetAppendTotal);
        LogPipeLine.getTl().setLogMsgFormat("套餐加价总金额：{0}", priceSetAppendTotal.toString());
        //原订单金额
        entityOrder.setBorder_price_original(entityOrder.matchOriginalPrice());
        //最终金额
        entityOrder.setBorder_price_final(entityOrder.matchFinalPrice());
        entityOrder.setBorder_price_share_final(entityOrder.getBorder_price_original());
        LogPipeLine.getTl().setLogMsgFormat("最终订单金额：{0}", entityOrder.getBorder_price_final().toString());
        //支付金额
        BigDecimal payPrice = entityOrder.getBorder_price_final();
        if (getVo().getPay_deposit() == 1) {
            //开启订金支付
            BookStore entityStore = SvcBookStore.getApp().getEntity(entityOrder.getBorder_store_id());
            payPrice = payPrice.multiply(XApp.decimal100(entityStore.getBstore_deposit_rate()));
            payPrice = XApp.toDecimalPay(payPrice);
        }
        entityOrder.setBorder_price_pay(payPrice);
        LogPipeLine.getTl().setLogMsgFormat("支付金额：{0}", entityOrder.getBorder_price_pay().toString());

        return EnumSys.OK.getR();
    }
}

