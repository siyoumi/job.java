package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//价格上下架
@Data
public class VoBookSkuAudit {
    @HasAnyText
    private List<String> ids;
    @HasAnyText
    @EqualsValues(vals = {"0", "1"})
    private Integer enable;
}
