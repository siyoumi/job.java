package com.siyoumi.app.modules.prize.service;

import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.app.entity.SysPrizeSetChou;
import com.siyoumi.app.entity.SysStock;
import com.siyoumi.app.modules.prize.entity.EnumPrizeType;
import com.siyoumi.app.modules.prize.entity.EnumPrizeUseInfoOption;
import com.siyoumi.app.modules.prize.vo.VaSysPrizeSet;
import com.siyoumi.app.service.SysPrizeSetChouService;
import com.siyoumi.app.service.SysStockService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;

import java.util.*;

@Data
@Slf4j
public class SvcSysPrizeSetAdmin {
    Boolean setChou = false; //是否需要保存抽奖
    Boolean setStock = false; //是否需要设置库存

    static public SvcSysPrizeSetAdmin getIns() {
        return new SvcSysPrizeSetAdmin();
    }

    static public SvcSysPrizeSetAdmin getIns(Boolean setStock) {
        return getIns(setStock, false);
    }

    static public SvcSysPrizeSetAdmin getIns(Boolean setStock, Boolean setChou) {
        SvcSysPrizeSetAdmin app = getIns();
        app.setSetChou(setChou);
        app.setSetStock(setStock);

        return app;
    }


    public void editAfter(InputData inputData, XReturn r, String idSrc) {
        Map<String, Object> data = (Map<String, Object>) r.get("data");
        if (data == null) {
            data = new HashMap<>();
        }

        data.put("pset_app_id", XHttpContext.getAppId());
        data.put("pset_type", "");
        data.put("pset_use_type", 2);
        data.put("pset_id_src", idSrc);
        data.put("pset_int_00", 0);
        data.put("pset_int_01", 0);
        data.put("pset_vaild_date_type", 1);
        data.put("pset_vaild_date_win_h", 0);
        data.put("pset_key", XApp.getStrID());
        data.put("pset_use_info_option", List.of());
        if (getSetStock()) {
            // 为了避免，错误修改，库存会赋到新值
            data.put("stock_count_use", 0);
            data.put("stock_count_left", 0);
            data.put("stock_count_left_old", 0);
        }
        // 通知
        data.put("pset_send_type", 0);

        if (getSetChou()) {
            data.put("pschou_win_rate", 0);
        }
        if (inputData.isAdminEdit()) {
            SysPrizeSet entity = SvcSysPrizeSet.getBean().getEntityByIdSrc(idSrc);
            data.putAll(entity.toMap());

            Map<String, Object> dataAppend = new LinkedHashMap<>();
//            dataAppend.put("id", entity.getKey());

            List<String> userInfoOption = new ArrayList<>();
            if (XStr.hasAnyText(entity.getPset_use_info_option())) {
                //填写项
                userInfoOption = List.of(entity.getPset_use_info_option().split(","));
            }
            dataAppend.put("pset_use_info_option", userInfoOption);

            if (getSetStock()) {
                SysStock entityStock = SysStockService.getBean().getEntityBySrc(entity.getPset_id_src(), false);
                if (entityStock != null) {
                    dataAppend.put("stock_count_use", entityStock.getStock_count_use());
                    dataAppend.put("stock_count_left", entityStock.getStock_count_left());
                    dataAppend.put("stock_count_left_old", entityStock.getStock_count_left());
                }
            }

            if (getSetChou()) {
                SysPrizeSetChou entityPrizeChou = SysPrizeSetChouService.getBean().getEntity(entity.getKey());
                if (entityPrizeChou != null) {
                    dataAppend.putAll(entityPrizeChou.toMap());
                }
            }
            //合并
            data.putAll(dataAppend);
        }
        r.setData("data", data);


        Map<String, Object> mapPageInfo = r.getData("page_info");

        //奖品类型
        LinkedHashMap<String, String> prizeType = IEnum.toMap(EnumPrizeType.class);
        mapPageInfo.put("prize_type", prizeType);
        mapPageInfo.put("is_wxapp", XHttpContext.getXConfig().wxApp());
        //填写资料
        LinkedHashMap<String, String> prizeUseReg = IEnum.toMap(EnumPrizeUseInfoOption.class);
        mapPageInfo.put("prize_use_info_option", prizeUseReg);

        mapPageInfo.put("set_stock", getSetStock());
        mapPageInfo.put("set_chou", getSetChou());
    }

    /**
     * 保存验证
     *
     * @param result
     * @param vo
     */
    public void editSaveValidator(BindingResult result, VaSysPrizeSet vo) {
        if ("obj".equals(vo.getPset_type()) || "".equals(vo.getPset_type())) {
            switch (vo.getPset_use_type()) {
                case 0: //密码核销
                    if (XStr.isNullOrEmpty(vo.getPset_use_str_00())) {
                        result.addError(XValidator.getErr("pset_use_str_00", "请输入核销密码"));
                    }
                    break;
                case 1: //表单核销
                    break;
                case 10: //填写信息核销
                    if (XStr.isNullOrEmpty(vo.getPset_use_info_option())) {
                        result.addError(XValidator.getErr("pset_use_info_option", "至少勾选1项填写"));
                    }
                    break;
            }
        }
    }

    /**
     * 发奖配置-保存
     *
     * @param idSrc
     * @param vo
     */
    @SneakyThrows
    public void editSave(String idSrc, String appId, Object vo) {
        VaSysPrizeSet prizeSetVo = (VaSysPrizeSet) vo;
        //发奖配置
        SysPrizeSet entityPrizeSet = SvcSysPrizeSet.getBean().getEntityByIdSrc(idSrc);
        SysPrizeSet entityPrizeSetUpdate = SvcSysPrizeSet.getApp().loadEntity(XBean.toMap(prizeSetVo));
        if (entityPrizeSet == null) {
            entityPrizeSetUpdate.setPset_x_id(XHttpContext.getX());
            entityPrizeSetUpdate.setPset_app_id(appId);
            entityPrizeSetUpdate.setPset_id_src(idSrc);
            entityPrizeSetUpdate.setAutoID();
        }
        SvcSysPrizeSet.getApp().saveOrUpdatePassEqualField(entityPrizeSet, entityPrizeSetUpdate);

        if (getSetChou()) {
            //抽奖配置
            SysPrizeSetChou entityChou = SysPrizeSetChouService.getBean().getEntity(entityPrizeSet.getPset_id());
            SysPrizeSetChou entityChouUpdate = SysPrizeSetChouService.getBean().loadEntity(XBean.toMap(prizeSetVo));
            if (entityChou == null) {
                entityChouUpdate.setPschou_id(entityPrizeSet.getPset_id());
                entityChouUpdate.setPschou_x_id(XHttpContext.getX());
                entityChouUpdate.setPschou_id_src(entityPrizeSet.getPset_id_src());
            }
            SysPrizeSetChouService.getBean().saveOrUpdatePassEqualField(entityChou, entityChouUpdate);
        }

        if (getSetStock()) {
            //库存配置
            SysStock entityStock = SysStockService.getBean().getEntityBySrc(idSrc);
            if (entityStock == null) {
                entityStock = new SysStock();
                entityStock.setStock_x_id(XHttpContext.getX());
                entityStock.setStock_app_id(appId);
                entityStock.setStock_id_src(idSrc);
                entityStock.setAutoID();
                SysStockService.getBean().save(entityStock);
            }
            //修改余量
            SysStockService.getBean().updateLeft(idSrc, prizeSetVo.getStock_count_left(), prizeSetVo.getStock_count_left_old());
        }
    }
}
