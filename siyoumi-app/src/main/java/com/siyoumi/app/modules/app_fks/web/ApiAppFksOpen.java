package com.siyoumi.app.modules.app_fks.web;

import com.siyoumi.app.entity.FksUser;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_fks.service.SvcFksDepartment;
import com.siyoumi.app.modules.app_fks.service.SvcFksPosition;
import com.siyoumi.app.modules.app_fks.service.SvcFksUser;
import com.siyoumi.app.modules.app_fks.vo.VaFksUser;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.config.SysConfig;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

//公共接口
@Slf4j
@RestController
@RequestMapping("/wxapp/app_fks/api_open")
public class ApiAppFksOpen
        extends WxAppApiController {

    //初始化
    @GetMapping("init")
    public XReturn init() {
        SysUser entityUser = SvcSysUser.getBean().getUserByOpenid(getOpenid());
        Map<String, Object> mapUser = XBean.toMap(entityUser, new String[]{
                "user_id",
                "user_name",
                "user_phone",
                "user_enable",
        });
        getR().put("user", mapUser);
        getR().put("department", SvcFksDepartment.getBean().getList());
        getR().put("position", SvcFksPosition.getBean().getList());

        return getR();
    }

    /**
     * 用户注册
     */
    @PostMapping("user_reg")
    public XReturn userReg(@Validated @RequestBody VaFksUser vo, BindingResult result) {
        if (XStr.isNullOrEmpty(vo.getUser_pwd())) {
            result.addError(XValidator.getErr("user_pwd", "缺少密码"));
        }
        //统一验证
        XValidator.getResult(result, true, true);
        String phoneEnc = XApp.encPwd(XHttpContext.getX(), vo.getUser_phone());
        if (!phoneEnc.equals(vo.getUser_phone_enc())) {
            return EnumSys.ERR_VAL.getR("手机号非法");
        }

        JoinWrapperPlus<FksUser> query = SvcFksUser.getBean().listQuery();
        query.eq("user_phone", vo.getUser_phone());
        FksUser entity = SvcFksUser.getApp().first(query);
        if (entity != null) {
            result.addError(XValidator.getErr("user_phone", "手机号已注册"));
        }
        XValidator.getResult(result, true, true);
        //
        vo.setFuser_id(XApp.getStrID());
        vo.setUser_openid(getOpenid());
        //
        InputData inputData = InputData.fromRequest();
        return SvcFksUser.getBean().edit(inputData, vo);
    }
}
