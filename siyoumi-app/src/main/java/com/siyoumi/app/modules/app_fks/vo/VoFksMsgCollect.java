package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//知识收藏
@Data
public class VoFksMsgCollect {
    @HasAnyText(message = "miss msg_id")
    private String msg_id;
    @HasAnyText
    private Integer collect; //【0】取消收藏；【1】收藏
    private String uid;
}
