package com.siyoumi.app.modules.money.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.math.BigDecimal;

//下单
@Data
public class MoneyOrderVo {
    String id;
    @HasAnyText(message = "miss type")
    Integer type; //0: 默认金额；1: 自定义金额
    BigDecimal pay_price; //自定义金额
}
