package com.siyoumi.app.modules.app_fks.entity;

import com.siyoumi.component.XEnumBase;


public class EnumShareRoomType
        extends XEnumBase<String> {
    @Override
    protected void initKV() {
        put("common", "公司");
        put("department", "部门");
        put("person", "个人");
    }
}
