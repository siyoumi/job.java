package com.siyoumi.app.modules.prize.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PrizeDeliverVo {
    @NotBlank
    String prize_id;
    @NotBlank
    String deliver_no;
}
