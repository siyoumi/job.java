package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;

//文件移动
@Data
public class FksFileMove {
    @HasAnyText
    private String file_ids; //文件ids
    private String folder_id;
}
