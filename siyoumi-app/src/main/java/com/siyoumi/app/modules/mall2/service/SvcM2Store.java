package com.siyoumi.app.modules.mall2.service;

import com.siyoumi.app.entity.M2Store;
import com.siyoumi.app.entity.base.M2StoreBase;
import com.siyoumi.app.modules.mall2.entity.VaM2Store;
import com.siyoumi.app.service.M2StoreService;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SvcM2Store
        implements IWebService {
    static public SvcM2Store getBean() {
        return XSpringContext.getBean(SvcM2Store.class);
    }

    static public M2StoreService getApp() {
        return M2StoreService.getBean();
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<M2Store> listQuery(InputData inputData) {
        String name = inputData.input("name");

        JoinWrapperPlus<M2Store> query = getApp().join();
        query.eq("mstore_x_id", XHttpContext.getX());
        //排序
        query.orderByAsc("mstore_id")
                .orderByDesc("mstore_create_date");
        if (XStr.hasAnyText(name)) { //名称
            query.like("mstore_name", name);
        }

        return query;
    }

    /**
     * 编辑
     *
     * @param inputData
     * @param vo
     */
    @SneakyThrows
    public XReturn edit(InputData inputData, VaM2Store vo) {

        XReturn r = getApp().saveEntity(inputData, vo, true, null);
        return r;
    }

    /**
     * 删除
     *
     * @return
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        InputData inputData = InputData.getIns();
        JoinWrapperPlus<M2Store> query = listQuery(inputData);
        query.in(M2StoreBase.F.mstore_id, ids);

        List<M2Store> list = getApp().get(query);
        for (M2Store entity : list) {
            getApp().delete(entity.getKey());
        }

        return r;
    }
}
