package com.siyoumi.app.modules.iot.service.device_action;

import com.siyoumi.app.entity.IotDevice;
import com.siyoumi.app.modules.iot.entity.IotPinCode;
import com.siyoumi.app.modules.iot.service.DeviceActionHandle;
import com.siyoumi.app.modules.iot.service.SvcIotDevice;
import com.siyoumi.app.netty.NettyMqttUtil;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DeviceActionHandlePowerOn
        extends DeviceActionHandle {
    @Override
    public XReturn send(IotDevice entityDevice) {
        IotPinCode code = IotPinCode.of(13, 2);

        XReturn r = code.toR();
        String msg = XStr.toJsonStr(r);
        log.info(msg);
        String mqttQueueId = SvcIotDevice.getBean().getMqttQueueId(entityDevice);
        Channel channel = NettyMqttUtil.getMapChannel().get(mqttQueueId);
        if (channel == null) {
            return XReturn.getR(20026, "设备不在线");
        }
        NettyMqttUtil.sendMessage(channel
                , SvcIotDevice.getBean().getTopic()
                , msg);

        //记录日志
        return r;
    }
}
