package com.siyoumi.app.modules.prize.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.app.entity.SysPrizeSetChou;
import com.siyoumi.app.modules.prize.entity.EnumPrizeType;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSet;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/prize/prize__set__list")
public class prize__set__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("奖品-列表");

        String appId = XHttpContext.getAppId();

        InputData inputData = InputData.fromRequest();
        String idsrc = inputData.input("id_src");

        JoinWrapperPlus<SysPrizeSet> query = SvcSysPrizeSet.getBean().listQuery(inputData);
        query.select("t_s_prize_set.*", "stock_count_left", "stock_count_use", "t_s_prize_set_chou.*");
        query.eq("pset_app_id", appId);

        IPage<SysPrizeSet> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcSysPrizeSet.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            SysPrizeSet entitySysPrizeSet = XBean.fromMap(data, SysPrizeSet.class);
            data.put("id", entitySysPrizeSet.getKey());

            //类型
            String prizeType = IEnum.getEnmuVal(EnumPrizeType.class, entitySysPrizeSet.getPset_type());
            data.put("type", prizeType);

            return data;
        }).collect(Collectors.toList());

        getR().setData("list", listData);
        getR().setData("count", count);

        setPageFrom("id_src", idsrc);
        setPageFrom("app_id", appId);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        SvcSysPrizeSet app = SvcSysPrizeSet.getBean();
        return app.delete(Arrays.asList(ids));
    }
}
