package com.siyoumi.app.modules.fun_win.vo;

import com.siyoumi.app.modules.prize.vo.VaSysPrizeSet;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

//商品
@Data
public class VaFunWinSession
        extends VaSysPrizeSet {
    //    @HasAnyText
//    @Size(max = 50)
    private String fwins_name;
    private LocalDateTime fwins_begin_date;
    @HasAnyText
    private LocalDateTime fwins_end_date;
    @HasAnyText
    private Long fwins_fun_num;
    @HasAnyText
    private Long fwins_prize_total;
    private Long fwins_user_total;
    @HasAnyText
    private LocalDateTime fwins_win_date;
    private Integer fwins_order;

    private String fwins_acc_id;
}
