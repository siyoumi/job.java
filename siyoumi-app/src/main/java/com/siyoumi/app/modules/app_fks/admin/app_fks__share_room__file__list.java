package com.siyoumi.app.modules.app_fks.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FksCity;
import com.siyoumi.app.entity.FksFile;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_fks.entity.EnumFksFileDataType;
import com.siyoumi.app.modules.app_fks.entity.EnumShareRoomType;
import com.siyoumi.app.modules.app_fks.service.SvcFksCtiy;
import com.siyoumi.app.modules.app_fks.service.SvcFksFile;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__share_room__file__list")
public class app_fks__share_room__file__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        InputData inputData = InputData.fromRequest();
        String folderId = inputData.input("folder_id", "");
        String typeId = inputData.input("type_id", "");

        String folderName = "";
        if (XStr.hasAnyText(folderId)) { //目录名称
            FksFile entityFolder = SvcFksFile.getApp().first(folderId);
            folderName = entityFolder.getFfile_name();

            typeId = entityFolder.getFfile_type_id();
            setPageFrom("type_id", typeId);
            inputData.put("type_id", typeId);
        }
        if (XStr.isNullOrEmpty(typeId)) {
            return EnumSys.MISS_VAL.getR("miss type_id");
        }

        EnumShareRoomType enumShareRoomType = XEnumBase.of(EnumShareRoomType.class);
        String typeIdName = enumShareRoomType.get(typeId);
        String titlePrefix = XStr.format("{0}空间{1}", typeIdName
                , XStr.hasAnyText(folderName) ? "-" + folderName : ""
        );

        setPageTitle("文件列表");
        if (XStr.hasAnyText(titlePrefix)) {
            setPageTitle(titlePrefix, "-文件列表");
        }

        String[] select = {
                "ffile_id",
                "ffile_folder",
                "ffile_folder_id",
                "ffile_data_type",
                "ffile_name",
                "ffile_create_date",
                "ffile_path",
                "ffile_file_url",
                "ffile_file_ext",
                "ffile_order",
                "user_name",
        };
        JoinWrapperPlus<FksFile> query = SvcFksFile.getBean().listQuery(1, inputData);
        query.join(SysUser.table(), SysUser.tableKey(), "ffile_uid");
        query.select(select);
        query.orderByDesc("ffile_folder")
                .orderByAsc("ffile_id");

        IPage<FksFile> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcFksFile.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();
        for (Map<String, Object> item : list) {
            FksFile entity = XBean.fromMap(item, FksFile.class);
            item.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);

        setPageFrom("folder_id", folderId);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcFksFile.getBean().delete(getIds());
    }
}
