package com.siyoumi.app.modules.wifi.web;

import com.siyoumi.app.modules.wifi.service.SvcWifi;
import com.siyoumi.app.modules.wifi.vo.VaWifi;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.ApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/wxapp/wifi/api")
public class wifi_api
        extends ApiController {
    //商家信息
    @GetMapping("wifi_info")
    public XReturn wifiInfo() {
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }

        return SvcWifi.getBean().info(getID());
    }

    //商家信息
    @PostMapping("wifi_save")
    public XReturn wifiSave(@Validated VaWifi vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        SysAbcService svcAbc = SysAbcService.getBean();

        vo.setAbc_table("wifi");
        vo.setAbc_app_id(XHttpContext.getAppId());

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
            ignoreField.add("abc_app_id");
        }

        InputData inputData = InputData.fromRequest();

        return XApp.getTransaction().execute(status -> {
            return svcAbc.saveEntity(inputData, vo, true, ignoreField);
        });
    }
}
