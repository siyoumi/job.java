package com.siyoumi.app.modules.app_fks.service;

import com.siyoumi.app.modules.app_fks.service.file_auth.FksFileAuthHandleDel;
import com.siyoumi.app.modules.app_fks.service.file_auth.FksFileAuthHandleLook;
import com.siyoumi.app.modules.app_fks.service.file_auth.FksFileAuthHandleMove;
import com.siyoumi.app.modules.app_fks.service.file_auth.FksFileAuthHandleRename;
import com.siyoumi.app.modules.app_fks.vo.FksFileAuthHandleData;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;

abstract public class FksFileAuthHandle {
    public static FksFileAuthHandle of(String type) {
        FksFileAuthHandle app = null;
        switch (type) {
            case "del":
                app = new FksFileAuthHandleDel();
                break;
            case "move":
                app = new FksFileAuthHandleMove();
                break;
            case "look":
                app = new FksFileAuthHandleLook();
                break;
            case "rename":
                app = new FksFileAuthHandleRename();
                break;
            default:
                XValidator.err(EnumSys.SYS.getR("权限类型异常，" + type));
        }
        return app;
    }

    abstract protected XReturn check(FksFileAuthHandleData data);

    public XReturn handle(FksFileAuthHandleData data) {
        //权限判断
        if (data.getEntityFile().getFfile_uid().equals(data.getEntityUser().getKey())) {
            return EnumSys.OK.getR();
        }

        Integer positionLevel = SvcFksPosition.getBean().getPositionLevel(data.getEntityUser().getFuser_position_id());
        if (positionLevel == 0) { //顶级
            return EnumSys.OK.getR();
        }

        return check(data);
    }
}
