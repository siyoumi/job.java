package com.siyoumi.app.modules.mall2.vo;

import com.siyoumi.app.entity.M2Order;
import com.siyoumi.app.entity.M2OrderItem;
import lombok.Data;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

//订单列表接口
@Data
public class OrderListVo {
    String morder_id;
    String mstore_name;
    //pay_order
    Integer payorder_pay_ok;
    LocalDateTime payorder_pay_ok_date;
    LocalDateTime payorder_expire_time;
    Integer payorder_expire_handle;

    //order
    M2Order entity_order;
    List<M2OrderItem> items;

    /**
     * 订单剩余过期时间（秒）
     */
    public long getExpireTimeLeftS() {
        if (getEntity_order().getMorder_pay() == 1) {
            return 0;
        }
        if (getPayorder_expire_handle() == 1) {
            return 0;
        }

        Duration between = Duration.between(LocalDateTime.now(), getPayorder_expire_time());
        long s = between.getSeconds();
        if (s < 0) {
            s = 0;
        }

        return s;
    }
}
