package com.siyoumi.app.modules.fun_mall.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FunMallGroup;
import com.siyoumi.app.entity.FunMallPrize;
import com.siyoumi.app.entity.SysStock;
import com.siyoumi.app.modules.fun_mall.service.SvcFunMall;
import com.siyoumi.app.modules.fun_mall.service.SvcFunMallGroup;
import com.siyoumi.app.modules.fun_mall.service.SvcFunMallPrize;
import com.siyoumi.app.modules.fun_mall.vo.FunMallGet;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wxapp/fun_mall/api")
public class api_fun_mall
        extends WxAppApiController {
    /**
     * 初始化
     */
    @GetMapping("init")
    public XReturn init() {
        //设置
        Map<String, Object> mapSetting = SvcFunMall.getBean().getSetting();
        getR().setData("setting", mapSetting);

        //分组列表
        JoinWrapperPlus<FunMallGroup> queryGroup = SvcFunMallGroup.getBean().listQuery();
        queryGroup.select("fgroup_id", "fgroup_name", "fgroup_pic");
        List<FunMallGroup> listGroup = SvcFunMallGroup.getApp().get(queryGroup);
        getR().setData("group", listGroup);

        return getR();
    }

    /**
     * 商品列表
     */
    @GetMapping("goods_list")
    public XReturn goodsList() {
        InputData inputData = InputData.fromRequest();
        String groupId = inputData.input("group_id");

        IPage<FunMallPrize> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        JoinWrapperPlus<FunMallPrize> query = SvcFunMallPrize.getBean().listQuery(inputData);
        //list
        IPage<Map<String, Object>> pageData = SvcFunMallPrize.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();

        for (Map<String, Object> data : list) {
            FunMallPrize entity = SvcFunMallPrize.getApp().loadEntity(data);
            data.put("id", entity.getKey());
        }

        XReturn r = XReturn.getR(0);

        if (XStr.hasAnyText(groupId)) {
            //存在分组，输出分组信息
            FunMallGroup entityGroup = SvcFunMallGroup.getApp().loadEntity(groupId);
            if (entityGroup == null) {
                return EnumSys.ERR_VAL.getR("分组ID异常");
            }

            Map<String, Object> mapGroup = XBean.toMap(entityGroup, new String[]{
                    "fgroup_id",
                    "fgroup_name",
                    "fgroup_pic",
            });
            r.setData("group", mapGroup);
        }
        r.setData("group_id", groupId);
        r.setData("list", list);

        return r;
    }

    @GetMapping("goods_info")
    public XReturn goodsInfo() {
        String[] select = {
                "pset_name",
                "pset_pic",
                "pset_use_type",
                "pset_desc",
                "fprize_id",
                "fprize_uix",
                "fprize_group_id",
                "fprize_get_total",
                "fprize_fun",
                "fprize_begin_date",
                "fprize_end_date",
                "fprize_shelve",
                "fprize_order",
                "stock_count_left",
                "stock_count_use",
        };
        JoinWrapperPlus<FunMallPrize> query = SvcFunMallPrize.getBean().listQuery();
        query.select(select);
        query.join(SysStock.table(), SysStock.tableKey(), FunMallPrize.tableKey());
        query.eq("fprize_id", getID());
        Map<String, Object> mapPrize = SvcFunMallPrize.getApp().firstMap(query);
        if (mapPrize == null) {
            return EnumSys.ERR_VAL.getR("id error");
        }

        XReturn r = XReturn.getR(0);
        r.setData("goods", mapPrize);

        //能否兑换
        FunMallGet getVo = new FunMallGet();
        getVo.setGoods_id(getID());
        getVo.setOpenid(getOpenid());
        r.setData("can_get", SvcFunMallPrize.getBean().canGet(getVo));

        return r;
    }

    //兑换
    @GetMapping("goods_get")
    public XReturn get(@Validated FunMallGet vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        return SvcFunMallPrize.getBean().get(vo);
    }
}
