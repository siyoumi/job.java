package com.siyoumi.app.modules.website_fjciecc.service;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.entity.SysItem;
import com.siyoumi.app.modules.website_fjciecc.entity.WebSiteFjcieccSettingTxt00;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.app.service.SysItemService;
import com.siyoumi.component.XRedis;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SvcFjciecc {
    static public SvcFjciecc getBean() {
        return XSpringContext.getBean(SvcFjciecc.class);
    }

    static public String keySetting = "fjciecc_setting|map";

    /**
     * 基础配置
     */
    public Map getSetting() {
        String key = SysAbcService.getBean().getEntityCacheKey(keySetting);
        return XRedis.getBean().getAndSetData(key, k -> {
            SysAbc setting = SysAbcService.getBean().getEntityByUix("fjciecc_setting");
            if (setting == null) {
                XValidator.err(EnumSys.SYS.getR("基础配置未初始化"));
            }

            Map<String, Object> mapSetting = new HashMap<>();

            //轮播图
            SysItem entityItem = SysItemService.getBean().getEntityByIdSrc(setting.getAbc_str_00());
            if (entityItem != null) {
                mapSetting.put("banner", entityItem.commonData());
            }

            mapSetting.put("abc_str_01", setting.getAbc_str_01());

            //加入我们
            WebSiteFjcieccSettingTxt00 txt00 = WebSiteFjcieccSettingTxt00.getInstance(setting.getAbc_txt_00());
            mapSetting.put("abc_txt_00", txt00);

            return mapSetting;
        }, Map.class);
    }

    public JoinWrapperPlus<SysAbc> listQuery(InputData inputData, List<String> abcTable) {
        String name = inputData.input("name");
        String type = inputData.input("type");
        String id = inputData.getID();

        JoinWrapperPlus<SysAbc> query = SysAbcService.getBean().listQuery(XHttpContext.getAppId(), false);
        query.in("abc_table", abcTable);

        if (XStr.hasAnyText(id)) {
            query.eq("abc_id", id);
        } else {
            if (XStr.hasAnyText(name)) { //标题
                query.like("abc_name", name);
            }
            if (XStr.hasAnyText(type)) { //类型
                query.eq("abc_type", type);
            }
        }

        return query;
    }
}
