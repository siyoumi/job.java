package com.siyoumi.app.modules.lucky_num.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumGroup;
import com.siyoumi.app.service.LnumNumService;
import com.siyoumi.app.service.LnumWinService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/lucky_num/lucky_num__num__list")
public class lucky_num__num__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("号码列表");

        InputData inputData = InputData.fromRequest();
        String groupId = inputData.input("group_id");

        String[] select = {
                "lnum_id",
                "lnum_x_id",
                "lnum_create_date",
                "lnum_group_id",
                "lnum_wxuser_id",
                "lnum_key",
                "lnum_num",
                "wxuser_openid",
                "wxuser_nickname",
                "lngroup_name",
        };
        JoinWrapperPlus<LnumNum> query = SvcLNumGroup.getBean().numQuery(groupId, inputData);
        query.join(WxUser.table(), "wxuser_openid", "lnum_wxuser_id");
        query.join(LnumGroup.table(), LnumGroup.tableKey(), "lnum_group_id");
        query.select(select);
        query.orderByDesc("lnum_num");

        IPage<LnumNum> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = LnumNumService.getBean().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            LnumWin entity = LnumWinService.getBean().loadEntity(data);
            data.put("id", entity.getKey());
        }

        if (!isAdminExport()) {
            getR().setData("list", list);
            getR().setData("count", count);
        } else {
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("lnum_id", "ID");
            tableHead.put("lngroup_name", "号码组");
            tableHead.put("wxuser_openid", "openid");
            tableHead.put("wxuser_nickname", "微信名");
            tableHead.put("lnum_num", "号码");

            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }

        return getR();
    }

    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/del")
    public XReturn del() {
        return null;
    }
}
