package com.siyoumi.app.modules.app_fks.admin;

import com.siyoumi.app.entity.FksMsg;
import com.siyoumi.app.entity.FksPosition;
import com.siyoumi.app.modules.app_fks.service.SvcFksFile;
import com.siyoumi.app.modules.app_fks.service.SvcFksMsg;
import com.siyoumi.app.modules.app_fks.service.SvcFksPosition;
import com.siyoumi.app.modules.app_fks.vo.VaFksMsg;
import com.siyoumi.app.modules.app_fks.vo.VaFksPosition;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__msg__edit")
public class app_fks__msg__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("fmsg_order", 0);
        data.put("fmsg_type", XStr.toInt(input("type", "0")));
        if (isAdminEdit()) {
            FksMsg entity = SvcFksMsg.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        setPageInfo("file_accept", SvcFksFile.fileAccept().stream().collect(Collectors.joining(","))); //允许上传的文件格式

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated VaFksMsg vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        if (!isAdminEdit()) {
            vo.setFmsg_acc_id(getAccId());
        } else {
            vo.setFmsg_acc_id(null);
        }

        InputData inputData = InputData.fromRequest();
        return SvcFksMsg.getBean().edit(inputData, vo);
    }
}