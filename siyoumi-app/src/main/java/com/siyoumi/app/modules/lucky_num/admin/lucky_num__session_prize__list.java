package com.siyoumi.app.modules.lucky_num.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.LnumGroup;
import com.siyoumi.app.entity.LnumSession;
import com.siyoumi.app.entity.LnumSessionPrize;
import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumSessionPrize;
import com.siyoumi.app.modules.prize.entity.EnumPrizeType;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSet;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/lucky_num/lucky_num__session_prize__list")
public class lucky_num__session_prize__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("奖品期-奖品列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "lnsp_id",
                "lnsp_acc_id",
                "lnsp_group_id",
                "lnsp_session_id",
                "lnsp_name",
                "lnsp_pic",
                "lnsp_count",
                "lnsp_order",
                "pset_name",
                "pset_type",
                "pset_pic",
        };
        JoinWrapperPlus<LnumSessionPrize> query = SvcLNumSessionPrize.getBean().listQuery(inputData);
        query.join(SysPrizeSet.table(), "pset_id_src", LnumSessionPrize.tableKey());
        query.join(LnumGroup.table(), LnumGroup.tableKey(), "lnsp_group_id");
        query.join(LnumSession.table(), LnumSession.tableKey(), "lnsp_session_id");
        query.select(select);

        IPage<LnumSessionPrize> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcLNumSessionPrize.getApp().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            LnumSessionPrize entity = SvcLNumSessionPrize.getApp().loadEntity(data);
            data.put("id", entity.getKey());

            //奖品类型
            SysPrizeSet entityPrizeSet = SvcSysPrizeSet.getApp().loadEntity(data);
            String prizeType = IEnum.getEnmuVal(EnumPrizeType.class, entityPrizeSet.getPset_type());
            data.put("type", prizeType);
        }

        getR().setData("list", list);
        getR().setData("count", count);
        return getR();
    }


    //删除
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        String sessionId = input("session_id");
        return SvcLNumSessionPrize.getBean().delete(sessionId, List.of(ids));
    }
}
