package com.siyoumi.app.modules.website_fjciecc.entity;

import com.alibaba.fastjson.JSON;
import com.siyoumi.util.XStr;
import lombok.Data;

//招聘岗位字段
@Data
public class WebSiteFjcieccRecruit {
    String desc = ""; //岗位描述
    String ask = ""; //任职要求
    //String address = ""; //工作地点
    String salary = ""; //薪资待遇
    String contact = ""; //投递方式

    public static WebSiteFjcieccRecruit getInstance(String s) {
        if (XStr.isNullOrEmpty(s)) {
            return new WebSiteFjcieccRecruit();
        }

        return JSON.parseObject(s, WebSiteFjcieccRecruit.class);
    }
}
