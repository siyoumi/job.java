package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;


//评测发布
@Data
public class VoEssTestAudit {
    @HasAnyText(message = "缺少评测ID")
    String test_id;
    @HasAnyText(message = "缺少状态")
    @EqualsValues(vals = {"0", "1"}, message = "状态参数异常")
    Integer state;
}
