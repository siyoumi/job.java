package com.siyoumi.app.modules.pwd.admin;

import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.feign.view_admin.SiyoumiFeign;
import com.siyoumi.app.modules.pwd.vo.PwdVo;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.component.XApp;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/pwd/pwd__edit")
public class pwd__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("修改密码");


        SysAccountService appAcc = SysAccountService.getBean();
        SysAccount entity = appAcc.getSysAccountByToken();

        Map<String, Object> data = new HashMap<>();
        data.put("acc_name", entity.getAcc_name());
        data.put("acc_openid", entity.getAcc_openid());
        data.put("acc_headimg", entity.getAcc_headimg());

        getR().setData("data", data);

        String token = SiyoumiFeign.getBean().getToken(XApp.getStrID());
        setPageInfo("token", token);

        return getR();
    }

    @PostMapping("/save")
    @Transactional
    public XReturn save(@Validated() PwdVo pwdVo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        //自定义验证
        if (!pwdVo.getNew_pwd2().equals(pwdVo.getNew_pwd())) {
            result.addError(XValidator.getErr("new_pwd", "密码不一致"));
        }
        if (pwdVo.getOld_pwd().equals(pwdVo.getNew_pwd())) {
            result.addError(XValidator.getErr("new_pwd", "旧密码与新密码相同"));
        }

        SysAccountService appAcc = SysAccountService.getBean();
        SysAccount entityAcc = appAcc.getSysAccountByToken();

        //检查旧密码
        String encOldPwd = XApp.encPwd(entityAcc.getAcc_x_id(), pwdVo.getOld_pwd());
        if (!entityAcc.getAcc_pwd().equals(encOldPwd)) {
            result.addError(XValidator.getErr("old_pwd", "原密码不正确"));
        }
        XValidator.getResult(result);

        //修改密码
        appAcc.resetPwd(entityAcc.getKey(), pwdVo.getNew_pwd());


        return getR();
    }

    @GetMapping("/unbind")
    public XReturn unBind() {
        SysAccount entityAcc = getEntityAcc();

        SysAccount entityAccUpdate = new SysAccount();
        entityAccUpdate.setAcc_id(entityAcc.getAcc_id());
        entityAccUpdate.setAcc_openid("");
        entityAccUpdate.setAcc_headimg("");
        SysAccountService.getBean().updateById(entityAccUpdate);

        String key = XHttpContext.getX() + "|getSysAccount";
        XHttpContext.del(key);

        return getR();
    }

    @GetMapping("/bind")
    public XReturn bind() {
        SysAccount entityAcc = getEntityAcc();
        if (XStr.hasAnyText(entityAcc.getAcc_openid())) {
            return XReturn.getR(20102, "账号已绑定");
        }

        String oauthCode = input("oauth_code");
        SiyoumiFeign feign = SiyoumiFeign.getBean();
        XReturn r = feign.getRedisVal(oauthCode);
        if (r.err()) {
            return r;
        }
        String openid = r.getData("val");
        if (XStr.isNullOrEmpty(openid)) {
            return EnumSys.ERR_VAL.getR("授权码已失效");
        }
        WxUser entityWxUser = feign.getUser(openid);
        String headimgurl = entityWxUser.getWxuser_headimgurl();

        SysAccount entityAccByOpenid = SysAccountService.getBean().getAccByOpenid(openid);
        if (entityAccByOpenid != null) {
            return XReturn.getR(20118, XStr.format("用户已绑定{0}帐号", entityAccByOpenid.getAcc_name()));
        }

        SysAccount entityAccUpdate = new SysAccount();
        entityAccUpdate.setAcc_id(entityAcc.getAcc_id());
        entityAccUpdate.setAcc_openid(openid);
        entityAccUpdate.setAcc_headimg(headimgurl);
        SysAccountService.getBean().saveOrUpdatePassEqualField(entityAcc, entityAccUpdate);


        String key = XHttpContext.getX() + "|getSysAccount";
        XHttpContext.del(key);

        getR().setData("entity_acc", entityAcc);
        return getR();
    }
}

