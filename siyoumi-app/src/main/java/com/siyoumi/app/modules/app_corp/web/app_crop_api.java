package com.siyoumi.app.modules.app_corp.web;

import com.siyoumi.app.entity.CorpStore;
import com.siyoumi.app.modules.app_corp.service.SvcCropStore;
import com.siyoumi.app.modules.app_corp.vo.VaCropStore;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/wxapp/app_corp/api")
public class app_crop_api
        extends WxAppApiController {
    //商家信息
    @GetMapping("store_info_by_openid")
    public XReturn storeEditByOpenid() {
        CorpStore entityStore = SvcCropStore.getBean().getEntityByOpenid(getOpenid());
        if (entityStore == null) {
            return XReturn.getR(20029, "未创建wifi");
        }

        return SvcCropStore.getBean().info(entityStore.getCstore_id());
    }

    //商家保存
    @PostMapping("store_edit")
    public XReturn storeEdit(@Validated VaCropStore vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        InputData inputData = InputData.fromRequest();
        vo.setCstore_openid(getOpenid());

        CorpStore entityStore = SvcCropStore.getBean().getEntityByOpenid(getOpenid());
        if (entityStore != null) {
            //1个用户只能新建1个
            inputData.put("id", entityStore.getCstore_id());
        }

        vo.setCstore_enable(0);
        vo.setCstore_enable_date(XDate.now());

        return SvcCropStore.getBean().edit(inputData, vo);
    }

    //更新uv
    @GetMapping("store_add_uv")
    public XReturn addUv() {
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }

        return SvcCropStore.getBean().updateUv(getID(), getOpenid());
    }
}
