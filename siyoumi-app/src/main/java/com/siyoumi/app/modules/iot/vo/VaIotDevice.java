package com.siyoumi.app.modules.iot.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//产品设备
@Data
public class VaIotDevice {
    private String idev_acc_id;
    @HasAnyText
    private String idev_pro_id;
    @HasAnyText
    @Size(max = 50)
    private String idev_num;

    private String idev_key;
    private Integer idev_order;
}
