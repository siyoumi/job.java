package com.siyoumi.app.modules.wifi.service;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;

//商家
@Slf4j
@Service
public class SvcWifi {
    static public SvcWifi getBean() {
        return XSpringContext.getBean(SvcWifi.class);
    }

    static public SysAbcService getApp() {
        return SysAbcService.getBean();
    }


    //wifi信息
    public XReturn info(String id) {
        SysAbc entityStore = getApp().getEntityByTable(id, "wifi");
        if (entityStore == null) {
            return EnumSys.ERR_VAL.getR("id异常");
        }

        Map<String, Object> mapStore = XBean.toMap(entityStore, new String[]{
                "abc_id",
                "abc_name",
                "abc_str_00",
                "abc_str_01",
                "abc_str_02",
                "abc_order",
        });

        XReturn r = XReturn.getR(0);
        r.setData("wifi", mapStore);
        return r;
    }
}
