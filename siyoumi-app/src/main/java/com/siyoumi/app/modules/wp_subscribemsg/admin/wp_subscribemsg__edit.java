package com.siyoumi.app.modules.wp_subscribemsg.admin;

import com.siyoumi.app.entity.WxSubscribemsg;
import com.siyoumi.app.modules.wp_subscribemsg.entity.MiniprogramState;
import com.siyoumi.app.service.WxSubscribemsgService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/wp_subscribemsg/wp_subscribemsg__edit")
public class wp_subscribemsg__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("订阅消息-编辑");

        WxSubscribemsgService app = WxSubscribemsgService.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("wxsg_miniprogram_state", "");
        if (isAdminEdit()) {
            WxSubscribemsg entity = app.loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            Map<String, Object> map = app.contentToMap(entity.getWxsg_content());
            map.forEach((k, v) -> {
                Map vMap = (Map) v;
                dataAppend.put("data_" + k, vMap.get("value"));
            });

            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        setPageInfo("miniprogram_state", IEnum.toMap(MiniprogramState.class));


        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() WxSubscribemsg entity, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //自定交验证
        if (!XStr.startsWith(entity.getWxsg_page(), "pages")) {
            result.addError(XValidator.getErr("wxsg_page", "page内容错误【pages/index/index】"));
        }
        XValidator.getResult(result);

        WxSubscribemsgService app = WxSubscribemsgService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("wxsg_uix");
            ignoreField.add("wxsg_app_id");
        }

        InputData inputData = InputData.fromRequest();
        inputData.putAll(entity.toMap());
        return app.saveEntity(inputData, true, ignoreField);
    }

    //测试发送
    @GetMapping("/test_send")
    public XReturn testSend() {
        String openid = input("test_openid");
        if (XStr.isNullOrEmpty(openid) || XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR();
        }

        WxSubscribemsgService app = WxSubscribemsgService.getBean();
        XReturn r = app.send(getID(), openid, null);
        getR().setData("res", XJson.toJSONString(r));

        return getR();
    }

    //获取接口模板
    @GetMapping("/mp_templmsg")
    public XReturn getMpTemplmsg() {
        return WxSubscribemsgService.getBean().getMpTemplmsg();
    }
}
