package com.siyoumi.app.modules.app_ess.admin;

import com.siyoumi.app.entity.EssClass;
import com.siyoumi.app.modules.app_ess.service.SvcEssClass;
import com.siyoumi.app.modules.app_ess.service.SvcEssUser;
import com.siyoumi.app.modules.app_ess.vo.VaEssClass;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__class__edit")
public class app_ess__class__edit
        extends AdminApiController {

    @GetMapping()
    public XReturn index() {
        setPageTitle("班级-编辑");

        List<String> teacherUids = new ArrayList<>();
        Map<String, Object> data = new HashMap<>();
        data.put("eclass_order", 0);
        if (isAdminEdit()) {
            EssClass entity = SvcEssClass.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);

            teacherUids = SvcEssClass.getBean().getUids(entity.getKey(), 1);
        }
        data.put("teacher_uids", teacherUids);
        getR().setData("data", data);

        //老师列表
        setPageInfo("list_teacher", SvcEssUser.getBean().getTeacherList());

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaEssClass vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        vo.setEclass_acc_id(getAccId());
        //
        InputData inputData = InputData.fromRequest();
        return SvcEssClass.getBean().edit(inputData, vo);
    }
}