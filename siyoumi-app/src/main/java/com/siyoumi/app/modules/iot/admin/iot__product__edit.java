package com.siyoumi.app.modules.iot.admin;

import com.siyoumi.app.entity.IotProduct;
import com.siyoumi.app.modules.iot.entity.EnumIotProductType;
import com.siyoumi.app.modules.iot.service.SvcIotProduct;
import com.siyoumi.app.modules.iot.vo.VaIotProduct;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/iot/iot__product__edit")
public class iot__product__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("产品-编辑");

        InputData inputData = InputData.fromRequest();

        Map<String, Object> data = new HashMap<>();
        data.put("ipro_order", 0);
        if (isAdminEdit()) {
            IotProduct entity = SvcIotProduct.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //类型
        setPageInfo("types", EnumIotProductType.getBean());

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaIotProduct vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        if (!isAdminEdit()) {
            vo.setIpro_acc_id(getAccId());
        }

        InputData inputData = InputData.fromRequest();
        return SvcIotProduct.getBean().edit(inputData, vo);
    }
}
