package com.siyoumi.app.modules.app_corp.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.CorpStore;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.modules.app_corp.entity.EnumCorpStoreEnable;
import com.siyoumi.app.modules.app_corp.service.SvcCropStore;
import com.siyoumi.app.modules.app_corp.vo.CropStoreAuditVo;
import com.siyoumi.app.modules.app_corp.vo.VaCropStore;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_corp/app_corp__store__list")
public class app_corp__store__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商家-列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "cstore_id",
                "cstore_name",
                "cstore_wifi_ssid",
                "cstore_pic_h5",
                "cstore_uv",
                "cstore_create_date",
                "cstore_order",
                "cstore_enable",
                "wxuser_openid",
                "wxuser_phone",
                "wxuser_nickName",
        };
        JoinWrapperPlus<CorpStore> query = SvcCropStore.getBean().listQuery(inputData);
        query.leftJoin(WxUser.table(), "wxuser_openid", "cstore_openid");
        query.select(select);

        IPage<CorpStore> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcCropStore.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        LinkedHashMap<String, String> mapEnable = IEnum.toMap(EnumCorpStoreEnable.class);
        for (Map<String, Object> data : list) {
            CorpStore entity = XBean.fromMap(data, CorpStore.class);
            data.put("id", entity.getKey());

            //使用时长
            data.put("use_day", entity.useDay());

            //审核状态
            String enable = mapEnable.get(entity.getCstore_enable().toString());
            data.put("enable", enable);
        }

        //审核状态
        setPageInfo("enable_arr", mapEnable);

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcCropStore.getBean().delete(Arrays.asList(ids));
    }

    //删除
    @PostMapping("/audit")
    public XReturn audit(@Validated() CropStoreAuditVo vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        return SvcCropStore.getBean().audit(vo);
    }
}
