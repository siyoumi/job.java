package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaBookStoreAccount {
    private String acc_role;

    @HasAnyText()
    @Size(max = 50)
    private String acc_uid;
    @Size(max = 50)
    private String acc_pwd;
    @HasAnyText()
    @Size(max = 50)
    private String acc_name;
}
