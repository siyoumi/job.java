package com.siyoumi.app.modules.account.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysDepartment;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.entity.SysRole;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/account/account__list")
public class account__list
        extends AccountController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("帐号列表");

        String compKw = input("compKw");

        SysAccountService app = SysAccountService.getBean();
        if (!isSuperAdmin()) {
            return EnumSys.ADMIN_AUTH_SUPER_ERR.getR();
        }

        String[] select = {
                "t_s_account.*",
                "role_name",
                "dep_name",
        };
        //query
        JoinWrapperPlus<SysAccount> query = app.join();
        //join
        query.leftJoin(SysRole.table(), SysRole.tableKey(), "acc_role");
        query.leftJoin(SysDepartment.table(), SysDepartment.tableKey(), "acc_department_id");
        query.select(select)
                .eq(app.fdX(), XHttpContext.getX())
                .notIn("acc_role", List.of("super_admin", "dev", "store"))
                .orderByDesc("acc_create_date");
        if (XStr.hasAnyText(compKw)) //名称
        {
            query.like("acc_name", XStr.concat("%", compKw, "%"));
        }

        IPage<SysAccount> page = new Page<>(getPageIndex(), getPageSize());

        IPage<Map<String, Object>> pageData = app.mapper().getMaps(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<Map<String, Object>> listData = pageData.getRecords();
        List<Map<String, Object>> list = listData.stream().map(item ->
        {
            SysAccount entityAcc = app.loadEntity(item);

            item.put("id", item.get("acc_id"));
            item.put("uid", app.getUid(entityAcc));
            item.put("vaild", app.isValid(entityAcc));

            return item;
        }).collect(Collectors.toList());


        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        SysAccountService app = SysAccountService.getBean();
        if (!isSuperAdmin()) {
            return EnumSys.ADMIN_AUTH_SUPER_ERR.getR();
        }

        String ids = input("ids");
        if (XStr.isNullOrEmpty(ids)) {
            return XReturn.getR(EnumSys.MISS_VAL.getErrcode(), "miss ids");
        }

        String[] arr = ids.split(",");
        List<String> collect = Arrays.stream(arr).collect(Collectors.toList());
        app.removeBatchByIds(collect);


        return getR();
    }

    @GetMapping("/reset_pwd")
    public XReturn resetPwd() {
        SysAccountService app = SysAccountService.getBean();
        if (!isSuperAdmin()) {
            return EnumSys.ADMIN_AUTH_SUPER_ERR.getR();
        }

        String ids = input("ids");
        if (XStr.isNullOrEmpty(ids)) {
            return XReturn.getR(EnumSys.MISS_VAL.getErrcode(), "miss ids");
        }

        return app.resetPwd(ids, null);
    }

    @GetMapping("/reset_lock_date")
    public XReturn resetLockDate() {
        if (!isSuperAdmin()) {
            return EnumSys.ADMIN_AUTH_SUPER_ERR.getR();
        }

        String ids = input("ids");
        if (XStr.isNullOrEmpty(ids)) {
            return XReturn.getR(EnumSys.MISS_VAL.getErrcode(), "miss ids");
        }

        return SysAccountService.getBean().resetLockDate(ids);
    }
}
