package com.siyoumi.app.modules.book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//核销
@Data
public class VaBookUse {
    @HasAnyText(message = "请选择预约记录")
    String record_id;
}
