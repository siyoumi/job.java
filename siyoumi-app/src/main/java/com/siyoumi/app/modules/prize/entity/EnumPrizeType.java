package com.siyoumi.app.modules.prize.entity;

import com.siyoumi.util.IEnum;

//奖品类型
public enum EnumPrizeType
        implements IEnum {
    NULL("", "实物奖"),
    FUN("fun", "积分奖")
    ;


    private String key;
    private String val;

    EnumPrizeType(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
