package com.siyoumi.app.modules.app_book.vo;

import lombok.Data;

//商家提现信息
@Data
public class VaBookStoreMoneyInfo {
    private String user_name = "";
    private String bank_no = "";
    private String bank_name = "";
}
