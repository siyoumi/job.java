package com.siyoumi.app.modules.app_book.admin;

import com.siyoumi.app.entity.BookSet;
import com.siyoumi.app.entity.BookSetDay;
import com.siyoumi.app.entity.BookSpuDay;
import com.siyoumi.app.modules.app_book.service.SvcBookSet;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.service.BookSetDayService;
import com.siyoumi.app.service.BookSpuDayService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__set_day__list")
public class app_book__set_day__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        String setId = input("set_id");
        String month = input("month");
        if (XStr.isNullOrEmpty(month)) {
            month = XDate.format(XDate.now(), "yyyy-MM");
        }
        setPageFrom("month", month);


        XValidator.isNullOrEmpty(setId, "miss set_id");

        BookSet entitySet = SvcBookSet.getApp().getEntity(setId);
        XValidator.isNull(entitySet, "spu_id error");

        setPageTitle(entitySet.getBset_name(), "-每日加价");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "bsday_id",
                "bsday_date",
                "bsday_add_price",
                "bsday_add_price_breakfast",
                "bsday_add_price_lunch",
                "bsday_add_price_dinner",
        };
        JoinWrapperPlus<BookSetDay> query = SvcBookSet.getBean().listDayQuery(setId);
        LocalDateTime b = XDate.parse(month + "-01");
        LocalDateTime e = b.plusMonths(1).minusDays(1);
        query.between("bsday_date", b, e);
        query.select(select);
        //list
        List<Map<String, Object>> listDay = BookSetDayService.getBean().getMaps(query);
        //
        List<LocalDateTime> dateList = XDate.getDateList(b, e, null);
        Map<String, Object> mapDateList = new LinkedHashMap<>();
        for (LocalDateTime date : dateList) {
            Map<String, Object> mapDate = listDay.stream().filter(i -> i.get("bsday_date").equals(date)).findFirst().orElse(null);
            BookSetDay entity;
            if (mapDate == null) {
                entity = new BookSetDay();
                entity.setBsday_add_price(BigDecimal.ZERO);
                entity.setBsday_add_price_breakfast(BigDecimal.ZERO);
                entity.setBsday_add_price_lunch(BigDecimal.ZERO);
                entity.setBsday_add_price_dinner(BigDecimal.ZERO);
            } else {
                entity = XBean.fromMap(mapDate, BookSetDay.class);
            }

            Map<String, Object> item = new HashMap<>();
            item.put("add_price_breakfast", entity.getBsday_add_price_breakfast());
            item.put("add_price_lunch", entity.getBsday_add_price_lunch());
            item.put("add_price_dinner", entity.getBsday_add_price_dinner());
            item.put("add_price", entity.getBsday_add_price());
            mapDateList.put(XDate.toDateString(date), item);
        }

        getR().setData("list", mapDateList);
        getR().setData("count", listDay.size());

        return getR();
    }
}
