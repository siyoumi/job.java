package com.siyoumi.app.modules.user.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SysUserAdd {
    String id;
    String user_phone;
    String user_name;

    public static SysUserAdd of(String phone, String name) {
        return new SysUserAdd()
                .setUser_phone(phone)
                .setUser_name(name);
    }
}
