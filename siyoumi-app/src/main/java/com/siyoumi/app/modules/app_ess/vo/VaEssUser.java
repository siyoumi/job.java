package com.siyoumi.app.modules.app_ess.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

//学生老师
@Data
public class VaEssUser {
    private String euser_id;
    @HasAnyText
    private String euser_type;

    @HasAnyText
    @Size(max = 50)
    private String user_name;
    //@HasAnyText
    @Size(max = 200)
    private String user_headimg;
    //@HasAnyText
    //@Size(max = 50)
    private String user_phone;
    @HasAnyText
    @Size(max = 50)
    private String user_username;

    @EqualsValues(vals = {"1", "2", "0"})
    private Integer euser_sex;
    @Size(max = 50)
    private String euser_speciality;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime euser_school_date;
    @Size(max = 500)
    private String euser_address;
    @Size(max = 50)
    private String euser_contact;
}
