package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;

//订单退款状态
public class EnumBookOrderRefundHandle
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "待处理");
        put(1, "已处理");
        put(10, "处理失败");
    }
}
