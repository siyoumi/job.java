package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysSale;
import com.siyoumi.app.entity.SysSaleRecord;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_book.entity.EnumBookSaleLevel;
import com.siyoumi.app.modules.app_book.vo.VoBookSpuAudit;
import com.siyoumi.app.modules.user.service.SvcSysSale;
import com.siyoumi.app.service.SysSaleRecordService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__sale__record__list")
public class app_book__sale__record__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("入账记录列表");

        InputData inputData = InputData.fromRequest();
        String type = inputData.input("type", "");

        String[] select = {
                "srec_id",
                "srec_create_date",
                "srec_ab_type",
                "srec_id_src",
                "srec_desc",
                "srec_num",
                "srec_uid",
                "sale_name",
        };
        JoinWrapperPlus<SysSaleRecord> query = SvcSysSale.getBean().listRecordQuery(inputData);
        query.join(SysSale.table(), "sale_uid", "srec_uid");
        query.select(select);
        query.eq("sale_type", type);
        if (isStore()) { //商家
            query.eq("sale_uid", getStoreId());
        }

        query.orderByDesc("srec_create_date")
                .orderByAsc("srec_id");

        IPage<SysSaleRecord> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SysSaleRecordService.getBean().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> item : list) {
            SysSaleRecord entity = XBean.fromMap(item, SysSaleRecord.class);

            item.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }
}
