package com.siyoumi.app.modules.redbook.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.RedbookComment;
import com.siyoumi.app.entity.RedbookQuan;
import com.siyoumi.app.service.RedbookQuanService;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XSqlStr;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//朋友圈
@Slf4j
@Service
public class SvcRedbookQuan {
    static public SvcRedbookQuan getBean() {
        return XSpringContext.getBean(SvcRedbookQuan.class);
    }

    static public RedbookQuanService getApp() {
        return RedbookQuanService.getBean();
    }

    //检查小红书是否有效
    public XReturn validQuan(RedbookQuan entityQuan) {
        if (entityQuan.getRquan_enable() != 1) {
            return XReturn.getR(20205, "审核中");
        }
        //if (entityQuan.getRbquan_del() != 0) {
        //    return CommonReturn.funcNew(20215, "不存在");
        //}

        return EnumSys.OK.getR();
    }

    public JoinWrapperPlus<RedbookQuan> listQuery(InputData inputData) {
        String title = inputData.input("title");
        String enable = inputData.input("enable");
        String uid = inputData.input("uid");

        JoinWrapperPlus<RedbookQuan> query = SvcRedbookQuan.getApp().join();
        query.orderByDesc("rquan_top")
                .orderByDesc("rquan_create_date");
        if (XStr.hasAnyText(title)) { //标题
            query.likeRight("rquan_title", title);
        }
        if (XStr.hasAnyText(enable)) { //审核状态
            query.likeRight("rquan_enable", enable);
        }
        if (XStr.hasAnyText(uid)) { //uid
            query.likeRight("rquan_uid", uid);
        }

        return query;
    }


    /**
     * 删除
     *
     * @return
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids, String openid) {
        QueryWrapper<RedbookQuan> query = getApp().q();
        query.in("rquan_id", ids);

        if (XStr.hasAnyText(openid)) { //用户
            query.eq("rquan_openid", openid);
        }
        List<RedbookQuan> list = getApp().list(query);
        getApp().removeBatchByIds(list);

        return EnumSys.OK.getR();
    }


    /**
     * 置顶
     * 1：设置置顶，0：取消置顶
     *
     * @param ids
     * @param top
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn top(List<String> ids, int top) {
        long quanTop = XDate.toS();
        if (top == 0) {
            quanTop = 0;
        }

        SvcRedbookQuan.getApp().update()
                .set("rquan_top", quanTop)
                .in(RedbookQuan.tableKey(), ids)
                .eq("rquan_x_id", XHttpContext.getX())
                .ne("rquan_top", top)
                .update();

        return EnumSys.OK.getR();
    }


    //审核
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn enable(List<String> ids, int enable) {

        SvcRedbookQuan.getApp().update()
                .set("rquan_enable", enable)
                .in(RedbookQuan.tableKey(), ids)
                .eq("rquan_x_id", XHttpContext.getX())
                .ne("rquan_enable", enable)
                .update();

        return EnumSys.OK.getR();
    }


    /**
     * 更新评论总数
     *
     * @param entity
     */
    public Integer updateCommentCount(RedbookQuan entity) {
        Long count = SvcRedbookComment.getBean().getCount(entity.getRquan_id(), null);
        RedbookQuan entityUpdate = new RedbookQuan();
        entityUpdate.setRquan_id(entity.getRquan_id());
        entityUpdate.setRquan_comment_count(count);
        getApp().saveOrUpdatePassEqualField(entity, entityUpdate);

        //updateCommentCountCheck(entity);

        return count.intValue();
    }

    /**
     * 更新最新1级评论ID（3个）
     *
     * @param entity
     */
    public void updateCommentLastIds(RedbookQuan entity) {
        JoinWrapperPlus<RedbookComment> query = SvcRedbookComment.getBean().listQuery(entity.getRquan_id());
        query.select("rbc_id");
        query.eq("rbc_enable", 1)
                .eq("rbc_pid", "")
                .orderByAsc("rbc_create_date")
                .orderByAsc("rbc_id");
        query.last(XSqlStr.limitX(3));
        List<Map<String, Object>> list = SvcRedbookComment.getApp().getMaps(query);

        String ids = list.stream().map(item -> (String) item.get("rbc_id")).collect(Collectors.joining(","));

        RedbookQuan entityUpdate = new RedbookQuan();
        entityUpdate.setRquan_id(entity.getRquan_id());
        entityUpdate.setRquan_comment_last_ids(ids);
        getApp().saveOrUpdatePassEqualField(entity, entityUpdate);
    }
}
