package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

//新建文件
@Data
public class VaFksDataFile {
    private Integer ffile_type;
    @HasAnyText
    private String ffile_type_id;
    private String ffile_acc_id;
    private Integer ffile_folder;
    @HasAnyText
    @Size(max = 50)
    private String ffile_name;
    private String ffile_folder_id;
    @Size(max = 500)
    private String ffile_path;
    private Long ffile_file_size;
    @Size(max = 500)
    private String ffile_file_url;
    @Size(max = 20)
    private String ffile_file_ext;
    @HasAnyText
    private String ffile_data_type;

    private String ffile_department_id;
    private String ffile_uid;

    private String ffile_note_id;

    //权限
    private Integer ffile_auth = 0; //【0】全部可见；【1】部分可见；
    private List<String> auth_uids;
    private Integer auth_set = 0;
}
