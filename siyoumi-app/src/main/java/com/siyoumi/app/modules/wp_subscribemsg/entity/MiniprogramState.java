package com.siyoumi.app.modules.wp_subscribemsg.entity;

import com.siyoumi.util.IEnum;


public enum MiniprogramState
        implements IEnum {
    PRO("", "正式"),
    TRIAL("trial", "体验"),
    DEV("developer", "开发");


    private String key;
    private String val;

    MiniprogramState(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key.toString();
    }
}
