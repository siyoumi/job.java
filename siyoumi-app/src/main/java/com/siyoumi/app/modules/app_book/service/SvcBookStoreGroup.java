package com.siyoumi.app.modules.app_book.service;

import com.siyoumi.app.entity.BookStoreGroup;
import com.siyoumi.app.entity.FksCity;
import com.siyoumi.app.modules.app_book.vo.VaBookStoreGroup;
import com.siyoumi.app.modules.app_fks.vo.VaFksCity;
import com.siyoumi.app.service.BookStoreGroupService;
import com.siyoumi.app.service.FksCityService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//
@Slf4j
@Service
public class SvcBookStoreGroup
        implements IWebService {
    static public SvcBookStoreGroup getBean() {
        return XSpringContext.getBean(SvcBookStoreGroup.class);
    }

    static public BookStoreGroupService getApp() {
        return BookStoreGroupService.getBean();
    }

    public List<Map<String, Object>> getList() {
        JoinWrapperPlus<BookStoreGroup> query = listQuery();
        query.select("bsgroup_id", "bsgroup_name");
        return getApp().getMaps(query);
    }

    public JoinWrapperPlus<BookStoreGroup> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<BookStoreGroup> listQuery(InputData inputData) {
        String name = inputData.input("name");

        JoinWrapperPlus<BookStoreGroup> query = getApp().join();
        query.eq("bsgroup_x_id", XHttpContext.getX());
        //排序
        query.orderByAsc("bsgroup_order")
                .orderByDesc("bsgroup_id");
        if (XStr.hasAnyText(name)) { //名称
            query.like("bsgroup_name", name);
        }

        return query;
    }

    public XReturn edit(InputData inputData, VaBookStoreGroup vo) {
        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminEdit()) {
            ignoreField.add("bsgroup_acc_id");
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, true, ignoreField);
            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }
}
