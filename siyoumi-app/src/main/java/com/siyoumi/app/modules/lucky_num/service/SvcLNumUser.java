package com.siyoumi.app.modules.lucky_num.service;

import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.siyoumi.app.entity.LnumUser;
import com.siyoumi.app.entity.LnumWin;
import com.siyoumi.app.service.LnumUserService;
import com.siyoumi.app.service.LnumWinService;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SvcLNumUser
        implements IWebService {
    public static SvcLNumUser getBean() {
        return XSpringContext.getBean(SvcLNumUser.class);
    }

    public static LnumUserService getApp() {
        return LnumUserService.getBean();
    }


    public LnumUser getEntity(String openid) {
        InputData inputData = InputData.getIns();
        inputData.put("openid", openid);

        String key = openid;
        return XHttpContext.getAndSetData(key, k -> {
            JoinWrapperPlus<LnumUser> query = listQuery(inputData);

            return getApp().first(query);
        });
    }

    public LnumUser getOrNewEntity(String openid) {
        LnumUser entity = getEntity(openid);
        if (entity == null) {
            entity = new LnumUser();
            entity.setLnuser_x_id(XHttpContext.getX());
            entity.setLnuser_openid(openid);
            entity.setAutoID();
            getApp().save(entity);

            entity = getEntity(openid);
        }

        return entity;
    }

    /**
     * 更新领卡数量
     *
     * @param openid
     * @param useDb  true: 从数据库；false: 直接加1;
     */
    public void updateGetCardCount(String openid, Boolean useDb) {
        LnumUser entity = getOrNewEntity(openid);

        if (entity.getLnuser_get_total() == null) {
            //新建，1次，使用数据库更新
            useDb = true;
        }

        String setSql = "lnuser_get_total = lnuser_get_total + 1";
        if (useDb) {
            Long numCount = SvcLNumGroup.getBean().numCount(openid);
            setSql = "lnuser_get_total = " + numCount;
        }
        setSql += " , lnuser_get_last_date = '" + XDate.toDateTimeString() + "'";

        UpdateChainWrapper<LnumUser> update = getApp().update();
        update.eq("lnuser_id", entity.getLnuser_id())
                .setSql(setSql)
                .update();
    }

    /**
     * 更新中奖数量
     *
     * @param groupId
     * @param openid
     */
    public void updateWinCount(String groupId, String openid) {
        LnumUser entity = getOrNewEntity(openid);

        JoinWrapperPlus<LnumWin> query = LnumWinService.getBean().join();
        query.eq("lnwin_x_id", XHttpContext.getX())
                .eq("lwin_wxuser_id", openid);
        Long winCount = LnumWinService.getBean().count(query);

        LnumUser entityUpdate = new LnumUser();
        entityUpdate.setLnuser_id(entityUpdate.getLnuser_id());
        entityUpdate.setLnuser_win_total(winCount);

        getApp().saveOrUpdatePassEqualField(entity, entityUpdate);
    }


    public JoinWrapperPlus<LnumUser> listQuery(String openid) {
        InputData inputData = InputData.getIns();
        inputData.put("openid", openid);

        return listQuery(inputData);
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<LnumUser> listQuery(InputData inputData) {
        String openid = inputData.input("openid");

        JoinWrapperPlus<LnumUser> query = getApp().join();
        query.eq("lnuser_x_id", XHttpContext.getX());

        if (XStr.hasAnyText(openid)) { //openid
            query.like("lnuser_openid", openid);
        }

        return query;
    }
}
