package com.siyoumi.app.modules.mall2.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

//退款审核接口
@Data
public class OrderItemRefundAuditVo {
    @HasAnyText(message = "miss refund_id")
    String refund_id; //退款总订单
    @HasAnyText(message = "请输入密码")
    @Size(max = 50)
    String admin_pwd;

    //审核状态，0：未审核；1：已通过；-10：不通过；-20：用户撤回；
    @HasAnyText(message = "缺少审核状态")
    Integer audit_status;

    BigDecimal refund_price; //退款金额
    @Size(max = 100)
    String audit_desc = ""; //审核描述
    String audit_acc; //审核人acc_id

    /**
     * 审核通过
     */
    public Boolean auditOk() {
        return getAudit_status() == 1;
    }
}
