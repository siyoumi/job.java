package com.siyoumi.app.modules.website_fjciecc.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.entity.SysItem;
import com.siyoumi.app.modules.website_fjciecc.entity.EnumFjcieccPerformance;
import com.siyoumi.app.modules.website_fjciecc.entity.EnumFjcieccTrends;
import com.siyoumi.app.modules.website_fjciecc.entity.WebSiteFjcieccRecruit;
import com.siyoumi.app.modules.website_fjciecc.service.SvcFjciecc;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.app.service.SysItemService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.ApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@Slf4j
@RestController
@RequestMapping("/sys/website_fjciecc/api")
public class website_fjciecc_api
        extends ApiController {

    @ModelAttribute
    public void initController(ModelMap modelMap) {
        log.debug("initController");

    }

    //初始化
    @GetMapping("init")
    public XReturn init() {
        Map mapSetting = SvcFjciecc.getBean().getSetting();

        getR().setData("setting", mapSetting);

        return getR();
    }

    //列表
    @GetMapping("list")
    public XReturn list() {
        String abcTable = input("abc_table");
        String name = input("name");

        List<String> allowTable = List.of(new String[]{
                "fjciecc_performance",
                "fjciecc_decision",
                "fjciecc_trends",
                "fjciecc_recruit",
                "fjciecc_social",
        });

        List<String> abcTables = new ArrayList<>();
        Boolean searchAll = "all".equals(abcTable);
        if (!searchAll) {
            //搜索单个
            if (XStr.isNullOrEmpty(abcTable)) {
                return EnumSys.MISS_VAL.getR("miss abc_table");
            }
            if (!allowTable.contains(abcTable)) {
                return EnumSys.ERR_VAL.getR("abc_table error");
            }

            abcTables.add(abcTable);
        } else {
            //全文搜索
            if (XStr.isNullOrEmpty(name)) {
                return EnumSys.MISS_VAL.getR("miss name");
            }

            abcTables = allowTable;
        }

        Set<String> select = new HashSet<>();
        select.add("abc_id");
        select.add("abc_table");
        select.add("abc_name");
        select.add("abc_int_00");
        select.add("abc_str_00");
        select.add("abc_str_01");
        select.add("abc_str_02");
        select.add("abc_str_03");
        select.add("abc_txt_00");
        select.add("abc_date_00");
        select.add("abc_create_date");

        InputData inputData = InputData.fromRequest();
        JoinWrapperPlus<SysAbc> query = SvcFjciecc.getBean().listQuery(inputData, abcTables);
        query.select(select.toArray(new String[]{}));
        IPage<SysAbc> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), true);
        //list
        IPage<Map<String, Object>> pageData = SysAbcService.getBean().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        for (Map<String, Object> data : list) {
            SysAbc entityAbc = SysAbcService.getBean().loadEntity(data);
            switch (abcTable) {
                case "fjciecc_performance":
                case "fjciecc_decision":
                case "fjciecc_trends":
                case "fjciecc_social":
                    break;
                case "fjciecc_recruit": //招聘岗位
                    WebSiteFjcieccRecruit txt00 = WebSiteFjcieccRecruit.getInstance(entityAbc.getAbc_txt_00());
                    data.putAll(XBean.toMap(txt00));
                    break;
            }
        }

        getR().setData("abc_table", abcTable);
        getR().setData("list", list);
        getR().setData("count", page.getTotal());

        return getR();
    }

    //详情
    @GetMapping("info")
    public XReturn info() {
        if (XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR("miss id");
        }
        SysAbc entity = SysAbcService.getBean().getEntity(getID());
        if (entity == null) {
            return EnumSys.ERR_VAL.getR("id 异常");
        }

        getR().setData("data", entity);
        //追加显示
        switch (entity.getAbc_table()) {
            case "fjciecc_performance": //典型业绩
                //轮播图
                SysItem entityItem = SysItemService.getBean().getEntityByIdSrc(entity.getAbc_str_01());
                if (entityItem != null) {
                    getR().setData("banner", entityItem.commonData());
                }
                getR().setData("type", IEnum.getEnmuVal(EnumFjcieccPerformance.class, entity.getAbc_type()));
                break;
            case "fjciecc_decision": //决策咨询
                break;
            case "fjciecc_trends": //公司动态
                getR().setData("type", IEnum.getEnmuVal(EnumFjcieccTrends.class, entity.getAbc_type()));
                break;
            case "fjciecc_recruit": //招聘岗位
                WebSiteFjcieccRecruit txt00 = WebSiteFjcieccRecruit.getInstance(entity.getAbc_txt_00());
                getR().setData("data_append", txt00);
                break;
        }

        return getR();
    }
}
