package com.siyoumi.app.modules.mall2.vo;

import com.siyoumi.app.entity.M2Sku;
import lombok.Data;

@Data
public class SkuData {
    //商品
    String mspu_id;
    String mspu_name;
    String mspu_pic;
    Integer mspu_enable;
    //商家
    String mstore_id;
    String mstore_name;
    //库存
    Long stock_count_left;
    Long stock_count_use;

    M2Sku entity;

}
