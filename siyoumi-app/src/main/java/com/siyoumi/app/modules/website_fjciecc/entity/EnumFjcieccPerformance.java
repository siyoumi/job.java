package com.siyoumi.app.modules.website_fjciecc.entity;

import com.siyoumi.util.IEnum;

//典型业绩
public enum EnumFjcieccPerformance
        implements IEnum {
    P0("0", "规划咨询"),
    P1("1", "投资策划"),
    P2("2", "咨询评估"),
    P3("3", "产业研究"),
    ;


    private String key;
    private String val;

    EnumFjcieccPerformance(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
