package com.siyoumi.app.modules.lucky_num.admin;

import com.siyoumi.app.entity.LnumGroup;
import com.siyoumi.app.entity.LnumSession;
import com.siyoumi.app.modules.lucky_num.entity.LuckyNumType;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumGroup;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumSession;
import com.siyoumi.app.modules.lucky_num.service.SvcLnumNumSet;
import com.siyoumi.app.modules.lucky_num.vo.VaLnumSession;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/lucky_num/lucky_num__session__edit")
public class lucky_num__session__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("奖品期-编辑");

        InputData inputData = InputData.fromRequest();
        String groupId = inputData.input("group_id");
        if (isAdminAdd()) {
            if (XStr.isNullOrEmpty(groupId)) {
                return EnumSys.MISS_VAL.getR("miss group_id");
            }
        }

        Map<String, Object> data = new HashMap<>();
        data.put("lns_group_id", groupId);
        data.put("lns_order", 0);
        if (isAdminEdit()) {
            LnumSession entity = SvcLNumSession.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);

            groupId = entity.getLns_group_id();
        }
        getR().setData("data", data);

        LnumGroup entityGroup = SvcLNumGroup.getApp().first(groupId);
        if (entityGroup == null) {
            return EnumSys.ERR_VAL.getR("group_id异常");
        }
        Boolean setNum = false;
        if (!SvcLNumGroup.isLuckyNum(entityGroup.getLngroup_type())) {
            //初始化，号码设置
            SvcLnumNumSet.getBean().editAfter(inputData, getR(), getID());
            setNum = true;
        }
        setPageInfo("set_num", setNum);

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaLnumSession vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        if (!isAdminEdit()) {
            vo.setLns_acc_id(getAccId());
            if (XStr.isNullOrEmpty(vo.getLns_group_id())) {
                vo.setLnset_group_id(vo.getLns_group_id());
            }
        } else {

        }

        InputData inputData = InputData.fromRequest();
        return SvcLNumSession.getBean().edit(inputData, vo);
    }
}
