package com.siyoumi.app.modules.app_fks.service;

import com.siyoumi.app.entity.FksTag;
import com.siyoumi.app.entity.FksTagUser;
import com.siyoumi.app.modules.app_fks.vo.FksTagDel;
import com.siyoumi.app.modules.app_fks.vo.VaFksCity;
import com.siyoumi.app.modules.app_fks.vo.VaFksTag;
import com.siyoumi.app.service.FksTagService;
import com.siyoumi.app.service.FksTagUserService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//标签
@Slf4j
@Service
public class SvcFksTag
        implements IWebService {
    static public SvcFksTag getBean() {
        return XSpringContext.getBean(SvcFksTag.class);
    }

    static public FksTagService getApp() {
        return FksTagService.getBean();
    }

    public List<Map<String, Object>> getList(String uid) {
        JoinWrapperPlus<FksTag> query = listQuery(uid);
        query.select("ftag_id", "ftag_name");
        return getApp().getMaps(query);
    }

    public JoinWrapperPlus<FksTag> listQuery(String uid) {
        return listQuery(uid, null);
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<FksTag> listQuery(String uid, String name) {
        JoinWrapperPlus<FksTag> query = getApp().join();
        query.eq("ftag_x_id", XHttpContext.getX())
                .eq("ftag_uid", uid);
        //排序
        query.orderByDesc("ftag_create_date");
        if (XStr.hasAnyText(name)) { //名称
            query.like("ftag_name", name);
        }

        return query;
    }

    public JoinWrapperPlus<FksTagUser> listUserQuery() {
        JoinWrapperPlus<FksTagUser> query = FksTagUserService.getBean().join();
        query.eq("ftu_x_id", XHttpContext.getX());
        return query;
    }

    public JoinWrapperPlus<FksTagUser> listUserQuery(String tagId) {
        JoinWrapperPlus<FksTagUser> query = listUserQuery();
        query.eq("ftu_tag_id", tagId);

        return query;
    }


    /**
     * 标签全部uid
     *
     * @param tagId
     */
    public List<String> getTagUids(String tagId) {
        JoinWrapperPlus<FksTagUser> query = listUserQuery(tagId);
        query.select("ftu_uid");
        List<FksTagUser> list = FksTagUserService.getBean().get(query);
        return list.stream().map(item -> item.getFtu_uid()).collect(Collectors.toList());
    }

    /**
     * 删除标签ID所有关系
     *
     * @param tagId
     */
    public XReturn delTagUser(String tagId) {
        JoinWrapperPlus<FksTagUser> query = listUserQuery(tagId);
        List<FksTagUser> list = FksTagUserService.getBean().get(query);
        if (list.size() > 0) {
            List<String> ids = list.stream().map(item -> item.getFtu_id()).collect(Collectors.toList());
            FksTagUserService.getBean().delete(ids);
        }
        return EnumSys.OK.getR();
    }

    /**
     * 标签编辑
     *
     * @param inputData
     * @param vo
     */
    public XReturn edit(InputData inputData, VaFksTag vo) {
        List<String> ignoreField = new ArrayList<>();

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, true, ignoreField);
            XValidator.err(r);

            FksTag entity = r.getData("entity");
            if (vo.getUid_set() == 1) {
                //删除关系
                delTagUser(entity.getKey());

                //添加关系
                List<FksTagUser> list = new ArrayList<>();
                for (String uid : vo.getUids()) {
                    FksTagUser entityTagUser = new FksTagUser();
                    entityTagUser.setFtu_x_id(XHttpContext.getX());
                    entityTagUser.setFtu_tag_id(entity.getKey());
                    entityTagUser.setFtu_uid(uid);
                    entityTagUser.setAutoID();

                    list.add(entityTagUser);
                }
                FksTagUserService.getBean().saveBatch(list);
            }
            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(FksTagDel vo) {
        XReturn r = XReturn.getR(0);

        JoinWrapperPlus<FksTag> query = listQuery(vo.getUid());
        query.in(FksTag.tableKey(), vo.getIds());
        List<FksTag> listTag = getApp().get(query);

        getApp().delete(listTag.stream().map(item -> item.getKey()).collect(Collectors.joining()));

        return r;
    }
}
