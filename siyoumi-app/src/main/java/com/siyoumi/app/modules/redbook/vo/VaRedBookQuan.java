package com.siyoumi.app.modules.redbook.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaRedBookQuan {
    private String rquan_id;
    private String rquan_app_id;
    private String rquan_group_id;

    //@HasAnyText
    private String rquan_uid;

    private Integer rquan_type;
    @Size(max = 1000)
    private String rquan_url;
    @Size(max = 200)
    private String rquan_thumbnail;
    @HasAnyText
    @Size(max = 50, message = "请输入标题")
    private String rquan_title;
    private String rquan_content;
    @Size(max = 50)
    private String rquan_content_src;
}
