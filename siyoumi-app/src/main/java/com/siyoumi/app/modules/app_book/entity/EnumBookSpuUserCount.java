package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;

//房型
public class EnumBookSpuUserCount
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(1, "单人房");
        put(2, "双人房");
        put(3, "3人房");
        put(4, "4人房");
        put(5, "5人房");
        put(6, "6人房");
        put(7, "7人房");
        put(8, "8人房");
    }
}
