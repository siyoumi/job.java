package com.siyoumi.app.modules.fun_mall.vo;

import com.siyoumi.app.modules.prize.vo.VaSysPrizeSet;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

//商品
@Data
public class VaFunMallPrize
        extends VaSysPrizeSet {
    private Integer fprize_fun;
    private String fprize_group_id;
    private Integer fprize_get_total;
    private LocalDateTime fprize_begin_date;
    @HasAnyText
    private LocalDateTime fprize_end_date;

    private Integer fprize_order;

    private String fprize_acc_id;
}
