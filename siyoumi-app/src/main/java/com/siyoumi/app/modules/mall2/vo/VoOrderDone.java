package com.siyoumi.app.modules.mall2.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

//下单接口
@Data
public class VoOrderDone {
    @HasAnyText(message = "请输入手机号")
    @Size(max = 50)
    String order_id;

    Integer src = 0; //0：用户操作；1：后台管理员操作；2：任务操作；
}
