package com.siyoumi.app.modules.wechat.entity;

import lombok.Data;

@Data
public class WechatGroupChatUser {
    String id;
    String name;

    public static WechatGroupChatUser getIns(String id, String name) {
        WechatGroupChatUser data = new WechatGroupChatUser();
        data.setId(id);
        data.setName(name);
        return data;
    }
}
