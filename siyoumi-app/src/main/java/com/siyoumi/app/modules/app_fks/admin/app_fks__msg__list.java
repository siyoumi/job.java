package com.siyoumi.app.modules.app_fks.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FksMsg;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_fks.entity.EnumFksMsgType;
import com.siyoumi.app.modules.app_fks.service.SvcFksMsg;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__msg__list")
public class app_fks__msg__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        InputData inputData = InputData.fromRequest();
        EnumFksMsgType enumType = XEnumBase.of(EnumFksMsgType.class);

        String type = inputData.input("type");
        setPageTitle(enumType.get(XStr.toInt(type)), "-列表");

        String[] select = {
                "fmsg_id",
                "fmsg_type",
                "fmsg_title",
                "fmsg_type",
                "fmsg_tiao_title",
                "fmsg_tiao_desc",
                "fmsg_src",
                "fmsg_pic",
                "fmsg_release_date",
                "fmsg_order",
        };
        JoinWrapperPlus<FksMsg> query = SvcFksMsg.getBean().listQuery(inputData);
        query.select(select);

        IPage<FksMsg> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcFksMsg.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> item : list) {
            FksMsg entity = SvcFksMsg.getApp().loadEntity(item);
            item.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }

    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcFksMsg.getBean().delete(getIds());
    }
}
