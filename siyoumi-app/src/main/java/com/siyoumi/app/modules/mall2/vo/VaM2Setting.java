package com.siyoumi.app.modules.mall2.vo;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

//基础设置
@Data
public class VaM2Setting {
    private String abc_id;
    private String abc_str_00;
}
