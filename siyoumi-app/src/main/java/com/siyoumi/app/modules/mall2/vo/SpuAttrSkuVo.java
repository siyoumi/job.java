package com.siyoumi.app.modules.mall2.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

//修改商城规格属性
@Data
public class SpuAttrSkuVo {
    String masku_id; //有值，视为编辑
    @NotBlank
    @Size(max = 50)
    String masku_name;
    Integer del = 0;

    @JsonIgnore
    Boolean doSync = false; //false：未同步,true：已同步

    @JsonIgnore
    public Boolean isDelete() {
        return getDel() == 1;
    }
}
