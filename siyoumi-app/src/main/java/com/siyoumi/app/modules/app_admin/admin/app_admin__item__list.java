package com.siyoumi.app.modules.app_admin.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysItem;
import com.siyoumi.app.service.SysItemService;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysApp;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysAppService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/app_admin/app_admin__item__list")
public class app_admin__item__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        if (XStr.isNullOrEmpty(getPid())) {
            return EnumSys.MISS_VAL.getR("应用ID miss");
        }

        SysAppService app = SysAppService.getBean();
        SysApp entityApp = app.loadEntity(getPid());

        setPageTitle(entityApp.getApp_name(), "-活动属性模板-列表");


        String compKw = input("compKw");
        SysItemService appItem = SysItemService.getBean();
        //query
        JoinWrapperPlus<SysItem> query = appItem.join();
        query.eq("item_app_id", entityApp.getApp_id())
                .orderByAsc("item_order")
                .orderByAsc("item_id");
        if (XStr.hasAnyText(compKw)) //名称
        {
            query.like("item_name", compKw);
        }

        IPage<SysItem> page = new Page<>(getPageIndex(), getPageSize());

        IPage<Map<String, Object>> pageData = appItem.getMaps(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<Map<String, Object>> listData = pageData.getRecords();
        for (Map<String, Object> item : listData) {
            SysItem entityItem = SysItemService.getBean().loadEntity(item);
            item.put("id", entityItem.getKey());
        }


        getR().setData("list", listData);
        getR().setData("count", count);

        setPageInfo("entity_app", entityApp);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        XReturn r = XReturn.getR(0);

        SysItemService appItem = SysItemService.getBean();
        List<SysItem> list = appItem.listByIds(getIds());
        appItem.removeBatchByIds(list);

        return r;
    }
}
