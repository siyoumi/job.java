package com.siyoumi.app.modules.app_show.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.math.BigDecimal;

//项目订单
@Data
public class VaJobOrder {
    private String jorder_id;

    @HasAnyText
    @Size(max = 50)
    private String jorder_name;
    @Size(max = 500)
    private String jorder_banner;
    @Size(max = 500)
    private String jorder_desc;
    private Integer jorder_type;
    
    private Integer jorder_state;

    private BigDecimal jorder_price_total;
    private BigDecimal jorder_price_sales;

    private String jorder_src;
    private String jorder_openid;
}
