package com.siyoumi.app.modules.accsuper_admin.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccsuperZzzApp;
import com.siyoumi.entity.SysApp;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysAccsuperZzzAppService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/accsuper_admin/accsuper_admin__app__list")
public class accsuper_admin__app__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        String title = "关联应用列表";
        if (XStr.hasAnyText(getID())) {
            title = XStr.concat(getID(), "关联应用列表");
        }
        setPageTitle(title);


        String compKw = input("compKw");


        SysAccsuperZzzAppService app = SysAccsuperZzzAppService.getBean();
        //query
        JoinWrapperPlus<SysAccsuperZzzApp> query = app.join();
        //join
        query.leftJoin(SysApp.table(), "app_id", "acczapp_app_id");
        query.select("t_s_accsuper_zzz_app.*", "app_name")
                .orderByDesc("acczapp_create_date");
        if (XStr.hasAnyText(getID())) //站点关系应用
        {
            query.eq("acczapp_x_id", getID());
        }
        if (XStr.hasAnyText(compKw)) //应用名称搜索
        {
            query.like("t_s_app.app_name", compKw);
        }

        IPage<SysAccsuperZzzApp> page = new Page<>(getPageIndex(), getPageSize());

        IPage<Map<String, Object>> pageData = app.mapper().getMaps(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<Map<String, Object>> listData = pageData.getRecords();
        List<Map<String, Object>> list = listData.stream().map(item ->
        {
            item.put("id", item.get("acczapp_id"));

            return item;
        }).collect(Collectors.toList());


        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        SysAccsuperZzzAppService app = SysAccsuperZzzAppService.getBean();

        String ids = input("ids");
        if (XStr.isNullOrEmpty(ids)) {
            return XReturn.getR(EnumSys.MISS_VAL.getErrcode(), "miss ids");
        }

        String[] arr = ids.split(",");
        List<String> collect = Arrays.stream(arr).collect(Collectors.toList());
        app.removeBatchByIds(collect);


        return getR();
    }
}
