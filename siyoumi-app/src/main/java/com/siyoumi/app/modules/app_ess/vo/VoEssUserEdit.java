package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class VoEssUserEdit {
    @EqualsValues(vals = {"1", "2", "0"})
    private Integer euser_sex;
    @Size(max = 50)
    private String euser_speciality;
    private LocalDateTime euser_school_date;
    @Size(max = 500)
    private String euser_address;
    @Size(max = 50)
    private String euser_contact;
}
