package com.siyoumi.app.modules.mall2.admin;

import com.siyoumi.app.entity.M2Group;
import com.siyoumi.app.modules.mall2.entity.VaM2Group;
import com.siyoumi.app.modules.mall2.service.SvcM2Group;
import com.siyoumi.app.service.M2GroupService;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.component.XBean;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import com.siyoumi.component.http.InputData;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/mall2/m2__group__edit")
public class m2__group__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商品分类-编辑");

        SvcM2Group app = SvcM2Group.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("mgroup_order", 0);
        if (isAdminEdit()) {
            M2Group entity = app.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaM2Group vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        M2GroupService app = M2GroupService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("mgroup_x_id");
        } else {
            vo.setMgroup_acc_id(getAccId());
        }

        InputData inputData = InputData.fromRequest();
        inputData.putAll(XBean.toMap(vo));
        return app.saveEntity(inputData, vo, true, ignoreField);
    }
}
