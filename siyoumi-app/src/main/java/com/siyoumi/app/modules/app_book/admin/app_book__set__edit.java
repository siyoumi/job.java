package com.siyoumi.app.modules.app_book.admin;

import com.siyoumi.app.entity.BookSet;
import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.modules.app_book.service.SvcBookSet;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.modules.app_book.vo.VaBookSet;
import com.siyoumi.app.modules.app_book.vo.VaBookSpu;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__set__edit")
public class app_book__set__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("套餐-编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("bset_order", 0);
        data.put("bset_day_price_breakfast", 0);
        data.put("bset_day_price_lunch", 0);
        data.put("bset_day_price_dinner", 0);
        if (isAdminEdit()) {
            BookSet entity = SvcBookSet.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaBookSet vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        InputData inputData = InputData.fromRequest();
        //计算总价
        vo.setBset_day_price(vo.getBset_day_price_breakfast().add(vo.getBset_day_price_dinner()).add(vo.getBset_day_price_lunch()));
        //
        if (isAdminAdd()) {
            vo.setBset_acc_id(getAccId());
            vo.setBset_store_id(getStoreId());
        }
        //
        return SvcBookSet.getBean().edit(inputData, vo);
    }
}