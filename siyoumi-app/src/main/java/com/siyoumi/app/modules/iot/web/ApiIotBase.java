package com.siyoumi.app.modules.iot.web;

import com.siyoumi.app.entity.IotDevice;
import com.siyoumi.app.modules.iot.service.SvcIotDevice;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApiIotBase
        extends WxAppApiController {
    
    public IotDevice getDevice(Boolean checkState) {
        String deviceNum = input("device_num");
        String productId = input("product_id");
        if (XStr.isNullOrEmpty(productId)) {
            XValidator.err(EnumSys.MISS_VAL.getR("miss product_id"));
        }
        if (XStr.isNullOrEmpty(deviceNum)) {
            XValidator.err(EnumSys.MISS_VAL.getR("miss device_num"));
        }
        IotDevice entityDevice = SvcIotDevice.getBean().getEntity(productId, deviceNum);
        XReturn r = SvcIotDevice.getBean().checkDevice(entityDevice);
        XValidator.err(r);

        if (checkState) {
            Integer state = SvcIotDevice.getBean().getState(entityDevice);
            if (state != 1) {
                XValidator.err(XReturn.getR(20030, "设备不在线"));
            }
        }

        return entityDevice;
    }
}
