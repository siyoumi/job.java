package com.siyoumi.app.modules.app_fks.service;

import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.app_fks.entity.FksMsgEsData;
import com.siyoumi.app.modules.app_fks.vo.VoFksMsgCollect;
import com.siyoumi.app.modules.app_fks.vo.VaFksMsg;
import com.siyoumi.app.service.*;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//知识库
@Slf4j
@Service
public class SvcFksMsg
        implements IWebService {
    static public SvcFksMsg getBean() {
        return XSpringContext.getBean(SvcFksMsg.class);
    }

    static public FksMsgService getApp() {
        return FksMsgService.getBean();
    }

    public List<FksCollect> getListCollect(List<String> msgIds) {
        if (msgIds.size() <= 0) {
            return new ArrayList<>();
        }
        JoinWrapperPlus<FksCollect> queryCollect = SvcFksMsg.getBean().listCollectQuery(0);
        queryCollect.eq("fc_uid", getUid())
                .in("fc_type_id", msgIds);
        return FksCollectService.getBean().get(queryCollect);
    }

    public JoinWrapperPlus<FksMsg> listApiQuery(InputData inputData) {
        String[] select = {
                "fmsg_id",
                "fmsg_title",
                "fmsg_create_date",
                "fmsg_type",
                "fmsg_pic",
                "fmsg_tiao_title",
                "fmsg_tiao_desc",
                "fmsg_file_json",
        };
        JoinWrapperPlus<FksMsg> query = SvcFksMsg.getBean().listQuery(inputData);
        query.select(select);
        return query;
    }

    public JoinWrapperPlus<FksMsg> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<FksMsg> listQuery(InputData inputData) {
        String title = inputData.input("title");
        String type = inputData.input("type");

        JoinWrapperPlus<FksMsg> query = getApp().join();
        query.eq("fmsg_x_id", XHttpContext.getX());

        query.orderByAsc("fmsg_order").orderByDesc("id");
        if (XStr.hasAnyText(type)) { //类型
            query.eq("fmsg_type", type);
        }
        if (XStr.hasAnyText(title)) { //标题
            query.like("fmsg_title", title);
        }

        return query;
    }

    public XReturn edit(InputData inputData, VaFksMsg vo) {
        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminEdit()) {
            ignoreField.add("lngroup_acc_id");
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, true, ignoreField);
            if (r.ok()) {
                FksMsg entity = r.getData("entity");

                FksMsgEsData data = new FksMsgEsData();
                data.setId(entity.getFmsg_id());
                data.setFmsg_type(entity.getFmsg_type());
                data.setFmsg_title(entity.getFmsg_title());
                data.setFmsg_keyword(entity.getFmsg_keyword());

                SvcFksMsgEs.getBean().edit(data);
            }

            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);
        SvcFksMsgEs.getBean().delete(ids);

        return r;
    }

    public JoinWrapperPlus<FksCollect> listCollectQuery(Integer type) {
        JoinWrapperPlus<FksCollect> query = FksCollectService.getBean().join();
        query.eq("fc_type", type);
        return query;
    }

    public FksCollect getEntityCollect(Integer type, String typeId, String uid) {
        JoinWrapperPlus<FksCollect> query = listCollectQuery(type);
        query.eq("fc_x_id", XHttpContext.getX()).eq("fc_type_id", typeId).eq("fc_uid", uid);
        return FksCollectService.getBean().first(query);
    }

    /**
     * 收藏知识库
     *
     * @param vo
     */
    @Transactional(rollbackFor = Exception.class)
    public XReturn collect(VoFksMsgCollect vo) {
        FksMsg entityMsg = getApp().getEntity(vo.getMsg_id());
        if (entityMsg == null) {
            return EnumSys.ERR_VAL.getR("知识库ID异常");
        }

        FksCollect entityCollect = getEntityCollect(0, vo.getMsg_id(), vo.getUid());
        if (vo.getCollect() == 1) {
            //收藏
            if (entityCollect != null) {
                return XReturn.getR(0, "已收藏");
            }

            FksCollect entity = new FksCollect();
            entity.setFc_type(0);
            entity.setFc_type_id(vo.getMsg_id());
            entity.setFc_uid(vo.getUid());
            entity.setFc_x_id(XHttpContext.getX());
            entity.setAutoID();
            FksCollectService.getBean().save(entity);
        } else {
            //取消收藏
            if (entityCollect == null) {
                return XReturn.getR(0, "已取消收藏");
            }

            FksCollectService.getBean().delete(entityCollect.getFc_id());
        }

        return EnumSys.OK.getR();
    }
}
