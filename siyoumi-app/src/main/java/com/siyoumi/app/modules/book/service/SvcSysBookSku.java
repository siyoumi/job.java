package com.siyoumi.app.modules.book.service;

import com.siyoumi.app.entity.SysBookSku;
import com.siyoumi.app.entity.SysBookSkuDay;
import com.siyoumi.app.modules.book.entity.BookSkuTime;
import com.siyoumi.app.modules.book.vo.VaBookSku;
import com.siyoumi.app.service.BookSkuService;
import com.siyoumi.app.service.SysBookSkuService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

//sku
@Slf4j
@Service
public class SvcSysBookSku
        implements IWebService {
    static public SvcSysBookSku getBean() {
        return XSpringContext.getBean(SvcSysBookSku.class);
    }

    static public SysBookSkuService getApp() {
        return SysBookSkuService.getBean();
    }

    public JoinWrapperPlus<SysBookSku> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * 当天截止时间
     *
     * @param entitySku
     * @param beforeMinute
     */
    public LocalDateTime getDateTimeEnd(SysBookSku entitySku, String date, Long beforeMinute) {
        BookSkuTime timeData = BookSkuTime.parse(entitySku.getBsku_name());
        LocalDateTime dateTime = XDate.parse(date + " " + timeData.getBeginTime() + ":00");
        //提交X分钟
        LocalDateTime dateTimeEnd = dateTime.minusMinutes(beforeMinute);
        return dateTimeEnd;
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<SysBookSku> listQuery(InputData inputData) {
        String bookId = inputData.input("book_id");
        String name = inputData.input("name");

        JoinWrapperPlus<SysBookSku> query = getApp().join();
        query.eq("bsku_x_id", XHttpContext.getX())
                .eq("bsku_del", 0)
        //.eq("book_del", 0)
        ;
        query.orderByAsc("bsku_order")
                .orderByAsc("bsku_create_date");
        if (XStr.hasAnyText(bookId)) { //ID
            query.eq("bsku_book_id", bookId);
        }
        if (XStr.hasAnyText(name)) {
            query.like("bsku_name", name);
        }

        return query;
    }

    /**
     * 编辑
     *
     * @param inputData
     * @param vo
     */
    @SneakyThrows
    public XReturn edit(InputData inputData, VaBookSku vo) {
        XReturn r = getApp().saveEntity(inputData, vo, true, null);
        return r;
    }

    /**
     * 删除
     */
    public XReturn delete(List<String> ids) {
        JoinWrapperPlus<SysBookSku> query = listQuery();
        query.in(SysBookSku.tableKey(), ids)
                .eq("bsku_del", 0);

        List<SysBookSku> list = getApp().get(query);

        XApp.getTransaction().execute(status -> {
            for (SysBookSku entity : list) {
                getApp().delete(entity.getKey());
            }

            //删除所有日期
            JoinWrapperPlus<SysBookSkuDay> queryDay = SvcSysBookSkuDate.getBean().listQuery();
            queryDay.in("bday_sku_id", ids);
            List<SysBookSkuDay> listDay = SvcSysBookSkuDate.getApp().get(queryDay);
            for (SysBookSkuDay entityDay : listDay) {
                SvcSysBookSkuDate.getApp().delete(entityDay.getKey());
            }
            return null;
        });

        return XReturn.getR(0);
    }
}
