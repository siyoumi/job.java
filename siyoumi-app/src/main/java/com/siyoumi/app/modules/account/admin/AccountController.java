package com.siyoumi.app.modules.account.admin;

import com.siyoumi.controller.AdminApiController;
import com.siyoumi.service.SysAccountService;

public class AccountController
        extends AdminApiController {
    protected SysAccountService getAppAcc() {
        return SysAccountService.getBean();
    }

    protected Boolean isSuperAdmin() {
        return getAppAcc().isSpuerAdminOrDev(getAppAcc().getSysAccountByToken());
    }
}
