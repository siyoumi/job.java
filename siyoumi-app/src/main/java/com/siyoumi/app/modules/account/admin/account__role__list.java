package com.siyoumi.app.modules.account.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.entity.SysRole;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysRoleService;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/account/account__role__list")
public class account__role__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("角色-列表");

        String compKw = input("compKw");

        SysRoleService app = SysRoleService.getBean();

        //query
        JoinWrapperPlus<SysRole> query = app.getListQuery(XHttpContext.InputAll());

        IPage<SysRole> page = new Page<>(getPageIndex(), getPageSize());

        IPage<Map<String, Object>> pageData = app.mapper().getMaps(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<Map<String, Object>> list = pageData.getRecords();
        for (Map<String, Object> data : list) {
            SysRole entity = SysRoleService.getBean().loadEntity(data);
            data.put("id", entity.getKey());
            //系统角色
            Boolean sysRole = XStr.isNullOrEmpty(entity.getRole_x_id());
            data.put("sys_role", sysRole);
        }

        getR().setData("list", list);
        getR().setData("count", count);

        setPageInfo("master", SysAccsuperConfig.master(XHttpContext.getX()));

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        String ids = input("ids");
        if (XStr.isNullOrEmpty(ids)) {
            return XReturn.getR(EnumSys.MISS_VAL.getErrcode(), "miss ids");
        }

        SysRoleService app = SysRoleService.getBean();

        String[] arr = ids.split(",");
        List<String> arrId = Arrays.stream(arr).collect(Collectors.toList());
        List<SysRole> list = app.listByIds(arrId);
        for (SysRole item : list) {
            if (app.existSetAcc(item.getRole_id())) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return XReturn.getR(20086, XStr.concat("有账号设置此权限，表先删除帐号", "[", item.getRole_name(), "]"));
            }
            //删除
            app.removeById(item.getKey());
        }

        return getR();
    }
}
