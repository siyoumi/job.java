package com.siyoumi.app.modules.app_book.service;

import com.siyoumi.aspect.RequestLimitGetKey;
import com.siyoumi.component.http.InputData;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XStr;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;

//默认key 类名 + 方法名
@Service
public class RequestLimitAppBook
        implements RequestLimitGetKey {
    @Override
    public String getKey(ProceedingJoinPoint point) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();

        String key = method.getDeclaringClass().getSimpleName() + "." + method.getName();

        switch (method.getName()) {
            case "order": //下单
                InputData inputData = InputData.fromRequest();
                String spuId = inputData.input("spu_id");
                if (XStr.hasAnyText(spuId)) {
                    key += "." + spuId;
                }
                break;
        }

        return key;
    }
}
