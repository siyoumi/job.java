package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysSale;
import com.siyoumi.app.entity.SysSaleClient;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_book.entity.EnumBookSaleLevel;
import com.siyoumi.app.modules.user.entity.EnumSaleState;
import com.siyoumi.app.modules.user.service.SvcSysSale;
import com.siyoumi.app.modules.user.vo.VoSaleAudit;
import com.siyoumi.app.service.SysSaleClientService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__sale_client__list")
public class app_book__sale_client__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("客户关系列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "sacl_id",
                "sacl_create_date",
                "sale_name",
                "sale_uid",
                "sale_level",
                "user_id",
                "user_name",
        };
        JoinWrapperPlus<SysSaleClient> query = SvcSysSale.getBean().listClientQuery(inputData);
        query.join(SysSale.table(), "sale_uid", "sacl_sale_uid");
        query.join(SysUser.table(), SysUser.tableKey(), "sacl_client_uid");
        query.select(select);
        query.eq("sale_type", "");

        IPage<SysSaleClient> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SysSaleClientService.getBean().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumBookSaleLevel enumSaleLevel = XEnumBase.of(EnumBookSaleLevel.class);
        //EnumSaleState enumSaleState = XEnumBase.of(EnumSaleState.class);
        for (Map<String, Object> item : list) {
            SysSaleClient entity = XBean.fromMap(item, SysSaleClient.class);
            item.put("id", entity.getKey());

            SysSale entitySale = XBean.fromMap(item, SysSale.class);
            item.put("level", enumSaleLevel.get(entitySale.getSale_level())); //等级
        }

        getR().setData("list", list);
        getR().setData("count", count);

        setPageInfo("enum_sale_level", enumSaleLevel);
        //setPageInfo("enum_sale_state", enumSaleState);


        return getR();
    }

    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcSysSale.getBean().deleteClient(getIds());
    }
}
