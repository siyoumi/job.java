package com.siyoumi.app.modules.app_ess.admin;

import com.siyoumi.app.entity.EssFile;
import com.siyoumi.app.modules.app_ess.entity.EnumEssFileType;
import com.siyoumi.app.modules.app_ess.entity.EnumEssFileTypeSys;
import com.siyoumi.app.modules.app_ess.service.SvcEssFile;
import com.siyoumi.app.modules.app_ess.service.SvcEssModule;
import com.siyoumi.app.modules.app_ess.vo.VoEssFileAdd;
import com.siyoumi.app.modules.app_ess.vo.VaEssFile;
import com.siyoumi.app.sys.service.CommonApiServcie;
import com.siyoumi.app.sys.vo.VoUploadFile;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__file__edit")
public class app_ess__file__edit
        extends AdminApiController {

    @GetMapping()
    public XReturn index() {
        setPageTitle("文件添加");

        String moduleId = input("module_id");
        Map<String, Object> data = new HashMap<>();
        data.put("efile_module_id", moduleId);
        data.put("efile_file_size", 0);
        data.put("efile_order", 0);
        data.put("efile_type_sys", "0");
        if (isAdminEdit()) {
            EssFile entity = SvcEssFile.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
            data.put("efile_type_sys", entity.getEfile_type_sys().toString());
        }
        getR().setData("data", data);

        //数据分类
        EnumEssFileType enumFileType = XEnumBase.of(EnumEssFileType.class);
        setPageInfo("enum_file_type", enumFileType);
        //类型
        EnumEssFileTypeSys enumFileTypeSys = XEnumBase.of(EnumEssFileTypeSys.class);
        setPageInfo("enum_file_type_sys", enumFileTypeSys);
        //
        setPageInfo("list_module", SvcEssModule.getBean().getList()); //模块列表
        setPageInfo("file_accept", SvcEssFile.fileAccept().stream()
                .map(item -> XStr.concat(".", item))
                .collect(Collectors.joining(","))); //允许上传的文件格式

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaEssFile vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        vo.setEfile_acc_id(getAccId());
        //
        InputData inputData = InputData.fromRequest();
        return SvcEssFile.getBean().edit(inputData, vo);
    }


    @PostMapping({"/upload_file"})
    public XReturn uploadFile(@Validated VoUploadFile vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true, true);

        List<String> ex = SvcEssFile.fileAccept();
        return CommonApiServcie.getBean().uploadFile(vo, ex);
    }


    //批量添加文件
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/file_add")
    public XReturn fileAdd(@Validated @RequestBody VoEssFileAdd vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        InputData inputData = InputData.fromRequest();
        //
        for (VaEssFile file : vo.getFiles()) {
            file.setEfile_acc_id(getAccId());
            file.setEfile_module_id(vo.getEfile_module_id());
            file.setEfile_type(vo.getEfile_type());

            XReturn r = SvcEssFile.getBean().edit(inputData, file);
            if (r.err()) {
                r.setErrMsg(file.fileName(), ":", r.getErrMsg());
            }
            XValidator.err(r);
        }

        return EnumSys.OK.getR();
    }
}
