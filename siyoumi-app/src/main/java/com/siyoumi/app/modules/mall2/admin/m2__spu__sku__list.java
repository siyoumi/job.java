package com.siyoumi.app.modules.mall2.admin;

import com.siyoumi.app.entity.M2Spu;
import com.siyoumi.app.modules.mall2.service.SvcM2Sku;
import com.siyoumi.app.modules.mall2.service.SvcM2Spu;
import com.siyoumi.app.modules.mall2.vo.SkuSetStockVo;
import com.siyoumi.app.modules.mall2.vo.SpuAttrData;
import com.siyoumi.app.modules.mall2.vo.SpuAttrVo;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/xadmin/mall2/m2__spu__sku__list")
public class m2__spu__sku__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        String spuId = input("spu_id");
        if (XStr.isNullOrEmpty(spuId)) {
            return EnumSys.MISS_VAL.getR("miss spu_id");
        }

        M2Spu entitySpu = SvcM2Spu.getApp().getEntity(spuId);
        setPageTitle(entitySpu.getMspu_name(), "商品规格列表");

        SvcM2Spu appSpu = SvcM2Spu.getBean();

        //规格
        List<SpuAttrVo> listAttrSku = appSpu.getListAttrSku(entitySpu);
        //规格sku
        List<SpuAttrData> listAttrData = appSpu.getListAttrData(listAttrSku, true);

        getR().setData("list", listAttrData);
        getR().setData("count", listAttrData.size());

        setPageInfo("spu", entitySpu);
        setPageInfo("can_edit", entitySpu.canEdit());
        setPageInfo("list_attr_sku", listAttrSku);

        return getR();
    }


    /**
     * 上下架
     */
    @RequestMapping("m2mall__spu_attr__sku__list__set_enable")
    @Transactional(rollbackFor = Exception.class)
    public Object setEnable(List<String> ids) {
        String enable = input("enable");
        if (XStr.isNullOrEmpty(enable)) {
            return EnumSys.MISS_VAL.getR("miss enable");
        }
        Integer enableInt = XStr.toInt(enable, 0);

        SvcM2Sku app = SvcM2Sku.getBean();
        return app.setEnable(ids, enableInt);
    }

    /**
     * 库存
     */
    @PostMapping("m2mall__spu_attr__sku__list__set_stock")
    @Transactional(rollbackFor = Exception.class)
    public Object setStock(@RequestBody List<SkuSetStockVo> listVo) {
        if (listVo.size() <= 0) {
            return XReturn.getR(20061, "size 0");
        }

        SvcM2Sku app = SvcM2Sku.getBean();
        return app.setStock(listVo);
    }
}
