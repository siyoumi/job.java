package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.BookStore;
import com.siyoumi.app.modules.app_book.entity.EnumBookStoreEnable;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.app.modules.app_book.vo.VoBookSpuAudit;
import com.siyoumi.app.sys.entity.EnumEnable;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__store__list")
public class app_book__store__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商家列表");

        InputData inputData = InputData.fromRequest();


        JoinWrapperPlus<BookStore> query = SvcBookStore.getBean().listQuery(inputData);

        IPage<BookStore> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcBookStore.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumBookStoreEnable enumEnable = XEnumBase.of(EnumBookStoreEnable.class); //上下架
        for (Map<String, Object> item : list) {
            BookStore entity = SvcBookStore.getApp().loadEntity(item);
            item.put("id", entity.getKey());
            //状态
            item.put("enable", enumEnable.get(entity.getBstore_enable()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcBookStore.getBean().delete(getIds());
    }


    //上下架
    @Transactional
    @PostMapping("/audit")
    public XReturn audit(@Validated VoBookSpuAudit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        return SvcBookStore.getBean().audit(vo);
    }
}
