package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.app.entity.FksFile;
import com.siyoumi.app.entity.FksUser;
import lombok.Data;

//文件权限验证
@Data
public class FksFileAuthHandleData {
    FksFile entityFile;
    FksUser entityUser;

    public static FksFileAuthHandleData of(FksFile entityFile, FksUser entityUser) {
        FksFileAuthHandleData data = new FksFileAuthHandleData();
        data.setEntityFile(entityFile);
        data.setEntityUser(entityUser);
        return data;
    }
}
