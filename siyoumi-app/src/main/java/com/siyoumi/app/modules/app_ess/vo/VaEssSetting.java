package com.siyoumi.app.modules.app_ess.vo;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaEssSetting {
    @Size(max = 2000)
    String abc_txt_00;
}
