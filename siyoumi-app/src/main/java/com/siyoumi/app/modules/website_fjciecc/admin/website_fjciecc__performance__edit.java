package com.siyoumi.app.modules.website_fjciecc.admin;

import com.siyoumi.app.entity.SysItem;
import com.siyoumi.app.sys.vo.CommonItemData;
import com.siyoumi.app.modules.website_fjciecc.entity.EnumFjcieccPerformance;
import com.siyoumi.app.modules.website_fjciecc.service.WebSiteFjcieccEditBase;
import com.siyoumi.app.modules.website_fjciecc.vo.VaFjcieccPerFormance;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.app.service.SysItemService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@Slf4j
@RestController
@RequestMapping("/xadmin/website_fjciecc/website_fjciecc__performance__edit")
public class website_fjciecc__performance__edit
        extends WebSiteFjcieccEditBase {
    @Override
    public String title() {
        return "典型业绩-编辑";
    }

    @Override
    public String abcTable() {
        return "fjciecc_performance";
    }

    @Override
    public void indexBefore(Map<String, Object> data) {
        if (isAdminAdd()) {
            data.put("abc_str_01", XApp.getStrID());
        }
    }

    @Override
    public void indexAfter() {
        //类型
        LinkedHashMap<String, String> types = IEnum.toMap(EnumFjcieccPerformance.class);
        setPageInfo("types", types);
    }

    @GetMapping()
    public XReturn index() {
        return super.index();
    }


    @PostMapping("/save")
    public XReturn save(@Validated() VaFjcieccPerFormance vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAbcService svcAbc = SysAbcService.getBean();

        vo.setAbc_table(abcTable());
        vo.setAbc_app_id(XHttpContext.getAppId());

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
            ignoreField.add("abc_app_id");
        }

        InputData inputData = InputData.fromRequest();

        if (XStr.hasAnyText(vo.getAbc_str_01())) {
            log.debug("保存轮播图第1张图片");
            SysItem entityItem = SysItemService.getBean().getEntityByIdSrc(vo.getAbc_str_01());
            if (entityItem != null) {
                List<CommonItemData> commonItemData = entityItem.commonData();
                if (commonItemData.size() > 0) {
                    vo.setAbc_str_00(commonItemData.get(0).getPic());
                }
            }
        }

        return svcAbc.saveEntity(inputData, vo, true, ignoreField);
    }
}
