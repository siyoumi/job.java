package com.siyoumi.app.modules.app_book.service.handle_order;

import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.app.entity.SysMaster;
import com.siyoumi.app.modules.app_book.service.BookOrderHandleAbs;
import com.siyoumi.app.modules.app_book.service.SvcBookOrder;
import com.siyoumi.app.modules.app_book.vo.VoBookOrder;
import com.siyoumi.app.modules.user.service.SvcSysMaster;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;

public class BookOrderHandle10GiveFun
        extends BookOrderHandleAbs
        implements IWebService {
    @Override
    public XReturn orderBeforeCheck() {
        return EnumSys.OK.getR();
    }

    @Override
    public String getTitle() {
        return "积分赠送";
    }

    @Override
    public XReturn handle(BookOrder entityOrder) {
        Boolean existOrder = SvcBookOrder.getBean().existOrderFirstEnable(entityOrder.getBorder_uid());
        String masterUid = "";
        entityOrder.setBorder_give_fun(0L);
        if (!existOrder) {
            LogPipeLine.getTl().setLogMsg("积分赠送->第1次下单");
            SysMaster entityMaster = SvcSysMaster.getBean().getEntity(getOpenid());
            if (entityMaster != null) {
                LogPipeLine.getTl().setLogMsg("积分赠送->有主人");
                masterUid = entityMaster.getMast_uid();
                entityOrder.setBorder_give_uid(masterUid);
                entityOrder.setBorder_give_fun(SvcBookOrder.getBean().giveFunNum()); //标记加1千积分
            }
        } else {
            LogPipeLine.getTl().setLogMsg("积分赠送->非第1次下单");
        }
        entityOrder.setBorder_give_uid(masterUid);

        return EnumSys.OK.getR();
    }
}
