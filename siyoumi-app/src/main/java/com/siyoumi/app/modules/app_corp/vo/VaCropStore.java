package com.siyoumi.app.modules.app_corp.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class VaCropStore {
    private String cstore_openid;
    @NotBlank
    @Size(max = 50)
    private String cstore_name;
    @NotBlank
    @Size(max = 50)
    private String cstore_wifi_ssid;
    @NotBlank
    @Size(max = 20, min = 8, message = "wifi密码8-20位")
    private String cstore_wifi_pwd;
    @Size(max = 50)
    private String cstore_wifi_bssid;
    @NotBlank
    @Size(max = 200)
    private String cstore_pic_h5;

    private Integer cstore_enable;
    private LocalDateTime cstore_enable_date;

    private Integer cstore_order;
}
