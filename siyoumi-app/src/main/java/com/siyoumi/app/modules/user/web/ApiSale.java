package com.siyoumi.app.modules.user.web;

import com.siyoumi.app.modules.user.service.SvcSysSale;
import com.siyoumi.app.modules.user.vo.VoSaleGetMoney;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/wxapp/user/api_sale")
public class ApiSale
        extends WxAppApiController {
    /**
     * 提现
     */
    @GetMapping("/get_money")
    public XReturn getMoney(@Validated VoSaleGetMoney vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        vo.setUid(getUid());
        return SvcSysSale.getBean().getMoney(vo);
    }


}
