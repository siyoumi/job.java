package com.siyoumi.app.modules.app_ess.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.EssFile;
import com.siyoumi.app.entity.EssFileType;
import com.siyoumi.app.entity.EssModule;
import com.siyoumi.app.modules.app_ess.entity.EnumEssFileType;
import com.siyoumi.app.modules.app_ess.entity.EnumEssFileTypeSys;
import com.siyoumi.app.modules.app_ess.service.SvcEssFile;
import com.siyoumi.app.modules.app_ess.service.SvcEssModule;
import com.siyoumi.app.modules.app_ess.service.SvcEssUser;
import com.siyoumi.app.sys.service.FileHandle;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XFile;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__file__list")
public class app_ess__file__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("资源文件列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "efile_id",
                "efile_name",
                "efile_create_date",
                "efile_type",
                "efile_type_sys",
                "efile_path",
                "efile_file_size",
                "efile_file_ext",
                "efile_collect_count",
                "efile_order",
                "emod_name",
        };
        JoinWrapperPlus<EssFile> query = SvcEssFile.getBean().listQuery(inputData);
        query.join(EssModule.table(), EssModule.tableKey(), "efile_module_id");
        query.select(select);

        IPage<EssFile> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcEssFile.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumEssFileType enumFileType = XEnumBase.of(EnumEssFileType.class);
        EnumEssFileTypeSys enumFileTypeSys = XEnumBase.of(EnumEssFileTypeSys.class);
        for (Map<String, Object> item : list) {
            EssFile entity = XBean.fromMap(item, EssFile.class);
            item.put("id", entity.getKey());
            //类型
            item.put("type", enumFileType.get(entity.getEfile_type()));
            //类型
            item.put("type_sys", enumFileTypeSys.get(entity.getEfile_type_sys()));
            //访问路径
            item.put("file_url", XApp.fileUrl(entity.getEfile_path()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        setPageInfo("enum_file_type", enumFileType); //文件分类
        setPageInfo("enum_file_type_sys", enumFileTypeSys); //文件类型
        setPageInfo("list_module", SvcEssModule.getBean().getList()); //模块列表

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcEssFile.getBean().delete(getIds());
    }
}
