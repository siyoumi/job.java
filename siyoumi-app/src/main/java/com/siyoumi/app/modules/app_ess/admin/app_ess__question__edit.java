package com.siyoumi.app.modules.app_ess.admin;

import com.siyoumi.app.entity.EssQuestion;
import com.siyoumi.app.modules.app_ess.entity.EnumEssQuestionLevel;
import com.siyoumi.app.modules.app_ess.entity.EnumEssQuestionType;
import com.siyoumi.app.modules.app_ess.entity.EnumEssType;
import com.siyoumi.app.modules.app_ess.service.SvcEssModule;
import com.siyoumi.app.modules.app_ess.service.SvcEssQuestion;
import com.siyoumi.app.modules.app_ess.vo.VoEssQuestion;
import com.siyoumi.app.modules.app_ess.vo.VoEssQuestionAddBatch;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XFile;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__question__edit")
public class app_ess__question__edit
        extends AdminApiController {

    @GetMapping()
    public XReturn index() {
        setPageTitle("题目-编辑");

        List<String> questionItem = new ArrayList<>(); //题目选项
        List<Integer> answer = new ArrayList<>(); //答案

        Map<String, Object> data = new HashMap<>();
        data.put("etqu_level", "0");
        data.put("etqu_question_type", "0");
        data.put("etqu_fun", 2);
        data.put("etqu_order", 0);
        if (isAdminEdit()) {
            EssQuestion entity = SvcEssQuestion.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            dataAppend.put("etqu_type", entity.getEtqu_type().toString());
            dataAppend.put("etqu_level", entity.getEtqu_level().toString());
            dataAppend.put("etqu_question_type", entity.getEtqu_question_type().toString());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);

            questionItem = XJson.parseArray(entity.getEtqu_question_item(), String.class);
            answer = XJson.parseArray(entity.getEtqu_answer(), Integer.class);
        }
        data.put("question_item", questionItem);
        data.put("answer", answer);

        getR().setData("data", data);

        EnumEssQuestionLevel enumQuestionLevel = XEnumBase.of(EnumEssQuestionLevel.class);
        EnumEssQuestionType enumQuestionType = XEnumBase.of(EnumEssQuestionType.class);
        EnumEssType enumType = XEnumBase.of(EnumEssType.class);
        setPageInfo("list_module", SvcEssModule.getBean().getList()); //模块列表
        setPageInfo("enum_type", enumType); //题库类型
        setPageInfo("enum_question_level", enumQuestionLevel); //题目难度等级
        setPageInfo("enum_question_type", enumQuestionType); //类型
        //导入模板
        setPageInfo("import_url", XApp.fileUrl("res/import/导入题目.xls"));

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated VoEssQuestion vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        if (vo.getAnswer().isEmpty()) {
            result.addError(XValidator.getErr("answer", "请选择答案"));
        }
        XValidator.getResult(result);
        //
        vo.setEtqu_acc_id(getAccId());
        //
        InputData inputData = InputData.fromRequest();
        return SvcEssQuestion.getBean().edit(inputData, vo);
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/add")
    public XReturn add(@Validated @RequestBody VoEssQuestionAddBatch vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        vo.setEtqu_acc_id(getAccId());
        //
        return SvcEssQuestion.getBean().addBatch(vo);
    }
}