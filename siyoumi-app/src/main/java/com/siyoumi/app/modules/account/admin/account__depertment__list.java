package com.siyoumi.app.modules.account.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysDepartment;
import com.siyoumi.app.modules.account.service.SvcDepartment;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.entity.SysRole;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysRoleService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/account/account__depertment__list")
public class account__depertment__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("部门-列表");

        InputData inputData = InputData.fromRequest();
        //query
        JoinWrapperPlus<SysDepartment> query = SvcDepartment.getBean().listQuery(inputData);

        IPage<SysDepartment> page = new Page<>(getPageIndex(), getPageSize());
        IPage<Map<String, Object>> pageData = SvcDepartment.getApp().getMaps(page, query);
        //数量
        long count = pageData.getTotal();
        //列表数据处理
        List<Map<String, Object>> list = pageData.getRecords();
        for (Map<String, Object> data : list) {
            SysDepartment entity = SvcDepartment.getApp().loadEntity(data);
            data.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcDepartment.getBean().delete(getIds());
    }
}
