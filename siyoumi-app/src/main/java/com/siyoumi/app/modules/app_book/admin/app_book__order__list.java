package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.modules.app_book.entity.EnumBookOrderStatus;
import com.siyoumi.app.modules.app_book.entity.EnumBookSpuUserCount;
import com.siyoumi.app.modules.app_book.service.SvcBookOrder;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.app.modules.app_book.vo.VaBookOrderCheckin;
import com.siyoumi.app.modules.app_book.vo.VaBookOrderCheckout;
import com.siyoumi.app.modules.app_book.vo.VaBookOrderConfirm;
import com.siyoumi.app.modules.app_book.vo.VoBookSpuAudit;
import com.siyoumi.app.sys.entity.EnumEnable;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__order__list")
public class app_book__order__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("房源列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "border_id",
                "border_create_date",
                "border_date_begin",
                "border_date_end",
                "border_user_name",
                "border_user_phone",
                "border_day",
                "border_set_name",
                "border_spu_count",
                "border_sku_user_count",
                "border_set_user_count",
                "border_pay_deposit",
                "border_price_final",
                "border_price_pay",
                "border_status",
                "border_desc",
                "bspu_name",
        };
        JoinWrapperPlus<BookOrder> query = SvcBookOrder.getBean().listQuery(inputData);
        query.join(BookSpu.table(), BookSpu.tableKey(), "border_spu_id");
        query.eq("border_store_id", getStoreId())
                .eq("border_enable",1);
        query.select(select);

        IPage<BookOrder> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcBookOrder.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumBookOrderStatus enumOrderStatus = XEnumBase.of(EnumBookOrderStatus.class);
        for (Map<String, Object> item : list) {
            BookOrder entity = XBean.fromMap(item, BookOrder.class);
            item.put("id", entity.getKey());
            //订单日期
            item.put("date_begin", XDate.toDateString(entity.getBorder_date_begin()));
            item.put("date_end", XDate.toDateString(entity.getBorder_date_end()));
            //状态
            item.put("status", enumOrderStatus.get(entity.getBorder_status()));
            //未付金额
            item.put("pay_price_left", entity.getBorder_price_final().subtract(entity.getBorder_price_pay()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        //房源列表
        List<Map<String, Object>> listSpu = SvcBookSpu.getBean().getList(getStoreId());
        setPageInfo("list_spu", listSpu);

        return getR();
    }


    //订单确认
    @Transactional
    @PostMapping("/confirm")
    public XReturn confirm(@Validated VaBookOrderConfirm vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        return SvcBookOrder.getBean().confirm(vo);
    }

    //订单入住
    @Transactional
    @PostMapping("/checkin")
    public XReturn checkin(@Validated VaBookOrderCheckin vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        return SvcBookOrder.getBean().checkin(vo);
    }

    //订单退房
    @Transactional
    @PostMapping("/checkout")
    public XReturn checkout(@Validated VaBookOrderCheckout vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        return SvcBookOrder.getBean().checkout(vo);
    }
}
