package com.siyoumi.app.modules.account.admin;

import com.siyoumi.app.modules.account.entity.VaSysRole;
import com.siyoumi.app.modules.account.service.SvcAccount;
import com.siyoumi.app.sys.vo.ApprVo;
import com.siyoumi.component.XApp;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.entity.SysRole;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.SysRoleService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/account/account__role__edit")
public class account__role__edit
        extends AccountController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("角色-编辑");

        Boolean canEdit = true;

        List<String> pathMap = new ArrayList<>();
        Map<String, Object> data = new HashMap<>();
        data.put("role_order", 0);
        data.put("system_role", 0); //1:系统角色
        if (isAdminEdit()) {
            SysRoleService app = SysRoleService.getBean();
            SysRole entity = app.loadEntity(getID());

            HashMap<String, Object> dataAppend = new HashMap<>();
            dataAppend.put("id", entity.getKey());

            //合并
            data = entity.toMap();
            data.putAll(dataAppend);

            data.put("system_role", XStr.isNullOrEmpty(entity.getRole_x_id()) ? 1 : 0);

            pathMap = app.getPathsByRole(entity.getKey(), false);


            if (XStr.isNullOrEmpty(entity.getRole_x_id())
                    && !SysAccsuperConfig.master(XHttpContext.getX())) {
                //系统角色，只有x站点可以编辑
                canEdit = false;
            }
        }
        data.put("do_set_tree", 0); //是否已设置权限

        getR().setData("data", data);


        setPageInfo("r_path", pathMap); //已勾选权限数组

        //站点权限
        SvcAccount appAcc = SvcAccount.getBean();
        List<ApprVo> listApprVo = appAcc.listSetRoleApprVo();
        setPageInfo("appr", listApprVo);

        setPageInfo("master", SysAccsuperConfig.master(XHttpContext.getX()));
        setPageInfo("can_edit", canEdit);

        return getR();
    }


    @PostMapping("/save")
    public XReturn save(@Validated VaSysRole vo, BindingResult result) {
        if (!isSuperAdmin()) {
            return EnumSys.ADMIN_AUTH_SUPER_ERR.getR();
        }

        if (vo.getSystem_role() == 1) {
            if (!SysAccsuperConfig.master(XHttpContext.getX())) {
                return XReturn.getR(20079, "非中心站点，无法逻辑系统角色");
            }
        }

        //统一验证
        XValidator.getResult(result);

        SysRoleService app = SysRoleService.getBean();
        InputData inputData = InputData.fromRequest();

        List<String> ignoreField = new ArrayList<>();

        return XApp.getTransaction().execute(status -> app.saveEntity(inputData, vo, true, ignoreField));
    }
}
