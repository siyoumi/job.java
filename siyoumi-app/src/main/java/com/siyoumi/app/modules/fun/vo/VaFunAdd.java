package com.siyoumi.app.modules.fun.vo;

import lombok.Data;

//添加积分
@Data
public class VaFunAdd {
    String key;
    String uid;
    String idSrc;
    String appId;
    Integer num;
    String desc;
    String str00;
    String str01;

    public static VaFunAdd of(String key, String uid, String idSrc, String appId, Integer num, String desc, String str00, String str01) {
        VaFunAdd data = new VaFunAdd();
        data.setKey(key);
        data.setUid(uid);
        data.setIdSrc(idSrc);
        data.setAppId(appId);
        data.setNum(num);
        data.setDesc(desc);
        data.setStr00(str00);
        data.setStr01(str01);
        return data;
    }
}
