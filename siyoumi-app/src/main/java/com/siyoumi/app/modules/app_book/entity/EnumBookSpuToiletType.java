package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;

//厕所
public class EnumBookSpuToiletType
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "独立马桶");
        put(1, "独立蹲厕");
        put(2, "公用蹲厕");
    }
}
