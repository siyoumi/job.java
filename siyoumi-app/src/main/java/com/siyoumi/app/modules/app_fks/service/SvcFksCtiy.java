package com.siyoumi.app.modules.app_fks.service;

import com.siyoumi.app.entity.FksCity;
import com.siyoumi.app.entity.FksPosition;
import com.siyoumi.app.modules.app_fks.vo.VaFksCity;
import com.siyoumi.app.modules.app_fks.vo.VaFksPosition;
import com.siyoumi.app.service.FksCityService;
import com.siyoumi.app.service.FksPositionService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//城市
@Slf4j
@Service
public class SvcFksCtiy
        implements IWebService {
    static public SvcFksCtiy getBean() {
        return XSpringContext.getBean(SvcFksCtiy.class);
    }

    static public FksCityService getApp() {
        return FksCityService.getBean();
    }

    public List<Map<String, Object>> getList() {
        JoinWrapperPlus<FksCity> query = listQuery();
        query.select("fcity_id", "fcity_name");
        return getApp().getMaps(query);
    }

    public JoinWrapperPlus<FksCity> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<FksCity> listQuery(InputData inputData) {
        String name = inputData.input("name");

        JoinWrapperPlus<FksCity> query = getApp().join();
        query.eq("fcity_x_id", XHttpContext.getX());
        //排序
        query.orderByAsc("fcity_order")
                .orderByDesc("fcity_id");
        if (XStr.hasAnyText(name)) { //名称
            query.like("fcity_name", name);
        }

        return query;
    }

    public XReturn edit(InputData inputData, VaFksCity vo) {
        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminEdit()) {
            ignoreField.add("fcity_acc_id");
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, false, ignoreField);
            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }
}
