package com.siyoumi.app.modules.user.entity;

import com.siyoumi.component.XEnumBase;


public class EnumUserEnable
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "待审核");
        put(1, "生效");
        put(10, "失效");
    }
}
