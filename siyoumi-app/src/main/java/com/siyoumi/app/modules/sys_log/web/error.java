package com.siyoumi.app.modules.sys_log.web;

import com.siyoumi.component.XApp;
import com.siyoumi.controller.ApiController;
import com.siyoumi.entity.SysLog;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.exception.XException;
import com.siyoumi.service.SysLogService;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app/sys_log/error")
public class error
        extends ApiController {
    @SneakyThrows
    @GetMapping("/{log_id}")
    public String index(@PathVariable String log_id) {
        if (XStr.isNullOrEmpty(log_id)) {
            throw new XException(EnumSys.MISS_VAL.getR("miss log_id"));
        }
        SysLogService appLog = SysLogService.getBean();
        SysLog entityLog = appLog.getEntity(log_id);
        if (entityLog == null) {
            throw new XException(EnumSys.MISS_VAL.getR("log_id异常"));
        }

        String html = XApp.getFileContent("view/sys_log/error.html");
        html = html.replace("{#errmsg}", entityLog.getLog_str_00())
                .replace("{#err_info}", entityLog.getLog_txt_00())
                .replace("{#request_param}", entityLog.getLog_str_01());

        return html;
    }
}
