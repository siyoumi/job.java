package com.siyoumi.app.modules.app_ess.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.app_ess.service.*;
import com.siyoumi.app.modules.app_ess.vo.*;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.app.service.EssStudyRecordService;
import com.siyoumi.app.service.EssTestQuestionService;
import com.siyoumi.app.service.EssTestResultService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//老师接口
@Slf4j
@RestController
@RequestMapping("/wxapp/app_ess/api_teacher")
public class ApiAppEssTeacher
        extends WxAppApiController {
    /**
     * 老师-学习任务列表
     */
    @GetMapping("study_list")
    public XReturn studyList() {
        InputData inputData = InputData.fromRequest();
        String moduleId = inputData.input("module_id");
        XValidator.isNullOrEmpty(moduleId, "miss module_id");

        String[] select = {
                "estudy_id",
                "estudy_name",
                "estudy_date_begin",
                "estudy_date_end",
        };
        JoinWrapperPlus<EssStudy> query = SvcEssStudy.getBean().listQuery(inputData);
        query.eq("estudy_uid", getUid()); //指定老师
        query.select(select);
        IPage<EssStudy> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), true);
        //list
        IPage<Map<String, Object>> pageData = SvcEssStudy.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        //收集所有ID
        List<String> estudyIds = list.stream().map(item -> (String) item.get("estudy_id")).collect(Collectors.toList());
        List<EssStudyTask> listTask = SvcEssStudy.getBean().getTaskList(estudyIds); //子任务列表
        //
        for (Map<String, Object> item : list) {
            EssStudy entity = XBean.fromMap(item, EssStudy.class);
            item.put("id", entity.getKey());
            item.put("state", SvcEssStudy.getBean().getState(entity));

            List<EssStudyTask> tasks = listTask.stream().filter(i -> i.getEstask_study_id().equals(entity.getKey()))
                    .collect(Collectors.toList());
            item.put("tasks", tasks);
        }

        getR().setData("list", list);
        getR().setData("total", pageData.getTotal());

        return getR();
    }

    /**
     * 老师-学习任务-子任务列表
     */
    @GetMapping("study_task_list")
    public XReturn studyTaskList() {
        InputData inputData = InputData.fromRequest();
        String studayId = inputData.input("studay_id");
        XValidator.isNullOrEmpty(studayId, "缺少学习任务ID");
        String taskId = inputData.input("task_id");
        XValidator.isNullOrEmpty(taskId, "缺少子任务ID");
        String name = inputData.input("name");

        String[] select = {
                "user_id",
                "user_name",
                "esre_fun",
                "esre_state",
                "esre_state_date",
        };
        JoinWrapperPlus<EssStudyRecord> query = SvcEssStudy.getBean().listRecordQuery(inputData); //子任务列表
        query.join(SysUser.table(), SysUser.tableKey(), "esre_uid");
        query.select(select);
        if (XStr.hasAnyText(name)) { //学生名字
            query.like("user_name", name);
        }
        IPage<EssStudyRecord> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = EssStudyRecordService.getBean().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        getR().setData("list", list);

        return getR();
    }

    /**
     * 老师-添加学习任务
     */
    @PostMapping("study_add")
    public XReturn studyAdd(@Validated @RequestBody VoEssStudyAdd vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        vo.setEstudy_uid(getUid());
        return SvcEssStudy.getBean().add(vo);
    }


    /**
     * 评测列表
     */
    @GetMapping("test_list")
    public XReturn testList() {
        InputData inputData = InputData.fromRequest();

        String[] select = {
                "etest_id",
                "etest_create_date",
                "etest_title",
                "etest_date_begin",
                "etest_date_end",
                "etest_test_minute",
                "etest_test_not_submit",
                "etest_test_end_remind",
                "etest_random",
                "etest_score_show",
                "etest_submit_total",
                "etest_test_total",
                "etest_fun_total",
                "etest_question_total",
                "etest_state",
                "etest_state_date",
        };
        JoinWrapperPlus<EssTest> query = SvcEssTest.getBean().listQuery(inputData);
        query.eq("etest_uid", getUid()); //指定老师
        query.select(select);
        IPage<EssTest> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), true);
        //list
        IPage<Map<String, Object>> pageData = SvcEssTest.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        for (Map<String, Object> item : list) {
            EssTest entity = XBean.fromMap(item, EssTest.class);
            item.put("id", entity.getKey());
            //状态
            XReturn rState = SvcEssTest.getBean().getState(entity);
            item.put("state", rState);
        }

        getR().setData("list", list);
        getR().setData("total", pageData.getTotal());

        return getR();
    }

    /**
     * 测评学生成绩列表
     */
    @GetMapping("test_result_list")
    public XReturn testResultList() {
        InputData inputData = InputData.fromRequest();
        String testId = inputData.input("test_id");
        XValidator.isNullOrEmpty(testId, "miss test_id");
        EssTest entityTest = SvcEssTest.getApp().getEntity(testId);
        XValidator.isNull(entityTest, "评测ID异常");
        String orderby = inputData.input("orderby", "0"); //排序
        String classId = inputData.input("class_id"); //班级ID

        String[] select = {
                "etres_id",
                "etres_submit",
                "etres_submit_date",
                "etres_submit_minute",
                "etres_test_fun",
                "user_id",
                "user_name",
                "eclass_id",
                "eclass_name",
        };
        JoinWrapperPlus<EssTestResult> query = SvcEssTest.getBean().listResultQuery(inputData);
        query.join(SysUser.table(), SysUser.tableKey(), "etres_uid");
        query.join(EssClassUser.table(), SysUser.tableKey(), "ecu_uid");
        query.join(EssClass.table(), EssClass.tableKey(), "ecu_class_id");
        query.select(select);
        query.eq("ecu_user_type", 0);
        switch (orderby) {
            case "0": //创建时间
                query.orderByDesc("etres_id");
                break;
            case "1": //按成绩排序
                query.orderByDesc("etres_test_fun");
                break;
        }
        if (XStr.hasAnyText(classId)) { //班级
            query.eq("etres_class_id", classId);
        }
        //list
        List<Map<String, Object>> list = EssTestResultService.getBean().getMaps(query);
        //班级
        Map<String, Object> mapClass = new HashMap<>();
        for (Map<String, Object> item : list) {
            String id = (String) item.get("eclass_id");
            String name = (String) item.get("eclass_name");
            mapClass.put(id, name);
        }

        getR().setData("list", list);

        //评测设置
        Map<String, Object> mapTest = XBean.toMap(entityTest, new String[]{
                "etest_id",
                "etest_title",
                "etres_submit_minute",
                "etest_test_not_submit",
                "etest_fun_total",
                "etest_question_total",
                "etest_submit_total",
                "etest_test_total",
                "etest_date_begin",
                "etest_date_end",
        });
        getR().setData("test", mapTest);
        //创建人
        SysUser entityTeacher = SvcSysUser.getBean().getEntity(entityTest.getEtest_uid());
        Map<String, Object> mapTeacher = XBean.toMap(entityTeacher, new String[]{
                "user_id",
                "user_name",
        });
        getR().setData("teacher", mapTeacher);
        getR().setData("class", mapClass);

        return getR();
    }

    /**
     * 老师-添加评测
     */
    @PostMapping("test_add")
    public XReturn testAdd(@Validated @RequestBody VoEssTestAdd vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        InputData inputData = InputData.fromRequest();
        vo.setEtest_uid(getUid());
        return SvcEssTest.getBean().add(inputData, vo);
    }


    /**
     * 老师-添加学生
     */
    @PostMapping("test_add_student")
    public XReturn testAddStudent(@Validated @RequestBody VoEssTestAddStudent vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);
        return SvcEssTest.getBean().addStudent(vo);
    }

    /**
     * 老师-添加题目
     */
    @PostMapping("test_add_question")
    public XReturn testAddQuestion(@Validated @RequestBody VoEssTestBatchAddQuestion vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);
        return SvcEssTest.getBean().batchAddQuestion(vo);
    }

    /**
     * 老师-删除题目
     *
     * @param vo
     * @param result
     */
    @PostMapping("test_del_question")
    public XReturn testDelQuestion(@Validated VoEssTestQuestionDel vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);
        return SvcEssTest.getBean().delQuestion(vo);
    }


    /**
     * 评测题目列表
     */
    @GetMapping("test_question_list")
    public XReturn testQuestionList() {
        InputData inputData = InputData.fromRequest();
        String testId = inputData.input("test_id");
        XValidator.isNullOrEmpty(testId, "miss test_id");

        String[] select = {
                "etques_id",
                "etques_id",
                "etques_fun",
                "etques_question_id",
                "etques_order",
                "etqu_type",
                "etqu_question_type",
                "etqu_question",
                "etqu_question_item",
                "etqu_level",
                "etqu_answer",
                "etqu_level",
                "etqu_answer_total",
                "etqu_answer_bingo_total",
        };
        JoinWrapperPlus<EssTestQuestion> query = SvcEssTest.getBean().listQuestionQuery(inputData);
        query.join(EssQuestion.table(), EssQuestion.tableKey(), "etques_question_id");
        query.select(select);
        query.orderByAsc("etques_order")
                .orderByDesc("etques_id");

        IPage<EssTestQuestion> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = EssTestQuestionService.getBean().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        //
        for (Map<String, Object> item : list) {
        }

        getR().setData("list", list);
        //getR().setData("count", pageData.getTotal());
        return getR();
    }

    /**
     * 添加单题目，或者修改积分
     *
     * @param vo
     * @param result
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/test_question_save")
    public XReturn save(@Validated VoEssTestQuestion vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);
        //
        InputData inputData = InputData.fromRequest();
        return SvcEssTest.getBean().editQuestion(inputData, vo);
    }

    /**
     * 删除评测
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/test_del")
    public XReturn testDel() {
        return SvcEssTest.getBean().delete(getIds());
    }

    /**
     * 发布评测
     */
    @PostMapping("/test_audit")
    public XReturn testAudit(@Validated VoEssTestAudit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        return SvcEssTest.getBean().audit(vo);
    }

    /**
     * 复制评测
     */
    @PostMapping("/test_copy")
    public XReturn testCopy(@Validated VoEssTestCopy vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        return SvcEssTest.getBean().copy(vo);
    }

    /**
     * 加时
     */
    @PostMapping("/test_add_time")
    public XReturn addTime(@Validated VoEssTestAddTime vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        return SvcEssTest.getBean().addTime(vo);
    }

    /**
     * 公布成绩
     */
    @PostMapping("/test_show_result")
    public XReturn showResult(@Validated VoEssTestShowResult vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true);

        return SvcEssTest.getBean().showResult(vo);
    }
}
