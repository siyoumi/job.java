package com.siyoumi.app.modules.app_corp.service;

import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.siyoumi.app.entity.CorpStore;
import com.siyoumi.app.modules.app_corp.vo.CropStoreAuditVo;
import com.siyoumi.app.modules.app_corp.vo.VaCropStore;
import com.siyoumi.app.service.CorpStoreService;
import com.siyoumi.app.service.SysUvService;
import com.siyoumi.app.sys.service.SvcSysUv;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XRedis;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.config.SysConfig;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//商家
@Slf4j
@Service
public class SvcCropStore {
    static public SvcCropStore getBean() {
        return XSpringContext.getBean(SvcCropStore.class);
    }

    static public CorpStoreService getApp() {
        return CorpStoreService.getBean();
    }

    public XReturn validStore(CorpStore entityStore) {
        if (entityStore.getCstore_enable() != 1) {
            return XReturn.getR(20088, "商家审核未通过");
        }

        if (entityStore.getCstore_expire_date().isBefore(XDate.now())) {
            //return XReturn.getR(20098, "商家已过期");
        }

        return XReturn.getR(0);
    }

    //更新uv
    public XReturn updateUv(String id, String openid) {
        CorpStore entity = getApp().getEntity(id, XHttpContext.getX());
        if (entity == null) {
            return EnumSys.ERR_VAL.getR("id异常");
        }

        Long uv = entity.getCstore_uv();

        XReturn doUpdate = SvcSysUv.getBean().save("app_corp", "", id, openid);
        if (doUpdate.ok()) {
            uv++;
            CorpStore entityUpdate = new CorpStore();
            entityUpdate.setCstore_id(entity.getCstore_id());
            entityUpdate.setCstore_uv(uv);
            getApp().updateById(entityUpdate);
        }

        XReturn r = XReturn.getR(0);
        r.setData("do_update", doUpdate);
        r.setData("uv", uv);

        return r;
    }

    public CorpStore getEntityByOpenid(String openid) {
        JoinWrapperPlus<CorpStore> query = listQuery(InputData.getIns());
        query.eq("cstore_openid", openid);
        return getApp().first(query);
    }

    //list
    public JoinWrapperPlus<CorpStore> listQuery(InputData inputData) {
        String name = inputData.input("name");
        String enable = inputData.input("enable");

        JoinWrapperPlus<CorpStore> query = SvcCropStore.getApp().join();
        query.eq("cstore_x_id", XHttpContext.getX());
        query.orderByAsc("cstore_order")
                .orderByDesc("cstore_create_date");

        if (XStr.hasAnyText(name)) { //商家名称
            query.like("cstore_name", name);
        }
        if (XStr.hasAnyText(enable)) { //审核
            query.like("cstore_enable", enable);
        }

        return query;
    }

    //编辑
    public XReturn edit(InputData inputData, VaCropStore vo) {
        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminAdd()) {
        } else {
        }

        return SvcCropStore.getApp().saveEntity(inputData, vo, true, ignoreField);
    }

    //商家信息
    public XReturn info(String id) {
        String key = getApp().getEntityCacheKey(id);
        CorpStore entityStore = XRedis.getBean().getAndSetData(key, k -> getApp().getEntity(id), CorpStore.class);
        if (entityStore == null) {
            return EnumSys.ERR_VAL.getR("id异常");
        }
        //XReturn r = validStore(entityStore);
        //if (r.err()) {
        //    return r;
        //}

        Map<String, Object> mapStore = XBean.toMap(entityStore, new String[]{
                "cstore_id",
                "cstore_name",
                "cstore_wifi_ssid",
                "cstore_wifi_pwd",
                "cstore_wifi_bssid",
                "cstore_pic_h5",
                "cstore_expire_date",
                "cstore_enable",
        });

        XReturn r = XReturn.getR(0);
        r.setData("store", mapStore);
        return r;
    }

    //删除
    public XReturn delete(List<String> ids) {
        JoinWrapperPlus<CorpStore> query = listQuery(InputData.getIns());
        query.in("cstore_id", ids);

        List<CorpStore> list = SvcCropStore.getApp().list(query);
        boolean result = SvcCropStore.getApp().removeBatchByIds(list);

        for (String id : ids) {
            SvcCropStore.getApp().delEntityCache(id);
        }

        XReturn r = XReturn.getR(0);
        r.setData("result", result);

        return r;
    }

    //审核
    public XReturn audit(CropStoreAuditVo vo) {
        JoinWrapperPlus<CorpStore> query = listQuery(InputData.getIns());
        query.in("cstore_id", vo.getIds());

        UpdateChainWrapper<CorpStore> update = SvcCropStore.getApp().update();
        update.eq("cstore_x_id", XHttpContext.getX())
                .in("cstore_id", vo.getIds());
        update.set("cstore_enable_date", LocalDateTime.now());
        if (vo.getEnable() == 1) {
            log.debug("审核通过");
            update.set("cstore_enable", 1)
                    .ne("cstore_enable", 1);
        } else {
            log.debug("审核不通过");
            update.set("cstore_enable", 10)
                    .ne("cstore_enable", 10);
        }
        boolean result = update.update();

        log.debug("清删除");
        for (String id : vo.getIds()) {
            SvcCropStore.getApp().delEntityCache(id);
        }

        XReturn r = XReturn.getR(0);
        r.setData("result", result);

        return r;
    }

    //入口链接
    public String getUrl(String id) {
        return SysConfig.getIns().getImgRoot() + "res/app_corp/store.html?id=" + id;
    }

    //小程序路径
    public String getWxAppPath(String id) {
        return "pages/app_crop/shop/shop?id=" + id;
    }
}
