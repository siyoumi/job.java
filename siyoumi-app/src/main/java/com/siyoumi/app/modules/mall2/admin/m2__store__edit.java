package com.siyoumi.app.modules.mall2.admin;

import com.siyoumi.app.entity.M2Group;
import com.siyoumi.app.entity.M2Store;
import com.siyoumi.app.modules.mall2.entity.VaM2Store;
import com.siyoumi.app.modules.mall2.service.SvcM2Group;
import com.siyoumi.app.modules.mall2.service.SvcM2Store;
import com.siyoumi.app.service.M2GroupService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/mall2/m2__store__edit")
public class m2__store__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商家-编辑");

        Map<String, Object> data = new HashMap<>();
        if (isAdminEdit()) {
            M2Store entity = SvcM2Store.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() VaM2Store vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("mstore_x_id");
        } else {
            vo.setMstore_acc_id(getAccId());
        }
        InputData inputData = InputData.fromRequest();

        return SvcM2Store.getBean().edit(inputData, vo);
    }
}
