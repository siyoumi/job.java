package com.siyoumi.app.modules.wechat.service;

import com.siyoumi.app.modules.wechat.entity.WechatGroupChatUser;
import com.siyoumi.app.modules.wechat.entity.WechatRoomUser;
import com.siyoumi.app.modules.wechat.entity.WechatNotifyData;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XRedis;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMap;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

//摸鱼榜
@Slf4j
@Service
public class WeChatEventMoyu
        implements WechatEvent {
    static public WeChatEventMoyu getBean() {
        return XSpringContext.getBean(WeChatEventMoyu.class);
    }

    @Override
    public XReturn handle(WechatNotifyData data) {
        //摸鱼
        moyuAdd(data);
        moyuSend(data);

        return null;
    }

    /**
     * 摸鱼收集
     *
     * @param data
     */
    public void moyuAdd(WechatNotifyData data) {
        if (data.getIs_self()) {
            return;
        }
        
        String redisKey = WeChatUtil.getKeyMoyu(data.getRoomid());
        RMap<String, String> list = XRedis.getBean().getList(redisKey);
        String sum = list.get(data.getSender());
        Integer sumInt = XStr.toInt(sum, 0);
        sumInt++;

        list.put(data.getSender(), sumInt.toString());
    }

    public void moyuSend(WechatNotifyData data) {
        if (!data.getIs_at()) {
            return;
        }
        if (!data.getContent().contains("摸鱼")) {
            return;
        }

        send(data.getRoomid());
    }


    /**
     * 摸鱼发送
     *
     * @param roomId
     */
    public void send(String roomId) {
        WechatOpenApi api = WechatOpenApi.getIns();
        List<WechatGroupChatUser> listUser = api.groupchatUser(roomId);

        String redisKey = WeChatUtil.getKeyMoyu(roomId);
        RMap<String, String> list = XRedis.getBean().getList(redisKey);
        if (list.size() <= 0) {
            WechatOpenApi.getIns().sendTxt(roomId, "群没有人摸鱼", "");
            return;
        }

        List<WechatRoomUser> listMoyuUser = new ArrayList<>();
        for (Map.Entry<String, String> entry : list.entrySet()) {
            //获取群用户名称
            WechatGroupChatUser chatUser = listUser.stream().filter(v ->
                    v.getId().equals(entry.getKey())).findFirst().orElse(null);

            WechatRoomUser moyuUser = new WechatRoomUser();
            XBean.copyProperties(chatUser, moyuUser);
            moyuUser.setCount(XStr.toInt(entry.getValue()));

            listMoyuUser.add(moyuUser);
        }

        Collections.sort(listMoyuUser, (u1, u2) -> Integer.compare(u2.getCount(), u1.getCount()));

        StringBuilder sb = new StringBuilder();
        sb.append("【摸鱼榜】");
        for (WechatRoomUser moyuUser : listMoyuUser) {
            sb.append("\n" + moyuUser.getName() + ": 摸鱼" + moyuUser.getCount() + "次");
        }

        WechatOpenApi.getIns(null).sendTxt(roomId, sb.toString(), "");
    }
}
