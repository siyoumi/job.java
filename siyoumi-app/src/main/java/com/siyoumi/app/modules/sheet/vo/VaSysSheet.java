package com.siyoumi.app.modules.sheet.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class VaSysSheet {
    @NotBlank
    @Size(max = 50)
    private String sheet_name;
    @NotNull
    private LocalDateTime sheet_begin_date;
    @NotNull
    private LocalDateTime sheet_end_date;
    @Size(max = 500)
    private String sheet_banner_imgs;
    private Integer sheet_submit_type;
    private String sheet_item_data;
    private String sheet_uix;
}
