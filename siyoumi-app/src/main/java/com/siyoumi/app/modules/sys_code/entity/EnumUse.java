package com.siyoumi.app.modules.sys_code.entity;

import com.siyoumi.util.IEnum;

//正则表达工
public enum EnumUse
        implements IEnum {
    USE("1", "已使用"),
    UN_USE("0", "未使用");


    private String key;
    private String val;

    EnumUse(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
