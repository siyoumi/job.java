package com.siyoumi.app.modules.app_fks.admin;

import com.siyoumi.app.entity.FksDepartment;
import com.siyoumi.app.entity.LnumGroup;
import com.siyoumi.app.modules.app_fks.service.SvcFksDepartment;
import com.siyoumi.app.modules.app_fks.vo.VaFksDepartment;
import com.siyoumi.app.modules.lucky_num.entity.LuckyNumType;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumGroup;
import com.siyoumi.app.modules.lucky_num.service.SvcLnumNumSet;
import com.siyoumi.app.modules.lucky_num.vo.VaLnumGroup;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__department__edit")
public class app_fks__department__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("部门-编辑");

        InputData inputData = InputData.fromRequest();

        Map<String, Object> data = new HashMap<>();
        data.put("fdep_order", 0);
        if (isAdminEdit()) {
            FksDepartment entity = SvcFksDepartment.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaFksDepartment vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        if (!isAdminEdit()) {
            vo.setFdep_acc_id(getAccId());
        } else {
            vo.setFdep_acc_id(null);
        }

        InputData inputData = InputData.fromRequest();
        return SvcFksDepartment.getBean().edit(inputData, vo);
    }
}