package com.siyoumi.app.modules.book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysBook;
import com.siyoumi.app.modules.book.service.SvcSysBook;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/xadmin/book/book__item__list")
public class book__item__list
        extends AdminApiController {

    @GetMapping()
    public XReturn index() {
        setPageTitle("预约列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "t_book.*",
        };
        JoinWrapperPlus<SysBook> query = SvcSysBook.getBean().listQuery(inputData);
        query.select(select);

        IPage<SysBook> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcSysBook.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            SysBook entity = SvcSysBook.getApp().loadEntity(data);
            data.put("id", entity.getKey());

            //过期
            data.put("expire", entity.expire());
        }

        getR().setData("list", list);
        getR().setData("count", count);


        return getR();
    }

    //删除
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcSysBook.getBean().delete(List.of(ids));
    }
}
