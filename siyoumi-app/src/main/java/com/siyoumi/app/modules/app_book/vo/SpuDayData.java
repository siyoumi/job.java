package com.siyoumi.app.modules.app_book.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

//每日库存
@Data
public class SpuDayData {
    private LocalDateTime date; //日期
    private Long stock_total; //总库存
    private Long left_total; //剩余库存
    private BigDecimal add_price; //加价
    private Integer stock_add; //加库存
}
