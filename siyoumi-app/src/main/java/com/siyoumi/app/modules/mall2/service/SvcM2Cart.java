package com.siyoumi.app.modules.mall2.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.mall2.vo.SkuData;
import com.siyoumi.app.service.M2CartService;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SvcM2Cart
        implements IWebService {
    static public SvcM2Cart getBean() {
        return XSpringContext.getBean(SvcM2Cart.class);
    }

    static public M2CartService getApp() {
        return M2CartService.getBean();
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<M2Cart> listQuery(String wxFrom, String skuId) {
        JoinWrapperPlus<M2Cart> query = SvcM2Cart.getApp().join();
        query.eq("mcart_x_id", XHttpContext.getX())
                .eq("mcart_openid", wxFrom);
        if (XStr.hasAnyText(skuId)) { //sku
            query.eq("mcart_sku_id", skuId);
        }

        return query;
    }

    public M2Cart getEntity(String wxFrom, String skuId) {
        JoinWrapperPlus<M2Cart> query = listQuery(wxFrom, skuId);
        return getApp().first(query);
    }


    /**
     * 购物车sku数量 + 已购买sku数量
     *
     * @param wxFrom
     * @param skuId
     */
    public long getSkuCount(String wxFrom, String skuId) {
        JoinWrapperPlus<M2Cart> query = listQuery(wxFrom, skuId);
        BigDecimal count = SvcM2Cart.getApp().sum(query, "mcart_count");

        long skuCount = SvcM2Order.getBean().getSkuCount(wxFrom, skuId);


        return count.toBigInteger().longValue() + skuCount;
    }


    /**
     * 用户购物车
     *
     * @param wxFrom
     */
    public List<Map<String, Object>> list(String wxFrom) {
        JoinWrapperPlus<M2Cart> query = listQuery(wxFrom, null);
        query.join(M2Sku.table(), "mcart_sku_id", "msku_id");
        query.join(SysStock.table(), "msku_id", "lecnt_id_src");
        query.join(M2Spu.table(), "mcart_spu_id", "mspu_id");
        query.select("mspu_name", "mspu_pic", "stock_count_left", "stock_count_use"
                , "t_m2_cart.*"
                , "t_m2_sku.*"
        );

        return SvcM2Cart.getApp().getMaps(query);
    }


    /**
     * 添加购买车
     *
     * @param skuId 商品规格ID
     * @param add   数量，正数：添加，负数：减少；
     */
    @Transactional
    public XReturn add(String wxFrom, String skuId, Integer add) {
        if (add == 0) {
            return XReturn.getR(20100, "count == 0");
        }

        M2Sku entitySku = SvcM2Sku.getApp().getEntity(skuId);
        if (entitySku == null) {
            return EnumSys.ERR_VAL.getR("商品规格ID异常");
        }

        SvcM2Sku appSku = SvcM2Sku.getBean();
        //获取sku相关信息
        List<String> skuIds = new ArrayList<>();
        skuIds.add(skuId);
        List<SkuData> skuList = appSku.getList(null, skuIds);
        SkuData skuData = skuList.get(0);
        //
        XReturn r = appSku.valid(skuData);
        if (r.err()) {
            return r;
        }

        M2Cart entity = getEntity(wxFrom, skuId);
        if (add > 0) {
            XLog.debug(this.getClass(), "添加数量");

            SysStock entityLeft = appSku.getEntityLeft(entitySku.getKey());
            if (entityLeft.getStock_count_left() <= 0) {
                return XReturn.getR(20086, entitySku.getMsku_name() + ":库存不足");
            }

            //限购
            Long limitMax = entitySku.limitMax();
            long skuCount = getSkuCount(wxFrom, skuId) + add;
            if (skuCount > limitMax) {
                return XReturn.getR(20087, entitySku.getMsku_name() + ":商品最多购买" + limitMax);
            }
        } else {
            XLog.debug(this.getClass(), "减数量");
            if (entity == null) {
                return XReturn.getR(20132, "购买车操作数量异常");
            }
            int leftCount = entity.getMcart_count() + add;
            if (leftCount < 1) {
                return XReturn.getR(20135, "最少购买1件");
            }
        }

        if (entity == null) {
            entity = new M2Cart();
            entity.setMcart_x_id(XHttpContext.getX());
            entity.setMcart_openid(wxFrom);
            entity.setMcart_sku_id(entitySku.getMsku_id());
            entity.setMcart_spu_id(entitySku.getMsku_spu_id());
            entity.setMcart_count(add);
            entity.setAutoID();

            SvcM2Cart.getApp().save(entity);
        } else {
            XLog.debug(this.getClass(), "更新数量");

            UpdateChainWrapper<M2Cart> update = getApp().update();
            if (add > 0) {
                update.setSql("mcart_count = mcart_count + " + add);
            } else {
                update.setSql("mcart_count = mcart_count - " + Math.abs(add));
            }
            update.eq("mcart_id", entity.getKey())
                    .update();
        }


        return XReturn.getR(0);
    }


    /**
     * 移除购买车
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        QueryWrapper<M2Cart> query = listQuery(getOpenid(), null);
        query.in("mcart_id", ids);
        boolean remove = SvcM2Cart.getApp().remove(query);

        return XReturn.getR(0);
    }
}
