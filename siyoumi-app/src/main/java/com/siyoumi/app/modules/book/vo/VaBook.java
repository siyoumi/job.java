package com.siyoumi.app.modules.book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class VaBook {
    String book_acc_id;
    String book_type = "";
    @HasAnyText
    @Size(max = 50)
    String book_name;
    @Size(max = 50)
    String book_title;
    LocalDateTime book_date_begin;
    LocalDateTime book_date_end;
    Integer book_sign_fun_enable;
    Integer book_sign_fun;
    String book_pic;
    String book_pic_00;
    String book_field_json;
    Integer book_sign_day_total;
    Integer book_sign_cancel;
    Integer book_audit_enable;
    String book_info_json;
    Integer book_order;
    Integer book_sign_total;
    Integer book_sign_give_enable;
    Integer book_sign_give_fun;
}
