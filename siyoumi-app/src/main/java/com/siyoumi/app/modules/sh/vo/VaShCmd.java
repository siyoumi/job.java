package com.siyoumi.app.modules.sh.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class VaShCmd {
    private LocalDateTime scmd_run_date;
    @HasAnyText
    @Size(max = 500)
    private String scmd_script;
    @Size(max = 50)
    private String scmd_desc;
}
