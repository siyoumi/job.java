package com.siyoumi.app.modules.sh.task.data;

import lombok.Data;

@Data
public class QmaiData {
    String token;
    String phone;
    String username;

    public static QmaiData getIns(String token, String phone, String username) {
        QmaiData data = new QmaiData();
        data.setToken(token);
        data.setPhone(phone);
        data.setUsername(username);
        return data;
    }
}
