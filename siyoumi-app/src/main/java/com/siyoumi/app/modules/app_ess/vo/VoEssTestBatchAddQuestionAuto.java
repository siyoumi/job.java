package com.siyoumi.app.modules.app_ess.vo;

import lombok.Data;


//评测自动选题目（选场景）
@Data
public class VoEssTestBatchAddQuestionAuto {
    Integer type; //【0】评价；【1】理论实训；
    String module_id; //场景ID
    Integer question_count; //选择题目数量
    Integer fun; //每题分数
}
