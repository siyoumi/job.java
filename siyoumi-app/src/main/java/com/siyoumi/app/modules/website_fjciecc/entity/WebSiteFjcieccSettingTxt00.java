package com.siyoumi.app.modules.website_fjciecc.entity;

import com.alibaba.fastjson.JSON;
import com.siyoumi.util.XStr;
import lombok.Data;

//设置
@Data
public class WebSiteFjcieccSettingTxt00 {
    String company = ""; //公司简介
    String characteristic = ""; //中咨本色
    String forward = ""; //期待你们
    String about = ""; //关于得益

    public static WebSiteFjcieccSettingTxt00 getInstance(String s) {
        if (XStr.isNullOrEmpty(s)) {
            return new WebSiteFjcieccSettingTxt00();
        }

        return JSON.parseObject(s, WebSiteFjcieccSettingTxt00.class);
    }
}
