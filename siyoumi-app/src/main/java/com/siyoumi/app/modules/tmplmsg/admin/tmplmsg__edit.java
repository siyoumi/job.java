package com.siyoumi.app.modules.tmplmsg.admin;

import com.siyoumi.app.entity.WxTmplmsg;
import com.siyoumi.app.service.WxTmplmsgService;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import com.siyoumi.component.http.InputData;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/tmplmsg/tmplmsg__edit")
public class tmplmsg__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("模板消息-编辑");

        WxTmplmsgService app = WxTmplmsgService.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("wxtmplmsg_redirect_type", "");
        if (isAdminEdit()) {
            WxTmplmsg entity = app.loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            Map<String, Object> map = app.contentToMap(entity.getWxtmplmsg_content());
            map.forEach((k, v) -> {
                Map vMap = (Map) v;
                dataAppend.put("data_" + k, vMap.get("value"));
                if (vMap.containsKey("color")) {
                    dataAppend.put("color_" + k, vMap.get("color"));
                }
            });
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        data.put("app_id", "tmplmsg");
        getR().setData("data", data);

        XReturn r = app.getMpTemplmsg();
        if (r.err()) {
            return r;
        }
        setPageInfo("list_templmsg", r.get("list"));

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() WxTmplmsg entity, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //自定交验证
        if (entity.getWxtmplmsg_redirect_type().equals("wxapp")) {
            if (XStr.hasAnyText(entity.getWxtmplmsg_redirect_str_01())) {
                result.addError(XValidator.getErr("wxtmplmsg_redirect_str_01", "不能为空"));
            }
        }
        XValidator.getResult(result);

        WxTmplmsgService app = WxTmplmsgService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("wxtmplmsg_id_src");
            ignoreField.add("wxtmplmsg_uix");
            ignoreField.add("wxtmplmsg_app_id");
        }

        InputData inputData = InputData.fromRequest();
        inputData.putAll(entity.toMap());
        return app.saveEntity(inputData, true, ignoreField);
    }

    @Transactional
    @GetMapping("/send_test")
    public XReturn sendTest() {
        String openid = input("openid");
        if (XStr.isNullOrEmpty(openid) || XStr.isNullOrEmpty(getID())) {
            return EnumSys.MISS_VAL.getR();
        }

        WxTmplmsgService app = WxTmplmsgService.getBean();
        return app.send(getID(), openid, null, null);
    }
}
