package com.siyoumi.app.modules.prize.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class VaSysPrizeSet {
    @HasAnyText
    @Size(max = 50)
    private String pset_name;
    private String pset_type;
    private String pset_app_id;
    private String pset_key;
    //    private String pset_id_src;
    @HasAnyText
    private String pset_pic;
    @HasAnyText
    @Size(max = 5000)
    private String pset_desc;
    private String pset_content;
    @HasAnyText(message = "不能为空")
    private Integer pset_use_type;
    private String pset_use_str_00;

    @HasAnyText(message = "不能为空")
    private Integer pset_vaild_date_type;
    private Integer pset_vaild_date_win_h;
    private LocalDateTime pset_vaild_date_begin;
    private LocalDateTime pset_vaild_date_end;
    private Integer pset_int_00;
    private Integer pset_int_01;
    private String pset_str_00;
    private String pset_use_info_option;
    //private Long pset_del;
    private Integer pset_order;
    private Integer pset_send_type;
    private String pset_send_id;
    private Long pset_fun;

    private BigDecimal pschou_win_rate;
    private Integer pschou_win_multi;
    private String pschou_index;

    //库存
    private Long stock_count_left;
    private Long stock_count_left_old;
}
