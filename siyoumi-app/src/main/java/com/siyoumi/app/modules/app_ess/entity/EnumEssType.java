package com.siyoumi.app.modules.app_ess.entity;

import com.siyoumi.component.XEnumBase;


public class EnumEssType
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "评价");
        put(1, "理论实训");
    }
}
