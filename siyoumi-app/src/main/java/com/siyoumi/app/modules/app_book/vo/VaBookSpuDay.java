package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

//批量添加库存&加价
@Data
public class VaBookSpuDay {
    @HasAnyText
    private String bspud_spu_id;
    @HasAnyText
    private Integer bspud_stock_add;
    @HasAnyText
    private BigDecimal bspud_spu_add_price;
    @HasAnyText
    private LocalDateTime begin_date;
    @HasAnyText
    private LocalDateTime end_date;

    private List<Integer> week;
}
