package com.siyoumi.app.modules.app_menu.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class VaMenu {
    @NotBlank()
    private String abc_pid;
    @NotBlank()
    @Size(max = 200)
    private String abc_name;

    @Size(max = 2000)
    private String abc_txt_00;
    @Size(max = 2000)
    private String abc_txt_01;
    @NotBlank()
    @Size(max = 200)
    private String abc_str_02;
    @Size(max = 200)
    private String abc_str_03;

    private String abc_app_id;
    private String abc_table;
}
