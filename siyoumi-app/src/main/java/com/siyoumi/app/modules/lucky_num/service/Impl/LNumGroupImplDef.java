package com.siyoumi.app.modules.lucky_num.service.Impl;

import com.siyoumi.app.entity.LnumNum;
import com.siyoumi.app.entity.LnumNumSet;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumGroup;
import com.siyoumi.app.modules.lucky_num.service.SvcLnumNumSet;
import com.siyoumi.app.modules.lucky_num.vo.LNumGetVo;
import com.siyoumi.app.service.LnumNumService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

//幸运摇号
@Slf4j
public class LNumGroupImplDef
        extends SvcLNumGroup {
    @Override
    public Long numGen(LNumGetVo vo) {
        LocalDateTime lastDate = null;

        //String id = XStr.shortId();
        LnumNumSet entitySet = SvcLnumNumSet.getApp().first(vo.getSet_id());

        JoinWrapperPlus<LnumNum> query = numQuery(entitySet.getLnset_group_id(), InputData.getIns());
        query.orderByDesc("lnum_create_date");
        LnumNum entityNumLast = LnumNumService.getBean().first(query);

        Long num = 1L;
        if (entityNumLast != null) {
            num = entityNumLast.getLnum_num() + 1;
        }

        return num;
    }
}
