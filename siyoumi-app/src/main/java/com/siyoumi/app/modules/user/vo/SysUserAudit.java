package com.siyoumi.app.modules.user.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//用户审核
@Data
public class SysUserAudit {
    @HasAnyText
    private List<String> ids;
    @HasAnyText
    @EqualsValues(vals = {"0", "1", "10"})
    private Integer enable;
}
