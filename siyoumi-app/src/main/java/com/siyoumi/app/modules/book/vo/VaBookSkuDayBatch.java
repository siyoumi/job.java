package com.siyoumi.app.modules.book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//批量操作
@Data
public class VaBookSkuDayBatch {
    String acc_id;

    @HasAnyText
    String sku_id;
    @HasAnyText
    String date_begin;
    @HasAnyText
    String date_end;

    Integer set_left = 0; //设置库存
    Long left_count; //库存
    Long left_count_old; //库存

    Integer set_before = 0; //设置截止时间
    Integer before_enable;
    Long before_m;

    Integer set_append_max = 0; //设置同行人
    Integer append_max;
}
