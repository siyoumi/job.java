package com.siyoumi.app.modules.wifi.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class VaWifi {
    @NotBlank(message = "请输入wifi名称")
    @Size(max = 50)
    String abc_name;
    @NotBlank(message = "请输入wifi密码")
    @Size(max = 200)
    String abc_str_00;
    @Size(max = 200)
    String abc_str_01;

    String abc_table;
    String abc_app_id;
}
