package com.siyoumi.app.modules.app_ess.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.EssTest;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_ess.service.SvcEssTest;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__test0__list")
public class app_ess__test0__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("评价列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "etest_id",
                "etest_create_date",
                "etest_title",
                "etest_date_begin",
                "etest_date_end",
                "etest_test_minute",
                "etest_submit_total",
                "etest_test_total",
                "etest_fun_total",
                "etest_question_total",
                "etest_state",
                "etest_state_date",
                "user_id",
                "user_name",
        };
        JoinWrapperPlus<EssTest> query = SvcEssTest.getBean().listQuery(inputData);
        query.join(SysUser.table(), SysUser.tableKey(), "etest_uid");

        query.select(select);
        IPage<EssTest> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcEssTest.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        for (Map<String, Object> item : list) {
            EssTest entity = XBean.fromMap(item, EssTest.class);
            item.put("id", entity.getKey());
            //状态
            XReturn rState = SvcEssTest.getBean().getState(entity);
            item.put("state", rState);
        }

        getR().setData("list", list);
        getR().setData("count", pageData.getTotal());

        return getR();
    }
}
