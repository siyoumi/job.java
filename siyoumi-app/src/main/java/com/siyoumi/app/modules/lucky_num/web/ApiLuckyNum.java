package com.siyoumi.app.modules.lucky_num.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.LnumGroup;
import com.siyoumi.app.entity.LnumNum;
import com.siyoumi.app.entity.LnumNumSet;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumGroup;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumSession;
import com.siyoumi.app.modules.lucky_num.service.SvcLnumNumSet;
import com.siyoumi.app.modules.lucky_num.vo.LNumGetVo;
import com.siyoumi.app.service.LnumNumService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/wxapp/lucky_num/api")
public class ApiLuckyNum
        extends WxAppApiController {
    /**
     * 初始化
     */
    @GetMapping("init")
    public XReturn init() {
        String groupId = input("group_id");
        String setId = input("set_id");
        if (XStr.isNullOrEmpty(groupId)) {
            return XReturn.getR(20034, "miss group_id");
        }
        LnumGroup entityGroup = SvcLNumGroup.getApp().first(groupId);
        if (entityGroup == null) {
            return XReturn.getR(20050, "group_id异常");
        }

        log.debug("号码组");
        Map<String, Object> mapGroup = XBean.toMap(entityGroup, new String[]{
                "lngroup_id",
                "lngroup_name",
                "lngroup_type",
        });


        if (XStr.hasAnyText(setId)) {
            log.debug("号类型");
            LnumNumSet entitySet = SvcLnumNumSet.getBean().first(entityGroup.getKey(), setId);
            if (entitySet == null) {
                return EnumSys.ERR_VAL.getR("类型ID异常");
            }

            Map<String, Object> mapSet = XBean.toMap(entitySet, new String[]{
                    "lnset_name",
                    "lnset_get_date_begin",
                    "lnset_get_date_end",
                    "lnset_pic",
                    "lnset_pic_bg",
                    "lnset_user_get_max",
                    "lnset_fun_enable",
                    "lnset_fun",
            });

            XReturn rValid = SvcLnumNumSet.getBean().valid(entitySet, getOpenid());
            getR().setData("set", mapSet);
            getR().setData("set_valid", rValid);
        }
        getR().setData("group", mapGroup);

        return getR();
    }

    /**
     * 我的号码列表
     */
    @GetMapping("num_list")
    public XReturn numList() {
        String groupId = input("group_id");
        if (XStr.isNullOrEmpty(groupId)) {
            return XReturn.getR(20034, "miss group_id");
        }
        LnumGroup entityGroup = SvcLNumGroup.getApp().first(groupId);
        if (entityGroup == null) {
            return XReturn.getR(20050, "group_id异常");
        }

        String type = input("type");
        if (XStr.isNullOrEmpty(type)) {
            type = "0";
        }

        InputData inputData = InputData.fromRequest();
        inputData.put("group_id", entityGroup.getKey());
        //inputData.put("openid", funcGetWxFrom());

        String[] select = {
                "lnum_id",
                "lnum_num",
                "lnum_create_date",
                "lnum_wxuser_id",
                "wxuser_nickName",
                "wxuser_headimgurl",
        };
        //query
        JoinWrapperPlus<LnumNum> query = SvcLNumGroup.getBean().numQuery(entityGroup.getLngroup_id(), inputData);
        query.join(WxUser.table(), "lnum_wxuser_id", WxUser.tableOpenid());
        query.select(select);
        query.orderByDesc("lnum_create_date");
        //page
        IPage<LnumNum> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), false);
        //list
        IPage<Map<String, Object>> pageData = LnumNumService.getBean().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        for (Map<String, Object> item : list) {
//            T__t_a_lnum_num entity = new T__t_a_lnum_num();
//            entity.funcFillMap(item);
//
//            String getJoinKey = (String) item.get("lnset_get_join_key");
        }

        getR().setData("list", list);

        return getR();
    }

    /**
     * 领号码
     */
    @PostMapping("num_get")
    public XReturn numGet(@Validated LNumGetVo vo, BindingResult result) {
        //通用验证
        XValidator.getResult(result, true, true);

        return SvcLNumGroup.getBean().numGet(vo);
    }

    /**
     * 奖品期列表
     */
    @GetMapping("session_list")
    public XReturn sessionList() {
        InputData inputData = InputData.fromRequest();
        return SvcLNumSession.getBean().list(inputData);
    }
}
