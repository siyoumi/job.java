package com.siyoumi.app.modules.app_admin.admin;

import com.siyoumi.app.modules.accsuper_admin.vo.SysAccsuperZzzAppVo;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccsuperZzzApp;
import com.siyoumi.entity.SysApp;
import com.siyoumi.service.SysAccsuperZzzAppService;
import com.siyoumi.service.SysAppService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/app_admin/app_admin__edit")
public class app_admin__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        SysAppService app = SysAppService.getBean();
        SysApp entityApp = app.loadEntity(getID());

        setPageTitle(XStr.concat(entityApp.getApp_name(), "-配置"));

        String key = input("key");
        if (XStr.isNullOrEmpty(key)) {
            key = "0";
        }

        Map<String, Object> data = new HashMap<>();

        return getR();
    }


    @Transactional
    @PostMapping("/save")
    public XReturn save(@Validated() SysAccsuperZzzAppVo entity, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        String[] listAppIds = entity.getApp_ids().split(",");
        //
        List<SysAccsuperZzzApp> list = Arrays.stream(listAppIds).map(appId -> {
            SysAccsuperZzzApp entityAcczapp = new SysAccsuperZzzApp();
            entityAcczapp.setAcczapp_app_id(appId);
            entityAcczapp.setAcczapp_x_id(entity.getAcczapp_x_id());
            return entityAcczapp;
        }).collect(Collectors.toList());
        //批量添加
        SysAccsuperZzzAppService.getBean().saveBatch(list);

        return getR();
    }
}
