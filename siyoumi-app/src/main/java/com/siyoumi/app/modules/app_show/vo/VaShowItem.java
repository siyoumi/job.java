package com.siyoumi.app.modules.app_show.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class VaShowItem {
    @NotBlank
    @Size(max = 50)
    String abc_name;
    @NotBlank
    @Size(max = 200)
    String abc_str_00;
    @NotBlank
    @Size(max = 200)
    String abc_str_01;
    @NotBlank
    @Size(max = 200)
    String abc_str_02;

    @JsonFormat(pattern = "yyyy-MM-dd 00:00:00", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd 00:00:00")
    LocalDateTime abc_date_00;
}
