package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;


//学生提交答案
@Data
public class VoEssTestAnswer {
    @HasAnyText(message = "缺少评测ID")
    String result_id;
    @HasAnyText(message = "题目答案")
    List<VoEssTestAnswerItem> items;
}
