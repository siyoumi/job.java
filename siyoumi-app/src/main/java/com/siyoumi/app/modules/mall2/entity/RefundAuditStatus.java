package com.siyoumi.app.modules.mall2.entity;

import com.siyoumi.util.IEnum;


public enum RefundAuditStatus
        implements IEnum {
    //0|待审核,1|审核已通过,-10|审核不通过,-20|用户撤回
    WAIT(0, "待审核"),
    PASS(1, "审核已通过"),
    NOTPASS(-10, "审核不通过"),
    CANCEL(-20, "用户撤回");


    private Integer key;
    private String val;

    RefundAuditStatus(Integer key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key.toString();
    }
}
