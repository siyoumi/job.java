package com.siyoumi.app.modules.app_menu.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.app_menu.vo.VaMenu;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.component.XApp;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_menu/app_menu__menu__edit")
public class app_menu__menu__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("菜单-编辑");

        SysAbcService app = SysAbcService.getBean();

        Map<String, Object> data = new HashMap<>();
        data.put("abc_order", 0);
        data.put("abc_str_02", new ArrayList<>());
        data.put("abc_str_03", new ArrayList<>());
        if (isAdminEdit()) {
            SysAbc entity = app.loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            dataAppend.put("abc_pid", entity.getAbc_pid());
            //
            //烹饪方式
            if (XStr.hasAnyText(entity.getAbc_str_02())) {
                dataAppend.put("abc_str_02", entity.getAbc_str_02().split(","));
            }
            //过敏源
            if (XStr.hasAnyText(entity.getAbc_str_03())) {
                dataAppend.put("abc_str_03", entity.getAbc_str_03().split(","));
            }
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //食用方式
        List<Map<String, Object>> listType = app.getListKV(XHttpContext.getAppId(), "app_menu_type");
        setPageInfo("list_type", listType);
        //烹饪方式
        List<Map<String, Object>> listType2 = app.getListKV(XHttpContext.getAppId(), "app_menu_type2");
        setPageInfo("list_type2", listType2);
        //过敏源
        List<Map<String, Object>> listType0 = app.getListKV(XHttpContext.getAppId(), "app_menu_type0");
        setPageInfo("list_type0", listType0);

        return getR();
    }


    //@Transactional
    @PostMapping("/save")
    public XReturn save(@Validated VaMenu vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        SysAbcService app = SysAbcService.getBean();

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {
            ignoreField.add("abc_id");
            ignoreField.add("abc_table");
            ignoreField.add("abc_pid");
        }

        InputData inputData = InputData.fromRequest();
        inputData.putAll(XBean.toMap(vo));
        if (isAdminAdd()) {
            vo.setAbc_table("app_menu_item");
            vo.setAbc_app_id(XHttpContext.getAppId());
        }

        return app.saveEntity(inputData, vo, true, ignoreField);
    }
}
