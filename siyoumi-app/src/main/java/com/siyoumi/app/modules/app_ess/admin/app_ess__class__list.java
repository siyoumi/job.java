package com.siyoumi.app.modules.app_ess.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.EssClass;
import com.siyoumi.app.modules.app_ess.service.SvcEssClass;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__class__list")
public class app_ess__class__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("班级列表");

        InputData inputData = InputData.fromRequest();

        JoinWrapperPlus<EssClass> query = SvcEssClass.getBean().listQuery(inputData);

        IPage<EssClass> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcEssClass.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> item : list) {
            EssClass entity = XBean.fromMap(item, EssClass.class);
            item.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcEssClass.getBean().delete(getIds());
    }
}
