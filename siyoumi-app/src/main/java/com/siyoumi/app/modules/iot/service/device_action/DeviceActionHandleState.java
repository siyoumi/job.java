package com.siyoumi.app.modules.iot.service.device_action;

import com.siyoumi.app.entity.IotDevice;
import com.siyoumi.app.modules.iot.entity.IotPinCode;
import com.siyoumi.app.modules.iot.service.DeviceActionHandle;
import com.siyoumi.app.modules.iot.service.SvcIotDevice;
import com.siyoumi.app.netty.NettyMqttUtil;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;

//获取设备状态
@Slf4j
public class DeviceActionHandleState
        extends DeviceActionHandle {
    @Override
    public XReturn send(IotDevice entityDevice) {
        IotPinCode code = IotPinCode.of("device_state", 0, 0, "");
        code.setActionId(SvcIotDevice.getBean().getMqttQueueId(entityDevice));

        XReturn r = code.toR();
        String msg = XStr.toJsonStr(r);
        log.info(msg);
        String mqttQueueId = SvcIotDevice.getBean().getMqttQueueId(entityDevice);
        Channel channel = NettyMqttUtil.getMapChannel().get(mqttQueueId);
        if (channel == null) {
            return XReturn.getR(20026, "设备不在线");
        }
        NettyMqttUtil.sendMessage(channel
                , SvcIotDevice.getBean().getTopic()
                , msg);

        //记录日志
        return r;
    }
}
