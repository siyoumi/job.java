package com.siyoumi.app.modules.sheet.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysSheet;
import com.siyoumi.app.entity.SysSheetRecord;
import com.siyoumi.app.modules.sheet.entity.SysSheetItem;
import com.siyoumi.app.modules.sheet.service.SvcSheet;
import com.siyoumi.app.service.SysSheetRecordService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/sheet/sheet__record__list")
public class sheet__record__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        if (XStr.isNullOrEmpty(getPid())) {
            return EnumSys.MISS_VAL.getR("miss pid");
        }
        SysSheet entitySheet = SvcSheet.getApp().getEntity(getPid());
        if (entitySheet == null) {
            return EnumSys.ERR_VAL.getR("pid 异常");
        }

        setPageTitle(entitySheet.getSheet_name(), "-填写记录");

        SysSheetRecordService appSheetRecord = SysSheetRecordService.getBean();

        List<String> select = new ArrayList<>();
        select.add("t_a_sheet_record.*");
        select.add("wxuser_openid");
        select.add("wxuser_nickname");

        InputData inputData = InputData.fromRequest();
        inputData.put("sheet_id", entitySheet.getKey());
        JoinWrapperPlus<SysSheetRecord> query = SvcSheet.getBean().listQueryRecord(inputData);
        query.select(select.toArray(new String[select.size()]));

        IPage<SysSheetRecord> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), !isAdminExport());
        //list
        IPage<Map<String, Object>> pageData = appSheetRecord.getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            SysSheetRecord entitySheetRecord = XBean.fromMap(data, SysSheetRecord.class);
            data.put("id", entitySheetRecord.getKey());

            return data;
        }).collect(Collectors.toList());

        List<SysSheetItem> items = SvcSheet.getBean().getItems(entitySheet.getKey());
        if (!isAdminExport()) {
            getR().setData("list", listData);
            getR().setData("count", count);

            setPageInfo("table_head", items);
        } else {
            //导出
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("scord_id", "id");
            tableHead.put("wxuser_openid", "openid");
            tableHead.put("wxuser_nickname", "微信名");
            tableHead.put("scord_create_date", "创建时间");
            for (SysSheetItem item : items) {
                tableHead.put(item.getField_key(), item.getLabel());
            }

            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }


        return getR();
    }

    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }

    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        SysSheetRecordService appSheetRecord = SysSheetRecordService.getBean();
        appSheetRecord.delete(getIds());

        return getR();
    }
}
