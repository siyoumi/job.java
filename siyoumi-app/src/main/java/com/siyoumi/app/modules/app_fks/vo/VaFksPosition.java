package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

//职位
@Data
public class VaFksPosition {
    private String fposi_acc_id;
    @Size(max = 50)
    @HasAnyText
    private String fposi_name;
    @HasAnyText
    private Integer fposi_level;
}
