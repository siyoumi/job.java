package com.siyoumi.app.modules.app_book.admin;

import com.siyoumi.app.entity.BookStore;
import com.siyoumi.app.entity.BookStoreTxt;
import com.siyoumi.app.modules.account.service.SvcAccount;
import com.siyoumi.app.modules.app_book.service.SvcBookItem;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.app.modules.app_book.service.SvcBookStoreGroup;
import com.siyoumi.app.modules.app_book.vo.*;
import com.siyoumi.app.service.BookStoreTxtService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/app_book/app_book__store__edit")
public class app_book__store__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商家-编辑");
        BookStore entity = null;
        if (isStore()) {
            entity = SvcBookStore.getApp().getEntity(getStoreId());
            if (entity == null) {
                return EnumSys.ERR_VAL.getR("商家帐号异常");
            }
        } else {
            if (isAdminEdit()) {
                entity = SvcBookStore.getApp().loadEntity(getID());
            }
        }

        //扩展字段
        VaBookStoreTxtInfo storeTxtInfo = new VaBookStoreTxtInfo();
        VaBookStoreMoneyInfo storeMoneyInfo = new VaBookStoreMoneyInfo(); //商家提现信息
        List<VoBookOrderRefundRule> refundRuleList = SvcBookStore.getBean().getRefundRuleDef();

        List<String> itemPlayIds = new ArrayList<>();


        Map<String, Object> data = new HashMap<>();
        data.put("bstore_sales0_rate", 0);
        data.put("bstore_sales1_rate", 0);
        data.put("bstore_deposit_rate", 0);
        if (entity != null) {
            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            BookStoreTxt entityTxt = BookStoreTxtService.getBean().getEntity(entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
            //省市区
            //data.put("city", List.of(entity.getBstore_province(), entity.getBstore_city(), entity.getBstore_district()));
            data.put("city", XStr.concat(entity.getBstore_province(), " ", entity.getBstore_city(), " ", entity.getBstore_district()));

            if (XStr.hasAnyText(entityTxt.getBstoret_info())) { //介绍
                storeTxtInfo = XJson.parseObject(entityTxt.getBstoret_info(), VaBookStoreTxtInfo.class);
            }
            if (XStr.hasAnyText(entityTxt.getBstoret_money_info())) { //商家提现信息
                storeMoneyInfo = XJson.parseObject(entityTxt.getBstoret_money_info(), VaBookStoreMoneyInfo.class);
            }
            if (XStr.hasAnyText(entity.getBstore_refund_rule())) { //商家提现信息
                refundRuleList = XJson.parseArray(entity.getBstore_refund_rule(), VoBookOrderRefundRule.class);
            }

            itemPlayIds = SvcBookItem.getBean().getItemIds("play", entity.getKey());
        }
        data.put("item_play_ids", itemPlayIds); //娱乐设施
        data.put("bstoret_info", storeTxtInfo);
        data.put("bstoret_money_info", storeMoneyInfo); //商家提现信息
        data.put("bstore_refund_rule", refundRuleList); //退款规则

        getR().setData("data", data);

        //帐号列表
        List<Map<String, Object>> listAcc = SvcAccount.getBean().getList("store");
        setPageInfo("list_acc", listAcc);
        setPageInfo("list_group", SvcBookStoreGroup.getBean().getList()); //分组
        //娱乐设备
        setPageInfo("items_play", SvcBookItem.getBean().getList("play", null));

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaBookStore vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        InputData inputData = InputData.fromRequest();
        //
        if (isAdminAdd()) {
            vo.setBstore_id(XApp.getStrID("S"));
            vo.setBstore_enable(1);
        }
        vo.setBstoret_info(null);
        //
        return SvcBookStore.getBean().save(inputData, vo);
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save1")
    public XReturn save1(@Validated() VaBookStore1 vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        InputData inputData = InputData.fromRequest();
        //
        if (isAdminAdd()) {
            vo.setBstore_id(XApp.getStrID("S"));
        }
        //
        VaBookStore voStore = new VaBookStore();
        XBean.copyProperties(vo, voStore);
        //
        return SvcBookStore.getBean().save(inputData, voStore);
    }
}