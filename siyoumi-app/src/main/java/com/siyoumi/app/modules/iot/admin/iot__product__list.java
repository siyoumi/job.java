package com.siyoumi.app.modules.iot.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FunWinSession;
import com.siyoumi.app.entity.IotProduct;
import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.app.modules.fun_win.service.SvcFunWinSession;
import com.siyoumi.app.modules.iot.service.SvcIotProduct;
import com.siyoumi.app.modules.prize.entity.EnumPrizeType;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSet;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/iot/iot__product__list")
public class iot__product__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("产品-列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "ipro_id",
                "ipro_create_date",
                "ipro_type",
                "ipro_name",
                "ipro_attr",
                "ipro_order",
        };
        JoinWrapperPlus<IotProduct> query = SvcIotProduct.getBean().listQuery(inputData);
        query.select(select);
        query.orderByAsc("ipro_order")
                .orderByDesc("id");

        IPage<IotProduct> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcIotProduct.getApp().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            IotProduct entity = SvcIotProduct.getApp().loadEntity(data);
            data.put("id", entity.getKey());
        }

        getR().setData("list", list);
        getR().setData("count", count);
        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcIotProduct.getBean().delete(List.of(ids));
    }
}
