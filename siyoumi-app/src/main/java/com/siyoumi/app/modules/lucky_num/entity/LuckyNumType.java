package com.siyoumi.app.modules.lucky_num.entity;

import com.siyoumi.util.IEnum;


//号码组类型
public enum LuckyNumType
        implements IEnum {
    //0|集卡,1|积分夺宝,2|报名抽奖
    LUCKY_NUM("lucky_num", "集卡"),
    FUN_WIN("fun_win", "积分夺宝"),
    ACT("sign_chou", "报名抽奖"),
    ;


    private String key;
    private String val;

    LuckyNumType(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
