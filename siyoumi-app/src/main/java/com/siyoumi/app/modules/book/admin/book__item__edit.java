package com.siyoumi.app.modules.book.admin;

import com.siyoumi.app.entity.SysBook;
import com.siyoumi.app.modules.book.entity.BookInfo;
import com.siyoumi.app.modules.book.service.SvcSysBook;
import com.siyoumi.app.modules.book.vo.VaBook;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/xadmin/book/book__item__edit")
public class book__item__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("预约-编辑");

        InputData inputData = InputData.fromRequest();

        Map<String, Object> data = new HashMap<>();
        data.put("book_order", 0);
        data.put("book_hide", 0);
        data.put("book_sign_total", 0);
        setPageInfo("book_info_json", new BookInfo());
        if (isAdminEdit()) {
            SysBook entity = SvcSysBook.getApp().loadEntity(getID());

            setPageInfo("book_info_json", SvcSysBook.getBean().getInfo(entity.getBook_info_json()));

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            dataAppend.put("url", SvcSysBook.getBean().getUrl(entity.getKey()));
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        return getR();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaBook vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        if (!isAdminEdit()) {
            vo.setBook_acc_id(getAccId());
        } else {
            vo.setBook_type(null);
            vo.setBook_acc_id(null);
        }

        InputData inputData = InputData.fromRequest();
        return SvcSysBook.getBean().edit(inputData, vo);
    }
}
