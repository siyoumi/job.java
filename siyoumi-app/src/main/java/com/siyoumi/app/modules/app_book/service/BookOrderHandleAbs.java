package com.siyoumi.app.modules.app_book.service;

import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.app.entity.BookSetDay;
import com.siyoumi.app.modules.app_book.vo.SpuDayData;
import com.siyoumi.app.modules.app_book.vo.VoBookOrder;
import com.siyoumi.util.XReturn;
import lombok.Data;

import java.util.List;

@Data
public abstract class BookOrderHandleAbs {
    VoBookOrder vo; //输入参数
    List<SpuDayData> listDay; //日期加价列表

    abstract public XReturn orderBeforeCheck();

    abstract public String getTitle();

    abstract public XReturn handle(BookOrder bookOrder);
}
