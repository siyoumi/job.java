package com.siyoumi.app.modules.app_book.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.BookSku;
import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.modules.app_book.entity.EnumBookSpuUserCount;
import com.siyoumi.app.modules.app_book.service.SvcBookSku;
import com.siyoumi.app.modules.app_book.service.SvcBookSpu;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.app.modules.app_book.vo.VoBookSkuAudit;
import com.siyoumi.app.sys.entity.EnumEnable;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_book/app_book__sku__list")
public class app_book__sku__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        String spuId = input("spu_id");
        BookSpu entitySpu = SvcBookSpu.getApp().loadEntity(spuId);

        setPageTitle(entitySpu.getBspu_name(), "-价格列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "bsku_id",
                "bsku_day",
                "bsku_day_price",
                "bsku_enable",
        };
        JoinWrapperPlus<BookSku> query = SvcBookSku.getBean().listQuery(inputData);
        query.select(select);
        //排序
        query.orderByAsc("bsku_day")
                .orderByDesc("bsku_day_price");

        IPage<BookSku> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcBookSku.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumEnable enumEnable = XEnumBase.of(EnumEnable.class); //上下架
        for (Map<String, Object> item : list) {
            BookSku entity = XBean.fromMap(item, BookSku.class);
            item.put("id", entity.getKey());
            //状态
            item.put("enable", enumEnable.get(entity.getBsku_enable()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcBookSku.getBean().delete(getIds());
    }

    //上下架
    @Transactional
    @PostMapping("/audit")
    public XReturn audit(@Validated VoBookSkuAudit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        return SvcBookSku.getBean().audit(vo);
    }
}
