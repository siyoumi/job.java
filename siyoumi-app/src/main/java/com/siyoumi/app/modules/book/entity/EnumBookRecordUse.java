package com.siyoumi.app.modules.book.entity;

import com.siyoumi.util.IEnum;


//号码组类型
public enum EnumBookRecordUse
        implements IEnum {
    UNUSE("0", "未使用"),
    USE("1", "已使用"),
    ;


    private String key;
    private String val;

    EnumBookRecordUse(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
