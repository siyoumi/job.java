package com.siyoumi.app.modules.account.entity;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaSysDepartment {
    @HasAnyText
    @Size(max = 50)
    private String dep_name;
    private Integer dep_order;
}
