package com.siyoumi.app.modules.lucky_num.admin;

import com.siyoumi.app.entity.LnumSession;
import com.siyoumi.app.entity.LnumSessionPrize;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumSession;
import com.siyoumi.app.modules.lucky_num.service.SvcLNumSessionPrize;
import com.siyoumi.app.modules.lucky_num.vo.LNumChouWinItem;
import com.siyoumi.app.modules.lucky_num.vo.LNumChouWin;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//开奖
@RestController
@RequestMapping("/xadmin/lucky_num/lucky_num__session_win")
public class lucky_num__session_win
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        InputData inputData = InputData.fromRequest();
        String sessionId = inputData.input("session_id");
        if (XStr.isNullOrEmpty(sessionId)) {
            return EnumSys.MISS_VAL.getR("miss session_id");
        }
        LnumSession entitySession = SvcLNumSession.getApp().loadEntity(sessionId);
        if (entitySession == null) {
            return EnumSys.ERR_VAL.getR("session_id异常");
        }
        setPageTitle(entitySession.getLns_name(), "奖品期-开奖");


        LNumChouWin winData = new LNumChouWin();
        winData.setSession_id(entitySession.getKey());

        List<LnumSessionPrize> listPrize = SvcLNumSessionPrize.getBean().list(entitySession.getKey());
        List<Map<String, Object>> listWin = new ArrayList<>();
        if (entitySession.win()) {
            listWin = SvcLNumSession.getBean().winList(entitySession.getKey());
        }

        List<LNumChouWinItem> listWinItem = new ArrayList<>();
        for (LnumSessionPrize entityPrize : listPrize) {
            List<Long> numsWin = new ArrayList<>();
            if (entitySession.win()) {
                //已开奖，需要显示中奖号码
                numsWin = listWin.stream().filter(itemWin ->
                {
                    return itemWin.get("lnwin_session_id").equals(entitySession.getKey())
                            && itemWin.get("lnwin_prize_id").equals(entityPrize.getLnsp_id());
                }).map(itemWin -> (Long) itemWin.get("lnwin_num")
                ).collect(Collectors.toList());
            }

            for (int i = 0; i < entityPrize.getLnsp_count(); i++) {
                LNumChouWinItem item = new LNumChouWinItem();
                item.setPrize_id(entityPrize.getLnsp_id());
                int prizeIndex = i + 1;
                item.setPrize_name(XStr.concat(entityPrize.getLnsp_name(), "-" + prizeIndex));
                //显示开奖号码
                if (entitySession.win()) {
                    item.setNum(numsWin.get(i));
                }

                listWinItem.add(item);
            }
        }
        winData.setItems(listWinItem);

        Map<String, Object> data = new HashMap<>();
        if (isAdminEdit()) {
        }
        getR().setData("data", data);

        setPageInfo("win", entitySession.win());
        setPageInfo("win_data", winData);

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/chou_win")
    public XReturn chouWin(@Validated @RequestBody LNumChouWin vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        if (!isAdminEdit()) {
        } else {

        }

        return SvcLNumSession.getBean().chouWin(vo);
    }
}
