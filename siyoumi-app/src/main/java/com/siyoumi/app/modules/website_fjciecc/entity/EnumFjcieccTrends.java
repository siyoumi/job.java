package com.siyoumi.app.modules.website_fjciecc.entity;

import com.siyoumi.util.IEnum;

//公司动态-
public enum EnumFjcieccTrends
        implements IEnum {
    P0("0", "业务动态"),
    P1("1", "集团动向"),
    P2("2", "公告公示"),
    P3("3", "党建活动"),
    ;


    private String key;
    private String val;

    EnumFjcieccTrends(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
