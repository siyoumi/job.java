package com.siyoumi.app.modules.app_ess.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;


//添加评测
@Data
public class VoEssTestAdd {
    String etest_uid;

    @HasAnyText(message = "缺少标题")
    @Size(max = 50, message = "名称长度不能超50")
    String etest_title;
    @HasAnyText(message = "缺少开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    LocalDateTime etest_date_begin;
    @HasAnyText(message = "缺少结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    LocalDateTime etest_date_end;

    @HasAnyText(message = "缺少评测时长")
    Integer etest_test_minute;
    @HasAnyText(message = "缺少交卷提醒")
    Integer etest_test_end_remind;
    @HasAnyText(message = "缺少限时提交")
    Integer etest_test_not_submit;

    Integer etest_random = 0;
    Integer etest_score_show = 0;
}
