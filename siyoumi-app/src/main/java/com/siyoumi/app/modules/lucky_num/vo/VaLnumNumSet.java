package com.siyoumi.app.modules.lucky_num.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

//号码发放
@Data
public class VaLnumNumSet {
    private String lnset_acc_id;
//    @HasAnyText
    private String lnset_group_id;
//    @HasAnyText
//    @Size(max = 50)
    private String lnset_name;
    private LocalDateTime lnset_get_date_begin;
    private LocalDateTime lnset_get_date_end;
    private String lnset_pic;
    private String lnset_pic_bg;
    private Integer lnset_user_get_max;
    private Integer lnset_fun_enable;
    private Long lnset_fun;
    //库存
    private Long stock_count_left;
    private Long stock_count_left_old;
}
