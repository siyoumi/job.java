package com.siyoumi.app.modules.sh.task;

import com.siyoumi.app.modules.sh.task.data.QmaiData;
import com.siyoumi.component.api.jj.JJ;
import com.siyoumi.component.api.jj.robot.RobotErr;
import com.siyoumi.task.EnumTask;
import com.siyoumi.task.TaskController;
import com.siyoumi.util.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//蒙自源签到
@RestController
@RequestMapping("/task/sh/task_qmai")
public class task_qmai
        extends TaskController {
    @Override
    protected List handleData() {
        List<String> list = new ArrayList<>();
        list.add("1");
        return list;
    }

    @Override
    protected void handleBefore(Object data) {

    }

    @Override
    protected XReturn handle(Object data) {
        List<QmaiData> list = new ArrayList<>();
        list.add(QmaiData.getIns("NAn6awOj9pRnyzDnxSMQSmf_ChGbWMbALI0uucsRO1I5QQ1wFWSpqimh2TbIMwv0", "13632350630", "小神童"));
        list.add(QmaiData.getIns("RCHwWMlc8Q8J2Pdo3SqVth_Ykt0rSl8he7dx3B9PuG9_TWt3Wb5V9HFa9DLVAqO5", "18819208821", "Double"));

        StringBuilder sendMsg = new StringBuilder();
        sendMsg.append("**蒙自源签到** \n\n");

        for (QmaiData item : list) {
            XReturn r = signIn(item);
            sendMsg.append(XStr.concat(item.getUsername(), "-签到：", r.getErrMsg(), "\n\n"));

            r = getFun(item);
            if (r.ok()) {
                Integer fun = r.getData("data");
                sendMsg.append(XStr.concat(item.getUsername(), "-积分：" + fun, "\n\n"));
            } else {
                sendMsg.append(XStr.concat(item.getUsername(), "-积分：", r.getErrMsg(), "\n\n"));
            }
        }

        //发jj
        JJ jj = JJ.getIns(RobotErr.class);
        jj.send(sendMsg.toString(), "", true);

        XLog.info(this.getClass(), "END:" + XDate.toDateTimeString());

        return EnumTask.STOP.getR();
    }


    private XReturn signIn(QmaiData data) {
        String url = "https://webapi.qmai.cn/web/catering/integral/sign/signIn";

        Map<String, Object> postData = new HashMap<>();
        postData.put("activityId", "745656309067620352");
        postData.put("appid", "wx6cff4ec7eaabc6db");
        postData.put("mobilePhone", data.getPhone());
        postData.put("userName", data.getUsername());

        XHttpClient client = XHttpClient.getInstance();
        String returnStr = client.postJson(url, apiHeader(data.getToken()), postData);
        return XReturn.parse(returnStr);
    }

    //获取积分
    private XReturn getFun(QmaiData data) {
        String url = "https://webapi.qmai.cn/web/catering/crm/total-points";

        Map<String, Object> postData = new HashMap<>();
        postData.put("appid", "wx6cff4ec7eaabc6db");

        XHttpClient client = XHttpClient.getInstance();
        String returnStr = client.postJson(url, apiHeader(data.getToken()), postData);
        return XReturn.parse(returnStr);
    }


    private Map<String, String> apiHeader(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Qm-From-Type", "catering");
        headers.put("Qm-User-Token", token);
        headers.put("Qm-From", "wechat");
        headers.put("store-id", "211679");

        return headers;
    }
}