package com.siyoumi.app.modules.app_book.service.handle_order;

import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.app.entity.BookStore;
import com.siyoumi.app.entity.SysSale;
import com.siyoumi.app.modules.app_book.service.BookOrderHandle;
import com.siyoumi.app.modules.app_book.service.BookOrderHandleAbs;
import com.siyoumi.app.modules.app_book.service.SvcBookStore;
import com.siyoumi.app.modules.user.service.SvcSysSale;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.component.XApp;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;

import java.math.BigDecimal;

/**
 * 分帐，平台分账，主播分帐
 */
public class BookOrderHandle90SharePrice
        extends BookOrderHandleAbs {
    static public BookOrderHandle90SharePrice of() {
        return new BookOrderHandle90SharePrice();
    }

    @Override
    public XReturn orderBeforeCheck() {

        return EnumSys.OK.getR();
    }

    @Override
    public String getTitle() {
        return "平台分销员分帐";
    }

    @Override
    public XReturn handle(BookOrder entityOrder) {
        BookStore entityStore = SvcBookStore.getApp().getEntity(entityOrder.getBorder_store_id());
        //赋值分成比率
        {
            //平台
            entityOrder.setBorder_share_app_rate(entityStore.getBstore_app_rate());
            //分销员
            {
                entityOrder.setBorder_share_sales_rate(BigDecimal.ZERO);
                SysSale entitySale = SvcSysSale.getBean().getEntitySale(entityOrder.getBorder_uid());
                if (entitySale != null) {
                    entityOrder.setBorder_share_sales_uid(entitySale.getSale_uid());
                    entityOrder.setBorder_share_sales_level(entitySale.getSale_level());

                    entityOrder.setBorder_share_sales_rate(SvcBookStore.getSalesRate(entityStore, entitySale.getSale_level()));
                }
            }
        }
        //计算分成金额
        matchSharePrice(entityOrder);

        return EnumSys.OK.getR();
    }

    /**
     * 根据分成比率，计算分成金额
     *
     * @param entityOrder
     */
    static public void matchSharePrice(BookOrder entityOrder) {
        BigDecimal priceShareFinal = entityOrder.getBorder_price_share_final();
        LogPipeLine.getTl().setLogMsg("分账总金额：" + priceShareFinal);
        //平台
        if (entityOrder.getBorder_share_app_rate().compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal appPrice = XApp.toDecimalPay(priceShareFinal.multiply(XApp.decimal100(entityOrder.getBorder_share_app_rate())));
            appPrice = appPrice.subtract(entityOrder.getBorder_price_down_fun()); //扣减积分抵扣
            entityOrder.setBorder_share_app_price(appPrice);
            LogPipeLine.getTl().setLogMsg("积分抵扣：" + entityOrder.getBorder_price_down_fun());
            LogPipeLine.getTl().setLogMsg("平台分账：" + appPrice);
            priceShareFinal = priceShareFinal.subtract(appPrice);
        }
        //分销员
        if (entityOrder.getBorder_share_sales_rate().compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal salesPrice = XApp.toDecimalPay(priceShareFinal.multiply(XApp.decimal100(entityOrder.getBorder_share_sales_rate())));
            LogPipeLine.getTl().setLogMsg("分销员分账：" + salesPrice);
            priceShareFinal = priceShareFinal.subtract(salesPrice);
        }
        LogPipeLine.getTl().setLogMsg("商家分账：" + priceShareFinal);
        entityOrder.setBorder_share_store_price(priceShareFinal);
    }
}
