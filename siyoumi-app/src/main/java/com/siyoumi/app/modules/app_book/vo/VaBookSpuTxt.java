package com.siyoumi.app.modules.app_book.vo;

import lombok.Data;

//房源
@Data
public class VaBookSpuTxt {
    private String desc = "";
    private String refund_food = "";
    private String book_remind = "";
}
