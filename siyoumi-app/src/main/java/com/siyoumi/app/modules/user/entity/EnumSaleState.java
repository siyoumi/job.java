package com.siyoumi.app.modules.user.entity;

import com.siyoumi.component.XEnumBase;


public class EnumSaleState
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "待审核");
        put(1, "审核通过");
        put(10, "禁用");
    }
}
