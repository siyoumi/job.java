package com.siyoumi.app.modules.iot.web;

import com.siyoumi.app.entity.IotDevice;
import com.siyoumi.app.entity.IotProduct;
import com.siyoumi.app.modules.iot.service.DeviceActionHandle;
import com.siyoumi.app.modules.iot.service.SvcIotProduct;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//远程开机
@Slf4j
@RestController
@RequestMapping("/wxapp/iot/power_on")
public class ApiIotPowerOn
        extends ApiIotBase {
    /**
     * 发送指令
     */
    @GetMapping("action")
    public XReturn action() {
        IotDevice entityDevice = getDevice(true);
        IotProduct entityPro = SvcIotProduct.getApp().first(entityDevice.getIdev_pro_id());

        DeviceActionHandle handle = DeviceActionHandle.getInstance(entityPro);
        return handle.send(entityDevice);
    }
}
