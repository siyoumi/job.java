package com.siyoumi.app.modules.app_ess.service;

import com.baomidou.mybatisplus.annotation.TableId;
import com.siyoumi.app.entity.EssFile;
import com.siyoumi.app.entity.EssQuestion;
import com.siyoumi.app.entity.EssUser;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_ess.vo.VaEssFile;
import com.siyoumi.app.modules.app_ess.vo.VoEssQuestion;
import com.siyoumi.app.modules.app_ess.vo.VoEssQuestionAddBatch;
import com.siyoumi.app.service.EssFileService;
import com.siyoumi.app.sys.service.SvcSysUv;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

//资源文件
@Slf4j
@Service
public class SvcEssFile
        implements IWebService {
    static public SvcEssFile getBean() {
        return XSpringContext.getBean(SvcEssFile.class);
    }

    static public EssFileService getApp() {
        return EssFileService.getBean();
    }

    static public List<String> fileAccept() {
        List<String> list = new ArrayList<>();
        list.add("jpg");
        list.add("mp4");
        list.add("png");
        list.add("txt");
        list.add("pdf");
        list.add("svgz");
        return list;
    }

    public JoinWrapperPlus<EssFile> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<EssFile> listQuery(InputData inputData) {
        String name = inputData.input("name");
        String type = inputData.input("type");
        String typeSys = inputData.input("type_sys");
        String moduleId = inputData.input("module_id");

        JoinWrapperPlus<EssFile> query = getApp().join();
        query.eq("efile_x_id", XHttpContext.getX());
        query.orderByAsc("efile_order")
                .orderByDesc("efile_id");
        if (XStr.hasAnyText(name)) { //名称
            query.like("efile_name", name);
        }
        if (XStr.hasAnyText(type)) { //分类
            query.eq("efile_type", type);
        }
        if (XStr.hasAnyText(typeSys)) { //资源类型
            query.eq("efile_type_sys", typeSys);
        } else {
            query.in("efile_type_sys", 0, 1);
        }
        if (XStr.hasAnyText(moduleId)) { //场景
            query.eq("efile_module_id", moduleId);
        }

        return query;
    }

    public XReturn edit(InputData inputData, VaEssFile vo) {
        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminEdit()) {
            ignoreField.add("efile_acc_id");
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, true, ignoreField);
            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }


    /**
     * 更新数量
     *
     * @param fileId
     */
    public void updateCollect(String fileId) {
        Long total = SvcSysUv.getBean().uvTotal("file", fileId);

        EssFile entityUpdate = new EssFile();
        entityUpdate.setEfile_id(fileId);
        entityUpdate.setEfile_collect_count(total);
        getApp().updateById(entityUpdate);
    }
}
