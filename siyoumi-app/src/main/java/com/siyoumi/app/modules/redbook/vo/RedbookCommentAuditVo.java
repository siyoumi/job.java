package com.siyoumi.app.modules.redbook.vo;

import lombok.Data;

import java.util.List;

//评论审核
@Data
public class RedbookCommentAuditVo {
    List<String> ids;
    Integer enable;
}
