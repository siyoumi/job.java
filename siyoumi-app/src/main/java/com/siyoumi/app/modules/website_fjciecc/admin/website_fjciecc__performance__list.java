package com.siyoumi.app.modules.website_fjciecc.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.website_fjciecc.entity.EnumFjcieccPerformance;
import com.siyoumi.app.modules.website_fjciecc.service.WebSiteFjcieccListBase;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/website_fjciecc/website_fjciecc__performance__list")
public class website_fjciecc__performance__list
        extends WebSiteFjcieccListBase {
    @Override
    public String title() {
        return "典型业绩";
    }

    @Override
    public String abcTable() {
        return "fjciecc_performance";
    }

    //for处理
    @Override
    public void forHandle(List<Map<String, Object>> list) {
        SysAbcService svcAbc = SysAbcService.getBean();

        LinkedHashMap<String, String> types = IEnum.toMap(EnumFjcieccPerformance.class);
        for (Map<String, Object> data : list) {
            SysAbc entityAbc = svcAbc.loadEntity(data);
            data.put("id", entityAbc.getKey());
            data.put("type", types.get(entityAbc.getAbc_type()));
        }

        setPageInfo("types", types);
    }

    //导出处理
    @Override
    public List<List<String>> exportHandle(IPage<Map<String, Object>> pageData, List<Map<String, Object>> list) {
        LinkedMap<String, String> tableHead = new LinkedMap<>();
        tableHead.put("abc_id", "id");
        tableHead.put("abc_name", "标题");
        tableHead.put("type", "类型");
        tableHead.put("abc_create_date", "创建时间");

        return adminExprotData(tableHead, list, pageData.getCurrent() == 1);
    }

    @GetMapping()
    public XReturn index() {
        return super.index();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return super.del(ids);
    }
}
