package com.siyoumi.app.modules.app_fks.service.file_auth;

import com.siyoumi.app.modules.app_fks.service.FksFileAuthHandle;
import com.siyoumi.app.modules.app_fks.service.SvcFksFile;
import com.siyoumi.app.modules.app_fks.vo.FksFileAuthHandleData;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;

//查看权限
public class FksFileAuthHandleLook
        extends FksFileAuthHandle {
    @Override
    protected XReturn check(FksFileAuthHandleData data) {
        if (SvcFksFile.getBean().existsAuth(data.getEntityFile().getKey(), data.getEntityUser().getKey())) { //有配置权限
            return EnumSys.OK.getR();
        }

        return XReturn.getR(20018, "查看权限不足");
    }
}
