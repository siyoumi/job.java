package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;

//标签删除
@Data
public class FksTagDel {
    @HasAnyText
    private List<String> ids; //标签ID
    private String uid;
}
