package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.math.BigDecimal;

//退款审核
@Data
public class VoBookOrderRefundAudit {
    @HasAnyText
    private String refund_id; //退款订单
    @HasAnyText
    private BigDecimal refund_price; //退款金额

    @HasAnyText
    @EqualsValues(vals = {"10", "1"})
    private Integer audit_status; //退款状态

    @Size(max = 50)
    private String audit_desc;
}
