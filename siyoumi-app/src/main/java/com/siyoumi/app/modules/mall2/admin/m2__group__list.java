package com.siyoumi.app.modules.mall2.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.M2Group;
import com.siyoumi.app.modules.mall2.service.SvcM2Group;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.component.http.InputData;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/mall2/m2__group__list")
public class m2__group__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商品分类-列表");

        SvcM2Group app = SvcM2Group.getBean();

        InputData inputData = InputData.fromRequest();
        QueryWrapper<M2Group> query = app.listQuery(inputData);

        IPage<M2Group> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<M2Group> pageData = SvcM2Group.getApp().mapper().get(page, query);
        List<M2Group> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(entity ->
        {
            HashMap<String, Object> data = new HashMap<>(entity.toMap());
            data.put("id", entity.getKey());

            return data;
        }).collect(Collectors.toList());

        getR().setData("list", listData);
        getR().setData("count", count);
        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        SvcM2Group app = SvcM2Group.getBean();
        return app.delete(Arrays.asList(ids));
    }
}
