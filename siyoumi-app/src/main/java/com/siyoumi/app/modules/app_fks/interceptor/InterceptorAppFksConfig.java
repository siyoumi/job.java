package com.siyoumi.app.modules.app_fks.interceptor;

import com.siyoumi.app.modules.act.interceptor.ActApiInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorAppFksConfig
        implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //过滤器
        registry.addInterceptor(new AppFksApiInterceptor())
                .addPathPatterns("/wxapp/app_fks/api/**")
                .order(10);
        ;
    }
}
