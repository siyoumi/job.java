package com.siyoumi.app.modules.setting.admin;

import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.siyoumi.config.SysConfig;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.component.XRedis;
import com.siyoumi.util.*;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/setting/setting__edit")
public class setting__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("微信平台");

        Map<String, Object> data = new HashMap<>();
        SysAccsuperConfig entityConfig = XHttpContext.getXConfig(false);
        if (entityConfig != null) {
            SysConfig sysConfig = SysConfig.getIns();
            String appRoot = sysConfig.getAppRoot();
            String url = XStr.concat(appRoot, "wx/center?x=", entityConfig.getKey());

            HashMap<String, Object> dataAppend = new HashMap<>();
            dataAppend.put("oauth_root", sysConfig.getAppRoot());
            dataAppend.put("url", url);
            dataAppend.put("id", entityConfig.getKey());
            dataAppend.put("is_x", entityConfig.getKey().equals("x"));
            dataAppend.put("is_dev", false);

            //合并
            data = entityConfig.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //公众号站点
        List<Map<String, Object>> listWx = SysAccsuperConfigService.getBean().getList("wx");
        setPageInfo("list_wx", listWx);


        return getR();
    }

    @PostMapping("/save")
    @Transactional
    public XReturn save(@Validated() SysAccsuperConfig entityConfig, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        SysAccsuperConfigService app = SysAccsuperConfigService.getBean();
        JoinWrapperPlus<SysAccsuperConfig> query = app.join();
        query.eq("aconfig_app_id", entityConfig.getAconfig_app_id());
        if (isAdminEdit()) {
            query.ne("aconfig_id", getID());
        }
        SysAccsuperConfig entityConfigApp = app.first(query);
        if (entityConfigApp != null) {
            result.addError(XValidator.getErr("aconfig_app_id", "appID已存在"));
        }
        XValidator.getResult(result);
        //
        //entityConfig.getAconfig_app_id()
        //
        XLog.debug(this.getClass(), entityConfig);
        app.updateById(entityConfig);
        //清徐缓存
        app.delEntityCache(entityConfig.getKey());

        return getR();
    }


    //清除token
    @GetMapping("/redisClearToken")
    public XReturn redisClearToken() {
        SysAccsuperConfigService app = SysAccsuperConfigService.getBean();

        //还原时间
        UpdateChainWrapper<SysAccsuperConfig> update = app.update();
        update.set("aconfig_access_token_date", XDate.date2000())
                .eq(app.fdKey(), XHttpContext.getX())
                .update();
        //清空redis
        XRedis.getBean().del(app.getRedisKeyAccessToken());

        return getR();
    }
}
