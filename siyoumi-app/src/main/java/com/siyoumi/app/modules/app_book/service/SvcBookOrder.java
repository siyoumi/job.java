package com.siyoumi.app.modules.app_book.service;

import com.siyoumi.app.entity.*;
import com.siyoumi.app.modules.app_book.service.handle_order.BookOrderHandle90SharePrice;
import com.siyoumi.app.modules.app_book.vo.*;
import com.siyoumi.app.modules.fun.service.SvcFun;
import com.siyoumi.app.modules.user.service.SvcSysSale;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.app.modules.user.vo.VoSaleAddRecord;
import com.siyoumi.app.service.*;
import com.siyoumi.app.sys.service.IPayOrder;
import com.siyoumi.app.sys.service.payorder.PayOrder;
import com.siyoumi.app.sys.service.wxapi.WxApiPay;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

//订单
@Slf4j
@Service
public class SvcBookOrder
        implements IWebService {
    static public SvcBookOrder getBean() {
        return XSpringContext.getBean(SvcBookOrder.class);
    }

    static public BookOrderService getApp() {
        return BookOrderService.getBean();
    }

    /**
     * 获取是否存在有效订单
     *
     * @param uid
     */
    public Boolean existOrderFirstEnable(String uid) {
        JoinWrapperPlus<BookOrder> query = listQuery();
        query.eq("border_uid", uid).eq("border_enable", 1);
        query.select("border_uid");
        return getApp().firstMap(query) != null;
    }

    public JoinWrapperPlus<BookOrder> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<BookOrder> listQuery(InputData inputData) {
        String id = inputData.input("id");
        String phone = inputData.input("phone");
        String userName = inputData.input("user_name");
        String spuId = inputData.input("spu_id");
        String status = inputData.input("status");
        String refund = inputData.input("refund");

        JoinWrapperPlus<BookOrder> query = getApp().join();
        query.eq("border_x_id", XHttpContext.getX());
        if (XStr.hasAnyText(id)) { //订单号
            query.eq("border_id", id);
        } else {
            if (XStr.hasAnyText(phone)) { //手机号
                query.eq("border_user_phone", phone);
            }
            if (XStr.hasAnyText(userName)) { //姓名
                query.like("border_user_name", userName);
            }
            if (XStr.hasAnyText(spuId)) { //房源
                query.eq("border_spu_id", spuId);
            }
            if (XStr.hasAnyText(refund)) { //申请退款状态
                query.eq("border_refund", refund);
            }
            if (XStr.hasAnyText(status)) { //订单状态
                query.eq("border_status", status);
            }
        }

        return query;
    }

    public JoinWrapperPlus<BookRefundOrder> listRefundQuery() {
        return listRefundQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<BookRefundOrder> listRefundQuery(InputData inputData) {
        String orderId = inputData.input("order_id");
        String auditStatus = inputData.input("audit_status");

        JoinWrapperPlus<BookRefundOrder> query = BookRefundOrderService.getBean().join();
        query.eq("brefo_x_id", XHttpContext.getX());
        if (XStr.hasAnyText(orderId)) { //订单号
            query.eq("brefo_order_id", orderId);
        } else {
            if (XStr.hasAnyText(auditStatus)) { //申请退款状态
                query.eq("brefo_audit_status", auditStatus);
            }
        }

        return query;
    }

    /**
     * 赠送积分数值
     */
    public Long giveFunNum() {
        return 1000L;
    }

    /**
     * 下单
     *
     * @param vo
     */
    public XReturn order(VoBookOrder vo) {
        LogPipeLine.getTl().setLogMsg(vo);
        //房源
        BookSpu entitySpu = SvcBookSpu.getApp().getEntity(vo.getSpu_id());
        XValidator.isNull(entitySpu, "spu_id error");
        XValidator.err(SvcBookSpu.getBean().valid(entitySpu));
        if (vo.getSet_user_count() < vo.getSpu_count()) {
            return XReturn.getR(20071, "每个房间至少要有1人用餐");
        }
        if (vo.getSet_user_count() > (entitySpu.getBspu_user_count() * vo.getSpu_count())) {
            return XReturn.getR(20081, "用餐人数不能大于房间入住人数");
        }
        //商家
        BookStore entityStore = SvcBookStore.getApp().getEntity(entitySpu.getBspu_store_id());
        XValidator.err(SvcBookStore.getBean().valid(entityStore));
        if (vo.getPay_deposit() == 1) {
            if (entityStore.getBstore_deposit_enable() != 1) {
                return XReturn.getR(20091, "商家未开启支付订金");
            }
        }
        //sku
        BookSku entitySku = SvcBookSku.getBean().getEntitySku(entitySpu.getKey(), vo.getDay_count());
        XValidator.isNull(entitySku, "房间未配置sku");
        if (!vo.getSku_id().equals(entitySku.getKey())) {
            return XReturn.getR(20101, "sku配置有变动，请刷新页面再试");
        }
        //套餐
        BookSet entitySet = SvcBookSet.getApp().getEntity(vo.getSet_id());
        XValidator.isNull(entitySet, "套餐ID异常");
        XValidator.err(SvcBookSet.getBean().valid(entitySet));
        //
        LocalDateTime dateBegin = vo.getDate_begin();
        LocalDateTime dateEnd = vo.getDate_begin().plusDays(vo.getDay_count() - 1);
        LogPipeLine.getTl().setLogMsgFormat("日期：{0} - {1}", dateEnd.toString(), dateEnd.toString());
        //
        //检查库存
        //List<LocalDateTime> dateList = XDate.getDateList(dateBegin, dateEnd, null);
        List<SpuDayData> listDay = SvcBookSpu.getBean().listDay(entitySpu, dateBegin, dateEnd);
        List<SpuDayData> listDayLeft = listDay.stream().filter(item -> item.getLeft_total() < vo.getSpu_count()).collect(Collectors.toList());
        if (!listDayLeft.isEmpty()) { //存在库存不足的日期
            List<String> dayStr = listDayLeft.stream().map(item -> XDate.toDateString(item.getDate())).collect(Collectors.toList());
            return XReturn.getR(20111, "库存不足，" + dayStr);
        }
        //
        //检查工厂检查
        BookOrderHandle orderHandle = BookOrderHandle.of();
        orderHandle.setVo(vo);
        orderHandle.setListDay(listDay);
        XValidator.err(orderHandle.orderBeforeCheck());
        //
        //开始下单
        return XApp.getTransaction().execute(status -> {
            SvcBookSpu.getApp().getEntityLock(entitySpu.getKey()); //锁房间

            LogPipeLine.getTl().setLogMsg("开始扣库存");
            LogPipeLine.getTl().setLogMsg("开始扣库存->添加每日库存记录");
            {
                VaBookSpuDay vaBookSpuDay = new VaBookSpuDay();
                vaBookSpuDay.setBspud_spu_id(entitySpu.getKey());
                vaBookSpuDay.setBegin_date(dateBegin);
                vaBookSpuDay.setEnd_date(dateEnd);
                vaBookSpuDay.setBspud_spu_add_price(BigDecimal.ZERO);
                vaBookSpuDay.setBspud_stock_add(0);
                SvcBookSpu.getBean().dayEditBatch(vaBookSpuDay);
            }
            LogPipeLine.getTl().setLogMsg("开始扣库存->批量扣库存");
            {
                VaBookSpuDayUpdateStock updateStock = VaBookSpuDayUpdateStock.of(entitySpu.getKey(), dateBegin, dateEnd, 1);
                SvcBookSpu.getBean().dayBatchUpdateStock(updateStock);
            }

            BookOrder entityOrder = new BookOrder();
            entityOrder.setBorder_x_id(XHttpContext.getX());
            entityOrder.setBorder_id(XApp.getStrID("O"));
            entityOrder.setBorder_uid(getUid());
            entityOrder.setBorder_uid_order(getUid());
            entityOrder.setBorder_uid_pay(getUid());
            entityOrder.setBorder_pay_deposit(vo.getPay_deposit()); //支付订金

            entityOrder.setBorder_user_name(vo.getUser_name());
            entityOrder.setBorder_user_phone(vo.getUser_phone());
            //spu
            entityOrder.setBorder_store_id(entitySpu.getBspu_store_id());
            entityOrder.setBorder_spu_id(vo.getSpu_id());
            entityOrder.setBorder_spu_count(vo.getSpu_count().longValue());
            //sku
            entityOrder.setBorder_sku_id(entitySku.getKey());
            entityOrder.setBorder_sku_day_price(entitySku.getBsku_day_price());
            entityOrder.setBorder_sku_user_count(entitySpu.getBspu_user_count()); //入住人数
            //set
            entityOrder.setBorder_set_id(vo.getSet_id());
            entityOrder.setBorder_set_name(entitySet.getBset_name());
            entityOrder.setBorder_set_day_price(entitySet.getBset_day_price());
            entityOrder.setBorder_set_user_count(vo.getSet_user_count()); //用餐人数
            //日期
            entityOrder.setBorder_date_begin(dateBegin);
            entityOrder.setBorder_date_end(dateEnd);
            entityOrder.setBorder_date_end_final(dateEnd);
            entityOrder.setBorder_day(vo.getDay_count());
            entityOrder.setBorder_day_final(vo.getDay_count());
            entityOrder.setBorder_enable(1);
            //
            entityOrder.setBorder_desc(vo.getDesc());

            //数据处理
            orderHandle.handle(entityOrder);
            getApp().save(entityOrder);

            LogPipeLine.getTl().setLogMsg("支付订单创建");
            newPayOrder(entityOrder, "预约" + entitySpu.getBspu_name());

            XReturn r = XReturn.getR(0);
            r.setData("entity", entityOrder);
            return r;
        });
    }

    /**
     * 支付订单
     *
     * @param entityOrder
     * @param desc
     */
    protected SysOrder newPayOrder(BookOrder entityOrder, String desc) {
        SysOrder entity = new SysOrder();
        entity.setOrder_id(entityOrder.getKey());
        entity.setOrder_app_id("app_book");
        entity.setOrder_x_id(entityOrder.getBorder_x_id());
        entity.setOrder_uid(entityOrder.getBorder_uid());
        entity.setOrder_openid(SvcSysUser.getBean().getOpenid(entityOrder.getBorder_uid(), true));
        //订单过期时间
        LocalDateTime expireTime = LocalDateTime.now().plusMinutes(10);
        entity.setOrder_pay_expire_date(expireTime);
        //金额
        entity.setOrder_price(entityOrder.getBorder_price_final());
        entity.setOrder_pay_price(entityOrder.getBorder_price_pay());
        entity.setOrder_desc(desc);
        SysOrderService.getBean().save(entity);

        return entity;
    }

    //订单确认
    public XReturn confirm(VaBookOrderConfirm vo) {
        List<BookOrder> listOrder = getApp().get(vo.getIds());
        for (BookOrder entityOrder : listOrder) {
            if (entityOrder.getBorder_status() != 5) {
                continue;
            }
            if (vo.getConfirm() == 1) { //确认
                updateStatus(entityOrder, 10);
            } else { //拒绝，退款
                updateStatus(entityOrder, 7);
            }
        }

        return EnumSys.OK.getR();
    }

    //确认入住
    public XReturn checkin(VaBookOrderCheckin vo) {
        List<BookOrder> listOrder = getApp().get(vo.getIds());
        for (BookOrder entityOrder : listOrder) {
            if (entityOrder.getBorder_date_begin().isAfter(XDate.now())) {
                return XReturn.getR(20264, entityOrder.getBorder_id() + "：未到时间入住");
            }

            updateStatus(entityOrder, 15);
        }

        return EnumSys.OK.getR();
    }

    //退房
    public XReturn checkout(VaBookOrderCheckout vo) {
        List<BookOrder> listOrder = getApp().get(vo.getIds());
        for (BookOrder entityOrder : listOrder) {
            updateStatus(entityOrder, 20);
        }

        return EnumSys.OK.getR();
    }

    /**
     * 申请退款
     *
     * @param vo
     */
    public XReturn refundApply(VoBookOrderRefundApply vo) {
        BookOrder entityOrder = getApp().getEntity(vo.getOrder_id());
        XValidator.isNull(entityOrder, "订单号异常");

        if (entityOrder.getBorder_status() < 5) {
            return XReturn.getR(20288, "订单未支付");
        }
        if (entityOrder.getBorder_refund() != 0) {
            return XReturn.getR(20298, "订单已申请退款");
        }

        BookStore entityStore = SvcBookStore.getApp().getEntity(entityOrder.getBorder_store_id());
        BigDecimal refundRate = SvcBookStore.getBean().getRefundRate(entityStore.getKey(), entityOrder.getBorder_date_begin()); //费率

        BigDecimal refundPriceDown = BigDecimal.ZERO; //退款手续费
        if (refundRate.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal rate = refundRate.divide(BigDecimal.valueOf(100), 2, RoundingMode.DOWN);
            refundPriceDown = entityOrder.getBorder_price_pay().multiply(rate);
            refundPriceDown = XApp.toDecimalPay(refundPriceDown);
        }
        BigDecimal refundPriceDownX = refundPriceDown;
        BigDecimal refundPrice = entityOrder.getBorder_price_pay().subtract(refundPriceDown); //申请退款金额

        return XApp.getTransaction().execute(status -> {
            BookRefundOrder entityRefund = new BookRefundOrder();
            entityRefund.setAutoID();
            entityRefund.setBrefo_x_id(entityOrder.getBorder_x_id());
            entityRefund.setBrefo_order_id(entityOrder.getKey());
            entityRefund.setBrefo_refund_id(entityOrder.getKey());
            entityRefund.setBrefo_store_id(entityOrder.getBorder_store_id());
            entityRefund.setBrefo_uid(entityOrder.getBorder_uid());
            entityRefund.setBrefo_price_pay(entityOrder.getBorder_price_pay());
            entityRefund.setBrefo_price(refundPrice); //申请退款金额
            entityRefund.setBrefo_price_down(refundPriceDownX); //手续费
            BookRefundOrderService.getBean().save(entityRefund);

            BookOrder entityOrderUpdate = new BookOrder();
            entityOrderUpdate.setBorder_id(entityOrder.getKey());
            entityOrderUpdate.setBorder_refund(10);
            entityOrderUpdate.setBorder_refund_date(XDate.now());
            SvcBookOrder.getApp().updateById(entityOrderUpdate);

            return EnumSys.OK.getR();
        });
    }

    /**
     * 退款审核
     *
     * @param vo
     */
    @Transactional(rollbackFor = Exception.class)
    public XReturn refundAudit(VoBookOrderRefundAudit vo) {
        BookRefundOrder entityOrderRefund = BookRefundOrderService.getBean().first(vo.getRefund_id());
        XValidator.isNull(entityOrderRefund, "退款订单ID异常");
        if (entityOrderRefund.getBrefo_audit_status() != 0) {
            return XReturn.getR(20356, "已审核");
        }
        BookOrder entityBookOrder = SvcBookOrder.getApp().getEntity(entityOrderRefund.getBrefo_order_id());
        XValidator.isNull(entityOrderRefund, "订单不存在");
        if (vo.getAudit_status() == 1) {
            if (vo.getRefund_price().compareTo(entityBookOrder.getBorder_price_pay()) > 0) {
                return XReturn.getR(20366, "退款金额不能大于支付金额");
            }
        }

        //更新退款订单审核状态
        BookRefundOrder entityOrderRefundUpdate = new BookRefundOrder();
        entityOrderRefundUpdate.setBrefo_id(entityOrderRefund.getKey());
        entityOrderRefundUpdate.setBrefo_audit_status(vo.getAudit_status());
        entityOrderRefundUpdate.setBrefo_audit_desc(vo.getAudit_desc());
        if (vo.getAudit_status() == 1) { //审核通过
            entityOrderRefundUpdate.setBrefo_refund_price(vo.getRefund_price());
        }
        BookRefundOrderService.getBean().saveOrUpdatePassEqualField(entityOrderRefund, entityOrderRefundUpdate);

        //更新订单状态
        BookOrder entityOrderUpdate = new BookOrder();
        entityOrderUpdate.setBorder_id(entityOrderRefund.getBrefo_order_id());
        if (vo.getAudit_status() == 10) { //不通过
            entityOrderUpdate.setBorder_refund(0);
            entityOrderUpdate.setBorder_refund_date(XDate.date2000());
        } else { //通过
            entityOrderUpdate.setBorder_refund(1);
            entityOrderUpdate.setBorder_refund_date(XDate.now());
            //重新计算分摊金额
            entityOrderUpdate.setBorder_price_share_final(entityBookOrder.getBorder_price_original().subtract(vo.getRefund_price()));
            //为了计算，需要赋值
            entityOrderUpdate.setBorder_share_app_rate(entityBookOrder.getBorder_share_app_rate());
            entityOrderUpdate.setBorder_share_sales_rate(entityBookOrder.getBorder_share_sales_rate());
            entityOrderUpdate.setBorder_price_down_fun(entityBookOrder.getBorder_price_down_fun());
            BookOrderHandle90SharePrice.matchSharePrice(entityOrderUpdate);
        }
        SvcBookOrder.getApp().saveOrUpdatePassEqualField(entityBookOrder, entityOrderUpdate);

        if (vo.getAudit_status() == 1) { //通过处理
            //返还库存
            log.debug("库存释放");
            VaBookSpuDayUpdateStock stockUpdate = VaBookSpuDayUpdateStock.of(entityBookOrder.getBorder_spu_id()
                    , entityBookOrder.getBorder_date_begin()
                    , entityBookOrder.getBorder_date_end_final()
                    , -1);
            SvcBookSpu.getBean().dayBatchUpdateStock(stockUpdate);
        }

        return EnumSys.OK.getR();
    }

    /**
     * 退款
     *
     * @param refundId
     */
    public XReturn refund(String refundId) {
        BookRefundOrder entityOrderRefund = BookRefundOrderService.getBean().first(refundId);
        XValidator.isNull(entityOrderRefund, "退款订单ID异常");
        if (entityOrderRefund.getBrefo_refund_handle() == 1) {
            return XReturn.getR(20500, "已退款");
        }

        SysOrder entityOrder = SysOrderService.getBean().getEntity(entityOrderRefund.getBrefo_order_id());
        XValidator.isNull(entityOrder, "支付订单号异常");

        //接口退款
        WxApiPay pay = WxApiPay.getIns(XHttpContext.getXConfig());
        Long orderFee = entityOrder.getOrder_pay_price().multiply(BigDecimal.valueOf(100)).longValue();
        Long refundFee = entityOrderRefund.getBrefo_refund_price().multiply(BigDecimal.valueOf(100)).longValue();
        XReturn r = pay.orderRefund(entityOrder.getOrder_id()
                , entityOrderRefund.getBrefo_refund_id()
                , orderFee
                , refundFee);
        BookRefundOrder entityOrderRefundUpdate = new BookRefundOrder();
        if (r.ok()) {
            entityOrderRefundUpdate.setBrefo_refund_handle(1);
        } else {
            entityOrderRefundUpdate.setBrefo_refund_handle(10);
        }
        entityOrderRefundUpdate.setBrefo_refund_errmsg(r.getErrMsg());
        entityOrderRefundUpdate.setBrefo_refund_handle_date(XDate.now());
        BookRefundOrderService.getBean().saveOrUpdatePassEqualField(entityOrderRefund, entityOrderRefundUpdate);

        return r;
    }

    public void updateStatus(BookOrder entityOrder, Integer status) {
        updateStatus(entityOrder, VaBookOrderUpdateStatus.of(status, ""));
    }

    /**
     * 更新订单状态
     *
     * @param entityOrder
     */
    public void updateStatus(BookOrder entityOrder, VaBookOrderUpdateStatus vo) {
        XValidator.checkTransaction();

        if (vo.getStatus() <= entityOrder.getBorder_status()) {
            XValidator.err(20256, "操作异常，订单状态不能回退");
        }

        BookOrder entityOrderUpdate = new BookOrder();
        entityOrderUpdate.setBorder_id(entityOrder.getKey());
        entityOrderUpdate.setBorder_status(vo.getStatus());

        switch (vo.getStatus()) {
            case 3: //关闭订单
                entityOrderUpdate.setBorder_enable(0);
                break;
            case 5: //已支付
                break;
            case 7: //确认拒绝
            case 10: //待入住
                entityOrderUpdate.setBorder_confirm(1);
                if (vo.getStatus() == 7) {
                    entityOrderUpdate.setBorder_confirm(10); //标记拒绝
                }
                entityOrderUpdate.setBorder_confirm_date(XDate.now());
                entityOrderUpdate.setBorder_confirm_desc(vo.getDesc());
                break;
            case 15: //入住中
                entityOrderUpdate.setBorder_checkin(1);
                entityOrderUpdate.setBorder_checkin_date(XDate.now());
                break;
            case 20: //已退房
                entityOrderUpdate.setBorder_checkout(1);
                entityOrderUpdate.setBorder_checkout_date(XDate.now());
                break;
        }

        getApp().saveOrUpdatePassEqualField(entityOrder, entityOrderUpdate);
    }

    /**
     * 订单分账
     */
    public XReturn sharePrice(BookOrder entity) {
        if (entity.getBorder_status() != 20) {
            return XReturn.getR(20507, "订单未退房");
        }
        if (entity.getBorder_share_sales_price().compareTo(BigDecimal.ZERO) > 0) { //主播分帐
            String key = "sale|" + entity.getKey();
            VoSaleAddRecord data = VoSaleAddRecord.of(key
                    , entity.getBorder_share_sales_uid()
                    , entity.getBorder_share_sales_price()
                    , "订单完成", "app_book"
                    , entity.getKey());
            SvcSysSale.getBean().addRecord(data);
        }

        if (entity.getBorder_share_store_price().compareTo(BigDecimal.ZERO) > 0) { //商家分账
            String key = "store|" + entity.getKey();
            VoSaleAddRecord data = VoSaleAddRecord.of(key
                    , entity.getBorder_store_id()
                    , entity.getBorder_share_store_price()
                    , "订单完成", "app_book"
                    , entity.getKey());
            SvcSysSale.getBean().addRecord(data);
        }

        if (entity.getBorder_give_fun() > 0) {
            log.info("邀请好友奖励积分");
            SvcFun.getBean().add(entity.getKey()
                    , entity.getBorder_give_uid()
                    , entity.getKey()
                    , "app_book"
                    , null
                    , "邀请好友奖励积分");
        }

        return EnumSys.OK.getR();
    }
}
