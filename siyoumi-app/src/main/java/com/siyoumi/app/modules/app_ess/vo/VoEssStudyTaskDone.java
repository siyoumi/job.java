package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class VoEssStudyTaskDone {
    @HasAnyText(message = "miss record_id")
    String record_id;
}
