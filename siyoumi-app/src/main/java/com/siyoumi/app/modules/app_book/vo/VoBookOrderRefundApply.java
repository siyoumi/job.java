package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//退款申请
@Data
public class VoBookOrderRefundApply {
    @HasAnyText(message = "缺少订单号")
    private String order_id;
}
