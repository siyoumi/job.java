package com.siyoumi.app.modules.mall2.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

//商品
@Data
public class VaM2Spu {
    private String mspu_x_id;
    private String mspu_app_id;
    private String mspu_acc_id;
    @HasAnyText
    private String mspu_group_id;
    private Integer mspu_type;
    @HasAnyText
    @Size(max = 50)
    private String mspu_name;
    private String mspu_img;
    @Size(max = 500)
    private String mspu_img_banner;
    @Size(max = 500)
    private String mspu_img_info;
    @Max(1)
    private BigDecimal mspu_price;
    private Integer mspu_order;

    private List<SpuAttrVo> attr;
}
