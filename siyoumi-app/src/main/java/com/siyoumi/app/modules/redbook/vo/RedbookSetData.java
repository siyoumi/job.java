package com.siyoumi.app.modules.redbook.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//评论
@Data
public class RedbookSetData {
    @HasAnyText(message = "缺少来源ID")
    String rdata_id_src;
    @HasAnyText(message = "缺少类型")
    @EqualsValues(vals = {"0", "1", "2"}, message = "类型异常")
    Integer rdata_type; //0：点赞；1：收藏；
    String rdata_uid;

    String rdata_quan_id;

    @EqualsValues(vals = {"0", "1"})
    @HasAnyText(message = "miss ok")
    private Integer action; //【0】取消收藏；【1】收藏
}
