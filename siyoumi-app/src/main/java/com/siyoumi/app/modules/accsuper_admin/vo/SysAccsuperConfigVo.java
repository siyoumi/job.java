package com.siyoumi.app.modules.accsuper_admin.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class SysAccsuperConfigVo {
    @HasAnyText
    private String aconfig_type;
    @HasAnyText
    @Size(max = 50)
    private String acc_name;
    @HasAnyText
    @Size(max = 50)
    private String acc_uid;
    @HasAnyText
    @Size(max = 50)
    private String acc_pwd;
}
