package com.siyoumi.app.modules.app_fks.entity;

import com.siyoumi.component.XEnumBase;


public class EnumFksPositionLevel
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "高层");
        put(5, "中层");
        //put(2, "2级");
        put(10, "普通");
        put(99, "其他");
    }
}
