package com.siyoumi.app.modules.fun_mall.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.FunMallGroup;
import com.siyoumi.app.entity.FunMallPrize;
import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.app.entity.SysStock;
import com.siyoumi.app.modules.fun_mall.service.SvcFunMallPrize;
import com.siyoumi.app.modules.prize.entity.EnumPrizeType;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSet;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/fun_mall/fun_mall__prize__list")
public class fun_mall__prize__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("商品列表");

        SvcFunMallPrize svcPrize = SvcFunMallPrize.getBean();

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "pset_name",
                "pset_type",
                "pset_pic",
                "fprize_id",
                "fprize_group_id",
                "fprize_get_total",
                "fprize_fun",
                "fprize_begin_date",
                "fprize_end_date",
                "fprize_enable",
                "fprize_order",
                "stock_count_left",
                "stock_count_use",
                "fgroup_name",
        };
        JoinWrapperPlus<FunMallPrize> query = svcPrize.listQuery(inputData);
        query.join(SysStock.table(), "stock_id_src", FunMallPrize.tableKey());
        query.leftJoin(FunMallGroup.table(), "fprize_group_id", FunMallGroup.tableKey());
        query.select(select);

        IPage<FunMallPrize> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcFunMallPrize.getApp().mapper().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        for (Map<String, Object> data : list) {
            FunMallPrize entity = SvcFunMallPrize.getApp().loadEntity(data);
            data.put("id", entity.getKey());
            //过期
            data.put("is_expire", entity.expire());

            SysPrizeSet entityPrizeSet = SvcSysPrizeSet.getApp().loadEntity(data);
            String prizeType = IEnum.getEnmuVal(EnumPrizeType.class, entityPrizeSet.getPset_type());
            data.put("type", prizeType);
        }

        getR().setData("list", list);
        getR().setData("count", count);
        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return SvcFunMallPrize.getBean().delete(List.of(ids));
    }
}
