package com.siyoumi.app.modules.book.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

@Data
public class VaBookSku {
    String bsku_acc_id;
    @HasAnyText
    String bsku_book_id;
    @HasAnyText
    String time_begin;
    @HasAnyText
    String time_end;
    String bsku_name;
    Integer bsku_hide;
    Integer bsku_order;
    Long bsku_before_time;
}
