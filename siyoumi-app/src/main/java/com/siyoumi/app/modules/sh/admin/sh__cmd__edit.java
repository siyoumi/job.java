package com.siyoumi.app.modules.sh.admin;

import com.siyoumi.app.entity.ShCmd;
import com.siyoumi.app.modules.sh.entity.EnumShCmdRun;
import com.siyoumi.app.modules.sh.service.SvcShCmd;
import com.siyoumi.app.modules.sh.vo.VaShCmd;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/xadmin/sh/sh__cmd__edit")
public class sh__cmd__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("脚本-编辑");

        Map<String, Object> data = new HashMap<>();
        data.put("scmd_run_date", XDate.now());
        if (isAdminEdit()) {
            ShCmd entity = SvcShCmd.getApp().getEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //运行状态
        LinkedHashMap<String, String> run = IEnum.toMap(EnumShCmdRun.class);
        setPageInfo("run", run);


        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaShCmd vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);

        List<String> ignoreField = new ArrayList<>();
        if (isAdminEdit()) {

        } else {
        }

        InputData inputData = InputData.fromRequest();
        
        return SvcShCmd.getApp().saveEntity(inputData, vo, true, ignoreField);
    }
}
