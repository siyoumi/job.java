package com.siyoumi.app.modules.app_ess.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.EssUser;
import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.modules.app_ess.entity.EnumEssUserType;
import com.siyoumi.app.modules.app_ess.service.SvcEssClass;
import com.siyoumi.app.modules.app_ess.service.SvcEssUser;
import com.siyoumi.app.modules.app_ess.vo.VoEssCancelClass;
import com.siyoumi.app.modules.user.entity.EnumUserEnable;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.app.modules.user.vo.SysUserAudit;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/app_ess/app_ess__user__list")
public class app_ess__user__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("帐号列表");

        InputData inputData = InputData.fromRequest();

        String[] select = {
                "user_id",
                "user_name",
                "user_username",
                "user_fun",
                "user_enable",
                "user_enable_date",
                "euser_set_class",
                "euser_type",
                "euser_sex",
                "euser_speciality",
                "euser_create_date",
                "euser_school_date",
        };
        JoinWrapperPlus<EssUser> query = SvcEssUser.getBean().listQuery(inputData);
        query.select(select);
        query.orderByDesc("euser_id");

        IPage<EssUser> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = SvcEssUser.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        EnumUserEnable enumUserEnable = XEnumBase.of(EnumUserEnable.class);
        EnumEssUserType enumType = EnumEssUserType.of(EnumEssUserType.class);
        for (Map<String, Object> item : list) {
            SysUser entitySysUser = XBean.fromMap(item, SysUser.class);
            item.put("id", entitySysUser.getKey());
            //
            EssUser entity = XBean.fromMap(item, EssUser.class);
            //类型
            item.put("type", enumType.get(entity.getEuser_type()));
            //状态
            item.put("enable", enumUserEnable.get(entitySysUser.getUser_enable()));
            item.put("school_date", XDate.toDateString(entity.getEuser_school_date()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        //班级列表
        setPageInfo("list_class", SvcEssClass.getBean().getList());
        setPageInfo("enum_type", enumType); //类型

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        return SvcEssUser.getBean().delete(getIds());
    }


    //审核
    @PostMapping("/audit")
    public XReturn audit(@Validated SysUserAudit vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        return SvcSysUser.getBean().audit(vo);
    }

    //取消班级
    @PostMapping("/cancel_class")
    public XReturn cancelClass(@Validated VoEssCancelClass vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        return SvcEssClass.getBean().studentCancelClass(vo);
    }
}
