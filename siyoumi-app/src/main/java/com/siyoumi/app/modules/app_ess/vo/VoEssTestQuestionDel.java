package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//评测题目
@Data
public class VoEssTestQuestionDel {
    @HasAnyText
    String test_id;
    String question_id;
}
