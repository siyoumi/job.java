package com.siyoumi.app.modules.prize.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysPrize;
import com.siyoumi.app.modules.prize.entity.EnumPrizeType;
import com.siyoumi.app.modules.prize.service.SvcSysPrize;
import com.siyoumi.app.modules.prize.vo.PrizeDeliverVo;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysApp;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.IEnum;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/prize/prize__item__list")
public class prize__item__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("奖品-列表");

        InputData inputData = InputData.fromRequest();

        List<String> select = new ArrayList<>();
        select.add("prize_id");
        select.add("prize_app_id");
        select.add("app_name");
        select.add("wxuser_openid");
        select.add("wxuser_nickname");
        select.add("prize_type");
        select.add("prize_name");
        select.add("prize_pic");
        select.add("prize_id_src");
        select.add("prize_begin_date");
        select.add("prize_end_date");
        select.add("prize_use_type");
        select.add("prize_use");
        select.add("prize_use_date");
        select.add("prize_create_date");
        select.add("prize_desc");
        select.add("prize_user_name");
        select.add("prize_user_phone");
        select.add("prize_user_address");
        select.add("prize_del");
        select.add("prize_deliver_no");
        select.add("prize_deliver_date");

        JoinWrapperPlus<SysPrize> query = SvcSysPrize.getBean().listQuery(inputData);
        query.leftJoin(SysApp.table(), "prize_app_id", "app_id");
        query.select(select.toArray(new String[select.size()]));

        IPage<SysPrize> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize(), !isAdminExport());
        //list
        IPage<Map<String, Object>> pageData = SvcSysPrize.getApp().getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();

        List<Map<String, Object>> listData = list.stream().map(data ->
        {
            SysPrize entitySysPrize = XBean.fromMap(data, SysPrize.class);
            data.put("id", entitySysPrize.getKey());

            //类型
            String prizeType = IEnum.getEnmuVal(EnumPrizeType.class, entitySysPrize.getPrize_type());
            data.put("type", prizeType);
            //过期
            data.put("is_expire", entitySysPrize.expire());
            return data;
        }).collect(Collectors.toList());

        if (!isAdminExport()) {
            getR().setData("list", listData);
            getR().setData("count", count);
        } else {
            LinkedMap<String, String> tableHead = new LinkedMap<>();
            tableHead.put("prize_id", "编号");
            tableHead.put("app_name", "来源");
            tableHead.put("wxuser_openid", "openid");
            tableHead.put("wxuser_nickname", "微信名");
            tableHead.put("type", "类型");
            tableHead.put("prize_name", "名称");
            tableHead.put("prize_begin_date", "有效期-开始");
            tableHead.put("prize_end_date", "有效期-结束");
            tableHead.put("prize_use", "使用状态");
            tableHead.put("prize_use_date", "使用状态-时间");
            tableHead.put("create_date", "领取时间");
            tableHead.put("prize_deliver_no", "物流单号");
            tableHead.put("prize_deliver_date", "发货时间");
            tableHead.put("prize_user_name", "用户姓名");
            tableHead.put("prize_user_phone", "用户手机");
            tableHead.put("prize_user_address", "用户地址");

            List<List<String>> listDataEx = adminExprotData(tableHead, list, pageData.getCurrent() == 1);
            getR().setData("list", listDataEx);
        }

        return getR();
    }

    @GetMapping("/export")
    public XReturn export() {
        setAdminExport();
        return index();
    }

    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        SvcSysPrize app = SvcSysPrize.getBean();
        return app.delete(getIds());
    }


    @Transactional
    @GetMapping("/deliver")
    public XReturn deliver(@Validated() PrizeDeliverVo vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true, true);

        SvcSysPrize app = SvcSysPrize.getBean();
        return app.deliver(vo);
    }
}

