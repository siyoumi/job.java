package com.siyoumi.app.modules.website_fjciecc.admin;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.modules.website_fjciecc.service.WebSiteFjcieccListBase;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.util.XReturn;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/website_fjciecc/website_fjciecc__recruit__list")
public class website_fjciecc__recruit__list
        extends WebSiteFjcieccListBase {
    @Override
    public String title() {
        return "招聘岗位";
    }

    @Override
    public String abcTable() {
        return "fjciecc_recruit";
    }

    //for处理
    @Override
    public void forHandle(List<Map<String, Object>> list) {
        SysAbcService svcAbc = SysAbcService.getBean();

        for (Map<String, Object> data : list) {
            SysAbc entityAbc = svcAbc.loadEntity(data);
            data.put("id", entityAbc.getKey());
        }
    }


    @GetMapping()
    public XReturn index() {
        return super.index();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del(String[] ids) {
        return super.del(ids);
    }
}
