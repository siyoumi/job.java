package com.siyoumi.app.modules.app_book.vo;

import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import java.util.List;

//套餐上下架
@Data
public class VoBookSetAudit {
    String store_id;

    @HasAnyText
    private List<String> ids;
    @HasAnyText
    @EqualsValues(vals = {"0", "1"})
    private Integer enable;
}
