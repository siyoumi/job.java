package com.siyoumi.app.modules.app_fks.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;
import java.awt.print.Printable;
import java.util.List;

//新建共享空间文件
@Data
public class FksFileAdd {
    private Integer ffile_folder = 0;
    @HasAnyText
    @Size(max = 50)
    private String ffile_name;
    private String ffile_folder_id;
    @Size(max = 500)
    private String ffile_path;
    private Long ffile_file_size;
    @Size(max = 500)
    private String ffile_file_url;
    @Size(max = 20)
    private String ffile_file_ext;
    //笔记
    private String ffile_note_id;
    private String rquan_url; //图片
    private String rquan_content; //内容
}
