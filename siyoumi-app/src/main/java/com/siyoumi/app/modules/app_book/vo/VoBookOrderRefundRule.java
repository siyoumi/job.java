package com.siyoumi.app.modules.app_book.vo;

import lombok.Data;

import java.math.BigDecimal;

//订单退款规则
@Data
public class VoBookOrderRefundRule {
    Integer day_min; //最小日期
    Integer day_max; //最大日期

    BigDecimal rate; //扣手续费率
}
