package com.siyoumi.app.modules.accsuper_admin.admin;

import com.siyoumi.app.modules.accsuper_admin.service.SvcSysAccspuerConfig;
import com.siyoumi.app.modules.accsuper_admin.vo.SysAccsuperConfigVo;
import com.siyoumi.component.XBean;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.util.*;
import com.siyoumi.component.http.InputData;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/xadmin/accsuper_admin/accsuper_admin__edit")
public class accsuper_admin__edit
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("新建超管帐号");

        Map<String, Object> data = new HashMap<>();
        data.put("aconfig_type", "wx");
        if (isAdminEdit()) {
        }
        getR().setData("data", data);

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() SysAccsuperConfigVo vo, BindingResult result) {
        log.debug("统一验证");
        XValidator.getResult(result);

        log.debug("自定义验证");
        SysAccsuperConfigService app = SysAccsuperConfigService.getBean();
        SysAccsuperConfig entityConfig = app.getEntity(vo.getAcc_uid());
        if (entityConfig != null) {
            result.addError(XValidator.getErr("acc_uid", "帐号已存在"));
        }
        XValidator.getResult(result);
        
        SvcSysAccspuerConfig.getBean().addAccsuper(vo, false);

        return getR();
    }
}
