package com.siyoumi.app.modules.app_fks.admin;

import com.siyoumi.app.entity.FksCity;
import com.siyoumi.app.entity.FksFile;
import com.siyoumi.app.modules.app_fks.entity.EnumFksFileDataType;
import com.siyoumi.app.modules.app_fks.service.SvcFksCtiy;
import com.siyoumi.app.modules.app_fks.service.SvcFksFile;
import com.siyoumi.app.modules.app_fks.vo.VaFksCity;
import com.siyoumi.app.modules.app_fks.vo.VaFksDataFile;
import com.siyoumi.app.sys.service.CommonApiServcie;
import com.siyoumi.app.sys.service.FileHandle;
import com.siyoumi.app.sys.service.file.entity.FileUploadData;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.component.http.InputData;
import com.siyoumi.config.SysConfig;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/app_fks/app_fks__data_file__edit")
public class app_fks__data_file__edit
        extends AdminApiController {

    @GetMapping()
    public XReturn index() {
        String folder = input("folder", "0");
        if ("0".equals(folder)) {
            setPageTitle("添加文件");
        } else {
            setPageTitle("添加目录");
        }


        Map<String, Object> data = new HashMap<>();
        data.put("ffile_folder_id", input("folder_id", ""));
        data.put("ffile_type_id", input("type_id", ""));
        data.put("ffile_folder", folder);
        data.put("ffile_file_size", 0);
        data.put("ffile_order", 0);
        if (isAdminEdit()) {
            FksFile entity = SvcFksFile.getApp().loadEntity(getID());

            HashMap<String, Object> dataAppend = new LinkedHashMap<>();
            dataAppend.put("id", entity.getKey());
            //
            //合并
            data = entity.toMap();
            data.putAll(dataAppend);
        }
        getR().setData("data", data);

        //数据分类
        EnumFksFileDataType enumDataType = XEnumBase.of(EnumFksFileDataType.class);
        setPageInfo("enum_data_type", enumDataType);
        //城市列表
        setPageInfo("list_city", SvcFksCtiy.getBean().getList());
        setPageInfo("file_accept", SvcFksFile.fileAccept().stream().collect(Collectors.joining(","))); //允许上传的文件格式

        return getR();
    }


    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public XReturn save(@Validated() VaFksDataFile vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result);
        //
        vo.setFfile_acc_id(getAccId());
        vo.setFfile_type(2);
        //
        InputData inputData = InputData.fromRequest();
        return SvcFksFile.getBean().edit(inputData, vo);
    }


    @PostMapping({"/upload_file"})
    public XReturn uploadFile(@RequestParam("file0") MultipartFile file0) {
        String type = input("type"); //类型
        String typeId = input("type_id"); //城市ID
        if (XStr.isNullOrEmpty(type)) {
            return EnumSys.MISS_VAL.getR("miss type");
        }
        if ("2".equals(type)) {
            if (XStr.isNullOrEmpty(typeId)) {
                return EnumSys.MISS_VAL.getR("miss type_id");
            }
        }
        if (XStr.isNullOrEmpty(typeId)) {
            typeId = "0";
        }

        List<String> ex = SvcFksFile.fileAccept().stream().map(item -> item.substring(1, item.length())).collect(Collectors.toList());

        String saveFileDir = XStr.format("app/data_{0}_{1}", type, typeId);
        FileUploadData data = FileUploadData.of(file0, ex, 20, saveFileDir);
        return FileHandle.of("aliyun").uploadFile(data);
        //return CommonApiServcie.getBean().uploadFile(file0, ex, SysConfig.getIns().getUploadFileSizeMax(), null);
    }
}
