package com.siyoumi.app.modules.sh.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.ShUser;
import com.siyoumi.app.entity.base.ShUserBase;
import com.siyoumi.app.modules.sh.service.SvcSh;
import com.siyoumi.app.service.ShUserService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xadmin/sh/sh__user__list")
public class sh__user__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("脚本用户-列表");

        String compKw = input("compKw");

        ShUserService app = ShUserService.getBean();
        JoinWrapperPlus<ShUser> query = app.getQuery();
        if (XStr.hasAnyText(compKw)) { //名称
            query.like(ShUser.F.suser_name, compKw);
        }

        InputData inputData = InputData.fromRequest();
        IPage<ShUser> page = new Page<>(inputData.getPageIndex(), inputData.getPageSize());
        //list
        IPage<Map<String, Object>> pageData = app.getMaps(page, query);
        List<Map<String, Object>> list = pageData.getRecords();
        long count = pageData.getTotal();
        for (Map<String, Object> item : list) {
            ShUser entity = ShUserService.getBean().loadEntity(item);

            item.put("id", entity.getKey());
            item.put("day_count", entity.leftExpireHours());
            item.put("h_count", entity.leftExpireHours(ShUser.expireHourMax()));

            String phone = "";
            String pwd = "";
            if (XStr.hasAnyText(entity.getSuser_phone())) {
                String[] phoneArr = entity.getSuser_phone().split(",");
                phone = phoneArr[0];
                if (phoneArr.length > 1) {
                    pwd = phoneArr[1];
                }
            }
            item.put("phone", phone.trim());
            item.put("p", pwd.trim());
            item.put("suser_share_code", XStr.urlDec(entity.getSuser_share_code()));
        }

        getR().setData("list", list);
        getR().setData("count", count);

        return getR();
    }


    //删除
    @Transactional
    @PostMapping("/del")
    public XReturn del() {
        ShUserService app = ShUserService.getBean();
        //
        QueryWrapper<ShUser> query = app.q();
        query.in(ShUser.tableKey(), getIds())
                .eq(ShUserBase.F.suser_x_id, XHttpContext.getX());
        app.mapper().delete(query);

        return getR();
    }

    //
    @PostMapping("/enable")
    public XReturn enable() {
        String enable = input("enable", "0");

        ShUserService app = ShUserService.getBean();
        //
        app.update()
                .set(ShUserBase.F.suser_enable, enable)
                .in(ShUser.tableKey(), getIds())
                .eq(ShUserBase.F.suser_x_id, XHttpContext.getX())
                .update();

        return getR();
    }

    @GetMapping("/export_config")
    public void exportConfig() {
        String jdCookies = SvcSh.getBean().shExportConfig();
        StringBuilder sb = new StringBuilder();
        sb.append("JD_COOKIE=" + jdCookies);

        String fileName = "_docker_config";

        exportTxt(fileName, sb.toString());
    }
}
