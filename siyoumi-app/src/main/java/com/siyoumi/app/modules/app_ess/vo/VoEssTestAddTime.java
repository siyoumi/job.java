package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;


//添加评测时长
@Data
public class VoEssTestAddTime {
    @HasAnyText(message = "缺少评测ID")
    String test_id;

    @HasAnyText(message = "缺少考试时长")
    Integer test_minute; //考试时长
    @HasAnyText(message = "缺少结束时长")
    Integer add_date_end_minute; //结束时长
}
