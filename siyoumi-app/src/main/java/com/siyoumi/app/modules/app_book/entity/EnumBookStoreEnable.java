package com.siyoumi.app.modules.app_book.entity;

import com.siyoumi.component.XEnumBase;

//订单状态
public class EnumBookStoreEnable
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        //【0】失效；【1】生效；
        put(0, "失效");
        put(1, "生效");
    }
}
