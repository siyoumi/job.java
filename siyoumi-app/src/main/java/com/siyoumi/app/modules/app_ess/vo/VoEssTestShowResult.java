package com.siyoumi.app.modules.app_ess.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;


//添加评测时长
@Data
public class VoEssTestShowResult {
    @HasAnyText(message = "缺少评测ID")
    String test_id;
}
