package com.siyoumi.app.modules.sh.task.jm;

import com.siyoumi.component.api.jj.JJ;
import com.siyoumi.component.api.jj.robot.RobotErr;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


//劳卡任务
@RestController
public class JmTaskRoco
        extends TaskBase {
    @Override
    public void exec() {
        XLog.info(this.getClass(), "BEGIN:" + XDate.toDateTimeString());

        Map<String, String> arrTask = getArrTask();

        StringBuilder sendMsg = new StringBuilder();
        sendMsg.append("**roco**\n\n");
        for (Map.Entry<String, String> task : arrTask.entrySet()) {
            String taskName = task.getKey();
            String returnStr = "";
            switch (taskName) {
                case "摇钱树":
                    returnStr = raoHandle(task);
                    break;
                default:
                    returnStr = commonHandle(task);
                    break;
            }

            sendMsg.append(returnStr);
        }

        //发jj
        JJ jj = JJ.getIns(RobotErr.class);
        jj.send(sendMsg.toString(), "", true);

        XLog.info(this.getClass(), "END:" + XDate.toDateTimeString());
    }

    //摇钱树
    public String raoHandle(Map.Entry<String, String> task) {
        String apiUrlLunch = "https://s.rocochina.com/z_app/star_act/web/zz_api__api_x__user_launch?" +
                "site_id=app.roco&act_id={sact_id}&{wx_from}";
        apiUrlLunch = apiUrlLunch.replace("{sact_id}", task.getValue());

        String apiUrlChou = "https://s.rocochina.com/z_app/star_act/web/zz_api__api_x__user_chou?" +
                "site_id=app.roco&master_id={master_id}&{wx_from}";

        StringBuilder sendMsg = new StringBuilder();
        String taskName = task.getKey();
        sendMsg.append("**" + taskName + "** \n\n");

        Map<String, String> arrWxFrom = getArrWxFrom();
        for (Map.Entry<String, String> entry : arrWxFrom.entrySet()) {
            String userName = entry.getKey();
            //发起
            String apiUrl = apiUrlLunch
                    .replace("{wx_from}", entry.getValue());
            XReturn r = requestApi(apiUrl);
            sendMsg.append(XStr.concat(userName, "-发起:", r.getErrMsg(), "\n\n"));
            if (r.ok()) {
                Map data = (Map) r.getData("data");
                String samasterId = (String) data.get("samaster_id");

                apiUrl = apiUrlChou
                        .replace("{wx_from}", entry.getValue())
                        .replace("{master_id}", samasterId);
                r = requestApi(apiUrl);
                sendMsg.append(XStr.concat(userName, "-抽奖:", r.getErrMsg(), "\n\n"));
            }
        }

        return sendMsg.toString();
    }

    @Override
    public Map<String, String> getArrTask() {
        Map<String, String> arr = new HashMap<>();
        arr.put("摇钱树", "sact-630ed9ed4577e-32a4-ac89-ebf711369fa8");
        return arr;
    }

    @Override
    public Map<String, String> getArrWxFrom() {
        Map<String, String> arr = new HashMap<>();
        arr.put("佬大", "wx_from=ovqzw5e_ocdbaDG4nzmaeK5frsBo&wx_from_enc=OGE4OWJhOYjNaeGVuYzFaVjl2WTJSaVlVUkhORzU2YldGbFN6Vm1jbk5DYnc9PQ==");
        arr.put("夫人", "wx_from=ovqzw5drQFcnmQKdGZI3boWr84v8&wx_from_enc" +
                "=MmFlYjg0OYjNaeGVuYzFaSEpSUm1OdWJWRkxaRWRhU1ROaWIxZHlPRFIyT0E9PQ==");
        arr.put("空仔", "wx_from=ovqzw5X0HvxytZ2IryTGdWO2zjvM&wx_from_enc" +
                "=NmY2NjZhYYjNaeGVuYzFXREJJZG5oNWRGb3lTWEo1VkVka1YwOHllbXAyVFE9PQ==");

        return arr;
    }
}
