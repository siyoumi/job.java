package com.siyoumi.app.modules.sys_log.admin;

import com.siyoumi.component.XRedis;
import com.siyoumi.config.SysConfig;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import org.redisson.api.RMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/xadmin/sys_log/sys_log__sys_job__list")
public class sys_log__sys_job__list
        extends AdminApiController {
    @GetMapping()
    public XReturn index() {
        setPageTitle("系统任务");

        String key = SysConfig.getIns().getEnv() + "|task";
        if (SysConfig.getIns().isDev()) {
            key = "sh|task";
        }

        RMap<String, String> list = XRedis.getBean().getList(key);

        List<Map<String, Object>> listData = new LinkedList<>();
        list.forEach((k, v) -> {
            Map<String, Object> data = new HashMap<>();
            data.put("job_name", k);
            data.put("last_time", v);

            LocalDateTime lastTime = XDate.parse(v);
            Duration be = XDate.between(lastTime, XDate.now());
            long stopMinutes = be.toMinutes();
            data.put("stop_m", stopMinutes);

            listData.add(data);
        });

        //超1小时，任务数
        long jobCountStop1h = listData.stream().filter(item -> {
            return (long) item.get("stop_m") > 60;
        }).count();

        getR().setData("list", listData);
        getR().setData("count", listData.size());

        //任务总数
        setPageInfo("job_count", list.size());
        setPageInfo("job_count_stop_1h", jobCountStop1h);

        return getR();
    }
}
