package com.siyoumi.app.modules.app_fks.service;

import com.siyoumi.app.entity.FksDepartment;
import com.siyoumi.app.modules.app_fks.vo.VaFksDepartment;
import com.siyoumi.app.modules.app_fks.vo.VaFksMsg;
import com.siyoumi.app.service.FksDepartmentService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//部门
@Slf4j
@Service
public class SvcFksDepartment
        implements IWebService {
    static public SvcFksDepartment getBean() {
        return XSpringContext.getBean(SvcFksDepartment.class);
    }

    static public FksDepartmentService getApp() {
        return FksDepartmentService.getBean();
    }


    public List<Map<String, Object>> getList() {
        JoinWrapperPlus<FksDepartment> query = listQuery();
        query.select("fdep_id", "fdep_name");
        return getApp().getMaps(query);
    }

    public JoinWrapperPlus<FksDepartment> listQuery() {
        return listQuery(InputData.getIns());
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<FksDepartment> listQuery(InputData inputData) {
        String name = inputData.input("name");

        JoinWrapperPlus<FksDepartment> query = getApp().join();
        query.eq("fdep_x_id", XHttpContext.getX())
                .eq("fdep_del", 0);
        //排序
        query.orderByAsc("fdep_order")
                .orderByDesc("fdep_id");
        if (XStr.hasAnyText(name)) { //名称
            query.like("fdep_name", name);
        }

        return query;
    }

    public XReturn edit(InputData inputData, VaFksDepartment vo) {
        List<String> ignoreField = new ArrayList<>();
        if (inputData.isAdminEdit()) {
            ignoreField.add("fdep_acc_id");
        }

        return XApp.getTransaction().execute(status -> {
            XReturn r = getApp().saveEntity(inputData, vo, true, ignoreField);
            return r;
        });
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }
}
