package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookStoreGroup;
import com.siyoumi.app.mapper.BookStoreGroupMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_store_group
@Service
public class BookStoreGroupService
        extends ServiceImplBase<BookStoreGroupMapper, BookStoreGroup>{
    static public BookStoreGroupService getBean(){
        return XSpringContext.getBean(BookStoreGroupService.class);
    }
}
