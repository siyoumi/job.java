package com.siyoumi.app.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.siyoumi.app.entity.SysSet;
import com.siyoumi.app.mapper.SysSetMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;

import java.util.List;

//t_s_set
@Service
public class SysSetService
        extends ServiceImplBase<SysSetMapper, SysSet> {
    static public SysSetService getBean() {
        return XSpringContext.getBean(SysSetService.class);
    }


    public List<SysSet> getListWebSite(String x) {
        LambdaQueryWrapper<SysSet> query = queryByApp("website");
        query.select(SysSet::getSet_id, SysSet::getSet_name, SysSet::getSet_type);
        query.eq(SysSet::getSet_x_id, x)
                .orderByAsc(SysSet::getSet_order);

        return mapper().selectList(query);
    }


    LambdaQueryWrapper<SysSet> queryByApp(String appId) {
        LambdaQueryWrapper<SysSet> query = ql();
        return query.eq(SysSet::getSet_app_id, appId);
    }
}
