package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysMoneyOption;
import com.siyoumi.app.mapper.SysMoneyOptionMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_s_money_option
@Service
public class SysMoneyOptionService
        extends ServiceImplBase<SysMoneyOptionMapper, SysMoneyOption>{
    static public SysMoneyOptionService getBean(){
        return XSpringContext.getBean(SysMoneyOptionService.class);
    }
}
