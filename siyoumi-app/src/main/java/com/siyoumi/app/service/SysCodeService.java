package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysCode;
import com.siyoumi.app.mapper.SysCodeMapper;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;


//t_s_code
@Service
public class SysCodeService
        extends ServiceImplBase<SysCodeMapper, SysCode> {
    static public SysCodeService getBean() {
        return XSpringContext.getBean(SysCodeService.class);
    }

    public JoinWrapperPlus<SysCode> listQuery(String appId) {
        JoinWrapperPlus<SysCode> query = join();
        query.eq("code_x_id", XHttpContext.getX());

        if (XStr.hasAnyText(appId)) {
            query.eq("code_app_id", appId);
        }

        return query;
    }
}
