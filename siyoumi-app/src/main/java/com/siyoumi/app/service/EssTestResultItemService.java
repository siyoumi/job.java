package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssTestResultItem;
import com.siyoumi.app.mapper.EssTestResultItemMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_test_result_item
@Service
public class EssTestResultItemService
        extends ServiceImplBase<EssTestResultItemMapper, EssTestResultItem>{
    static public EssTestResultItemService getBean(){
        return XSpringContext.getBean(EssTestResultItemService.class);
    }
}
