package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookRefundOrder;
import com.siyoumi.app.mapper.BookRefundOrderMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_refund_order
@Service
public class BookRefundOrderService
        extends ServiceImplBase<BookRefundOrderMapper, BookRefundOrder>{
    static public BookRefundOrderService getBean(){
        return XSpringContext.getBean(BookRefundOrderService.class);
    }
}
