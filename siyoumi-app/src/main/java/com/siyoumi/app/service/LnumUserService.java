package com.siyoumi.app.service;

import com.siyoumi.app.entity.LnumUser;
import com.siyoumi.app.mapper.LnumUserMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_lnum_user
@Service
public class LnumUserService
        extends ServiceImplBase<LnumUserMapper, LnumUser>{
    static public LnumUserService getBean(){
        return XSpringContext.getBean(LnumUserService.class);
    }
}
