package com.siyoumi.app.service;

import com.siyoumi.app.entity.IotDeviceLog;
import com.siyoumi.app.mapper.IotDeviceLogMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_iot_device_log
@Service
public class IotDeviceLogService
        extends ServiceImplBase<IotDeviceLogMapper, IotDeviceLog>{
    static public IotDeviceLogService getBean(){
        return XSpringContext.getBean(IotDeviceLogService.class);
    }
}
