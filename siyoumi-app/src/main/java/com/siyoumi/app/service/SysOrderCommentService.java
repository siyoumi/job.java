package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysOrderComment;
import com.siyoumi.app.mapper.SysOrderCommentMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_s_order_comment
@Service
public class SysOrderCommentService
        extends ServiceImplBase<SysOrderCommentMapper, SysOrderComment>{
    static public SysOrderCommentService getBean(){
        return XSpringContext.getBean(SysOrderCommentService.class);
    }
}
