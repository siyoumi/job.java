package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssStudy;
import com.siyoumi.app.mapper.EssStudyMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_study
@Service
public class EssStudyService
        extends ServiceImplBase<EssStudyMapper, EssStudy>{
    static public EssStudyService getBean(){
        return XSpringContext.getBean(EssStudyService.class);
    }
}
