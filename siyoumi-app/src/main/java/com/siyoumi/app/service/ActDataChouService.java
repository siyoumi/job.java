package com.siyoumi.app.service;

import com.siyoumi.app.entity.ActDataChou;
import com.siyoumi.app.mapper.ActDataChouMapper;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_act_data_chou
@Service
public class ActDataChouService
        extends ServiceImplBase<ActDataChouMapper, ActDataChou> {
    static public ActDataChouService getBean() {
        return XSpringContext.getBean(ActDataChouService.class);
    }

    public JoinWrapperPlus<ActDataChou> listQuery(String actId) {
        JoinWrapperPlus<ActDataChou> query = join();
        query.eq("adc_actset_id", actId)
                .orderByDesc("adc_id");
        return query;
    }

    public ActDataChou getEntity(String actId, String key) {
        JoinWrapperPlus<ActDataChou> query = listQuery(actId);
        query.eq("adc_uix", key);

        return first(query);
    }

    /**
     * 抽奖数量
     *
     * @param actId
     * @param openid
     * @param win
     */
    public Long chouCount(String actId, String openid, Boolean win) {
        JoinWrapperPlus<ActDataChou> query = listQuery(actId);
        query.eq("adc_openid", openid);

        if (win != null) {
            if (win) {
                query.isNotNull("adc_win_uix");
            } else {
                query.isNull("adc_win_uix");
            }
        }

        return count(query);
    }

    /**
     * 中过奖
     *
     * @param actId
     * @param openid
     */
    public Boolean win(String actId, String openid) {
        JoinWrapperPlus<ActDataChou> query = listQuery(actId);
        query.eq("adc_openid", openid)
                .isNotNull("adc_win_uix");
        return first(query) != null;
    }


    public ActDataChou getEntityByWinKey(String actId, String winKey) {
        JoinWrapperPlus<ActDataChou> query = listQuery(actId);
        query.eq("adc_win_uix", winKey);

        return first(query);
    }
}
