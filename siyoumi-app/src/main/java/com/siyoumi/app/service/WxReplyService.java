package com.siyoumi.app.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.WxReply;
import com.siyoumi.app.mapper.WxReplyMapper;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_wx_reply
@Service
public class WxReplyService
        extends ServiceImplBase<WxReplyMapper, WxReply> {
    static public WxReplyService getBean() {
        return XSpringContext.getBean(WxReplyService.class);
    }
    
    public WxReply getEntityByAppId(String appId) {
        QueryWrapper<WxReply> q = q();
        q.eq("reply_x_id", XHttpContext.getX())
                .eq("reply_app_id", appId);

        return getOne(q);
    }
}
