package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysBookSkuDay;
import com.siyoumi.app.mapper.SysBookSkuDayMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_sys_book_sku_day
@Service
public class SysBookSkuDayService
        extends ServiceImplBase<SysBookSkuDayMapper, SysBookSkuDay>{
    static public SysBookSkuDayService getBean(){
        return XSpringContext.getBean(SysBookSkuDayService.class);
    }
}
