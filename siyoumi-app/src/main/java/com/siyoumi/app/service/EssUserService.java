package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssUser;
import com.siyoumi.app.mapper.EssUserMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_user
@Service
public class EssUserService
        extends ServiceImplBase<EssUserMapper, EssUser>{
    static public EssUserService getBean(){
        return XSpringContext.getBean(EssUserService.class);
    }
}
