package com.siyoumi.app.service;

import com.siyoumi.app.entity.LnumSessionPrize;
import com.siyoumi.app.mapper.LnumSessionPrizeMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_lnum_session_prize
@Service
public class LnumSessionPrizeService
        extends ServiceImplBase<LnumSessionPrizeMapper, LnumSessionPrize>{
    static public LnumSessionPrizeService getBean(){
        return XSpringContext.getBean(LnumSessionPrizeService.class);
    }
}
