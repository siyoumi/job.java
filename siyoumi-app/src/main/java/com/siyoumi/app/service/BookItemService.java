package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookItem;
import com.siyoumi.app.mapper.BookItemMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_item
@Service
public class BookItemService
        extends ServiceImplBase<BookItemMapper, BookItem>{
    static public BookItemService getBean(){
        return XSpringContext.getBean(BookItemService.class);
    }
}
