package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssStudyTask;
import com.siyoumi.app.mapper.EssStudyTaskMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_study_task
@Service
public class EssStudyTaskService
        extends ServiceImplBase<EssStudyTaskMapper, EssStudyTask>{
    static public EssStudyTaskService getBean(){
        return XSpringContext.getBean(EssStudyTaskService.class);
    }
}
