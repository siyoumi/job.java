package com.siyoumi.app.service;

import com.siyoumi.app.entity.FksMsg;
import com.siyoumi.app.mapper.FksMsgMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fks_msg
@Service
public class FksMsgService
        extends ServiceImplBase<FksMsgMapper, FksMsg> {
    static public FksMsgService getBean() {
        return XSpringContext.getBean(FksMsgService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
