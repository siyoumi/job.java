package com.siyoumi.app.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.entity.WxUserLogin;
import com.siyoumi.app.mapper.WxUserLoginMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_wx_user_login
@Service
public class WxUserLoginService
        extends ServiceImplBase<WxUserLoginMapper, WxUserLogin>{
    static public WxUserLoginService getBean(){
        return XSpringContext.getBean(WxUserLoginService.class);
    }
}
