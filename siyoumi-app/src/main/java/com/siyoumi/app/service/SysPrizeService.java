package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysPrize;
import com.siyoumi.app.mapper.SysPrizeMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;

//t_s_prize
@Service
public class SysPrizeService
        extends ServiceImplBase<SysPrizeMapper, SysPrize> {
    static public SysPrizeService getBean() {
        return XSpringContext.getBean(SysPrizeService.class);
    }

    public XReturn delete(String id) {
        SysPrize update = new SysPrize();
        update.setPrize_id(id);
        update.setPrize_del(XDate.toS());
        updateById(update);

        return XReturn.getR(0);
    }
}
