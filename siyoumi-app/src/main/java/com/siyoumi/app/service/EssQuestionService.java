package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssQuestion;
import com.siyoumi.app.mapper.EssQuestionMapper;
import com.siyoumi.component.http.InputData;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;


//t_ess_question
@Service
public class EssQuestionService
        extends ServiceImplBase<EssQuestionMapper, EssQuestion>{
    static public EssQuestionService getBean(){
        return XSpringContext.getBean(EssQuestionService.class);
    }
}
