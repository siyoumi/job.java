package com.siyoumi.app.service;

import com.siyoumi.app.entity.FunAuctionLog;
import com.siyoumi.app.mapper.FunAuctionLogMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fun_auction_log
@Service
public class FunAuctionLogService
        extends ServiceImplBase<FunAuctionLogMapper, FunAuctionLog>{
    static public FunAuctionLogService getBean(){
        return XSpringContext.getBean(FunAuctionLogService.class);
    }
}
