package com.siyoumi.app.service;

import com.siyoumi.app.entity.WxSubscribemsgLog;
import com.siyoumi.app.mapper.WxSubscribemsgLogMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_wx_subscribemsg_log
@Service
public class WxSubscribemsgLogService
        extends ServiceImplBase<WxSubscribemsgLogMapper, WxSubscribemsgLog>{
    static public WxSubscribemsgLogService getBean(){
        return XSpringContext.getBean(WxSubscribemsgLogService.class);
    }
}
