package com.siyoumi.app.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.mapper.SysAbcMapper;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

//t_s_abc
@Service
public class SysAbcService
        extends ServiceImplBase<SysAbcMapper, SysAbc> {
    static public SysAbcService getBean() {
        return XSpringContext.getBean(SysAbcService.class);
    }

    public JoinWrapperPlus<SysAbc> listQuery(String appId, Boolean passOrder) {
        JoinWrapperPlus<SysAbc> query = join();
        query.eq("t_s_abc.abc_x_id", XHttpContext.getX())
                .eq("t_s_abc.abc_app_id", appId)
                .eq("t_s_abc.abc_del", 0);
        if (!passOrder) {
            //默认系统排序
            query.orderByAsc("t_s_abc.abc_order")
                    .orderByDesc("t_s_abc.abc_create_date");
        }

        return query;
    }

    public SysAbc getEntityByUix(String uix) {
        QueryWrapper<SysAbc> query = q();
        query.eq("abc_x_id", XHttpContext.getX())
                .eq("abc_del", 0)
                .eq("abc_uix", uix);
        return first(query);
    }

    /**
     * table
     */
    public SysAbc getEntityByTable(String id, String table) {
        QueryWrapper<SysAbc> query = q();
        query.eq("abc_x_id", XHttpContext.getX())
                .eq("abc_id", id)
                .eq("abc_del", 0)
                .eq("abc_table", table);
        return first(query);
    }


    public List<Map<String, Object>> getListKV(String appId, String table) {
        return getListKV(appId, table, "abc_id", "abc_name");
    }

    public List<Map<String, Object>> getListKV(String appId, String table, String... select) {
        JoinWrapperPlus<SysAbc> query = listQuery(appId, false);
        query.select(select);
        query.eq("abc_table", table);

        return getMaps(query);
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids, String table) {
        QueryWrapper<SysAbc> query = q();
        query.eq("abc_x_id", XHttpContext.getX());
        query.eq("abc_table", table);
        query.in("abc_id", ids);

        List<SysAbc> list = list(query);
        boolean result = removeBatchByIds(list);

        XReturn r = XReturn.getR(0);
        r.setData("result", result);

        return r;
    }

    /**
     * 逻辑删除
     *
     * @param ids
     * @param table
     */
    public XReturn deleteLogic(List<String> ids, String table) {
        UpdateWrapper<SysAbc> update = new UpdateWrapper<>();
        update.eq("abc_x_id", XHttpContext.getX())
                .eq("abc_table", table)
                .in("abc_id", ids)
                .eq("abc_del", 0);
        update.set("abc_del", XDate.toS());
        boolean result = update(update);

        XReturn r = XReturn.getR(0);
        r.setData("result", result);

        return r;
    }

    @Override
    public XReturn saveEntityBefore(InputData inputData, SysAbc entity) {
        //if (entity.getAbc_uix() == null) {
        //    entity.setAbc_uix(XApp.getStrID());
        //}

        if (entity.getAbc_acc_id() == null) {
            XReturn tokenData = XHttpContext.getTokenData();
            entity.setAbc_acc_id(tokenData.getData("acc_id"));
        }

        return super.saveEntityBefore(inputData, entity);
    }
}
