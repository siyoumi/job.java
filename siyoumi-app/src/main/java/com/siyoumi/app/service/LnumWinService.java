package com.siyoumi.app.service;

import com.siyoumi.app.entity.LnumWin;
import com.siyoumi.app.mapper.LnumWinMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_lnum_win
@Service
public class LnumWinService
        extends ServiceImplBase<LnumWinMapper, LnumWin>{
    static public LnumWinService getBean(){
        return XSpringContext.getBean(LnumWinService.class);
    }
}
