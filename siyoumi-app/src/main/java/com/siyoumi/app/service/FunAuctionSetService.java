package com.siyoumi.app.service;

import com.siyoumi.app.entity.FunAuctionSet;
import com.siyoumi.app.mapper.FunAuctionSetMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fun_auction_set
@Service
public class FunAuctionSetService
        extends ServiceImplBase<FunAuctionSetMapper, FunAuctionSet>{
    static public FunAuctionSetService getBean(){
        return XSpringContext.getBean(FunAuctionSetService.class);
    }
}
