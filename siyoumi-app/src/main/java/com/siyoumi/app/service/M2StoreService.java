package com.siyoumi.app.service;

import com.siyoumi.app.entity.M2Store;
import com.siyoumi.app.mapper.M2StoreMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_m2_store
@Service
public class M2StoreService
        extends ServiceImplBase<M2StoreMapper, M2Store> {
    @Override
    protected boolean softDelete() {
        return true;
    }

    static public M2StoreService getBean() {
        return XSpringContext.getBean(M2StoreService.class);
    }
}
