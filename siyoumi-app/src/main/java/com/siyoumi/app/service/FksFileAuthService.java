package com.siyoumi.app.service;

import com.siyoumi.app.entity.FksFileAuth;
import com.siyoumi.app.mapper.FksFileAuthMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fks_file_auth
@Service
public class FksFileAuthService
        extends ServiceImplBase<FksFileAuthMapper, FksFileAuth>{
    static public FksFileAuthService getBean(){
        return XSpringContext.getBean(FksFileAuthService.class);
    }
}
