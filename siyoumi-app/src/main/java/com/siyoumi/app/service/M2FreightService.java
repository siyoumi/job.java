package com.siyoumi.app.service;

import com.siyoumi.app.entity.M2Freight;
import com.siyoumi.app.mapper.M2FreightMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_m2_freight
@Service
public class M2FreightService
        extends ServiceImplBase<M2FreightMapper, M2Freight>{
    static public M2FreightService getBean(){
        return XSpringContext.getBean(M2FreightService.class);
    }
}
