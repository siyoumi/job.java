package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssTest;
import com.siyoumi.app.mapper.EssTestMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_test
@Service
public class EssTestService
        extends ServiceImplBase<EssTestMapper, EssTest> {
    static public EssTestService getBean() {
        return XSpringContext.getBean(EssTestService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
