package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysSheetRecord;
import com.siyoumi.app.mapper.SysSheetRecordMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_a_sheet_record
@Service
public class SysSheetRecordService
        extends ServiceImplBase<SysSheetRecordMapper, SysSheetRecord>{
    static public SysSheetRecordService getBean(){
        return XSpringContext.getBean(SysSheetRecordService.class);
    }
}
