package com.siyoumi.app.service;

import com.siyoumi.app.entity.LnumNum;
import com.siyoumi.app.mapper.LnumNumMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_lnum_num
@Service
public class LnumNumService
        extends ServiceImplBase<LnumNumMapper, LnumNum>{
    static public LnumNumService getBean(){
        return XSpringContext.getBean(LnumNumService.class);
    }
}
