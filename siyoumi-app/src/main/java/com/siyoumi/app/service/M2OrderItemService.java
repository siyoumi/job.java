package com.siyoumi.app.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.M2OrderItem;
import com.siyoumi.app.mapper.M2OrderItemMapper;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;

import java.util.List;


//t_m2_order_item
@Service
public class M2OrderItemService
        extends ServiceImplBase<M2OrderItemMapper, M2OrderItem> {
    static public M2OrderItemService getBean() {
        return XSpringContext.getBean(M2OrderItemService.class);
    }

    public List<M2OrderItem> getList(String orderId) {
        JoinWrapperPlus<M2OrderItem> query = join();
        query.eq("moitem_order_id", orderId);
        return get(query);
    }


    public JoinWrapperPlus<M2OrderItem> listQuery(String itemId, List<String> itemxIds) {
        JoinWrapperPlus<M2OrderItem> query = join();
        if (XStr.hasAnyText(itemId)) { //子订单ID
            query.eq("moitemx_item_id", itemId);
        }
        if (itemxIds != null && itemxIds.size() > 0) {
            query.in("moitemx_id", itemxIds);
        }
        query.orderByAsc("moitemx_refund");

        return query;
    }
}
