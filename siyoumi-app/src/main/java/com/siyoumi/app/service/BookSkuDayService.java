package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysBookSkuDay;
import com.siyoumi.app.mapper.BookSkuDayMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_sku_day
@Service
public class BookSkuDayService
        extends ServiceImplBase<BookSkuDayMapper, SysBookSkuDay> {
    static public BookSkuDayService getBean() {
        return XSpringContext.getBean(BookSkuDayService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
