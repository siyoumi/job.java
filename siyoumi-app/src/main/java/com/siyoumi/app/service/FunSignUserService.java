package com.siyoumi.app.service;

import com.siyoumi.app.entity.FunSignUser;
import com.siyoumi.app.mapper.FunSignUserMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fun_sign_user
@Service
public class FunSignUserService
        extends ServiceImplBase<FunSignUserMapper, FunSignUser>{
    static public FunSignUserService getBean(){
        return XSpringContext.getBean(FunSignUserService.class);
    }
}
