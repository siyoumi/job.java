package com.siyoumi.app.service;

import com.siyoumi.app.entity.M2Group;
import com.siyoumi.app.mapper.M2GroupMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.util.XReturn;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import org.springframework.stereotype.Service;


//t_m2_group
@Service
public class M2GroupService
        extends ServiceImplBase<M2GroupMapper, M2Group>{
    static public M2GroupService getBean(){
        return XSpringContext.getBean(M2GroupService.class);
    }

    @Override
    public XReturn saveEntityAfter(InputData inputData, M2Group entity) {


        return super.saveEntityAfter(inputData, entity);
    }
}
