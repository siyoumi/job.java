package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysItemData;
import com.siyoumi.app.mapper.SysItemDataMapper;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_sys_item_data
@Service
public class SysItemDataService
        extends ServiceImplBase<SysItemDataMapper, SysItemData> {
    static public SysItemDataService getBean() {
        return XSpringContext.getBean(SysItemDataService.class);
    }

    /**
     * 根据key获取data
     *
     * @param key
     */
    public SysItemData getEntityByKey(String itemId, String key) {
        return XHttpContext.getAndSetData(itemId + key, k -> {
            JoinWrapperPlus<SysItemData> query = join();
            query.eq("idata_key", key)
                    .eq("idata_item_id", itemId);
            return first(query);
        });
    }
}
