package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysBookRecord;
import com.siyoumi.app.mapper.SysBookRecordMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_sys_book_record
@Service
public class SysBookRecordService
        extends ServiceImplBase<SysBookRecordMapper, SysBookRecord>{
    static public SysBookRecordService getBean(){
        return XSpringContext.getBean(SysBookRecordService.class);
    }
}
