package com.siyoumi.app.service;

import com.siyoumi.app.entity.FunPrize;
import com.siyoumi.app.mapper.FunPrizeMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fun_prize
@Service
public class FunPrizeService
        extends ServiceImplBase<FunPrizeMapper, FunPrize>{
    static public FunPrizeService getBean(){
        return XSpringContext.getBean(FunPrizeService.class);
    }
}
