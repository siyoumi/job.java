package com.siyoumi.app.service;

import com.siyoumi.app.entity.JobOrder;
import com.siyoumi.app.mapper.JobOrderMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_job_order
@Service
public class JobOrderService
        extends ServiceImplBase<JobOrderMapper, JobOrder> {
    static public JobOrderService getBean() {
        return XSpringContext.getBean(JobOrderService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
