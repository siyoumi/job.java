package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssFileType;
import com.siyoumi.app.mapper.EssFileTypeMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_file_type
@Service
public class EssFileTypeService
        extends ServiceImplBase<EssFileTypeMapper, EssFileType>{
    static public EssFileTypeService getBean(){
        return XSpringContext.getBean(EssFileTypeService.class);
    }
}
