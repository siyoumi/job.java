package com.siyoumi.app.service;

import com.siyoumi.app.entity.ShCmd;
import com.siyoumi.app.mapper.ShCmdMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_sh_cmd
@Service
public class ShCmdService
        extends ServiceImplBase<ShCmdMapper, ShCmd>{
    static public ShCmdService getBean(){
        return XSpringContext.getBean(ShCmdService.class);
    }
}
