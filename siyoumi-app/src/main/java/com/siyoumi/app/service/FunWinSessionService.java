package com.siyoumi.app.service;

import com.siyoumi.app.entity.FunWinSession;
import com.siyoumi.app.mapper.FunWinSessionMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fun_win_session
@Service
public class FunWinSessionService
        extends ServiceImplBase<FunWinSessionMapper, FunWinSession> {
    static public FunWinSessionService getBean() {
        return XSpringContext.getBean(FunWinSessionService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
