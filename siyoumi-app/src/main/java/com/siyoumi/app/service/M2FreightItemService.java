package com.siyoumi.app.service;

import com.siyoumi.app.entity.M2FreightItem;
import com.siyoumi.app.mapper.M2FreightItemMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_m2_freight_item
@Service
public class M2FreightItemService
        extends ServiceImplBase<M2FreightItemMapper, M2FreightItem>{
    static public M2FreightItemService getBean(){
        return XSpringContext.getBean(M2FreightItemService.class);
    }
}
