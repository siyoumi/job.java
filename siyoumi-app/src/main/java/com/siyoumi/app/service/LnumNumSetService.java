package com.siyoumi.app.service;

import com.siyoumi.app.entity.LnumNumSet;
import com.siyoumi.app.mapper.LnumNumSetMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_lnum_num_set
@Service
public class LnumNumSetService
        extends ServiceImplBase<LnumNumSetMapper, LnumNumSet> {
    static public LnumNumSetService getBean() {
        return XSpringContext.getBean(LnumNumSetService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
