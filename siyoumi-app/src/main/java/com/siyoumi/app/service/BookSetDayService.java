package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookSetDay;
import com.siyoumi.app.mapper.BookSetDayMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_set_day
@Service
public class BookSetDayService
        extends ServiceImplBase<BookSetDayMapper, BookSetDay>{
    static public BookSetDayService getBean(){
        return XSpringContext.getBean(BookSetDayService.class);
    }
}
