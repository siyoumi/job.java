package com.siyoumi.app.service;

import com.siyoumi.app.entity.WxSubscribemsg;
import com.siyoumi.app.mapper.WxSubscribemsgMapper;
import com.siyoumi.app.sys.service.wxapi.WxApiApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.exception.XException;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


//t_wx_subscribemsg
@Service
public class WxSubscribemsgService
        extends ServiceImplBase<WxSubscribemsgMapper, WxSubscribemsg> {
    static public WxSubscribemsgService getBean() {
        return XSpringContext.getBean(WxSubscribemsgService.class);
    }

    @Override
    public XReturn saveEntityBefore(InputData inputData, WxSubscribemsg entity) {
        Map<String, Object> content = new HashMap<>();
        inputData.forEach((k, v) -> {
            if (XStr.startsWith(k, "data_")) {
                String key = k.replace("data_", "");

                Map<String, Object> value = new HashMap<>();
                value.put("value", v);
                content.put(key, value);
            }
        });
        if (content.size() > 0) {
            entity.setWxsg_content(XJson.toJSONString(content));
        }

        return super.saveEntityBefore(inputData, entity);
    }

    /**
     * json转map
     *
     * @param content
     */
    public Map<String, Object> contentToMap(String content) {
        if (content.equals("[]")) {
            return new HashMap<>();
        }
        return XStr.toJson(content);
    }

    /**
     * 模板列表
     */
    public XReturn getMpTemplmsg() {
        SysAccsuperConfig entityConfig = XHttpContext.getXConfig(true);
        WxApiApp miniProgram = WxApiApp.getIns(entityConfig);
        XReturn r = miniProgram.getTemplate();
        if (r.err()) {
            return r;
        }
        List<Map> apiData = r.getData("data");
        if (apiData.size() <= 0) {
            return XReturn.getR(20063, "订阅模板列表为空，请前往mp后台添加");
        }

        Map<String, Object> list = new HashMap<>();

        Map<String, Map<String, String>> errmsgs = contentErrmsg();
        for (Map apiItem : apiData) {
            Map<String, Object> data = new HashMap<>();

            String priTmplId = (String) apiItem.get("priTmplId");
            String content = (String) apiItem.get("content");

            String[] contentArr = content.split("\n");
            for (String contentItem : contentArr) {
                if (XStr.isNullOrEmpty(contentItem)) continue;

                String[] itemArr = contentItem.split(":");
                String errmsgKey = itemArr[1].replace("{{", "")
                        .replace("}}", "");
                String errmsgKeyX = errmsgKey.replaceAll("[0-9]", ""); //去掉数字
                Map<String, String> errmsgMap = errmsgs.get(errmsgKeyX);
                if (errmsgMap == null) {
                    continue;
                }

                Map<String, Object> dataItem = new HashMap<>();
                dataItem.put("val", itemArr[0]);
                dataItem.put("msg", errmsgMap.get("msg"));
                dataItem.put("example", errmsgMap.get("example"));
                data.put(errmsgKey, dataItem);
            }

            apiItem.put("content_list", data);
            list.put(priTmplId, apiItem);
        }

        r.setData("errmsgs", errmsgs);
        r.setData("list", list);
        return r;
    }


    protected Map<String, Map<String, String>> contentErrmsg() {
        String msg = "thing.DATA|20个以内字符|可汉字、数字、字母或符号组合\n" +
                "number.DATA|32位以内数字|只能数字，可带小数\n" +
                "letter.DATA|32位以内字母|只能字母\n" +
                "symbol.DATA|5位以内符号|只能符号\n" +
                "character_string.DATA|32位以内数字、字母或符号|可数字、字母或符号组合\n" +
                "time.DATA|24小时制时间格式（支持+年月日）|例如：15:01，或：2019年10月1日 15:01\n" +
                "date.DATA|年月日格式（支持+24小时制时间）|例如：2019年10月1日，或：2019年10月1日 15:01\n" +
                "amount.DATA|1个币种符号+10位以内纯数字，可带小数，结尾可带“元”|可带小数\n" +
                "phone_number.DATA|17位以内，数字、符号|电话号码，例：+86-0766-66888866\n" +
                "car_number.DATA|8位以内，第一位与最后一位可为汉字，其余为字母或数字|车牌号码：粤A8Z888挂\n" +
                "name.DATA|10个以内纯汉字或20个以内纯字母或符号|中文名10个汉字内；纯英文名20个字母内；中文和字母混合按中文名算，10个字内\n" +
                "phrase.DATA|5个以内汉字|5个以内纯汉字，例如：配送中";

        Map<String, Map<String, String>> data = new HashMap<>();
        String[] arrMsg = msg.split("\n");
        for (String s : arrMsg) {
            String[] sArr = s.split("\\|");

            Map<String, String> item = new HashMap<>();
            item.put("msg", sArr[1]); //说明
            item.put("example", sArr[2]); //例子

            data.put(sArr[0], item);
        }

        return data;
    }


    public XReturn send(String id, String openid, Map<String, String> replaceArr) {
        String sendStr = getSendJson(id, openid, replaceArr);
        WxApiApp app = WxApiApp.getIns(XHttpContext.getXConfig());
        return app.sendTemplate(XStr.toJson(sendStr));
    }

    /**
     * 获取发送内容
     *
     * @param id
     * @param openid     用户
     * @param replaceArr 替换符
     */
    @SneakyThrows
    public String getSendJson(String id, String openid, Map<String, String> replaceArr) {
        WxSubscribemsg entity = getEntity(id);
        if (entity == null || XStr.isNullOrEmpty(entity.getWxsg_content())) {
            throw new XException(EnumSys.ERR_VAL.getR("id异常或配置内容为空"));
        }

        Map<String, Object> sendData = new HashMap<>();
        sendData.put("touser", openid);
        sendData.put("template_id", entity.getWxsg_template_id());
        sendData.put("data", XStr.toJson(entity.getWxsg_content()));
        sendData.put("page", entity.getWxsg_page());

        if (XStr.hasAnyText(entity.getWxsg_miniprogram_state())) {
            //环境
            sendData.put("miniprogram_state", entity.getWxsg_miniprogram_state());
        }

        String sendContent = XJson.toJSONString(sendData);
        //替换
        if (replaceArr == null) {
            replaceArr = new HashMap<>();
        }
        replaceArr.put("{$now}", XDate.toDateTimeString());
        replaceArr.put("{$date}", XDate.toDateString());

        return XStr.replaceByMap(sendContent, replaceArr);
    }
}
