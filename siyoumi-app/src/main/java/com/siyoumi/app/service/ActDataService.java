package com.siyoumi.app.service;

import com.siyoumi.app.entity.ActData;
import com.siyoumi.app.mapper.ActDataMapper;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;


//t_act_data
@Service
public class ActDataService
        extends ServiceImplBase<ActDataMapper, ActData> {
    static public ActDataService getBean() {
        return XSpringContext.getBean(ActDataService.class);
    }

    public ActData getEntity(String actId, String key) {
        JoinWrapperPlus<ActData> query = listQuery(actId, null);
        query.eq("adata_uix", key);
        return first(query);
    }


    public JoinWrapperPlus<ActData> listQuery(String actId, String openid) {
        JoinWrapperPlus<ActData> query = join();
        query.eq("adata_actset_id", actId)
                .orderByDesc("adata_id");
        if (XStr.hasAnyText(openid)) {
            query.eq("adata_openid", openid);
        }
        return query;
    }
}
