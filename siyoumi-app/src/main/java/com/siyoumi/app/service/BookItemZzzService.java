package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookItemZzz;
import com.siyoumi.app.mapper.BookItemZzzMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_item_zzz
@Service
public class BookItemZzzService
        extends ServiceImplBase<BookItemZzzMapper, BookItemZzz>{
    static public BookItemZzzService getBean(){
        return XSpringContext.getBean(BookItemZzzService.class);
    }
}
