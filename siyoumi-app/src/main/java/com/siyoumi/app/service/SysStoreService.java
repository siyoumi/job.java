package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysStore;
import com.siyoumi.app.mapper.SysStoreMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_sys_store
@Service
public class SysStoreService
        extends ServiceImplBase<SysStoreMapper, SysStore>{
    static public SysStoreService getBean(){
        return XSpringContext.getBean(SysStoreService.class);
    }
}
