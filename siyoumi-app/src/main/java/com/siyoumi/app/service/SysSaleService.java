package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysSale;
import com.siyoumi.app.mapper.SysSaleMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_s_sale
@Service
public class SysSaleService
        extends ServiceImplBase<SysSaleMapper, SysSale>{
    static public SysSaleService getBean(){
        return XSpringContext.getBean(SysSaleService.class);
    }
}
