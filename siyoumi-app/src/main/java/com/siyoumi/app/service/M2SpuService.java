package com.siyoumi.app.service;

import com.siyoumi.app.entity.M2Spu;
import com.siyoumi.app.mapper.M2SpuMapper;
import com.siyoumi.app.modules.mall2.service.SvcM2Spu;
import com.siyoumi.app.modules.mall2.vo.SpuAttrVo;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.util.*;
import com.siyoumi.component.http.InputData;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


//t_m2_spu
@Slf4j
@Service
public class M2SpuService
        extends ServiceImplBase<M2SpuMapper, M2Spu> {
    @Override
    protected boolean softDelete() {
        return true;
    }

    static public M2SpuService getBean() {
        return XSpringContext.getBean(M2SpuService.class);
    }

    @Override
    public XReturn saveEntityAfter(InputData inputData, M2Spu entity) {
        List<SpuAttrVo> attr = (List<SpuAttrVo>) inputData.get("attr");
        if (attr == null) {
            log.debug("没商品属性操作");
            return XReturn.getR(0);
        }

        XReturn r = SvcM2Spu.getBean().saveAttrSku(entity, attr);
        XValidator.err(r);

        return r;
    }

    /**
     * del
     */
    public Boolean delete(M2Spu entity) {
        if (entity.del()) {
            return false;
        }

        delete(entity.getKey());

        //同时删除sku
        M2SkuService.getBean().deleteBySpuId(entity.getMspu_id());

        return true;
    }
}
