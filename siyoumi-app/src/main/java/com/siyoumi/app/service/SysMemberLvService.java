package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysMemberLv;
import com.siyoumi.app.mapper.SysMemberLvMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_s_member_lv
@Service
public class SysMemberLvService
        extends ServiceImplBase<SysMemberLvMapper, SysMemberLv>{
    static public SysMemberLvService getBean(){
        return XSpringContext.getBean(SysMemberLvService.class);
    }
}
