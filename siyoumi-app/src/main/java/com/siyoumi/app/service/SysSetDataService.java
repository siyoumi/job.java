package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysSetData;
import com.siyoumi.app.mapper.SysSetDataMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_s_set_data
@Service
public class SysSetDataService
        extends ServiceImplBase<SysSetDataMapper, SysSetData>{
    static public SysSetDataService getBean(){
        return XSpringContext.getBean(SysSetDataService.class);
    }
}
