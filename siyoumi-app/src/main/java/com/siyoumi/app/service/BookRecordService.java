package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysBookRecord;
import com.siyoumi.app.mapper.BookRecordMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_record
@Service
public class BookRecordService
        extends ServiceImplBase<BookRecordMapper, SysBookRecord>{
    static public BookRecordService getBean(){
        return XSpringContext.getBean(BookRecordService.class);
    }
}
