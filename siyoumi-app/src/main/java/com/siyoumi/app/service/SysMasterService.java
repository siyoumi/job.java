package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysMaster;
import com.siyoumi.app.mapper.SysMasterMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_sys_master
@Service
public class SysMasterService
        extends ServiceImplBase<SysMasterMapper, SysMaster>{
    static public SysMasterService getBean(){
        return XSpringContext.getBean(SysMasterService.class);
    }
}
