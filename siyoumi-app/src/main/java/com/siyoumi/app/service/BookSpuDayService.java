package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookSpuDay;
import com.siyoumi.app.mapper.BookSpuDayMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_spu_day
@Service
public class BookSpuDayService
        extends ServiceImplBase<BookSpuDayMapper, BookSpuDay>{
    static public BookSpuDayService getBean(){
        return XSpringContext.getBean(BookSpuDayService.class);
    }
}
