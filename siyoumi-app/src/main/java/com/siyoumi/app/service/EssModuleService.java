package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssModule;
import com.siyoumi.app.mapper.EssModuleMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_module
@Service
public class EssModuleService
        extends ServiceImplBase<EssModuleMapper, EssModule>{
    static public EssModuleService getBean(){
        return XSpringContext.getBean(EssModuleService.class);
    }

    protected boolean softDelete() {
        return true;
    }
}
