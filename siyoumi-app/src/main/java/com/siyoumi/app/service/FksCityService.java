package com.siyoumi.app.service;

import com.siyoumi.app.entity.FksCity;
import com.siyoumi.app.mapper.FksCityMapper;
import com.siyoumi.component.http.InputData;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;


//t_fks_city
@Service
public class FksCityService
        extends ServiceImplBase<FksCityMapper, FksCity>{
    static public FksCityService getBean(){
        return XSpringContext.getBean(FksCityService.class);
    }
}
