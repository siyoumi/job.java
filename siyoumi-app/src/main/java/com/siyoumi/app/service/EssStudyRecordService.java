package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssStudyRecord;
import com.siyoumi.app.mapper.EssStudyRecordMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_study_record
@Service
public class EssStudyRecordService
        extends ServiceImplBase<EssStudyRecordMapper, EssStudyRecord>{
    static public EssStudyRecordService getBean(){
        return XSpringContext.getBean(EssStudyRecordService.class);
    }
}
