package com.siyoumi.app.service;

import com.siyoumi.app.entity.FksTagUser;
import com.siyoumi.app.mapper.FksTagUserMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fks_tag_user
@Service
public class FksTagUserService
        extends ServiceImplBase<FksTagUserMapper, FksTagUser>{
    static public FksTagUserService getBean(){
        return XSpringContext.getBean(FksTagUserService.class);
    }
}
