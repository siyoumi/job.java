package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysBook;
import com.siyoumi.app.mapper.BookMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book
@Service
public class BookService
        extends ServiceImplBase<BookMapper, SysBook>{
    static public BookService getBean(){
        return XSpringContext.getBean(BookService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
