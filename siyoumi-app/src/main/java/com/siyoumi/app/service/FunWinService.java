package com.siyoumi.app.service;

import com.siyoumi.app.entity.FunWin;
import com.siyoumi.app.mapper.FunWinMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fun_win
@Service
public class FunWinService
        extends ServiceImplBase<FunWinMapper, FunWin>{
    static public FunWinService getBean(){
        return XSpringContext.getBean(FunWinService.class);
    }
}
