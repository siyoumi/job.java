package com.siyoumi.app.service;

import com.siyoumi.app.entity.RedbookQuan;
import com.siyoumi.app.mapper.RedbookQuanMapper;
import com.siyoumi.app.modules.redbook.service.SvcRedbookQuan;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;

import java.util.List;


//t_redbook_quan
@Service
public class RedbookQuanService
        extends ServiceImplBase<RedbookQuanMapper, RedbookQuan> {
    static public RedbookQuanService getBean() {
        return XSpringContext.getBean(RedbookQuanService.class);
    }

    @Override
    public XReturn saveEntityAfter(InputData inputData, RedbookQuan entity) {
        //审核通过
        SvcRedbookQuan.getBean().enable(List.of(entity.getKey()), 1);

        return super.saveEntityAfter(inputData, entity);
    }
}
