package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysOrder;
import com.siyoumi.app.mapper.SysOrderMapper;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.util.XSqlStr;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;


//t_s_order
@Service
public class SysOrderService
        extends ServiceImplBase<SysOrderMapper, SysOrder> {
    static public SysOrderService getBean() {
        return XSpringContext.getBean(SysOrderService.class);
    }

    public SysOrder getEntityByOrderId(String orderId) {
        return getEntityByOrderId(orderId, false);
    }

    public SysOrder getEntityByOrderId(String orderId, Boolean lock) {
        JoinWrapperPlus<SysOrder> query = join();
        query.eq("order_id", orderId);
        if (lock) {
            query.last(XSqlStr.FOR_UPDATE);
        }

        return first(query);
    }

    /**
     * 标记回滚完成
     */
    public boolean setRollbackDone(SysOrder entity) {
        if (entity.checkPayOk()) {
            return false;
        }
        if (entity.getOrder_pay_expire_handle() == 1) {
            return false;
        }

        SysOrder update = new SysOrder();
        update.setOrder_id(entity.getOrder_id());
        update.setOrder_pay_expire_handle(1);
        update.setOrder_pay_expire_handle_date(LocalDateTime.now());
        update.setOrder_del(0L);
        updateById(update);

        return true;
    }


    public SysOrder newEntity(String appId, String openid, LocalDateTime expireDate, BigDecimal orderPrice, String desc, Integer canMoney, String id00) {
        return newEntity(appId, openid, expireDate, orderPrice, desc, canMoney, id00, null, null, null, null);
    }

    /**
     * 新建
     *
     * @param appId
     * @param openid
     * @param expireDate 过期时间
     * @param orderPrice 订单原金额
     * @param desc       描述
     * @param canMoney   1：允许使用钱包支付；
     * @param id00       备用字段
     * @param phone      手机号
     * @param name       姓名
     * @param orderId    支付订单号
     * @param payType    支付类型：0:现金；10：积分
     */
    public SysOrder newEntity(String appId, String openid, LocalDateTime expireDate, BigDecimal orderPrice
            , String desc, Integer canMoney, String id00, String phone, String name
            , String orderId, Integer payType) {
        SysOrder entityOrder = new SysOrder();
        entityOrder.setOrder_app_id(appId);
        entityOrder.setOrder_uid(openid);

        entityOrder.setAutoID();
        entityOrder.setOrder_pay_price(orderPrice);
        entityOrder.setOrder_pay_type(payType);
        entityOrder.setOrder_pay_expire_date(expireDate);
        entityOrder.setOrder_user_phone(phone);
        entityOrder.setOrder_user_name(name);
        entityOrder.setOrder_can_money(canMoney.longValue());
        entityOrder.setOrder_id_00(id00);
        entityOrder.setOrder_x_id(XHttpContext.getX());
        entityOrder.setOrder_desc(XStr.maxLen(desc, 128));
        entityOrder.setOrder_price_coupon_down(BigDecimal.ZERO);
        entityOrder.setOrder_price_money_down(BigDecimal.ZERO);
        entityOrder.setOrder_price(BigDecimal.ZERO);
        entityOrder.setOrder_price_ship(BigDecimal.ZERO);
        entityOrder.setPayPrice();

        save(entityOrder);
        return entityOrder;
    }
}
