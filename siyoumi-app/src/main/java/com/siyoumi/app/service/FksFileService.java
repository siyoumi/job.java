package com.siyoumi.app.service;

import com.siyoumi.app.entity.FksFile;
import com.siyoumi.app.mapper.FksFileMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fks_file
@Service
public class FksFileService
        extends ServiceImplBase<FksFileMapper, FksFile>{
    static public FksFileService getBean(){
        return XSpringContext.getBean(FksFileService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
