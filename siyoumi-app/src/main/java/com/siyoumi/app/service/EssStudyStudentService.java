package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssStudyStudent;
import com.siyoumi.app.mapper.EssStudyStudentMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_study_student
@Service
public class EssStudyStudentService
        extends ServiceImplBase<EssStudyStudentMapper, EssStudyStudent>{
    static public EssStudyStudentService getBean(){
        return XSpringContext.getBean(EssStudyStudentService.class);
    }
}
