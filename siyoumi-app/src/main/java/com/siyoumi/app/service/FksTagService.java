package com.siyoumi.app.service;

import com.siyoumi.app.entity.FksTag;
import com.siyoumi.app.mapper.FksTagMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fks_tag
@Service
public class FksTagService
        extends ServiceImplBase<FksTagMapper, FksTag>{
    static public FksTagService getBean(){
        return XSpringContext.getBean(FksTagService.class);
    }
}
