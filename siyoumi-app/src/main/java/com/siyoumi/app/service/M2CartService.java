package com.siyoumi.app.service;

import com.siyoumi.app.entity.M2Cart;
import com.siyoumi.app.mapper.M2CartMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_m2_cart
@Service
public class M2CartService
        extends ServiceImplBase<M2CartMapper, M2Cart>{
    static public M2CartService getBean(){
        return XSpringContext.getBean(M2CartService.class);
    }
}
