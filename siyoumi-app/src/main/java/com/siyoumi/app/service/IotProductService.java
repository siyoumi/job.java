package com.siyoumi.app.service;

import com.siyoumi.app.entity.IotProduct;
import com.siyoumi.app.mapper.IotProductMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_iot_product
@Service
public class IotProductService
        extends ServiceImplBase<IotProductMapper, IotProduct> {
    static public IotProductService getBean() {
        return XSpringContext.getBean(IotProductService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
