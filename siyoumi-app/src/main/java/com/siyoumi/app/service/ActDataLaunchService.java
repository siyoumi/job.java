package com.siyoumi.app.service;

import com.siyoumi.app.entity.ActDataLaunch;
import com.siyoumi.app.mapper.ActDataLaunchMapper;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;

import java.util.Map;


//t_act_data_launch
@Service
public class ActDataLaunchService
        extends ServiceImplBase<ActDataLaunchMapper, ActDataLaunch> {
    static public ActDataLaunchService getBean() {
        return XSpringContext.getBean(ActDataLaunchService.class);
    }

    public ActDataLaunch getOrNewEntity(String actId, String key, String openId) {
        ActDataLaunch entity = getEntity(actId, key);
        if (entity == null) {
            entity = new ActDataLaunch();
            entity.setAdl_x_id(XHttpContext.getX());
            entity.setAdl_actset_id(actId);
            entity.setAdl_key(key);
            entity.setAdl_openid(openId);
            entity.setAutoID();

            save(entity);
        }
        return entity;
    }

    public ActDataLaunch getEntity(String actId, String key) {
        JoinWrapperPlus<ActDataLaunch> query = join();
        query.eq("adl_actset_id", actId)
                .eq("adl_key", key);
        return first(query);
    }
}
