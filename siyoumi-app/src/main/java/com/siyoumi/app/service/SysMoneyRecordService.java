package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysMoneyRecord;
import com.siyoumi.app.mapper.SysMoneyRecordMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_s_money_record
@Service
public class SysMoneyRecordService
        extends ServiceImplBase<SysMoneyRecordMapper, SysMoneyRecord> {
    static public SysMoneyRecordService getBean() {
        return XSpringContext.getBean(SysMoneyRecordService.class);
    }
}
