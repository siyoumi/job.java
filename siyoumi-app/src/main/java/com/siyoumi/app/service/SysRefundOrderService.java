package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysRefundOrder;
import com.siyoumi.app.mapper.SysRefundOrderMapper;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_s_refund_order
@Service
public class SysRefundOrderService
        extends ServiceImplBase<SysRefundOrderMapper, SysRefundOrder> {
    static public SysRefundOrderService getBean() {
        return XSpringContext.getBean(SysRefundOrderService.class);
    }

    /**
     * 获取退款订单
     *
     * @param orderId
     * @param refundId
     */
    public SysRefundOrder getEntityByOrderId(String orderId, String refundId) {
        JoinWrapperPlus<SysRefundOrder> query = join();
        query.eq("refo_order_id", orderId)
                .eq("refo_refund_id", refundId);

        return first(query);
    }
}
