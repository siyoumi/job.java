package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssTestResult;
import com.siyoumi.app.mapper.EssTestResultMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_test_result
@Service
public class EssTestResultService
        extends ServiceImplBase<EssTestResultMapper, EssTestResult>{
    static public EssTestResultService getBean(){
        return XSpringContext.getBean(EssTestResultService.class);
    }
}
