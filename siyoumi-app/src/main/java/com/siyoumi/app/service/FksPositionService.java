package com.siyoumi.app.service;

import com.siyoumi.app.entity.FksPosition;
import com.siyoumi.app.mapper.FksPositionMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fks_position
@Service
public class FksPositionService
        extends ServiceImplBase<FksPositionMapper, FksPosition>{
    static public FksPositionService getBean(){
        return XSpringContext.getBean(FksPositionService.class);
    }
}
