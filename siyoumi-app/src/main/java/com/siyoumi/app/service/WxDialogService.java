package com.siyoumi.app.service;

import com.siyoumi.app.entity.WxDialog;
import com.siyoumi.app.mapper.WxDialogMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_wx_dialog
@Service
public class WxDialogService
        extends ServiceImplBase<WxDialogMapper, WxDialog>{
    static public WxDialogService getBean(){
        return XSpringContext.getBean(WxDialogService.class);
    }
}
