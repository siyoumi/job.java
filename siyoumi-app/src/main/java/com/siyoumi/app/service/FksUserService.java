package com.siyoumi.app.service;

import com.siyoumi.app.entity.FksUser;
import com.siyoumi.app.mapper.FksUserMapper;
import com.siyoumi.app.modules.user.service.SvcSysUser;
import com.siyoumi.component.http.InputData;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;


//t_fks_user
@Service
public class FksUserService
        extends ServiceImplBase<FksUserMapper, FksUser> {
    static public FksUserService getBean() {
        return XSpringContext.getBean(FksUserService.class);
    }
}
