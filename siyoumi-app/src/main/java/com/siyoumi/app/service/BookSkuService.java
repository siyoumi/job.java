package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookSku;
import com.siyoumi.app.mapper.BookSkuMapper;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.service.ServiceImplBase;
import org.springframework.stereotype.Service;


//t_book_sku
@Service
public class BookSkuService
        extends ServiceImplBase<BookSkuMapper, BookSku>{
    static public BookSkuService getBean(){
        return XSpringContext.getBean(BookSkuService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
