package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.app.entity.SysPrizeSetChou;
import com.siyoumi.app.entity.SysStock;
import com.siyoumi.app.mapper.SysPrizeSetMapper;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSet;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;


//t_s_prize_set
@Service
public class SysPrizeSetService
        extends ServiceImplBase<SysPrizeSetMapper, SysPrizeSet> {
    static public SysPrizeSetService getBean() {
        return XSpringContext.getBean(SysPrizeSetService.class);
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(String id) {
        SysPrizeSet update = new SysPrizeSet();
        update.setPset_id(id);
        update.setPset_del(XDate.toS());
        updateById(update);

        //删除chou_set
        SysPrizeSetChou updateChou = new SysPrizeSetChou();
        updateChou.setPschou_id(id);
        updateChou.setPschou_del(XDate.toS());
        updateChou.setPschou_win_rate(new BigDecimal("0"));
        SysPrizeSetChouService.getBean().updateById(updateChou);

        return XReturn.getR(0);
    }


    @Override
    public XReturn saveEntityAfter(InputData inputData, SysPrizeSet entity) {
        XLog.info(this.getClass(), "抽奖扩展表");
        SysPrizeSetChouService appChou = SysPrizeSetChouService.getBean();

        SysPrizeSetChou entityChou = appChou.getEntity(entity.getKey());
        if (entityChou == null) {
            entityChou = new SysPrizeSetChou();
            entityChou.setPschou_x_id(entity.getPset_x_id());
            entityChou.setPschou_id_src(entity.getPset_id_src());
            entityChou.setPschou_id(entity.getKey());
            appChou.save(entityChou);
        }
        SysPrizeSetChou entityChouUpdate = appChou.loadEntity(inputData);
        entityChouUpdate.setPschou_id(entityChou.getKey());
        appChou.saveOrUpdatePassEqualField(entityChou, entityChouUpdate);

        XLog.info(this.getClass(), "库存");
        SysStockService appStock = SysStockService.getBean();
        SysStock entityStock = appStock.getEntityBySrc(entity.getKey());
        if (entityStock == null) {
            entityStock = new SysStock();
            entityStock.setStock_x_id(XHttpContext.getX());
            entityStock.setStock_id_src(entity.getKey());
            entityStock.setStock_app_id(entity.getPset_app_id());
            entityStock.setAutoID();
            //
            appStock.save(entityStock);
        }

        String stockCountLeft = inputData.input("__stock_count_left", "0");
        String stockCountLeftOld = inputData.input("__stock_count_left_old", "0");
        if (!stockCountLeft.equals(stockCountLeftOld)) {
            appStock.updateLeft(entity.getKey(), Long.valueOf(stockCountLeft), Long.valueOf(stockCountLeftOld));
        }

        return super.saveEntityAfter(inputData, entity);
    }
}
