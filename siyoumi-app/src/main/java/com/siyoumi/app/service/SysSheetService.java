package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysSheet;
import com.siyoumi.app.mapper.SysSheetMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_a_sheet
@Service
public class SysSheetService
        extends ServiceImplBase<SysSheetMapper, SysSheet>{
    static public SysSheetService getBean(){
        return XSpringContext.getBean(SysSheetService.class);
    }
}
