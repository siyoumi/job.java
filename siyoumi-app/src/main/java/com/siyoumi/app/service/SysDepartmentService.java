package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysDepartment;
import com.siyoumi.app.mapper.SysDepartmentMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_sys_department
@Service
public class SysDepartmentService
        extends ServiceImplBase<SysDepartmentMapper, SysDepartment>{
    static public SysDepartmentService getBean(){
        return XSpringContext.getBean(SysDepartmentService.class);
    }
}
