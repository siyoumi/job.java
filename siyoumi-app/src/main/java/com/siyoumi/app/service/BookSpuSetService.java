package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookSpuSet;
import com.siyoumi.app.mapper.BookSpuSetMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_spu_set
@Service
public class BookSpuSetService
        extends ServiceImplBase<BookSpuSetMapper, BookSpuSet>{
    static public BookSpuSetService getBean(){
        return XSpringContext.getBean(BookSpuSetService.class);
    }
}
