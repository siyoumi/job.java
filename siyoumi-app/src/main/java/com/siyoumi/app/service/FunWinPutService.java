package com.siyoumi.app.service;

import com.siyoumi.app.entity.FunWinPut;
import com.siyoumi.app.mapper.FunWinPutMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fun_win_put
@Service
public class FunWinPutService
        extends ServiceImplBase<FunWinPutMapper, FunWinPut>{
    static public FunWinPutService getBean(){
        return XSpringContext.getBean(FunWinPutService.class);
    }
}
