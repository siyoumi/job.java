package com.siyoumi.app.service;

import com.siyoumi.app.entity.WxMsgQueue;
import com.siyoumi.app.mapper.WxMsgQueueMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_wx_msg_queue
@Service
public class WxMsgQueueService
        extends ServiceImplBase<WxMsgQueueMapper, WxMsgQueue>{
    static public WxMsgQueueService getBean(){
        return XSpringContext.getBean(WxMsgQueueService.class);
    }
}
