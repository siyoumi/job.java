package com.siyoumi.app.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.M2Sku;
import com.siyoumi.app.mapper.M2SkuMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.util.XDate;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


//t_m2_sku
@Service
public class M2SkuService
        extends ServiceImplBase<M2SkuMapper, M2Sku> {
    static public M2SkuService getBean() {
        return XSpringContext.getBean(M2SkuService.class);
    }

    public QueryWrapper<M2Sku> getQuery(String spuId) {
        QueryWrapper<M2Sku> query = q();
        query.eq("msku_del", 0)
                .eq("msku_spu_id", spuId);

        return query;
    }

    /**
     * 删除
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public Boolean delete(M2Sku entity) {
        M2Sku entityUpdate = new M2Sku();
        entityUpdate.setMsku_id(entity.getKey());
        entityUpdate.setMsku_del(XDate.toS());
        return updateById(entityUpdate);
    }

    /**
     * 删除整个商品的sku
     * 及标记重新同步sku
     *
     * @param spuId
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public Boolean deleteBySpuId(String spuId) {
        List<M2Sku> list = list(getQuery(spuId));
        for (M2Sku entitySku : list) {
            delete(entitySku);
        }

        return true;
    }
}
