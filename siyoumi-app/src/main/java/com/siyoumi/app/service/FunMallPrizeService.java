package com.siyoumi.app.service;

import com.siyoumi.app.entity.FunMallPrize;
import com.siyoumi.app.mapper.FunMallPrizeMapper;
import com.siyoumi.app.modules.prize.service.SvcSysPrizeSetAdmin;
import com.siyoumi.component.http.InputData;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;


//t_fun_mall_prize
@Service
public class FunMallPrizeService
        extends ServiceImplBase<FunMallPrizeMapper, FunMallPrize> {
    static public FunMallPrizeService getBean() {
        return XSpringContext.getBean(FunMallPrizeService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
