package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysSaleClient;
import com.siyoumi.app.mapper.SysSaleClientMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_s_sale_client
@Service
public class SysSaleClientService
        extends ServiceImplBase<SysSaleClientMapper, SysSaleClient>{
    static public SysSaleClientService getBean(){
        return XSpringContext.getBean(SysSaleClientService.class);
    }
}
