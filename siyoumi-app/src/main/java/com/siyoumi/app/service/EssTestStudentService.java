package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssTestStudent;
import com.siyoumi.app.mapper.EssTestStudentMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_test_student
@Service
public class EssTestStudentService
        extends ServiceImplBase<EssTestStudentMapper, EssTestStudent>{
    static public EssTestStudentService getBean(){
        return XSpringContext.getBean(EssTestStudentService.class);
    }
}
