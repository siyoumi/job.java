
package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysUv;
import com.siyoumi.app.mapper.SysUvMapper;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


//t_sys_uv
@Slf4j
@Service
public class SysUvService
        extends ServiceImplBase<SysUvMapper, SysUv> {
    static public SysUvService getBean() {
        return XSpringContext.getBean(SysUvService.class);
    }
}
