package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookSpuTxt;
import com.siyoumi.app.mapper.BookSpuTxtMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_spu_txt
@Service
public class BookSpuTxtService
        extends ServiceImplBase<BookSpuTxtMapper, BookSpuTxt>{
    static public BookSpuTxtService getBean(){
        return XSpringContext.getBean(BookSpuTxtService.class);
    }
}
