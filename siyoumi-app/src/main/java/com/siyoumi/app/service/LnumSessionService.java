package com.siyoumi.app.service;

import com.siyoumi.app.entity.LnumSession;
import com.siyoumi.app.mapper.LnumSessionMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_lnum_session
@Service
public class LnumSessionService
        extends ServiceImplBase<LnumSessionMapper, LnumSession> {
    static public LnumSessionService getBean() {
        return XSpringContext.getBean(LnumSessionService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
