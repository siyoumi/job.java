package com.siyoumi.app.service;

import com.siyoumi.app.entity.M2SpuHot;
import com.siyoumi.app.mapper.M2SpuHotMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_m2_spu_hot
@Service
public class M2SpuHotService
        extends ServiceImplBase<M2SpuHotMapper, M2SpuHot>{
    static public M2SpuHotService getBean(){
        return XSpringContext.getBean(M2SpuHotService.class);
    }
}
