package com.siyoumi.app.service;

import com.siyoumi.app.entity.FunMallGroup;
import com.siyoumi.app.mapper.FunMallGroupMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fun_mall_group
@Service
public class FunMallGroupService
        extends ServiceImplBase<FunMallGroupMapper, FunMallGroup> {
    static public FunMallGroupService getBean() {
        return XSpringContext.getBean(FunMallGroupService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
