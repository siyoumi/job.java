package com.siyoumi.app.service;

import com.siyoumi.app.entity.ActSet;
import com.siyoumi.app.entity.SysItem;
import com.siyoumi.app.entity.SysItemData;
import com.siyoumi.app.mapper.ActSetMapper;
import com.siyoumi.app.modules.app_admin.vo.SysItemVo;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.stereotype.Service;

import java.util.List;


//t_act_set
@Service
public class ActSetService
        extends ServiceImplBase<ActSetMapper, ActSet> {
    static public ActSetService getBean() {
        return XSpringContext.getBean(ActSetService.class);
    }

    public XReturn delete(String id) {
        ActSet update = new ActSet();
        update.setAset_id(id);
        update.setAset_del(XDate.toS());
        updateById(update);

        return XReturn.getR(0);
    }


    @Override
    public XReturn saveEntityAfter(InputData inputData, ActSet entity) {
        if (XStr.hasAnyText(entity.getAset_item_id())) {
            SysItem entityItem = SysItemService.getBean().loadEntity(entity.getAset_item_id());
            List<SysItemVo> sysItems = entityItem.commonData(SysItemVo.class);

            LinkedMap<String, String> mapVal = new LinkedMap<>();
            for (SysItemVo sysItem : sysItems) {
                String key = "data_" + sysItem.getField_key();
                String val = sysItem.getDef();
                if (inputData.existsKey(key)) {
                    val = inputData.input(key);
                }

                mapVal.put(sysItem.getField_key(), val);
            }
            // 保存item data
            SysItemDataService appItemData = SysItemDataService.getBean();
            SysItemData entityData = appItemData.getEntityByKey(entityItem.getKey(), entity.getKey());
            if (entityData == null) {
                entityData = new SysItemData();
                entityData.setIdata_x_id(entity.getAset_x_id());
                entityData.setIdata_app_id("act");
                entityData.setIdata_key(entity.getKey());
                entityData.setIdata_item_id(entityItem.getKey());
                entityData.setAutoID();
                entityData.setIdata_data(XJson.toJSONString(mapVal));

                appItemData.save(entityData);
            } else {
                SysItemData entityDataUpdate = new SysItemData();
                entityDataUpdate.setIdata_id(entityData.getIdata_id());
                entityDataUpdate.setIdata_data(XJson.toJSONString(mapVal));
                // 更新
                appItemData.saveOrUpdatePassEqualField(entityData, entityDataUpdate);
            }

        }

        //清缓存
        delEntityCache(entity.getKey()); //活动
        delEntityCache(entity.getKey() + "|append"); //活动属性

        return XReturn.getR(0);
    }
}
