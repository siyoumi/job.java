package com.siyoumi.app.service;

import com.siyoumi.app.entity.M2Order;
import com.siyoumi.app.entity.SysOrder;
import com.siyoumi.app.mapper.M2OrderMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;


//t_m2_order
@Service
public class M2OrderService
        extends ServiceImplBase<M2OrderMapper, M2Order> {
    static public M2OrderService getBean() {
        return XSpringContext.getBean(M2OrderService.class);
    }

    /**
     * 设置状态状态，同步商家订单
     *
     * @param entityOrder
     * @param enable
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public Boolean setEnable(M2Order entityOrder, Integer enable) {
        M2Order entityUpdate = new M2Order();
        entityUpdate.setMorder_id(entityOrder.getMorder_id());
        entityUpdate.setMorder_enable(enable);
        boolean update = updateById(entityUpdate);


        return update;
    }
}
