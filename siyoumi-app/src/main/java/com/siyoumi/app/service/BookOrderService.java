package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookOrder;
import com.siyoumi.app.mapper.BookOrderMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_order
@Service
public class BookOrderService
        extends ServiceImplBase<BookOrderMapper, BookOrder>{
    static public BookOrderService getBean(){
        return XSpringContext.getBean(BookOrderService.class);
    }
}
