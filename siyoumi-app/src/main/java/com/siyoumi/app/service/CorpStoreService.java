package com.siyoumi.app.service;

import com.siyoumi.app.entity.CorpStore;
import com.siyoumi.app.mapper.CorpStoreMapper;
import com.siyoumi.app.modules.app_corp.service.SvcCropStore;
import com.siyoumi.component.http.InputData;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;


//t_corp_store
@Service
public class CorpStoreService
        extends ServiceImplBase<CorpStoreMapper, CorpStore> {
    static public CorpStoreService getBean() {
        return XSpringContext.getBean(CorpStoreService.class);
    }

    @Override
    public XReturn saveEntityAfter(InputData inputData, CorpStore entity) {
        SvcCropStore.getApp().delEntityCache(entity.getKey()); //清缓存

        return super.saveEntityAfter(inputData, entity);
    }
}
