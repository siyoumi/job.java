package com.siyoumi.app.service;

import com.siyoumi.app.entity.ShUser;
import com.siyoumi.app.mapper.ShUserMapper;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


//t_sh_user
@Slf4j
@Service
public class ShUserService
        extends ServiceImplBase<ShUserMapper, ShUser> {
    static public ShUserService getBean() {
        return XSpringContext.getBean(ShUserService.class);
    }


    public JoinWrapperPlus<ShUser> getQuery() {
        return getQuery(0);
    }

    public JoinWrapperPlus<ShUser> getQuery(int type) {
        JoinWrapperPlus<ShUser> query = join();
        query.eq("suser_x_id", XHttpContext.getX())
                .eq("suser_type", type)
                .orderByAsc(ShUser.F.suser_order)
                .orderByDesc(ShUser.F.suser_id);
        return query;
    }

    @Override
    public XReturn saveEntityBefore(InputData inputData, ShUser entity) {
        if (XStr.hasAnyText(entity.getSuser_key())) {
            log.debug("更新cookie失效时间");
            if (inputData.isAdminAdd()) {
                entity.setSuser_key_expire_date(XDate.now());
            } else {
                String key = inputData.input("key");
                if (!entity.getSuser_key().equals(key)) {
                    entity.setSuser_key_expire_date(XDate.now());
                }
            }
        }

        return XReturn.getR(0);
    }
}
