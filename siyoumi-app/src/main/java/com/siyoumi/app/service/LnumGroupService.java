package com.siyoumi.app.service;

import com.siyoumi.app.entity.LnumGroup;
import com.siyoumi.app.mapper.LnumGroupMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_lnum_group
@Service
public class LnumGroupService
        extends ServiceImplBase<LnumGroupMapper, LnumGroup> {
    static public LnumGroupService getBean() {
        return XSpringContext.getBean(LnumGroupService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
