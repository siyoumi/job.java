package com.siyoumi.app.service;

import com.siyoumi.app.entity.FksDepartment;
import com.siyoumi.app.mapper.FksDepartmentMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fks_department
@Service
public class FksDepartmentService
        extends ServiceImplBase<FksDepartmentMapper, FksDepartment> {
    static public FksDepartmentService getBean() {
        return XSpringContext.getBean(FksDepartmentService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
