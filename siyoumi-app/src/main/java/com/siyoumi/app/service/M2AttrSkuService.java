package com.siyoumi.app.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.M2AttrSku;
import com.siyoumi.app.entity.M2Sku;
import com.siyoumi.app.mapper.M2AttrSkuMapper;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.util.XDate;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


//t_m2_attr_sku
@Service
public class M2AttrSkuService
        extends ServiceImplBase<M2AttrSkuMapper, M2AttrSku> {
    static public M2AttrSkuService getBean() {
        return XSpringContext.getBean(M2AttrSkuService.class);
    }


    public QueryWrapper<M2AttrSku> listQuery(String spuId) {
        QueryWrapper<M2AttrSku> query = q();
        query.eq("masku_del", 0);
        if (XStr.hasAnyText(spuId)) { //商品id
            query.eq("masku_spu_id", spuId);
        }

        return query;
    }

    /**
     * 是否需要同步
     *
     * @param spuId
     * @return true：需要同步
     */
    public Boolean isSync(String spuId) {
        QueryWrapper<M2AttrSku> query = listQuery(spuId);
        query.eq("masku_sync", 0);
        M2AttrSku entity = first(query);
        return entity != null;
    }

    /**
     * 列表
     *
     * @param spuId
     */
    public List<M2AttrSku> getList(String spuId, String attrId) {
        QueryWrapper<M2AttrSku> query = listQuery(spuId)
                .orderByAsc("masku_order");
        if (XStr.hasAnyText(attrId)) { //商品属性id
            query.eq("masku_attr_id", attrId);
        }

        return list(query);
    }

    /**
     * 删除规格属性,并同步删除sku
     *
     * @param id
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(String id) {
        //标记删除
        M2AttrSku entityUpdate = new M2AttrSku();
        entityUpdate.setMasku_del(XDate.toS());
        entityUpdate.setMasku_id(id);
        boolean is = updateById(entityUpdate);

        //删除sku
        M2SkuService appSku = M2SkuService.getBean();
        QueryWrapper<M2Sku> query = appSku.q();
        query.eq("msku_del", 0)
                .and(q ->
                {
                    q.eq("msku_attr_sku_00", id)
                            .or().eq("msku_attr_sku_01", id)
                            .or().eq("msku_attr_sku_02", id);
                });
        List<M2Sku> listSku = appSku.get(query);
        for (M2Sku entitySku : listSku) {
            appSku.delete(entitySku);
        }

        return EnumSys.OK.getR();
    }

    /**
     * 标记同步状态
     */
    public void setSyncX(List<String> ids, String spuId, Integer sync) {
        QueryWrapper<M2AttrSku> query = listQuery(spuId);
        if (ids != null) {
            query.in("masku_id", ids);
        }

        List<M2AttrSku> list = get(query);
        List<String> attrSkuids = list.stream().map(M2AttrSku::getMasku_id).collect(Collectors.toList());

        update().set("masku_sync", sync)
                .in("masku_id", attrSkuids)
                .update();
    }

    /**
     * 标记已同步
     */
    public void setSync(List<String> ids, String spuId) {
        setSyncX(ids, spuId, 1);
    }

    /**
     * 标记重新操作同步
     */
    public void setUnSync(List<String> ids, String spuId) {
        setSyncX(ids, spuId, 0);
    }
}
