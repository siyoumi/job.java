package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssClass;
import com.siyoumi.app.mapper.EssClassMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_class
@Service
public class EssClassService
        extends ServiceImplBase<EssClassMapper, EssClass> {
    static public EssClassService getBean() {
        return XSpringContext.getBean(EssClassService.class);
    }

    protected boolean softDelete() {
        return true;
    }
}
