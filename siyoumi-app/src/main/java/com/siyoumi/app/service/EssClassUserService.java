package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssClassUser;
import com.siyoumi.app.mapper.EssClassUserMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_class_user
@Service
public class EssClassUserService
        extends ServiceImplBase<EssClassUserMapper, EssClassUser>{
    static public EssClassUserService getBean(){
        return XSpringContext.getBean(EssClassUserService.class);
    }
}
