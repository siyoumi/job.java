package com.siyoumi.app.service;

import com.siyoumi.app.entity.RedbookData;
import com.siyoumi.app.mapper.RedbookDataMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_redbook_data
@Service
public class RedbookDataService
        extends ServiceImplBase<RedbookDataMapper, RedbookData>{
    static public RedbookDataService getBean(){
        return XSpringContext.getBean(RedbookDataService.class);
    }
}
