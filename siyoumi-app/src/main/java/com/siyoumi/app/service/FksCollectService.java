package com.siyoumi.app.service;

import com.siyoumi.app.entity.FksCollect;
import com.siyoumi.app.mapper.FksCollectMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fks_collect
@Service
public class FksCollectService
        extends ServiceImplBase<FksCollectMapper, FksCollect>{
    static public FksCollectService getBean(){
        return XSpringContext.getBean(FksCollectService.class);
    }
}
