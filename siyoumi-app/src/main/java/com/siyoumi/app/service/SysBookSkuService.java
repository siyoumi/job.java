package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysBookSku;
import com.siyoumi.app.mapper.SysBookSkuMapper;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.service.ServiceImplBase;
import org.springframework.stereotype.Service;


//t_sys_book_sku
@Service
public class SysBookSkuService
        extends ServiceImplBase<SysBookSkuMapper, SysBookSku> {
    static public SysBookSkuService getBean() {
        return XSpringContext.getBean(SysBookSkuService.class);
    }
}
