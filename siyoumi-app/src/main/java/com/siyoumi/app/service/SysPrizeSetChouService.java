package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysPrizeSetChou;
import com.siyoumi.app.mapper.SysPrizeSetChouMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;


//t_s_prize_set_chou
@Service
public class SysPrizeSetChouService
        extends ServiceImplBase<SysPrizeSetChouMapper, SysPrizeSetChou> {
    static public SysPrizeSetChouService getBean() {
        return XSpringContext.getBean(SysPrizeSetChouService.class);
    }
}
