package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookStore;
import com.siyoumi.app.mapper.BookStoreMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_store
@Service
public class BookStoreService
        extends ServiceImplBase<BookStoreMapper, BookStore> {
    static public BookStoreService getBean() {
        return XSpringContext.getBean(BookStoreService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
