package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysUser;
import com.siyoumi.app.mapper.SysUserMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_sys_user
@Service
public class SysUserService
        extends ServiceImplBase<SysUserMapper, SysUser>{
    static public SysUserService getBean(){
        return XSpringContext.getBean(SysUserService.class);
    }
}
