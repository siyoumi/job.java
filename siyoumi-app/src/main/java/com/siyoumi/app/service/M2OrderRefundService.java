package com.siyoumi.app.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.M2OrderRefund;
import com.siyoumi.app.mapper.M2OrderRefundMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


//t_m2_order_refund
@Service
public class M2OrderRefundService
        extends ServiceImplBase<M2OrderRefundMapper, M2OrderRefund> {
    static public M2OrderRefundService getBean() {
        return XSpringContext.getBean(M2OrderRefundService.class);
    }

    /**
     * 订单是否存在退款订单，审核中、已通过
     *
     * @param itemId
     */
    public M2OrderRefund getEntityByItemId(String itemId) {
        QueryWrapper<M2OrderRefund> query = join();
        query.eq("mref_item_id", itemId)
                .ge("mref_audit_status", 0);
        return first(query);
    }

    /**
     * 更新退款状态
     *
     * @param entity
     * @param refund
     * @param errmsg
     */
    public void updateRefundStatus(M2OrderRefund entity, Integer refund, String errmsg) {
        M2OrderRefund entityRefundUpdate = new M2OrderRefund();
        entityRefundUpdate.setMref_id(entity.getMref_id());
        entityRefundUpdate.setMref_refund(refund);
        //记录错误信息
        if (XStr.hasAnyText(errmsg)) {
            entityRefundUpdate.setMref_refund_errmsg(XStr.maxLen(errmsg, 100));
        }
        entityRefundUpdate.setMref_refund_date(LocalDateTime.now());
        updateById(entityRefundUpdate);
    }
}
