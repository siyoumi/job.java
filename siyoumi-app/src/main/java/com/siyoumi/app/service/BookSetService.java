package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookSet;
import com.siyoumi.app.mapper.BookSetMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_set
@Service
public class BookSetService
        extends ServiceImplBase<BookSetMapper, BookSet> {
    static public BookSetService getBean() {
        return XSpringContext.getBean(BookSetService.class);
    }

    @Override
    protected boolean softDelete() {
        return true;
    }
}
