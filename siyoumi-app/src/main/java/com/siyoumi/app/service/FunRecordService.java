package com.siyoumi.app.service;

import com.siyoumi.app.entity.FunRecord;
import com.siyoumi.app.mapper.FunRecordMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_fun_record
@Service
public class FunRecordService
        extends ServiceImplBase<FunRecordMapper, FunRecord>{
    static public FunRecordService getBean(){
        return XSpringContext.getBean(FunRecordService.class);
    }
}
