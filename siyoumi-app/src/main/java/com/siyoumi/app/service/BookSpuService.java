package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookSpu;
import com.siyoumi.app.mapper.BookSpuMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_spu
@Service
public class BookSpuService
        extends ServiceImplBase<BookSpuMapper, BookSpu> {
    static public BookSpuService getBean() {
        return XSpringContext.getBean(BookSpuService.class);
    }


    @Override
    protected boolean softDelete() {
        return true;
    }
}
