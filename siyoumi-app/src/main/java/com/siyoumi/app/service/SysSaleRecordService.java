package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysSaleRecord;
import com.siyoumi.app.mapper.SysSaleRecordMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_s_sale_record
@Service
public class SysSaleRecordService
        extends ServiceImplBase<SysSaleRecordMapper, SysSaleRecord>{
    static public SysSaleRecordService getBean(){
        return XSpringContext.getBean(SysSaleRecordService.class);
    }
}
