package com.siyoumi.app.service;

import com.siyoumi.app.entity.IotDevice;
import com.siyoumi.app.mapper.IotDeviceMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_iot_device
@Service
public class IotDeviceService
        extends ServiceImplBase<IotDeviceMapper, IotDevice>{
    static public IotDeviceService getBean(){
        return XSpringContext.getBean(IotDeviceService.class);
    }
}
