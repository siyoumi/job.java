package com.siyoumi.app.service;

import com.siyoumi.app.entity.BookStoreTxt;
import com.siyoumi.app.mapper.BookStoreTxtMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_book_store_txt
@Service
public class BookStoreTxtService
        extends ServiceImplBase<BookStoreTxtMapper, BookStoreTxt>{
    static public BookStoreTxtService getBean(){
        return XSpringContext.getBean(BookStoreTxtService.class);
    }
}
