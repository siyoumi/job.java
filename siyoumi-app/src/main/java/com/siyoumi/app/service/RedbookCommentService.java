package com.siyoumi.app.service;

import com.siyoumi.app.entity.RedbookComment;
import com.siyoumi.app.mapper.RedbookCommentMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_redbook_comment
@Service
public class RedbookCommentService
        extends ServiceImplBase<RedbookCommentMapper, RedbookComment>{
    static public RedbookCommentService getBean(){
        return XSpringContext.getBean(RedbookCommentService.class);
    }
}
