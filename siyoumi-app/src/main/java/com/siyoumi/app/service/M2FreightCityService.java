package com.siyoumi.app.service;

import com.siyoumi.app.entity.M2FreightCity;
import com.siyoumi.app.mapper.M2FreightCityMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_m2_freight_city
@Service
public class M2FreightCityService
        extends ServiceImplBase<M2FreightCityMapper, M2FreightCity>{
    static public M2FreightCityService getBean(){
        return XSpringContext.getBean(M2FreightCityService.class);
    }
}
