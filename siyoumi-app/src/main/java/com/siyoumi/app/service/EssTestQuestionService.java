package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssTestQuestion;
import com.siyoumi.app.mapper.EssTestQuestionMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_test_question
@Service
public class EssTestQuestionService
        extends ServiceImplBase<EssTestQuestionMapper, EssTestQuestion>{
    static public EssTestQuestionService getBean(){
        return XSpringContext.getBean(EssTestQuestionService.class);
    }
}
