package com.siyoumi.app.service;

import com.siyoumi.app.entity.EssFile;
import com.siyoumi.app.mapper.EssFileMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_ess_file
@Service
public class EssFileService
        extends ServiceImplBase<EssFileMapper, EssFile> {
    static public EssFileService getBean() {
        return XSpringContext.getBean(EssFileService.class);
    }
}
