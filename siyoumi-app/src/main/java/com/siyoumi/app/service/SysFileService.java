package com.siyoumi.app.service;

import com.siyoumi.app.entity.SysFile;
import com.siyoumi.app.mapper.SysFileMapper;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.component.XSpringContext;
import org.springframework.stereotype.Service;


//t_sys_file
@Service
public class SysFileService
        extends ServiceImplBase<SysFileMapper, SysFile>{
    static public SysFileService getBean(){
        return XSpringContext.getBean(SysFileService.class);
    }
}
