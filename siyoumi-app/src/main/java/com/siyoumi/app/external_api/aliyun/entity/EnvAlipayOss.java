package com.siyoumi.app.external_api.aliyun.entity;

import com.siyoumi.component.XSpringContext;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "alipay-oss")
public class EnvAlipayOss {
    String access_key_id;
    String access_key_secret;
    String bucket_name;
    //Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
    String endpoint;
    String dir_name = "app"; //目录

    public static EnvAlipayOss getBean() {
        return XSpringContext.getBean(EnvAlipayOss.class);
    }
}
