package com.siyoumi.app.external_api.aliyun;

import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.dysmsapi20170525.models.SendSmsResponseBody;
import com.aliyun.tea.TeaException;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

//阿里云api
@Data
@Slf4j
public class ApiAliYun
        extends ApiAliYunOss {
    @SneakyThrows
    protected Config getConfig(String endpoint) {
        // 阿里云账号AccessKey拥有所有API的访问权限，建议您使用RAM用户进行API访问或日常运维。
        // 强烈建议不要把AccessKey ID和AccessKey Secret保存到工程代码里，否则可能导致AccessKey泄露，威胁您账号下所有资源的安全。
        // 本示例通过阿里云Credentials工具从环境变量中读取AccessKey，来实现API访问的身份验证。
        //com.aliyun.credentials.Client credentialClient = new com.aliyun.credentials.Client();
        Config config = new Config();
        config.setAccessKeyId(getConfig().getAccess_key_id());
        config.setAccessKeySecret(getConfig().getAccess_key_secret());
        //config.setCredential(credentialClient);
        config.setEndpoint(endpoint);

        return config;
    }

    public XReturn sendSms(String phone, String SignName, String templateCode, Map<String, String> templateJson) {
        String json = "";
        if (templateJson != null) {
            json = XJson.toJSONString(templateJson);
        }

        return sendSms(phone, SignName, templateCode, json);
    }

    /**
     * 发短信
     *
     * @param phone
     * @param SignName
     * @param templateCode
     * @param templateJson
     * @throws Exception
     */
    @SneakyThrows
    public XReturn sendSms(String phone, String SignName, String templateCode, String templateJson) {
        Config config = getConfig("dysmsapi.aliyuncs.com");
        com.aliyun.dysmsapi20170525.Client client = new com.aliyun.dysmsapi20170525.Client(config);
        SendSmsRequest sendSmsRequest = new SendSmsRequest();
        sendSmsRequest.setPhoneNumbers(phone);
        sendSmsRequest.setSignName(SignName);
        sendSmsRequest.setTemplateCode(templateCode);

        if (XStr.hasAnyText(templateJson)) {
            sendSmsRequest.setTemplateParam(templateJson); //{"name":"张三","number":"1390000****"}
        }

        XReturn r = null;
        try {
            // 复制代码运行请自行打印 API 的返回值
            SendSmsResponse response = client.sendSmsWithOptions(sendSmsRequest, new RuntimeOptions());
            SendSmsResponseBody body = response.getBody();
            r = XReturn.getR(20500, body.getMessage());
            r.setData("code", body.getCode());
            if ("OK".equals(body.getCode())) {
                r.setErrCode(0);
                r.setData("BizId", body.getBizId());
            }
        } catch (Exception ex) {
            TeaException error = new TeaException(ex.getMessage(), ex);
            String recommend = "";
            if (error.getData() != null) {
                recommend = (String) error.getData().get("Recommend");
            }

            r = EnumSys.SYS.getR(ex.getMessage());
            r.setData("recommend", recommend);
            r.setData("message", error.getMessage());
        }

        return r;
    }
}

