package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysItemData;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysItemDataBase
        extends EntityBase<SysItemData>
        implements Serializable {
    @Override
    public String prefix() {
        return "idata_";
    }

    static public String table() {
        return "wx_app.t_sys_item_data";
    }

    static public String tableKey() {
        return "idata_id";
    }


    @TableId(value = "idata_id", type = IdType.INPUT)
    private String idata_id;
    private String idata_x_id;
    private LocalDateTime idata_create_date;
    private LocalDateTime idata_update_date;
    private String idata_item_id;
    private String idata_app_id;
    private String idata_key;
    private String idata_data;

}
