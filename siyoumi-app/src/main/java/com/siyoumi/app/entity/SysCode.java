package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysCodeBase;
import com.siyoumi.util.XDate;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_code", schema = "wx_app")
public class SysCode
        extends SysCodeBase {
    public Boolean expire() {
        return getCode_vaild_date_end().isBefore(XDate.now());
    }

    public Boolean use() {
        return getCode_use() == 1;
    }
}
