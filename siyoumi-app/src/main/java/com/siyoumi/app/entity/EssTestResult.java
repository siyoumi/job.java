package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.EssTestResultBase;
import com.siyoumi.util.XDate;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Duration;


@Data
@Accessors(chain = true)
@TableName(value = "t_ess_test_result", schema = "wx_app_x")
public class EssTestResult
        extends EssTestResultBase {
    /**
     * 提交剩余X秒
     */
    public long submitLeftSecond() {
        Duration between = XDate.between(XDate.now(), getEtres_submit_date_end());
        long seconds = between.getSeconds();
        if (seconds < 0) {
            seconds = 0;
        }
        return seconds;
    }
}
