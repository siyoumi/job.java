package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FunMallGroup;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FunMallGroupBase
        extends EntityBase<FunMallGroup>
        implements Serializable
{
    @Override
    public String prefix(){
        return "fgroup_";
    }

    static public String table() {
        return "wx_app.t_fun_mall_group";
    }

    static public String tableKey() {
        return "fgroup_id";
    }


	@TableId(value = "fgroup_id", type = IdType.INPUT)
	private String fgroup_id;
	private String fgroup_x_id;
	private LocalDateTime fgroup_create_date;
	private LocalDateTime fgroup_update_date;
	private String fgroup_acc_id;
	private String fgroup_name;
	private String fgroup_pic;
	private Integer fgroup_order;

}
