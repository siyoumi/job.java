package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.WxMsgQueue;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class WxMsgQueueBase
        extends EntityBase<WxMsgQueue>
        implements Serializable
{
    @Override
    public String prefix(){
        return "msgq_";
    }

    static public String table() {
        return "wx_app.t_wx_msg_queue";
    }

    static public String tableKey() {
        return "msgq_id";
    }


	@TableId(value = "msgq_id", type = IdType.INPUT)
	private Long msgq_id;
	private String msgq_x_id;
	private LocalDateTime msgq_create_date;
	private LocalDateTime msgq_update_date;
	private String msgq_app_id;
	private Integer msgq_type;
	private String msgq_openid;
	private Integer msgq_send_status;
	private String msgq_send_status_retrun;
	private String msgq_send_content;
	private LocalDateTime msgq_send_date;

}
