package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.WxReply;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class WxReplyBase
        extends EntityBase<WxReply>
        implements Serializable
{
    @Override
    public String prefix(){
        return "reply_";
    }

    static public String table() {
        return "wx_app.t_wx_reply";
    }

    static public String tableKey() {
        return "reply_id";
    }


	@TableId(value = "reply_id", type = IdType.INPUT)
	private String reply_id;
	private String reply_x_id;
	private LocalDateTime reply_create_date;
	private LocalDateTime reply_update_date;
	private String reply_app_id;
	private String reply_type;
	private String reply_key;
	private String reply_name;
	private String reply_title;
	private String reply_pic;
	private String reply_desc;
	private String reply_app_url;
	private String reply_pic_wxid;
	private String reply_wxapp_appid;

}
