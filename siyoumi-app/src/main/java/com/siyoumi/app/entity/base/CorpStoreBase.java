package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.CorpStore;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class CorpStoreBase
        extends EntityBase<CorpStore>
        implements Serializable {
    @Override
    public String prefix() {
        return "cstore_";
    }

    static public String table() {
        return "wx_app_x.t_corp_store";
    }

    static public String tableKey() {
        return "cstore_id";
    }


    @TableId(value = "cstore_id", type = IdType.INPUT)
    private String cstore_id;
    private String cstore_x_id;
    private LocalDateTime cstore_create_date;
    private LocalDateTime cstore_update_date;
    private String cstore_name;
    private String cstore_openid;
    private String cstore_wifi_ssid;
    private String cstore_wifi_pwd;
    private String cstore_wifi_bssid;
    private String cstore_pic_h5;
    private LocalDateTime cstore_expire_date;
    private Integer cstore_enable;
    private Long cstore_uv;
    private LocalDateTime cstore_enable_date;
    private Integer cstore_order;

}
