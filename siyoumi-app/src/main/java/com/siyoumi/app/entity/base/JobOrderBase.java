package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.JobOrder;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class JobOrderBase
        extends EntityBase<JobOrder>
        implements Serializable {
    @Override
    public String prefix() {
        return "jorder_";
    }

    static public String table() {
        return "wx_app_x.t_job_order";
    }

    static public String tableKey() {
        return "jorder_id";
    }


    @TableId(value = "jorder_id", type = IdType.INPUT)
    private String jorder_id;
    private String jorder_x_id;
    private LocalDateTime jorder_create_date;
    private LocalDateTime jorder_update_date;
    private String jorder_openid;
    private Integer jorder_type;
    private String jorder_name;
    private String jorder_banner;
    private String jorder_desc;
    private BigDecimal jorder_price_total;
    private BigDecimal jorder_price_sales;
    private Integer jorder_state;
    private String jorder_src;
    private Long jorder_del;

}
