package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssUser;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssUserBase
        extends EntityBase<EssUser>
        implements Serializable
{
    @Override
    public String prefix(){
        return "euser_";
    }

    static public String table() {
        return "wx_app_x.t_ess_user";
    }

    static public String tableKey() {
        return "euser_id";
    }


	@TableId(value = "euser_id", type = IdType.INPUT)
	private String euser_id;
	private String euser_x_id;
	private LocalDateTime euser_create_date;
	private LocalDateTime euser_update_date;
	private Integer euser_type;
	private Integer euser_sex;
	private Integer euser_set_class;
	private String euser_speciality;
	private LocalDateTime euser_school_date;
	private String euser_address;
	private String euser_contact;
	private Long euser_model_count;
	private Long euser_model_fun;
	private Long euser_file_count;
	private Long euser_file_fun;
	private Long euser_test0_count;
	private Long euser_test0_fun;
	private Long euser_test1_count;
	private Long euser_test1_fun;

}
