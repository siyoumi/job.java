package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysMoneyOptionBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_money_option", schema = "wx_app")
public class SysMoneyOption
        extends SysMoneyOptionBase
{

}
