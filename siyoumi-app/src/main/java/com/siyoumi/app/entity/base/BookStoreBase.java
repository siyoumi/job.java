package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookStore;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookStoreBase
        extends EntityBase<BookStore>
        implements Serializable {
    @Override
    public String prefix() {
        return "bstore_";
    }

    static public String table() {
        return "wx_app_x.t_book_store";
    }

    static public String tableKey() {
        return "bstore_id";
    }


    @TableId(value = "bstore_id", type = IdType.INPUT)
    private String bstore_id;
    private String bstore_x_id;
    private String bstore_acc_id;
    private LocalDateTime bstore_create_date;
    private LocalDateTime bstore_update_date;
    private String bstore_group_id;
    private String bstore_name;
    private String bstore_title;
    private Integer bstore_deposit_enable;
    private BigDecimal bstore_deposit_rate;
    private BigDecimal bstore_app_rate;
    private BigDecimal bstore_sales0_rate;
    private BigDecimal bstore_sales1_rate;
    private String bstore_banner;
    private String bstore_point_x;
    private String bstore_point_y;
    private String bstore_province;
    private String bstore_city;
    private String bstore_district;
    private String bstore_address;
    private String bstore_tag;
    private Long bstore_altitude;
    private Integer bstore_hot;
    private Integer bstore_foreign;
    private BigDecimal bstore_day1_price;
    private String bstore_set_id;
    private Long bstore_del;
    private Integer bstore_enable;
    private String bstore_refund_rule;

}
