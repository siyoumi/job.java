package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.WxSubscribemsgBase;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@TableName(value = "t_wx_subscribemsg", schema = "wx_app")
public class WxSubscribemsg
        extends WxSubscribemsgBase {
    private String wxsg_uix;
    @NotBlank
    @Size(max = 50)
    private String wxsg_name;
    private String wxsg_template_id;
    private String wxsg_content;
    @NotBlank
    @Size(max = 200)
    private String wxsg_page;
    private String wxsg_miniprogram_state;

}
