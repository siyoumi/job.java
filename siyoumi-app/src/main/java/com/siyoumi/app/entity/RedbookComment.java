package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.RedbookCommentBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_redbook_comment", schema = "wx_app")
public class RedbookComment
        extends RedbookCommentBase
{

}
