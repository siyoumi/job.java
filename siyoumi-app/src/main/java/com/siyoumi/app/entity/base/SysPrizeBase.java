package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysPrize;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class SysPrizeBase
        extends EntityBase<SysPrize>
        implements Serializable
{
    @Override
    public String prefix(){
        return "prize_";
    }

    static public String table() {
        return "wx_app.t_s_prize";
    }

    static public String tableKey() {
        return "prize_id";
    }


	@TableId(value = "prize_id", type = IdType.INPUT)
	private String prize_id;
	private String prize_x_id;
	private LocalDateTime prize_create_date;
	private LocalDateTime prize_update_date;
	private String prize_openid;
	private String prize_set_id;
	private String prize_name;
	private String prize_type;
	private String prize_key;
	private String prize_app_id;
	private String prize_id_src;
	private BigDecimal prize_fun;
	private String prize_pic;
	private String prize_desc;
	private String prize_code;
	private LocalDateTime prize_begin_date;
	private LocalDateTime prize_end_date;
	private String prize_use_type;
	private Long prize_use;
	private LocalDateTime prize_use_date;
	private String prize_use_uix;
	private String prize_sheet_id;
	private String prize_use_json;
	private String prize_user_name;
	private String prize_user_phone;
	private String prize_user_address;
	private String prize_deliver_no;
	private LocalDateTime prize_deliver_date;
	private String prize_str_00;
	private Long prize_del;

}
