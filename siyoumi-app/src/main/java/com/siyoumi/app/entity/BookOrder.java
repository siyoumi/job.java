package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookOrderBase;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@TableName(value = "t_book_order", schema = "wx_app_x")
public class BookOrder
        extends BookOrderBase {
    public BigDecimal matchOriginalPrice()
    {
        return getBorder_price_sku()
                .add(getBorder_price_sku_append())
                .add(getBorder_price_set())
                .add(getBorder_price_set_append());
    }

    /**
     * 计算最终金额
     */
    public BigDecimal matchFinalPrice() {
        return getBorder_price_original()
                .subtract(getBorder_price_down_fun());
    }
}
