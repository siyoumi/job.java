package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssTestResultItem;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssTestResultItemBase
        extends EntityBase<EssTestResultItem>
        implements Serializable {
    @Override
    public String prefix() {
        return "etri_";
    }

    static public String table() {
        return "wx_app_x.t_ess_test_result_item";
    }

    static public String tableKey() {
        return "etri_id";
    }


    @TableId(value = "etri_id", type = IdType.INPUT)
    private String etri_id;
    private String etri_x_id;
    private LocalDateTime etri_create_date;
    private LocalDateTime etri_update_date;
    private String etri_test_id;
    private String etri_result_id;
    private String etri_etques_id;
    private String etri_question_id;
    private Long etri_test_fun;
    private String etri_answer;
    private Integer etri_answer_state;

}
