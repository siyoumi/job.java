package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.ActDataChou;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class ActDataChouBase
        extends EntityBase<ActDataChou>
        implements Serializable
{
    @Override
    public String prefix(){
        return "adc_";
    }

    static public String table() {
        return "wx_app.t_act_data_chou";
    }

    static public String tableKey() {
        return "adc_id";
    }


	@TableId(value = "adc_id", type = IdType.INPUT)
	private String adc_id;
	private String adc_x_id;
	private LocalDateTime adc_create_date;
	private LocalDateTime adc_update_date;
	private String adc_actset_id;
	private String adc_openid;
	private String adc_uix;
	private String adc_win_uix;
	private String adc_prize_id;
	private String adc_str_00;
	private Integer adc_int_00;

}
