package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookBase;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;


@Data
@Accessors(chain = true)
@TableName(value = "t_book", schema = "wx_app")
public class SysBook
        extends BookBase
{
    public boolean expire() {
        if (getBook_date_end().isBefore(LocalDateTime.now())) {
            return true;
        }
        return false;
    }
}
