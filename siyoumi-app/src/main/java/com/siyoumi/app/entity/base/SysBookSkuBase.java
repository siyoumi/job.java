package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysBookSku;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysBookSkuBase
        extends EntityBase<SysBookSku>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bsku_";
    }

    static public String table() {
        return "wx_app.t_sys_book_sku";
    }

    static public String tableKey() {
        return "bsku_id";
    }


	@TableId(value = "bsku_id", type = IdType.INPUT)
	private String bsku_id;
	private String bsku_x_id;
	private String bsku_acc_id;
	private LocalDateTime bsku_create_date;
	private LocalDateTime bsku_update_date;
	private String bsku_book_id;
	private String bsku_name;
	private Long bsku_before_time;
	private Integer bsku_hide;
	private Integer bsku_order;
	private Long bsku_del;

}
