package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FksPosition;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FksPositionBase
        extends EntityBase<FksPosition>
        implements Serializable
{
    @Override
    public String prefix(){
        return "fposi_";
    }

    static public String table() {
        return "wx_app_x.t_fks_position";
    }

    static public String tableKey() {
        return "fposi_id";
    }


	@TableId(value = "fposi_id", type = IdType.INPUT)
	private String fposi_id;
	private String fposi_x_id;
	private LocalDateTime fposi_create_date;
	private LocalDateTime fposi_update_date;
    private String fposi_acc_id;
	private String fposi_name;
	private Integer fposi_level;
	private Long fposi_del;

}
