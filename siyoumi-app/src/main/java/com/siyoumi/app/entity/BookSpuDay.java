package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookSpuDayBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_book_spu_day", schema = "wx_app_x")
public class BookSpuDay
        extends BookSpuDayBase {
    /**
     * 剩余库存
     */
    public Long leftStock(Long spuDayStock) {
        long totalStock = getBspud_stock_add() + spuDayStock;
        if (totalStock <= 0) {
            totalStock = 0;
        }

        long leftStock = totalStock - getBspud_stock_use();
        if (leftStock < 0) {
            leftStock = 0;
        }
        return leftStock;
    }
}
