package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2OrderItem;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2OrderItemBase
        extends EntityBase<M2OrderItem>
        implements Serializable
{
    @Override
    public String prefix(){
        return "moitem_";
    }

    static public String table() {
        return "wx_app.t_m2_order_item";
    }

    static public String tableKey() {
        return "moitem_id";
    }


	@TableId(value = "moitem_id", type = IdType.INPUT)
	private String moitem_id;
	private String moitem_x_id;
	private LocalDateTime moitem_create_date;
	private LocalDateTime moitem_update_date;
	private String moitem_app_id;
	private String moitem_morder_id;
	private String moitem_openid;
	private String moitem_spu_id;
	private String moitem_sku_id;
	private Long moitem_count;
	private BigDecimal moitem_sku_price;
	private String moitem_spu_name;
	private String moitem_spu_pic;
	private String moitem_sku_name;

}
