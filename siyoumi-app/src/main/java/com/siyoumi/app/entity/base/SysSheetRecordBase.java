package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysSheetRecord;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysSheetRecordBase
        extends EntityBase<SysSheetRecord>
        implements Serializable {
    @Override
    public String prefix() {
        return "scord_";
    }

    static public String table() {
        return "wx_app.t_a_sheet_record";
    }

    static public String tableKey() {
        return "scord_id";
    }


    @TableId(value = "scord_id", type = IdType.INPUT)
    private String scord_id;
    private String scord_x_id;
    private LocalDateTime scord_create_date;
    private LocalDateTime scord_update_date;
    private String scord_sheet_id;
    private String scord_key;
    private String scord_openid;
    private String scord_str_00;
    private String scord_str_01;
    private String scord_str_02;
    private String scord_str_03;
    private String scord_str_04;
    private String scord_str_05;
    private String scord_txt_00;
    private String scord_txt_01;
    private String scord_txt_02;
    private String scord_txt_03;

}
