package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.WxUserInfo;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class WxUserInfoBase
        extends EntityBase<WxUserInfo>
        implements Serializable
{
    @Override
    public String prefix(){
        return "winfo_";
    }

    static public String table() {
        return "wx_app.t_wx_user_a_info";
    }

    static public String tableKey() {
        return "winfo_id";
    }


	@TableId(value = "winfo_id", type = IdType.INPUT)
	private Long winfo_id;
	private String winfo_x_id;
	private LocalDateTime winfo_create_date;
	private LocalDateTime winfo_update_date;
	private String winfo_openid;
	private String winfo_phone;
	private String winfo_name;
	private String winfo_sex;
	private Integer winfo_age;
	private String winfo_birthday;
	private String winfo_province;
	private String winfo_city;
	private String winfo_district;
	private String winfo_address;
	private String winfo_wechat;
	private String winfo_headimg;
	private String winfo_str_00;
	private String winfo_str_01;
	private String winfo_str_02;
	private Integer winfo_int_00;
	private Long winfo_memlv_id;
	private Integer winfo_memlv_num;
	private LocalDateTime winfo_date_00;

}
