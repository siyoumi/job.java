package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysBookSkuDay;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookSkuDayBase
        extends EntityBase<SysBookSkuDay>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bday_";
    }

    static public String table() {
        return "wx_app.t_book_sku_day";
    }

    static public String tableKey() {
        return "bday_id";
    }


	@TableId(value = "bday_id", type = IdType.INPUT)
	private String bday_id;
	private String bday_x_id;
	private String bday_acc_id;
	private LocalDateTime bday_create_date;
	private LocalDateTime bday_update_date;
	private String bday_book_id;
	private String bday_sku_id;
	private LocalDateTime bday_date;
	private Integer bday_before_enable;
	private Long bday_before_time;
	private Integer bday_append_max;
	private Long bday_del;

}
