package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.WxMsgQueueBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_wx_msg_queue", schema = "wx_app")
public class WxMsgQueue
        extends WxMsgQueueBase
{
}
