package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.ActData;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class ActDataBase
        extends EntityBase<ActData>
        implements Serializable
{
    @Override
    public String prefix(){
        return "adata_";
    }

    static public String table() {
        return "wx_app.t_act_data";
    }

    static public String tableKey() {
        return "adata_id";
    }


	@TableId(value = "adata_id", type = IdType.INPUT)
	private String adata_id;
	private String adata_x_id;
	private LocalDateTime adata_create_date;
	private LocalDateTime adata_update_date;
	private String adata_actset_id;
	private String adata_uix;
	private String adata_openid;
	private String adata_str_00;
	private String adata_str_01;
	private Integer adata_int_00;
	private Integer adata_int_01;

}
