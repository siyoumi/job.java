package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FksCity;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FksCityBase
        extends EntityBase<FksCity>
        implements Serializable {
    @Override
    public String prefix() {
        return "fcity_";
    }

    static public String table() {
        return "wx_app_x.t_fks_city";
    }

    static public String tableKey() {
        return "fcity_id";
    }

    @TableId(value = "fcity_id", type = IdType.INPUT)
    private Long fcity_id;
    private String fcity_x_id;
    private LocalDateTime fcity_create_date;
    private LocalDateTime fcity_update_date;
    private String fcity_acc_id;
    private String fcity_name;
    private String fcity_pic;
    private Integer fcity_order;

}
