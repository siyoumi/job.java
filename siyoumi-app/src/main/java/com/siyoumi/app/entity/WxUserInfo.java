package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.WxUserInfoBase;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@TableName(value = "t_wx_user_a_info", schema = "wx_app")
public class WxUserInfo
        extends WxUserInfoBase {
}
