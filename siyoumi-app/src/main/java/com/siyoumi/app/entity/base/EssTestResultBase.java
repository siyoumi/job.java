package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssTestResult;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssTestResultBase
        extends EntityBase<EssTestResult>
        implements Serializable {
    @Override
    public String prefix() {
        return "etres_";
    }

    static public String table() {
        return "wx_app_x.t_ess_test_result";
    }

    static public String tableKey() {
        return "etres_id";
    }


    @TableId(value = "etres_id", type = IdType.INPUT)
    private String etres_id;
    private String etres_x_id;
    private LocalDateTime etres_create_date;
    private LocalDateTime etres_update_date;
    private String etres_test_id;
    private String etres_uid;
    private Integer etres_submit;
    private LocalDateTime etres_submit_date;
    private LocalDateTime etres_submit_date_end;
    private Integer etres_submit_minute;
    private Integer etres_end;
    private Long etres_test_fun;
    private String etres_class_id;
    private Integer etres_send_fun;
}
