package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.IotDeviceLogBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_iot_device_log", schema = "wx_app")
public class IotDeviceLog
        extends IotDeviceLogBase
{

}
