package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssStudyStudent;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssStudyStudentBase
        extends EntityBase<EssStudyStudent>
        implements Serializable
{
    @Override
    public String prefix(){
        return "esstu_";
    }

    static public String table() {
        return "wx_app_x.t_ess_study_student";
    }

    static public String tableKey() {
        return "esstu_id";
    }


	@TableId(value = "esstu_id", type = IdType.INPUT)
	private String esstu_id;
	private String esstu_x_id;
	private LocalDateTime esstu_create_date;
	private LocalDateTime esstu_update_date;
	private String esstu_study_id;
	private String esstu_uid;

}
