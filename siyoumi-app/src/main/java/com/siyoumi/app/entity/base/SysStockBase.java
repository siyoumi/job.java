package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysStock;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class SysStockBase
        extends EntityBase<SysStock>
        implements Serializable
{
    @Override
    public String prefix(){
        return "stock_";
    }

    static public String table() {
        return "wx_app.t_s_stock";
    }

    static public String tableKey() {
        return "stock_id";
    }


	@TableId(value = "stock_id", type = IdType.INPUT)
	private Long stock_id;
	private String stock_x_id;
	private LocalDateTime stock_create_date;
	private LocalDateTime stock_update_date;
	private String stock_app_id;
	private String stock_id_src;
	private Long stock_count_left;
	private Long stock_count_use;
	private Integer stock_count_use_add;

}
