package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysSaleClient;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysSaleClientBase
        extends EntityBase<SysSaleClient>
        implements Serializable
{
    @Override
    public String prefix(){
        return "sacl_";
    }

    static public String table() {
        return "wx_app.t_s_sale_client";
    }

    static public String tableKey() {
        return "sacl_id";
    }


	@TableId(value = "sacl_id", type = IdType.INPUT)
	private String sacl_id;
	private String sacl_x_id;
	private String sacl_acc_id;
	private LocalDateTime sacl_create_date;
	private LocalDateTime sacl_update_date;
	private String sacl_sale_uid;
	private String sacl_client_uid;

}
