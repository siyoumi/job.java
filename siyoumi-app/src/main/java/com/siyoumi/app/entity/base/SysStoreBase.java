package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysStore;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysStoreBase
        extends EntityBase<SysStore>
        implements Serializable
{
    @Override
    public String prefix(){
        return "store_";
    }

    static public String table() {
        return "wx_app.t_sys_store";
    }

    static public String tableKey() {
        return "store_id";
    }


	@TableId(value = "store_id", type = IdType.INPUT)
	private String store_id;
	private String store_x_id;
	private String store_acc_id;
	private LocalDateTime store_create_date;
	private LocalDateTime store_update_date;
	private String store_name;
	private Integer store_order;
	private Integer store_del;

}
