package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookOrder;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookOrderBase
        extends EntityBase<BookOrder>
        implements Serializable {
    @Override
    public String prefix() {
        return "border_";
    }

    static public String table() {
        return "wx_app_x.t_book_order";
    }

    static public String tableKey() {
        return "border_id";
    }


    @TableId(value = "border_id", type = IdType.INPUT)
    private String border_id;
    private String border_x_id;
    private String border_acc_id;
    private LocalDateTime border_create_date;
    private LocalDateTime border_update_date;
    private String border_store_id;
    private String border_spu_id;
    private String border_sku_id;
    private String border_uid_order;
    private String border_uid;
    private String border_uid_pay;
    private String border_user_name;
    private String border_user_phone;
    private Integer border_confirm;
    private LocalDateTime border_confirm_date;
    private String border_confirm_desc;
    private LocalDateTime border_date_begin;
    private LocalDateTime border_date_end;
    private Integer border_day;
    private BigDecimal border_sku_day_price;
    private String border_set_id;
    private String border_set_name;
    private BigDecimal border_set_day_price;
    private Long border_spu_count;
    private Integer border_sku_user_count;
    private Integer border_set_user_count;
    private BigDecimal border_price_sku;
    private BigDecimal border_price_sku_append;
    private BigDecimal border_price_set;
    private BigDecimal border_price_set_append;
    private BigDecimal border_price_original;
    private BigDecimal border_price_down_fun;
    private BigDecimal border_price_final;
    private Integer border_pay_deposit;
    private BigDecimal border_price_pay;
    private Integer border_enable;
    private Integer border_day_final;
    private LocalDateTime border_date_end_final;
    private Integer border_checkin;
    private LocalDateTime border_checkin_date;
    private Integer border_checkout;
    private LocalDateTime border_checkout_date;
    private Integer border_refund;
    private LocalDateTime border_refund_date;
    private String border_give_uid;
    private Long border_give_fun;
    private String border_share_sales_uid;
    private Integer border_share_sales_level;
    private BigDecimal border_price_share_final;
    private BigDecimal border_share_sales_rate;
    private BigDecimal border_share_sales_price;
    private BigDecimal border_share_app_rate;
    private BigDecimal border_share_app_price;
    private BigDecimal border_share_store_price;
    private Integer border_handle;
    private LocalDateTime border_handle_date;
    private Integer border_status;
    private String border_desc;

}
