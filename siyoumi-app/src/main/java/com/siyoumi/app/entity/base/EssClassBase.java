package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssClass;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssClassBase
        extends EntityBase<EssClass>
        implements Serializable {
    @Override
    public String prefix() {
        return "eclass_";
    }

    static public String table() {
        return "wx_app_x.t_ess_class";
    }

    static public String tableKey() {
        return "eclass_id";
    }


    @TableId(value = "eclass_id", type = IdType.INPUT)
    private String eclass_id;
    private String eclass_x_id;
    private LocalDateTime eclass_create_date;
    private LocalDateTime eclass_update_date;
    private String eclass_acc_id;
    private String eclass_name;
    private Long eclass_student_total;
    private Long eclass_teacher_total;
    private Long eclass_del;
    private Integer eclass_order;

}
