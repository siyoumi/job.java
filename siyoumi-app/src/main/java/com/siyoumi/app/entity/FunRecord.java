package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.FunRecordBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_fun_record", schema = "wx_app")
public class FunRecord
        extends FunRecordBase
{

}
