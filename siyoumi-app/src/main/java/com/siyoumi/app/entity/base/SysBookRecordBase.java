package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysBookRecord;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysBookRecordBase
        extends EntityBase<SysBookRecord>
        implements Serializable
{
    @Override
    public String prefix(){
        return "brecord_";
    }

    static public String table() {
        return "wx_app.t_sys_book_record";
    }

    static public String tableKey() {
        return "brecord_id";
    }


	@TableId(value = "brecord_id", type = IdType.INPUT)
	private String brecord_id;
	private String brecord_x_id;
	private String brecord_acc_id;
	private LocalDateTime brecord_create_date;
	private LocalDateTime brecord_update_date;
	private String brecord_book_id;
	private String brecord_bday_id;
	private String brecord_key;
	private String brecord_wxuser_id;
	private String brecord_pid;
	private String brecord_name;
	private String brecord_phone;
	private String brecord_idcard;
	private Integer brecord_cancel;
	private LocalDateTime brecord_cancel_date;
	private Integer brecord_state;
	private LocalDateTime brecord_state_date;
	private Integer brecord_use;
	private LocalDateTime brecord_use_date;

}
