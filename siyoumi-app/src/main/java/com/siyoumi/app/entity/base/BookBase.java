package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysBook;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookBase
        extends EntityBase<SysBook>
        implements Serializable
{
    @Override
    public String prefix(){
        return "book_";
    }

    static public String table() {
        return "wx_app.t_book";
    }

    static public String tableKey() {
        return "book_id";
    }


	@TableId(value = "book_id", type = IdType.INPUT)
	private String book_id;
	private String book_x_id;
	private String book_acc_id;
	private LocalDateTime book_create_date;
	private LocalDateTime book_update_date;
	private String book_name;
	private String book_title;
	private LocalDateTime book_date_begin;
	private LocalDateTime book_date_end;
	private Integer book_sign_total;
	private Integer book_sign_day_total;
	private Integer book_sign_fun_enable;
	private Integer book_sign_fun;
	private Integer book_sign_cancel;
	private Integer book_sign_give_enable;
	private Integer book_sign_give_fun;
	private Integer book_audit_enable;
	private String book_pic;
	private String book_pic_00;
	private String book_info_json;
	private String book_field_json;
	private Integer book_hide;
	private Integer book_order;
	private Long book_del;

}
