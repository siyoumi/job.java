package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysSaleBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_sale", schema = "wx_app")
public class SysSale
        extends SysSaleBase
{

}
