package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookSpu;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookSpuBase
        extends EntityBase<BookSpu>
        implements Serializable {
    @Override
    public String prefix() {
        return "bspu_";
    }

    static public String table() {
        return "wx_app_x.t_book_spu";
    }

    static public String tableKey() {
        return "bspu_id";
    }


    @TableId(value = "bspu_id", type = IdType.INPUT)
    private String bspu_id;
    private String bspu_x_id;
    private String bspu_acc_id;
    private LocalDateTime bspu_create_date;
    private LocalDateTime bspu_update_date;
    private String bspu_store_id;
    private String bspu_name;
    private String bspu_logo;
    private String bspu_banner;
    private Integer bspu_user_count;
    private Integer bspu_area;
    private String bspu_area_balcony;
    private Long bspu_stock_max;
    private BigDecimal bspu_day_price;
    private Integer bspu_food_type;
    private Integer bspu_bed_12;
    private Integer bspu_bed_13;
    private Integer bspu_bed_18;
    private Integer bspu_toilet;
    private Integer bspu_toilet_type;
    private Integer bspu_view_type;
    private Integer bspu_enable;
    private LocalDateTime bspu_enable_date;
    private Integer bspu_del;
    private Integer bspu_order;

}
