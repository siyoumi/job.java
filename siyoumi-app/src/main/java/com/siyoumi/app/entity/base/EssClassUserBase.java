package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssClassUser;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssClassUserBase
        extends EntityBase<EssClassUser>
        implements Serializable
{
    @Override
    public String prefix(){
        return "ecu_";
    }

    static public String table() {
        return "wx_app_x.t_ess_class_user";
    }

    static public String tableKey() {
        return "ecu_id";
    }


	@TableId(value = "ecu_id", type = IdType.INPUT)
	private String ecu_id;
	private String ecu_x_id;
	private LocalDateTime ecu_create_date;
	private LocalDateTime ecu_update_date;
	private String ecu_uid;
	private Integer ecu_user_type;
	private String ecu_class_id;

}
