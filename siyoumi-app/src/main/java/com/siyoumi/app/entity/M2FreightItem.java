package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.M2FreightItemBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_m2_freight_item", schema = "wx_app")
public class M2FreightItem
        extends M2FreightItemBase
{

}
