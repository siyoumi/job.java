package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssQuestion;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssQuestionBase
        extends EntityBase<EssQuestion>
        implements Serializable
{
    @Override
    public String prefix(){
        return "etqu_";
    }

    static public String table() {
        return "wx_app_x.t_ess_question";
    }

    static public String tableKey() {
        return "etqu_id";
    }


	@TableId(value = "etqu_id", type = IdType.INPUT)
	private String etqu_id;
	private String etqu_x_id;
	private LocalDateTime etqu_create_date;
	private LocalDateTime etqu_update_date;
	private String etqu_acc_id;
	private String etqu_module_id;
	private Integer etqu_type;
	private Integer etqu_question_type;
	private String etqu_question;
	private String etqu_question_item;
	private String etqu_answer;
	private Long etqu_fun;
	private Integer etqu_level;
	private Long etqu_answer_total;
	private Long etqu_answer_bingo_total;
	private Integer etqu_order;

}
