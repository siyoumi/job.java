package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.ShUser;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.experimental.FieldNameConstants;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class ShUserBase
        extends EntityBase<ShUser>
        implements Serializable {
    @Override
    public String prefix() {
        return "suser_";
    }

    static public String table() {
        return "wx_app_x.t_sh_user";
    }

    static public String tableKey() {
        return "suser_id";
    }


    @TableId(value = "suser_id", type = IdType.INPUT)
    private Long suser_id;
    private String suser_x_id;
    private LocalDateTime suser_create_date;
    private LocalDateTime suser_update_date;
    private String suser_name;
    private String suser_account;
    private String suser_phone;
    private String suser_key;
    private LocalDateTime suser_key_expire_date;
    private String suser_share_code;
    private Integer suser_type;
    private Integer suser_enable;
    private Integer suser_order;
    private String suser_task_api_url;

}
