package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookStoreTxtBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_book_store_txt", schema = "wx_app_x")
public class BookStoreTxt
        extends BookStoreTxtBase
{

}
