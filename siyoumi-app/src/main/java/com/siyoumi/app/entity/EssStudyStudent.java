package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.EssStudyStudentBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_ess_study_student", schema = "wx_app_x")
public class EssStudyStudent
        extends EssStudyStudentBase
{

}
