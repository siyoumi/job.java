package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysSet;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class SysSetBase
        extends EntityBase<SysSet>
        implements Serializable
{
    @Override
    public String prefix(){
        return "set_";
    }

    static public String table() {
        return "wx_app.t_s_set";
    }

    static public String tableKey() {
        return "set_id";
    }


	@TableId(value = "set_id", type = IdType.INPUT)
	private String set_id;
	private LocalDateTime set_create_date;
	private LocalDateTime set_update_date;
	private String set_app_id;
	private String set_x_id;
	private Integer set_type;
	private String set_name;
	private String set_item_data;
	private Integer set_order;

}
