package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.FunMallPrizeBase;
import com.siyoumi.util.XDate;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_fun_mall_prize", schema = "wx_app")
public class FunMallPrize
        extends FunMallPrizeBase {
    public Boolean expire() {
        if (getFprize_end_date().isBefore(XDate.now())) {
            return true;
        }

        return false;
    }
}
