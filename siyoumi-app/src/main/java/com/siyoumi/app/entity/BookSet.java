package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookSetBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_book_set", schema = "wx_app_x")
public class BookSet
        extends BookSetBase {
    public Boolean enable() {
        return getBset_enable() == 1;
    }
}
