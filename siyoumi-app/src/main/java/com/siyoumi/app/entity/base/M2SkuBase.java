package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2Sku;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2SkuBase
        extends EntityBase<M2Sku>
        implements Serializable
{
    @Override
    public String prefix(){
        return "msku_";
    }

    static public String table() {
        return "wx_app.t_m2_sku";
    }

    static public String tableKey() {
        return "msku_id";
    }


	@TableId(value = "msku_id", type = IdType.INPUT)
	private String msku_id;
	private String msku_x_id;
	private LocalDateTime msku_create_date;
	private LocalDateTime msku_update_date;
	private String msku_spu_id;
	private String msku_attr_sku_00;
	private String msku_attr_sku_01;
	private String msku_attr_sku_02;
	private String msku_name;
	private BigDecimal msku_price;
	private Integer msku_limit_enable;
	private Long msku_limit_max;
	private Integer msku_enable;
	private Long msku_del;

}
