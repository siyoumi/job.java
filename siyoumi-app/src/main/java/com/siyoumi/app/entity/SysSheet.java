package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysSheetBase;
import com.siyoumi.component.XRedis;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_a_sheet", schema = "wx_app")
public class SysSheet
        extends SysSheetBase {
    // 过期
    public Boolean expire() {
        if (XDate.now().isAfter(getSheet_end_date())) {
            return true;
        }
        return false;
    }

    public XReturn validDate() {
        if (XDate.now().isBefore(getSheet_begin_date())) {
            return XReturn.getR(20019, "未开始");
        }

        if (XDate.now().isAfter(getSheet_end_date())) {
            return XReturn.getR(20022, "已结束");
        }

        return XReturn.getR(0);
    }
}
