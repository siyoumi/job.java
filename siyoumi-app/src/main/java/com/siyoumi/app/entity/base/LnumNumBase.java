package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.LnumNum;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class LnumNumBase
        extends EntityBase<LnumNum>
        implements Serializable {
    @Override
    public String prefix() {
        return "lnum_";
    }

    static public String table() {
        return "wx_app.t_lnum_num";
    }

    static public String tableKey() {
        return "lnum_id";
    }


    @TableId(value = "lnum_id", type = IdType.INPUT)
    private String lnum_id;
    private String lnum_x_id;
    private LocalDateTime lnum_create_date;
    private LocalDateTime lnum_update_date;
    private String lnum_key;
    private String lnum_group_id;
    private String lnum_wxuser_id;
    private Long lnum_num;
    private Integer lnum_invaild;
    private String lnum_set_id;

}
