package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysAddressBase;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_address", schema = "wx_app")
public class SysAddress
        extends SysAddressBase {

    private String addr_openid;
    @NotBlank
    @Size(max = 50)
    private String addr_name;
    @NotBlank
    @Size(max = 50)
    private String addr_phone;
    @NotBlank
    @Size(max = 50)
    private String addr_province;
    @NotBlank
    @Size(max = 50)
    private String addr_city;
    @NotBlank
    @Size(max = 50)
    private String addr_district;
    @Size(max = 200)
    private String addr_address;
    @Size(max = 50)
    private String addr_str_00;
    @Size(max = 50)
    private String addr_tag;
    private Long addr_order;

}
