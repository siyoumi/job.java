package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.ActDataLaunchBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_act_data_launch", schema = "wx_app")
public class ActDataLaunch
        extends ActDataLaunchBase
{

}
