package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FunAuctionSet;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FunAuctionSetBase
        extends EntityBase<FunAuctionSet>
        implements Serializable
{
    @Override
    public String prefix(){
        return "faset_";
    }

    static public String table() {
        return "wx_app.t_fun_auction_set";
    }

    static public String tableKey() {
        return "faset_id";
    }


	@TableId(value = "faset_id", type = IdType.INPUT)
	private String faset_id;
	private String faset_x_id;
	private LocalDateTime faset_create_date;
	private LocalDateTime faset_update_date;
	private String faset_acc_id;
	private LocalDateTime faset_date_begin;
	private LocalDateTime faset_date_end;
	private Long faset_fun_start;
	private Long faset_fun_add;
	private Long faset_prize_total;
	private Integer faset_order;

}
