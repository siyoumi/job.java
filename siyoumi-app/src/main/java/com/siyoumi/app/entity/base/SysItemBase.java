package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysItem;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class SysItemBase
        extends EntityBase<SysItem>
        implements Serializable {
    @Override
    public String prefix() {
        return "item_";
    }

    static public String table() {
        return "wx_app.t_sys_item";
    }

    static public String tableKey() {
        return "item_id";
    }


    @TableId(value = "item_id", type = IdType.INPUT)
    private String item_id;
    private String item_x_id;
    private LocalDateTime item_create_date;
    private LocalDateTime item_update_date;
    private String item_app_id;
    private String item_id_src;
    private String item_name;
    private String item_data;
    private Long item_order;
}
