package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssFile;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssFileBase
        extends EntityBase<EssFile>
        implements Serializable
{
    @Override
    public String prefix(){
        return "efile_";
    }

    static public String table() {
        return "wx_app_x.t_ess_file";
    }

    static public String tableKey() {
        return "efile_id";
    }


	@TableId(value = "efile_id", type = IdType.INPUT)
	private String efile_id;
	private String efile_x_id;
	private LocalDateTime efile_create_date;
	private LocalDateTime efile_update_date;
	private String efile_acc_id;
	private String efile_module_id;
	private Integer efile_type_sys;
	private String efile_type;
	private String efile_name;
	private String efile_path;
	private String efile_file_ext;
	private Long efile_file_size;
	private Long efile_collect_count;
	private Integer efile_order;
	private Long efile_del;

}
