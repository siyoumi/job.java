package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FunRecord;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FunRecordBase
        extends EntityBase<FunRecord>
        implements Serializable
{
    @Override
    public String prefix(){
        return "funrec_";
    }

    static public String table() {
        return "wx_app.t_fun_record";
    }

    static public String tableKey() {
        return "funrec_id";
    }


	@TableId(value = "funrec_id", type = IdType.INPUT)
	private String funrec_id;
	private String funrec_x_id;
	private LocalDateTime funrec_create_date;
	private LocalDateTime funrec_update_date;
	private LocalDateTime funrec_expire_date;
	private String funrec_uix;
	private String funrec_id_src;
	private String funrec_app_id;
	private String funrec_uid;
	private Integer funrec_num;
	private Integer funrec_num_left;
	private String funrec_desc;
	private String funrec_str_00;
	private String funrec_str_01;

}
