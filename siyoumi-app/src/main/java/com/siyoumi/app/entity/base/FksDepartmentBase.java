package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FksDepartment;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FksDepartmentBase
        extends EntityBase<FksDepartment>
        implements Serializable
{
    @Override
    public String prefix(){
        return "fdep_";
    }

    static public String table() {
        return "wx_app_x.t_fks_department";
    }

    static public String tableKey() {
        return "fdep_id";
    }


	@TableId(value = "fdep_id", type = IdType.INPUT)
	private String fdep_id;
	private String fdep_x_id;
	private LocalDateTime fdep_create_date;
	private LocalDateTime fdep_update_date;
    private String fdep_acc_id;
	private String fdep_name;
	private Integer fdep_order;
	private Long fdep_del;

}
