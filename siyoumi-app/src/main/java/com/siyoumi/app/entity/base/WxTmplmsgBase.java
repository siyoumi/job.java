package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.WxTmplmsg;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class WxTmplmsgBase
        extends EntityBase<WxTmplmsg>
        implements Serializable
{
    @Override
    public String prefix(){
        return "wxtmplmsg_";
    }

    static public String table() {
        return "wx_app.t_wx_tmplmsg";
    }

    static public String tableKey() {
        return "wxtmplmsg_id";
    }


	@TableId(value = "wxtmplmsg_id", type = IdType.INPUT)
	private String wxtmplmsg_id;
	private String wxtmplmsg_x_id;
	private LocalDateTime wxtmplmsg_create_date;
	private LocalDateTime wxtmplmsg_update_date;
	private String wxtmplmsg_uix;
	private String wxtmplmsg_tmpl_id;
	private String wxtmplmsg_id_src;
	private String wxtmplmsg_app_id;
	private String wxtmplmsg_name;
	private String wxtmplmsg_content;
	private String wxtmplmsg_redirect_type;
	private String wxtmplmsg_redirect_str_00;
	private String wxtmplmsg_redirect_str_01;

}
