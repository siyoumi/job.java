package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.WxSubscribemsgLog;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class WxSubscribemsgLogBase
        extends EntityBase<WxSubscribemsgLog>
        implements Serializable
{
    @Override
    public String prefix(){
        return "wxsglog_";
    }

    static public String table() {
        return "wx_app.t_wx_subscribemsg_log";
    }

    static public String tableKey() {
        return "wxsglog_id";
    }


	@TableId(value = "wxsglog_id", type = IdType.INPUT)
	private Integer wxsglog_id;
	private String wxsglog_x_id;
	private LocalDateTime wxsglog_create_date;
	private LocalDateTime wxsglog_update_date;
	private String wxsglog_wxsg_id;
	private String wxsglog_openid;
	private Integer wxsglog_use;
	private Integer wxsglog_use_errcode;
	private String wxsglog_use_errmsg;

}
