package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookSpuBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_book_spu", schema = "wx_app_x")
public class BookSpu
        extends BookSpuBase {
    public Boolean enable() {
        return getBspu_enable() == 1;
    }
}
