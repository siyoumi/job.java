package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FksTag;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FksTagBase
        extends EntityBase<FksTag>
        implements Serializable
{
    @Override
    public String prefix(){
        return "ftag_";
    }

    static public String table() {
        return "wx_app_x.t_fks_tag";
    }

    static public String tableKey() {
        return "ftag_id";
    }


	@TableId(value = "ftag_id", type = IdType.INPUT)
	private String ftag_id;
	private String ftag_x_id;
	private LocalDateTime ftag_create_date;
	private LocalDateTime ftag_update_date;
	private String ftag_uid;
	private String ftag_name;

}
