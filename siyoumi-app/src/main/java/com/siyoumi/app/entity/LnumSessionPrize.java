package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.siyoumi.app.entity.base.LnumSessionPrizeBase;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Duration;
import java.time.LocalDateTime;


@Data
@Accessors(chain = true)
@TableName(value = "t_lnum_session_prize", schema = "wx_app")
public class LnumSessionPrize
        extends LnumSessionPrizeBase
{
}
