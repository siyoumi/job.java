package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.M2SpuBase;
import com.siyoumi.util.XReturn;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_m2_spu", schema = "wx_app")
public class M2Spu
        extends M2SpuBase {

    /**
     * 已删除
     */
    public Boolean del() {
        if (getMspu_del() == null) {
            return true;
        }
        return getMspu_del() > 0;
    }

    /**
     * 上架
     */
    public Boolean enable() {
        if (getMspu_enable() == null) {
            return true;
        }
        return getMspu_enable().equals(1);
    }

    /**
     * 能否编辑
     */
    public XReturn canEdit() {
        if (del()) {
            return XReturn.getR(20026, "已删除");
        }
        if (!enable()) {
            return XReturn.getR(0);
        }

        return XReturn.getR(20033, "无法编辑，请下架商品");
    }
}
