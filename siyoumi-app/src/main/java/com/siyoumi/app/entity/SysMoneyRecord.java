package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysMoneyRecordBase;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_money_record", schema = "wx_app")
public class SysMoneyRecord
        extends SysMoneyRecordBase {
    /**
     * 是否可以退款
     */
    public Boolean canRefund() {
        if (getMrec_num().compareTo(new BigDecimal("0")) <= 0) {
            return false;
        }
        return "money".equals(getMrec_app_id());
    }
}
