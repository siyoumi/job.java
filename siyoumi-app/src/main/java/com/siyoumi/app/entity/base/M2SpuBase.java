package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2Spu;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2SpuBase
        extends EntityBase<M2Spu>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mspu_";
    }

    static public String table() {
        return "wx_app.t_m2_spu";
    }

    static public String tableKey() {
        return "mspu_id";
    }


	@TableId(value = "mspu_id", type = IdType.INPUT)
	private String mspu_id;
	private String mspu_x_id;
	private LocalDateTime mspu_create_date;
	private LocalDateTime mspu_update_date;
	private String mspu_acc_id;
	private String mspu_app_id;
	private String mspu_group_id;
	private Integer mspu_type;
	private String mspu_name;
	private String mspu_img;
	private String mspu_img_banner;
	private String mspu_img_info;
	private BigDecimal mspu_price;
	private Long mspu_sales_count;
	private Integer mspu_limit_enable;
	private Integer mspu_limit_type;
	private Long mspu_limit_max;
	private Integer mspu_enable;
	private LocalDateTime mspu_enable_date;
	private Integer mspu_order;
	private Long mspu_del;

}
