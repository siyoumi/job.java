package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2OrderRefund;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2OrderRefundBase
        extends EntityBase<M2OrderRefund>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mref_";
    }

    static public String table() {
        return "wx_app.t_m2_order_refund";
    }

    static public String tableKey() {
        return "mref_id";
    }


	@TableId(value = "mref_id", type = IdType.INPUT)
	private String mref_id;
	private String mref_x_id;
	private LocalDateTime mref_create_date;
	private LocalDateTime mref_update_date;
	private String mref_app_id;
	private String mref_order_id;
	private String mref_item_id;
	private String mref_openid;
	private BigDecimal mref_apply_price;
	private String mref_refund_id;
	private BigDecimal mref_refund_price;
	private String mref_refund_desc;
	private Integer mref_audit_status;
	private LocalDateTime mref_audit_date;
	private String mref_audit_acc;
	private String mref_audit_desc;
	private Integer mref_refund;
	private LocalDateTime mref_refund_date;
	private String mref_refund_errmsg;

}
