package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssStudyRecord;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssStudyRecordBase
        extends EntityBase<EssStudyRecord>
        implements Serializable
{
    @Override
    public String prefix(){
        return "esre_";
    }

    static public String table() {
        return "wx_app_x.t_ess_study_record";
    }

    static public String tableKey() {
        return "esre_id";
    }


	@TableId(value = "esre_id", type = IdType.INPUT)
	private String esre_id;
	private String esre_x_id;
	private LocalDateTime esre_create_date;
	private LocalDateTime esre_update_date;
	private String esre_study_id;
	private String esre_task_id;
	private String esre_uid;
	private Long esre_fun;
    private Integer esre_state;
    private LocalDateTime esre_state_date;
    private String esre_class_id;

}
