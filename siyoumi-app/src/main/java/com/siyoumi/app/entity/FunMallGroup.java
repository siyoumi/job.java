package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.FunMallGroupBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_fun_mall_group", schema = "wx_app")
public class FunMallGroup
        extends FunMallGroupBase
{

}
