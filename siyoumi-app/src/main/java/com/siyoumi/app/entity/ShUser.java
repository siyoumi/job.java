package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.ShUserBase;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XStr;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Duration;


@Data
@Accessors(chain = true)
@TableName(value = "t_sh_user", schema = "wx_app_x")
public class ShUser
        extends ShUserBase {
    static public Integer expireDayMax() {
        return 3;
    }

    static public Integer expireHourMax() {
        return 60;
    }

    /**
     * 帐号过期剩余天数
     */
    public Integer leftExpireDay() {
        Duration between = XDate.between(getSuser_key_expire_date(), XDate.now());
        int dayCount = (int) between.toDays();
        Integer leftDay = expireDayMax() - dayCount;
        //if (leftDay < 0) {
        //    leftDay = 0;
        //}
        return leftDay;
    }

    public Integer leftExpireHours(int hourMax) {
        Duration between = XDate.between(getSuser_key_expire_date(), XDate.now());
        int hour = (int) between.toHours();
        Integer leftHour = hourMax - hour;

        return leftHour;
    }

    public Integer leftExpireHours() {
        return leftExpireHours(2);
    }

    /**
     * pt_key=abczcvasdf;pt_pin=123;
     * 获取
     * 123
     */
    public String ptPin() {
        if (XStr.isNullOrEmpty(getSuser_key())) {
            return "";
        }

        String[] keyArr = getSuser_key().split(";");
        if (keyArr.length <= 1) {
            return "";
        }

        String pin = keyArr[1];
        String[] pinArr = pin.split("=");
        if (pinArr.length <= 1) {
            return "";
        }

        return pinArr[1];
    }
}
