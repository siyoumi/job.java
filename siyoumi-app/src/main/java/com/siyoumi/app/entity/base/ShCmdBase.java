package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.ShCmd;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class ShCmdBase
        extends EntityBase<ShCmd>
        implements Serializable {
    @Override
    public String prefix() {
        return "scmd_";
    }

    static public String table() {
        return "wx_app_x.t_sh_cmd";
    }

    static public String tableKey() {
        return "scmd_id";
    }


    @TableId(value = "scmd_id", type = IdType.INPUT)
    private String scmd_id;
    private String scmd_x_id;
    private LocalDateTime scmd_create_date;
    private LocalDateTime scmd_update_date;
    private Integer scmd_run;
    private LocalDateTime scmd_run_date;
    private String scmd_script;
    private String scmd_desc;
    private String scmd_run_return;

}
