package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.LnumGroup;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class LnumGroupBase
        extends EntityBase<LnumGroup>
        implements Serializable {
    @Override
    public String prefix() {
        return "lngroup_";
    }

    static public String table() {
        return "wx_app.t_lnum_group";
    }

    static public String tableKey() {
        return "lngroup_id";
    }


    @TableId(value = "lngroup_id", type = IdType.INPUT)
    private String lngroup_id;
    private String lngroup_x_id;
    private String lngroup_acc_id;
    private LocalDateTime lngroup_create_date;
    private LocalDateTime lngroup_update_date;
    private String lngroup_type;
    private String lngroup_name;
    private Integer lngroup_order;
    private Long lngroup_del;

}
