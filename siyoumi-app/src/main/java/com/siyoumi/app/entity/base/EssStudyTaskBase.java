package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssStudyTask;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssStudyTaskBase
        extends EntityBase<EssStudyTask>
        implements Serializable
{
    @Override
    public String prefix(){
        return "estask_";
    }

    static public String table() {
        return "wx_app_x.t_ess_study_task";
    }

    static public String tableKey() {
        return "estask_id";
    }


	@TableId(value = "estask_id", type = IdType.INPUT)
	private String estask_id;
	private String estask_x_id;
	private LocalDateTime estask_create_date;
	private LocalDateTime estask_update_date;
	private String estask_study_id;
	private String estask_key;
	private Long estask_fun;
	private Long estask_total_done;
	private Long estask_total;

}
