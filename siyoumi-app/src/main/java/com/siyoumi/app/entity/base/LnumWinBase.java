package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.LnumWin;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class LnumWinBase
        extends EntityBase<LnumWin>
        implements Serializable
{
    @Override
    public String prefix(){
        return "lnwin_";
    }

    static public String table() {
        return "wx_app.t_lnum_win";
    }

    static public String tableKey() {
        return "lnwin_id";
    }


	@TableId(value = "lnwin_id", type = IdType.INPUT)
	private String lnwin_id;
	private String lnwin_x_id;
	private LocalDateTime lnwin_create_date;
	private LocalDateTime lnwin_update_date;
	private String lnwin_group_id;
	private String lnwin_session_id;
	private String lnwin_prize_id;
	private String lnwin_wxuser_id;
	private String lwin_wxuser_id__master;
	private Long lnwin_num;

}
