package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysSheet;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysSheetBase
        extends EntityBase<SysSheet>
        implements Serializable {
    @Override
    public String prefix() {
        return "sheet_";
    }

    static public String table() {
        return "wx_app.t_a_sheet";
    }

    static public String tableKey() {
        return "sheet_id";
    }


    @TableId(value = "sheet_id", type = IdType.INPUT)
    private String sheet_id;
    private String sheet_x_id;
    private LocalDateTime sheet_create_date;
    private LocalDateTime sheet_update_date;
    private String sheet_uix;
    private String sheet_name;
    private LocalDateTime sheet_begin_date;
    private LocalDateTime sheet_end_date;
    private String sheet_banner_imgs;
    private Integer sheet_submit_type;
    private String sheet_item_data;

}
