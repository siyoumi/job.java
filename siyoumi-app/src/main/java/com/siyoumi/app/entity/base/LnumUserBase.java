package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.LnumUser;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class LnumUserBase
        extends EntityBase<LnumUser>
        implements Serializable
{
    @Override
    public String prefix(){
        return "lnuser_";
    }

    static public String table() {
        return "wx_app.t_lnum_user";
    }

    static public String tableKey() {
        return "lnuser_id";
    }


	@TableId(value = "lnuser_id", type = IdType.INPUT)
	private String lnuser_id;
	private String lnuser_x_id;
	private LocalDateTime lnuser_create_date;
	private LocalDateTime lnuser_update_date;
	private String lnuser_openid;
	private Long lnuser_get_total;
	private Long lnuser_win_total;

}
