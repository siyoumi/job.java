package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookSet;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookSetBase
        extends EntityBase<BookSet>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bset_";
    }

    static public String table() {
        return "wx_app_x.t_book_set";
    }

    static public String tableKey() {
        return "bset_id";
    }


	@TableId(value = "bset_id", type = IdType.INPUT)
	private String bset_id;
	private String bset_x_id;
	private String bset_acc_id;
	private LocalDateTime bset_create_date;
	private LocalDateTime bset_update_date;
	private String bset_store_id;
	private String bset_name;
	private BigDecimal bset_day_price_breakfast;
	private BigDecimal bset_day_price_lunch;
	private BigDecimal bset_day_price_dinner;
	private BigDecimal bset_day_price;
	private String bset_desc;
	private Integer bset_enable;
	private LocalDateTime bset_enable_date;
	private Integer bset_order;
	private Long bset_del;

}
