package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysRefundOrder;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysRefundOrderBase
        extends EntityBase<SysRefundOrder>
        implements Serializable
{
    @Override
    public String prefix(){
        return "refo_";
    }

    static public String table() {
        return "wx_app.t_s_refund_order";
    }

    static public String tableKey() {
        return "refo_id";
    }


	@TableId(value = "refo_id", type = IdType.INPUT)
	private Long refo_id;
	private String refo_x_id;
	private LocalDateTime refo_create_date;
	private LocalDateTime refo_update_date;
	private String refo_order_id;
	private String refo_id_src;
	private String refo_openid;
	private String refo_refund_id;
	private BigDecimal refo_refund_price;
	private String refo_apply_desc;
	private Integer refo_apply_status;
	private LocalDateTime refo_apply_date;
	private String refo_apply_acc;
	private Integer refo_refund_errcode;
	private String refo_refund_errmsg;
	private LocalDateTime refo_refund_date;

}
