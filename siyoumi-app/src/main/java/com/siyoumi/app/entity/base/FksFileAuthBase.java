package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FksFileAuth;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FksFileAuthBase
        extends EntityBase<FksFileAuth>
        implements Serializable
{
    @Override
    public String prefix(){
        return "ffauth_";
    }

    static public String table() {
        return "wx_app_x.t_fks_file_auth";
    }

    static public String tableKey() {
        return "ffauth_id";
    }


	@TableId(value = "ffauth_id", type = IdType.INPUT)
	private String ffauth_id;
	private String ffauth_x_id;
	private LocalDateTime ffauth_create_date;
	private LocalDateTime ffauth_update_date;
	private String ffauth_file_id;
	private Integer ffauth_type;
	private String ffauth_type_id;

}
