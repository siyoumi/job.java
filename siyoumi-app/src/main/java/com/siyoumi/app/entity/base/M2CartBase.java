package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2Cart;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2CartBase
        extends EntityBase<M2Cart>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mcart_";
    }

    static public String table() {
        return "wx_app.t_m2_cart";
    }

    static public String tableKey() {
        return "mcart_id";
    }


	@TableId(value = "mcart_id", type = IdType.INPUT)
	private String mcart_id;
	private String mcart_x_id;
	private LocalDateTime mcart_create_date;
	private LocalDateTime mcart_update_date;
	private String mcart_openid;
	private String mcart_sku_id;
	private String mcart_spu_id;
	private Integer mcart_count;

}
