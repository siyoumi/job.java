package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysAbc;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class SysAbcBase
        extends EntityBase<SysAbc>
        implements Serializable {
    @Override
    public String prefix() {
        return "abc_";
    }

    static public String table() {
        return "wx_app.t_s_abc";
    }

    static public String tableKey() {
        return "abc_id";
    }


    @TableId(value = "abc_id", type = IdType.INPUT)
    private String abc_id;
    private String abc_x_id;
    private LocalDateTime abc_create_date;
    private LocalDateTime abc_update_date;
    private String abc_uix;
    private String abc_app_id;
    private String abc_table;
    private String abc_acc_id;
    private String abc_key;
    private String abc_type;
    private String abc_pid;
    private String abc_name;
    private String abc_str_00;
    private String abc_str_01;
    private String abc_str_02;
    private String abc_str_03;
    private String abc_str_04;
    private String abc_str_05;
    private String abc_txt_00;
    private String abc_txt_01;
    private String abc_long_00;
    private Integer abc_int_00;
    private Integer abc_int_01;
    private Integer abc_int_02;
    private BigDecimal abc_num_00;
    private BigDecimal abc_num_01;
    private LocalDateTime abc_date_00;
    private LocalDateTime abc_date_01;
    private Integer abc_hide;
    private Integer abc_order;
    private Integer abc_del;

}
