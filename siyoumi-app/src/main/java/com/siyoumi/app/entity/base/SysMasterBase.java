package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysMaster;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysMasterBase
        extends EntityBase<SysMaster>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mast_";
    }

    static public String table() {
        return "wx_app.t_sys_master";
    }

    static public String tableKey() {
        return "mast_id";
    }


	@TableId(value = "mast_id", type = IdType.INPUT)
	private String mast_id;
	private String mast_x_id;
	private String mast_acc_id;
	private LocalDateTime mast_create_date;
	private LocalDateTime mast_update_date;
	private String mast_uid;
	private String mast_openid;

}
