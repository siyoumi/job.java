package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.M2SkuBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_m2_sku", schema = "wx_app")
public class M2Sku
        extends M2SkuBase {

    /**
     * 限购
     */
    public Boolean doLimit() {
        return getMsku_limit_enable() == 1;
    }

    /**
     * 限购总量（每人总数）
     */
    public Long limitMax() {
        if (!doLimit()) {
            return 20L;
        }
        return getMsku_limit_max();
    }

}
