package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysCode;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysCodeBase
        extends EntityBase<SysCode>
        implements Serializable
{
    @Override
    public String prefix(){
        return "code_";
    }

    static public String table() {
        return "wx_app.t_s_code";
    }

    static public String tableKey() {
        return "code_id";
    }


	@TableId(value = "code_id", type = IdType.INPUT)
	private Long code_id;
	private String code_x_id;
	private LocalDateTime code_create_date;
	private String code_app_id;
	private String code_key;
	private String code_code;
	private Long code_use;
	private String code_use_key;
	private LocalDateTime code_use_date;
	private LocalDateTime code_vaild_date_begin;
	private LocalDateTime code_vaild_date_end;

}
