package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysPrizeSet;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysPrizeSetBase
        extends EntityBase<SysPrizeSet>
        implements Serializable {
    @Override
    public String prefix() {
        return "pset_";
    }

    static public String table() {
        return "wx_app.t_s_prize_set";
    }

    static public String tableKey() {
        return "pset_id";
    }


    @TableId(value = "pset_id", type = IdType.INPUT)
    private String pset_id;
    private String pset_x_id;
    private LocalDateTime pset_create_date;
    private LocalDateTime pset_update_date;
    private String pset_name;
    private String pset_type;
    private String pset_app_id;
    private String pset_key;
    private String pset_id_src;
    private String pset_pic;
    private String pset_desc;
    private String pset_content;
    private Integer pset_use_type;
    private String pset_use_str_00;
    private Integer pset_vaild_date_type;
    private Integer pset_vaild_date_win_h;
    private LocalDateTime pset_vaild_date_begin;
    private LocalDateTime pset_vaild_date_end;
    private Integer pset_int_00;
    private Integer pset_int_01;
    private String pset_str_00;
    private Long pset_del;
    private Integer pset_order;
    private Integer pset_send_type;
    private String pset_send_id;
    private Long pset_fun;
    private String pset_use_info_option;

}
