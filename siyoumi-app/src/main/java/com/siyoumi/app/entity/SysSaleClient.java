package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysSaleClientBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_sale_client", schema = "wx_app")
public class SysSaleClient
        extends SysSaleClientBase
{

}
