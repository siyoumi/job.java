package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.LnumSessionPrize;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class LnumSessionPrizeBase
        extends EntityBase<LnumSessionPrize>
        implements Serializable
{
    @Override
    public String prefix(){
        return "lnsp_";
    }

    static public String table() {
        return "wx_app.t_lnum_session_prize";
    }

    static public String tableKey() {
        return "lnsp_id";
    }


	@TableId(value = "lnsp_id", type = IdType.INPUT)
	private String lnsp_id;
	private String lnsp_x_id;
	private String lnsp_acc_id;
	private LocalDateTime lnsp_create_date;
	private LocalDateTime lnsp_update_date;
	private String lnsp_group_id;
	private String lnsp_session_id;
	private String lnsp_name;
	private String lnsp_pic;
	private String lnsp_desc;
	private Integer lnsp_count;
	private Integer lnsp_order;

}
