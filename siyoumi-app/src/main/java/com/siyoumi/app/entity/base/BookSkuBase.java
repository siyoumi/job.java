package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookSku;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookSkuBase
        extends EntityBase<BookSku>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bsku_";
    }

    static public String table() {
        return "wx_app_x.t_book_sku";
    }

    static public String tableKey() {
        return "bsku_id";
    }


	@TableId(value = "bsku_id", type = IdType.INPUT)
	private String bsku_id;
	private String bsku_x_id;
	private String bsku_acc_id;
	private LocalDateTime bsku_create_date;
	private LocalDateTime bsku_update_date;
	private String bsku_store_id;
	private String bsku_spu_id;
	private Integer bsku_day;
	private BigDecimal bsku_day_price;
	private Integer bsku_enable;
	private Integer bsku_del;

}
