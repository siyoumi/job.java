package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookRefundOrderBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_book_refund_order", schema = "wx_app_x")
public class BookRefundOrder
        extends BookRefundOrderBase
{

}
