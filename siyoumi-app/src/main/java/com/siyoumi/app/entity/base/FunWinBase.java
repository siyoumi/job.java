package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FunWin;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FunWinBase
        extends EntityBase<FunWin>
        implements Serializable
{
    @Override
    public String prefix(){
        return "fwin_";
    }

    static public String table() {
        return "wx_app.t_fun_win";
    }

    static public String tableKey() {
        return "fwin_id";
    }


	@TableId(value = "fwin_id", type = IdType.INPUT)
	private String fwin_id;
	private String fwin_x_id;
	private LocalDateTime fwin_create_date;
	private LocalDateTime fwin_update_date;
	private String fwin_session_id;
	private String fwin_openid;
	private String fwin_code;

}
