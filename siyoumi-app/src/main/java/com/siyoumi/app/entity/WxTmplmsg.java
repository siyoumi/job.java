package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.WxTmplmsgBase;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;


@Data
@Accessors(chain = true)
@TableName(value = "t_wx_tmplmsg", schema = "wx_app")
public class WxTmplmsg
        extends WxTmplmsgBase {

    private String wxtmplmsg_uix;
    @HasAnyText
    private String wxtmplmsg_tmpl_id;
    private String wxtmplmsg_id_src;
    private String wxtmplmsg_app_id;

    @HasAnyText
    @Size(max = 50)
    private String wxtmplmsg_name;
    @Size(max = 5000)
    private String wxtmplmsg_content;
    private String wxtmplmsg_redirect_type;
    @Size(max = 200)
    private String wxtmplmsg_redirect_str_00;
    private String wxtmplmsg_redirect_str_01;

}
