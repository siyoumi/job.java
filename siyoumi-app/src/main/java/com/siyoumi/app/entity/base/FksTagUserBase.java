package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FksTagUser;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FksTagUserBase
        extends EntityBase<FksTagUser>
        implements Serializable
{
    @Override
    public String prefix(){
        return "ftu_";
    }

    static public String table() {
        return "wx_app_x.t_fks_tag_user";
    }

    static public String tableKey() {
        return "ftu_id";
    }


	@TableId(value = "ftu_id", type = IdType.INPUT)
	private String ftu_id;
	private String ftu_x_id;
	private LocalDateTime ftu_create_date;
	private LocalDateTime ftu_update_date;
	private String ftu_tag_id;
	private String ftu_uid;

}
