package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysDepartment;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysDepartmentBase
        extends EntityBase<SysDepartment>
        implements Serializable
{
    @Override
    public String prefix(){
        return "dep_";
    }

    static public String table() {
        return "wx_app.t_sys_department";
    }

    static public String tableKey() {
        return "dep_id";
    }


	@TableId(value = "dep_id", type = IdType.INPUT)
	private String dep_id;
	private String dep_x_id;
	private LocalDateTime dep_create_date;
	private LocalDateTime dep_update_date;
	private String dep_name;
	private Integer dep_order;
	private Long dep_del;

}
