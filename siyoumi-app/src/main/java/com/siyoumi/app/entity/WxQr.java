package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.WxQrBase;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@TableName(value = "t_wx_qr", schema = "wx_app")
public class WxQr
        extends WxQrBase {

    private String wxqr_key;
    private String wxqr_type;
    private String wxqr_type_qr;
    @NotBlank
    @Size(max = 50)
    private String wxqr_name;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime wxqr_expire_date;
    private Long wxqr_reply_id;
    private String wxqr_info;
    @NotBlank
    @Size(max = 128)
    private String wxqr_path;
    private String wxqr_append_data;

}
