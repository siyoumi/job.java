package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.EssTestQuestionBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_ess_test_question", schema = "wx_app_x")
public class EssTestQuestion
        extends EssTestQuestionBase
{

}
