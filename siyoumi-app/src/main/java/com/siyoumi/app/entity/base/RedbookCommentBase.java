package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.RedbookComment;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class RedbookCommentBase
        extends EntityBase<RedbookComment>
        implements Serializable
{
    @Override
    public String prefix(){
        return "rbc_";
    }

    static public String table() {
        return "wx_app.t_redbook_comment";
    }

    static public String tableKey() {
        return "rbc_id";
    }


	@TableId(value = "rbc_id", type = IdType.INPUT)
	private String rbc_id;
	private String rbc_x_id;
	private LocalDateTime rbc_create_date;
	private LocalDateTime rbc_update_date;
	private String rbc_rquan_id;
	private String rbc_uid;
	private String rbc_pid;
	private Integer rbc_is_master;
	private String rbc_reply_to;
	private Integer rbc_reply_num;
	private String rbc_content;
	private String rbc_pic;
	private Long rbc_good_count;
	private Integer rbc_enable;
	private LocalDateTime rbc_enable_date;

}
