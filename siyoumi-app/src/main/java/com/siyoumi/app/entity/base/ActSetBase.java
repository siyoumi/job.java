package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.ActSet;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class ActSetBase
        extends EntityBase<ActSet>
        implements Serializable {
    @Override
    public String prefix() {
        return "aset_";
    }

    static public String table() {
        return "wx_app.t_act_set";
    }

    static public String tableKey() {
        return "aset_id";
    }


    @TableId(value = "aset_id", type = IdType.INPUT)
    private String aset_id;
    private String aset_x_id;
    private LocalDateTime aset_create_date;
    private LocalDateTime aset_update_date;
    private String aset_uix;
    private Long aset_reply_id;
    private String aset_item_id;
    private String aset_name;
    private LocalDateTime aset_begin_date;
    private LocalDateTime aset_end_date;
    private Integer aset_win_multi;
    private String aset_desc;
    private Integer aset_status;
    private Integer aset_order;
    private Long aset_del;

}
