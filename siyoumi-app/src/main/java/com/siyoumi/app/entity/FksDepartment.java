package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.FksDepartmentBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_fks_department", schema = "wx_app_x")
public class FksDepartment
        extends FksDepartmentBase
{

}
