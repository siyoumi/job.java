package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.RedbookQuan;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class RedbookQuanBase
        extends EntityBase<RedbookQuan>
        implements Serializable {
    @Override
    public String prefix() {
        return "rquan_";
    }

    static public String table() {
        return "wx_app.t_redbook_quan";
    }

    static public String tableKey() {
        return "rquan_id";
    }


    @TableId(value = "rquan_id", type = IdType.INPUT)
    private String rquan_id;
    private String rquan_x_id;
    private LocalDateTime rquan_create_date;
    private LocalDateTime rquan_update_date;
    private String rquan_uix;
    private String rquan_uid;
    private String rquan_app_id;
    private String rquan_group_id;
    private Integer rquan_type;
    private String rquan_url;
    private String rquan_thumbnail;
    private String rquan_title;
    private String rquan_content;
    private String rquan_content_src;
    private Integer rquan_star;
    private Long rquan_top;
    private Long rquan_good_count;
    private Long rquan_good_count_x;
    private Long rquan_comment_count;
    private String rquan_comment_last_ids;
    private Long rquan_collect_count;
    private Integer rquan_enable;
    private LocalDateTime rquan_enable_date;

}
