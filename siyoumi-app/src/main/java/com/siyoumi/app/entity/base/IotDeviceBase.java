package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.IotDevice;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class IotDeviceBase
        extends EntityBase<IotDevice>
        implements Serializable {
    @Override
    public String prefix() {
        return "idev_";
    }

    static public String table() {
        return "wx_app.t_iot_device";
    }

    static public String tableKey() {
        return "idev_id";
    }


    @TableId(value = "idev_id", type = IdType.INPUT)
    private String idev_id;
    private String idev_acc_id;
    private String idev_x_id;
    private LocalDateTime idev_create_date;
    private LocalDateTime idev_update_date;
    private String idev_pro_id;
    private String idev_num;
    private String idev_key;
    private Long idev_del;
    private Integer idev_order;

}
