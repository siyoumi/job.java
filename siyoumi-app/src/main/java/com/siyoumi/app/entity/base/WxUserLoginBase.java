package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.WxUserLogin;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class WxUserLoginBase
        extends EntityBase<WxUserLogin>
        implements Serializable
{
    @Override
    public String prefix(){
        return "wulogin_";
    }

    static public String table() {
        return "wx_app.t_wx_user_login";
    }

    static public String tableKey() {
        return "wulogin_id";
    }


	private Long wulogin_id;
	private String wulogin_x_id;
	private LocalDateTime wulogin_create_date;
	private LocalDateTime wulogin_update_date;
	private String wulogin_openid;
	private String wulogin_uid;
	private Integer wulogin_state;
	private LocalDateTime wulogin_state_date;

}
