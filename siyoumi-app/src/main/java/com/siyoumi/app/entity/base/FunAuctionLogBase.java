package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FunAuctionLog;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FunAuctionLogBase
        extends EntityBase<FunAuctionLog>
        implements Serializable
{
    @Override
    public String prefix(){
        return "falog_";
    }

    static public String table() {
        return "wx_app.t_fun_auction_log";
    }

    static public String tableKey() {
        return "falog_id";
    }


	@TableId(value = "falog_id", type = IdType.INPUT)
	private String falog_id;
	private String falog_x_id;
	private LocalDateTime falog_create_date;
	private LocalDateTime falog_update_date;
	private String falog_uix;
	private Long falog_set_id;
	private String falog_openid;
	private Integer falog_num;

}
