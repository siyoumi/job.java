package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.ActDataLaunch;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class ActDataLaunchBase
        extends EntityBase<ActDataLaunch>
        implements Serializable
{
    @Override
    public String prefix(){
        return "adl_";
    }

    static public String table() {
        return "wx_app.t_act_data_launch";
    }

    static public String tableKey() {
        return "adl_id";
    }


	@TableId(value = "adl_id", type = IdType.INPUT)
	private String adl_id;
	private String adl_x_id;
	private LocalDateTime adl_create_date;
	private LocalDateTime adl_update_date;
	private String adl_actset_id;
	private String adl_key;
	private String adl_openid;
	private String adl_str_00;
	private String adl_str_01;
	private String adl_str_02;
	private String adl_str_03;
	private String adl_str_04;
	private Integer adl_int_00;
	private Integer adl_int_01;

}
