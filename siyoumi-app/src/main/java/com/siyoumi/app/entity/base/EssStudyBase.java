package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssStudy;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssStudyBase
        extends EntityBase<EssStudy>
        implements Serializable
{
    @Override
    public String prefix(){
        return "estudy_";
    }

    static public String table() {
        return "wx_app_x.t_ess_study";
    }

    static public String tableKey() {
        return "estudy_id";
    }


	@TableId(value = "estudy_id", type = IdType.INPUT)
	private String estudy_id;
	private String estudy_x_id;
	private LocalDateTime estudy_create_date;
	private LocalDateTime estudy_update_date;
	private String estudy_uid;
	private String estudy_module_id;
	private String estudy_name;
	private LocalDateTime estudy_date_begin;
	private LocalDateTime estudy_date_end;
    private Long estudy_student_total;

}
