package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysPrizeBase;
import com.siyoumi.util.XDate;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_prize", schema = "wx_app")
public class SysPrize
        extends SysPrizeBase {
    public Boolean use() {
        return getPrize_use() == 1;
    }

    public Boolean expire() {
        if (use()) {
            return false;
        }

        if (XDate.now().isAfter(getPrize_end_date())) {
            return true;
        }

        return false;
    }
}
