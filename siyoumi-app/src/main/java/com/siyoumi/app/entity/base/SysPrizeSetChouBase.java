package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysPrizeSetChou;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysPrizeSetChouBase
        extends EntityBase<SysPrizeSetChou>
        implements Serializable {
    @Override
    public String prefix() {
        return "pschou_";
    }

    static public String table() {
        return "wx_app.t_s_prize_set_chou";
    }

    static public String tableKey() {
        return "pschou_id";
    }


    @TableId(value = "pschou_id", type = IdType.INPUT)
    private String pschou_id;
    private String pschou_x_id;
    private LocalDateTime pschou_create_date;
    private LocalDateTime pschou_update_date;
    private String pschou_id_src;
    private BigDecimal pschou_win_rate;
    private Integer pschou_win_multi;
    private Integer pschou_index;
    private Long pschou_del;

}
