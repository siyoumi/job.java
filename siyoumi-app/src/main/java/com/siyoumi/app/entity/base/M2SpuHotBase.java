package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2SpuHot;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2SpuHotBase
        extends EntityBase<M2SpuHot>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mspuh_";
    }

    static public String table() {
        return "wx_app.t_m2_spu_hot";
    }

    static public String tableKey() {
        return "mspuh_id";
    }


	@TableId(value = "mspuh_id", type = IdType.INPUT)
	private String mspuh_id;
	private String mspuh_x_id;
	private LocalDateTime mspuh_create_date;
	private LocalDateTime mspuh_update_date;
	private String mspuh_spu_id;
	private Integer mspuh_order;

}
