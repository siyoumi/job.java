package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.M2FreightCityBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_m2_freight_city", schema = "wx_app")
public class M2FreightCity
        extends M2FreightCityBase
{

}
