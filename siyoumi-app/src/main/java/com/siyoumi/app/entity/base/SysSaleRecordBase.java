package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysSaleRecord;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysSaleRecordBase
        extends EntityBase<SysSaleRecord>
        implements Serializable
{
    @Override
    public String prefix(){
        return "srec_";
    }

    static public String table() {
        return "wx_app.t_s_sale_record";
    }

    static public String tableKey() {
        return "srec_id";
    }


	@TableId(value = "srec_id", type = IdType.INPUT)
	private String srec_id;
	private String srec_x_id;
	private LocalDateTime srec_create_date;
	private LocalDateTime srec_update_date;
	private String srec_app_id;
	private String srec_key;
	private String srec_id_src;
	private String srec_uid;
	private String srec_ab_type;
	private BigDecimal srec_num;
	private String srec_desc;
	private String srec_id_00;
	private String srec_id_01;
	private Integer srec_int_00;
	private String srec_str_00;

}
