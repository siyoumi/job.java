package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2AttrSku;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2AttrSkuBase
        extends EntityBase<M2AttrSku>
        implements Serializable
{
    @Override
    public String prefix(){
        return "masku_";
    }

    static public String table() {
        return "wx_app.t_m2_attr_sku";
    }

    static public String tableKey() {
        return "masku_id";
    }


	@TableId(value = "masku_id", type = IdType.INPUT)
	private String masku_id;
	private String masku_x_id;
	private LocalDateTime masku_create_date;
	private LocalDateTime masku_update_date;
	private String masku_spu_id;
	private String masku_attr_id;
	private String masku_name;
	private Integer masku_sync;
	private Integer masku_order;
	private Long masku_del;

}
