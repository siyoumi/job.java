package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssModule;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssModuleBase
        extends EntityBase<EssModule>
        implements Serializable
{
    @Override
    public String prefix(){
        return "emod_";
    }

    static public String table() {
        return "wx_app_x.t_ess_module";
    }

    static public String tableKey() {
        return "emod_id";
    }


	@TableId(value = "emod_id", type = IdType.INPUT)
	private String emod_id;
	private String emod_x_id;
	private LocalDateTime emod_create_date;
	private LocalDateTime emod_update_date;
	private String emod_acc_id;
	private String emod_name;
	private String emod_pic;
	private String emod_exe_tag;
	private Integer emod_set_task_total;
	private Integer emod_enable;
	private Long emod_question0;
	private Long emod_question1;
	private Integer emod_order;
	private Long emod_del;

}
