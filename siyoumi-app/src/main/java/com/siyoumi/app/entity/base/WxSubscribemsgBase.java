package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.WxSubscribemsg;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class WxSubscribemsgBase
        extends EntityBase<WxSubscribemsg>
        implements Serializable
{
    @Override
    public String prefix(){
        return "wxsg_";
    }

    static public String table() {
        return "wx_app.t_wx_subscribemsg";
    }

    static public String tableKey() {
        return "wxsg_id";
    }


	@TableId(value = "wxsg_id", type = IdType.INPUT)
	private String wxsg_id;
	private String wxsg_x_id;
	private LocalDateTime wxsg_create_date;
	private LocalDateTime wxsg_update_date;
	private String wxsg_uix;
	private String wxsg_name;
	private String wxsg_template_id;
	private String wxsg_content;
	private String wxsg_page;
	private String wxsg_miniprogram_state;

}
