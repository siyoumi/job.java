package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FunSignUser;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FunSignUserBase
        extends EntityBase<FunSignUser>
        implements Serializable
{
    @Override
    public String prefix(){
        return "signu_";
    }

    static public String table() {
        return "wx_app.t_fun_sign_user";
    }

    static public String tableKey() {
        return "signu_id";
    }


	@TableId(value = "signu_id", type = IdType.INPUT)
	private Long signu_id;
	private String signu_x_id;
	private LocalDateTime signu_create_date;
	private LocalDateTime signu_update_date;
	private String signu_uix;
	private Long signu_abc_id__sign;
	private String signu_openid;
	private Integer signu_x_now;
	private Integer signu_x_long;
	private Integer signu_total;
	private LocalDateTime signu_last_time;

}
