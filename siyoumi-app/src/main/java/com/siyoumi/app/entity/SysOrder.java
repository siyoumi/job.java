package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.siyoumi.app.entity.base.SysOrderBase;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XDate;
import com.siyoumi.validator.XValidator;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.Duration;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_order", schema = "wx_app")
public class SysOrder
        extends SysOrderBase {
    @JsonIgnore
    public Boolean isExpire() {
        return getOrder_pay_expire_date().isBefore(XDate.now());
    }

    /**
     * 过期剩余时间（秒）
     */
    @JsonIgnore
    public Long expireSeconds() {
        if (isExpire()) {
            return 0L;
        }
        Duration between = XDate.between(XDate.now(), getOrder_pay_expire_date());
        long seconds = between.toSeconds();
        if (seconds < 0) {
            seconds = 0;
        }
        return seconds;
    }

    @JsonIgnore
    public Boolean checkPayOk() {
        return 1 == getOrder_pay_ok();
    }

    @JsonIgnore
    public Boolean checkCancel() {
        return getOrder_del() > 0;
    }

    //是否需要支付成功后处理
    @JsonIgnore
    public Boolean doPayOkHandle() {
        return checkPayOk() && getOrder_pay_ok_handle() == 0;
    }


    /**
     * 设置支付金额
     */
    public void setPayPrice() {
        BigDecimal orderPrice = getOrder_price().subtract(getOrder_price_money_down()).subtract(getOrder_price_coupon_down());
        if (BigDecimal.ZERO.compareTo(orderPrice) >= 0) {
            XValidator.err(EnumSys.ERR_VAL.getR("pay_price error " + orderPrice.toString()));
        }
        //加上运费
        setOrder_pay_price(orderPrice.add(getOrder_price_ship()));
    }
}
