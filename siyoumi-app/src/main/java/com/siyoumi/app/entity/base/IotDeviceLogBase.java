package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.IotDeviceLog;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class IotDeviceLogBase
        extends EntityBase<IotDeviceLog>
        implements Serializable
{
    @Override
    public String prefix(){
        return "idlog_";
    }

    static public String table() {
        return "wx_app.t_iot_device_log";
    }

    static public String tableKey() {
        return "idlog_id";
    }


	@TableId(value = "idlog_id", type = IdType.INPUT)
	private String idlog_id;
	private String idlog_x_id;
	private LocalDateTime idlog_create_date;
	private LocalDateTime idlog_update_date;
	private String idlog_pro_id;
	private String idlog_device_id;
	private String idlog_type;
	private String idlog_content;

}
