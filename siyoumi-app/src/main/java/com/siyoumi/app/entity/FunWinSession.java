package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.FunWinSessionBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_fun_win_session", schema = "wx_app")
public class FunWinSession
        extends FunWinSessionBase
{

}
