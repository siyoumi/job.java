package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookRefundOrder;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookRefundOrderBase
        extends EntityBase<BookRefundOrder>
        implements Serializable
{
    @Override
    public String prefix(){
        return "brefo_";
    }

    static public String table() {
        return "wx_app_x.t_book_refund_order";
    }

    static public String tableKey() {
        return "brefo_id";
    }


	@TableId(value = "brefo_id", type = IdType.INPUT)
	private String brefo_id;
	private String brefo_x_id;
	private String brefo_acc_id;
	private LocalDateTime brefo_create_date;
	private LocalDateTime brefo_update_date;
	private String brefo_store_id;
	private String brefo_order_id;
	private String brefo_uid;
	private BigDecimal brefo_price_pay;
	private BigDecimal brefo_price;
	private BigDecimal brefo_price_down;
	private BigDecimal brefo_refund_price;
	private Integer brefo_audit_status;
	private LocalDateTime brefo_audit_status_date;
	private String brefo_audit_desc;
	private Integer brefo_refund_handle;
	private LocalDateTime brefo_refund_handle_date;
	private String brefo_refund_errmsg;
	private String brefo_refund_id;

}
