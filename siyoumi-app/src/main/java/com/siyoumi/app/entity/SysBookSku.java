package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookSkuBase;
import com.siyoumi.app.entity.base.SysBookSkuBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_book_sku", schema = "wx_app")
public class SysBookSku
        extends SysBookSkuBase
{

}
