package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.EssStudyTaskBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_ess_study_task", schema = "wx_app_x")
public class EssStudyTask
        extends EssStudyTaskBase
{

}
