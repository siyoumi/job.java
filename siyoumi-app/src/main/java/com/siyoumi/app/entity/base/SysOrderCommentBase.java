package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysOrderComment;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class SysOrderCommentBase
        extends EntityBase<SysOrderComment>
        implements Serializable
{
    @Override
    public String prefix(){
        return "ocmment_";
    }

    static public String table() {
        return "wx_app.t_s_order_comment";
    }

    static public String tableKey() {
        return "ocmment_id";
    }


	@TableId(value = "ocmment_id", type = IdType.INPUT)
	private Long ocmment_id;
	private String ocmment_x_id;
	private LocalDateTime ocmment_create_date;
	private LocalDateTime ocmment_update_date;
	private String ocmment_order_id;
	private String ocmment_openid;
	private Integer ocmment_type;
	private Integer ocmment_fun;
	private String ocmment_content;

}
