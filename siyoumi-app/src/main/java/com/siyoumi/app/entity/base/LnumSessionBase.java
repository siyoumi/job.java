package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.LnumSession;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class LnumSessionBase
        extends EntityBase<LnumSession>
        implements Serializable
{
    @Override
    public String prefix(){
        return "lns_";
    }

    static public String table() {
        return "wx_app.t_lnum_session";
    }

    static public String tableKey() {
        return "lns_id";
    }


	@TableId(value = "lns_id", type = IdType.INPUT)
	private String lns_id;
	private String lns_x_id;
	private String lns_acc_id;
	private LocalDateTime lns_create_date;
	private LocalDateTime lns_update_date;
	private String lns_group_id;
	private String lns_name;
	private LocalDateTime lns_win_date_end;
	private Integer lns_win;
	private LocalDateTime lns_win_date;
	private Long lns_fun;
	private Integer lns_order;
	private Long lns_del;

}
