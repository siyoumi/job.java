package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssTest;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssTestBase
        extends EntityBase<EssTest>
        implements Serializable {
    @Override
    public String prefix() {
        return "etest_";
    }

    static public String table() {
        return "wx_app_x.t_ess_test";
    }

    static public String tableKey() {
        return "etest_id";
    }


    @TableId(value = "etest_id", type = IdType.INPUT)
    private String etest_id;
    private String etest_x_id;
    private LocalDateTime etest_create_date;
    private LocalDateTime etest_update_date;
    private String etest_uid;
    private Integer etest_type;
    private String etest_title;
    private LocalDateTime etest_date_begin;
    private LocalDateTime etest_date_end;
    private Integer etest_test_minute;
    private Integer etest_test_end_remind;
    private Integer etest_test_not_submit;
    private Integer etest_random;
    private Integer etest_score_show;
    private Long etest_submit_total;
    private Long etest_test_total;
    private Long etest_fun_total;
    private Long etest_question_total;
    private Integer etest_state;
    private LocalDateTime etest_state_date;
    private Integer etest_send_fun;
    private LocalDateTime etest_send_fun_date;
    private Integer etest_del;

}
