package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.M2SpuHotBase;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@TableName(value = "t_m2_spu_hot", schema = "wx_app")
public class M2SpuHot
        extends M2SpuHotBase
{


}
