package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysOrder;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class SysOrderBase
        extends EntityBase<SysOrder>
        implements Serializable
{
    @Override
    public String prefix(){
        return "order_";
    }

    static public String table() {
        return "wx_app.t_s_order";
    }

    static public String tableKey() {
        return "order_id";
    }


	@TableId(value = "order_id", type = IdType.INPUT)
	private String order_id;
	private String order_x_id;
	private LocalDateTime order_create_date;
	private LocalDateTime order_update_date;
	private String order_app_id;
	private String order_uid;
	private String order_openid;
	private String order_order_id_ex;
	private Integer order_pay_type;
	private Integer order_pay_ok;
	private Integer order_pay_ok_handle;
	private LocalDateTime order_pay_ok_date;
	private LocalDateTime order_pay_expire_date;
	private Integer order_pay_expire_handle;
	private LocalDateTime order_pay_expire_handle_date;
	private String order_coupon;
	private Long order_can_money;
	private Long order_use_money;
	private BigDecimal order_price;
	private BigDecimal order_price_coupon_down;
	private BigDecimal order_price_money_down;
	private BigDecimal order_price_ship;
	private BigDecimal order_pay_price;
	private String order_user_name;
	private String order_user_phone;
	private String order_desc;
	private String order_id_00;
	private Integer order_comment;
	private Integer order_refund;
	private Long order_del;

}
