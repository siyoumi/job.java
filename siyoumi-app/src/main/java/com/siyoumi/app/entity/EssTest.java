package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.EssTestBase;
import com.siyoumi.util.XDate;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Duration;


@Data
@Accessors(chain = true)
@TableName(value = "t_ess_test", schema = "wx_app_x")
public class EssTest
        extends EssTestBase {
    /**
     * 离结束还剩余X分钟
     */
    public long dateEndLeftMinute() {
        Duration between = XDate.between(XDate.now(), getEtest_date_end());
        long m = between.toMinutes();
        if (m < 0) {
            m = 0;
        }
        return m;
    }
}
