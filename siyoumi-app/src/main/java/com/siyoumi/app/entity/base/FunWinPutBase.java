package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FunWinPut;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FunWinPutBase
        extends EntityBase<FunWinPut>
        implements Serializable
{
    @Override
    public String prefix(){
        return "fwput_";
    }

    static public String table() {
        return "wx_app.t_fun_win_put";
    }

    static public String tableKey() {
        return "fwput_id";
    }


	@TableId(value = "fwput_id", type = IdType.INPUT)
	private String fwput_id;
	private String fwput_x_id;
	private LocalDateTime fwput_create_date;
	private LocalDateTime fwput_update_date;
	private String fwput_session_id;
	private String fwput_openid;
	private String fwput_code;

}
