package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2FreightItem;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2FreightItemBase
        extends EntityBase<M2FreightItem>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mfitem_";
    }

    static public String table() {
        return "wx_app.t_m2_freight_item";
    }

    static public String tableKey() {
        return "mfitem_id";
    }


	@TableId(value = "mfitem_id", type = IdType.INPUT)
	private String mfitem_id;
	private String mfitem_x_id;
	private LocalDateTime mfitem_create_date;
	private LocalDateTime mfitem_update_date;
	private String mfitem_acc_id;
	private String mfitem_freight_id;
	private Integer mfitem_def;
	private BigDecimal mfitem_price;
	private BigDecimal mfitem_price_add;
	private String mfitem_city;

}
