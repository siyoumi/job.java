package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysSheetRecordBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_a_sheet_record", schema = "wx_app")
public class SysSheetRecord
        extends SysSheetRecordBase
{

}
