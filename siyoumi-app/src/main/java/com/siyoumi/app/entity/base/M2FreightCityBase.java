package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2FreightCity;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2FreightCityBase
        extends EntityBase<M2FreightCity>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mfcity_";
    }

    static public String table() {
        return "wx_app.t_m2_freight_city";
    }

    static public String tableKey() {
        return "mfcity_id";
    }


	@TableId(value = "mfcity_id", type = IdType.INPUT)
	private String mfcity_id;
	private String mfcity_x_id;
	private LocalDateTime mfcity_create_date;
	private LocalDateTime mfcity_update_date;
	private String mfcity_acc_id;
	private String mfcity_freight_id;
	private String mfcity_item_id;
	private String mfcity_key;
	private String mfcity_province;
	private String mfcity_city;

}
