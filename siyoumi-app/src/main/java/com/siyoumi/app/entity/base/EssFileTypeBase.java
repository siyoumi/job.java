package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssFileType;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssFileTypeBase
        extends EntityBase<EssFileType>
        implements Serializable
{
    @Override
    public String prefix(){
        return "eftype_";
    }

    static public String table() {
        return "wx_app_x.t_ess_file_type";
    }

    static public String tableKey() {
        return "eftype_id";
    }


	@TableId(value = "eftype_id", type = IdType.INPUT)
	private String eftype_id;
	private String eftype_x_id;
	private LocalDateTime eftype_create_date;
	private LocalDateTime eftype_update_date;
	private String eftype_acc_id;
	private String eftype_name;
	private Integer eftype_order;

}
