package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysSetData;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class SysSetDataBase
        extends EntityBase<SysSetData>
        implements Serializable
{
    @Override
    public String prefix(){
        return "sdata_";
    }

    static public String table() {
        return "wx_app.t_s_set_data";
    }

    static public String tableKey() {
        return "sdata_id";
    }


	@TableId(value = "sdata_id", type = IdType.INPUT)
	private Long sdata_id;
	private String sdata_x_id;
	private LocalDateTime sdata_create_date;
	private LocalDateTime sdata_update_date;
	private String sdata_set_id;
	private String sdata_uix;
	private String sdata_json;
	private Integer sdata_order;

}
