package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.IotProduct;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class IotProductBase
        extends EntityBase<IotProduct>
        implements Serializable {
    @Override
    public String prefix() {
        return "ipro_";
    }

    static public String table() {
        return "wx_app.t_iot_product";
    }

    static public String tableKey() {
        return "ipro_id";
    }


    @TableId(value = "ipro_id", type = IdType.INPUT)
    private String ipro_id;
    private String ipro_acc_id;
    private String ipro_x_id;
    private LocalDateTime ipro_create_date;
    private LocalDateTime ipro_update_date;
    private String ipro_type;
    private String ipro_name;
    private String ipro_key;
    private String ipro_attr;
    private Integer ipro_order;
    private Long ipro_del;

}
