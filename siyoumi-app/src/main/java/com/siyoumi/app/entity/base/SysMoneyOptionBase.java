package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysMoneyOption;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysMoneyOptionBase
        extends EntityBase<SysMoneyOption>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mopt_";
    }

    static public String table() {
        return "wx_app.t_s_money_option";
    }

    static public String tableKey() {
        return "mopt_id";
    }


	@TableId(value = "mopt_id", type = IdType.INPUT)
	private String mopt_id;
	private String mopt_x_id;
	private LocalDateTime mopt_create_date;
	private LocalDateTime mopt_update_date;
	private String mopt_acc_id;
	private Integer mopt_type;
	private BigDecimal mopt_pay_price;
	private BigDecimal mopt_give_price;
	private Integer mopt_give_fun_enable;
	private Long mopt_give_fun;
	private Integer mopt_give_cp_enable;
	private String mopt_give_cp;
	private Long mopt_del;

}
