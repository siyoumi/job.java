package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookSpuDay;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookSpuDayBase
        extends EntityBase<BookSpuDay>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bspud_";
    }

    static public String table() {
        return "wx_app_x.t_book_spu_day";
    }

    static public String tableKey() {
        return "bspud_id";
    }


	@TableId(value = "bspud_id", type = IdType.INPUT)
	private String bspud_id;
	private String bspud_x_id;
	private String bspud_acc_id;
	private LocalDateTime bspud_create_date;
	private LocalDateTime bspud_update_date;
	private String bspud_spu_id;
	private LocalDateTime bspud_date;
	private BigDecimal bspud_spu_add_price;
	private Integer bspud_stock_add;
	private Long bspud_stock_use;
	private Long bspud_stock_use_add;

}
