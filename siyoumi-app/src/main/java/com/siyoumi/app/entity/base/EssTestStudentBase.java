package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssTestStudent;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssTestStudentBase
        extends EntityBase<EssTestStudent>
        implements Serializable
{
    @Override
    public String prefix(){
        return "etcla_";
    }

    static public String table() {
        return "wx_app_x.t_ess_test_student";
    }

    static public String tableKey() {
        return "etcla_id";
    }


	@TableId(value = "etcla_id", type = IdType.INPUT)
	private String etcla_id;
	private String etcla_x_id;
	private LocalDateTime etcla_create_date;
	private LocalDateTime etcla_update_date;
	private String etcla_test_id;
	private String etcla_uid;

}
