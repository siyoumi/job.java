package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookStoreTxt;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookStoreTxtBase
        extends EntityBase<BookStoreTxt>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bstoret_";
    }

    static public String table() {
        return "wx_app_x.t_book_store_txt";
    }

    static public String tableKey() {
        return "bstoret_id";
    }


	@TableId(value = "bstoret_id", type = IdType.INPUT)
	private String bstoret_id;
	private String bstoret_x_id;
	private String bstoret_acc_id;
	private LocalDateTime bstoret_create_date;
	private LocalDateTime bstoret_update_date;
	private String bstoret_info;
    private String bstoret_money_info;

}
