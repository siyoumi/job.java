package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysPrizeSetChouBase;
import com.siyoumi.component.XApp;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@TableName(value = "t_s_prize_set_chou", schema = "wx_app")
public class SysPrizeSetChou
        extends SysPrizeSetChouBase {
    /**
     * 是否中奖
     */
    public XReturn compareWin() {
        int rnd = XApp.random(1, 100000);
        BigDecimal winRate = getPschou_win_rate().multiply(new BigDecimal("100000"));
        int winRateInt = winRate.intValue();

        XLog.info(this.getClass(), "设置数：", winRateInt, "，随机数：", rnd);
        XLog.info(this.getClass(), winRateInt, " >= ", rnd);
        if (winRateInt > 0 && winRateInt >= rnd) {
            return XReturn.getR(0);
        }

        return XReturn.getR(20029, "没抽中");
    }
}
