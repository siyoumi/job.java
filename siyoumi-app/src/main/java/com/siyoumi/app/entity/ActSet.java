package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.ActSetBase;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_act_set", schema = "wx_app")
public class ActSet
        extends ActSetBase {
    /**
     * 测试
     *
     * @return
     */
    public Boolean test() {
        return getAset_status() == 0;
    }

    public XReturn valid(Boolean passValidDate) {
        if (getAset_del() != 0) {
            return XReturn.getR(20018, "活动不存在");
        }

        if (!passValidDate) {
            return validDate();
        }

        return XReturn.getR(0);
    }

    public XReturn validDate() {
        if (XDate.now().isBefore(getAset_begin_date())) {
            return XReturn.getR(20019, "活动未开始");
        }

        if (XDate.now().isAfter(getAset_end_date())) {
            return XReturn.getR(20022, "活动已结束");
        }

        return XReturn.getR(0);
    }
}
