package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookSpuTxt;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookSpuTxtBase
        extends EntityBase<BookSpuTxt>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bsput_";
    }

    static public String table() {
        return "wx_app_x.t_book_spu_txt";
    }

    static public String tableKey() {
        return "bsput_id";
    }


	@TableId(value = "bsput_id", type = IdType.INPUT)
	private String bsput_id;
	private String bsput_x_id;
	private String bsput_acc_id;
	private LocalDateTime bsput_create_date;
	private LocalDateTime bsput_update_date;
	private String bsput_txt_00;

}
