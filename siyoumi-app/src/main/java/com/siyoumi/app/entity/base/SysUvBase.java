package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysUv;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysUvBase
        extends EntityBase<SysUv>
        implements Serializable {
    @Override
    public String prefix() {
        return "uv_";
    }

    static public String table() {
        return "wx_app.t_sys_uv";
    }

    static public String tableKey() {
        return "uv_id";
    }


    @TableId(value = "uv_id", type = IdType.INPUT)
    private String uv_id;
    private String uv_x_id;
    private LocalDateTime uv_create_date;
    private String uv_app_id;
    private String uv_type;
    private String uv_type_id;
    private String uv_uid;

}
