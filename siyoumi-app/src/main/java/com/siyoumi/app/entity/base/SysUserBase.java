package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysUser;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysUserBase
        extends EntityBase<SysUser>
        implements Serializable {
    @Override
    public String prefix() {
        return "user_";
    }

    static public String table() {
        return "wx_app.t_sys_user";
    }

    static public String tableKey() {
        return "user_id";
    }


    @TableId(value = "user_id", type = IdType.INPUT)
    private String user_id;
    private String user_x_id;
    private LocalDateTime user_create_date;
    private LocalDateTime user_update_date;
    private String user_name;
    private String user_headimg;
    private String user_username;
    private String user_pwd;
    private String user_phone;
    private String user_email;
    private Long user_fun;
    private String user_openid;
    private BigDecimal user_money;
    private BigDecimal user_money_total;
    private Integer user_enable;
    private LocalDateTime user_enable_date;
}
