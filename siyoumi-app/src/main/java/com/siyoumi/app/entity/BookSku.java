package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookSkuBase;
import com.siyoumi.util.XStr;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_book_sku", schema = "wx_app_x")
public class BookSku
        extends BookSkuBase {
    public String skuName() {
        return XStr.concat(getBsku_day() + 1 + "天" + getBsku_day() + "晚");
    }
}
