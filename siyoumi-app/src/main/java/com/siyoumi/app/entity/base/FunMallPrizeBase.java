package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FunMallPrize;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FunMallPrizeBase
        extends EntityBase<FunMallPrize>
        implements Serializable
{
    @Override
    public String prefix(){
        return "fprize_";
    }

    static public String table() {
        return "wx_app.t_fun_mall_prize";
    }

    static public String tableKey() {
        return "fprize_id";
    }


	@TableId(value = "fprize_id", type = IdType.INPUT)
	private String fprize_id;
	private String fprize_x_id;
	private LocalDateTime fprize_create_date;
	private LocalDateTime fprize_update_date;
	private String fprize_acc_id;
	private Integer fprize_fun;
	private String fprize_group_id;
	private Integer fprize_get_total;
	private LocalDateTime fprize_begin_date;
	private LocalDateTime fprize_end_date;
	private Integer fprize_enable;
	private Integer fprize_order;

}
