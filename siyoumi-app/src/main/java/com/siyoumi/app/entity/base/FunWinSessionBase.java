package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FunWinSession;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FunWinSessionBase
        extends EntityBase<FunWinSession>
        implements Serializable
{
    @Override
    public String prefix(){
        return "fwins_";
    }

    static public String table() {
        return "wx_app.t_fun_win_session";
    }

    static public String tableKey() {
        return "fwins_id";
    }


	@TableId(value = "fwins_id", type = IdType.INPUT)
	private String fwins_id;
	private String fwins_acc_id;
	private String fwins_x_id;
	private LocalDateTime fwins_create_date;
	private LocalDateTime fwins_update_date;
	private String fwins_name;
	private LocalDateTime fwins_begin_date;
	private LocalDateTime fwins_end_date;
	private Long fwins_fun_num;
	private Long fwins_prize_total;
	private Long fwins_user_total;
	private LocalDateTime fwins_win_date;
	private Integer fwins_win;
	private Integer fwins_enable;
	private LocalDateTime fwins_enable_date;
	private Integer fwins_order;

}
