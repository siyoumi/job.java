package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.LnumNumSet;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class LnumNumSetBase
        extends EntityBase<LnumNumSet>
        implements Serializable {
    @Override
    public String prefix() {
        return "lnset_";
    }

    static public String table() {
        return "wx_app.t_lnum_num_set";
    }

    static public String tableKey() {
        return "lnset_id";
    }


    @TableId(value = "lnset_id", type = IdType.INPUT)
    private String lnset_id;
    private String lnset_x_id;
    private LocalDateTime lnset_create_date;
    private LocalDateTime lnset_update_date;
    private String lnset_acc_id;
    private String lnset_group_id;
    private String lnset_name;
    private LocalDateTime lnset_get_date_begin;
    private LocalDateTime lnset_get_date_end;
    private String lnset_pic;
    private String lnset_pic_bg;
    private Integer lnset_user_get_max;
    private Integer lnset_fun_enable;
    private Long lnset_fun;
    private Long lnset_del;

}
