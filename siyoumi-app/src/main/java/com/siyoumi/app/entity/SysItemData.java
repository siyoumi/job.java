package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysItemDataBase;
import com.siyoumi.util.XJson;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;


@Data
@Accessors(chain = true)
@TableName(value = "t_sys_item_data", schema = "wx_app")
public class SysItemData
        extends SysItemDataBase {
    public Map<String, String> data() {
        return XJson.parseObject(getIdata_data(), Map.class);
    }
}
