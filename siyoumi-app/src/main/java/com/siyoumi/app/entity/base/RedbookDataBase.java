package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.RedbookData;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class RedbookDataBase
        extends EntityBase<RedbookData>
        implements Serializable
{
    @Override
    public String prefix(){
        return "rdata_";
    }

    static public String table() {
        return "wx_app.t_redbook_data";
    }

    static public String tableKey() {
        return "rdata_id";
    }


	@TableId(value = "rdata_id", type = IdType.INPUT)
	private String rdata_id;
	private String rdata_x_id;
	private LocalDateTime rdata_create_date;
	private LocalDateTime rdata_update_date;
	private String rdata_uix;
	private Integer rdata_type;
	private String rdata_uid;
	private String rdata_id_src;
    private String rdata_quan_id;

}
