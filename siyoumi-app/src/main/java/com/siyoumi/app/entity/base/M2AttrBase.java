package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2Attr;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2AttrBase
        extends EntityBase<M2Attr>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mattr_";
    }

    static public String table() {
        return "wx_app.t_m2_attr";
    }

    static public String tableKey() {
        return "mattr_id";
    }


	@TableId(value = "mattr_id", type = IdType.INPUT)
	private String mattr_id;
	private String mattr_x_id;
	private LocalDateTime mattr_create_date;
	private LocalDateTime mattr_update_date;
	private String mattr_spu_id;
	private String mattr_name;
	private Integer mattr_order;
	private Long mattr_del;

}
