package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FksFile;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FksFileBase
        extends EntityBase<FksFile>
        implements Serializable {
    @Override
    public String prefix() {
        return "ffile_";
    }

    static public String table() {
        return "wx_app_x.t_fks_file";
    }

    static public String tableKey() {
        return "ffile_id";
    }


    @TableId(value = "ffile_id", type = IdType.AUTO)
    private Long ffile_id;
    private String ffile_x_id;
    private LocalDateTime ffile_create_date;
    private LocalDateTime ffile_update_date;
    private Integer ffile_type;
    private String ffile_type_id;
    private Integer ffile_folder;
    private String ffile_folder_id;
    private String ffile_data_type;
    private String ffile_acc_id;
    private String ffile_uid;
    private String ffile_department_id;
    private String ffile_name;
    private Integer ffile_file_type;
    private String ffile_path;
    private Long ffile_file_size;
    private String ffile_file_url;
    private String ffile_file_ext;
    private Integer ffile_auth;
    private Integer ffile_order;
    private Long ffile_del;
    private String ffile_note_id;

}
