package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookItem;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookItemBase
        extends EntityBase<BookItem>
        implements Serializable {
    @Override
    public String prefix() {
        return "bitem_";
    }

    static public String table() {
        return "wx_app_x.t_book_item";
    }

    static public String tableKey() {
        return "bitem_id";
    }


    @TableId(value = "bitem_id", type = IdType.INPUT)
    private String bitem_id;
    private String bitem_x_id;
    private String bitem_acc_id;
    private LocalDateTime bitem_create_date;
    private LocalDateTime bitem_update_date;
    private String bitem_pid;
    private String bitem_type;
    private String bitem_name;
    private String bitem_pic;
    private String bitem_str_00;
    private Integer bitem_order;

}
