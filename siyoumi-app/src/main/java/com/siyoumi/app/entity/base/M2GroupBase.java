package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2Group;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2GroupBase
        extends EntityBase<M2Group>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mgroup_";
    }

    static public String table() {
        return "wx_app.t_m2_group";
    }

    static public String tableKey() {
        return "mgroup_id";
    }


	@TableId(value = "mgroup_id", type = IdType.INPUT)
	private String mgroup_id;
	private String mgroup_x_id;
	private LocalDateTime mgroup_create_date;
	private LocalDateTime mgroup_update_date;
	private String mgroup_acc_id;
	private String mgroup_name;
	private Integer mgroup_order;

}
