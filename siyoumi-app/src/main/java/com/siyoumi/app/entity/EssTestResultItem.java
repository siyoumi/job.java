package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.EssTestResultItemBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_ess_test_result_item", schema = "wx_app_x")
public class EssTestResultItem
        extends EssTestResultItemBase
{

}
