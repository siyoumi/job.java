package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysMemberLv;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysMemberLvBase
        extends EntityBase<SysMemberLv>
        implements Serializable {
    @Override
    public String prefix() {
        return "memlv_";
    }

    static public String table() {
        return "wx_app.t_s_member_lv";
    }

    static public String tableKey() {
        return "memlv_id";
    }


    @TableId(value = "memlv_id", type = IdType.INPUT)
    private Long memlv_id;
    private String memlv_x_id;
    private LocalDateTime memlv_create_date;
    private LocalDateTime memlv_update_date;
    private String memlv_uix;
    private String memlv_name;
    private String memlv_logo;
    private String memlv_pic;
    private String memlv_pic_prize;
    private Integer memlv_level;
    private Integer memlv_lv_num;
    private String memlv_lv_prize;
    private String memlv_lv_power;

}
