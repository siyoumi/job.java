package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.RedbookDataBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_redbook_data", schema = "wx_app")
public class RedbookData
        extends RedbookDataBase
{

}
