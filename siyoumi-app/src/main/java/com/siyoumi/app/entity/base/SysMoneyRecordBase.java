package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysMoneyRecord;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysMoneyRecordBase
        extends EntityBase<SysMoneyRecord>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mrec_";
    }

    static public String table() {
        return "wx_app.t_s_money_record";
    }

    static public String tableKey() {
        return "mrec_id";
    }


	@TableId(value = "mrec_id", type = IdType.INPUT)
	private Long mrec_id;
	private String mrec_x_id;
	private LocalDateTime mrec_create_date;
	private LocalDateTime mrec_update_date;
	private String mrec_app_id;
	private String mrec_uix;
	private String mrec_id_src;
	private String mrec_openid;
	private BigDecimal mrec_num;
	private String mrec_desc;
	private String mrec_id_00;
	private String mrec_id_01;

}
