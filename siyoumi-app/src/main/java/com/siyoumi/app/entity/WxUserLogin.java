package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.WxUserLoginBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_wx_user_login", schema = "wx_app")
public class WxUserLogin
        extends WxUserLoginBase {
}
