package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.siyoumi.app.entity.base.LnumSessionBase;
import com.siyoumi.util.XDate;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Duration;
import java.time.LocalDateTime;


@Data
@Accessors(chain = true)
@TableName(value = "t_lnum_session", schema = "wx_app")
public class LnumSession
        extends LnumSessionBase {
    public boolean win() {
        return getLns_win() == 1;
    }

    @JsonIgnore
    public Long winLeftSeconds() {
        Duration between = Duration.between(LocalDateTime.now(), getLns_win_date_end());
        long s = between.getSeconds();
        if (s < 0) {
            s = 0;
        }

        return s;
    }
}
