package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookStoreGroupBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_book_store_group", schema = "wx_app_x")
public class BookStoreGroup
        extends BookStoreGroupBase
{

}
