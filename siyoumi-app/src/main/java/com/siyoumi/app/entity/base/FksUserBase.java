package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FksUser;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FksUserBase
        extends EntityBase<FksUser>
        implements Serializable {
    @Override
    public String prefix() {
        return "fuser_";
    }

    static public String table() {
        return "wx_app_x.t_fks_user";
    }

    static public String tableKey() {
        return "fuser_id";
    }


    @TableId(value = "fuser_id", type = IdType.INPUT)
    private String fuser_id;
    private String fuser_x_id;
    private LocalDateTime fuser_create_date;
    private LocalDateTime fuser_update_date;
    private String fuser_department_id;
    private String fuser_position_id;
    private Long fuser_note_count;

}
