package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2Order;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2OrderBase
        extends EntityBase<M2Order>
        implements Serializable {
    @Override
    public String prefix() {
        return "morder_";
    }

    static public String table() {
        return "wx_app.t_m2_order";
    }

    static public String tableKey() {
        return "morder_id";
    }


    @TableId(value = "morder_id", type = IdType.INPUT)
    private String morder_id;
    private String morder_x_id;
    private LocalDateTime morder_create_date;
    private LocalDateTime morder_update_date;
    private String morder_acc_id;
    private String morder_app_id;
    private String morder_openid;
    private BigDecimal morder_price;
    private BigDecimal morder_price_pay;
    private String morder_addr_user_name;
    private String morder_addr_user_phone;
    private String morder_addr_address;
    private Integer morder_enable;
    private Integer morder_pay;
    private Integer morder_cancel;
    private Integer morder_deliver;
    private LocalDateTime morder_deliver_date;
    private String morder_deliver_no;
    private String morder_deliver_store;
    private Integer morder_done;
    private LocalDateTime morder_done_date;

}
