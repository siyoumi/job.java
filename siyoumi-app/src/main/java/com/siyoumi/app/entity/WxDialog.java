package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.WxDialogBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_wx_dialog", schema = "wx_app")
public class WxDialog
        extends WxDialogBase
{

}
