package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.SysItemBase;
import com.siyoumi.app.sys.vo.CommonItemData;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XStr;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;


@Data
@Accessors(chain = true)
@TableName(value = "t_sys_item", schema = "wx_app")
public class SysItem
        extends SysItemBase {
    public List<CommonItemData> commonData() {
        return commonData(CommonItemData.class);
    }

    public <T> List<T> commonData(Class<T> clazz) {
        if (XStr.isNullOrEmpty(getItem_data())) {
            return new ArrayList<>();
        }
        List<T> list = XJson.parseArray(getItem_data(), clazz);


        return list;
    }
}
