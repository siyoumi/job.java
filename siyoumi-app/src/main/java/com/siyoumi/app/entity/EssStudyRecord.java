package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.EssStudyRecordBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_ess_study_record", schema = "wx_app_x")
public class EssStudyRecord
        extends EssStudyRecordBase
{

}
