package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.WxDialog;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class WxDialogBase
        extends EntityBase<WxDialog>
        implements Serializable
{
    @Override
    public String prefix(){
        return "wxdlg_";
    }

    static public String table() {
        return "wx_app.t_wx_dialog";
    }

    static public String tableKey() {
        return "wxdlg_id";
    }


	@TableId(value = "wxdlg_id", type = IdType.INPUT)
	private String wxdlg_id;
	private String wxdlg_x_id;
	private LocalDateTime wxdlg_create_date;
	private LocalDateTime wxdlg_update_date;
	private String wxdlg_ToUserName;
	private String wxdlg_FromUserName;
	private String wxdlg_CreateTime;
	private LocalDateTime wxdlg_CreateTime_date;
	private String wxdlg_MsgType;
	private String wxdlg_Content;
	private String wxdlg_MsgId;
	private String wxdlg_MenuId;
	private String wxdlg_Event;
	private String wxdlg_EventKey;
	private String wxdlg_Ticket;

}
