package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.FunAuctionSetBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_fun_auction_set", schema = "wx_app")
public class FunAuctionSet
        extends FunAuctionSetBase
{

}
