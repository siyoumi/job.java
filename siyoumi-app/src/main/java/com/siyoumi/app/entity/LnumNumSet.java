package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.LnumNumSetBase;
import com.siyoumi.util.XDate;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_lnum_num_set", schema = "wx_app")
public class LnumNumSet
        extends LnumNumSetBase {

    public boolean expire() {
        return getLnset_get_date_end().isBefore(XDate.now());
    }
}
