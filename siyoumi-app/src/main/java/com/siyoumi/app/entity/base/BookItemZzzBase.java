package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookItemZzz;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookItemZzzBase
        extends EntityBase<BookItemZzz>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bizz_";
    }

    static public String table() {
        return "wx_app_x.t_book_item_zzz";
    }

    static public String tableKey() {
        return "bizz_id";
    }


	@TableId(value = "bizz_id", type = IdType.INPUT)
	private String bizz_id;
	private String bizz_x_id;
	private String bizz_acc_id;
	private LocalDateTime bizz_create_date;
	private LocalDateTime bizz_update_date;
	private String bizz_store_id;
	private String bizz_type;
	private String bizz_type_id;
    private String bizz_item_id;

}
