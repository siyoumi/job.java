package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookSetDay;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookSetDayBase
        extends EntityBase<BookSetDay>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bsday_";
    }

    static public String table() {
        return "wx_app_x.t_book_set_day";
    }

    static public String tableKey() {
        return "bsday_id";
    }


	@TableId(value = "bsday_id", type = IdType.INPUT)
	private String bsday_id;
	private String bsday_x_id;
	private String bsday_acc_id;
	private LocalDateTime bsday_create_date;
	private LocalDateTime bsday_update_date;
	private String bsday_set_id;
	private LocalDateTime bsday_date;
	private BigDecimal bsday_add_price_breakfast;
	private BigDecimal bsday_add_price_lunch;
	private BigDecimal bsday_add_price_dinner;
	private BigDecimal bsday_add_price;

}
