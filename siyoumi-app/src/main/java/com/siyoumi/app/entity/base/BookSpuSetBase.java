package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookSpuSet;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookSpuSetBase
        extends EntityBase<BookSpuSet>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bset_";
    }

    static public String table() {
        return "wx_app_x.t_book_spu_set";
    }

    static public String tableKey() {
        return "bset_id";
    }


	@TableId(value = "bset_id", type = IdType.INPUT)
	private String bset_id;
	private String bset_x_id;
	private String bset_acc_id;
	private LocalDateTime bset_create_date;
	private LocalDateTime bset_update_date;
	private String bset_spu_id;
	private String bset_set_id;
	private Integer bset_order;

}
