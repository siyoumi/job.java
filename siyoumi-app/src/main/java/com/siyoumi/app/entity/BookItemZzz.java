package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.BookItemZzzBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_book_item_zzz", schema = "wx_app_x")
public class BookItemZzz
        extends BookItemZzzBase
{

}
