package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FksCollect;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FksCollectBase
        extends EntityBase<FksCollect>
        implements Serializable
{
    @Override
    public String prefix(){
        return "fc_";
    }

    static public String table() {
        return "wx_app_x.t_fks_collect";
    }

    static public String tableKey() {
        return "fc_id";
    }


	@TableId(value = "fc_id", type = IdType.INPUT)
	private String fc_id;
	private String fc_x_id;
	private LocalDateTime fc_create_date;
	private LocalDateTime fc_update_date;
	private Integer fc_type;
	private String fc_type_id;
	private String fc_uid;

}
