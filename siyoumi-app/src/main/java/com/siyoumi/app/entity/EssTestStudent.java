package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.EssTestStudentBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_ess_test_student", schema = "wx_app_x")
public class EssTestStudent
        extends EssTestStudentBase
{

}
