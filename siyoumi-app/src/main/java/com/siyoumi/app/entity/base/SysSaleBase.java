package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysSale;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysSaleBase
        extends EntityBase<SysSale>
        implements Serializable {
    @Override
    public String prefix() {
        return "sale_";
    }

    static public String table() {
        return "wx_app.t_s_sale";
    }

    static public String tableKey() {
        return "sale_id";
    }


    @TableId(value = "sale_id", type = IdType.INPUT)
    private String sale_id;
    private String sale_x_id;
    private String sale_acc_id;
    private LocalDateTime sale_create_date;
    private LocalDateTime sale_update_date;
    private Integer sale_level;
    private String sale_type;
    private String sale_uid;
    private String sale_name;
    private Integer sale_state;
    private LocalDateTime sale_state_date;
    private BigDecimal sale_money_left;
    private BigDecimal sale_money_total;

}
