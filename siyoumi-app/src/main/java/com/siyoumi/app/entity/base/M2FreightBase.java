package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2Freight;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2FreightBase
        extends EntityBase<M2Freight>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mfit_";
    }

    static public String table() {
        return "wx_app.t_m2_freight";
    }

    static public String tableKey() {
        return "mfit_id";
    }


	@TableId(value = "mfit_id", type = IdType.INPUT)
	private String mfit_id;
	private String mfit_x_id;
	private LocalDateTime mfit_create_date;
	private LocalDateTime mfit_update_date;
	private String mfit_acc_id;
	private String mfit_name;
	private Integer mfit_disabled;
	private Long mfit_del;

}
