package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.BookStoreGroup;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class BookStoreGroupBase
        extends EntityBase<BookStoreGroup>
        implements Serializable
{
    @Override
    public String prefix(){
        return "bsgroup_";
    }

    static public String table() {
        return "wx_app_x.t_book_store_group";
    }

    static public String tableKey() {
        return "bsgroup_id";
    }


	@TableId(value = "bsgroup_id", type = IdType.INPUT)
	private String bsgroup_id;
	private String bsgroup_x_id;
	private String bsgroup_acc_id;
	private LocalDateTime bsgroup_create_date;
	private LocalDateTime bsgroup_update_date;
	private String bsgroup_name;
	private String bsgroup_pic;
	private Integer bsgroup_order;

}
