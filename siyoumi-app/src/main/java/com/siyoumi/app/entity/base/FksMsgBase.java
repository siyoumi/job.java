package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.FksMsg;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class FksMsgBase
        extends EntityBase<FksMsg>
        implements Serializable {
    @Override
    public String prefix() {
        return "fmsg_";
    }

    static public String table() {
        return "wx_app_x.t_fks_msg";
    }

    static public String tableKey() {
        return "fmsg_id";
    }


    @TableId(value = "fmsg_id", type = IdType.INPUT)
    private String fmsg_id;
    private String fmsg_x_id;
    private LocalDateTime fmsg_create_date;
    private LocalDateTime fmsg_update_date;
    private String fmsg_acc_id;
    private Integer fmsg_type;
    private String fmsg_pic;
    private String fmsg_title;
    private String fmsg_text;
    private String fmsg_keyword;
    private LocalDateTime fmsg_release_date;
    private String fmsg_src;
    private String fmsg_tiao_title;
    private String fmsg_tiao_desc;
    private String fmsg_file_json;
    private Integer fmsg_order;
    private Long fmsg_del;
    private Long fmsg_collect_count;

}
