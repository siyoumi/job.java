package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysFile;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class SysFileBase
        extends EntityBase<SysFile>
        implements Serializable
{
    @Override
    public String prefix(){
        return "file_";
    }

    static public String table() {
        return "wx_app.t_sys_file";
    }

    static public String tableKey() {
        return "file_id";
    }


	@TableId(value = "file_id", type = IdType.INPUT)
	private String file_id;
	private String file_x_id;
	private LocalDateTime file_create_date;
	private LocalDateTime file_update_date;
	private String file_uid;
	private String file_name;
	private String file_file_ext;
	private String file_path;

}
