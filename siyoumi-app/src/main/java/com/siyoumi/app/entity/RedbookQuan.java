package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.RedbookQuanBase;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_redbook_quan", schema = "wx_app")
public class RedbookQuan
        extends RedbookQuanBase
{

}
