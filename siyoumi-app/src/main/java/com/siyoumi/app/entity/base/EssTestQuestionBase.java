package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.EssTestQuestion;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class EssTestQuestionBase
        extends EntityBase<EssTestQuestion>
        implements Serializable
{
    @Override
    public String prefix(){
        return "etques_";
    }

    static public String table() {
        return "wx_app_x.t_ess_test_question";
    }

    static public String tableKey() {
        return "etques_id";
    }


	@TableId(value = "etques_id", type = IdType.INPUT)
	private String etques_id;
	private String etques_x_id;
	private LocalDateTime etques_create_date;
	private LocalDateTime etques_update_date;
	private String etques_module_id;
	private String etques_test_id;
	private String etques_question_id;
	private Integer etques_fun;
	private Integer etques_order;

}
