package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.SysAddress;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class SysAddressBase
        extends EntityBase<SysAddress>
        implements Serializable
{
    @Override
    public String prefix(){
        return "addr_";
    }

    static public String table() {
        return "wx_app.t_s_address";
    }

    static public String tableKey() {
        return "addr_id";
    }


	@TableId(value = "addr_id", type = IdType.INPUT)
	private String addr_id;
	private String addr_x_id;
	private LocalDateTime addr_create_date;
	private LocalDateTime addr_update_date;
	private String addr_openid;
	private String addr_name;
	private String addr_phone;
	private String addr_province;
	private String addr_city;
	private String addr_district;
	private String addr_address;
	private String addr_str_00;
	private Long addr_def;
	private String addr_tag;
	private Long addr_order;

}
