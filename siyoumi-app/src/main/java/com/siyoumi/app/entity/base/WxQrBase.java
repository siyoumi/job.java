package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.WxQr;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class WxQrBase
        extends EntityBase<WxQr>
        implements Serializable
{
    @Override
    public String prefix(){
        return "wxqr_";
    }

    static public String table() {
        return "wx_app.t_wx_qr";
    }

    static public String tableKey() {
        return "wxqr_id";
    }


	@TableId(value = "wxqr_id", type = IdType.INPUT)
	private String wxqr_id;
	private String wxqr_x_id;
	private LocalDateTime wxqr_create_date;
	private LocalDateTime wxqr_update_date;
	private String wxqr_key;
	private String wxqr_type;
	private String wxqr_type_qr;
	private String wxqr_name;
	private LocalDateTime wxqr_expire_date;
	private Long wxqr_reply_id;
	private String wxqr_info;
	private String wxqr_path;
	private String wxqr_append_data;

}
