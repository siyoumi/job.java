package com.siyoumi.app.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siyoumi.app.entity.base.CorpStoreBase;
import com.siyoumi.util.XDate;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName(value = "t_corp_store", schema = "wx_app_x")
public class CorpStore
        extends CorpStoreBase {
    //使用时长(天)
    public Long useDay() {
        return XDate.between(getCstore_create_date(), XDate.now()).toDays();
    }
}
