package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.WxUser;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class WxUserBase
        extends EntityBase<WxUser>
        implements Serializable {
    @Override
    public String prefix() {
        return "wxuser_";
    }

    static public String table() {
        return "wx_app.t_wx_user";
    }

    static public String tableKey() {
        return "wxuser_id";
    }

    static public String tableOpenid() {
        return "wxuser_openid";
    }


    @TableId(value = "wxuser_id", type = IdType.INPUT)
    private String wxuser_id;
    private String wxuser_x_id;
    private LocalDateTime wxuser_create_date;
    private LocalDateTime wxuser_update_date;
    private LocalDateTime wxuser_user_data_update_date;
    private String wxuser_openid;
    private String wxuser_unionid;
    private String wxuser_nickname;
    private String wxuser_headimgurl;
    private String wxuser_subscribe;
    private String wxuser_subscribe_time;
    private LocalDateTime wxuser_unsub_time;
    private String wxuser_true_name;
    private String wxuser_phone;
    private String wxuser_desc;
    private String wxuser_uid;

}
