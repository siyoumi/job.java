package com.siyoumi.app.entity.base;

import com.siyoumi.entity.EntityBase;
import com.siyoumi.app.entity.M2Store;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import java.time.LocalDateTime;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@FieldNameConstants(innerTypeName = "F")
public class M2StoreBase
        extends EntityBase<M2Store>
        implements Serializable
{
    @Override
    public String prefix(){
        return "mstore_";
    }

    static public String table() {
        return "wx_app.t_m2_store";
    }

    static public String tableKey() {
        return "mstore_id";
    }


	@TableId(value = "mstore_id", type = IdType.INPUT)
	private String mstore_id;
	private String mstore_x_id;
	private String mstore_acc_id;
	private LocalDateTime mstore_create_date;
	private LocalDateTime mstore_update_date;
	private String mstore_uid;
	private String mstore_name;
	private String mstore_phone;
	private String mstore_address;
	private String mstore_desc;
	private String mstore_pic_desc;

}
