package com.siyoumi.app.sys.service.wx_center;

import com.siyoumi.app.sys.service.WxTokenService;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;

/**
 * 企业微信回复处理
 */
@Slf4j
public class CenterHandlerCorp
        extends CenterHandler {
    @Override
    public String checkSign() {
        if (!"get".equalsIgnoreCase(XHttpContext.getHttpServletRequest().getMethod())) {
            return null;
        }

        InputData inputData = InputData.fromRequest();

        String timestamp = inputData.input("timestamp");
        String nonce = inputData.input("nonce");
        String signature = inputData.input("msg_signature");
        String echostr = inputData.input("echostr");
        if (XStr.isNullOrEmpty(signature)) {
            XValidator.err(20039, "miss msg_signature");
        }

        String token = WxTokenService.getBean(getEntityConfig()).getAccessToken();

        String[] ss = new String[]{token, timestamp, nonce, echostr};
        String str1_sign = getSign(ss);

        if (!str1_sign.equals(signature)) {
            String errmsg = "签名失败 , " + str1_sign + " != " + signature;
            XValidator.err(50069, errmsg);
        }

        return decryptTxt(echostr);
    }
}
