package com.siyoumi.app.sys.service;

import com.siyoumi.util.XStr;
import lombok.Data;

@Data
public class WxPayConfig {
    String x;
    String appId;
    String mchId;
    String mchKey;
    String paySerial;

    String appIdSub;
    String mchIdSub;

    /**
     * 服务商模式
     */
    public boolean isPartner() {
        return XStr.hasAnyText(getMchIdSub());
    }
}
