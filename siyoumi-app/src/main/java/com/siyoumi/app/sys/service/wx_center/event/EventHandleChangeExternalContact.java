package com.siyoumi.app.sys.service.wx_center.event;

import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerData;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;

//企业微信群，添加，删除员工，等操作，更新任务处理时间
@Slf4j
public class EventHandleChangeExternalContact
        implements ICenterHandle {
    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        CenterHandlerData data = ctx.getHandleData();
        ctx.setFinal(); //终止标记

        String externalUserId = (String) data.getInMsg().get("ExternalUserID");
        String userId = (String) data.getInMsg().get("UserID");
        String changeType = (String) data.getInMsg().get("ChangeType");
        Integer createTime = NumberUtils.toInt((String) data.getInMsg().get("CreateTime"));
        LogPipeLine.getTl().setLogMsgFormat("external_user_id: {0}", externalUserId); //客户userId
        LogPipeLine.getTl().setLogMsgFormat("change_type: {0}", changeType); //add_external_contact: 添加员工, 删除员工 del_follow_user
        log.info("user_id: {}", userId); //员工userId


//        T__t_a_groupchat_user entityGroupUser = SvcGroupchatUser.getBean().getEntityByUserid(ctx.getSiteId(), externalUserId);
//        if (entityGroupUser != null) {
//            SvcGroupchatUser.getBean().updateTaskHandle(entityGroupUser, null);
//        }

        //T__t_a_groupchat_staff entityStaff = SvcGroupchatStaff.getBean().getEntity(userId, false);
        ////添加、删除员工
        //if (entityStaff == null) {
        //    entityStaff = new T__t_a_groupchat_staff();
        //    entityStaff.setGcsf_site_id(data.getSiteId());
        //    entityStaff.setGcsf_userid(userId);
        //    entityStaff.funcSetID_auto();
        //    entityStaff.setGcsf_delete(0);
        //    try {
        //        SvcGroupchatStaff.getApp().funcInsert(entityStaff);
        //    } catch (Exception ex) {
        //        log.error(ex.getMessage());
        //        ex.printStackTrace();
        //    }
        //}
        //
        //ContextUtil.funcSetData(ISvcBase.KEY_SITE_ID, data.getSiteId());
        //
        //LocalDateTime createDate = Helper.funcTimestampToDateTime(createTime + "");
        //LogPipeLine.getTl().setLogMsgFormat("员工: {0}", entityStaff.getGcsf_delete() == 0 ? "在职" : "标记离职");
        //if (entityStaff.getGcsf_delete() == 0) {
        //    if ("add_external_contact".equals(changeType)) {
        //        //添加员工
        //        if (LocalDateTime.now().isBefore(entityStaff.getGcsf_to_handle_date())) {
        //            SvcGroupchatStaff.getBean().updateStaffHandleDate(userId);
        //        }
        //
        //        SvcGroupchatStaff.getBean().updateStaffUserExternal(userId, externalUserId, 0, createDate, true, true);
        //    }
        //
        //    if ("del_follow_user".equals(changeType) //客户删除员工
        //            || "del_external_contact".equals(changeType) //员工删除客户
        //    ) {
        //        //删除员工
        //        SvcGroupchatStaff.getBean().updateStaffUserExternal(userId, externalUserId, 1, createDate, true);
        //    }
        //}

        return null;
    }
}
