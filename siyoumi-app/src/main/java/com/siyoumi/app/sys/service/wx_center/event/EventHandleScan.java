package com.siyoumi.app.sys.service.wx_center.event;

import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerData;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerXml;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.util.XReturn;

//扫二维码事件
public class EventHandleScan
        implements ICenterHandle {
    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        CenterHandlerData data = ctx.getHandleData();
        CenterHandlerXml inMsg = data.getInMsg();
        String word = inMsg.getEventKey();

        ctx.getHandleData().setWord(word);
        return null;
    }
}
