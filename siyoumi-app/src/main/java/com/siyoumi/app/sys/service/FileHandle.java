package com.siyoumi.app.sys.service;

import com.siyoumi.app.sys.service.file.FileHandleAliyun;
import com.siyoumi.app.sys.service.file.FileHandleSys;
import com.siyoumi.app.sys.service.file.entity.FileUploadData;
import com.siyoumi.component.XApp;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

//文件接口
@Slf4j
public abstract class FileHandle {
    public static FileHandle of(String type) {
        FileHandle app = null;
        switch (type) {
            case "":
            case "sys":
                app = new FileHandleSys();
                break;
            case "aliyun":
                app = new FileHandleAliyun();
                break;
            default:
                XValidator.err(EnumSys.SYS.getR("类型未开发"));
        }

        return app;
    }

    abstract protected XReturn saveFile(FileUploadData data) throws IOException;

    /**
     * 获取访问文件链接
     *
     * @param fileVal
     */
    public String getFileUrl(String fileVal) {
        return XApp.fileUrl(fileVal);
    }

    public XReturn uploadFile(FileUploadData data) {
        if (data.getMultFile() == null) {
            return EnumSys.FILE_EXISTS.getR();
        }

        XReturn r;
        try {
            r = uploadFileMain(data);
        } catch (Exception ex) {
            throw ex;
        }

        return r;
    }

    //主方法
    @SneakyThrows
    public XReturn uploadFileMain(FileUploadData data) {
        MultipartFile file = data.getMultFile();
        String fileName = file.getOriginalFilename();
        String fileExt = getFileExt(fileName);
        fileName = fileName.replace("." + fileExt, ""); //去掉后缀，例：123.jpg 变 123
        log.debug("文件名：{}", fileName);
        log.debug("文件后缀：{}", fileExt);
        if (!data.getFileExtenstions().contains(fileExt)) {
            return EnumSys.FILE_EXTENSTION.getR(XStr.concat("只能上传指定格式的文件[", String.join(",", data.getFileExtenstions()), "]"));
        }

        long fileSize = file.getSize();
        long fileSizeMax = data.getFileSizeMax() * 1024 * 1024;
        if (fileSize > fileSizeMax) {
            return EnumSys.FILE_SIZE.getR(XStr.concat("上传文件不能超", data.getFileSizeMax() + "M"));
        }

        XReturn r = saveFile(data);
        if (r.err()) {
            return r;
        }

        String fileVal = r.getData("file_val");
        String fileUrl = r.getData("file_url");
        r.setData("file_name", fileName);
        r.setData("file_size", fileSize);
        r.setData("file_ext", fileExt);
        //后台数据一致
        HashMap<String, String> img = new HashMap<>();
        img.put("id", fileVal);
        img.put("url", fileUrl);

        r.setData("img", img);

        return r;
    }


    protected String getFileExt(String fileName) {
        String[] fileTypeData = fileName.split("\\.");
        String fileExt = "";
        if (fileTypeData.length > 1) {
            fileExt = fileTypeData[fileTypeData.length - 1];
        }

        return fileExt;
    }
}
