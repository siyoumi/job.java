package com.siyoumi.app.sys.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.NotBlank;

//生成小程序码
@Data
public class WxAppQrVo {
    @HasAnyText(message = "miss scene")
    private String scene;
    private String path;

    private Integer is_hyaline = 0;
    private String env_version = "release"; //正式版为 release，体验版为 trial，开发版为 develop
}
