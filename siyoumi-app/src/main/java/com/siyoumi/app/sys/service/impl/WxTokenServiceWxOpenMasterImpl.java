package com.siyoumi.app.sys.service.impl;

import com.siyoumi.app.sys.service.WxTokenService;
import com.siyoumi.app.sys.service.wxapi.ApiConfig;
import com.siyoumi.app.sys.service.wxapi.WxApiOpen;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;

//开放平台-主站点
@Slf4j
public class WxTokenServiceWxOpenMasterImpl
        extends WxTokenService {

    @Override
    protected String getApiToken(SysAccsuperConfig entityConfig) {
        ApiConfig apiConfig = new ApiConfig();
        apiConfig.setAppId(entityConfig.getAconfig_app_id());
        apiConfig.setAppSecret(entityConfig.getAconfig_app_secret());

        WxApiOpen api = WxApiOpen.getIns(null);
        api.setConfig(apiConfig);
        XReturn r = api.masterToken(entityConfig.getAconfig_js_ticket());
        if (r.err()) {
            XValidator.err(r);
        }

        return r.getData("component_access_token");
    }

    @Override
    protected String getApiJsTicket(SysAccsuperConfig entityConfig) {
        return null;
    }
}
