package com.siyoumi.app.sys.service.web_oauth;

import com.siyoumi.app.sys.vo.WebOauthCallBackData;
import com.siyoumi.app.sys.vo.WebOauthVo;
import com.siyoumi.component.XJwt;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;

//小程序
@Slf4j
public class WebOauthWxapp
        extends WebOauth {
    @Override
    public String reHandle(WebOauthVo data) {
        if (XStr.isNullOrEmpty(data.getCode())) {
            XValidator.errWeb(EnumSys.MISS_VAL.getR("miss code"));
        }

        return getCallbackUrl(data);
    }

    @Override
    public WebOauthCallBackData callbackHandleToken(WebOauthVo data) {
        if (XStr.isNullOrEmpty(data.getCode())) {
            XValidator.errWeb(EnumSys.MISS_VAL.getR("miss code"));
        }

        String webToken = getWebToken(data.getCode());

        XReturn r = XJwt.getBean().parseToken(data.getCode());
        String openid = r.getData("openid");

        return WebOauthCallBackData.getIns(openid, webToken);
    }
}
