package com.siyoumi.app.sys.service.wxapi;

import com.siyoumi.app.sys.service.CommonApiServcie;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.util.XHttpClient;
import com.siyoumi.validator.XValidator;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
//微信小程序接口
public class WxApiApp
        extends ApiBase {
    static public WxApiApp getIns(SysAccsuperConfig entityConfig) {
        WxApiApp app = new WxApiApp();
        app.setConfig(entityConfig);
        return app;
    }


    /**
     * 获取用户信息
     *
     * @param code 微信授权code
     */
    public XReturn getUserInfo(@NonNull String code) {
        String apiUrl = getApiUrl("sns/jscode2session");

        HashMap<String, String> data = new HashMap<>();
        data.put("appid", getConfig().getAppId());
        data.put("secret", getConfig().getAppSecret());
        data.put("js_code", code);
        data.put("grant_type", "authorization_code");

        if (getConfig().isOpenClient()) {
            apiUrl = getApiUrl("sns/component/jscode2session");

            data.put("component_appid", getConfig().getParent().getAppId());
            data.put("component_access_token", getConfig().getParent().getAccessToken());
            data.remove("secret");
        }

        String query = XStr.queryFromMap(data, null, false);
        String url = XStr.concat(apiUrl, "?", query);

        return api(url);
    }

    /**
     * 获取手机号
     */
    public XReturn getUserPhone(@NonNull String code) {
        Map<String, Object> data = new HashMap<>();
        data.put("code", code);

        String apiUrl = getApiUrl("wxa/business/getuserphonenumber", getConfig().getAccessToken());
        return api(apiUrl, XHttpClient.METHOD_POST, data);
    }

    /**
     * 获取订阅消息模板列表
     */
    public XReturn getTemplate() {
        String apiUrl = getApiUrl("wxaapi/newtmpl/gettemplate", getConfig().getAccessToken());
        return api(apiUrl);
    }

    /**
     * 获取订阅消息模板列表
     */
    public XReturn sendTemplate(Map<String, Object> postData) {
        String apiUrl = getApiUrl("cgi-bin/message/subscribe/send", getConfig().getAccessToken());
        return api(apiUrl, XHttpClient.METHOD_POST, postData);
    }

    /**
     * 小程序发公众号模板消息
     */
    public XReturn sendTemplateWx(String openid, Map<String, Object> templateData) {
        HashMap<String, Object> postData = new HashMap<>();
        postData.put("touser", openid);
        postData.put("mp_template_msg", templateData);

        String apiUrl = getApiUrl("cgi-bin/message/wxopen/template/uniform_send", getConfig().getAccessToken());
        return api(apiUrl, XHttpClient.METHOD_POST, postData);
    }

    /**
     * 生成小程序码
     */
    public byte[] getQrCodeUnlimited(String scene, String page, Map<String, Object> appendData) {
        String apiUrl = getApiUrl("wxa/getwxacodeunlimit", getConfig().getAccessToken());

        Map<String, Object> data = new HashMap<>();
        data.put("scene", scene);
        data.put("page", page);
        if (appendData != null) {
            data.putAll(appendData);
        }

        log.debug(XStr.toJsonStr(data));
        XHttpClient client = XHttpClient.getInstance();
        byte[] returnByte = client.postJsonReturnByte(apiUrl, null, data);
        String returnStr = new String(returnByte);
        log.debug(returnStr);
        if (returnStr.length() < 500) {
            XValidator.err(XReturn.parse(returnStr));
        }
        return returnByte;
    }

    /**
     * 生成小程序码
     *
     * @param path
     * @param appendData
     */
    public XReturn getQrCode(String path, Map<String, Object> appendData) {
        String apiUrl = getApiUrl("wxa/getwxacode", getConfig().getAccessToken());

        Map<String, Object> data = new HashMap<>();
        data.put("path", path);
        if (appendData != null) {
            data.putAll(appendData);
        }

        XHttpClient client = XHttpClient.getInstance();
        byte[] returnByte = client.postJsonReturnByte(apiUrl, null, data);
        String returnStr = new String(returnByte);
        if (returnStr.length() < 500) {
            return XReturn.parse(returnStr);
        }


        CommonApiServcie app = CommonApiServcie.getBean();
        return app.saveFile(returnByte, "png");
    }
}
