package com.siyoumi.app.sys.service.netty;

import com.siyoumi.app.config.boot.ISiyoumiBoot;
import com.siyoumi.app.netty.NettyMqttServer;
import com.siyoumi.app.netty.to.NettyConnectionData;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MqttTestService
        extends NettyMqttServer
        implements ISiyoumiBoot {
    @SneakyThrows
    @Override
    public void run() {
        //log.info("清空redis房间记录");
//        XRedis.getBean().deleteByPattern("MqttTestService:*");

        MqttTestService app = new MqttTestService();
        app.handle(getConnectionData());
    }

    public static void main(String[] args) {
        MqttTestService app = new MqttTestService();
        app.run();
    }

    public static NettyConnectionData getConnectionData() {
        NettyConnectionData data = NettyConnectionData.getIns();
        data.setIp("0.0.0.0");
        data.setPort(8300);
//        data.setPath("/mqtt/test");
        data.setHandlerClazz(MqttTestHandler.class);
        return data;
    }
}
