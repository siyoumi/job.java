package com.siyoumi.app.sys.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//后台扫码登陆
@Data
public class AdminLoginQrVo {
    @HasAnyText(message = "miss oauth code")
    private String oauth_code;
}
