package com.siyoumi.app.sys.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ApprVo
{
    private String metaTitle;

    private String appr_id;
    private String appr_app_id;
    private String appr_pid = "";
    private String appr_file_path;
    private String appr_path_query;
    private String appr_path;
    private String appr_meta_icon = "";
    private Integer appr_hidden = 0;
    private Integer appr_order = 0;

    private List<ApprVo> children = new ArrayList<>();
}
