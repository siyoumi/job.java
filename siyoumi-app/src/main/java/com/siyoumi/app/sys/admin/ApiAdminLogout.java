package com.siyoumi.app.sys.admin;

import com.siyoumi.app.sys.service.AdminService;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.XReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/xadmin/sys/logout")
public class ApiAdminLogout
        extends AdminApiController
{
    @Autowired
    private AdminService adminService;


    @GetMapping()
    public XReturn logout()
    {
        String token = XHttpContext.getToken();
        return adminService.logout(token);
    }
}
