package com.siyoumi.app.sys.service;

import com.siyoumi.app.entity.*;
import com.siyoumi.app.service.SysUvService;
import com.siyoumi.app.sys.vo.VoUvSave;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//uv操作，点赞，收藏
@Slf4j
@Service
public class SvcSysUv
        implements IWebService {
    static public SvcSysUv getBean() {
        return XSpringContext.getBean(SvcSysUv.class);
    }

    static public SysUvService getApp() {
        return SysUvService.getBean();
    }

    /**
     * 这批Id，是否被用户点赞过
     *
     * @param typeIds
     * @param type
     * @param uid
     */
    public List<String> getTypeIds(List<String> typeIds, String type, String uid) {
        if (typeIds.isEmpty()) {
            return new ArrayList<>();
        }

        JoinWrapperPlus<SysUv> query = listQuery(type, typeIds, uid);
        query.select("uv_type_id");
        return getApp().getMaps(query).stream().map(item -> (String) item.get("uv_type_id")).collect(Collectors.toList());
    }

    public JoinWrapperPlus<SysUv> listQuery(String type) {
        InputData inputData = InputData.getIns();
        inputData.put("type", type);

        return listQuery(inputData);
    }

    public JoinWrapperPlus<SysUv> listQuery(String type, List<String> typeIds, String uid) {
        InputData inputData = InputData.getIns();
        inputData.put("type", type);

        JoinWrapperPlus<SysUv> query = listQuery(inputData);
        query.eq("uv_x_id", XHttpContext.getX())
                .in("uv_type_id", typeIds)
                .eq("uv_uid", uid);
        return query;
    }

    public JoinWrapperPlus<SysUv> listQuery(String type, String typeId, String uid) {
        return listQuery(type, List.of(typeId), uid);
    }

    /**
     * select
     *
     * @return query
     */
    public JoinWrapperPlus<SysUv> listQuery(InputData inputData) {
        String type = inputData.input("type");
        String uid = inputData.input("uid");

        JoinWrapperPlus<SysUv> query = getApp().join();
        query.eq("uv_x_id", XHttpContext.getX())
                .eq("uv_type", type);

        if (XStr.hasAnyText(uid)) { //uid
            query.eq("uv_uid", uid);
        }

        return query;
    }

    /**
     * 是否已存在记录
     *
     * @param type
     * @param typeId
     * @param uid
     */
    public Boolean exists(String type, String typeId, String uid) {
        JoinWrapperPlus<SysUv> query = listQuery(type, typeId, uid);
        query.select("uv_id");
        return getApp().first(query) != null;
    }

    public SysUv getEntity(String type, String typeId, String uid) {
        JoinWrapperPlus<SysUv> query = listQuery(type, typeId, uid);
        return getApp().first(query);
    }

    public XReturn save(String appId, String type, String typeId, String uid) {
        return save(VoUvSave.of(appId, type, typeId, uid, 1));
    }

    public XReturn save(VoUvSave data) {
        SysUv entity = getEntity(data.getType(), data.getType_id(), data.getUid());
        if (data.getAdd() == 1) {
            //加uv
            if (entity != null) {
                return XReturn.getR(20080, "已添加");
            }

            SysUv entityNew = new SysUv();
            entityNew.setUv_x_id(XHttpContext.getX());
            entityNew.setUv_app_id(data.getApp_id());
            entityNew.setUv_type(data.getType());
            entityNew.setUv_type_id(data.getType_id());
            entityNew.setUv_uid(data.getUid());
            entityNew.setAutoID();
            getApp().save(entityNew);
        } else {
            //减uv
            if (entity == null) {
                return XReturn.getR(20090, "已取消");
            }

            getApp().delete(entity.getKey());
        }

        return EnumSys.OK.getR();
    }

    /**
     * 删除
     */
    @SneakyThrows
    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn delete(List<String> ids) {
        XReturn r = XReturn.getR(0);

        getApp().delete(ids);

        return r;
    }


    /**
     * 总量
     *
     * @param typeId
     */
    public Long uvTotal(String type, String typeId) {
        JoinWrapperPlus<SysUv> query = listQuery(type);
        query.eq("uv_x_id", XHttpContext.getX())
                .eq("uv_type_id", typeId);

        return getApp().count(query);
    }
}
