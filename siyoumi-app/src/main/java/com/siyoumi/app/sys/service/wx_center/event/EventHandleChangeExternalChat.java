package com.siyoumi.app.sys.service.wx_center.event;

import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerData;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;

//企业微信群，进退群事件
@Slf4j
public class EventHandleChangeExternalChat
        implements ICenterHandle {
    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        CenterHandlerData data = ctx.getHandleData();

        String chatId = (String) data.getInMsg().get("ChatId");
        String updateDetail = (String) data.getInMsg().get("UpdateDetail");
        String changeType = (String) data.getInMsg().get("ChangeType");
        log.info("chat_id: {}", chatId);
        log.info("update_detail: {}", updateDetail); //del_member add_member
        log.info("change_type: {}", changeType); //update: 加群, 退群 dismiss: 解散

        //SvcGroupChatTask.getBean().changeExternalChat(data.getSiteId(), chatId, changeType);

        ctx.setFinal();
        return null;
    }
}
