package com.siyoumi.app.sys.service.wx_center;

import cn.hutool.core.util.XmlUtil;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;

import java.util.Map;

//xml实体
public class CenterHandlerXml
        extends XReturn {
    public static CenterHandlerXml parse(String xmlStr) {
        CenterHandlerXml xml = new CenterHandlerXml();

        Map<String, Object> xmlMap = XmlUtil.xmlToMap(xmlStr);
        for (Map.Entry<String, Object> entry : xmlMap.entrySet()) {
            xml.put(entry.getKey(), entry.getValue());
        }

        return xml;
    }

    public String getToUserName() {
        return (String) get("ToUserName");
    }

    public void setToUserName(String toUserName) {
        put("ToUserName", toUserName);
    }

    public String getFromUserName() {
        return getData("FromUserName");
    }

    public void setFromUserName(String fromUserName) {
        setData("FromUserName", fromUserName);
    }

    public Integer getCreateTime() {
        return XStr.toInt(getData("CreateTime"));
    }

    public void setCreateTime(String createTime) {
        setData("CreateTime", createTime);
    }

    public String getMsgType() {
        return getData("MsgType");
    }

    public void setMsgType(String msgType) {
        setData("MsgType", msgType);
    }

    public String getContent() {
        return (String) get("Content");
    }

    public void setContent(String content) {
        put("Content", content);
    }

    public String getEvent() {
        return getData("Event");
    }

    public void setEvent(String event) {
        setData("Event", event);
    }

    public String getEventKey() {
        return getData("EventKey");
    }

    public void setEventKey(String event) {
        setData("EventKey", event);
    }

    /**
     * 事件消息
     */
    public Boolean isEvent() {
        return "event".equals(getMsgType());
    }
}
