package com.siyoumi.app.sys.wx;

import com.siyoumi.app.sys.vo.WebOauthVo;
import com.siyoumi.config.SysConfig;
import com.siyoumi.util.XStr;

//应用处理
public interface WxOauthAppHandle {
    default String appUrl(String openid, WebOauthVo wxOauthData) {
        String t = "t_001";
        String url = XStr.format("{0}res/app/{1}/{2}/index.html"
                , SysConfig.getIns().getImgRoot()
                , wxOauthData.getApp_id()
                , t
        );
        if (SysConfig.getIns().isDev()) {
            url = XStr.format("{0}job.res_file/app/{1}/{2}/index.html"
                    , SysConfig.getIns().getImgRoot()
                    , wxOauthData.getApp_id()
                    , t
            );
        }
        return url;
    }
}
