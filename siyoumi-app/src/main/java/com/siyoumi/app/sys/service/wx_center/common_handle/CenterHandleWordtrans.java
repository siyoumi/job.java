package com.siyoumi.app.sys.service.wx_center.common_handle;

import com.siyoumi.app.sys.service.wx_center.CenterHandler;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;

//关键词回复（键转换）
@Slf4j
public class CenterHandleWordtrans
        implements ICenterHandle {
    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        if (XStr.isNullOrEmpty(ctx.getContent())) {
            log.info("内容为空，不处理wordtrans_b");
            return null;
        }

        return CenterHandler.handleByAppId("wordtrans_b", ctx);
    }
}
