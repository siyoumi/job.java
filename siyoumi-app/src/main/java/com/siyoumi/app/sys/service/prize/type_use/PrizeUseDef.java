package com.siyoumi.app.sys.service.prize.type_use;

import com.siyoumi.app.entity.SysPrize;
import com.siyoumi.app.sys.service.prize.PrizeUse;
import com.siyoumi.app.sys.service.prize.entity.PrizeUseData;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

//宝箱奖
@Slf4j
@Service
public class PrizeUseDef
        extends PrizeUse {
    @Override
    protected XReturn useCan(PrizeUseData data) {
        if (getEntityPrizeSet().getPset_use_type() == 0) {
            log.info("密码核销");
            if (XStr.isNullOrEmpty(data.getPwd())) {
                return EnumSys.MISS_VAL.getR("请输入密码");
            }

            String pwdDec = XStr.base64Dec(data.getPwd());
            if (!getEntityPrizeSet().getPset_use_str_00().equals(pwdDec)) {
                return EnumSys.ERR_VAL.getR("密码不正确");
            }
        } else if (getEntityPrizeSet().getPset_use_type() == 2) {
            log.info("发货");
            //if (XStr.isNullOrEmpty(data.getPrize_user_name())) {
            //    return SysErr.MISS_VAL.getR("缺少姓名字段");
            //}
            //if (XStr.isNullOrEmpty(data.getPrize_user_phone())) {
            //    return SysErr.MISS_VAL.getR("缺少手机字段");
            //}
            //if (XStr.isNullOrEmpty(data.getPrize_user_address())) {
            //    return SysErr.MISS_VAL.getR("缺少地址字段");
            //}
        }
        return XReturn.getR(0);
    }

    @Override
    protected XReturn useBefore(PrizeUseData data, SysPrize entityPrizeUpdate) {
        if (getEntityPrizeSet().getPset_use_type() == 2) {
            entityPrizeUpdate.setPrize_user_name(data.getPrize_user_name());
            entityPrizeUpdate.setPrize_user_phone(data.getPrize_user_phone());
            entityPrizeUpdate.setPrize_user_address(data.getPrize_user_address());
        }

        return XReturn.getR(0);
    }

    @Override
    protected XReturn useAfter(PrizeUseData data) {
        return XReturn.getR(0);
    }
}
