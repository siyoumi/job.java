package com.siyoumi.app.sys.vo;

import com.siyoumi.util.XStr;
import com.siyoumi.validator.annotation.EqualsValues;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Data
public class VoUploadFile {
    @HasAnyText(message = "缺少文件")
    MultipartFile file0;
    @HasAnyText(message = "缺少类型")
    @EqualsValues(vals = {"sys", "aliyun"})
    String type = "sys";

    //sys类型才生效
    Integer thumbnail = 0; //缩略图
    Integer thumbnail_width = 320; //缩略图，默认宽度

    public String getSaveFileDir() {
        String saveFileDir = "";
        if ("aliyun".equals(getType())) {
            saveFileDir = XStr.format("app/upload");
        }
        return saveFileDir;
    }
}
