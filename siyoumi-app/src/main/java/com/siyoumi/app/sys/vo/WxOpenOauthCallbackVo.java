package com.siyoumi.app.sys.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//开放平台
@Data
public class WxOpenOauthCallbackVo {
    @HasAnyText(message = "miss auth_code")
    private String auth_code;
    private String master_xid;
    private Integer wxapp = 0; //0:公众号; 1:小程序;
}