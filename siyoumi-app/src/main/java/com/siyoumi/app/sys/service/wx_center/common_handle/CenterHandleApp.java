package com.siyoumi.app.sys.service.wx_center.common_handle;

import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerData;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.app.sys.service.wxapi.WxApiMp;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;

//应用处理回复
@Slf4j
public class CenterHandleApp
        implements ICenterHandle {
    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        LogPipeLine.getTl().setLogMsg("CenterHandleApp");

        CenterHandlerData data = ctx.getHandleData();

        if (XStr.isNullOrEmpty(data.getWord())) {
            log.info("word还是为空");
            data.setWord(ctx.getContent());
        }
        LogPipeLine.getTl().setLogMsgFormat("word: {0}", data.getWord());

        //AppWord appWord = AppWord.getBean(data.getWord());
        //return CenterHandler.handleByAppId(appWord.getApp(), ctx);
        WxApiMp api = WxApiMp.getIns(ctx.getEntityConfig());
        api.sendText(ctx.getHandleData().getOpenid(), "测试：" + ctx.getContent());
        //

        return null;
    }
}
