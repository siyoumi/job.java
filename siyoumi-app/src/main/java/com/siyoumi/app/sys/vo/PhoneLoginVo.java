package com.siyoumi.app.sys.vo;

import com.siyoumi.util.XStr;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.Size;

@Data
@Slf4j
@Accessors(chain = true)
public class PhoneLoginVo {
    Boolean auto_reg = false; //是否自动注册
    private Integer login_type = 0; //【0】手机号登陆；【1】帐号登陆；

    @HasAnyText(message = "phone miss")
    @Size(max = 50)
    private String phone;
    private String phone_enc;

    //密码或者openid 2选1
    private String ukey;
    private String openid;

    public String getDecPwd() {
        log.debug(getUkey());
        String decPwd = XStr.base64Dec(getUkey());
        log.debug("base64解密：" + decPwd);

        return decPwd;
    }
}
