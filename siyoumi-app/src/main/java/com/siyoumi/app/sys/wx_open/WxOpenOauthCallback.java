package com.siyoumi.app.sys.wx_open;

import com.alibaba.fastjson.JSONObject;
import com.siyoumi.app.modules.accsuper_admin.service.SvcSysAccspuerConfig;
import com.siyoumi.app.modules.accsuper_admin.vo.SysAccsuperConfigVo;
import com.siyoumi.app.sys.service.wxapi.WxApiOpen;
import com.siyoumi.app.sys.vo.WxOpenOauthCallbackVo;
import com.siyoumi.controller.ApiController;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

//放开平台授权回调
@Slf4j
@RestController
@RequestMapping("/wx_open/server/oauth_callback")
public class WxOpenOauthCallback
        extends ApiController {
    @Transactional(rollbackFor = Exception.class)
    @GetMapping()
    public XReturn index(WxOpenOauthCallbackVo vo) {
        SysAccsuperConfig entityConfigMaster = SysAccsuperConfigService.getBean().getXConfig(vo.getMaster_xid(), true);
        WxApiOpen api = WxApiOpen.getIns(entityConfigMaster);
        XReturn r = api.auth(vo.getAuth_code());
        if (r.err()) {
            return r;
        }

        JSONObject authorizationInfo = r.getData("authorization_info");
        String authorizerAppid = authorizationInfo.getString("authorizer_appid");
        String authorizerAccessToken = authorizationInfo.getString("authorizer_access_token");
        String authorizerRefreshToken = authorizationInfo.getString("authorizer_refresh_token");

        String xid = vo.getMaster_xid() + "." + authorizerAppid;

        log.debug("新建站点");
        SysAccsuperConfigVo addVo = new SysAccsuperConfigVo();
        addVo.setAcc_name("");
        addVo.setAcc_uid(xid);
        addVo.setAcc_pwd(SysAccountService.getDefPwd());
        String aconfigType = "wx";
        if (vo.getWxapp() == 1) {
            aconfigType = "wxapp";
        }
        addVo.setAconfig_type(aconfigType);
        SvcSysAccspuerConfig.getBean().addAccsuper(addVo, true);

        SysAccsuperConfig entityConfigUpdate = new SysAccsuperConfig();
        entityConfigUpdate.setAconfig_id(xid);
        entityConfigUpdate.setAconfig_app_id(authorizerAppid);
        entityConfigUpdate.setAconfig_token(entityConfigMaster.getAconfig_token());
        entityConfigUpdate.setAconfig_wxopen(1);
        entityConfigUpdate.setAconfig_wxopen_parent(vo.getMaster_xid());
        entityConfigUpdate.setAconfig_access_token(authorizerAccessToken);
        entityConfigUpdate.setAconfig_refresh_token(authorizerRefreshToken);
        entityConfigUpdate.setAconfig_access_token_date(LocalDateTime.now().minusMonths(1));
        SvcSysAccspuerConfig.getApp().updateById(entityConfigUpdate);


        r = api.authInfo(authorizerAppid);
        if (r.err()) {
            return r;
        }

        log.info("更新站点名");
        JSONObject authorizerInfo = r.getData("authorizer_info");
        String nickName = authorizerInfo.getString("nick_name");
//        String headImg = authorizerInfo.getString("head_img");
//        String userName = authorizerInfo.getString("user_name");

        getR().setData("appid", authorizerAppid);
        getR().setData("name", nickName);

        return getR();
    }
}
