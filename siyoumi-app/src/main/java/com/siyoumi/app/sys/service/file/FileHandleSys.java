package com.siyoumi.app.sys.service.file;

import com.siyoumi.app.sys.service.CommonApiServcie;
import com.siyoumi.app.sys.service.FileHandle;
import com.siyoumi.app.sys.service.file.entity.FileUploadData;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;

import java.io.IOException;

//系统
public class FileHandleSys
        extends FileHandle {
    @Override
    protected XReturn saveFile(FileUploadData data) {
        byte[] fileBytes = new byte[0];
        try {
            fileBytes = data.getMultFile().getBytes();
        } catch (IOException e) {
            XReturn r = EnumSys.FILE_EXISTS.getR();
            r.setData("err_message", e.getMessage());
            XValidator.err(r);
        }

        String fileName = data.getMultFile().getOriginalFilename();
        String fileExt = getFileExt(fileName);

        XReturn r = null;
        if (XStr.isNullOrEmpty(data.getSaveFileDir())) {
            r = CommonApiServcie.getBean().saveFile(fileBytes, fileExt);
        } else {
            String saveFilePath = data.getSaveFileDir() + "/" + fileName;
            r = CommonApiServcie.getBean().saveFile(fileBytes, fileExt, saveFilePath);
        }
        return r;
    }
}
