package com.siyoumi.app.sys.wx.thread;

import java.util.HashMap;
import java.util.Map;

public class WxCenterThreadLocal {
    static ThreadLocal<Map<String, Object>> tl = new ThreadLocal<>();
    private static final String XML_KEY = "wx_center_xml";
    private static final String URL_KEY = "wx_center_url";

    static public void setUrl(String url) {
        set(URL_KEY, url);
    }

    static public void setXml(String xml) {
        set(XML_KEY, xml);
    }

    static public String getXml() {
        return get(XML_KEY, "");
    }

    static public String getUrl() {
        return get(URL_KEY, "");
    }

    static public void set(String k, Object v) {
        Map<String, Object> data = tl.get();
        if (data == null) {
            data = new HashMap<>();
        }

        data.put(k, v);
        tl.set(data);
    }

    static public <T> T get(String k, T def) {
        Map<String, Object> data = tl.get();
        if (data == null) {
            return def;
        }

        Object v = data.get(k);
        if (v == null) {
            return def;
        }

        return (T) v;
    }

    /**
     * 回收
     */
    static public void remove() {
        tl.remove();
    }
}
