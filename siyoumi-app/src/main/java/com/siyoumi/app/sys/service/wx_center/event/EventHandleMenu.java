package com.siyoumi.app.sys.service.wx_center.event;

import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerData;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerXml;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.app.sys.service.wxapi.WxApiMp;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;

//公众号菜单事件（所有菜单事件都会来这里）
@Slf4j
public class EventHandleMenu
        implements ICenterHandle {
    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        CenterHandlerData data = ctx.getHandleData();

        CenterHandlerXml inMsg = data.getInMsg();

        log.info("event: {}", inMsg.getEvent());
        log.info("event_key: {}", inMsg.getEventKey());

        if (!"CLICK".equals(inMsg.getEvent())) {
            LogPipeLine.getTl().setLogMsgFormat("{0} 菜单点击事件: 不处理", inMsg.getEvent());
            ctx.setFinal();
            return null;
        }

        WxApiMp api = WxApiMp.getIns(ctx.getEntityConfig());
        api.sendText(ctx.getHandleData().getOpenid(), "菜单点击事件：" + inMsg.getEventKey());
        ctx.setFinal();
        //T__t_c_wx_event entityEvent = Svc__t_c_wx_event.getBean().getEntity(ctx.getSiteId(), inMsg.getEvent(), inMsg.getEventKey());
        //if (entityEvent != null) {
        //    LogPipeLine.getTl().setLogMsgFormat("菜单点击事件: {}", entityEvent.getWxevent_word_src());
        //    ctx.getHandleData().setWord(entityEvent.getWxevent_word_src());
        //}


        return null;
    }
}
