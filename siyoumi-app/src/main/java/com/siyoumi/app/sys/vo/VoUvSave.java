package com.siyoumi.app.sys.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class VoUvSave {
    String app_id;
    String type;
    String type_id;
    String uid;
    Integer add = 1; //【0】取消点赞；【1】点赞

    public static VoUvSave of(String appId, String type, String typeId, String uid, Integer add) {
        return new VoUvSave().setApp_id(appId).setType(type).setType_id(typeId).setUid(uid).setAdd(add);
    }
}
