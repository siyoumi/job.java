package com.siyoumi.app.sys.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class VaSysItem {
    @HasAnyText
    private String item_app_id;
    @HasAnyText
    private String item_id_src;
    @Size(max = 50)
    private String item_name;
    private String item_data;
}
