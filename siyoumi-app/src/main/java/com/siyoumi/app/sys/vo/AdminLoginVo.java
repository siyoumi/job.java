package com.siyoumi.app.sys.vo;

import com.siyoumi.util.XLog;
import com.siyoumi.util.XStr;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotBlank;

//后台登陆
@Data
@Slf4j
public class AdminLoginVo {
    @NotBlank(message = "账号不能为空")
    private String username;
    @NotBlank(message = "请输入6-20位密码")
    private String password;
    @NotBlank(message = "请输入验证码")
    private String vcode;


    /**
     * base64解密
     */
    public String getDecPwd() {
        log.debug(getPassword());
        String dec_pwd = XStr.base64Dec(getPassword());
        log.debug("base64解密：" + dec_pwd);

        return dec_pwd;
    }
}
