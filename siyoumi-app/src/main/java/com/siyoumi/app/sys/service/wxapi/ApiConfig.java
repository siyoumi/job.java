package com.siyoumi.app.sys.service.wxapi;

import com.siyoumi.util.XStr;
import lombok.Data;

@Data
public class ApiConfig {
    private String appId;
    private String appSecret;
    private String accessToken;

    private String payAppId;
    private String mchId;
    private String mchKey;
    //
    private String mchIdSub;
    private String appIdSub;

    private ApiConfig parent;

    /**
     * 服务商模式
     */
    public Boolean isServer() {
        return XStr.hasAnyText(getMchIdSub());
    }

    /**
     * 开放平台代公众号模式/小程序
     */
    public Boolean isOpenClient() {
        return getParent() != null;
    }
}
