package com.siyoumi.app.sys.service.wxapi;

import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XHttpClient;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

//微信开放平台
@Slf4j
public class WxApiOpen
        extends ApiBase {
    static public WxApiOpen getIns(SysAccsuperConfig entityConfig) {
        WxApiOpen app = new WxApiOpen();
        if (entityConfig != null) {
            app.setConfig(entityConfig);
        }
        return app;
    }

    /**
     * 服务商token
     */
    public XReturn masterToken(String ticket) {
        if (XStr.isNullOrEmpty(ticket)) {
            return EnumSys.MISS_VAL.getR("component_verify_ticket miss ");
        }

        Map<String, Object> json = new HashMap<>();
        json.put("component_appid", getConfig().getAppId());
        json.put("component_appsecret", getConfig().getAppSecret());
        json.put("component_verify_ticket", ticket); //open_ticket

        String url = getApiUrl("cgi-bin/component/api_component_token");

        return api(url, XHttpClient.METHOD_POST, json);
    }

    public XReturn clientToken(String refreshToken) {
        Map<String, Object> json = new HashMap<>();
        json.put("component_appid", getConfig().getParent().getAppId());
        json.put("authorizer_appid", getConfig().getAppId());
        json.put("authorizer_refresh_token", refreshToken);

        String accessToken = getConfig().getParent().getAccessToken();
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token=" + accessToken;

        return api(url, XHttpClient.METHOD_POST, json);
    }

    /**
     * 获取授权码
     */
    public XReturn authCode() {
        Map<String, Object> json = new HashMap<>();
        json.put("component_appid", getConfig().getAppId());

        String accessToken = getConfig().getAccessToken();
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token=" + accessToken;

        return api(url, XHttpClient.METHOD_POST, json);
    }

    /**
     * 使用授权码获取授权信息
     */
    public XReturn auth(String authCode) {
        Map<String, Object> json = new HashMap<>();
        json.put("component_appid", getConfig().getAppId());
        json.put("authorization_code", authCode);

        String accessToken = getConfig().getAccessToken();
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token=" + accessToken;

        return api(url, XHttpClient.METHOD_POST, json);
    }


    /**
     * 使用授权码获取授权信息
     */
    public XReturn authInfo(String appId) {
        Map<String, Object> json = new HashMap<>();
        json.put("component_appid", getConfig().getAppId());
        json.put("authorizer_appid", appId);

        String accessToken = getConfig().getAccessToken();
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_info?component_access_token=" + accessToken;

        return api(url, XHttpClient.METHOD_POST, json);
    }
}
