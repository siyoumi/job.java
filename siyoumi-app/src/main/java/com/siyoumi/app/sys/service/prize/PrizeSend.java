package com.siyoumi.app.sys.service.prize;

import com.siyoumi.app.entity.SysPrize;
import com.siyoumi.app.entity.SysPrizeSet;
import com.siyoumi.app.service.SysPrizeService;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
//奖品发送
abstract public class PrizeSend {
    static String KEY = "PrizeSend::entity";

    @SneakyThrows
    static public PrizeSend getBean(SysPrizeSet entityPrizeSet) {
        String type = entityPrizeSet.getPset_type();
        if (XStr.isNullOrEmpty(type)) {
            type = "def";
        }

        type = XStr.lineToHump(type);
        type = XStr.toUpperCase1(type);

        String className = XStr.concat("com.siyoumi.app.modules.sys.service.prize.type.PrizeSend", type);
        log.info(entityPrizeSet.getPset_type());
        log.info(className);
        Class<?> clazz = Class.forName(className);
        PrizeSend app = (PrizeSend) XSpringContext.getBean(clazz);
        XHttpContext.set(KEY, entityPrizeSet);

        return app;
    }

    public SysPrizeSet getEntity() {
        return XHttpContext.get(KEY);
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public XReturn send(String sendKey, String openid) {
        SysPrize entityPrize = initPrize(sendKey, openid);
        XReturn r = sendBefore(entityPrize);
        if (r.err()) {
            return r;
        }
        SysPrizeService.getBean().save(entityPrize);
        r = sendAfter(entityPrize);
        r.setData("entity_prize", entityPrize);

        return r;
    }

    protected XReturn sendBefore(SysPrize entityPrize) {
        return XReturn.getR(0);
    }

    protected abstract XReturn sendAfter(SysPrize entityPrize);


    /**
     * 获取初始化完成的prize实体
     *
     * @return SysPrize
     */
    protected SysPrize initPrize(String sendKey, String openid) {
        SysPrize entityPrize = new SysPrize();
        entityPrize.setPrize_x_id(XHttpContext.getX());
        entityPrize.setPrize_openid(openid);
        entityPrize.setPrize_type(getEntity().getPset_type());
        entityPrize.setPrize_id_src(getEntity().getPset_id_src());
        entityPrize.setPrize_set_id(getEntity().getPset_id());
        entityPrize.setPrize_key(sendKey);
        entityPrize.setPrize_app_id(getEntity().getPset_app_id());
        entityPrize.setPrize_name(getEntity().getPset_name());
        entityPrize.setPrize_pic(getEntity().getPset_pic());
        entityPrize.setPrize_desc(getEntity().getPset_desc());
        entityPrize.setPrize_use_type(getEntity().getPset_use_type() + "");
        entityPrize.setAutoID();
        entityPrize.setPrize_fun(XStr.toBigDecimal(getEntity().getPset_fun()));

        //有效期
        if (getEntity().getPset_vaild_date_type() == 0) {
            entityPrize.setPrize_begin_date(getEntity().getPset_vaild_date_begin());
            entityPrize.setPrize_end_date(getEntity().getPset_vaild_date_end());
        } else {
            entityPrize.setPrize_begin_date(XDate.now());
            entityPrize.setPrize_end_date(XDate.now().plusHours(getEntity().getPset_vaild_date_win_h()));
        }

        return entityPrize;
    }
}
