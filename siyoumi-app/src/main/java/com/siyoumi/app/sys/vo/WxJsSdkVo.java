package com.siyoumi.app.sys.vo;

import com.siyoumi.component.XApp;
import com.siyoumi.util.XDate;
import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

//开放平台
@Data
public class WxJsSdkVo {
    @HasAnyText(message = "miss x")
    private String x;
    @HasAnyText(message = "miss url")
    private String url;
    private String appId;
    private String timestamp = String.valueOf(XDate.toS());
    private String nonceStr = XApp.getStrID();
    private String signature;
    private String jsapi_ticket;
}