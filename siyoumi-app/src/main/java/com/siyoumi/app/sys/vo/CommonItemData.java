package com.siyoumi.app.sys.vo;

import lombok.Data;

//通用项
@Data
public class CommonItemData {
    private String title; //标题
    private String pic; //图片
    private String desc; //描述
    private String redirect_type; //跳转方式
    private String redirect_str_00;
    private String redirect_str_01;
}
