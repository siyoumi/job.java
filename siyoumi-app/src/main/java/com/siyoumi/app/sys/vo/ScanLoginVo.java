package com.siyoumi.app.sys.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.Size;

@Data
@Slf4j
@Accessors(chain = true)
public class ScanLoginVo {
    @HasAnyText(message = "key miss")
    @Size(max = 50)
    private String key;

    public static String redisKey(String key) {
        return "scan_login:" + key;
    }
}
