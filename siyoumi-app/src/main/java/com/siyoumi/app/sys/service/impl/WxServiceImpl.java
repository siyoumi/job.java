package com.siyoumi.app.sys.service.impl;

import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.entity.WxUserInfo;
import com.siyoumi.app.sys.service.WxApiService;
import com.siyoumi.app.sys.service.wxapi.WxApiMp;
import com.siyoumi.app.sys.vo.VaWxUser;
import com.siyoumi.app.sys.vo.LoginVo;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

//公众号接口
@Service
public class WxServiceImpl
        implements WxApiService, IWebService {
    @Override
    @Transactional
    public XReturn login(LoginVo data) {
        XHttpContext.setX(data.getX());

        SysAccsuperConfig entityConfig = XHttpContext.getXConfig(true);
        if (entityConfig == null) {
            return EnumSys.ERR_VAL.getR();
        }

        WxApiMp appWx = WxApiMp.getIns(entityConfig);
        XReturn r;
        if (XStr.hasAnyText(data.getTest_uid())) {
            //测试
            r = XReturn.getR(0);
            r.setData("openid", data.getTest_uid());
        } else {
            r = appWx.getOauthAccessToken(data.getCode());
        }
        if (r.err()) {
            return r;
        }

        String openid = r.getData("openid");
        String accessToken = r.getData("access_token", "");

        Map<String, Object> userData = null;
        if (data.getScope().equals("snsapi_userinfo")) {
            //显式授权
            r = appWx.getOauthUserInfo(accessToken, openid);
            if (r.err()) {
                return r;
            }

            userData = new HashMap<>();
            for (Map.Entry<String, Object> entry : r.entrySet()) {
                userData.put("wxuser_" + entry.getKey(), entry.getValue().toString());
            }
        }

        WxUserService appWxUser = WxUserService.getBean();
        WxUser entityWxUser = appWxUser.getByOpenid(openid);
        //为了不更新所有字段，需要new
        WxUser entityWxUserUpdate;
        if (userData != null) {
            entityWxUserUpdate = XBean.fromMap(userData, WxUser.class);
        } else {
            entityWxUserUpdate = new WxUser();
        }
        entityWxUserUpdate.setWxuser_openid(openid);
        entityWxUserUpdate.setWxuser_x_id(entityConfig.getKey());
        //保存或者更新
        entityWxUser = appWxUser.saveOrUpdatePassEqualField(entityWxUser, entityWxUserUpdate);

        //前端返回参数
        r = loginReturnDataWx(entityWxUser, entityConfig);

        return r;
    }

    @Override
    public XReturn loginSetOpenid(String token) {
        return WxApiService.getBean().loginSetOpenid(token);
    }

    @Override
    public XReturn getPhone(String code) {
        return EnumSys.API_ERROR.getR("公众号无法获取手机号");
    }


    @Override
    public XReturn userEdit(WxUserInfo entityWxUserInfo) {
        return WxApiService.getBean().userEdit(entityWxUserInfo);
    }

    @Override
    public XReturn userUpdate(VaWxUser vo) {
        return WxApiService.getBean().userUpdate(vo);
    }

    @Override
    public XReturn addressList(InputData inputData) {
        return WxApiService.getBean().addressList(inputData);
    }
}
