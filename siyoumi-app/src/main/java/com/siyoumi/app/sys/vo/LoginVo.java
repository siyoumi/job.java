package com.siyoumi.app.sys.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

@Data
public class LoginVo {
    @HasAnyText
    private String code;
    @HasAnyText
    private String x;

    private String scope;
    private String test_uid;
}
