package com.siyoumi.app.sys.service.netty;

import com.siyoumi.app.config.boot.ISiyoumiBoot;
import com.siyoumi.app.netty.NettyWebSocketServer;
import com.siyoumi.app.netty.to.NettyConnectionData;
import com.siyoumi.component.XRedis;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoginQrService
        extends NettyWebSocketServer
        implements ISiyoumiBoot {
    @SneakyThrows
    @Override
    public void run() {
        log.info("清空redis房间记录");
        XRedis.getBean().deleteByPattern("LoginQrHandler:*");

        LoginQrService app = new LoginQrService();
        app.handle(getConnectionData());
    }

    public static void main(String[] args) {
        LoginQrService app = new LoginQrService();
        app.run();
    }

    public static NettyConnectionData getConnectionData() {
        NettyConnectionData data = NettyConnectionData.getIns();
        data.setIp("0.0.0.0");
        data.setPort(8200);
        data.setPath("/ws/login_qr");
        data.setHandlerClazz(LoginQrHandler.class);
        return data;
    }
}
