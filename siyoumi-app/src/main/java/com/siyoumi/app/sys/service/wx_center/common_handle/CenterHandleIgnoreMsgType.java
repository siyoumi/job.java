package com.siyoumi.app.sys.service.wx_center.common_handle;

import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerXml;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

//过滤某些msg type
@Slf4j
public class CenterHandleIgnoreMsgType
        implements ICenterHandle {
    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        CenterHandlerXml inMsg = ctx.getHandleData().getInMsg();
        //LogPipeLine.getTl().setLogMsg("msgType: ", inMsg.getMsgType());

        if (ignoreMsgType(inMsg)) {
            //LogPipeLine.getTl().setLogMsg("过滤type");
            ctx.setDiglog(false);
            ctx.setFinal();
        }

        return null;
    }

    /**
     * 需要过滤的type
     *
     * @param inMsg
     */
    public Boolean ignoreMsgType(CenterHandlerXml inMsg) {
        List<String> msgTypeArr = new ArrayList<>();
        msgTypeArr.add("location");

        if (msgTypeArr.contains(inMsg.getMsgType())) {
            return true;
        }

        return false;
    }
}
