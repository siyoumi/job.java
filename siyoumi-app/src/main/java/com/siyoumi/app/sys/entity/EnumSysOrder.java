package com.siyoumi.app.sys.entity;

import com.siyoumi.exception.IEnumSys;

/**
 * 10：系统错误
 * 20：app错误
 */
public enum EnumSysOrder
        implements IEnumSys {
    UNPAY(21010, "订单未支付"),
    EXPIRE(21020, "订单已过期"),
    PAY(21030, "订单已支付"),
    CANCEL(21050, "订单已取消"),
    REFUND_APPLY(21060, "订单已申请退款"),
    REFUND(21070, "订单已退款");


    private Integer errcode;
    private String errmsg;

    EnumSysOrder(Integer errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    @Override
    public Integer getErrcode() {
        return errcode;
    }

    @Override
    public String getErrmsg() {
        return errmsg;
    }
}
