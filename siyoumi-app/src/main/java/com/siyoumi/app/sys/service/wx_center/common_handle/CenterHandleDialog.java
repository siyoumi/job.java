package com.siyoumi.app.sys.service.wx_center.common_handle;

import com.siyoumi.app.entity.WxDialog;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerData;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerXml;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.app.service.WxDialogService;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;

import java.time.LocalDateTime;

//保存dialog
public class CenterHandleDialog
        implements ICenterHandle {
    static public String redisKey() {
        return "zzz_wx.t_c_wx_dialog|QUEUE";
    }


    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        ctx.setFinal();
        if ("test".equals(ctx.getContent())) {
            ctx.addLastHandle(new CenterHandleTest());
        } else {
            ctx.setFinal();
        }

        CenterHandlerData data = ctx.getHandleData();
        CenterHandlerXml inMsg = data.getInMsg();

        LocalDateTime create_date = XDate.parseTimestamp((inMsg.getCreateTime().longValue()));

        WxDialog entityDialog = new WxDialog();
        entityDialog.setWxdlg_create_date(create_date);
        entityDialog.setWxdlg_x_id(data.getX());
        entityDialog.setWxdlg_FromUserName(inMsg.getFromUserName());
        entityDialog.setWxdlg_ToUserName(inMsg.getToUserName());
        entityDialog.setWxdlg_CreateTime(inMsg.getCreateTime().toString());
        entityDialog.setWxdlg_create_date(XDate.parseTimestamp(entityDialog.getWxdlg_CreateTime()));
        entityDialog.setWxdlg_MsgType(inMsg.getMsgType());
        //
        if (inMsg.isEvent()) {
            //事件记录
            entityDialog.setWxdlg_Event(inMsg.getEvent());
            entityDialog.setWxdlg_EventKey(inMsg.getEventKey());
            if (inMsg.containsKey("Ticket")) {
                //扫码
                entityDialog.setWxdlg_Ticket(inMsg.getData("Ticket"));
            }
        }
        if (XStr.hasAnyText(ctx.getContent())) {
            entityDialog.setWxdlg_Content(ctx.getContent());
        }
        //
        entityDialog.setAutoID();
        WxDialogService.getBean().save(entityDialog);
        //
        return null;
    }
}
