package com.siyoumi.app.sys.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAddress;
import com.siyoumi.app.service.SysAddressService;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.IWebService;
import com.siyoumi.util.XReturn;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XStr;
import com.siyoumi.component.http.InputData;
import com.siyoumi.component.http.XHttpContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AddressService
        implements IWebService {
    static public AddressService getBean() {
        return XSpringContext.getBean(AddressService.class);
    }

    public XReturn addressList(InputData inputData) {
        String compKw = inputData.input("compKw");

        SysAddressService appAddress = SysAddressService.getBean();
        QueryWrapper<SysAddress> query = appAddress.q();
        query.eq("addr_x_id", XHttpContext.getX())
                .eq("addr_openid", getOpenid())
                .orderByDesc("addr_def")
                .orderByAsc("addr_order");
        if (XStr.hasAnyText(compKw)) {
            //名称或者手机号
            query.and(q -> {
                q.like("addr_name", compKw)
                        .or().eq("addr_phone", compKw);
            });
        }
        Page<SysAddress> page = appAddress.page(inputData.getPageIndex(), inputData.getPageSize(), false);
        IPage<SysAddress> pageData = appAddress.get(page, query);

        XReturn r = XReturn.getR(0);
        r.setData("list", pageData.getRecords());

        return r;
    }

    @Transactional(rollbackFor = Exception.class)
    public XReturn addressAdd(SysAddress entity, InputData inputData) {
        String def = inputData.input("def", "0");

        SysAddressService app = SysAddressService.getBean();
        inputData.putAll(entity.toMap());
        inputData.put("addr_openid", getOpenid());

        XReturn r = app.saveEntity(inputData, true, null);
        if (r.err()) {
            return r;
        }
        if ("1".equals(def)) { //设置默认地址
            SysAddress entityNew = r.getData("entity");
            app.setDef(entityNew);
        }

        return r;
    }

    /**
     * 地址删除
     */
    @Transactional(rollbackFor = Exception.class)
    public XReturn addressDel(String id) {
        if (XStr.isNullOrEmpty(id)) {
            return EnumSys.TOKEN_MISS.getR("miss id");
        }

        SysAddressService app = SysAddressService.getBean();
        SysAddress entity = app.getEntity(id);
        if (entity == null) {
            return EnumSys.ERR_VAL.getR("未找到地址");
        }
        if (!entity.getAddr_openid().equals(getOpenid())) {
            return XReturn.getR(20075, "未找到地址");
        }

        app.mapper().deleteById(entity);
        return XReturn.getR(0);
    }

    /**
     * 设置默认地址
     */
    @Transactional(rollbackFor = Exception.class)
    public XReturn addressDef(String id) {
        if (XStr.isNullOrEmpty(id)) {
            return EnumSys.TOKEN_MISS.getR("miss id");
        }

        SysAddressService app = SysAddressService.getBean();
        SysAddress entity = app.getEntity(id);
        if (entity == null) {
            return EnumSys.ERR_VAL.getR("未找到地址");
        }
        if (!entity.getAddr_openid().equals(getOpenid())) {
            return XReturn.getR(20075, "未找到地址");
        }

        app.setDef(entity);
        return XReturn.getR(0);
    }
}
