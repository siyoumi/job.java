package com.siyoumi.app.sys.service.wx_center;

import com.alibaba.fastjson.annotation.JSONField;
import com.siyoumi.util.XStr;
import lombok.Data;

@Data
public class CenterHandlerData {
    String word; //大部分情况是appid
    String openid;
    String x;
    String strXml;

    @JSONField(serialize = false)
    CenterHandlerXml inMsg;

    public void init() {
        CenterHandlerXml msg = CenterHandlerXml.parse(getStrXml());
        setInMsg(msg);
        if (XStr.hasAnyText(msg.getFromUserName())) {
            setOpenid(msg.getFromUserName());
        }
    }

    public static CenterHandlerData getInstance(String inXml, String siteId) {
        CenterHandlerData data = new CenterHandlerData();
        data.setX(siteId);
        data.setStrXml(inXml);
        data.init();

        return data;
    }
}
