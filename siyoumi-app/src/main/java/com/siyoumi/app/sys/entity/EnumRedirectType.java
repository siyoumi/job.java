package com.siyoumi.app.sys.entity;

import com.siyoumi.util.IEnum;

//跳转方式
public enum EnumRedirectType
        implements IEnum {
    NULL("", "无"),
    URL("url", "链接跳转"),
    WXAPP("wxapp", "跳本小程序"),
    WXAPP_OTHER("wxapp_other", "跳其他小程序");


    private String key;
    private String val;

    EnumRedirectType(String key, String val) {
        this.key = key;
        this.val = val;
    }

    @Override
    public String getValue() {
        return val;
    }

    @Override
    public String getKey() {
        return key;
    }
}
