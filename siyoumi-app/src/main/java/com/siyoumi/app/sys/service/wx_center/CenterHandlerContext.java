package com.siyoumi.app.sys.service.wx_center;

import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.util.XStr;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

//处理上下文
public class CenterHandlerContext {
    @Getter
    @Setter
    private Boolean fromTask = false; //任务执行

    private Boolean handleFinal = false; //处理是否已完成
    @Setter
    private Boolean diglog = true; //记录dialog
    @Getter
    @Setter
    private CenterHandlerData handleData;

    @Getter
    @Setter
    private SysAccsuperConfig entityConfig;

    @Getter
    //固定处理事件，按顺序处理
    private List<ICenterHandle> handles = new ArrayList<>();

    public void addLastHandle(ICenterHandle handle) {
        handles.add(handle);
    }

    public void setFinal() {
        handleFinal = true;
    }

    /**
     * 记录dialog
     *
     * @return
     */
    public Boolean doDiglog() {
        return diglog;
    }

    /**
     * 处理是否已完成
     */
    public Boolean isFinal() {
        return handleFinal;
    }


    public String getContent() {
        CenterHandlerXml inMsg = getHandleData().getInMsg();

        String content = inMsg.getContent();
        if (XStr.isNullOrEmpty(content)) {
            content = "";
        }

        return content;
    }
}
