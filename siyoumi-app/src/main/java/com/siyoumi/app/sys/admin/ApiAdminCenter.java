package com.siyoumi.app.sys.admin;

import com.siyoumi.app.entity.SysSet;
import com.siyoumi.app.modules.account.service.SvcAccount;
import com.siyoumi.app.modules.account.service.SvcSysStore;
import com.siyoumi.app.modules.account.vo.VaChangeStore;
import com.siyoumi.app.modules.app_fks.service.SvcFksFile;
import com.siyoumi.app.modules.app_fks.vo.FksFileRename;
import com.siyoumi.app.sys.service.AdminService;
import com.siyoumi.app.sys.vo.ApprVo;
import com.siyoumi.app.service.SysSetService;
import com.siyoumi.controller.AdminApiController;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.entity.SysApp;
import com.siyoumi.entity.SysAppRouter;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.service.SysAppService;
import com.siyoumi.util.*;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/xadmin/sys/center")
public class ApiAdminCenter
        extends AdminApiController {
    @Autowired
    private AdminService adminService;

    @GetMapping()
    public XReturn center() {
        SysAccountService appAcc = SysAccountService.getBean();
        SysAccount entityAcc = appAcc.getSysAccountByToken();

        //应用ID
        List<SysApp> apps = SysAppService.getBean().getApps(entityAcc, true);
        List<String> appIds = SysAppService.getBean().getAppIds(entityAcc, true);

        List<SysAppRouter> listAppr = appAcc.getListAppRouter(entityAcc);
        List<ApprVo> listApprVo = adminService.appRouterToApprVo(listAppr);


        Boolean existsWebsite = apps.stream().findFirst().map(app ->
        {
            return app.getApp_id().equals("website");
        }).get();
        if (existsWebsite) {
            log.debug("存在官网应用，动态加载页面");

            //官网1级菜单
            ApprVo webSiteApprVo = new ApprVo();
            webSiteApprVo.setAppr_app_id("website");
            webSiteApprVo.setAppr_pid("");
            webSiteApprVo.setMetaTitle("官网");
            webSiteApprVo.setAppr_meta_icon("website");
            webSiteApprVo.setAppr_path("/website");
            webSiteApprVo.setAppr_file_path("@/views/app/website/website");
            webSiteApprVo.setAppr_hidden(0);

            //官网2级菜单
            List<SysSet> listWebSite = SysSetService.getBean().getListWebSite(XHttpContext.getX());
            List<ApprVo> listChildVo = listWebSite.stream().map(item ->
            {
                String path = XStr.concat("/website/website__item__list?pid=", item.getKey());
                String filePath = "@/views/app/website/website__item__list";
                if (item.getSet_type().equals(0)) {
                    path = XStr.concat("/website/website__item__edit?pid=", item.getKey());
                    filePath = "@/views/app/website/website__item__edit";
                }

                ApprVo pVoChildren = new ApprVo();
                pVoChildren.setAppr_app_id("website");
                pVoChildren.setMetaTitle(item.getSet_name());
                pVoChildren.setAppr_path(path);
                pVoChildren.setAppr_file_path(filePath);

                return pVoChildren;
            }).collect(Collectors.toList());
            webSiteApprVo.setChildren(listChildVo);

            listApprVo.add(webSiteApprVo);
        }

        XReturn r = XReturn.getR(0);
        r.setData("acc", entityAcc);
        r.setData("app_ids", appIds);
        r.setData("apps_r", listApprVo);
        r.setData("x_center", SysAccsuperConfigService.isCenter());
        //商家信息，非商家类型帐号，返回null
        Map<String, Object> mapStore = SvcAccount.getBean().storeInfo(entityAcc);
        r.setData("store", mapStore);

        return r;
    }

    //切换商家
    @PostMapping("change_store")
    public XReturn changeStore(@Validated VaChangeStore vo, BindingResult result) {
        //通用验证
        XValidator.getResult(result, true, true);
        //
        vo.setAcc_id(getAccId());
        //
        return SvcAccount.getBean().changeStore(vo);
    }
}
