package com.siyoumi.app.sys.wxapp;

import com.siyoumi.app.entity.SysAddress;
import com.siyoumi.app.sys.service.AddressService;
import com.siyoumi.controller.WxAppApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import com.siyoumi.component.http.XHttpContext;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//地址相关接口
@RestController
@RequestMapping("/wxapp/app/api")
public class ApiAddress
        extends WxAppApiController {

    @GetMapping({"/address_list"})
    public XReturn addressList() {
        return AddressService.getBean().addressList(XHttpContext.InputAll());
    }

    @PostMapping({"/address_add"})
    public XReturn addressAdd(@Validated SysAddress entity, BindingResult result) {
        XValidator.getResult(result, true);

        return AddressService.getBean().addressAdd(entity, XHttpContext.InputAll());
    }

    @GetMapping({"/address_def"})
    public XReturn addressDef() {
        String id = input("id");
        return AddressService.getBean().addressDef(id);
    }

    @GetMapping({"/address_del"})
    public XReturn addressDel() {
        String id = input("id");
        return AddressService.getBean().addressDel(id);
    }
}
