package com.siyoumi.app.sys.wxapp;

import com.siyoumi.app.entity.SysOrder;
import com.siyoumi.app.service.SysOrderService;
import com.siyoumi.app.sys.service.payorder.PayOrder;
import com.siyoumi.component.XApp;
import com.siyoumi.controller.ApiController;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//通用订单接口
@RestController
@RequestMapping({"/wxapp/order/api"})
public class ApiOrder
        extends ApiController {
    /**
     * 订单支付
     */
    @GetMapping("order_pay")
    public XReturn orderPay() {
        String orderId = input("order_id");
        XValidator.isNullOrEmpty(orderId, "miss order_id");
        SysOrder entityOrder = SysOrderService.getBean().getEntityByOrderId(orderId);
        XValidator.isNull(entityOrder, "订单号异常");

        return PayOrder.getIns(entityOrder).pay("");
    }

    /**
     * 订单检查
     */
    @GetMapping("order_check")
    public XReturn orderCheck() {
        String orderId = input("order_id");
        XValidator.isNullOrEmpty(orderId, "miss order_id");
        SysOrder entityOrder = SysOrderService.getBean().getEntityByOrderId(orderId);
        XValidator.isNull(entityOrder, "订单号异常");

        return XApp.getTransaction().execute(status -> PayOrder.getIns(entityOrder).payCheck());
    }

    /**
     * 订单取消
     */
    @GetMapping("order_cancel")
    public XReturn orderCancel() {
        String orderId = input("order_id");
        XValidator.isNullOrEmpty(orderId, "miss order_id");
        SysOrder entityOrder = SysOrderService.getBean().getEntityByOrderId(orderId);
        XValidator.isNull(entityOrder, "订单号异常");

        return XApp.getTransaction().execute(status -> PayOrder.getIns(entityOrder).cancel());
    }
}
