package com.siyoumi.app.sys.service.netty;

import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.netty.NettyServerChannelHandler;
import com.siyoumi.app.netty.NettyUtil;
import com.siyoumi.app.netty.entity.EnumNettyErr;
import com.siyoumi.app.netty.entity.NettyMsg;
import com.siyoumi.app.netty.entity.NettyRoomUser;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.component.XRedis;
import com.siyoumi.config.SysConfig;
import com.siyoumi.util.XStr;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class LoginQrHandler
        extends NettyServerChannelHandler {
    @Override
    protected Integer roomJoinMax() {
        return 1;
    }

    //加入房间
    @Override
    protected NettyMsg handlerJoinRoomBegin(Channel channel, NettyMsg msg) {
        log.debug("加入房间成功，通知登陆页面及自己");
        WxUser entityWxUser = WxUserService.getBean().getByOpenid(msg.getId(), NettyUtil.getX(channel));
        if (entityWxUser == null) {
            log.debug("handlerJoinRoomBegin -1");
            return NettyMsg.getR(msg.getRoomId(), 20032, "用户id异常");
        }

        log.debug("handlerJoinRoomBegin 1");
        NettyMsg r = NettyMsg.getR(msg.getRoomId(), 0);
        log.debug("handlerJoinRoomBegin 2");
        r.setAction(msg.getAction());
        r.setId(msg.getId());
        r.setData("wxuser_nickName", entityWxUser.getWxuser_nickname());
        r.setData("wxuser_headimgurl", entityWxUser.getWxuser_headimgurl());

        log.debug("handlerJoinRoomBegin end");
        return r;
    }

    @Override
    protected void handlerJoinRoomAfter(Channel channel, NettyMsg msg) {
        Map<String, Channel> users = getRoomIdAllUser(msg.getRoomId(), null);
        for (Map.Entry<String, Channel> entry : users.entrySet()) {
            NettyUtil.sendText(entry.getValue(), msg);
        }
    }

    //通知
    @Override
    protected NettyMsg handlerRoomMsgAfter(Channel channel, NettyMsg msg) {
        if (XStr.isNullOrEmpty(msg.getEvent())) {
            return EnumNettyErr.MISS_EVENT.getR(msg.getRoomId());
        }
        NettyRoomUser roomUser = getRoomUser(channel);
        if (roomUser.getRoomMaster()) {
            return EnumNettyErr.ROOM_ERR.getR(msg.getRoomId(), "身份异常");
        }

        NettyMsg r = NettyMsg.getR(msg.getRoomId(), 0);
        r.setId(msg.getId());
        r.setAction(msg.getAction());
        r.setEvent(msg.getEvent());

        if ("1".equals(r.getEvent())) {
            log.debug("确认登陆，设置openid");

            XRedis.getBean().setEx(roomUser.getRoomId(), roomUser.getId(), 300);
        }

        log.debug("通知登陆页面及自己");
        Map<String, Channel> users = getRoomIdAllUser(msg.getRoomId(), null);
        for (Map.Entry<String, Channel> entry : users.entrySet()) {
            NettyUtil.sendText(entry.getValue(), r);
        }

        log.debug("通信完成，删除房间");
        delRedisRoomData(r.getRoomId(), true, r.getId());

        return NettyMsg.getR(msg.getRoomId());
    }

    @Override
    protected NettyMsg handlerCreateRoomAfter(Channel channel, NettyMsg msg) {
        String root = SysConfig.getIns().getAppRoot();

        String x = "pp";
        if (SysConfig.getIns().isDev()) {
            x = "wp";
        }

        //小程序码
        String qrUrl = XStr.concat(root, "sys/app/api/wxapp_qr?x=", x
                , "&path=pages/login_qr/login_qr&scene=", msg.getRoomId());
        msg.setData("qr_url", qrUrl);

        NettyUtil.sendText(channel, msg);
        return super.handlerCreateRoomAfter(channel, msg);
    }
}
