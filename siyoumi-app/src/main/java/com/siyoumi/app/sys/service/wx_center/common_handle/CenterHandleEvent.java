package com.siyoumi.app.sys.service.wx_center.common_handle;

import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerXml;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.app.sys.service.wx_center.event.EventHandleMenu;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

//只处理某些事件
@Slf4j
public class CenterHandleEvent
        implements ICenterHandle {
    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        CenterHandlerXml inMsg = ctx.getHandleData().getInMsg();

        if (inMsg.isEvent()) {
        } else {
            LogPipeLine.getTl().setLogMsg("非事件消息");
            return null;
        }

        ctx.setFinal(); //测试用
        String event = inMsg.getEvent();
        LogPipeLine.getTl().setLogMsgFormat("CenterHandleEvent event: {0}", event);

        Boolean menuEvent = isMenuEvent(inMsg);
        if (menuEvent) {
            //菜单事件
            EventHandleMenu handleMenu = new EventHandleMenu();
            return handleMenu.handle(ctx);
        }

        ICenterHandle handle = getHandleByEvent(inMsg.getEvent(), ctx);
        if (handle == null) {
            LogPipeLine.getTl().setLogMsgFormat("{0}: 不处理", inMsg.getEvent());
            ctx.setDiglog(false);
            ctx.setFinal();
            return null;
        }

        return handle.handle(ctx);
    }

    /**
     * 是否为菜单事件
     *
     * @param inMsg
     */
    public Boolean isMenuEvent(CenterHandlerXml inMsg) {
        if ("location_select".equals(inMsg.getEvent())) {
            return false;
        }

        List<String> eventMenuArr = new ArrayList<>();
        eventMenuArr.add("CLICK");
        eventMenuArr.add("VIEW");
        eventMenuArr.add("view_miniprogram");

        String event = inMsg.getEvent();
        if (eventMenuArr.contains(event)) {
            return true;
        }

        return false;
    }

    /**
     * 根据appid，处理
     *
     * @param event
     * @param ctx
     */
    @SneakyThrows
    static public ICenterHandle getHandleByEvent(String event, CenterHandlerContext ctx) {
        LogPipeLine.getTl().setLogMsgFormat("event: {0}", event);

        if (XStr.isNullOrEmpty(event)) {
            return null;
        }

        String className = XStr.lineToHump(event);
        className = XStr.toUpperCase1(className);
        String classPackage = XStr.concat("com.siyoumi.app.modules.sys.service.wx_center.event.EventHandle", className);
        LogPipeLine.getTl().setLogMsgFormat("className: EventHandle{0}", className);
        log.debug("classPackage:{}", classPackage);

        Class<?> clazz = null;
        try {
            clazz = Class.forName(classPackage);
        } catch (ClassNotFoundException ex) {
            LogPipeLine.getTl().setLogMsgFormat("未找到处理类");
        }

        if (clazz == null) {
            return null;
        }

        return (ICenterHandle) clazz.newInstance();
    }
}
