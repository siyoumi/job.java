package com.siyoumi.app.sys.service.prize.entity;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class PrizeUseData {
    @Size(max = 50)
    private String prize_user_name;
    @Size(max = 50)
    private String prize_user_phone;
    @Size(max = 300)
    private String prize_user_address;

    @Size(max = 50)
    private String pwd;
}
