package com.siyoumi.app.sys.service.impl;

import com.siyoumi.app.sys.service.WxTokenService;
import com.siyoumi.app.sys.service.wxapi.ApiBase;
import com.siyoumi.app.sys.service.wxapi.ApiConfig;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WxTokenServiceImpl
        extends WxTokenService {

    @Override
    protected String getApiToken(SysAccsuperConfig entityConfig) {
        ApiConfig apiConfig = new ApiConfig();
        apiConfig.setAppId(entityConfig.getAconfig_app_id());
        apiConfig.setAppSecret(entityConfig.getAconfig_app_secret());

        ApiBase api = new ApiBase();
        api.setConfig(apiConfig);
        XReturn r = api.getAccessToken(false);
        if (r.err()) {
            XValidator.err(r);
        }

        return r.getData("access_token");
    }

    @Override
    protected String getApiJsTicket(SysAccsuperConfig entityConfig) {
        return null;
    }
}
