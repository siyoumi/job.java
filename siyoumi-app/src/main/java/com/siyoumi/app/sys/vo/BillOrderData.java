package com.siyoumi.app.sys.vo;

import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//商家提现
@Data
@Slf4j
@Accessors(chain = true)
public class BillOrderData {
    String openid;
    String user_name; //收款用户姓名
    String out_bill_no; //订单号
    String transfer_scene_id; //场景ID
    BigDecimal price; //提现金额
    String transfer_remark; //转账备注
    List<Map<String, Object>> reportInfos; //报备信息

    static public BillOrderData of(String openid, String out_bill_no, String transfer_scene_id, String user_name, BigDecimal price, String transfer_remark) {
        return new BillOrderData()
                .setOpenid(openid)
                .setOut_bill_no(out_bill_no)
                .setTransfer_scene_id(transfer_scene_id)
                .setUser_name(user_name)
                .setPrice(price)
                .setTransfer_remark(transfer_remark);
    }

    /**
     * 根据场景ID，获取报备信息
     */
    public List<Map<String, Object>> getReportInfos(String infoContent0, String infoContent1) {
        if (reportInfos != null) {
            return reportInfos;
        }

        //默认场景
        List<Map<String, Object>> reportInfos = new ArrayList<>();
        switch (getTransfer_scene_id()) {
            case "1005":
                reportInfos = new ArrayList<>();
            {
                Map<String, Object> infoItem = new HashMap<>();
                infoItem.put("info_type", "岗位类型");
                infoItem.put("info_content", "主播");
                if (XStr.hasAnyText(infoContent0)) {
                    infoItem.put("info_content", infoContent0);
                }
                reportInfos.add(infoItem);
            }
            {
                Map<String, Object> infoItem = new HashMap<>();
                infoItem.put("info_type", "报酬说明");
                infoItem.put("info_content", "支付佣金");
                if (XStr.hasAnyText(infoContent1)) {
                    infoItem.put("info_content", infoContent1);
                }
                reportInfos.add(infoItem);
            }
            break;
            default:
                XValidator.err(20055, "未开发");
        }

        return reportInfos;
    }
}
