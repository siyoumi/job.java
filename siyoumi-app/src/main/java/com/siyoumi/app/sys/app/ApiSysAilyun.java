package com.siyoumi.app.sys.app;

import com.siyoumi.app.sys.service.FileHandle;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.config.SysConfig;
import com.siyoumi.controller.ApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//阿里云
@RestController
@RequestMapping({"/aliyun"})
public class ApiSysAilyun
        extends ApiController {
    /**
     * 阿里云oss访问文件
     * http://dev.x.siyoumi.com/aliyun/app/data_2_1/3.jpg.file
     */
    @SneakyThrows
    @GetMapping("/**/{fileName}.file")
    public void aliyunFileUrl() {
        String urlPath = XHttpContext.getUrlPath();
        String fileVal = urlPath.replace("/aliyun/", "")
                .replace(".file", "");
        if (XStr.isNullOrEmpty(fileVal)) {
            XValidator.err(EnumSys.MISS_VAL.getR("miss file_val"));
        }

        FileHandle fileHandle = FileHandle.of("aliyun");
        String fileUrl = fileHandle.getFileUrl(fileVal);

        getResponse().sendRedirect(fileUrl);
    }
}
