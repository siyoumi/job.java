package com.siyoumi.app.sys.vo;

import lombok.Data;

//开放平台
@Data
public class WxOpenOauthVo {
    private Integer wxapp = 0; //0:公众号; 1:小程序;
}