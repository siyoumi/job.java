package com.siyoumi.app.sys.entity;

import com.siyoumi.component.XEnumBase;


public class EnumEnable
        extends XEnumBase<Integer> {
    @Override
    protected void initKV() {
        put(0, "下架");
        put(1, "上架");
    }
}
