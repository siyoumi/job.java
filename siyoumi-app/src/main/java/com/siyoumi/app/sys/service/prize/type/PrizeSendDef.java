package com.siyoumi.app.sys.service.prize.type;

import com.siyoumi.app.entity.SysPrize;
import com.siyoumi.app.sys.service.prize.PrizeSend;
import com.siyoumi.util.XReturn;
import org.springframework.stereotype.Service;

//宝箱奖
@Service
public class PrizeSendDef
        extends PrizeSend {
    @Override
    protected XReturn sendAfter(SysPrize entityPrize) {
        return XReturn.getR(0);
    }
}
