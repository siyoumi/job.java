package com.siyoumi.app.sys.service.wx_center.event;

import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerData;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerXml;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.app.sys.service.wxapi.WxApiMp;
import com.siyoumi.component.LogPipeLine;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;

//关注事件、扫码关注事件
@Slf4j
public class EventHandleSubscribe
        implements ICenterHandle {
    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        CenterHandlerData data = ctx.getHandleData();

        CenterHandlerXml inMsg = data.getInMsg();
        if (XStr.hasAnyText(inMsg.getEventKey())) {
            LogPipeLine.getTl().setLogMsg("扫码关注");

            String word = inMsg.getEventKey().replace("qrscene_", "");
            ctx.getHandleData().setWord(word);

            WxApiMp api = WxApiMp.getIns(ctx.getEntityConfig());
            api.sendText(ctx.getHandleData().getOpenid(), "扫码关注");
        } else {
            LogPipeLine.getTl().setLogMsg("普通关注");

            WxApiMp api = WxApiMp.getIns(ctx.getEntityConfig());
            api.sendText(ctx.getHandleData().getOpenid(), "感谢关注~");
        }

        return null;
    }
}
