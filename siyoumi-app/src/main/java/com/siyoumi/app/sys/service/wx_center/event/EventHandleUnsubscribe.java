package com.siyoumi.app.sys.service.wx_center.event;

import com.siyoumi.app.sys.service.wx_center.CenterHandlerContext;
import com.siyoumi.app.sys.service.wx_center.CenterHandlerData;
import com.siyoumi.app.sys.service.wx_center.ICenterHandle;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;

//取消关注事件
@Slf4j
public class EventHandleUnsubscribe
        implements ICenterHandle {
    @Override
    public XReturn handle(CenterHandlerContext ctx) {
        log.info("EventHandleUnsubscribe");

        CenterHandlerData data = ctx.getHandleData();

        //SubHandleData handleData = new SubHandleData();
        //handleData.setOpenid(data.getOpenid());
        //handleData.setType("unsub");
        //handleData.setTime(Helper.funcTimestampToDateTime(data.getInMsg().getCreateTime().toString()));
        //
        //SvcWxUser.getBean().subAndUnSubHandle(handleData);
        //取关事件，终止后面处理
        ctx.setFinal();
        return null;
    }
}
