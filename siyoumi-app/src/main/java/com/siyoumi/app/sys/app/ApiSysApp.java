package com.siyoumi.app.sys.app;

import com.siyoumi.app.feign.view_admin.SiyoumiFeign;
import com.siyoumi.app.sys.service.CommonApiServcie;
import com.siyoumi.app.sys.service.FileHandle;
import com.siyoumi.app.sys.service.impl.WxAppServiceImpl;
import com.siyoumi.app.sys.vo.WxAppQrVo;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.controller.ApiController;
import com.siyoumi.entity.SysAppRouter;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.SysAppRouterService;
import com.siyoumi.util.XReturn;
import com.siyoumi.component.http.InputData;
import com.siyoumi.util.XStr;
import com.siyoumi.validator.XValidator;
import lombok.SneakyThrows;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

//通用接口
@RestController
@RequestMapping({"/sys/app/api", "/app/z_common/app_api"})
public class ApiSysApp
        extends ApiController {
    @GetMapping("/qr")
    public void qr(HttpServletResponse response) throws IOException {
        BufferedImage img = CommonApiServcie.getBean().qr(InputData.fromRequest());
        //输出图片
        ServletOutputStream outputStream = response.getOutputStream();
        //调用工具类
        ImageIO.write(img, "png", outputStream);
    }

    @GetMapping("/img_change")
    public void imgChange(HttpServletResponse response) throws IOException {
        BufferedImage img = CommonApiServcie.getBean().imgChange(InputData.fromRequest());
        //输出图片
        ServletOutputStream outputStream = response.getOutputStream();
        //调用工具类
        ImageIO.write(img, "jpg", outputStream);
    }

    @PostMapping("/img_base64")
    public XReturn imgBase64() throws IOException {
        String base64Str = input("base64");

        return CommonApiServcie.getBean().imgBase64(base64Str);
    }

    @GetMapping("/wxapp_qr")
    @SneakyThrows
    public void wxappQr(@Validated() WxAppQrVo vo, BindingResult result) {
        //统一验证
        XValidator.getResult(result, true, true);

        WxAppServiceImpl app = XSpringContext.getBean(WxAppServiceImpl.class);
        byte[] imgByte = app.wxappQr(vo);

        getResponse().setContentType("image/jpg");
        ServletOutputStream outputStream = getResponse().getOutputStream();
        outputStream.write(imgByte);
        outputStream.flush();
        outputStream.close();
    }

    @GetMapping("/info")
    public XReturn info() {
        getR().setData("url_full", XHttpContext.getUrlFull());
        getR().setData("root", XHttpContext.getRoot());
        getR().setData("ip", XHttpContext.getIpAddr());
        getR().setData("scheme", XHttpContext.getHttpServletRequest().getScheme());
        getR().setData("remote_port", XHttpContext.getHttpServletRequest().getRemotePort());

        return getR();
    }


    //开发环境同步到siyoumi
    @GetMapping("/sync_to_siyoumi")
    public XReturn syncToSiyoumi() {
        XValidator.isDev();

        List<SysAppRouter> list = SysAppRouterService.getBean().list();
        return SiyoumiFeign.getBean().updateAppRouter(list);
    }
}
