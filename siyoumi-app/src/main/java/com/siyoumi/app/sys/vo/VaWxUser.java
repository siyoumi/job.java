package com.siyoumi.app.sys.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

//更新微信用户
@Data
public class VaWxUser {
    @Size(max = 50, message = "昵称过长")
    private String wxuser_nickname;
    @HasAnyText
    @Size(max = 200)
    private String wxuser_headimgurl;
    @Size(max = 50, message = "手机号太长")
    private String wxuser_phone;
}
