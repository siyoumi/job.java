package com.siyoumi.app.sys.service;

import com.siyoumi.app.entity.SysOrder;
import com.siyoumi.app.service.SysOrderService;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.util.XReturn;

import java.math.BigDecimal;
import java.util.List;

//支付接口
public interface IPayOrder {
    String KEY = "PayOrderEntity";
    String KEY_CONFIG = "PayOrderConfig";

    static SysOrderService getApp() {
        return SysOrderService.getBean();
    }

    default void setEntity(Object entity) {
        XHttpContext.set(KEY, entity);
    }

    default SysOrder getEntity() {
        return XHttpContext.get(KEY);
    }

    /**
     * 支付
     */
    XReturn pay(String code);

    /**
     * 选择支付方式
     */
    XReturn payChoose(String code);

    /**
     * 检查支付
     */
    XReturn payCheck();

    /**
     * 订单回滚
     */
    XReturn rollback();

    /**
     * 订单取消
     */
    XReturn cancel();


    /**
     * 退款申请
     */
    XReturn refundApply(String refundId, BigDecimal refundPrice, String desc);

    /**
     * 退款
     */
    XReturn refund(String refundId);
}
