package com.siyoumi.app.sys.entity;

import com.siyoumi.util.XDate;
import lombok.Data;

/**
 * token
 */
@Data
public class GetTokenData {
    Long timestamp;
    String openid;
    String x;
    String uid;
    String ki; //加解密串
    Integer expireSecond = 3600 * 12;

    public static GetTokenData of(String uid, String x, String openid) {
        GetTokenData data = new GetTokenData();
        data.setTimestamp(XDate.toMs());
        data.setX(x);
        data.setOpenid(openid);
        data.setUid(uid);

        return data;
    }
}
