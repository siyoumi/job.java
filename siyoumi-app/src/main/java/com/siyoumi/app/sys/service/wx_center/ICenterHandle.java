package com.siyoumi.app.sys.service.wx_center;

import com.siyoumi.util.XReturn;

//处理
public interface ICenterHandle {
    XReturn handle(CenterHandlerContext ctx);
}
