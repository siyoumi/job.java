package com.siyoumi.app.sys.service.wxapi;

import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.util.XHttpClient;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

//微信公众号接口
@Slf4j
public class WxApiMp
        extends ApiBase {
    static public WxApiMp getIns(SysAccsuperConfig entityConfig) {
        WxApiMp app = new WxApiMp();
        app.setConfig(entityConfig);
        return app;
    }


    /**
     * 获取订阅消息模板列表
     */
    public XReturn getTemplateList() {
        String apiUrl = getApiUrl("cgi-bin/template/get_all_private_template", getConfig().getAccessToken());
        return api(apiUrl);
    }

    /**
     * 发送
     */
    public XReturn sendTemplate(Map<String, Object> postData) {
        String apiUrl = getApiUrl("cgi-bin/message/template/send", getConfig().getAccessToken());
        return api(apiUrl, XHttpClient.METHOD_POST, postData);
    }

    /**
     * 获取授权token及openid
     *
     * @param code
     */
    public XReturn getOauthAccessToken(String code) {
        String apiUrl = getApiUrl("sns/oauth2/access_token");

        Map<String, String> data = new HashMap<>();
        data.put("appid", getConfig().getAppId());
        data.put("secret", getConfig().getAppSecret());
        data.put("code", code);
        data.put("grant_type", "authorization_code");

        String query = XStr.queryFromMap(data, null, false);

        String url = apiUrl + "?" + query;

        return api(url);
    }

    /**
     * 获取用户详细信息
     */
    public XReturn getOauthUserInfo(String accessToken, String openid) {
        String apiUrl = getApiUrl("sns/userinfo");
        Map<String, String> data = new HashMap<>();
        data.put("access_token", accessToken);
        data.put("openid", openid);
        data.put("lang", "zh_CN");
        String query = XStr.queryFromMap(data, null, false);

        String url = apiUrl + "?" + query;

        return api(url);
    }


    /**
     * 发客服消息
     *
     * @param postData
     */
    public XReturn sendCustomApi(Map<String, Object> postData) {
        String apiUrl = getApiUrl("cgi-bin/message/custom/send", getConfig().getAccessToken());
        return api(apiUrl, XHttpClient.METHOD_POST, postData);
    }

    /**
     * 发图文
     */
    public XReturn sendNews(String openId, String title, String image, String desc, String url) {
        Map<String, Object> json = new HashMap<>();
        json.put("touser", openId);
        json.put("msgtype", "news");

        Map<String, Object> news = new HashMap<>();

        List<Map<String, Object>> articles = new LinkedList<>();
        Map<String, Object> article = new HashMap<>();
        article.put("title", title);
        article.put("description", desc);
        article.put("url", url);
        article.put("picurl", image);
        articles.add(article);
        news.put("articles", articles);

        json.put("news", news);
        return sendCustomApi(json);
    }


    /**
     * 发文本
     */
    public XReturn sendText(String openId, String text) {
        Map<String, Object> json = new HashMap();
        json.put("touser", openId);
        json.put("msgtype", "text");
        Map<String, Object> textObj = new HashMap();
        textObj.put("content", text);
        json.put("text", textObj);

        return sendCustomApi(json);
    }

    /**
     * 发图片
     */
    public XReturn sendImage(String openId, String media_id) {
        Map<String, Object> json = new HashMap<>();
        json.put("touser", openId);
        json.put("msgtype", "image");

        Map<String, Object> image = new HashMap<>();
        image.put("media_id", media_id);

        json.put("image", image);
        return sendCustomApi(json);
    }

    /**
     * 发音乐
     */
    public XReturn sendMusic(String openId, String title, String desc, String url, String thumb_media_id) {
        Map<String, Object> json = new HashMap<>();
        json.put("touser", openId);
        json.put("msgtype", "music");

        Map<String, Object> music = new HashMap<>();
        music.put("title", title);
        music.put("description", desc);
        music.put("musicurl", url);
        music.put("hqmusicurl", url);
        music.put("thumb_media_id", thumb_media_id);

        json.put("music", music);
        return sendCustomApi(json);
    }

    /**
     * 发小程序卡片
     */
    public XReturn sendMiniProgramPage(String openId, String title, String appid
            , String pagepath, String thumb_media_id) {
        Map<String, Object> json = new HashMap<>();
        json.put("touser", openId);
        json.put("msgtype", "miniprogrampage");

        Map<String, Object> sub = new HashMap<>();
        sub.put("title", title);
        sub.put("appid", appid);
        sub.put("pagepath", pagepath);
        sub.put("thumb_media_id", thumb_media_id);

        json.put("miniprogrampage", sub);
        return sendCustomApi(json);
    }


    /**
     * 获取素材总数
     *
     * @return ApiResult 返回信息
     */
    public XReturn getMaterialCount() {
        String apiUrl = getApiUrl("cgi-bin/material/get_materialcount", getConfig().getAccessToken());
        return api(apiUrl);
    }

    /**
     * 获取素材列表
     *
     * @param mediaType 素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news）
     * @param offset    从全部素材的该偏移位置开始返回，0表示从第一个素材 返回
     * @param count     返回素材的数量，取值在1到20之间
     * @return ApiResult 返回信息
     */
    public XReturn batchGetMaterial(String mediaType, int offset, int count) {
        String apiUrl = getApiUrl("cgi-bin/material/batchget_material", getConfig().getAccessToken());

        if (offset < 0) offset = 0;
        if (count > 20) count = 20;
        if (count < 1) count = 1;

        Map<String, Object> mapPost = new HashMap<>();
        mapPost.put("type", mediaType);
        mapPost.put("offset", offset);
        mapPost.put("count", count);

        return api(apiUrl, XHttpClient.METHOD_POST, mapPost);
    }
}
