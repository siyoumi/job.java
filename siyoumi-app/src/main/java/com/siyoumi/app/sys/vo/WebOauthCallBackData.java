package com.siyoumi.app.sys.vo;

import lombok.Data;

//网页授权-回调参数
@Data
public class WebOauthCallBackData {
    private String openid;
    private String webToken;


    public static WebOauthCallBackData getIns(String openid, String webToken) {
        WebOauthCallBackData data = new WebOauthCallBackData();
        data.setOpenid(openid);
        data.setWebToken(webToken);
        return data;
    }
}
