package com.siyoumi.app.sys.admin;

import com.siyoumi.app.sys.service.AdminService;
import com.siyoumi.app.sys.vo.AdminLoginQrVo;
import com.siyoumi.app.sys.vo.AdminLoginVo;
import com.siyoumi.controller.ApiController;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.IOException;

@RestController
@RequestMapping("/sys/login")
public class ApiAdminLogin
        extends ApiController {
    @PostMapping()
    public XReturn login(@Valid AdminLoginVo loginVo) {
        if (XStr.contains(loginVo.getDecPwd(), " ")) {
            return EnumSys.ERR_VAL.getR("密码存在空格");
        }
        return AdminService.getBean().login(loginVo);
    }

    @GetMapping("/login_qr_init")
    public XReturn loginQrInit() {
        return AdminService.getBean().loginQrInit();
    }

    @GetMapping("/login_qr")
    public XReturn loginQr(@Valid AdminLoginQrVo vo) {
        return AdminService.getBean().loginQr(vo);
    }

    @GetMapping("/vcode")
    public void vcode(HttpServletResponse response) throws IOException {
        BufferedImage img = AdminService.getBean().authCode();
        //输出图片
        ServletOutputStream outputStream = response.getOutputStream();
        //调用工具类
        ImageIO.write(img, "png", outputStream);
    }


    @GetMapping("/auth_img_drag")
    public XReturn authImgDrag(HttpServletResponse response) throws IOException {
        return AdminService.getBean().authImgDrag();
    }
}
