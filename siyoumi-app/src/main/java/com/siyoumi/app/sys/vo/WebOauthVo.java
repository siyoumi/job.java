package com.siyoumi.app.sys.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import lombok.Data;

import javax.validation.constraints.NotBlank;

//网页授权
@Data
public class WebOauthVo {
    @HasAnyText(message = "缺少x")
    private String x;
    @HasAnyText(message = "缺少应用ID")
    private String app_id;
    private String key = ""; //附加参数

    private String scope = "snsapi_base"; //隐式：snsapi_base，显式：snsapi_userinfo

    private String code;
    private String test_uid;
    private String scene; //备用
}
