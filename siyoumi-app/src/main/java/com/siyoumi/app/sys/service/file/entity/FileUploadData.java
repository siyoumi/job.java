package com.siyoumi.app.sys.service.file.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

@Data
@Accessors(chain = true)
public class FileUploadData {
    MultipartFile multFile;
    InputStream fileInputStream;

    List<String> fileExtenstions;
    Integer fileSizeMax; //mb
    String saveFileDir; //保存文件目录

    static public FileUploadData of(MultipartFile file, List<String> fileExtenstions, Integer fileSizeMax, String saveFileDir) {
        FileUploadData data = new FileUploadData();
        data.setMultFile(file)
                .setFileExtenstions(fileExtenstions)
                .setFileSizeMax(fileSizeMax)
                .setSaveFileDir(saveFileDir)
        ;
        return data;
    }
}
