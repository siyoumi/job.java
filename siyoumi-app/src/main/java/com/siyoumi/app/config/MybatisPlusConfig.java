package com.siyoumi.app.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.mybatispuls.FuncInjector;
import com.siyoumi.mybatispuls.SqlInterceptor;
import com.siyoumi.mybatispuls.SqlLogInterceptor;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.annotation.Resources;
import javax.sql.DataSource;
import java.util.function.Consumer;

@Configuration
@EnableTransactionManagement
@MapperScan({"com.siyoumi.app.mapper", "com.siyoumi.mapper"})
public class MybatisPlusConfig {
    @Bean
    public FuncInjector sqlInjector() {
        return new FuncInjector();
    }

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        //interceptor.addInnerInterceptor(new SQLInterceptor());
        return interceptor;
    }

    //@Bean
    //@ConfigurationProperties("mybatis-plus")
    //public MybatisPlusProperties mybatisProperties() {
    //    return new MybatisPlusProperties();
    //}
    //
    //@Resource(name = "dataSource")
    //private DataSource dataSource;
    //
    //@Bean
    //public SqlSessionFactory sqlSessionFactory(MybatisPlusProperties mybatisProperties) throws Exception {
    //    MybatisSqlSessionFactoryBean factoryBean = new MybatisSqlSessionFactoryBean();
    //    factoryBean.setDataSource(dataSource);
    //
    //    factoryBean.setMapperLocations(mybatisProperties.resolveMapperLocations());
    //    factoryBean.setConfiguration(mybatisProperties.getConfiguration());
    //    factoryBean.setPlugins(new SqlInterceptor());
    //    //factoryBean.setPlugins((Interceptor) new PaginationInnerInterceptor(DbType.MYSQL));
    //
    //    GlobalConfig globalConfig = new GlobalConfig();
    //    factoryBean.setGlobalConfig(globalConfig);
    //
    //    // 其他配置...
    //    return factoryBean.getObject();
    //}


}
