package com.siyoumi.app.config.boot;

import com.siyoumi.component.XRedis;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RTopic;

//测试入口
@Slf4j
public class TestBoot
        implements ISiyoumiBoot {
    @Override
    public void run() {
        log.debug("test boot");

        RTopic testSub = XRedis.getBean().getTopic("test_sub");
        int i = testSub.addListener(XReturn.class, (charSequence, r1) -> {
            log.debug("收到信息:{}", XStr.toJsonStr(r1));
            log.debug(XDate.toDateTimeString());
        });
    }
}
