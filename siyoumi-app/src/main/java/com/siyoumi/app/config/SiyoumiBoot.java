package com.siyoumi.app.config;

import com.siyoumi.app.config.boot.ISiyoumiBoot;
import com.siyoumi.component.XBean;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

//springboot启动完成后，再做的事情
@Slf4j
@Component
public class SiyoumiBoot
        implements CommandLineRunner {
    @Value("${siyoumi.boot-class:#{null}}")
    private String bootClass;

    @Override
    public void run(String... args) throws Exception {
        log.debug("自定义启动");
        if (XStr.isNullOrEmpty(bootClass)) {
            log.debug("没配置启动项");
            return;
        }

        String[] bootClassArr = bootClass.split(",");

        for (String bootClassName : bootClassArr) {
            Class<?> clazz = Class.forName(bootClassName);
            ISiyoumiBoot boot = (ISiyoumiBoot) XBean.newIns(clazz);
            log.debug("start {}", clazz.getSimpleName());

            Thread thread = new Thread(() -> {
                try {
                    boot.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }


    }
}
