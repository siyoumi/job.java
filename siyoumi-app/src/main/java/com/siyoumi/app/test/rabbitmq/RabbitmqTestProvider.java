package com.siyoumi.app.test.rabbitmq;
//

import com.siyoumi.app.rabbitmq.RabbitMqConsumerHandle;
import com.siyoumi.app.rabbitmq.RabbitMqMessage;
import com.siyoumi.app.rabbitmq.RabbitMqProperties;
import com.siyoumi.app.rabbitmq.RabbitMqPublishAbs;
import com.siyoumi.component.XApp;
import com.siyoumi.util.XDate;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RabbitmqTestProvider {
    @SneakyThrows
    public static void main(String[] args) {
        RabbitMqProperties config = new RabbitMqProperties();
        config.setHost("app.lai-m.com:5682,app.lai-m.com:5692");
        config.setUsername("admin");
        config.setPassword("admin112233");
        config.setPort(5682);
        config.setVhost("/");

        //RabbitMqConsumerHandle mp = RabbitMqConsumerHandle.getIns(config);
        //mp.consumeMessage();
        RabbitMqConsumerHandle handle = RabbitMqConsumerHandle.getIns(config, false);
        handle.setAutoClose(false);
        handle.consumeMessage(new RabbitMqConsumerDelay(), "fanout_queue");
    }
}
