package com.siyoumi.app.test.sys.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PrizeAutoConfig {
    @Bean
    public PrizeService prizeServiceRandom() {
        return new PrizeServiceRandom();
    }

    //@Bean
    //@ConditionalOnMissingBean
    //public PrizeService prizeService() {
    //    return new PrizeServiceDef();
    //}

    @Bean
    @ConditionalOnMissingBean
    public PrizeContext prizeContext(@Autowired(required = false) PrizeService s) {
        if (null == s) {
            s = new PrizeServiceDef();
        }
        return new PrizeContext(s);
    }

}
