package com.siyoumi.app.test.rabbitmq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.siyoumi.app.rabbitmq.RabbitMqConsumerHandle;
import com.siyoumi.app.rabbitmq.RabbitMqMessage;
import com.siyoumi.app.rabbitmq.RabbitMqPublishAbs;
import com.siyoumi.component.XApp;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XJson;
import com.siyoumi.util.XReturn;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

//redis相关
@TestClass(
        runMethods = "testDead"
)
@Slf4j
@RestController
@RequestMapping("/test/test_rabbitmq")
public class testRabbitmq
        extends TestController {

    @SneakyThrows
    @TestFunc("路由模式")
    public XReturn testDirect() {
        RabbitMqPublishAbs mq = RabbitMqPublishDirect.getIns();

        RabbitMqMessage msg = RabbitMqMessage.getIns("");
        for (int i = 0; i < 10; i++) {
            log.info("i: {}", i);
            String content = i + "运行时间: ";
            msg.setContent(content);
            mq.publishMessage(msg);
        }
        mq.close();

        XReturn r = XReturn.getR(0);
        return r;
    }

    @SneakyThrows
    @TestFunc("路由模式-延迟对队")
    public XReturn testDelay() {
        RabbitMqPublishAbs mq = RabbitMqPublishDelay.getIns();

        RabbitMqMessage msg = RabbitMqMessage.getIns("");
        for (int i = 0; i < 10; i++) {
            int delay = XApp.random(10, 50);
            log.info("i: {}, delay: {}", i, delay);

            String content = i + "运行时间: " + XDate.toDateTimeString(XDate.now().plusSeconds(delay));
            msg.setContent(content);
            msg.setDelaySecond(delay);
            mq.publishMessage(msg);
        }
        mq.close();

        XReturn r = XReturn.getR(0);
        return r;
    }

    @SneakyThrows
    @TestFunc("订阅模式")
    public XReturn testFanout() {
        RabbitMqPublishAbs mq = RabbitMqPublishFanout.getIns();

        RabbitMqMessage msg = RabbitMqMessage.getIns("");
        for (int i = 0; i < 3; i++) {
            log.info("i: {}", i);
            String content = i + "运行时间: " + XDate.toDateTimeString();
            msg.setContent(content);
            mq.publishMessage(msg);
        }
        mq.close();

        XReturn r = XReturn.getR(0);
        return r;
    }

    @SneakyThrows
    @TestFunc("主题模式")
    public XReturn testTopic() {
        RabbitMqPublishAbs mq = RabbitMqPublishTopic.getIns();

        mq.getChannel().queueBind("def_queue", "topic_exchange", "prize.#");
        mq.getChannel().queueBind("fanout_queue", "topic_exchange", "prize.*");

        RabbitMqMessage msg = RabbitMqMessage.getIns("");
        msg.setRoutingKey("prize.123");
        for (int i = 0; i < 10; i++) {
            log.info("i: {}", i);
            Map<String, Object> mapMsg = new HashMap<>();
            mapMsg.put("index", i);
            mapMsg.put("t", XDate.toDateTimeString());
            msg.setContent(XJson.toJSONString(mapMsg));
            mq.publishMessage(msg);
        }
        mq.close();

        XReturn r = XReturn.getR(0);
        return r;
    }

    @SneakyThrows
    @TestFunc("死信模式")
    public XReturn testDead() {
        RabbitMqPublishAbs mq = RabbitMqPublishDead.getIns();
        
        RabbitMqMessage msg = RabbitMqMessage.getIns("");
        for (int i = 0; i < 10; i++) {
            log.info("i: {}", i);
            Map<String, Object> mapMsg = new HashMap<>();
            mapMsg.put("index", i);
            mapMsg.put("t", XDate.toDateTimeString());
            msg.setContent(XJson.toJSONString(mapMsg));
            mq.publishMessage(msg);
        }
        mq.close();

        XReturn r = XReturn.getR(0);
        return r;
    }

    @SneakyThrows
    @TestFunc("消费者")
    public XReturn testConsumer() {
        RabbitMqConsumerDirect consumerDirect = new RabbitMqConsumerDirect();
        RabbitMqConsumerHandle.getIns().consumeMessage(consumerDirect, "def_queue");
        //RabbitMqConsumerHandle.getIns().consumeMessage(consumerDirect, "def_queue");
        //handle.consumeMessage(consumerDirect, "delay_queue");

        //handle.close();

        XReturn r = XReturn.getR(0);
        return r;
    }
}
