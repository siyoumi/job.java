package com.siyoumi.app.test.sys.vo;

import com.siyoumi.validator.annotation.HasAnyText;
import com.siyoumi.validator.annotation.IsJson;
import lombok.Data;

@Data
public class TestStr {
    @IsJson
    String json;

    @HasAnyText
    String str;
}
