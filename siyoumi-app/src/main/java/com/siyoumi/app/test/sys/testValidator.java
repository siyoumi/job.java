package com.siyoumi.app.test.sys;

import com.siyoumi.app.test.sys.vo.TestStr;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.util.XReturn;
import com.siyoumi.validator.XValidator;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//线程相关
@TestClass(
        //runMethods = "testSynchronized"
)
@RestController
@RequestMapping("/test/test_validator")
public class testValidator
        extends TestController {

    @PostMapping("/test001")
    public XReturn test001(@Validated() TestStr vo, BindingResult result) {
        XValidator.getResult(result);

        return getR();
    }
}
