package com.siyoumi.app.test.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.external_api.elasticsearch.EsApi;
import com.siyoumi.app.modules.app_fks.entity.FksMsgEsData;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

//redis相关
@TestClass(
        //runMethods = "testSearch"
)
@Slf4j
@RestController
@RequestMapping("/test/test_es")
public class testElasticsearch
        extends TestController {
    private EsApi getApi() {
        return EsApi.getBean();
    }

    //http://dev.x.siyoumi.com/test/test_es/doc_info
    @GetMapping("doc_info")
    public XReturn testDocInfo() {
        EsApi api = getApi();
        return api.docInfo("dev", "111");
    }

    //http://dev.x.siyoumi.com/test/test_es/analyzer_test
    @GetMapping("analyzer_test")
    public XReturn testAnalyzerTest() {
        EsApi api = getApi();
        Map<String, Object> mapPost = new HashMap<>();
        mapPost.put("analyzer", "ik_smart");
        mapPost.put("text", "我是中国人,我叫红六");
        return api.analyzerTest(mapPost);
    }
}
