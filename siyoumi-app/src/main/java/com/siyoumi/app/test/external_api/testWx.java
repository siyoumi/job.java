package com.siyoumi.app.test.external_api;

import com.siyoumi.app.sys.service.WxTokenService;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//接口
@TestClass(
        //runMethods = "upload_file"
)
@Slf4j
@RestController
@RequestMapping("/test/external_api/wx")
public class testWx
        extends TestController {
    //获取token
    //http://dev.x.siyoumi.com/test/external_api/wx/test_wx_token
    @GetMapping("test_wx_token")
    public XReturn testWxToken() {
        WxTokenService token = WxTokenService.getBean(SysAccsuperConfigService.getBean().getXConfig("x", false));

        XReturn r = XReturn.getR(0);
        r.setData("token", token.getAccessToken());
        return r;
    }
}
