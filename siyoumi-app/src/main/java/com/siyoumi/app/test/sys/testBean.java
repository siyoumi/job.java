package com.siyoumi.app.test.sys;

import com.siyoumi.app.test.sys.entity.PrizeContext;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.util.XReturn;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//线程相关
@TestClass(
        //runMethods = "enable_env"
)
@RestController
@RequestMapping("/test/test_bean")
public class testBean
        extends TestController {
    @SneakyThrows
    @GetMapping("/auto_config")
    public XReturn autoConfigs() {
        PrizeContext context = XSpringContext.getBean(PrizeContext.class);

        XReturn r = XReturn.getR(0);
        r.setData("send", context.send());
        return r;
    }
}
