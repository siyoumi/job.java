package com.siyoumi.app.test.sys.entity;

public class PrizeContext {
    PrizeService prizeService;

    public PrizeContext(PrizeService prizeService) {
        if (null == prizeService) {
            prizeService = new PrizeServiceDef();
        }
        this.prizeService = prizeService;
    }

    public String send() {
        return prizeService.send();
    }
}
