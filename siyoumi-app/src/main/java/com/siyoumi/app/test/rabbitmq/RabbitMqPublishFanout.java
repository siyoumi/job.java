package com.siyoumi.app.test.rabbitmq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.siyoumi.app.rabbitmq.RabbitMqMessage;
import com.siyoumi.app.rabbitmq.RabbitMqPublishAbs;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

//订阅发布
@Slf4j
public class RabbitMqPublishFanout
        extends RabbitMqPublishAbs {
    @Override
    protected String getExchangeName() {
        return "fanout_exchange";
    }

    @Override
    protected String getQueueName() {
        return "fanout_queue";
    }

    static public RabbitMqPublishAbs getIns() {
        RabbitMqPublishAbs mq = new RabbitMqPublishFanout();
        mq.init();
        return mq;
    }

    @SneakyThrows
    @Override
    public void init() {
        super.init();

        //创建交换机
        getChannel().exchangeDeclare(getExchangeName(), BuiltinExchangeType.FANOUT, true, false, null);
        //创建队列
        getChannel().queueDeclare(getQueueName(), true, false, false, null);

        String routeKey = "";

        getChannel().queueBind("delay_queue", getExchangeName(), routeKey);
        getChannel().queueBind("def_queue", getExchangeName(), routeKey);
        getChannel().queueBind(getQueueName(), getExchangeName(), routeKey);
    }

    @Override
    @SneakyThrows
    public void publishMessage(RabbitMqMessage message) {
        AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
                .deliveryMode(2) //消息持久化
                .build();

        getChannel().basicPublish(getExchangeName(), "", properties, message.getContent().getBytes());
    }
}