package com.siyoumi.app.test.sys;

import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.entity.SysMemberLv;
import com.siyoumi.app.entity.WxUserLogin;
import com.siyoumi.app.mapper.TestSysMapper;
import com.siyoumi.app.service.SysMemberLvService;
import com.siyoumi.app.service.WxUserLoginService;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.app.test.sys.entity.TransactionFunc;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.component.XApp;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysLogService;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//mybaits相关
@TestClass(
        runMethods = "testGetKey"
)
@Slf4j
@RestController
@RequestMapping("/test/test_mybatis")
public class testMybatis
        extends TestController {

    @TestFunc("测试拦截器")
    public XReturn testInterceptor() {
        SysAbcService svcAbc = SysAbcService.getBean();

        log.debug("添加");
        SysAbc entityNew = new SysAbc();
        entityNew.setAbc_x_id("x");
        entityNew.setAbc_app_id("test");
        entityNew.setAbc_table("test");
        entityNew.setAbc_uix(XApp.getStrID());
        entityNew.setAutoID();
        svcAbc.save(entityNew);

        log.debug("更新");
        SysAbc entityUpdate = new SysAbc();
        entityUpdate.setAbc_id(entityNew.getAbc_id());
        entityUpdate.setAbc_str_00(XDate.toDateTimeString());
        svcAbc.updateById(entityUpdate);

        log.debug("查询");
        SysAbc entityAbc = svcAbc.first(entityNew.getKey());

        log.debug("删除");
        svcAbc.removeById(entityNew);

        getR().setData("abc", entityAbc);

        return getR();
    }

    @TestFunc()
    public XReturn testSleep() {
        TestSysMapper sysMapper = XSpringContext.getBean(TestSysMapper.class);
        sysMapper.sleep();

        return getR();
    }


    @TestFunc("事务,嵌套事务")
    public XReturn testTransaction() {
        TransactionTemplate transaction = XApp.getTransaction();
        log.debug("设置事务隔离级别");
        transaction.setIsolationLevel(TransactionDefinition.ISOLATION_DEFAULT);

        XHttpContext.setX("x");
        SysLogService appLog = SysLogService.getBean();


        log.debug("嵌套事务，其他一个报异常，整个事务回滚");
        transaction.execute(status ->
        {
            appLog.addSysErrorLog("test01", "test01");

            transaction.execute(status03 ->
            {
                appLog.addSysErrorLog("test03", "test03");
                //throw new XException("test03");
                status03.setRollbackOnly();
                return true;
            });
            status.setRollbackOnly();
            //throw new XException("test02");
            return true;
        });


        transaction.execute(status ->
        {
            appLog.addSysErrorLog("test02", "test02");
            status.setRollbackOnly(); // 回滚
            return true;
        });

        return getR();
    }


    @TestFunc("测试不在容器的类，事务是否生效")
    public XReturn testTransaction02() {
        XHttpContext.setX("x");

        TransactionFunc func = new TransactionFunc();
        func.save();

        return getR();
    }


    @TestFunc("测试ID自增")
    public XReturn testIdType() {
        XHttpContext.setX("x");

        SysMemberLvService svcMember = SysMemberLvService.getBean();
        SysMemberLv entity = new SysMemberLv();
        entity.setMemlv_x_id("x");
        entity.setMemlv_uix(XApp.getStrID());
        boolean save = svcMember.save(entity);

        getR().setData("s", save);

        return getR();
    }


    @TestFunc("获取单行数据")
    public XReturn testFirst() {
        XHttpContext.setX("x");

        SysAbc entityAbc = SysAbcService.getBean().first("1");

        getR().setData("abc", entityAbc);

        return getR();
    }

    @TestFunc("测试获取主键值")
    public XReturn testGetKey() {
        XHttpContext.setX("x");

        WxUserLogin entity = WxUserLoginService.getBean().first(2);

        getR().setData("entity", entity);

        return getR();
    }
}
