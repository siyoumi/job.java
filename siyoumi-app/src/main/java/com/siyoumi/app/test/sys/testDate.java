package com.siyoumi.app.test.sys;

import com.siyoumi.component.XApp;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

//接口
@TestClass(
        runMethods = "dateList"
)
@Slf4j
@RestController
@RequestMapping("/test/test_date")
public class testDate
        extends TestController {

    @TestFunc(value = "获取日期列表")
    public XReturn dateList() {
        XReturn r = XReturn.getR(0);

        List<LocalDateTime> dates = XDate.getDateList(XDate.parse("2025-03-02"), XDate.parse("2025-03-12"), List.of(1, 2));

        r.setData("dates", dates);
        r.setData("weeks", XApp.getWeeks());
        return r;
    }
}
