package com.siyoumi.app.test.external_api;

import com.siyoumi.app.sys.service.wxapi.WxApiPay;
import com.siyoumi.app.sys.vo.BillOrderData;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

//接口
@TestClass(
        //runMethods = "upload_file"
)
@Slf4j
@RestController
@RequestMapping("/test/external_api/wx_pay")
public class testWxPay
        extends TestController {

    public WxApiPay getApi() {
        SysAccsuperConfig config = SysAccsuperConfigService.getBean().getXConfig("yituky", true);
        return WxApiPay.getIns(config);
    }

    //微信支付
    //http://dev.x.siyoumi.com/test/external_api/wx_pay/order
    @GetMapping("order")
    public XReturn order() {
        String orderId = "123546";
        String openid = "oE5xb7EPWzUIBhr4ZkCF5jD9vNhI";
        return getApi().order(orderId, 1, "测试", openid);
    }


    //商家转帐
    //http://dev.x.siyoumi.com/test/external_api/wx_pay/bill_order
    @GetMapping("bill_order")
    public XReturn billOrder() {
        String orderId = "123546A";
        String openid = "oE5xb7EPWzUIBhr4ZkCF5jD9vNhI";

        BillOrderData data = BillOrderData.of(openid, orderId, "1005", "", BigDecimal.valueOf(0.1), "测试");
        return getApi().billOrder(data);
    }

    //商家转帐查询
    //http://dev.x.siyoumi.com/test/external_api/wx_pay/bill_order_info
    @GetMapping("bill_order_info")
    public XReturn billOrderInfo() {
        String orderId = "123546";
        return getApi().billOrderInfo(orderId);
    }

    //加密字段
    //http://dev.x.siyoumi.com/test/external_api/wx_pay/enc_txt
    @GetMapping("enc_txt")
    public XReturn encTxt() {
        XReturn r = XReturn.getR(0);
        r.setData("123", getApi().encryptTxt("123"));
        r.setData("张先生", getApi().encryptTxt("张先生"));

        return r;
    }
}
