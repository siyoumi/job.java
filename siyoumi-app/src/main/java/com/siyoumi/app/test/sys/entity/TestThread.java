package com.siyoumi.app.test.sys.entity;

import com.siyoumi.util.XDate;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestThread
        implements Runnable {
    @Override
    public void run() {
        log.debug(XDate.toDateTimeString());
    }
}
