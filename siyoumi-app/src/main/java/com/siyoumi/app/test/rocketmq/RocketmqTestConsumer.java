//package com.siyoumi.app.test.rocketmq;
//
//import com.siyoumi.util.XStr;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
//import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
//import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
//import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
//import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
//import org.apache.rocketmq.common.message.MessageExt;
//import org.apache.rocketmq.remoting.protocol.heartbeat.MessageModel;
//
//import java.util.List;
//
//@Slf4j
//public class RocketmqTestConsumer {
//    public static final String TOPIC = "TopicTest";
//    public static final String CONSUMER_GROUP = "CID_JODIE_1";
//    public static final String NAMESRV_ADDR = "127.0.0.1:9876";
//
//    @SneakyThrows
//    public static void main(String[] args) {
//        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(CONSUMER_GROUP);
//
//        // Uncomment the following line while debugging, namesrvAddr should be set to your local address
//        consumer.setNamesrvAddr(NAMESRV_ADDR);
//        consumer.subscribe(TOPIC, "*");
//
//        consumer.setMessageModel(MessageModel.CLUSTERING);
//        //开启线程最小最大数
//        consumer.setConsumeThreadMin(5);
//        consumer.setConsumeThreadMax(10);
//        consumer.setConsumeMessageBatchMaxSize(1); //设置每次处理的条数
//        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET); //从队列尾开始消费
//        //consumer.setConsumeTimestamp("20181109221800");
//        consumer.registerMessageListener(new MessageListenerConcurrently() {
//            @Override
//            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
//                //System.out.printf("%s Receive New Messages: %s %n", Thread.currentThread().getName(),
//                //        msgs.get(0).getBody().toString());
//                for (MessageExt msg : msgs) {
//                    String s = XStr.toString(msg.getBody());
//                    log.debug("{} : {}", msg.getMsgId(), s);
//                }
//
//                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
//            }
//        });
//        consumer.start();
//        System.out.printf("Consumer Started.%n");
//    }
//}
