package com.siyoumi.app.test.sys;

import com.siyoumi.component.XApp;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//接口
@TestClass(
        runMethods = "stringArrayToList"
)
@Slf4j
@RestController
@RequestMapping("/test/test_string")
public class testString
        extends TestController {

    @TestFunc(value = "测试string[]互转List<string>")
    public XReturn stringArrayToList() {
        XReturn r = XReturn.getR(0);

        String[] arr = {"aaa", "bbb", "ccc"};
        List<String> list = XStr.arrToList(arr);
        list.add("123");

        String[] arrNew = XStr.listToArr(list);
        //XStr.hasAnyText()
        r.setData("list", list);
        r.setData("arr", arrNew);

        return r;
    }

    @TestFunc(value = "测试string[]")
    public XReturn stringArray() {
        XReturn r = XReturn.getR(0);

        String[] arr = {"aaa", "bbb", "ccc"};
        //XStr.hasAnyText()
        r.setData("aaa", arr.equals("aaa"));
        r.setData("aaa01", XStr.exists(arr, "aaa"));

        return r;
    }

    @TestFunc(value = "短ID")
    public XReturn enumShortId() {
        XReturn r = XReturn.getR(0);

        String shortId = XApp.shortId();

        r.setData("short_id", shortId);

        return r;
    }


    @TestFunc(value = "测试拼接")
    public XReturn concat() {
        XReturn r = XReturn.getR(0);
        r.setData("str01", XStr.concat("aaa", "111", "bbb"));
        r.setData("str02", XStr.format("aaa{0}{1}{2}", "111", "bbb"));
        r.setData("str12", XStr.format("aaa{0}{1}{0}", "111", "bbb"));
        r.setData("str03", XStr.format("", "111", "bbb"));
        r.setData("str04", XStr.format("aaa{0}{1}{0}"));

        return r;
    }
}
