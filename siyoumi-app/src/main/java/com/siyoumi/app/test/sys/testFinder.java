package com.siyoumi.app.test.sys;

import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.util.XFinder;
import com.siyoumi.util.XReturn;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//线程相关
@TestClass(
        //runMethods = "testSynchronized"
)
@RestController
@RequestMapping("/test/test_finder")
public class testFinder
        extends TestController {

    @GetMapping("/test")
    public XReturn test() {
        String txt = "你真棒，真棒你好吗，我好";

        XFinder finder = new XFinder();
        finder.addWords("你好", "你真棒", "真棒");
        getR().setData("word", finder.filter(txt));

        //XFinder finder2 = new XFinder();
        //finder2.addWords("吗", "我");
        //getR().setData("word2", finder2.filter(txt));
        getR().setData("tree", finder.getTREE());
        getR().setData("tree", finder.findNotExists(txt));
        return getR();
    }
}
