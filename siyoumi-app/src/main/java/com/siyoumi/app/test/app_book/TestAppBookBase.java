package com.siyoumi.app.test.app_book;

import com.siyoumi.app.service.*;
import com.siyoumi.service.ServiceImplBase;
import com.siyoumi.test.TestAppController;

import java.util.List;

public class TestAppBookBase
        extends TestAppController {
    @Override
    public String getTestX() {
        return "yituky";
    }

    @Override
    public List<ServiceImplBase> delServices() {
        List<ServiceImplBase> listSvc = super.delServices();
        listSvc.add(SysStoreService.getBean());

        listSvc.add(BookStoreGroupService.getBean());
        listSvc.add(BookStoreService.getBean());
        listSvc.add(BookStoreTxtService.getBean());
        listSvc.add(BookSpuService.getBean());
        listSvc.add(BookSpuTxtService.getBean());
        listSvc.add(BookSpuSetService.getBean());
        listSvc.add(BookSetService.getBean());
        listSvc.add(BookSkuService.getBean());

        return listSvc;
    }
}
