package com.siyoumi.app.test.sys.entity;

import com.siyoumi.aspect.RequestLimitGetKey;
import com.siyoumi.component.http.InputData;
import com.siyoumi.util.XStr;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;

//默认key 类名 + 方法名
@Service
public class RequestLimitGetKeyTest
        implements RequestLimitGetKey {
    @Override
    public String getKey(ProceedingJoinPoint point) {
        String key = "test";
        InputData inputData = InputData.fromRequest();
        String k = inputData.input("k");
        if (XStr.hasAnyText(k)) {
            key = k;
        }

        return key;
    }
}
