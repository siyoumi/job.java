package com.siyoumi.app.test.app_book;

import com.siyoumi.exception.EnumSys;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//http://dev.x.siyoumi.com/test/app_book
@TestClass(
        //runMethods = "upload_file"
)
@Slf4j
@RestController
@RequestMapping("/test/app_book")
public class TestApiAppBook
        extends TestAppBookBase {

    @TestFunc("测试下单")
    public XReturn order() {

        return EnumSys.OK.getR();
    }
}
