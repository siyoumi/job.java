package com.siyoumi.app.test.rabbitmq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.siyoumi.app.rabbitmq.RabbitMqMessage;
import com.siyoumi.app.rabbitmq.RabbitMqPublishAbs;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

//主题 发送消息
@Slf4j
public class RabbitMqPublishTopic
        extends RabbitMqPublishAbs {
    @Override
    protected String getExchangeName() {
        return "topic_exchange";
    }

    @Override
    protected String getQueueName() {
        return "def_queue";
    }

    static public RabbitMqPublishAbs getIns() {
        RabbitMqPublishAbs mq = new RabbitMqPublishTopic();
        mq.init();
        return mq;
    }

    @SneakyThrows
    @Override
    public void init() {
        super.init();

        //创建交换机
        getChannel().exchangeDeclare(getExchangeName(), BuiltinExchangeType.TOPIC, true, false, null);
        //创建队列
        getChannel().queueDeclare(getQueueName(), true, false, false, null);

        //String routeKey = getQueueName();
        //getChannel().queueBind(getQueueName(), getExchangeName(), "prize.abc");
    }

    @Override
    @SneakyThrows
    public void publishMessage(RabbitMqMessage message) {
        AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
                .deliveryMode(2) //消息持久化
                .build();

        getChannel().basicPublish(getExchangeName(), message.getRoutingKey(), properties, message.getContent().getBytes());
    }
}