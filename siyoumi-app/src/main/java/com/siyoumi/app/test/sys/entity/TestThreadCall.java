package com.siyoumi.app.test.sys.entity;

import com.siyoumi.util.XDate;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@Slf4j
public class TestThreadCall
        implements Callable<String> {

    @Override
    public String call() {
        return XDate.toDateTimeString();
    }
}
