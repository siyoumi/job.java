package com.siyoumi.app.test.sys;

import cn.hutool.extra.mail.Mail;
import cn.hutool.extra.mail.MailAccount;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.component.XApp;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import com.sun.mail.util.MailSSLSocketFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.net.ssl.SSLSocketFactory;
import java.security.GeneralSecurityException;
import java.util.Properties;
import java.util.Random;

@Slf4j
//线程相关
@TestClass(
        runMethods = "testSend"
)
@RestController
@RequestMapping("/test/test_email")
public class testEmail
        extends TestController {
    @TestFunc("发邮箱")
    public XReturn testSend() {
        XReturn r = XReturn.getR(0);

        //System.getProperties().setProperty("mail.mime.address.usecanonicalhostname", "false");
        //System.getProperties().setProperty("mail.smtp.localhost", "smtp.qq.com");
        //log.debug("163邮箱");
        //MailAccount account = new MailAccount();
        //account.setHost("smtp.163.com");
        //account.setFrom("18819208821@163.com");
        //account.setPort(25);
        //account.setAuth(true);
        ////account.setSslEnable(true);
        //account.setUser("18819208821");
        //account.setPass("NJVLHBVUZMHRHGGO");

        log.debug("QQ邮箱");
        MailAccount account = new MailAccount();
        account.setHost("smtp.qq.com");
        account.setFrom("459624123@qq.com");
        account.setPort(465);
        account.setAuth(true);
        account.setUser("459624123");
        account.setPass("obeqjddtsgglbjbc");
        account.setSslEnable(true);
        account.setStarttlsEnable(true);
        //account.setSocketFactoryClass("javax.net.ssl.SSLSocketFactory");
        //account.setSocketFactoryFallback(false);

        Mail.create(account)
                .setTos("18819208821@163.com")
                .setTitle("邮箱验证")
                .setContent(XStr.concat("您的验证码是：<h3>" + XApp.random(1000, 9999), "</h3>"))
                .setHtml(true)
                .send();
        return r;
    }

    //@TestFunc("发邮箱")
    //public XReturn testSendB() {
    //    XReturn r = XReturn.getR(0);
    //
    //    String vcode = sendVerificationCode("18819208821@163.com");
    //    r.setData("vcode", vcode);
    //
    //    return r;
    //}
    //
    //public static Session createSession() throws GeneralSecurityException {
    //    // SMTP服务器地址
    //    // String smtp = "smtp.exmail.qq.com";
    //    //String smtp = "hwsmtp.exmail.qq.com";
    //    // String smtp = "smtp-mail.outlook.com";
    //    String smtp = "smtp.qq.com";
    //
    //    // 邮箱账号和密码(授权密码)
    //    //String userName = "pingtai@seeucareer.cn";
    //    //String password = "Seeu8888";
    //    String userName = "459624123";
    //    String password = "obeqjddtsgglbjbc";
    //
    //    /*
    //    // SMTP服务器的连接信息
    //    Properties props = new Properties();
    //    props.put("mail.smtp.host", smtp); // SMTP主机号
    //    props.put("mail.smtp.port", "465"); // 主机端口号
    //    props.put("mail.smtp.auth", "true"); // 是否需要认证
    //    // props.put("mail.smtp.starttls.enable", "true"); // 启用TLS加密
    //
    //    MailSSLSocketFactory sf = new MailSSLSocketFactory();
    //    sf.setTrustAllHosts(true);
    //    // sf.setTrustedHosts(new String[] { "my-server" });
    //    props.put("mail.smtp.ssl.enable", "true");
    //    // 还可以使用下列额外的安全措施
    //    //props.put("mail.smtp.ssl.checkserveridentity", "true");
    //    props.put("mail.smtp.ssl.socketFactory", sf);
    //     */
    //
    //    Properties props = new Properties();
    //
    //    MailSSLSocketFactory sf = new MailSSLSocketFactory();
    //    sf.setTrustAllHosts(true);
    //    // sf.setTrustedHosts(new String[] { "my-server" });
    //    props.put("mail.smtp.ssl.enable", "true");
    //    // 还可以使用下列额外的安全措施
    //    //props.put("mail.smtp.ssl.checkserveridentity", "true");
    //    props.put("mail.smtp.ssl.socketFactory", sf);
    //    props.put("mail.smtp.ssl.protocols", "TLSv1.2");
    //
    //    props.put("mail.smtp.host", smtp);
    //    props.put("mail.smtp.socketFactory.port", "465");
    //    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    //    props.put("mail.smtp.auth", "true");
    //    props.put("mail.smtp.port", "465");
    //
    //    // 创建Session
    //    // 参数1：SMTP服务器的连接信息
    //    // 参数2：用户认证对象(Authenticator接口的匿名实现类)
    //    Session session = Session.getInstance(props, new Authenticator() {
    //        @Override
    //        protected PasswordAuthentication getPasswordAuthentication() {
    //            return new PasswordAuthentication(userName, password);
    //        }
    //    });
    //
    //    // 开启调试模式
    //    session.setDebug(true);
    //
    //    return session;
    //}
    //
    //public static String sendVerificationCode(String email) {
    //    Random random = new Random();
    //    Integer num = random.nextInt(9000) + 1000;
    //    String verificationCode = num.toString();
    //
    //    try {
    //        // 1.创建Session
    //        Session session = createSession();
    //
    //        // 2.创建邮件对象(Message抽象类的子类对象)
    //        MimeMessage msg = new MimeMessage(session); // 传入session
    //        msg.setFrom(new InternetAddress("459624123@qq.com")); // 发件人
    //        // msg.setFrom(new InternetAddress("seeu.edu.member@outlook.com")); // 发件人
    //        msg.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(email)); // 收件人
    //        // msg.setRecipient(MimeMessage.RecipientType.CC, new InternetAddress("*********")); // 抄送人
    //        msg.setSubject("思优米", "utf-8"); // 标题
    //
    //        // 邮件正文中包含有“html”标签(控制文本的格式)
    //        msg.setText("您的验证码是：<h3>" + XApp.random(1000, 9999) + "</h3>", "utf-8", "html"); // 正文
    //
    //        // 3.发送
    //        Transport.send(msg);
    //
    //    } catch (MessagingException | GeneralSecurityException e) {
    //        e.printStackTrace();
    //    }
    //
    //    return verificationCode;
    //}
}
