//package com.siyoumi.app.test.rocketmq;
//
//import com.siyoumi.util.XDate;
//import com.siyoumi.util.XStr;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.rocketmq.client.producer.DefaultMQProducer;
//import org.apache.rocketmq.client.producer.SendResult;
//import org.apache.rocketmq.common.message.Message;
//
//import java.nio.charset.StandardCharsets;
//
//@Slf4j
//public class RocketmqTestProvider {
//    public static final String TOPIC = "TopicTest";
//    public static final String PRODUCER_GROUP = "ProducerGroupName";
//    public static final String NAMESRV_ADDR = "127.0.0.1:9876";
//    public static final String TAG = "TagA";
//
//    @SneakyThrows
//    public static void main(String[] args) {
//        String[] msgDelayLevel = "1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h".split(" ");
//
//        DefaultMQProducer producer = new DefaultMQProducer(PRODUCER_GROUP);
//        producer.setNamesrvAddr(NAMESRV_ADDR);
//        producer.setMaxMessageSize(4096); //消息最大4m
//        producer.setSendMsgTimeout(3000); //超时时间3秒
//
//        producer.start();
//        for (int i = 0; i < 5; i++) {
//            try {
//                int lv = i % 4;
//                String delayMsg = XStr.concat("发送时间：", XDate.toDateTimeString(), "，延迟：", msgDelayLevel[lv]);
//                Message msg = new Message(TOPIC, TAG, delayMsg.getBytes(StandardCharsets.UTF_8));
//                //msg.setDelayTimeLevel(lv);
//
//                SendResult sendResult = producer.send(msg);
//                log.debug("{}:{}", delayMsg, sendResult.toString());
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        producer.shutdown();
//    }
//}
