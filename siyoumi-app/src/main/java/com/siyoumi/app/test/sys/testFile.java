package com.siyoumi.app.test.sys;

import com.google.common.io.Files;
import com.siyoumi.app.sys.service.CommonApiServcie;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.util.XReturn;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

//文件相关
@TestClass(
        //runMethods = "testSynchronized"
)
@RestController
@RequestMapping("/test/test_file")
public class testFile
        extends TestController {

    @SneakyThrows
    @PostMapping("/test_to_file")
    public XReturn testToFile(@RequestParam("file0") MultipartFile file0) {
        File file = CommonApiServcie.toFile(file0);

        getR().setData("file_name", file.getName());
        getR().setData("file_size", file.length());

        return getR();
    }
}
