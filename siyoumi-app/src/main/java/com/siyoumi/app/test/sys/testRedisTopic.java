package com.siyoumi.app.test.sys;

import com.siyoumi.component.XRedis;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RTopic;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//redis相关
@TestClass(
        runMethods = "testRedisTopic"
)
@Slf4j
@RestController
@RequestMapping("/test/test_redis_topic")
public class testRedisTopic
        extends TestController {
    @TestFunc("订阅消息")
    public XReturn testRedisTopic() {
        XReturn r = XReturn.getR(0);

        RTopic testSub = XRedis.getBean().getTopic("test_sub");
        int i = testSub.addListener(XReturn.class, (charSequence, r1) -> {
            log.debug("收到信息:{}", XStr.toJsonStr(r1));
            log.debug(XDate.toDateTimeString());
        });
        r.setData("i", i);

        return r;
    }


    //@TestFunc("发布消息")
    @GetMapping("/testRedisPublish")
    public XReturn testRedisPublish() {
        XReturn r = XReturn.getR(0);

        RTopic testSub = XRedis.getBean().getTopic("test_sub");
        XReturn msg = XReturn.getR(0);
        msg.setData("date", XDate.toDateTimeString());
        long publish = testSub.publish(msg);
        r.setData("i", publish);

        return r;
    }
}
