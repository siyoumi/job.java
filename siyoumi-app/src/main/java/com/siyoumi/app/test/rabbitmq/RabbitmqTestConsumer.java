package com.siyoumi.app.test.rabbitmq;
//

import com.siyoumi.app.rabbitmq.RabbitMqConsumerHandle;
import com.siyoumi.app.rabbitmq.RabbitMqProperties;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RabbitmqTestConsumer {
    @SneakyThrows
    public static void main(String[] args) {
        RabbitMqProperties config = new RabbitMqProperties();
        config.setHost("app.lai-m.com:5682,app.lai-m.com:5692");
        config.setUsername("admin");
        config.setPassword("admin112233");
        config.setPort(5682);
        config.setVhost("/");

        //RabbitMqConsumerHandle mp = RabbitMqConsumerHandle.getIns(config);
        //mp.consumeMessage();
        RabbitMqConsumerHandle handle = RabbitMqConsumerHandle.getIns(config, false);
        handle.consumeMessage(new RabbitMqConsumerDirect(), "dead_queue_x");
    }
}
