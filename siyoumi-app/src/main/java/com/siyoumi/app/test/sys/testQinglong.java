package com.siyoumi.app.test.sys;

import com.siyoumi.app.modules.sh.service.QinglongApi;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.util.XReturn;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//线程相关
@TestClass(
        //runMethods = "enable_env"
)
@RestController
@RequestMapping("/test/test_qinglong")
public class testQinglong
        extends TestController {
    @SneakyThrows
    @GetMapping("/check_token")
    public XReturn checkToken() {
        QinglongApi api = QinglongApi.getInstance();
        boolean b = api.checkExpireToken("123");

        XReturn r = XReturn.getR(0);
        r.setData("check_token", b);

        return r;
    }

    @GetMapping("/set_env")
    public XReturn setEnv() {
        QinglongApi api = QinglongApi.getInstance();
        XReturn r = api.setEvn(8, "yrdy", "tewst");

        return r;
    }


    @GetMapping("/enable_env")
    public XReturn enableEnv() {
        QinglongApi api = QinglongApi.getInstance();
        XReturn r = api.evnEnable(8);

        return r;
    }
}
