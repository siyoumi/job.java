package com.siyoumi.app.test.external_api.aliyun;

import com.aliyun.oss.model.OSSObjectSummary;
import com.siyoumi.app.external_api.aliyun.ApiAliYunOss;
import com.siyoumi.app.external_api.aliyun.entity.EnvAlipayOss;
import com.siyoumi.app.modules.file.service.FileService;
import com.siyoumi.app.sys.service.FileHandle;
import com.siyoumi.app.sys.service.file.entity.FileUploadData;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

//接口
@TestClass(
        //runMethods = "upload_file"
)
@Slf4j
@RestController
@RequestMapping("/test/external_api")
public class testAliyunOss
        extends TestController {

    public ApiAliYunOss getApi() {
        return ApiAliYunOss.of(EnvAlipayOss.getBean());
    }

    @GetMapping("file_list")
    public XReturn fileList() {
        List<OSSObjectSummary> fileList = getApi().fileList("app");

        XReturn r = XReturn.getR(0);
        r.setData("fileList", fileList);
        return r;
    }

    //http://dev.x.siyoumi.com/test/external_api/upload_file
    @GetMapping("upload_file")
    public XReturn uploadFile() {
        String filePath = "F:\\_img\\3.jpg";
        XReturn r = getApi().uploadFileByRoot("app", filePath);

        //r.setData("fileList", fileList);
        return r;
    }

    @PostMapping("upload_file_handle")
    public XReturn uploadFileHandle(@RequestParam("file0") MultipartFile file) {
        FileHandle fileHandle = FileHandle.of("aliyun");
        FileUploadData data = FileUploadData.of(file, List.of("jpg"), 10, "app");
        return fileHandle.uploadFile(data);
    }

    //http://dev.x.siyoumi.com/test/external_api/get_url
    @GetMapping("get_url")
    public XReturn getUrl() {
        String ossPath = "app/1.jpg";
        XReturn r = getApi().generatePresignedUrl(ossPath);

        //r.setData("fileList", fileList);
        return r;
    }
}
