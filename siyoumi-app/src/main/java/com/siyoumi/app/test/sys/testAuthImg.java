package com.siyoumi.app.test.sys;

import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.util.*;
import com.siyoumi.component.auth_img.impl.DragAuthImgServiceImpl;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

//线程相关
@TestClass(
        //runMethods = "testSynchronized"
)
@RestController
@RequestMapping("/test/test_auth_img")
public class testAuthImg
        extends TestController {
    @SneakyThrows
    @GetMapping("/img")
    public void imgHold() {
        HttpServletResponse response = XHttpContext.getHttpServletResponse();

        BufferedImage imgBg = ImageIO.read(new File("D:\\_img\\v\\2.png"));
        BufferedImage imgHoldSrc = ImageIO.read(new File("D:\\_img\\v\\j5.png"));

        XDragAuthImg app = XDragAuthImg.getInstance();
        int x = XApp.random(0, imgBg.getWidth() - imgHoldSrc.getHeight());
        int y = XApp.random(0, imgBg.getHeight() - imgHoldSrc.getHeight());

        BufferedImage imgHold = app.createImgHold(imgHoldSrc, imgBg.getHeight(), y);
        app.cutHandleImg(imgBg, imgHold, x, y);


        //输出图片
        ServletOutputStream outputStream = response.getOutputStream();
        //调用工具类
        ImageIO.write(imgBg, "png", outputStream);
    }

    @SneakyThrows
    @GetMapping("/test")
    public XReturn test() {
        BufferedImage imgBg = ImageIO.read(new File("D:\\_img\\v\\1.png"));
        BufferedImage imgHoldSrc = ImageIO.read(new File("D:\\_img\\v\\j2.png"));

        XDragAuthImg app = XDragAuthImg.getInstance();
        int x = XApp.random(0, imgBg.getWidth() - imgHoldSrc.getHeight());
        int y = XApp.random(0, imgBg.getHeight() - imgHoldSrc.getHeight());

        BufferedImage imgHold = app.createImgHold(imgHoldSrc, imgBg.getHeight(), y);
        app.cutHandleImg(imgBg, imgHold, x, y);

        getR().setData("img_bg", XFile.toImgBase64(imgBg));
        getR().setData("img", XFile.toImgBase64(imgHold));
        return getR();
    }

    @SneakyThrows
    @GetMapping("/test_rnd_img")
    public XReturn testRndImg() {
        HttpServletResponse response = XHttpContext.getHttpServletResponse();

        List<String> imgBgArr = List.of("D:\\_job\\job.res_file\\auth_img\\1.png", "D:\\_job\\job.res_file\\auth_img\\2.png", "D:\\_job\\job.res_file\\auth_img\\3.png");
        List<String> imgHoldArr = List.of("D:\\_job\\job.res_file\\auth_img\\j1.png", "D:\\_job\\job.res_file\\auth_img\\j2.png", "D:\\_job\\job.res_file\\auth_img\\j3.png"
                , "D:\\_job\\job.res_file\\auth_img\\j4.png", "D:\\_job\\job.res_file\\auth_img\\j5.png");
//        imgHoldArr = List.of("D:\\_job\\job.res_file\\auth_img\\j2.png");

        BufferedImage imgBgSrc = ImageIO.read(new File("D:\\_job\\job.res_file\\auth_img\\1.png"));
        BufferedImage imgHoldSrc = ImageIO.read(new File("D:\\_job\\job.res_file\\auth_img\\j1.png"));

        int x = XApp.random(0, imgBgSrc.getWidth() - imgHoldSrc.getHeight());
        int y = XApp.random(0, imgBgSrc.getHeight() - imgHoldSrc.getHeight());

        DragAuthImgServiceImpl app = new DragAuthImgServiceImpl();
        app.setImgBgPathArr(imgBgArr);
        app.setImgHoldPathArr(imgHoldArr);
        List<BufferedImage> imgArr = app.getImgInfo(null);
//        app.cutHandleImg(imgBgSrc, imgHoldSrc, x, y, true);
//
//        BufferedImage imgHold = app.createImgHold(imgHoldSrc, imgBgSrc.getHeight(), y);

        //输出图片
//        ServletOutputStream outputStream = response.getOutputStream();
//        //调用工具类
//        ImageIO.write(imgArr.get(0), "png", outputStream);

//        BufferedImage imgHold = app.createImgHold(imgHoldSrc, imgBg.getHeight(), y);
//        app.cutHandleImg(imgBg, imgHold, x, y);

        getR().setData("img_bg", XFile.toImgBase64(imgArr.get(0)));
        getR().setData("img", XFile.toImgBase64(imgArr.get(1)));
        return getR();
    }
}
