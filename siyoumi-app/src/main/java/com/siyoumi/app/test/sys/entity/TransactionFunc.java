package com.siyoumi.app.test.sys.entity;

import com.siyoumi.component.XApp;
import com.siyoumi.exception.XException;
import com.siyoumi.service.SysLogService;
import org.springframework.transaction.support.TransactionTemplate;

public class TransactionFunc {
    public void save()
    {
        TransactionTemplate transaction = XApp.getTransaction();
        SysLogService appLog = SysLogService.getBean();

        transaction.execute(status -> {
            appLog.addSysErrorLog("test02", "test02");
            //throw new XException("test");
            //status.setRollbackOnly(); // 回滚
            return true;
        });
    }
}
