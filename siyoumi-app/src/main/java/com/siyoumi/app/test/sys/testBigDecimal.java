package com.siyoumi.app.test.sys;

import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;

//线程相关
@TestClass(
        runMethods = "test"
)
@Slf4j
@RestController
@RequestMapping("/test/test_bigdecimal")
public class testBigDecimal
        extends TestController {
    @TestFunc
    public XReturn test() {
        BigDecimal a = new BigDecimal("1.5656");
        BigDecimal b = new BigDecimal("0.1");
        BigDecimal c = new BigDecimal("100");

        log.debug("a={} b={}", a, b);
        log.debug("a加b：{}", a.add(b));
        log.debug("a减b：{}", a.subtract(b));
        log.debug("a乘b：{}", a.multiply(b));
        log.debug("a除b(四舍五入)：{}", a.divide(b, 2, RoundingMode.HALF_UP));
        log.debug("a保留1位小数，不四舍五入：{}", a.setScale(1, RoundingMode.DOWN));
        log.debug("a乘100，转long：{}", a.multiply(c).toBigInteger().longValue());
        //-1：a小于b
        //0：a等于b
        //1：a大于b
        log.debug("a比较大小b：{}", a.compareTo(b));

        return getR();
    }

    @TestFunc
    public XReturn test01() {
        BigDecimal a = new BigDecimal("1.565");
        BigDecimal b = new BigDecimal("0.1");

        a = a.add(b);

        log.debug("a={} b={}", a, b);

        return getR();
    }
}
