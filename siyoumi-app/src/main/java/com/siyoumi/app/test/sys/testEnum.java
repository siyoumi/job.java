package com.siyoumi.app.test.sys;

import com.siyoumi.app.modules.lucky_num.entity.EnumLnumType;
import com.siyoumi.app.modules.user.entity.EnumSex;
import com.siyoumi.component.XEnumBase;
import com.siyoumi.test.TestController;
import com.siyoumi.test.annotation.TestClass;
import com.siyoumi.test.annotation.TestFunc;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@TestClass(
        //runMethods = "testOpenFeign"
)
@Slf4j
@RestController
@RequestMapping("/test/test_enum")
public class testEnum
        extends TestController {
    @TestFunc("测试枚举")
    public XReturn test() {
        XReturn r = XReturn.getR(0);

        EnumLnumType bean = EnumLnumType.getBean();

        r.setData("b", bean);
        return r;
    }

    @TestFunc("测试枚举sex")
    public XReturn testSex() {
        XReturn r = XReturn.getR(0);

        EnumSex enumSex = XEnumBase.of(EnumSex.class);
        String sex = enumSex.get(0);

        r.setData("0", sex);
        r.setData("values", enumSex.values());
        r.setData("keys", enumSex.keys());
        r.setData("sex", enumSex);
        return r;
    }
}
