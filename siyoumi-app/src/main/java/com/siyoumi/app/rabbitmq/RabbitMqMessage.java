package com.siyoumi.app.rabbitmq;

import lombok.Data;

@Data
public class RabbitMqMessage {
    private String content; //内容
    private Integer delaySecond = 0; //延迟时间（秒）
    private String routingKey = "";

    public static RabbitMqMessage getIns(String content) {
        return getIns(content, 0);
    }

    public static RabbitMqMessage getIns(String content, String routingKey, Integer delaySecond) {
        RabbitMqMessage msg = new RabbitMqMessage();
        msg.setContent(content);
        msg.setDelaySecond(delaySecond);
        msg.setRoutingKey(routingKey);

        return msg;
    }

    public static RabbitMqMessage getIns(String content, Integer delaySecond) {
        return getIns(content, "", delaySecond);
    }
}