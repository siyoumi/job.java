package com.siyoumi.app.rabbitmq;

import lombok.Data;

@Data
public class RabbitMqProperties {
    private String host; //ip
    private String username; //帐号
    private String password; //密码
    private Integer port = 5672;
    private String vhost = "/";
}