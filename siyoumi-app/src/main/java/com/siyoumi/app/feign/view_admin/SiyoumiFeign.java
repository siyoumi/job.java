package com.siyoumi.app.feign.view_admin;

import com.siyoumi.app.entity.WxUser;
import com.siyoumi.config.FeignRequestInterceptor;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.entity.SysAppRouter;
import com.siyoumi.util.XReturn;
import com.siyoumi.component.XSpringContext;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "siyoumi", url = "https://app.lai-m.com/open/sioumi/api", configuration = FeignRequestInterceptor.class)
//@FeignClient(name = "siyoumi", url = "http://127.0.0.1:8010/open/sioumi/api", configuration = FeignRequestInterceptor.class)
public interface SiyoumiFeign {
    static SiyoumiFeign getBean() {
        return XSpringContext.getBean(SiyoumiFeign.class);
    }

    /**
     * 获取redis val
     * 获取后，会删除
     */
    @PostMapping("/redis_val")
    XReturn getRedisVal(@RequestParam String key);

    @PostMapping("/get_token")
    String getToken(@RequestParam String openid);

    @PostMapping("/get_user")
    WxUser getUser(@RequestParam String openid);

    /**
     * 更新，路由页面
     */
    @PostMapping("/update_app_router")
    XReturn updateAppRouter(@RequestBody List<SysAppRouter> list);

    /**
     * 更新，路由页面
     */
    @PostMapping("/list_appr")
    List<SysAppRouter> getListAppr();

    /**
     * 获取siyoumi的config
     * 外部接口实体，时间字段需要加
     *
     * @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
     */
    @PostMapping("/get_config_center")
    SysAccsuperConfig getConfigCenter();

    /**
     * 获取sh配置
     */
    @PostMapping("/sh_expore_config")
    String shExportConfig();
}
