package com.siyoumi.app.feign.view_admin.impl;

import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.feign.view_admin.SiyoumiFeign;
import com.siyoumi.app.modules.sh.service.SvcSh;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.component.XJwt;
import com.siyoumi.component.XRedis;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.config.SysConfig;
import com.siyoumi.controller.ApiController;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.entity.SysAppRouter;
import com.siyoumi.exception.EnumSys;
import com.siyoumi.service.SysAppRouterService;
import com.siyoumi.service.SysAppService;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.SneakyThrows;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/open/sioumi/api")
public class SiyoumiFeignImpl
        extends ApiController
        implements SiyoumiFeign {
    @Override
    @SneakyThrows
    @PostMapping("/redis_val")
    public XReturn getRedisVal(String key) {
        if (XStr.isNullOrEmpty(key)) {
            return EnumSys.MISS_VAL.getR("miss key");
        }

        String val = XRedis.getBean().getAndDel(key);
        //if (!SysConfig.getIns().isDev()) {
        //    XRedis.getBean().del(key);
        //}

        XReturn r = XReturn.getR(0);
        r.setData("val", val);
        return r;
    }

    @Override
    public String getToken(String openid) {
        XReturn tokenData = XReturn.getR(0);
        tokenData.setData("timestamp", XDate.toMs());
        tokenData.setData("openid", openid);

        //WxUser entityWxUser = WxUserService.getBean().getByOpenid(openid);
        //tokenData.setData("wxuser_headimgurl", entityWxUser.getWxuser_headimgurl());
        //tokenData.setData("wxuser_nickname", entityWxUser.getWxuser_nickname());

        Integer expireSecond = 300;
        if (SysConfig.getIns().isDev()) {
            expireSecond = 60 * 60;
        }
        XJwt jwt = XJwt.getBean();
        return jwt.getToken(tokenData, expireSecond);
    }

    @Override
    public WxUser getUser(String openid) {
        return WxUserService.getBean().getByOpenid(openid);
    }

    @Override
    @PostMapping("/update_app_router")
    @Transactional(rollbackFor = Exception.class)
    public XReturn updateAppRouter(@RequestBody List<SysAppRouter> list) {
        if (list == null || list.size() <= 0) {
            return EnumSys.MISS_VAL.getR("miss list");
        }
        return SysAppService.getBean().syncApp(list);
    }

    @Override
    @PostMapping("/list_appr")
    public List<SysAppRouter> getListAppr() {
        SysAppRouterService appRouter = SysAppRouterService.getBean();
        return appRouter.list();
    }

    @Override
    public SysAccsuperConfig getConfigCenter() {
        XHttpContext.setX("pp");

        return XHttpContext.getXConfig();
    }


    @Override
    @PostMapping("/sh_expore_config")
    public String shExportConfig() {
        XHttpContext.setX("x");

        return SvcSh.getBean().shExportConfig();
        //ShUserService app = ShUserService.getBean();
        ////
        //JoinWrapperPlus<ShUser> query = app.getQuery();
        //query.eq("suser_enable", 1);
        //List<ShUser> list = app.get(query);
        //
        //StringBuilder sb = new StringBuilder();
        //String jdCookies = list.stream().map(item -> {
        //    return XStr.format("pt_key={0};pt_pin={1};", item.getSuser_key(), item.getSuser_share_code());
        //}).collect(Collectors.joining("&"));
        //sb.append(jdCookies);
        //
        //return sb.toString();
    }
}
