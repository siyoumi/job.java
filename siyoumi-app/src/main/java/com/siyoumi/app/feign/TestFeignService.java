package com.siyoumi.app.feign;

import com.siyoumi.component.XSpringContext;
import com.siyoumi.util.XReturn;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "test", url = "http://localhost:8010")
public interface TestFeignService {
    static TestFeignService getBean() {
        return XSpringContext.getBean(TestFeignService.class);
    }

    @GetMapping("/test/test_api/test_openfeign_api")
    XReturn test();
}
