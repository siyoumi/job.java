package com.siyoumi.app;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.mapper.SysAbcMapper;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.XSpringContext;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.config.SysConfig;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.util.*;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//@SpringBootTest
public class SysTest {

    @Autowired
    private Environment environment;

    @Value("${siyoumi.env}")
    private String val_server;

    @Autowired
    private SysConfig sysConfig;

    @Test
    @DisplayName("获取配置变量")
    void test_010() {
        int aa = 66;

        String env_server = environment.getProperty("siyoumi.server");
        System.out.println("env方式：" + env_server);
        System.out.println("val方式：" + val_server);
        System.out.println(sysConfig);

        Assert.isTrue(sysConfig.isDev(), "测试环境");

//        String.join("",)
    }

    @SneakyThrows
    @Test
    @DisplayName("反射")
    void test_030() {
//        Class abc = Class.forName("com.siyoumi.app.job.entity.t_s_abc");
////        Constructor constructor = abc.getConstructor();
////        Object entity = constructor.newInstance(null);
//        SysAbc entity = new t_s_abc();
//
//        Method m = abc.getMethod("getPrefix", null);
//        Object invoke = m.invoke(entity);
//        System.out.println(invoke);

        Object entity = XBean.newIns(SysAbc.class);
        Object prefix = XBean.getAttributeVal(entity, "prefix");
        XLog.debug(this.getClass(), prefix);
    }


    @Test
    @DisplayName("获取IP")
    void test_040() {
        XLog.debug(this.getClass(), XHttpContext.getIpAddr());
    }


    @Test
    @DisplayName("测试XSpringContext")
    void test_050() {
        SysAbcMapper mapperSysAbc = XSpringContext.getBean(SysAbcMapper.class);
        XLog.debug(this.getClass(), mapperSysAbc);
    }


    @Test
    @DisplayName("测试service非Autowired用法query mapper")
    void test_110() {
        XLog.debug(this.getClass(), "test");

//        SysAccountService ins = SysAccountService.getIns();
//        SysAccount sysAccount = ins.mapper().get("1");

        //LambdaQueryWrapper<SysAccount> query = ins.lambdaQuery().eq(SysAccount::getAcc_x_id, "x");
        //List<SysAccount> list = ins.mapper().selectList(query);
        //
        //LambdaQueryWrapper<SysAccount> query2 = ins.lambdaQuery().eq(SysAccount::getAcc_x_id, "wp");
        //List<SysAccount> list1 = ins.mapper().selectList(query2);
        //
        //XLog.debug(this.getClass(), list);
        //XLog.debug(this.getClass(), list1);
        //XLog.debug(this.getClass(), sysAccount);
    }


    @Test
    @DisplayName("entity转map,map转entity")
    void test_120() {
        SysAccount entity = SysAccountService.getBean().getEntity("1");
        Map<String, Object> map = entity.toMap();

        SysAccount entityNew = XBean.fromMap(map, SysAccount.class);
    }

    @Test
    @SneakyThrows
    @DisplayName("BigDecimal操作")
    void test250() {
        BigDecimal a = new BigDecimal("1.565");
        BigDecimal b = new BigDecimal("0.1");
        BigDecimal c = new BigDecimal("100");

        XLog.debug(this.getClass(), "a=", a, " b=", b);
        XLog.debug(this.getClass(), "a加b：", a.add(b));
        XLog.debug(this.getClass(), "a减b：", a.subtract(b));
        XLog.debug(this.getClass(), "a乘b：", a.multiply(b));
        XLog.debug(this.getClass(), "a除b：", a.divide(b, RoundingMode.DOWN));
        XLog.debug(this.getClass(), "a保留1位小数，不四舍五入：", a.setScale(1, RoundingMode.DOWN));
        XLog.debug(this.getClass(), "a乘100，转long：", a.multiply(c).toBigInteger().longValue());
        //-1：a小于b
        //0：a等于b
        //1：a大于b
        XLog.debug(this.getClass(), "a大小比较b：", a.compareTo(b));
    }

}



