package com.siyoumi.app.test.dome.single;


import lombok.NonNull;

public class Single
{
    static private volatile Single prop_ins;

    private Single()
    {

    }

    static public Single getInstance()
    {
        if (prop_ins != null)
        {
            return prop_ins;
        }

        synchronized (Single.class)
        {
            if (prop_ins == null)
            {
                prop_ins = new Single();
            }
        }

        return prop_ins;
    }


    public String test()
    {
        return "test";
    }

    public String funcTest(@NonNull String a)
    {
        return a;
    }
}
