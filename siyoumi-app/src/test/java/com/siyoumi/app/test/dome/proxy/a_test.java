package com.siyoumi.app.test.dome.proxy;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class a_test {
    @Test
    void test_100() {
        User user = new User();

        UserProxy userProxy = new UserProxy();
        userProxy.setTarget(user);
        UserInterface proxy = (UserInterface) userProxy.getProxy(); //只能强转接口,否则报错
        log.debug(proxy.add());
        log.debug(proxy.del());
        log.debug(proxy.add());
    }
}
