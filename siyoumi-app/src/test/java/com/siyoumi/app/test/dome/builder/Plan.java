package com.siyoumi.app.test.dome.builder;


import lombok.Data;

@Data
public class Plan
{
    private String nameA;
    private String nameB;
    private String nameC;

}
