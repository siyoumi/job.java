package com.siyoumi.app.test.dome.builder;

abstract public class PlanBuilder<T>
{
    abstract T setA(String s);

    abstract T setB(String s);

    abstract T setC(String s);

    abstract Plan getPlan();
}
