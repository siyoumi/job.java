package com.siyoumi.app.test.dome.bridge.car;

import com.siyoumi.app.test.dome.bridge.CarInterface;

public class HuaWeiCar
        implements CarInterface
{
    @Override
    public String info()
    {
        return "华为汽车";
    }
}
