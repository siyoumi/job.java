package com.siyoumi.app.test.dome.builder;

import com.siyoumi.util.XLog;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class a_test
{
    @Test
    void test_001()
    {
        PlanDo planDo = new PlanDo();
        XLog.debug(this.getClass(), "默认套餐");
        XLog.debug(this.getClass(), planDo.getPlan());

        XLog.debug(this.getClass(), "修改套餐");
        planDo.setB("牛奶");
        XLog.debug(this.getClass(), planDo.getPlan());
    }
}
