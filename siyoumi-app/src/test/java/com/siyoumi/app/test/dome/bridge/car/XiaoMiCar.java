package com.siyoumi.app.test.dome.bridge.car;

import com.siyoumi.app.test.dome.bridge.CarInterface;

public class XiaoMiCar
        implements CarInterface
{
    @Override
    public String info()
    {
        return "小米汽车";
    }
}
