package com.siyoumi.app.test.modules.sys.api;

import com.siyoumi.app.sys.service.wxapi.WxApiApp;
import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.util.XReturn;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class testMiniProgram {
    @Test
    @DisplayName("获取用户信息")
    void test100() {
        SysAccsuperConfigService appSysConfig = SysAccsuperConfigService.getBean();
        SysAccsuperConfig entityConfig = appSysConfig.getEntity("x");

        WxApiApp app = WxApiApp.getIns(entityConfig);
        XReturn r = app.getUserInfo("123");
    }

    @Test
    @DisplayName("获取用户手机号")
    void test120() {
        SysAccsuperConfigService appSysConfig = SysAccsuperConfigService.getBean();
        SysAccsuperConfig entityConfig = appSysConfig.getEntity("x");

        WxApiApp app = WxApiApp.getIns(entityConfig);
        XReturn r = app.getUserPhone("123");
    }
}
