package com.siyoumi.app.test.dome.factory;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
public class a_test
{
    @Test
    @DisplayName("动态工厂")
    void test_1010()
    {
        ColorInterface color = ColorFactory.getIns("yollow");
        System.out.println(color);
        Assert.notNull(color, "对象为null");

        System.out.println(color.getColor());
    }
}
