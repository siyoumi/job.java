package com.siyoumi.app.test.modules.user;

import com.siyoumi.app.entity.SysAddress;
import com.siyoumi.app.service.SysAddressService;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class testAddress
{
    @Test
    @DisplayName("获取默认地址")
    void test_100()
    {
        SysAddressService appAddress = SysAddressService.getBean();

        SysAddress entityAddress = appAddress.getDef("o4Ind5N8FeNQEAF23AJMT5E08Iv0");
        XLog.debug(this.getClass(), entityAddress);

        appAddress.setDef(entityAddress);
    }

    @Test
    @DisplayName("地址")
    void test_120()
    {
        SysAddressService appAddress = SysAddressService.getBean();
        XReturn r = appAddress.getAddressPc("广东省广州市天河区龙洞49-1");
        XLog.debug(this.getClass(), r);
    }
}
