package com.siyoumi.app.test.dome.bridge;

abstract public class CarInfoAbs
{
    private CarInterface car;

    CarInfoAbs(CarInterface c)
    {
        car = c;
    }
    
    CarInterface getCar()
    {
        return car;
    }

    abstract public String info();
}
