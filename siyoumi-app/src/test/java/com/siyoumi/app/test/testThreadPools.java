package com.siyoumi.app.test;

import com.siyoumi.util.XLog;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Async;

@SpringBootTest
public class testThreadPools {
    @Test
    @DisplayName("线程测试")
    void test001() {
        ce();
    }


    @Async("ThreadPools")
    public void ce() {
        XLog.debug(this.getClass(), "开始了");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            XLog.error(this.getClass(), e.getMessage());
        }
    }


}
