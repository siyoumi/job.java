package com.siyoumi.app.test.dome.factory;

public interface ColorInterface
{
    public String getColor();
}
