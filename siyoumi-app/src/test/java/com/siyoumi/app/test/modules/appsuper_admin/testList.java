package com.siyoumi.app.test.modules.appsuper_admin;

import com.siyoumi.entity.SysAccsuperConfig;
import com.siyoumi.entity.SysApp;
import com.siyoumi.service.SysAccsuperConfigService;
import com.siyoumi.service.SysAccsuperZzzAppService;
import com.siyoumi.util.XLog;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class testList {
    @Test
    @DisplayName("x可设置的应用列表")
    void test_100() {
        SysAccsuperConfigService app = SysAccsuperConfigService.getBean();
        SysAccsuperConfig entityConfig = app.loadEntity("x");

        List<SysApp> listApps = SysAccsuperZzzAppService.getBean().listApps(entityConfig);
        XLog.debug(this.getClass(), listApps);
    }
}
