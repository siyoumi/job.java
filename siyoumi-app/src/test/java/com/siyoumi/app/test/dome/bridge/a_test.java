package com.siyoumi.app.test.dome.bridge;

import com.siyoumi.app.test.dome.bridge.car.HuaWeiCar;
import com.siyoumi.app.test.dome.bridge.car.XiaoMiCar;
import com.siyoumi.util.XLog;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class a_test
{
    @Test
    void test_100()
    {
        HuaWeiCar huaWeiCar = new HuaWeiCar();
        XiaoMiCar xiaoMiCar = new XiaoMiCar();

        Dian car1 = new Dian(huaWeiCar);
        XLog.debug(this.getClass(), car1.info());

        Dian car2 = new Dian(xiaoMiCar);
        XLog.debug(this.getClass(), car2.info());

        You car3 = new You(xiaoMiCar);
        XLog.debug(this.getClass(), car3.info());
    }
}
