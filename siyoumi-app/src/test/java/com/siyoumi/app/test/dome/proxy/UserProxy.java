package com.siyoumi.app.test.dome.proxy;

import com.siyoumi.util.XProxyDynamic;
import com.siyoumi.util.XLog;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

@Slf4j
public class UserProxy
        extends XProxyDynamic
{
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
    {
        Object invoke = super.invoke(proxy, method, args);
        if (method.getName().equals("add"))
        {
            //添加方法，做一些事件
            log.debug(method.getName() + "开始");
        }

        return invoke;
    }
}
