package com.siyoumi.app.test.dome.proxy;

public interface UserInterface
{
    public String add();

    public String del();
}
