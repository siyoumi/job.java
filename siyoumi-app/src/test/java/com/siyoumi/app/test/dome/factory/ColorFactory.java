package com.siyoumi.app.test.dome.factory;

import java.lang.reflect.Constructor;

public class ColorFactory
{
    static public ColorInterface getIns(String type)
    {
        ColorInterface color = null;
        try
        {
            Class aClass = Class.forName("com.siyoumi.app.test.dome.factory.color." + type);
            //        Constructor declaredConstructor = aClass.getDeclaredConstructor(); //私有结构函数
            Constructor declaredConstructor = aClass.getConstructor(); //公开结构函数
            color = (ColorInterface) declaredConstructor.newInstance(null);
        } catch (Exception ex)
        {

        }

        return color;
    }
}
