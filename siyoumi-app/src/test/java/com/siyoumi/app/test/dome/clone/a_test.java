package com.siyoumi.app.test.dome.clone;

import com.siyoumi.util.XDate;
import com.siyoumi.util.XLog;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class a_test
{
    @Test
    void test_100() throws CloneNotSupportedException
    {
        Car car = new Car();
        car.setCarName("小米汽车");
        car.setCarDate(XDate.now());
        XLog.debug(this.getClass(), car);

        Car cp_car = (Car) car.clone();
        cp_car.setCarDate(XDate.now());
        
        XLog.debug(this.getClass(), car);
        XLog.debug(this.getClass(), cp_car);
    }
}
