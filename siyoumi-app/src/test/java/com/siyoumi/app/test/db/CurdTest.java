package com.siyoumi.app.test.db;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.app.entity.SysAbc;
import com.siyoumi.app.entity.WxUser;
import com.siyoumi.app.mapper.MapperTest;
import com.siyoumi.app.service.SysAbcService;
import com.siyoumi.app.service.WxUserService;
import com.siyoumi.entity.*;
import com.siyoumi.mybatispuls.JoinWrapperPlus;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.service.SysAccsuperZzzAppService;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XLog;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class CurdTest {
    @Test
    void select100() {
        SysAbc abc = SysAbcService.getBean().getBaseMapper().selectById("abc");
        XLog.debug(this.getClass(), abc);
    }

    @Test
    void select() {
        Long count = SysAbcService.getBean().mapper().selectCount(null);
        Assert.isTrue(count > 5, "数量异常");
    }

    @Test
    @DisplayName("测试mapper first方法")
    void first() {
        QueryWrapper<SysAbc> query = SysAbcService.getBean().q();
        query.eq("abc_id", "fun_sign");
//        query.eq(t_s_abc.abc_id_00, "fun_sign");
        query.orderByAsc("abc_order");
        SysAbc entity = SysAbcService.getBean().first(query);


        if (entity != null) {
            LocalDateTime date_00 = entity.getAbc_date_00();
            System.out.println(entity.getAbc_date_00());
            //System.out.println(entity.getId());
            System.out.println(entity.getAbc_name());
            Assert.notNull(entity, "数量异常");
        }
    }

    @Test
    @DisplayName("mapper映射")
    void select_one() {
        WxUserService appWxuser = WxUserService.getBean();
        QueryWrapper<WxUser> query = appWxuser.q().eq(appWxuser.fdX(), "x");
        WxUser entityWxuser = appWxuser.first(query, true);

        XLog.debug(this.getClass(), entityWxuser);
    }


    @Test
    @DisplayName("测试mapper get方法")
    void get() {
        SysAccountService app = SysAccountService.getBean();
        QueryWrapper<SysAccount> query = app.q();
        query.last("limit 2");
        //无分页
        List<SysAccount> list = app.mapper().get(query);
        XLog.debug(this.getClass(), list);

        //有分页
        QueryWrapper<SysAccount> query1 = app.q();
        Page<SysAccount> page = app.page(2, 3);
        IPage<SysAccount> pageData = app.mapper().get(page, query1);
        XLog.debug(this.getClass(), pageData);
    }

    @Test
    @DisplayName("事务")
    @Transactional
    void trans() {
        //WxUserService.getBean().getByOpenid("123");
    }


    @Test
    @DisplayName("join")
    void test_join() {
        SysAccountService app = SysAccountService.getBean();
        //
        JoinWrapperPlus<SysAccount> query = app.join();
        //join
        query.join(SysRole.table(), "acc_role", "role_id");
        query.select("t_s_account.*", "role_name")
                //.select("a.*", "B.role_name")
                .ne("acc_role", "super_admin")
                .ge("role_id", "")
                .orderByDesc("acc_create_date");
        IPage<SysAccount> page = new Page<>(1, 3);
        //有分页
        IPage<Map<String, Object>> pageData = app.mapper().getMaps(page, query);
        XLog.debug(this.getClass(), pageData);

        //无分页
        //query.last("limit 3");
        //List<Map<String, Object>> list = app.mapper().joinMaps(query, join);
        //XLog.debug(this.getClass(), list);
    }

    @Test
    void test_join1() {
        SysAccsuperZzzAppService app = SysAccsuperZzzAppService.getBean();
        //query
        JoinWrapperPlus<SysAccsuperZzzApp> query = app.join();
        //join
        query.leftJoin(SysApp.table() + " AS app", "app_id", "acczapp_app_id");
        query.select("t_s_accsuper_zzz_app.*", "app_name")
                .eq("acczapp_x_id", "x")
                .like("app.app_name", "test")
                .orderByDesc("acczapp_create_date");
        IPage<SysAccsuperZzzApp> page = new Page<>(1, 10);

        IPage<Map<String, Object>> pageData = app.mapper().getMaps(page, query);
        XLog.debug(this.getClass(), pageData);
    }

    @Test
    void test_join2() {
        SysAccsuperZzzAppService app = SysAccsuperZzzAppService.getBean();

        JoinWrapperPlus<SysAccsuperZzzApp> query = new JoinWrapperPlus<>();
        query.leftJoin(SysApp.table() + " AS app", "app_id", "acczapp_app_id", "AND app_id={0}", "mall2");
        query.leftJoin(SysAccsuperConfig.table(), "acczapp_x_id", "aconfig_id", "and aconfig_id = {0}", "x");
        query.select("t_s_accsuper_zzz_app.*", "app_name")
                //.eq("acczapp_x_id", "x")
                //.like("app.app_name", "test")
                .orderByDesc("acczapp_create_date");
        IPage<SysAccsuperZzzApp> page = new Page<>(1, 10);

        IPage<Map<String, Object>> pageData = app.mapper().getMaps(page, query);
        XLog.debug(this.getClass(), pageData);
    }


    @Test
    @DisplayName("实体转table_info对象")
    void test_110() {
        TableInfo tableInfo = TableInfoHelper.getTableInfo(SysAccount.class);
        XLog.debug(this.getClass(), tableInfo.getTableName());
    }

    @Test
    @DisplayName("实体转map")
    void test_200() {
        SysAccount entity = SysAccountService.getBean().loadEntity("1");
        Map<String, Object> map = entity.toMap();

        //for (Field f : entity.getClass().getDeclaredFields()) {
        //    XLog.debug(this.getClass(), f.getType().getName());
        //}
        SysAccount entityNew = SysAccountService.getBean().loadEntity(map);
    }

    @Test
    @DisplayName("sum")
    void test_300() {
        SysAccountService app = SysAccountService.getBean();
        JoinWrapperPlus<SysAccount> query = app.join();
        BigDecimal sum = app.sum(query, "acc_id");
        XLog.debug(this.getClass(), sum);
    }

    @Test
    @DisplayName("更新实体")
    void test500() {
        WxUserService app = WxUserService.getBean();
        WxUser entity = app.loadEntity("15");

        WxUser entityUpdate = new WxUser();
        entityUpdate.setWxuser_nickname("Double");
        entityUpdate.setWxuser_headimgurl("http://baidu.com");
        //
        app.saveOrUpdatePassEqualField(entity, entityUpdate);

        XLog.debug(this.getClass(), entityUpdate);
        XLog.debug(this.getClass(), entity);
    }

    @Test
    void test600() {
        MapperTest app = MapperTest.getBean();
        XLog.debug(this.getClass(), app.test(10));
    }
}