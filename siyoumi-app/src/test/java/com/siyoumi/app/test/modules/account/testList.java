package com.siyoumi.app.test.modules.account;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.util.XLog;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest
public class testList
{
    @Test
    @DisplayName("测试列表mapper")
    void test_100()
    {
        SysAccountService app = SysAccountService.getBean();

        IPage<SysAccount> page = new Page<>(1, 10);
        IPage<Map<String, Object>> pageData = app.mapper().listToMap(page, app.q());

        XLog.debug(this.getClass(), pageData.getRecords());
    }

    void test_200()
    {
    }
}
