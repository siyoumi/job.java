package com.siyoumi.app.test.dome.proxy;

public class User
        implements UserInterface
{
    public String add()
    {
        return "添加操作";
    }

    public String del()
    {
        return "删除操作";
    }
}
