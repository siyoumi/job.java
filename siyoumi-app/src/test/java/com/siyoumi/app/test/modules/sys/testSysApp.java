package com.siyoumi.app.test.modules.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.siyoumi.app.sys.vo.ApprVo;
import com.siyoumi.entity.SysAccount;
import com.siyoumi.entity.SysAccsuperZzzApp;
import com.siyoumi.entity.SysApp;
import com.siyoumi.entity.SysAppRouter;
import com.siyoumi.service.SysAccountService;
import com.siyoumi.service.SysAccsuperZzzAppService;
import com.siyoumi.service.SysAppRouterService;
import com.siyoumi.service.SysAppService;
import com.siyoumi.component.XBean;
import com.siyoumi.component.http.XHttpContext;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XStr;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
public class testSysApp {
    @Test
    @DisplayName("获取所有应用")
    void test_100() {
        SysAccountService service = SysAccountService.getBean();
        //
        SysAccount entityAcc = service.getEntity("1");
        XHttpContext.setX(entityAcc.getAcc_x_id());

        List<SysApp> apps = SysAppService.getBean().getApps(entityAcc, true);
        for (SysApp app : apps) {
            XLog.debug(this.getClass(), app);
        }
    }

    @Test
    @DisplayName("获取所有应用菜单")
    void test_105() {
        SysAccountService service = SysAccountService.getBean();
        //
        SysAccount entityAcc = service.getEntity("1");
        XHttpContext.setX(entityAcc.getAcc_x_id());

        List<SysAppRouter> list = service.getListAppRouter(entityAcc);
        //获取所有主菜单
        List<SysAppRouter> listPid = list.stream().filter(entity ->
        {
            if (XStr.isNullOrEmpty(entity.getAppr_pid())) {
                return true;
            }
            return false;
        }).collect(Collectors.toList());

        List<ApprVo> listData = new ArrayList<>();
        for (SysAppRouter p : listPid) {
            //根据pid获取所有子菜单
            List<SysAppRouter> listChild = list.stream().filter(item ->
            {
                return item.getAppr_pid().equals(p.getKey());
            }).collect(Collectors.toList());

            //主菜单
            ApprVo pVo = new ApprVo();
            XBean.copyProperties(p, pVo);
            pVo.setMetaTitle(SysAppRouterService.getBean().getMetaTitle(p));

            //子菜单
            List<ApprVo> listChildVo = listChild.stream().map(item ->
            {
                ApprVo pVoChildren = new ApprVo();
                XBean.copyProperties(item, pVoChildren);
                pVoChildren.setMetaTitle(SysAppRouterService.getBean().getMetaTitle(item));

                return pVoChildren;
            }).collect(Collectors.toList());

            pVo.setChildren(listChildVo);
            listData.add(pVo);
        }

        XLog.debug(this.getClass(), listData);
    }

    @Test
    void test_110() {
        SysAccountService ins = SysAccountService.getBean();
        XLog.debug(this.getClass(), ins.fdKey());
        XLog.debug(this.getClass(), ins.fdX());

        SysAccount sysAccount = ins.getEntity("1", "x");

        XLog.debug(this.getClass(), sysAccount.prefix());
        XLog.debug(this.getClass(), sysAccount.getKey());
        XLog.debug(this.getClass(), sysAccount.getX());
        //XLog.debug(this.getClass(), ins.mapper().get("1"));

        SysAccsuperZzzAppService ins1 = SysAccsuperZzzAppService.getBean();

        QueryWrapper<SysAccsuperZzzApp> query1 = ins1.q();
        //QueryChainWrapper<SysAccsuperZzzApp> query1 = ins1.query();
        query1.eq(ins1.fdKey(), "4");
        XLog.debug(this.getClass(), ins1.mapper().selectOne(query1));
    }
}


