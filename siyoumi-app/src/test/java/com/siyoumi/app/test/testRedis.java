package com.siyoumi.app.test;

import com.siyoumi.component.XRedis;
import com.siyoumi.component.XRedisLock;
import com.siyoumi.util.XDate;
import com.siyoumi.util.XLog;
import com.siyoumi.util.XReturn;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.Codec;
import org.redisson.client.codec.StringCodec;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

@Slf4j
@SpringBootTest
public class testRedis {
    @Test
    @DisplayName("redis调用")
    void test_001() {
        XRedis.getBean().setEx("test", XDate.toDateTimeString(), 100);
        XRedis.getBean().setEx("test_01", XDate.toDateTimeString(), XDate.now().plusDays(1));
        String test = XRedis.getBean().get("test");
        XLog.debug(this.getClass(), test);
    }

    @Test
    @DisplayName("测试锁lambda")
    void test_021() {
        String key = "abc";
        XReturn r1 = XRedisLock.lockFunc(key, k ->
        {
            XReturn r2 = XRedisLock.lockFunc(key, k1 ->
            {
                XLog.debug(this.getClass(), "处理逻辑");
                return XReturn.getR(0);
            }, 10);

            return r2;
        }, -1);
        XLog.debug(this.getClass(), r1);
    }


    @Test
    @DisplayName("另一种方式运行")
    void test_003() {
        XRedis.getBean().setEx("test", XDate.toDateTimeString(), 100);
        XRedis.getBean().expire("test", 50);
    }

    @Test
    void test_redisson() {
        RBucket<String> aaa = XRedis.getBean().getBucket("aaa");
        //aaa.set(XDate.toDateTimeString());
        aaa.delete();
        aaa.set("你最棒~！~！" + XDate.toDateTimeString(), 100, TimeUnit.SECONDS);
        aaa.set("你最棒~aaa！~！" + XDate.toDateTimeString(), 100, TimeUnit.SECONDS);
        XLog.debug(this.getClass(), aaa.get());

    }
}
