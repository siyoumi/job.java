package com.siyoumi.app.test.dome.bridge;

public class Dian
        extends CarInfoAbs
{
    Dian(CarInterface c)
    {
        super(c);
    }

    public String info()
    {
        return getCar().info() + "-电车";
    }
}
