package com.siyoumi.app.test.dome.bridge;

public class You
        extends CarInfoAbs
{
    You(CarInterface c)
    {
        super(c);
    }

    public String info()
    {
        return getCar().info() + "-油车";
    }
}
