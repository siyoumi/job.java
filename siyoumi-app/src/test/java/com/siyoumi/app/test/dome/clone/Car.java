package com.siyoumi.app.test.dome.clone;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Car
        implements Cloneable
{
    private String carName;
    private LocalDateTime carDate;

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }
}
