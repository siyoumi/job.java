package com.siyoumi.app.test.dome.builder;


public class PlanDo
        extends PlanBuilder<PlanDo>
{
    final private Plan plan;

    PlanDo()
    {
        plan = new Plan();
        //默认套餐
        plan.setNameA("薯条");
        plan.setNameB("可乐");
        plan.setNameC("牛肉汉堡");
    }

    @Override
    PlanDo setA(String s)
    {
        plan.setNameA(s);
        return this;
    }

    @Override
    PlanDo setB(String s)
    {
        plan.setNameB(s);
        return this;
    }

    @Override
    PlanDo setC(String s)
    {
        plan.setNameC(s);
        return this;
    }

    @Override
    Plan getPlan()
    {
        return plan;
    }
}
