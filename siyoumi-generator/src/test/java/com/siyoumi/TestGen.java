package com.siyoumi;

import com.siyoumi.generator.serivce.SysGeneratorSerivce;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestGen
{
    @Autowired
    private SysGeneratorSerivce sysGeneratorSerivce;

    @Test
    @DisplayName("表名转类名")
    void test_001()
    {
        String td = "t_s_set_data";
        System.out.println(td);
        String class_name = sysGeneratorSerivce.tableNameToClassName(td);
        System.out.println(class_name);
    }
}
