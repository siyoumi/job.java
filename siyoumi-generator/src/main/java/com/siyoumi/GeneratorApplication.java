package com.siyoumi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@MapperScan({"com.siyoumi.generator.mapper", "com.siyoumi.mapper"})
@SpringBootApplication
@ConfigurationPropertiesScan("com.siyoumi.config")
public class GeneratorApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(GeneratorApplication.class, args);
    }
}
