package com.siyoumi.generator.config;

import com.siyoumi.mybatispuls.FuncInjector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusConfig
{
    @Bean
    public FuncInjector sqlInjector()
    {
        return new FuncInjector();
    }


//    @Bean
//    public MybatisPlusInterceptor mybatisPlusInterceptor()
//    {
//        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
//        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
//        return interceptor;
//    }

}
