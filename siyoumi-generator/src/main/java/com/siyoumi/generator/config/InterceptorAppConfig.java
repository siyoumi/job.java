package com.siyoumi.generator.config;

import com.siyoumi.generator.interceptor.GenInterceptor;
import com.siyoumi.interceptor.CommonInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorAppConfig
        implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new GenInterceptor())
                .addPathPatterns("/**")
                .order(0);
    }
}
