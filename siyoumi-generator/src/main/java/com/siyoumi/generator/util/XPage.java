package com.siyoumi.generator.util;

import com.siyoumi.util.XReturn;
import com.siyoumi.util.XStr;
import lombok.NonNull;

import java.util.Map;

public class XPage
        extends XReturn
{
    public XPage(@NonNull Integer page, @NonNull Integer page_size)
    {
        setData("page_index", page);
        setData("page_size", page_size);
    }

    static public XPage getR()
    {
        return new XPage(1, 10);
    }

    static public XPage parse(Map<String, Object> data)
    {
        XPage r = XPage.getR();
        r.putAll(data);

        Integer page = 1;
        if (data.containsKey("page"))
        {
            page = XStr.toInt((String) data.get("page"), 1);
            page = Math.max(page, 1);
        }

        Integer page_size = 10;
        if (data.containsKey("page_size"))
        {
            page_size = XStr.toInt((String)data.get("page_size"), 10);
            page_size = Math.min(page_size, 500);
        }

        r.setData("page_index", (page - 1) * page_size);
        r.setData("page_size", page_size);

        return r;
    }
}
