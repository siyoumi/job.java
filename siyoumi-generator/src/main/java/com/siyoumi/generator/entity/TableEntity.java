/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.siyoumi.generator.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 表数据
 *
 * @author Mark sunlightcs@gmail.com
 */
@Data
public class TableEntity
        implements Serializable
{
    //数据库
    private String db;
    //表的名称
    private String tableName;
    //类的名称
    private String className;
    //表的备注
    private String tableComment;
    //表字段前缀
    private String prefix;
    //表的列名(不包含主键)
    private List<ColumnEntity> columns;

    //包名
    private String packageEntity;
    //entity文件内容
    private String fileStr;
    //entityBase文件内容
    private String fileStrEntityBase;
    //mapper文件内容
    private String fileStrMapper;
    //service文件内容
    private String fileStrService;
    //serviceImpl文件内容
    private String fileStrServiceImpl;
}
