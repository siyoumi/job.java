/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.siyoumi.generator.entity;


import com.siyoumi.util.XStr;
import lombok.Data;

@Data
public class ColumnEntity {
    //列名
    private String columnName;
    //列名类型
    private String dataType;
    //字段长度：50
    private String characterMaximumLength;
    //列名类型说细，例：int(11) unsigned
    private String columnType;
    //列名备注
    private String columnComment;
    //属性类型
    private String columnKey;
    //auto_increment
    private String extra;
    //编码 utf8mb4
    private String characterSetName;
    //编码 utf8mb4_general_ci
    private String collationName;

    //是否可以null no or yes
    private String isNullable;

    /**
     * 类型设置 unsigned
     */
    public Boolean setUnsigned() {
        return XStr.contains(getColumnType(), "unsigned");
    }

}
