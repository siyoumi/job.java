package com.siyoumi.generator.mapper;

import com.siyoumi.generator.entity.ColumnEntity;
import com.siyoumi.generator.entity.TableEntity;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


@Component
public interface MapperDb {
    @Select("SELECT column_name columnName, data_type dataType, column_comment columnComment, column_key columnKey,column_type columnType,character_maximum_length characterMaximumLength, character_set_name characterSetName, collation_name collationName, extra extra,is_nullable isNullable\n" +
            "FROM information_schema.columns\n" +
            "WHERE table_name = #{table_name} AND table_schema IN ('wx_app' , 'wx_app_x')\n" +
            "ORDER BY ordinal_position")
    List<ColumnEntity> tableColumns(String table_name);

    List<TableEntity> tableList(Map<String, Object> params);

    @Select("SELECT table_schema db,table_name tableName,table_comment tableComment, create_time\n" +
            "FROM information_schema.tables\n" +
            "WHERE table_name = #{table_name} AND table_schema IN ('wx_app' , 'wx_app_x')")
    TableEntity tableOne(String table_name);
}
