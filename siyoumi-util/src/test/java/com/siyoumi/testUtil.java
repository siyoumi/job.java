package com.siyoumi;

import com.siyoumi.util.XDate;
import com.siyoumi.util.XLog;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class testUtil {
    @Test
    @DisplayName("二进制运算")
    void test101() {
        XLog.debug(this.getClass(), "1 << 10 = ", Integer.toBinaryString(1 << 10), " = ", 1 << 10);
        XLog.debug(this.getClass(), "1 << 22 = ", Integer.toBinaryString(1 << 22), " = ", 1 << 22);
        XLog.debug(this.getClass(), "1 << 2 = ", Integer.toBinaryString(1 << 2), " = ", 1 << 2);
        XLog.debug(this.getClass(), "4 << 2 = ", Integer.toBinaryString(4 << 2), " = ", 4 << 2);
        XLog.debug(this.getClass(), "4 >> 2 = ", Integer.toBinaryString(4 >> 2), " = ", 4 >> 2);
        XLog.debug(this.getClass(), "1 >> 10 = ", Integer.toBinaryString(1 >> 10), " = ", 1 >> 10);
        XLog.debug(this.getClass(), "10 | 1 = ", 10 | 1);
        XLog.debug(this.getClass(), "10 | 2 = ", 10 | 2);
        XLog.debug(this.getClass(), "10 | 3 = ", 10 | 3);
        XLog.debug(this.getClass(), "11 | 1 = ", 11 | 1);
        XLog.debug(this.getClass(), "11 | 2 = ", 11 | 2);
        XLog.debug(this.getClass(), "11 | 3 = ", 11 | 3);
        XLog.debug(this.getClass(), "11 | 10 = ", 11 | 10);
    }

    @Test
    void test102() {
        int i = 100_00_0;

        XLog.debug(this.getClass(), i);
    }

    @Test
    void testDate() {
        String yyyMMdd = "2023-02-23 12:";
        String reg = "^\\d{4}\\-\\d{2}\\-\\d{2}$";
        //reg = "^\\d{4}\\-\\d{2}\\-\\d{2} \\d{2}\\:\\d{2}\\:\\d{2}$";
        Pattern p = Pattern.compile(reg);
        Matcher matcher = p.matcher(yyyMMdd);

        boolean matches = matcher.matches();

        XLog.debug(this.getClass(), yyyMMdd);
        XLog.debug(this.getClass(), matches);

        //LocalDateTime date1 = XDate.parse(yyyMMdd);
        //XLog.debug(this.getClass(), date1);
    }


}
