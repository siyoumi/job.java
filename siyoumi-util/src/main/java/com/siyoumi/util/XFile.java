package com.siyoumi.util;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class XFile {
    /**
     * 图片后缀
     */
    static public List<String> getImgExtenstions() {
        List<String> ex = new ArrayList<>();
        ex.add("png");
        ex.add("jpg");
        ex.add("gif");
        ex.add("jpeg");
        return ex;
    }

    static public List<String> getFileExtenstions() {
        List<String> ex = new ArrayList<>();
        ex.add("txt");
        ex.add("xlsx");
        ex.add("xls");
        ex.add("doc");
        ex.add("docx");
        ex.add("pdf");
        return ex;
    }

    /**
     * web文件后缀
     */
    static public List<String> getWebFileExtenstions() {
        List<String> ex = new ArrayList<>();
        ex.add("js");
        ex.add("css");
        ex.add("html");
        ex.add("htm");
        ex.add("txt");
        return ex;
    }

    /**
     * 根据链接获取图片流
     */
    static public BufferedImage toImgBuffer(String url) throws IOException {
        return ImageIO.read(new URL(url));
    }

    /**
     * 返回图片base64值
     */
    static public String toImgBase64(File file) throws IOException {
        FileInputStream inputStream = new FileInputStream(file);
        byte[] bytes = new byte[inputStream.available()];
        inputStream.read(bytes, 0, inputStream.available());
        return XStr.base64Enc(bytes);
    }

    @SneakyThrows
    static public String toImgBase64(BufferedImage img) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();//io流
        ImageIO.write(img, "png", baos);//写入流中

        byte[] bytes = baos.toByteArray();//转换成字节
        String base64 = XStr.base64Enc(bytes);
        base64 = base64.replaceAll("\n", "").replaceAll("\r", "");//删除 \r\n
        return "data:image/jpg;base64," + base64;
    }

    /**
     * 文件保存
     *
     * @param fileByte     二进制
     * @param saveFilePath 文件保存路径
     */
    @SneakyThrows
    static public void save(byte[] fileByte, String saveFilePath) {
        log.debug("saveFilePath: {}", saveFilePath);

        File file = new File(saveFilePath);
        //文件保存
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        bufferedOutputStream.write(fileByte);

        bufferedOutputStream.close();
        fileOutputStream.close();
    }

    ///**
    // * 文件访问链接
    // */
    //static public String fileUrl(String filePath) {
    //    if (XStr.isNullOrEmpty(filePath)) {
    //        return "";
    //    }
    //
    //    if (XStr.startsWith(filePath, "http")) {
    //        return filePath;
    //    }
    //
    //    String imgRoot = SysConfig.getIns().getImgRoot();
    //    return XStr.concat(imgRoot, filePath);
    //}


    /**
     * 获取文件内容
     *
     * @param filePath 文件路径
     */
    @SneakyThrows
    static public String getContent(String filePath) {
        return Files.readString(Paths.get(filePath));
    }

    /**
     * 写入文件内容
     *
     * @param filePath
     * @param content
     */
    @SneakyThrows
    static public void putContent(String filePath, String content) {
        Files.writeString(Paths.get(filePath), content);
    }

    /**
     * 创建目录
     *
     * @param dirPath
     */
    static public void mkdirs(String dirPath) {
        File file = new File(dirPath);
        if (!file.exists()) {
            //目录不存在，创建
            file.mkdirs();
        }
    }

    /**
     * 文件或者目录是否存在
     */
    static public Boolean exists(String path) {
        File file = new File(path);
        return file.exists();
    }
}
