package com.siyoumi.util;

import lombok.extern.slf4j.Slf4j;

import java.awt.*;
import java.awt.image.BufferedImage;

@Slf4j
public class XAuthImg {
    static Integer font_size = 30;
    private Graphics2D graphics;
    private Integer width;
    private Integer heigth;

    static public BufferedImage createAuthCode(String vcode) {
        Integer code_len = vcode.length();//验证码长度
        Integer font_size = XAuthImg.font_size;//字体大小
        //图片宽高
        Double width_d = font_size * code_len * 1.5 + font_size / 2;
        Integer width = width_d.intValue();
        Integer height = font_size * 2;
        log.debug("width: {}", width);
        log.debug("height: {}", height);
        //图片
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();
        g.setColor(Color.WHITE);//设置背景色
        g.fillRect(0, 0, width, height);//填充背景
//        g.setColor(Color.LIGHT_GRAY);//设置边框颜色

        XAuthImg ins = new XAuthImg();
        ins.graphics = g;
        ins.width = width;
        ins.heigth = height;

        log.debug("添加干扰");
        ins.addNoise();
        log.debug("绘验证码: {}", vcode);
        ins.addAuthCode(vcode);
        ins.dispose();
        g.dispose();

        return image;
    }

    /**
     * 添加干扰
     */
    private void addNoise() {
        String noise_list = "2345678abcdefhijkmnpqrstuvwxyz";
        String[] noise_arr = noise_list.split("");

        Font font = new Font("Arial", Font.ITALIC, 10);
        for (int i = 0; i < 10; i++) {
            this.graphics.setColor(new Color(XApp.random(150, 225), XApp.random(150, 225), XApp.random(150, 225)));
            for (int j = 0; j < 5; j++) {
                String s = noise_arr[XApp.random(0, 29)];
                addString(s, XApp.random(0, this.width), XApp.random(0, this.heigth), font);
            }
        }
    }

    private void addAuthCode(String vcode) {
        String[] vcode_arr = vcode.split("");

        this.graphics.setColor(new Color(XApp.random(1, 150), XApp.random(1, 150), XApp.random(1, 150)));

        Integer font_size = XAuthImg.font_size;
        Font font = new Font("宋体", Font.BOLD, font_size + 15);
        Double x = 0.0;
        Double y = 0.0;
        for (int i = 0; i < vcode_arr.length; i++) {
            String s = vcode_arr[i];
            y = font_size * (XApp.random(10, 20) / 10.0);
            x += font_size * (XApp.random(12, 14) / 10.0);
            XLog.debug(this.getClass(), "x:", x);
            XLog.debug(this.getClass(), "y:", y);
            //旋转
            int rnd = XApp.random(-50, 50);
            log.debug("rnd: {}", rnd);
            this.graphics.rotate(Math.toRadians(rnd), x, y);
            addString(s, x.intValue(), y.intValue(), font);
            this.graphics.rotate(Math.toRadians(-rnd), x, y);
        }
    }

    private void dispose() {
        this.graphics.dispose();
    }

    public void addString(String str, Integer x, Integer y, Font font) {
        if (font != null) {
            graphics.setFont(font);
        }
        graphics.drawString(str, x, y);
    }
}

