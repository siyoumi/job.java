package com.siyoumi.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class XLog {
    public static void debug(Class<?> class_name, Object... msg) {
        Logger logger = LoggerFactory.getLogger(class_name);
        logger.debug(toMsg(msg));
    }

    public static void info(Class<?> class_name, Object... msg) {
        Logger logger = LoggerFactory.getLogger(class_name);
        logger.info(toMsg(msg));
    }

    public static void warn(Class<?> class_name, Object... msg)
    {
        Logger logger = LoggerFactory.getLogger(class_name);
        logger.warn(toMsg(msg));
    }

    public static void error(Class<?> class_name, Object... msg) {
        Logger logger = LoggerFactory.getLogger(class_name);
        logger.error(toMsg(msg));
    }

    private static String toMsg(Object... msg) {
        StringBuilder sb = new StringBuilder();
        for (Object m : msg) {
            if (msg == null) continue;

            String msg_str = "";
            if (m instanceof String) {
                msg_str = (String) m;
            } else {
                msg_str = XJson.toJSONString(m);
            }
            sb.append(msg_str);
        }

        return sb.toString();
    }
}
