package com.siyoumi.util.entity;

import lombok.Data;

@Data
public class DragAuthImgOption {
    Integer width = 310;
    Integer height = 155;
    Integer holdImgSize = 47;
}
