package com.siyoumi.util.entity;

import java.util.HashMap;
import java.util.Map;

public class ThreadLocalData {
    static ThreadLocal<Map<String, Object>> tl = new ThreadLocal<>();

    static public void set(String k, Object v) {
        Map<String, Object> data = tl.get();
        if (data == null) {
            data = new HashMap<>();
        }

        data.put(k, v);
        tl.set(data);
    }

    static public <T> T get(String k, T def) {
        Map<String, Object> data = tl.get();
        if (data == null) {
            return def;
        }

        Object v = data.get(k);
        if (v == null) {
            return def;
        }

        return (T) v;
    }

    /**
     * 回收
     */
    static public void remove() {
        tl.remove();
    }
}
