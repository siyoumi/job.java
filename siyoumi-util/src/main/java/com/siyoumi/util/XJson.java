package com.siyoumi.util;

import com.alibaba.fastjson.JSON;

import java.util.List;

//json工具类
public class XJson {
    public static <T> T parseObject(String json, Class<T> clazz) {
        return JSON.parseObject(json, clazz);
    }

    public static String toJSONString(Object o) {
        return JSON.toJSONString(o);
    }

    public static <T> List<T> parseArray(String json, Class<T> clazz) {
        return JSON.parseArray(json, clazz);
    }
}
