package com.siyoumi.util;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;
import java.awt.image.BufferedImage;

//图片拖拽验证
@Slf4j
public class XDragAuthImg {
    public static XDragAuthImg getInstance() {
        XDragAuthImg app = new XDragAuthImg();

        return app;
    }

    /**
     * 生成方块图片
     *
     * @param imgHold 方块
     * @param rndY    随机y
     */
    @SneakyThrows
    public BufferedImage createImgHold(BufferedImage imgHold, int bgHeigth, Integer rndY) {
        Integer width = imgHold.getWidth();
        Integer height = bgHeigth;
        //图片
        //TYPE_INT_ARGB 创建一个带透明色的对象
        //TYPE_INT_RGB 创建一个不带透明色的对象
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = image.createGraphics();
//        g.clearRect(0, 0, width, height); //清空
//        g.setColor(Color.WHITE);//设置背景色
//        g.setComposite(AlphaComposite.Clear); //设置背景透明
//        g.fillRect(0, 0, width, height);//填充背景
//        int rnd = XApp.random(-50, 50);
        g.drawImage(imgHold, 0, rndY, imgHold.getWidth(), imgHold.getHeight(), null);

        g.dispose();

        return image;
    }


    /**
     * 背景图与方块图片，扣图处理
     *
     * @param imgBg   背景图
     * @param imgHold 方块图片
     */
    public void cutHandleImg(BufferedImage imgBg, BufferedImage imgHold, int rndX, int rndY) {
        //临时数组遍历用于高斯模糊存周边像素值
        int[][] martrix = new int[3][3];
        int[] values = new int[9];

        int xLength = imgHold.getWidth();
        int yLength = imgHold.getHeight();
        // 模板图像宽度
        for (int x = 0; x < xLength; x++) {
            // 模板图片高度
            for (int y = 0; y < yLength; y++) {
                // 如果模板图像当前像素点不是透明色 copy源文件信息到目标图片中
                int rgb = imgHold.getRGB(x, y);
                if (rgb == 0) {
                    continue;
                }

                //占位符图片白色rgb视为边界
                boolean isBroder = Color.white.getRGB() == rgb; //描边
                try {
                    if (isBroder) {
                        //边框设置白色
                        imgBg.setRGB(rndX + x, y, Color.white.getRGB());
                    } else {
                        int bgRGB = imgBg.getRGB(rndX + x, y);
                        //占位图赋值
                        imgHold.setRGB(x, y, bgRGB);

                        //抠图区域高斯模糊
                        readPixel(imgBg, rndX + x, y, values);
                        fillMatrix(martrix, values);
                        imgBg.setRGB(rndX + x, y, avgMatrix(martrix));
                    }
                } catch (Exception ex) {
                    log.debug("xl: {},yl: {}", xLength, yLength);
                    log.debug("x: {},y: {}", rndX, rndY);
                    log.debug("i: {},j: {}", rndX + x, rndY + y);

                    throw ex;
                }
            }
        }
    }


    private static int avgMatrix(int[][] matrix) {
        int r = 0;
        int g = 0;
        int b = 0;
        for (int i = 0; i < matrix.length; i++) {
            int[] x = matrix[i];
            for (int j = 0; j < x.length; j++) {
                if (j == 1) {
                    continue;
                }
                Color c = new Color(x[j]);
                r += c.getRed();
                g += c.getGreen();
                b += c.getBlue();
            }
        }
        return new Color(r / 8, g / 8, b / 8).getRGB();
    }

    private static void fillMatrix(int[][] matrix, int[] values) {
        int filled = 0;
        for (int i = 0; i < matrix.length; i++) {
            int[] x = matrix[i];
            for (int j = 0; j < x.length; j++) {
                x[j] = values[filled++];
            }
        }
    }

    private static void readPixel(BufferedImage img, int x, int y, int[] pixels) {
        int xStart = x - 1;
        int yStart = y - 1;
        int current = 0;
        for (int i = xStart; i < 3 + xStart; i++) {
            for (int j = yStart; j < 3 + yStart; j++) {
                int tx = i;
                if (tx < 0) {
                    tx = -tx;

                } else if (tx >= img.getWidth()) {
                    tx = x;
                }
                int ty = j;
                if (ty < 0) {
                    ty = -ty;
                } else if (ty >= img.getHeight()) {
                    ty = y;
                }
                pixels[current++] = img.getRGB(tx, ty);

            }
        }
    }
}

