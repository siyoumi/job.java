package com.siyoumi.util;

public class RedisKey {
    /**
     * 登陆错误次数
     */
    public static String getLoginErrCount(String accUid) {
        return XStr.concat("loginErrCount:", accUid);
    }

    /**
     * 登陆验证码
     */
    public static String getLoginAuthCode(String ip) {
        return XStr.concat("vcode:", ip);
    }

    /**
     * 后台令牌
     */
    public static String getAdminToken(String token) {
        return XStr.concat("admin_token:", token);
    }

    /**
     * 记录存活token
     *
     * @param x
     * @param uid
     */
    public static String getAdminTokenLive(String x, String uid) {
        return XStr.concat("admin_token_live:", x, "@", uid);
    }
}
