package com.siyoumi.util;

//字符串工具类
public class XSqlStr {
    static public String LIMIT_1 = "LIMIT 1";
    static public String FOR_UPDATE = "FOR UPDATE";

    static public String limitX(Integer x) {
        return limitX(x, 0);
    }

    static public String limitX(Integer x, Integer offset) {
        return XStr.concat("LIMIT " + x, " OFFSET " + offset);
    }


    /**
     * 表名转类名
     * <p>
     * t_s_app to sys_app
     * t_a_sheet to sys_sheet
     * </p>
     *
     * @param tableName 表名
     * @return 类名
     */
    static public String tableNameToClassName(String tableName) {
        //去掉t_
        tableName = tableName.replaceFirst("t_", "");

        //t_s_app to sys_app
        //t_a_sheet to sys_sheet
        //t_wx_user_a_info to wx_user_info
        if (XStr.startsWith(tableName, "a_")) {
            tableName = tableName.replace("a_", "sys_");
        } else if (XStr.startsWith(tableName, "s_")) {
            tableName = tableName.replace("s_", "sys_");
        } else if (XStr.startsWith(tableName, "m_")) {
            //商城
            tableName = tableName.replace("m_", "mall_");
        }

        if (XStr.contains(tableName, "_a_")) {
            tableName = tableName.replace("_a", "");
        }

        tableName = XStr.lineToHump(tableName);
        tableName = XStr.toUpperCase1(tableName);

        return tableName;
    }
}
