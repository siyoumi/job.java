package com.siyoumi.util;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public interface IEnum {
    String getKey();

    String getValue();


    static <E extends Enum<E> & IEnum, V> String getEnmuVal(Class<E> clazz, Object key, String def) {
        if (key == null) {
            return def;
        }

        LinkedHashMap<String, String> map = toMap(clazz);
        String v = map.get(key.toString());
        if (XStr.isNullOrEmpty(v)) {
            return def;
        }

        return v;
    }

    /**
     * 根据key获取val
     */
    static <E extends Enum<E> & IEnum> String getEnmuVal(Class<E> clazz, Object key) {
        return getEnmuVal(clazz, key, null);
    }


    Map<String, LinkedHashMap<String, String>> listMap = new HashMap<>();

    /**
     * 转map
     */
    static <E extends Enum<E> & IEnum> LinkedHashMap<String, String> toMap(Class<E> clazz) {
        String keyMap = clazz.getName();
        LinkedHashMap<String, String> map = listMap.get(keyMap);
        if (map != null) {
            return map;
        }

        EnumSet<E> all = EnumSet.allOf(clazz);

        map = new LinkedHashMap<>();
        for (E e : all) {
            map.put(e.getKey(), e.getValue());
        }
        listMap.put(keyMap, map);

        return map;
    }
}
